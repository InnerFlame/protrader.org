<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 * Date: 29.04.2016
 * Time: 12:26
 */

namespace app\controllers;


use app\components\CustomController;
use app\models\community\Comments;
use app\modules\forum\models\FrmComment;
use app\modules\forum\models\FrmTopic;
use Yii;
use yii\base\Event;

class EventsController extends CustomController
{
    public function actionIndex()
    {
        return $this->render('index.twig');
    }

    public function actionPush($name)
    {
        $topic = FrmTopic::findOne(145);
        $comment = Comments::findOne(12);
        $user = FrmComment::findOne(1);
//        VarDumper::dump($user,10,true); exit;
        Yii::$app->trigger($name, new Event(['sender' => $user]));
        return $this->redirect(Yii::$app->request->referrer);
    }
}