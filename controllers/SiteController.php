<?php

namespace app\controllers;

use app\components\CustomController;
use app\components\mailer\CustomMailer;
use app\models\forms\DemoAccountForm;
use app\models\forms\DownloadForm;
use app\models\forms\FeedbackForm;
use app\models\user\User;
use app\models\user\UserIdentity;
use app\modules\articles\models\Articles;
use app\modules\brokers\models\Improvement;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\HttpException;
use yii\web\Response;

class SiteController extends CustomController
{
    public $layout = false;

    public $limitHomePageArticles = 5;

    public $search_visible = false;

    public function behaviors()
    {
        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'rules' => [
//                    [
//                        'actions' => ['index', 'error'],
//                        'allow' => true,
//                        'roles' => ['?'],
//                    ],
//                ],
//            ],
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Return error twig array
     *
     * @return array
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'app\components\FlatErrorAction',
                'view'  => '404.twig'
            ],
        ];
    }

    /**
     * HomePage
     * @return string
     * @throws HttpException
     */
    public function actionIndex()
    {
        throw new HttpException(404);
        // $this->redirect('/' , 404);
        $this->breadcrumbs = [];

        $articles = Articles::find()
            ->orderBy('id DESC')
            ->limit($this->limitHomePageArticles)
            ->all();

        $countImprovement = count(Improvement::find()->select('id')->all());

        return $this->render('index.twig', [
            'articles'         => $articles,
            'random_feature'   => Improvement::find()->where('is_main = 1')->orderBy('RAND()')->one(),
            'countImprovement' => Improvement::getCountImprovement(),
        ]);
    }

    /**
     * window Сообшить об ошибке
     */
    public function actionFeedback()
    {
        $post = Yii::$app->request->post();
        if (isset($post['FeedbackForm']['website']) && empty($post['FeedbackForm']['website'])) { # robots
            $feedback_form = new FeedbackForm();
            if ($feedback_form->load($post)) {
                $form_data = [
                    'theme'       => $feedback_form->theme ? $feedback_form->theme : Yii::$app->user->identity->fullname,
                    'description' => $feedback_form->description,
                    // 'category'    => $feedback_form->getCategoryName($feedback_form->category_id),
                    'email'       => $feedback_form->email ? $feedback_form->email : Yii::$app->user->identity->email
                ];


                //$params = array_merge($form_data, GeoHelper::getGeoDataToMail());

                if (CustomMailer::send(Yii::$app->params['adminEmail'], 'feedback.twig', $form_data)) {
                    Yii::$app->session->setFlash('success', Yii::t('app', 'The e-mail sent to an administrator'));
                } else {
                    Yii::$app->session->setFlash('danger', Yii::t('app', 'Error'));
                }

                return $this->redirect(Yii::$app->request->referrer);
            }
            Yii::$app->session->setFlash('warning', Yii::t('app', 'Error.'));

            return $this->goHome();
        }
    }

    /**
     * Kbase documentation/index
     * @return mixed
     */
    public function actionPtmcIndex()
    {
        //   /site/ptmc-index
        return $this->render('/ptmc/index.twig');
    }

    /**
     * @return string
     */
    public function actionDownload()
    {
        $model = new DownloadForm();

        $must_download = 'false';

        $file = 'http://protrader.org/install_update/ProtraderMC/ProtraderMC.exe';
        $file64 = 'http://protrader.org/install_update/ProtraderMC64/ProtraderMC (64 bit).exe';

        if (Yii::$app->request->post()) {
            $model->scenario = User::SCENARIO_SIGN_UP;
            if ($model->load(Yii::$app->request->post())) {
                if ($model->validate() === true) {
                    $must_download = 'true';
                    $model->auth_key = md5(microtime() . rand());
                    $user = UserIdentity::findByUsernameOrEmail($model->email);
                    if ($user instanceof UserIdentity) {
                        Yii::$app->session->setFlash('success', Yii::t('app', 'This e-mail address already exists.'));
                    } else {
                        $model->registration();
                        Yii::$app->session->setFlash('success', Yii::t('app', 'The link to confirm registration has been sent to your e-mail.'));
                    }
                }
            }
        }

        $count = User::find()->where(['not in', 'role', [User::ROLE_MODERATOR, User::ROLE_ADMIN]])->count();

        return $this->render('download.twig', [
            'model'         => $model,
            'file'          => $file,
            'file64'        => $file64,
            'post'          => $_POST,
            'must_download' => $must_download,
            'tradersCount'  => ceil($count / 100) * 100
        ]);
    }

    public function actionSetLike()
    {
        Yii::$app->counter_like->setLike();
    }

    /**
     * Create demo account by soap API, or validate
     * AccountDemoForm.
     * @param string $action save or validate
     * @return DemoAccountForm|array
     * @throws BadRequestHttpException
     */
    public function actionDemo($action = 'validate')
    {
        $model = new DemoAccountForm();

        if ($action == 'form') {
            $this->viewPath = "@app/modules/brokers/views";

            return $this->renderAjax('default/_include/_form_demo_register');
        }

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $action == 'validate') {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($model);
        }

        if (Yii::$app->request->post() && $action == 'save') {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                if ($model->sendRequest()) {
                    $form_data = [
                        'login'    => $model->login,
                        'password' => $model->password,
                    ];
                    CustomMailer::send($model->email, 'after_demo_acc_registration.twig', $form_data);

                    return $this->renderAjax('_response_demo.twig', ['model' => $model]);
                }
            }
        }
        Yii::$app->response->format = Response::FORMAT_JSON;

        return ['message' => $model->message, 'attributes' => $model->attributes, 'error' => true];
    }

    public function actionTest()
    {
        return $this->render('test.twig');
    }

    public function actionStats()
    {
        return $this->render('cross-stat.twig');
    }

    public function actionStatsmy()
    {
        return $this->render('cross-my-stat.twig');
    }
}
