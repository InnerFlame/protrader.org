<?php

namespace app\controllers;

use app\components\Entity;
use app\models\main\Pages;
use app\modules\brokers\models\Improvement;
use Yii;
use app\components\CustomController;
use app\models\main\Faq;
use app\models\main\FaqCategory;
use app\models\Constants;
use yii\helpers\Url;

class FaqController extends CustomController
{
    public $indexUrl = '/faq';
    /**
     * Return error twig array
     *
     * @return array
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'app\components\FlatErrorAction',
                'view' => 'error.twig'
            ],
        ];
    }

    /**
     * Redirect to company/index
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $categories = FaqCategory::find()->where(['deleted' => Entity::NOT_DELETED])->orderBy('weight DESC')->all();

        return $this->render('index.twig',[
            'categories' => $categories,
            'restrictModelsIds' => false,
            'model' => Pages::getCeoCurrentPage(),
        ]);
    }

    public function actionSearch()
    {
        $models = Yii::$app->search->getCollection(['title', 'answer'], Yii::$app->request->get('search'))->orderBy('category_id')->all();

        $cat_set =  [];
        $models_set =  [];

        foreach($models as $model){
            $cat_set[] = $model->category_id;
            $models_set[] = $model->id;
        }


        $models_set = array_unique($models_set);
        $cat_set = array_unique($cat_set);

        $categories = [];

        if(count($cat_set) > 0)
            $categories = FaqCategory::find()->where('id IN (' . implode(',', $cat_set) . ')')->orderBy('weight DESC')->all();

        if(count($categories) === 0 && count($models_set) === 0)
            Yii::$app->session->setFlash('warning', 'Nothing found.');

        return $this->render('search.twig',[
            'categories' => $categories,
            'restrictModelsIds' => $models_set,
        ]);
    }

}
