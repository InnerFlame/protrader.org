<?php

namespace app\controllers;

use app\components\CustomController;
use app\models\main\Pages;
use Yii;
use yii\web\HttpException;

class PagesController extends CustomController
{
    public $search_visible = false;

    /**
     * Return error twig array
     *
     * @return array
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'app\components\FlatErrorAction',
                'view'  => 'error.twig'
            ],
        ];
    }

    /**
     * @return string
     * @throws HttpException
     */
    public function actionIndex()
    {
        $model = Pages::find()->where(['alias' => $this->getAliasFromURL()])->one();

        if (!is_object($model)) {
            throw new HttpException(404);
        }

        return $this->render('index.twig', [
            'model' => $model,
        ]);
    }

    public function getAliasFromURL()
    {
        $url = trim(Yii::$app->request->url, '/');

        if (empty($url))
            return 'about-us';

        if ($url == 'index.php')
            return 'about-us';

        return $url;
    }
}
