<?php

namespace app\controllers;

use app\components\CustomController;
use app\components\customEvents\_subscriber\Subscribers;
use app\components\Entity;
use app\components\MailChimpBehaviors;
use app\components\mailer\CustomMailer;
use app\models\forms\ChangePasswordForm;
use app\models\forms\LoginForm;
use app\models\forms\ProfileForm;
use app\models\forms\SignUp;
use app\models\user\User;
use app\models\user\UserCustomField;
use app\models\user\UserIdentity;
use app\models\user\UserProfile;
use app\models\user\UserSettings;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends CustomController
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }


    public function actionLogin()
    {

        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $serviceName = Yii::$app->getRequest()->getQueryParam('service');
        if (isset($serviceName)) {
            /** @var $eauth \nodge\eauth\ServiceBase */
            $eauth = Yii::$app->get('eauth')->getIdentity($serviceName);
            $eauth->setRedirectUrl(Yii::$app->getUser()->getReturnUrl());
            $eauth->setCancelUrl(Yii::$app->getUrlManager()->createAbsoluteUrl('/'));

            try {
                if ($eauth->authenticate()) {
                    $identity = UserIdentity::findByEAuth($eauth);
                    $eauth_field = UserIdentity::getEauthField($serviceName);
                    if($model = UserIdentity::isEauthIsset($identity, $eauth_field, $eauth)){
                        ProfileForm::saveCustomFieldsFromEauth($model,$identity,$eauth_field);
                    } else {
                        $model = UserIdentity::insertEauth($identity, $eauth_field);
                    }
                    Yii::$app->getUser()->login($model);
                    // special redirect with closing popup window
                    $eauth->redirect();
                } else {
                    // close popup window and redirect to cancelUrl
                    $eauth->cancel();
                }
            }
            catch (\nodge\eauth\ErrorException $e) {
                // save error to show it later
                Yii::$app->getSession()->setFlash('error', 'EAuthException: '.$e->getMessage());
                $eauth->redirect();
            }
        }


        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post())) {

            $user = User::findByUsernameOrEmail($model->username);

            if (is_object($user)) {

                if ($user->deleted === 1) {
                    Yii::$app->getSession()->setFlash('warning', 'Incorrect username or password.');
                    return $this->redirect(Yii::$app->request->referrer);
                }

                if ($user->status === User::STATUS_BANNED) {
                    Yii::$app->getSession()->setFlash('warning', 'Sorry you have blocked');
                    return $this->redirect(Yii::$app->request->referrer);
                }
            }

            if ($model->login())
                return $this->redirect(Yii::$app->request->referrer);
            else {
                if (is_array($model->getErrors()) && isset($model->getErrors()['password']) && isset($model->getErrors()['password'][0]))
                    Yii::$app->getSession()->setFlash('warning', $model->getErrors()['password'][0]);
            }
        }

        return $this->redirect(Yii::$app->request->referrer);

    }


    /**
     * @return Response
     */

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect(Yii::$app->request->referrer);
    }


    /**
     * @param $id
     * @return Response
     * @throws HttpException
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if(!$model){
            throw new NotFoundHttpException;
        }

        if (is_object(Yii::$app->user) && is_object(Yii::$app->user->identity)) {
            if ((int)$id === Yii::$app->user->identity->getId() || Yii::$app->user->identity->isAdmin()) {

                if ($model->delete()) {

                    if (!Yii::$app->user->identity->isAdmin())
                        Yii::$app->user->logout();

                    Yii::$app->session->setFlash('success',
                        Yii::t('app', 'Your account successfully deleted'));

                }
                return $this->redirect('/');
            }
        }

        Yii::$app->session->setFlash('warning',
            Yii::t('app', "You can't delete not your account"));

        if (Yii::$app->request->referrer)
            return $this->redirect(Yii::$app->request->referrer);
        return $this->redirect($model->getViewUrl());

    }


    /**
     * @param null $id
     * @param bool|true $caching
     * @return null|\yii\web\IdentityInterface|static
     * @throws NotFoundHttpException
     */
    public function findModel($id = null, $caching = true)
    {
        if ($id === null) return Yii::$app->user->identity;
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSignUp()
    {
        $model = new SignUp();
        if (Yii::$app->request->post()) {
            $model->scenario = User::SCENARIO_SIGN_UP;
            if ($model->load(Yii::$app->request->post())) {
                $model->channel = 'PTMC user';
                $model->registration();
                MailChimpBehaviors::run($model, MailChimpBehaviors::CHANNEL_PTMC);
            }else{
                Yii::$app->session->setFlash('danger', 'Registration Error');
            }
        }else{
            Yii::$app->session->setFlash('danger', 'Registration Error');
        }

        return !empty(Yii::$app->request->referrer) ? $this->redirect(Yii::$app->request->referrer) : $this->goHome();
    }

    public function actionConfirm()
    {
        $this->view->registerJs('<script>
                                      (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){
                                      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                                      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                                      })(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');

                                      ga(\'create\', \'UA-44579577-1\', \'auto\');
                                      ga(\'send\', \'pageview\');

                                </script>');
        // die;

        if (Yii::$app->request->get('key')) {
            if (User::findByAuthToken(Yii::$app->request->get('key'))) {
                $model = UserIdentity::findByAuthToken(Yii::$app->request->get('key'));
                $password = Yii::$app->security->generateRandomString(7);
                $model->status = User::STATUS_ACTIVE;
                $model->password_hash = Yii::$app->security->generatePasswordHash($password);
                $model->scenario = User::SCENARIO_CONFIRM;
                if ($model->save()) {
                    if (CustomMailer::send($model->email, 'sign-up-confirm.twig', ['email' => $model->email, 'password' => $password])) {
                        Yii::$app->session->setFlash('success', Yii::t('app', 'You have successfully registered! In the e-mail sent to the registration data.'));
                        $login = new LoginForm();
                        $login->load(['LoginForm' => ['username' => $model->email, 'password' => $password]]);
                        //$this->view->registerJs('<script>alert(1)</script>>');
                        $login->login();
                    }
                } else
                    Yii::$app->session->setFlash('danger', Yii::t('app', 'Registration Error'));
            }
        }

        return !empty(Yii::$app->request->referrer) ? $this->redirect(Yii::$app->request->referrer) : $this->goHome();
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */

    public function actionChangePassword($id = null) {

        if($id === null) $id = Yii::$app->user->getId();
        $user = UserIdentity::findOne($id);

        if(!$user)
            throw new NotFoundHttpException('The requested page does not exist.');

        $model = new ChangePasswordForm();
        $model->setUser($user);
        $model->scenario = ChangePasswordForm::EDIT_PROFILE;


        if(\Yii::$app->request->isAjax && $model->load(\Yii::$app->request->post())){
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if($model->load(\Yii::$app->request->post()) && $model->validate()){
            $user->setPassword($model->new_password);
            $user->save(false);
            \Yii::$app->session->setFlash('success',  'Password has been changed.');
        }else {
            static::setFlashWithErrors($user->getErrors());
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     * SignUp
     */

    public function actionRecoveryPassword()
    {
        if(isset($_POST['RecoveryPassword']['email'])) {
            $email = $_POST['RecoveryPassword']['email'];
            $model = User::findByUsernameOrEmail($email);

            if (!is_object($model) || !isset($model) || (is_object($model) && $model->deleted === Entity::DELETED)) {
                Yii::$app->session->setFlash('warning', Yii::t('app', 'This e-mail address was not found.'));

                return $this->goHome();
            }else{
                $model->auth_key = md5(microtime() . rand());
                $model->scenario = User::SCENARIO_RECOVERY_PASSWORD;

                if ($model->save()) {
                    $model->sendConfirmRecoveryPasswordMail($model->email,  $model->auth_key);
                    Yii::$app->session->setFlash('success', Yii::t('app', 'We have sent instructions to reset your password on e-mail ' . $model->email));
                }else{
                    Yii::$app->session->setFlash('danger', 'This e-mail address already exists.');
                }
            }
        }
        return $this->goHome();
    }


    /**
     * @return string|Response
     * @throws HttpException
     */
    public function actionEditProfile()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        if((Yii::$app->user->getId()) > 0){
            $user = UserIdentity::findIdentity(Yii::$app->user->getId());
            $user->scenario = UserIdentity::SCENARIO_EDIT_PROFILE;
            $profile = UserProfile::findOne($user->id);

            if (!is_object($profile)) {
                $profile = new UserProfile();
                $profile->user_id = $user->id;
            }
        }

        if (!isset($user) && !isset($profile)) {
            throw new HttpException(404);
        }

        $this->breadcrumbs[] = [
            'label' => 'Profile',
            'url' => $user->getViewurl(),
            'active' => 'active',
        ];

        $profile_form = new ProfileForm();

        if (Yii::$app->request->post()) {
            if ($user->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post()) &&  $profile_form->load(Yii::$app->request->post())){
                $profile_form->saveCustomFields();
                $user->fullname = Yii::$app->customFunctions->cleanText($profile->last_name . ' ' . $profile->first_name);

                if($user->save() && $profile->save()) {
                    return $this->redirect($user->getViewurl());
                }
            }
        }

        $profile_form->social_profile = UserCustomField::getDataByAttrName('social_profile');
        $profile_form->about_myself = UserCustomField::getDataByAttrName('about_myself');
        $profile_form->website = UserCustomField::getDataByAttrName('website');
        $profile_form->country = UserCustomField::getDataByAttrName('country');
        $profile_form->born_at = UserCustomField::getDataByAttrName('born_at');

        return $this->render('@views/user/edit.twig', [
            'user' => $user,
            'profile' => $profile,
            'profile_form' => $profile_form,
            'change_password_form' => new ChangePasswordForm()
        ]);
    }

    public function actionProfile($user_id)
    {
        $id = str_replace('id', '', $user_id);
        # it is not for yourself, for someone who want to see your profile
        $user = UserIdentity::findIdentity((int)$id);

        if ($user instanceof UserIdentity) {
            $profile = UserProfile::find()->where(['user_id' => $user->id])->one(); //$user->profile;
        } else {
            throw new HttpException(404);
        }

        return $this->render('@views/user/profile.twig', [
            'user'    => $user,
            'profile' => $profile,
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionSettings($id)
    {
        $model = UserSettings::findOne(['user_id' => $id]);

        if(!is_object($model))
            $this->setDefaultSettings();

        if (Yii::$app->request->post()) {
            $data = [];
            foreach(Yii::$app->request->post()['UserSettings'] as $field => $value){
                $data[$field] = $value ? true : false;
            }

            $model->data = json_encode($data);

            if ($model->save())
                Yii::$app->session->setFlash('success', 'Data changed');
            return $this->redirect('');
        }

        return $this->render('@views/user/settings.twig', [
            'model' => $model,
        ]);
    }

    /**
     * @return \yii\web\Response
     */
    public function setDefaultSettings()
    {
        $settings = [
            Subscribers::SUBSCRIBE_WHEN_OWN_TOPIC_COMMENTED => true,
            Subscribers::SUBSCRIBE_WHEN_TOPIC_COMMENTED => true,
            Subscribers::SUBSCRIBE_WHEN_ARTICLE_COMMENTED => true,
            Subscribers::SUBSCRIBE_WHEN_ANSWERED_OWN_COMMENT => true,
            Subscribers::SUBSCRIBE_WHEN_CREATED_ARTICLE_OR_TOPIC => true,
        ];

        $test = new UserSettings();
        $data = [];

        $data['UserSettings']['user_id'] = Yii::$app->user->identity->id;
        $data['UserSettings']['data'] = json_encode($settings);

        if($test->load($data) && $test->save())
            return $this->redirect('');
    }
}
