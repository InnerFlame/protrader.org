<?php
/**
 * Created by PhpStorm.
 * @author: Igor Ilyev
 * @date: 11.09.15 17:17
 * @copyright Copyright (c) 2015 PFSOFT LLC
 *
 * Class I18nController
 * @package app\controllers
 */

namespace app\controllers;

use yii;
use app\components\CustomController;
use app\models\rule\Rule;
use app\models\Constants;
use app\models\rule\RuleAlias;
use app\models\user\UserRules;


class I18nController extends CustomController
{
    /**
     * @return yii\web\Response
     */
    public function actionRu()
    {
        \Yii::$app->session->set('lang', 'ru-RU');

        $url = Yii::$app->request->referrer ? Yii::$app->request->referrer : '/';
        return $this->redirect($url);
    }

    /**
     * @return yii\web\Response
     */
    public function actionEn()
    {
        \Yii::$app->session->set('lang', 'en-US');

        $url = Yii::$app->request->referrer ? Yii::$app->request->referrer : '/';
        return $this->redirect($url);
    }
} 