<?php

namespace app\controllers;

use Yii;
use app\components\CustomController;
use app\models\video\Playlist;
use app\models\video\VdCategory;
use app\models\video\Video;
use app\models\community\Likes;
use app\models\community\Counts;
use app\models\Constants;

class VideoLessonsController extends CustomController
{

    /**
     * Return error twig array
     *
     * @return array
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'app\components\FlatErrorAction',
                'view' => 'error.twig'
            ],
        ];
    }

    public function init()
    {
        $this->breadcrumbs[] = [
            'label' => 'Video Lessons',
            'url' => '/video-lessons/',
            'active' => 'active',
        ];
        return parent::init();
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index.twig', [
            'categories' => VdCategory::find()->orderBy('weight DESC')->all(),
            'fresh'      => Video::find()->fresh()->one(),
            'playlists' => Playlist::find()->orderBy('weight DESC')->all(),
            'popular'    => Video::find()->popular()->all(),
        ]);
    }

    /**
     * @param $alias
     * @return string
     */
    public function actionVideo($alias)
    {
        $video = Video::find()->where(['=', 'alias', $alias])->one();

        Yii::$app->counter->setCount($video);

        $this->breadcrumbs[] = [
            'label' => $video->title,
            'url' => '/video-lessons/video/' . $video->alias,
            'active' => 'active',
        ];

        return $this->render('video.twig', [
            'video' => $video
        ]);
    }

    /**
     * @param $alias
     * @return string
     */
    public function actionCategory($alias)
    {
        $video = Video::find()->joinWith(['category'])->where(['=', 'vd_category.alias', $alias]);

        if(is_object($video->one()))
            $this->breadcrumbs[] = [
                'label' => $video->one()->category->title,
                'url' => '/video-lessons/category/' . $video->one()->category->alias,
                'active' => 'active',
            ];

        return $this->render('category.twig', [
            'dataProvider' => $this->getDataProvider($video, Constants::SEARCH_COUNT_VIDEO),
        ]);
    }

    public function actionSearch()
    {
        $models = Yii::$app->search->getCollection('title', Yii::$app->request->get('search'));

        return $this->render('search.twig',[
            'dataProvider' => $this->getDataProvider($models, Constants::SEARCH_COUNT_VIDEO),
        ]);
    }

}
