<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\HttpException;

class AjaxController extends Controller
{
    const RESPONSE_OK = 'OK';
    const RESPONSE_ERROR = 'ERROR ';

    public $layout = false;

    public function init()
    {
        parent::init();
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    }
    use \app\modules\forum\models\traits\CommentsTrait;
    use \app\components\customComments\models\traits\CustomCommentsTrait;

    public function actionIndex($method)
    {
        if (method_exists($this, $method)) {
            $result  = $this->$method();
            return [
                'status' => self::RESPONSE_OK,
                'data' => $result
            ];
        }else{
            throw new HttpException(404);
        }
    }

}