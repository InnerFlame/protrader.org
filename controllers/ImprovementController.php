<?php

namespace app\controllers;

use app\components\sypexGeo\GeoHelper;
use app\models\FilterHelper;
use Yii;
use app\components\CustomController;
use app\modules\brokers\models\Improvement;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use app\models\community\Likes;
use app\models\community\Counts;
use app\models\Constants;
use app\components\mailer\CustomMailer;

class ImprovementController extends CustomController
{
    # ImprovementIndex, ImprovementAdd, ImprovementEdit, ImprovementDelete
    use cabinet\forum\ImprovementTrait;



    public $indexUrl =  '/improvement/';

    /**
     * Return error twig array
     *
     * @return array
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'app\components\FlatErrorAction',
                'view' => 'error.twig'
            ],
        ];
    }

    public function init()
    {
        $this->breadcrumbs[] = [
            'label' => 'Improvement',
            'url' => '/improvement/',
            'active' => 'active',
        ];
        return parent::init();
    }

    /**
     * Redirect to company/index
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $improvement = Improvement::find();


        Yii::$app->sort->run($improvement);

        $models = $improvement->orderBy('weight DESC')->all();
        $category_array = ArrayHelper::map($models,'status','status');


        if(Yii::$app->request->post()){
            FilterHelper::filterByCategory($models,'status');
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $models,
            'pagination' => [
                'pageSize' => Constants::SEARCH_COUNT_IMPROVEMENT,
            ],
        ]);

        return $this->render('index.twig',[
            'improvs'      => $models,
            'dataProvider' => $dataProvider,
            'model'        => new Improvement(),
            'filterPOST' => $_POST,
            'category_array' => $category_array,
        ]);
    }


    /**
     * @param $id
     * @return string
     */
    public function actionView($id)
    {
        $improvement = Improvement::findOne($id);

        Yii::$app->counter->setCount($improvement);

        $this->breadcrumbs[] = [
            'label' => $improvement->title,
            'url' => '/improvement/view/'  . $improvement->id,
            'active' => 'active',
        ];

        return $this->render('view.twig',[
            'improvement' => $improvement,
        ]);
    }

    /**
     * window Сообшить об ошибке
     */

    public function actionWarning()
    {

        if(isset($_POST['Warning']['error']) && isset($_POST['Warning']['description'])){

            $form_data = [
                'error' => $_POST['Warning']['error'],
                'description' => $_POST['Warning']['description'],
            ];

            //$params = array_merge($form_data, GeoHelper::getGeoDataToMail());

            if (CustomMailer::send(Yii::$app->params['adminEmail'], 'improvement-warning.twig', $form_data)) {

                Yii::$app->session->setFlash('success',
                    Yii::t('app', 'In the e-mail sent to an administrator'));
            }
            else{

                Yii::$app->session->setFlash('danger',
                    Yii::t('app', 'Error.'));

            }

            return $this->redirect(Yii::$app->request->referrer);

        }
        Yii::$app->session->setFlash('warning',Yii::t('app', 'Error.'));
        return $this->goHome();
    }



    public function actionSearch()
    {
        $video = Yii::$app->search->getCollection('title', Yii::$app->request->get('search'));

        return $this->render('search.twig',[
            'dataProvider' => $this->getDataProvider($video, Constants::SEARCH_COUNT_IMPROVEMENT),
        ]);
    }
}
