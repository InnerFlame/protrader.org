<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2016
 */

namespace app\controllers;


use app\models\forms\LoginForm;
use app\models\forms\ProfileForm;
use app\models\user\UserIdentity;
use yii\web\Controller;

class ServiceController extends Controller
{
    public $enableCsrfValidation = false;

    /**
     * @return string
     */
    public function actionLogin()
    {
        $model = new LoginForm();

        if($serviceName = \Yii::$app->getRequest()->getQueryParam('service')) {
            $this->loginEauth($serviceName);
        }
        if(\Yii::$app->request->isPost){
            $model->load(\Yii::$app->request->post()) && $model->login();
        }
        return $this->renderAjax('login.php', ['model' => $model]);
    }

    /**
     * Осуществляет авторизацию через сервисы
     * @param $serviceName
     * @throws \app\models\user\ErrorException
     * @throws \yii\base\InvalidConfigException
     */
    private function loginEauth($serviceName)
    {
        /** @var $eauth \nodge\eauth\ServiceBase */
        $eauth = \Yii::$app->get('eauth')->getIdentity($serviceName);
        $eauth->setRedirectUrl('/service/login');
        $eauth->setCancelUrl(\Yii::$app->getUrlManager()->createAbsoluteUrl('/'));
        try {
            if ($eauth->authenticate()) {
                $identity = UserIdentity::findByEAuth($eauth);
                $eauth_field = UserIdentity::getEauthField($serviceName);
                if($model = UserIdentity::isEauthIsset($identity, $eauth_field, $eauth)){
                    ProfileForm::saveCustomFieldsFromEauth($model,$identity,$eauth_field);
                } else {
                    $model = UserIdentity::insertEauth($identity, $eauth_field);
                }
                \Yii::$app->getUser()->login($model);
                // special redirect with closing popup window
                $eauth->redirect();
            } else {
                // close popup window and redirect to cancelUrl
                $eauth->cancel();
            }
        }
        catch (\nodge\eauth\ErrorException $e) {
            // save error to show it later
            \Yii::$app->getSession()->setFlash('error', 'EAuthException: '.$e->getMessage());
            $eauth->redirect();
        }
    }
}