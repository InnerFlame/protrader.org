<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2016
 */

namespace app\events\user;


use yii\base\Event;

class RoleChangedEvent extends Event
{
    public $oldRole;

    public $newRole;
}