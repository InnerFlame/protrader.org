<?php
/**
 * Created by PhpStorm.
 * @author: Roman Budnitsky
 * @date: 23.10.14 16:05
 * @copyright Copyright (c) 2014 PFSOFT LLC
 *
 * Class RbacController
 * @package app\commands
 */


namespace app\commands;

use app\components\rbac\rules\ClientGroupAccessRule;
use app\components\rbac\rules\DepartmentAccessRule;
use app\components\rbac\rules\DepartmentRule;
use app\components\rbac\rules\ProviderCompanyRule;
use app\components\rbac\rules\UserRoleRule;
use app\components\rbac\rules\UserRule;
use yii;
use yii\console\Controller;
use app\components\rbac\rules\ClientCompanyRule;
use app\components\rbac\rules\CompanyRule;

/**
 * This command echoes the first argument that you have entered.
 */
class RbacController extends Controller {

    /**
     * This command echoes what you have entered as the message.
     */
    public function actionIndex()
    {
        echo 'It\'s RBAC utility. Please run "rbac/init" if you want reinitialisation RBAC system.' . "\n";
    }

    /**
     * Init Roles
     */
    public function actionInit()
    {
        $auth = Yii::$app->getAuthManager();
        $auth->removeAll();

        //region ROLES
        $root = $auth->createRole('root');
        $auth->add($root);
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $lead = $auth->createRole('lead');
        $auth->add($lead);
        $user = $auth->createRole('user');
        $auth->add($user);

        $auth->addChild($root, $admin);
        $auth->addChild($admin, $lead);
        $auth->addChild($lead, $user);
        //endregion


    }
} 