<?php

namespace app\commands;

use app\modules\articles\models\Articles;
use app\models\community\Comments;
use app\models\mail\Queue;
use Yii;
use yii\console\Controller;
use app\components\customEvents\_subscriber\Subscribers;
use app\components\mailer\CustomMailer;

class SendMessageController extends Controller
{
    public function actionIndex()
    {
        $queues = Queue::find()->where('operated_at is NULL')->all();

        foreach($queues as $queue) {

            $eventModel = $queue->getQueueModel(); # created model

            $subscribers = [];

            Subscribers::getSubscribers($eventModel, $subscribers);

            foreach($subscribers as $subscriber) {

                echo "<p>"

                    . $subscriber['user']->email
                    . ' message: ' . $subscriber['message']

                    ."</p>";
//                CustomMailer::send($subscriber->email, 'template.twig',[
//                    'message' => 'New comment created',
//                ]);

            }

        }

    }

}