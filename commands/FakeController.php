<?php

namespace app\commands;

use app\models\video\VdCategory;
use app\models\video\Video;
use app\modules\articles\models\ArtclCategory;
use app\modules\articles\models\Articles;
use app\modules\forum\models\FrmCategory;
use app\modules\forum\models\FrmTopic;
use app\modules\brokers\models\Improvement;
use app\modules\brokers\models\ImprovementsCategory;
use app\modules\news\models\News;
use app\modules\news\models\NewsCategory;
use Yii;
use yii\console\Controller;

class FakeController extends Controller
{

    public function actionIndex()
    {
        $faker = \Faker\Factory::create();


        # ArtclCategory
        for ($i = 0; $i < 5; $i++) {
            $vd_category = new ArtclCategory();
            $vd_category->title = $faker->word;
            $vd_category->alias = $vd_category->title;
            $vd_category->weight = $i;
            $vd_category->published = date('U');
            $vd_category->created_at = date('U');
            $vd_category->updated_at = date('U');
            $vd_category->save(false);

            # Articles
            for ($j = 0; $j < 25; $j++) {
                $video = new Articles();
                $video->category_id = $vd_category->id;
                $video->title = $faker->word;
                $video->content = $faker->text;
                $video->alias = $video->title;
                $video->published = date('U');
                $video->created_at = date('U');
                $video->updated_at = date('U');
                $video->save(false);
            }
            echo 'ArtclCategory - done!';

        }
        echo 'Articles - done!';


        for ($j = 0; $j < 5; $j++) {
            $category = new FrmCategory();
            $category->title = $faker->word;
            $category->description = $faker->word;
            $category->published = date('U');
            $category->created_at = date('U');
            $category->updated_at = date('U');

            $parent = FrmCategory::findOne(1);

            $category->appendTo($parent);

            echo 'FrmCategory - ';

            $faker = \Faker\Factory::create();
            # Faq
            if($category->id > 0){
                for ($j = 0; $j < 5 ; $j++) {
                    $faq = new FrmTopic();
                    $faq->category_id = $category->id;
                    $faq->title = $faker->word;
                    $faq->content = $faker->text;
                    $faq->published = date('U');
                    $faq->created_at = date('U');
                    $faq->updated_at = date('U');
                    $faq->save(false);
                }
                echo 'FrmTopic -  !';
            }

        }


        # VdCategory
        for ($i = 0; $i < 5; $i++) {
            $vd_category = new VdCategory();
            $vd_category->title = $faker->word;
            $vd_category->alias = $vd_category->title;
            $vd_category->weight = $i;
            $vd_category->published = date('U');
            $vd_category->created_at = date('U');
            $vd_category->updated_at = date('U');
            $vd_category->save(false);

            # Video
            for ($j = 0; $j < 25; $j++) {
                $video = new Video();
                $video->category_id = $vd_category->id;
                $video->title = $faker->word;
                $video->alias = $video->title;
                if ($j > 15)
                    $video->link = 'https://www.youtube.com/watch?v=w8KQmps-Sog';
                else
                    $video->link = 'https://vimeo.com/82139985';
                $video->weight = $i;
                $video->published = date('U');
                $video->created_at = date('U');
                $video->updated_at = date('U');
                $video->save(false);
            }
            echo 'Video - done!';

        }
        echo 'VdCategory - done!';


        $faker = \Faker\Factory::create();

        # VdCategory
        for ($i = 0; $i < 5; $i++) {
            $vd_category = new NewsCategory();
            $vd_category->title = $faker->word;
            $vd_category->alias = $vd_category->title;
            $vd_category->weight = $i;
            $vd_category->published = date('U');
            $vd_category->created_at = date('U');
            $vd_category->updated_at = date('U');
            $vd_category->save(false);

            # Video
            for ($j = 0; $j < 25; $j++) {
                $video = new News();
                $video->category_id = $vd_category->id;
                $video->title = $faker->word;
                $video->content = $faker->text;
                $video->alias = $video->title;
                $video->published = date('U');
                $video->created_at = date('U');
                $video->updated_at = date('U');
                $video->save(false);
            }
            echo 'NewsCategory - done!';

        }


        echo 'News - done!';


        $faker = \Faker\Factory::create();



        # ImprovementCategory
        for ($i = 0; $i < 4; $i++) {
            $imp_category = new ImprovementsCategory();
            $imp_category->title = $faker->word;
            $imp_category->alias = $imp_category->title;
            $imp_category->weight = $i;
            $imp_category->published = date('U');
            $imp_category->created_at = date('U');
            $imp_category->updated_at = date('U');
            $imp_category->save(false);

            # Improvement
            for ($i = 0; $i < 30; $i++) {
                $mprovement = new Improvement();
                $mprovement->category_id = $imp_category->id;
                $mprovement->title = $faker->word;
                $mprovement->content = $faker->text;
                $mprovement->is_main = rand(0, 1);
                $mprovement->is_broker = rand(0, 1);
                $mprovement->status = rand(0, 2);
                $mprovement->created_at = date('U');
                $mprovement->save(false);
            }
            echo 'Improvement - done!';

        }
    }


}
