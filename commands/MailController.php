<?php
/**
 * User: Bilyk Pavel
 * Date: 14.02.2016
 */

namespace app\commands;


use yii\console\Controller;

class MailController extends Controller{

    public function actionSend(){
        \Yii::$app->mailqueue->process();
    }
}