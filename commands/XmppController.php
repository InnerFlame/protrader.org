<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2016
 */

namespace app\commands;


use app\models\user\XMPPUserRecord;
use app\components\OpenFireRestApi;
use yii\console\Controller;
use yii\console\Exception;

class XmppController extends Controller
{
    const XMPP_GROUP_NAME = 'protrader_org';
    /**
     * @var OpenFireRestApi
     */
    protected $api;

    public function actionAddUser($username, $password, $name, $email, $groupname)
    {
        try{
            $result = $this->getApi()->addUser($username, $password, $name, $email, $groupname);
            var_dump($result); exit;
        }catch (\Exception $ex){
            printf($ex->getMessage());

        }
    }
    public function actionGetUser($username)
    {
        $result = $this->getApi()->getUser($username);
        var_dump($result); exit;
    }
    public function actionSyncUsers()
    {
        if(!$this->createGroup()){
            throw new \Exception(\Yii::t('app', 'Create group {groupname} failure', ['groupname' => self::XMPP_GROUP_NAME]), 500);
        }
        foreach(XMPPUserRecord::find()->with('user')->each(20) as $model){
            try{
                $result = $this->getApi()->getUser($model->username);
            }catch (\Exception $ex){
                if($ex)
                $result['status'] = false;
                $result['message'] = $ex->getMessage();
            }
            try{
                if(isset($result['status']) && $result['status']){
                    $return = $this->getApi()->updateUser($model->username, $model->token, $model->user->fullname, $model->user->email);
                    if(!isset($return['status']) || !$result['status']){
                        throw new Exception(\Yii::t('app','Failed to update user: {username} in xmpp server', ['username' => $model->username]), 500);
                    }
                }else{
                    $return = $this->getApi()->addUser($model->username, $model->token, $model->user->fullname, $model->user->email, self::XMPP_GROUP_NAME);
                    if(!isset($return['status']) || !$result['status']){
                        throw new Exception(\Yii::t('app','Failed to add new user: {username} to xmpp server', ['username' => $model->username]), 500);
                    }
                }
            }catch (\Exception $ex){
                \Yii::error($ex->getMessage());
                continue;
            }
            printf("Synchronization user %s with XMPP server: %s\n", $model->username, $this->getApi()->host);
        }
    }

    /**
     * Удаляет из XMPP всех юзеров, которые связаны с protrader.org
     *
     */
    public function actionClear()
    {
        foreach(XMPPUserRecord::find()->with('user')->each(20) as $model){
            try{
                $result = $this->getApi()->getUser($model->username);
            }catch (\Exception $ex){
                if($ex)
                    $result['status'] = false;
                $result['message'] = $ex->getMessage();
            }
            try{
                if(isset($result['status']) && $result['status']){
                    $return = $this->getApi()->deleteUser($model->username);
                    if(!isset($return['status']) || !$result['status']){
                        throw new Exception(\Yii::t('app','Failed to delete user: {username} in xmpp server', ['username' => $model->username]), 500);
                    }
                }
            }catch (\Exception $ex){
                \Yii::error($ex->getMessage());
                continue;
            }
            printf("Deleted user %s with XMPP server: %s\n", $model->username, $this->getApi()->host);
        }
    }

    /**
     * @return OpenFireRestApi
     */
    protected function getApi()
    {
        if(!$this->api){
            $this->api = new OpenFireRestApi();

            $this->api->secret = '9251j5Bk32UT5b7S';

            $this->api->host = '138.201.48.228';
            $this->api->useSSL = false;

            $this->api->plugin = "/plugins/restapi/v1";
        }

        return $this->api;
    }

    private function createGroup()
    {
        try{
            $result = $this->getApi()->getGroup(self::XMPP_GROUP_NAME);
            return $result['status'];
        }catch (\Exception $ex){
            $result = $this->getApi()->createGroup(self::XMPP_GROUP_NAME);
            return $result['status'];
        }
    }
}