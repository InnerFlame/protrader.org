<?php
/**
 * User: Bilyk Pavel
 * Date: 11.03.2016
 *
 * This command controller for all aplication.
 */

namespace app\commands;


use yii\console\Controller;

class ApplicationController  extends Controller{

    public function actionCache($action)
    {
        if($action == 'clear')
        {
            \Yii::$app->cache->flush();
            echo 'The cache is cleared' . PHP_EOL;
        }
    }
}