(function(){
	var _id="cross";
	while(document.getElementById("timer"+_id))_id=_id+"0";
	document.write("<div id='timer"+_id+"'></div>");
	var _t=document.createElement("script");
	_t.src="http://megatimer.ru/timer/timer.min.js";

	function get_utc() {
		var stmps = [(new Date("2016/08/08 00:00:00").getTime() / 1000), (new Date("2016/09/01 16:00:00").getTime() / 1000), (new Date("2016/09/19 00:00:00").getTime() / 1000), (new Date("2016/10/01 00:00:00").getTime() / 1000), (new Date("2016/10/05 00:00:00").getTime() / 1000)];
		var curr = Math.floor(Date.now() / 1000);
		for (var i = 0; i < stmps.length; i++) {
			if (stmps[i] > curr) {
				return parseInt(stmps[i] + (-new Date().getTimezoneOffset()*60) + '000');
			}
		}
	}

	var _f=function(_k){
		var l = new MegaTimer(_id, {
			"view":[1,1,1,1],
			"type":{
				"currentType":"1",
				"params":{
					"usertime":true,
					"tz":"3",
					"utc": get_utc()
				}
			},
			"design":{
				"type":"text",
				"params":{
					"number-font-family":{
						"family":"League Gothic",
						"link":"<link href='http://webfonts.ru/import/leaguegothic.css' rel='stylesheet' type='text/css'>"
					},
					"number-font-size":"75",
					"number-font-color":"#FFF",
					"separator-margin":"0",
					"separator-on":false,
					"separator-text":":",
					"text-on":true,
					"text-font-family":{"family":"Open Sans","link":"<link href='http://fonts.googleapis.com/css?family=Open+Sans&subset=latin,cyrillic' rel='stylesheet' type='text/css'>"},
					"text-font-size":"20",
					"text-font-color":"#a9a9a9"
				}
			},
			"designId":1,
			"theme":"black",
			"width":459,
			"height":110
		});
		if(_k!=null)l.run();
	};
	_t.onload=_f;
	_t.onreadystatechange=function(){
		if(_t.readyState=="loaded")_f(1);
	};
	var _h=document.head||document.getElementsByTagName("head")[0];
	_h.appendChild(_t);
}).call(this);