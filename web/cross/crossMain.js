jQuery(document).ready(function($) {
	$('.openForm').click(function() {
		var id = $(this).attr('id');
		var target = $('.'+id).parents('.crossForm');
		$('html, body').animate({scrollTop: $(target).offset().top-50}, 500);
		$('.'+id).parents('.crossForm').addClass('open');
		$(this).remove();
	});
	$(window).scroll(function(){
	 	parallax();
	});
	function parallax(){
		var scrolled = $(window).scrollTop();
		if ($(window).width() > 960) {
			$('.crossBander').css('top', (scrolled*0.4)+'px');
			$('.crossBander .crossWrapper').css('top', -(scrolled*0.4)+'px');
		};
	}
	$('.crossListElem').click(function() {
		$('.crossListElem').each(function() {
			$(this).removeClass('open');		
			var id = $(this).attr('data-toggle');
			$('.'+id).removeClass('active');
		});
		$(this).toggleClass('open');		
		var id = $(this).attr('data-toggle');
		$('.'+id).addClass('active');
	});
	$('.crossListElem').hover(function() {
		if ($(this).hasClass('open')) {

		}
		else{
			var id = $(this).attr('data-toggle');
			$('.'+id).addClass('active');
		}
	}, function() {		
		if ($(this).hasClass('open')) {

		}
		else{
			var id = $(this).attr('data-toggle');
			$('.'+id).removeClass('active');
		}
	});

});