/**
 * Created by P.Bilik on 03.02.2016.
 */

var app = (function(){

    var container = $("<div id='comments-modal-container'></div>");

    var windowModal  = null;

    var form = null;

    $('body').append(container);

    var ACTION = {
        POST:{
            REPLY: '/forum/comment/reply',
            SAVE: '/forum/comment/save',
            EDIT: '/forum/comment/edit'
        },
        COMMENT:{
            REPLY: '/comments/reply',
            SAVE: '/comments/save',
            EDIT: '/comments/edit'
        }
    }

    function Post(){
        $('#forum-comment-form').on('beforeSubmit', function(){
            var form = $(this);
            _submit(ACTION.POST.SAVE, form.serialize(), '#forum-comment');
            form.find('textarea').redactor('code.set', '');
            return false;
        });

        this.edit = function(id){
            _request(ACTION.POST.EDIT, {
                id:id,
            },_validateComplete);
        }

        this.reply = function(id){

            _request(ACTION.POST.REPLY, {
                id: id,
                selected_text: _select(),
            }, _validateComplete);

        }

        /**
         * Form is validate.
         * @param e
         * @private
         */
        function _validateComplete(e){
            windowModal.modal('hide');
            var form = $(e.target).find('form');
            _submit(ACTION.POST.SAVE, form.serialize(), '#forum-comment');
            return false;
        }
    }

    function Comment(){

        var selectorContainer = "#list-comments";

        $('#custom-comment-form').on('beforeSubmit', function(){
            var form = $(this);
            _submit(ACTION.COMMENT.SAVE, form.serialize(), selectorContainer);
            form.find('textarea').redactor('code.set', '');
            return false;
        });

        this.add = function(msg, entity, entity_id){
            _load(ACTION.COMMENT.save);
        }

        this.edit = function(id){
            _request(ACTION.COMMENT.EDIT, {
                id:id,
            },_validateComplete);
        }

        this.reply = function(id){
            _request(ACTION.COMMENT.REPLY, {
                id:id,
            }, _validateComplete);
        }

        /**
         * Form is validate.
         * @param e
         * @private
         */
        function _validateComplete(e){
            windowModal.modal('hide');
            var form = $(e.target).find('form');
            _submit(ACTION.COMMENT.SAVE, form.serialize(), selectorContainer);
            return false;
        }

        function _submit(action, params, selector){
            $.ajax({
                type:"POST",
                url: action,
                data:params,
                success:function(data){
                    $(selector).html(data);
                    $(selector).trigger('update');
                },
            });
        }
    }

    function _request(action, params, callback){
        $.ajax({
            type:"POST",
            url: action,
            data:params,
            success:function(data){
                container.html(data);
                windowModal = container.find('#comment-modal-form'),
                    form = windowModal.find('form');

                windowModal.modal('show');
                $(windowModal).on('beforeSubmit', callback);
            },
        });
    }

    function _submit(action, params, selector){
        $.ajax({
            type:"POST",
            url: action,
            data:params,
            success:function(data){
                $(selector).html(data);
                $(selector).trigger('update');
            },
        });
    }

    function _select() {
        try {
            if (window.getSelection) {

                if (document.getSelection().getRangeAt(0))
                    return document.getSelection().getRangeAt(0).toString();
                return null;

            } else {
                if (document.getSelection().getRangeAt(0))
                    return document.getSelection().getRangeAt(0).toString();
                return null;
            }
        } catch (err) {

        }
    }


    return {
        comments: new Comment(),
        posts: new Post()
    };
}());