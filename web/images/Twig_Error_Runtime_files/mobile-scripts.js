jQuery(document).ready(function($) {
	google();
	facebook();
	linkedin();
	twitter();
    $('a.burger').click(function(){
       var burger = $(this).attr('data-target');
       $('.'+burger).addClass('open');
       $('body').addClass('MenuOpen');
    });
    $('.mobPanel a[href="#close-navbar-mobile"]').click(function() {
    	$('.mobPanel').removeClass('open');
    	$('body').removeClass('MenuOpen');
    });
});

function facebook() {
	$('.sc.facebook').click(function(e) {
		window.open('http://www.facebook.com/sharer.php?p[url]=http://protrader.org','sharer','toolbar=0,status=0,width=600,height=400');
		e.preventDefault();
	});
}
function twitter() {
  	$('.sc.twitter').click(function(e) {
		window.open('https://twitter.com/share?text=The new trading software from creators of Protrader&hashtags=protrader,ptmc&url=http://protrader.org&related=@protrader_com','sharer','toolbar=0,status=0,width=700,height=400');
		e.preventDefault();
	});
}

function linkedin() {
  	$('.sc.linkedin').click(function(e) {
		window.open('http://www.linkedin.com/shareArticle?mini=true&url=http://protrader.org&title=PTMC%20|%20Protrader%20multi-connect&summary=The%20new%20trading%20software%20from%20creators%20of%20Protrader&source=protrader.org','sharer','toolbar=0,status=0,width=700,height=400');
		e.preventDefault();
	});
}

function google() {
  	$('.sc.google').click(function(e) {
		window.open('https://plusone.google.com/_/+1/confirm?hl=ru&url=http://protrader.org','sharer','toolbar=0,status=0,width=550,height=400');
		e.preventDefault();
	});
}