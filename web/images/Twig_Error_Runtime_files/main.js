jQuery(document)
    .on('pjax:start', function() { jQuery('body').addClass('pjax-loading'); })
    .on('pjax:end',   function() { jQuery('body').removeClass('pjax-loading'); });

// jQuery(".featCatName").click(function() {
//         jQuery(this).parents('.featuresTable').toggleClass("collapsed");
//     });

	//.ready(function(){
	//	$('table').footable({
	//		breakpoints: {
	//			phone: 480,
     //           tablet: 1024
     //       }
     //   });
	//});


window.onload = function () {

    jQuery('div#light-box  img').each(function () {
        jQuery(this).wrap('<a data-popup="lightbox" href="' + this.src + '"></a>');
    });


    /* link_target_rel for inserting here in every link rel(nofollow) and target (blank)*/
    jQuery('div.link_target_rel a').each(function () {

        var a_href = jQuery(this).attr('href');

        //console.log(jQuery(this).attr('href').indexOf( 'protrader' ),jQuery(this).attr('href'),indexof.indexOf( 'protrader' ));

        if (a_href.indexOf('protrader') == -1) {

            if (jQuery(this).attr('target') != '_blank')
                jQuery(this).attr('target', '_blank');

            if (jQuery(this).attr('rel') != 'nofollow')
                jQuery(this).attr('rel', 'nofollow');

        }

    });


};
//Lightbox is off for image in imperavi redactor
$('body').on('click','a[data-popup=lightbox]', function(e){
    if($(e.target).parents('.redactor-box').length > 0){
        return false;
    }
});