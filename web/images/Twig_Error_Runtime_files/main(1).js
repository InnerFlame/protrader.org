jQuery(document).ready(function($) {
    $('.mobileButton').click(function() {
        $('.mobile-sidebar').toggleClass('open');
    });
    
    $('.logo:last-child').addClass('last');
    $('.logo:first-child').addClass('first');
    function logosAnim (){
		var act = $('.logo.active');
		if (act.hasClass('last')) {
			$('.logo').removeClass('prev');
			act.removeClass('active').addClass('prev');
			$('.logo:first-child').addClass('active');
		}
		else{
			$('.logo').removeClass('prev');
			act.removeClass('active').addClass('prev');
			act.next('.logo').addClass('active');
		}
    }
	setInterval(function() {
		logosAnim ();
	}, 3000);

    $('.panel-article table').each(function() {
        $(this).wrap('<div class="table-responsive"></div>');
    });
    
	$('.post, .commentItem').each(function() {
		var postHeight = $(this).height();

		if (postHeight>400) {
			$(this).addClass('bHeight');
			$(this).addClass('hid_com');
		};
	});

	$('#forum-comment').on('update',function() {
	  $('.post').each(function() {
			var postHeight = $(this).height();
			if (postHeight>400) {
				$(this).addClass('bHeight');
				$(this).addClass('hid_com');
			};
		});
	});

	$('.forum-comment, .commentItem').on("click", ".showPostLink", function() {
     	var target = $(this).attr('href');
		$(this).parents('.post, .commentItem').toggleClass('hid_com');
     	$('html, body').animate({scrollTop: $(target).offset().top-50}, 500);
     	return false;
	});

	$('.forum-comment, .commentItem').on("click", ".hidePostLink",function() {
     	var target = $(this).attr('href');
		$(this).parents('.post, .commentItem').toggleClass('hid_com');
     	$('html, body').animate({scrollTop: $(target).offset().top-50}, 500);
     	return false;
	});

	$('#top').click( function(){
      $('body, html').animate({
        scrollTop: 0
      }, 300);
    });

    $('.notifications .body').mCustomScrollbar({
    	theme:"minimal-dark",
    	scrollInertia:100
    });

});