<?php
/**
 * Пример точки входа в приложение.
 * Основная точка входа в приложение игнорируется
 * что бы можно было менять константы, расположение вендоров и так далее.
 * @see https://github.com/yiisoft/yii2/issues/2997
 */
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'prod');

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');
require(__DIR__ . '/../components/Application.php');

$config = require(__DIR__ . '/../config/web.php');

(new \app\components\Application($config))->run();
