jQuery(document).ready(function($) {
	headLogin();
	notify();
	brtab ();
	slideCont();
	slideHeader();
	
    google();
    facebook();
    linkedin();
    twitter();


    $('.crossTabs li a').click(function(){
        if ($(this).hasClass('open')) {

        }
        else{
        	$('.crossTabs li a').removeClass('open');
        	$('.crossBody').removeClass('open');
            $(this).addClass('open');
            var id = $(this).attr('href');
            $(id).addClass('open');
        }
        return false;
    });  

	$('.styled').uniform();
    $('.mobileButton').click(function() {
        $('.mobile-sidebar').toggleClass('open');
    });
    
    $('.logo:last-child').addClass('last');
    $('.logo:first-child').addClass('first');
    $(window).scroll(function() {
    	slideCont();
    	slideHeader();
    });

    $('body').click(function(event) {
    	$('.dropFilter').removeClass('active');
    });

    $('body').on('click','.dropFilter', function(event) {
    	event.stopPropagation();
    	$(this).toggleClass('active');
    });

    $('a.burger').click(function(){
       var burger = $(this).attr('data-target');
       $('.'+burger).addClass('open');
       $('body').addClass('MenuOpen');
    });
    $('.mobPanel a[href="#close-navbar-mobile"]').click(function() {
    	$('.mobPanel').removeClass('open');
    	$('body').removeClass('MenuOpen');
    });

	function facebook() {
		$('.sc.facebook').click(function(e) {
			window.open('http://www.facebook.com/sharer.php?p[url]=http://protrader.org','sharer','toolbar=0,status=0,width=600,height=400');
			e.preventDefault();
		});
	}
	function twitter() {
	  	$('.sc.twitter').click(function(e) {
			window.open('https://twitter.com/share?text=The new trading software from creators of Protrader&hashtags=protrader,ptmc&url=http://protrader.org&related=@protrader_com','sharer','toolbar=0,status=0,width=700,height=400');
			e.preventDefault();
		});
	}

	function linkedin() {
	  	$('.sc.linkedin').click(function(e) {
			window.open('http://www.linkedin.com/shareArticle?mini=true&url=http://protrader.org&title=PTMC%20|%20Protrader%20multi-connect&summary=The%20new%20trading%20software%20from%20creators%20of%20Protrader&source=protrader.org','sharer','toolbar=0,status=0,width=700,height=400');
			e.preventDefault();
		});
	}

	function google() {
	  	$('.sc.google').click(function(e) {
			window.open('https://plusone.google.com/_/+1/confirm?hl=ru&url=http://protrader.org','sharer','toolbar=0,status=0,width=550,height=400');
			e.preventDefault();
		});
	}

    function slideCont() {
    	if ($('body').hasClass('article-single')) {
	    	var scr = $(window).scrollTop();
	    	var part = $('.panel-article').height();
	    	if (scr > 250) {
	    		$('.slideCont').addClass('active');
	    	}else{
	    		$('.slideCont').removeClass('active');
	    	}
	    	if (scr > part-60) {
	    		$('.slideCont').addClass('stop').css('top', part-160-195);
	    	}
	    	else{
	    		$('.slideCont').removeClass('stop').attr('style', '');
	    	}
    	};

    }

    function slideHeader(){
    	var scr = $(window).scrollTop();
    	if (scr > 1) {
    		$('.ptmc-navbar').addClass('active');
    	}
    	else{
    		$('.ptmc-navbar').removeClass('active');
    	}
    }

    function logosAnim (){
		var act = $('.logo.active');
		if (act.hasClass('last')) {
			$('.logo').removeClass('prev');
			act.removeClass('active').addClass('prev');
			$('.logo:first-child').addClass('active');
		}
		else{
			$('.logo').removeClass('prev');
			act.removeClass('active').addClass('prev');
			act.next('.logo').addClass('active');
		}
    }
    function brtab () {
    	$('.brokers-list-upcoming .tabs li a').click(function() {
    		$('.brokers-list-upcoming .tabs li').removeClass('active');
    		$('.brokers-list-upcoming .tabCont').removeClass('active');
    		$(this).parent().addClass('active');
    	});
    }
    function headLogin(){
		$('body').click(function(e) {
			$('.dropdown-loginForm').removeClass('open');
			$('.login-btn').removeClass('active')
		});

		$('.dropdown-loginForm').click(function(e) {	
			e.stopPropagation();		
		});

		$('a[data-toggle="tab"]').click(function() {
			var tab = $(this).attr('href');
			var container = $(tab).parent('.tab-content')[0];
			var li = $(this).parent('li')[0];
			$(li).siblings().removeClass('active');
			$(this).parent().addClass('active');
			$(container).find('.tab-pane.active').removeClass('active');
			$(tab).addClass('active');
		});

		$('.login-btn').click(function(e) {		
			e.stopPropagation();
			$(this).toggleClass('active').next('.dropdown-loginForm').toggleClass('open');
		});
    }

    function notify() {
    	$('body').click(function(e) {
			$('.notificationBody').removeClass('open');
			$('.login-btn').removeClass('active')
		});
		$('.notificationBody').click(function(event) {
			event.stopPropagation();
		});
    	$('.bellButton').click(function(e) {
    		e.stopPropagation();
    		$('.notificationBody').toggleClass('open');
    	});
    }
	$('.popularCharts .slider').each(function() {
		$(this).owlCarousel({
            nav:true,
		    responsiveClass:true,
		    responsive:{
	        0:{
	            items:1,
	        },
	        100:{
	            items:1,	        	
	        },
	        200:{
	            items:1,	        	
	        },
	        300:{
	            items:2,	        	
	        },
	        400:{
	            items:2,	        	
	        },
	        500:{
	            items:3,	        	
	        },
	        600:{
	            items:3,
	        },
	        800:{
				items:3,
	        },
	        1000:{
	        	items:4,
	        },
	        1200:{
	            items:5,
	        }
	    }});
	});

	$('#watchlistTable').mCustomScrollbar({
    	theme:"minimal-dark",
    	scrollInertia:100
    });

	var numT = 1;
	$('#watchlistTable tbody tr:first-child td').each(function() {
		var wid = $(this).innerWidth();
		$(this).css('width', wid).attr('wid', numT).attr('data-width', wid);
		numT++;
	});

	var numH = 1;
	$('.tableHead thead tr th').each(function() {
		var wid = $(this).innerWidth();
		$(this).attr('id', 'num'+numH);
		numH++;
	});

	$('#watchlistTable tbody tr:first-child td').each(function() {
		var id = $(this).attr('wid');
		var wid = $(this).attr('data-width');
		$('#num'+id).css('width', wid);
	});

	$('table .delete').click(function() {
		$(this).parent('tr').remove();
	});

	$('#fullCHarts').click(function() {
		$('.chartBox').toggleClass('fullScreen');
		$(this).toggleClass('ant');
	});

	$('#chpok').click(function() {		
		var chartScr = $('#chart').offset().top;
		$('.chartBox').toggleClass('fullWidth');
		$(this).toggleClass('ant');
		$('body,html').animate({scrollTop: chartScr-40}, 300);
	});

	$('#watchlistTable tbody tr td').click(function() {		
		var chartScr = $('#chart').offset().top;
		$('body,html').animate({scrollTop: chartScr-40}, 300);
	});

    $('.mobileButton').click(function() {
        $('.mobile-sidebar').toggleClass('open');
    });

	$('.select').click(function(e) {
    	e.stopPropagation();
    	$('.select').removeClass('open');
    	$(this).toggleClass('open');
    });

	var numI = 1;
    $('.select .icons li').each(function() {
    	$(this).attr('id', 'num'+numI);
    	numI++;
    });

    var numL = 1;
    $('.select .list li').each(function() {
    	$(this).attr('data-number', numL);
    	numL++;
    });

    $('body').click(function() {
    	$('.select').removeClass('open');
    });

   	$('.select .list li').click(function() {
   		var th = $(this);
   		var id = th.attr('data-number');
   		if (th.hasClass('active')) {

   		}
   		else{
   			th.parents('.select').children('.icons').find('li.active').removeClass('active');
   			th.parents('.select').children('.list').find('li.active').removeClass('active');
   			th.addClass('active');
   			$('#num'+id).addClass('active');
   		}
   	});

    $('.logo:last-child').addClass('last');
    $('.logo:first-child').addClass('first');
    function logosAnim (){
		var act = $('.logo.active');
		if (act.hasClass('last')) {
			$('.logo').removeClass('prev');
			act.removeClass('active').addClass('prev');
			$('.logo:first-child').addClass('active');
		}
		else{
			$('.logo').removeClass('prev');
			act.removeClass('active').addClass('prev');
			act.next('.logo').addClass('active');
		}
    }
	setInterval(function() {
		logosAnim ();
	}, 3000);

    $('.panel-article table').each(function() {
        $(this).wrap('<div class="table-responsive"></div>');
    });
    
	$('.post, .commentItem').each(function() {
		var postHeight = $(this).height();

		if (postHeight>400) {
			$(this).addClass('bHeight');
			$(this).addClass('hid_com');
		};
	});

	$('#forum-comment').on('update',function() {
	  $('.post').each(function() {
			var postHeight = $(this).height();
			if (postHeight>400) {
				$(this).addClass('bHeight');
				$(this).addClass('hid_com');
			};
		});
	});

	$('#list-comments').on('update',function() {
		$('.commentItem').each(function() {
			var postHeight = $(this).height();
			if (postHeight>400) {
				$(this).addClass('bHeight');
				$(this).addClass('hid_com');
			};
		});
	});

	$('.forum-comment, #list-comments').on("click", ".showPostLink", function() {
     	var target = $(this).attr('href');
		$(this).parents('.post, .commentItem').toggleClass('hid_com');
     	$('html, body').animate({scrollTop: $(target).offset().top-50}, 500);
     	return false;
	});

	$('.forum-comment, #list-comments').on("click", ".hidePostLink",function() {
     	var target = $(this).attr('href');
		$(this).parents('.post, .commentItem').toggleClass('hid_com');
     	$('html, body').animate({scrollTop: $(target).offset().top-50}, 500);
     	return false;
	});


    $('.notifications .body').mCustomScrollbar({
    	theme:"minimal-dark",
    	scrollInertia:100
    });
	$('a[data-popup="lightbox"]').fancybox();

});