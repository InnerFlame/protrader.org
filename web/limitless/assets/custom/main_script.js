jQuery(document).ready(function($) {

  treeFun();
  initScroll();
  functWidth();
  sidebarFixed();
  AppButtonKB();
  funct_delete();
  $(window).scroll(function(){
    headroomTop();
  })

  $('#enter').on('click', function() {
      swal({
          title: "Хорошая работа!",
          text: "Ты успешно вошел в аккаунт!",
          confirmButtonColor: "#66BB6A",
          type: "success",
          timer: 1500
      });
  });

  $('#reg').on('click', function() {
      swal({
          title: "Ай молодець!",
          text: "Проверь почту, там через пару мин, все будет!",
          confirmButtonColor: "#66BB6A",
          type: "success"
      });
  });

  $('.abs-right').click(function(e) {
    e.stopPropagation();
    $(this).removeClass('clSearch');
  });

  $('body').click(function(e) {
    $('.abs-right').addClass('clSearch');
  });

  $('a[href^="#"]').click(function(){
      var el = $(this).attr('href');
      if ($(this).attr('data-toggle')=='collapse') {

      }
      else {
        $('body').animate({
        scrollTop: $(el).offset().top-50}, 500);
      }
  });

  $('body').append('<div style="display: none;" class="fa fa-arrow-up bg-primary-300" id="top"></div>');

  var delay = 500;
    $('#top').mouseover( function(){
      $( this ).animate({opacity: 0.65},100);
    }).mouseout( function(){
      $( this ).animate({opacity: 1},100);
    }).click( function(){
      $('body, html').animate({
        scrollTop: 0
      }, delay);
    });

    $(window).scroll(function(){
      if ( $(document).scrollTop() > 150 ) {
        $('#top').fadeIn('fast');
      } else {
        $('#top').fadeOut('fast');
      }
    });

  function funct_delete() {
    $('.sweet_combine').on('click', function() {
        swal({
            title: "Вы действительно хотите удалить?",
            text: "Вы не сможете востановить этот материал!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: "Удалить",
            cancelButtonText: "Отмена",
            closeOnConfirm: false,
            closeOnCancel: true
        },
        function(isConfirm){
            if (isConfirm) {
                swal({
                    title: "Удалено",
                    text: "",
                    confirmButtonColor: "#66BB6A",
                    type: "success",
                    timer: 1200
                });
            }
        });
    });

  }

  function initScroll() {
    $(".sidebar-fixed .sidebar-content").niceScroll({
        mousescrollstep: 100,
        cursorcolor: '#ccc',
        cursorborder: '',
        cursorwidth: 3,
        hidecursordelay: 100,
        autohidemode: 'scroll',
        horizrailenabled: false,
        preservenativescrolling: false,
        railpadding: {
        	right: 0.5,
        	top: 1.5,
        	bottom: 1.5
        }
    });
	}

  // Buttons on pages KB
  function AppButtonKB() {
    $('#App .btn').click(function() {
      var id = $(this).children('span').text();
      $('#App .btn').removeClass('bg-blue');
      $(this).addClass('bg-blue');
      $('.tree').addClass('d-none');
      $('#'+id).removeClass('d-none');

    });

    $('#App .btn').each(function() {
      if ($(this).hasClass('bg-blue')) {
        var id = $(this).children('span').text();
        $('#'+id).removeClass('d-none');
      };
    });
  }

  // Top navbar
  function headroomTop() {

    $(".navbar-fixed-top").addClass('fix');

    var scroll = $(window).scrollTop();
    if ( scroll < 44 ){
      $(".navbar-fixed-top").addClass('fix');
      $('body').attr('style', '');
    }
    else {
      $(".navbar-fixed-top").removeClass('fix');
      $('body').css('padding-top', '46px');
    }

  }
  // Trees
  function treeFun() {
    $('.tree-d li.folder').click(function(event) {
      event.stopPropagation();
      $('.tree-d li.folder').removeClass('focused');
      $(this).addClass('focused');
      if ($(this).hasClass('folder')) {
        $(this).toggleClass('expanded');
      }
    });
  }

  // windowSize
  function functWidth() {
    var wWidth = $(window).width();

    if (wWidth > 1200) {
        $('body').addClass('layout-boxed');
        $('.navbar').wrapInner('<div class="navbar-boxed"></div>');
    }
  }

  // Attach BS affix component to the sidebar
  function sidebarFixed() {
    var bodyHe = $('.page-content .content').height();
    var aHe = $(window).height() - $('body > .navbar').outerHeight() - $('body > .navbar + .navbar').outerHeight() - $('body > .navbar + .navbar-collapse').outerHeight() - $('.page-header').outerHeight();

    if (bodyHe < aHe) {

    }
    else {
      if ($('.sidebar-fixed').length>0) {
        $('.sidebar-fixed').affix({
            offset: {
                top: $('.sidebar-fixed').offset().top - 64 // top offset - computed line height
            }
        });
      }
    }
  }

  // Resize
  function resizeScroll() {
    $('.sidebar-fixed .sidebar-content').getNiceScroll().resize();
  }

  // Remove
  function removeScroll() {
    $(".sidebar-fixed .sidebar-content").getNiceScroll().remove();
    $(".sidebar-fixed .sidebar-content").removeAttr('style').removeAttr('tabindex');
  }

});
