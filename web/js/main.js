jQuery(document)
    .on('pjax:start', function() { jQuery('body').addClass('pjax-loading'); })
    .on('pjax:end',   function() { jQuery('body').removeClass('pjax-loading'); });

// jQuery(".featCatName").click(function() {
//         jQuery(this).parents('.featuresTable').toggleClass("collapsed");
//     });

	//.ready(function(){
	//	$('table').footable({
	//		breakpoints: {
	//			phone: 480,
     //           tablet: 1024
     //       }
     //   });
	//});


window.onload = function () {


    /* link_target_rel for inserting here in every link rel(nofollow) and target (blank)*/
    jQuery('div.link_target_rel a').each(function () {

        var a_href = jQuery(this).attr('href');

        //console.log(jQuery(this).attr('href').indexOf( 'protrader' ),jQuery(this).attr('href'),indexof.indexOf( 'protrader' ));

        if (a_href.indexOf('protrader') == -1) {

            if (jQuery(this).attr('target') != '_blank')
                jQuery(this).attr('target', '_blank');

            if (jQuery(this).attr('rel') != 'nofollow')
                jQuery(this).attr('rel', 'nofollow');

        }

    });


};
//Lightbox is off for image in imperavi redactor
$('body').on('click','a[data-popup=lightbox]', function(e){
    if($(e.target).parents('.redactor-box').length > 0){
        return false;
    }
});

$(document).ready(function(){
    function handleTypeButton() {
        $('[type=button][onclick]').each(function () {
            var handler = $(this).attr('onclick');
            $(this).prop('onclick', false);
            $(this).data('handler', handler);
        });
    }
    $('[type=button]').on('click', function(e){
        if($(this).data('safe') == true){
            if(window.user != undefined && user != null && user.id > 0){
                if($(this).data('handler')){
                    eval($(this).data('handler'));
                }
                return true;
            }
            $('#modal_form_inline').modal('show');
            e.stopPropagation();
            return false;
        }
        return true;
    });
    handleTypeButton();
});
