/**
 * Created by P.Bilik on 04.02.2016.
 */

(function($)
{
    $.Redactor.prototype.advanced = function()
    {

        return {
            getTemplate: function()
            {
                return String()
                    + '<div class="modal-section" id="redactor-modal-advanced">'
                    + '<section>'
                    + '<label>Enter title</label>'
                    + '<input id="mymodal-textarea" type="text"/>'
                    + '</section>'
                    + '<section>'
                    + '<button id="redactor-modal-button-action">Insert</button>'
                    + '</section>'
                    + '</div>';
            },
            init: function ()
            {
                //var button = this.button.add('advanced', 'Spoiler');
                //$(button).removeClass();
                //$(button).addClass('fa fa-caret-square-o-down');
                //this.button.addCallback(button, this.advanced.show);
                //$(button).prop('disabled', true);
            },
            show: function()
            {
                this.selection.save();
                this.modal.addTemplate('advanced', this.advanced.getTemplate());
                this.modal.load('advanced', 'Set spoiler title', 400);
                var button = $('#redactor-modal-button-action');
                button.on('click', this.advanced.insert);

                this.modal.show();

                $('#mymodal-textarea').focus();
            },
            insert: function()
            {
                this.modal.close();
                this.selection.restore();
                var html = $('#mymodal-textarea').val();
                var text = this.selection.getText();
                this.selection.replaceWithHtml('<div class="spoiler collapsed"><div class="sp-title">'+html+'</div><div class="sp-body">'+text+'</div></div><br/>');

            }
        };
    };
})(jQuery);