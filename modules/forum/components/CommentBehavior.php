<?php
namespace app\modules\forum\components;


use yii\base\Behavior;
use yii\db\ActiveRecord;

class CommentBehavior extends Behavior
{

    /**
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
            ActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
        ];
    }

    /**
     * @param $event
     */
    public function afterInsert($event)
    {
        $this->owner->topic->updateCounters(['count_comments' => 1]);
    }

    /**
     * @param $event
     */
    public function afterDelete($event)
    {
        $this->owner->topic->updateCounters(['count_comments' => -1]);
    }

}

