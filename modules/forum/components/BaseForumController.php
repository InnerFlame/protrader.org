<?php
/**
 * Created by PhpStorm.
 * User: writer
 * Date: 27.01.2016
 * Time: 11:07
 */

namespace app\modules\forum\components;

use app\components\CustomController;
use app\modules\forum\assets\ForumAsset;
use app\modules\forum\models\FrmCategory;
use Yii;

abstract class BaseForumController extends CustomController
{

    public $indexUrl = '/forum';
    public $deleteCommentUrl = '/forum/comment/delete?id=';
    public $deleteTopic = '/forum/topic/delete?id=';

    public function init()
    {
        $view = $this->getView();
        ForumAsset::register($view);
        return parent::init();
    }

    /**
     * Replace and return current alias
     * @param $alias full alias categoryname/subcategoryname
     * @return mixed subcategoryname
     */
    protected function getCurrentAlias($alias)
    {
        $data = explode("/", $alias);
        
        $end = $data[count($data) - 1];
        
        //return before last element of array
        if ($end === '') {

            $array = explode("/", $alias);

            return $array[count($array) - 2];

        } else
            return $end;

    }

    //TODO: refactoring
    protected function addBreadCrumbs($model)
    {
        $this->breadcrumbs[] = [
            'label' => 'Forum',
            'url' => $this->indexUrl,
            'active' => 'active',
        ];

        if (is_object($model)) {
            if (is_object($model->parent)) {
                if ($model->parent->id > 0) {
                    $parent_model = FrmCategory::findOne($model->parent->id);
                    if (is_object($parent_model->parent)) {
                        if ($parent_model->parent->id > 0) {
                            $parent_model2 = FrmCategory::findOne($parent_model->parent->id);
                            if (is_object($parent_model2) && $parent_model2->title !== 'Root') {
                                $this->breadcrumbs[] = [
                                    'label' => $parent_model2->title,
                                    'url' => $parent_model2->getViewUrl(),
                                    'active' => 'active',
                                ];
                            }
                        }
                        if (is_object($parent_model) && $parent_model->title !== 'Root') {
                            $this->breadcrumbs[] = [
                                'label' => $parent_model->title,
                                'url' => $parent_model->getViewUrl(),
                                'active' => 'active',
                            ];
                        }
                    }
                }
            }
        }
    }
}