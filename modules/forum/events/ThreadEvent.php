<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 * Date: 20.04.2016
 * Time: 13:32
 */

namespace app\modules\forum\events;

use app\modules\forum\models\FrmTopic;
use app\modules\notify\components\Notification;

class ThreadEvent extends Notification
{
    public $topic_id;

    private $_topic;

    public function getTitle()
    {
        return $this->getEntity()->title;
    }

    public function getDescription()
    {
        return $this->getEntity()->content;
    }

    public function getAuthor()
    {
        return $this->getEntity()->user->getFullName();
    }
    /**
     * @return \app\models\CustomModel|FrmTopic
     */
    public function getEntity()
    {
        if(!$this->_topic){
            $this->_topic = FrmTopic::findOne($this->topic_id);
        }
        return $this->_topic;
    }

}