<?php

use app\modules\cabinet\modules\notify\models\NotifyTemplate;

return [

    \app\modules\forum\ForumModule::EVENT_COMMENT_ADD => [
        'label'     => 'New topics',
        'templates' => [
            NotifyTemplate::TYPE_EMAIL => [
                'title' => 'has created a new comment in',
                'body' => '{title}'
            ],
            NotifyTemplate::TYPE_FULL  => [
                'title' => '{title}',
                'body' => '{username} has added a new comment {description}'
            ],
            NotifyTemplate::TYPE_SHORT => [
                'title' => 'has created a new comment in',
                'body' => '{title}'
            ],
            NotifyTemplate::TYPE_GROUP => [
                'title' => '{count} new comments in thread',
                'body' => '{title}'
            ]
        ]
    ],

    \app\modules\forum\ForumModule::EVENT_COMMENT_EDIT => [
        'label'     => 'Edited post',
        'templates' => [
            NotifyTemplate::TYPE_EMAIL => [
                'title' => 'edited comment in',
                'body' => '{title}'
            ],
            NotifyTemplate::TYPE_FULL  => [
                'title' => '{title}',
                'body' => 'comment was edited: {description}'
            ],
            NotifyTemplate::TYPE_SHORT => [
                'title' => 'edited comment in',
                'body' => '{title}'
            ],
            NotifyTemplate::TYPE_GROUP => [
                'title' => '{count} comments were edited in',
                'body' => '{title}'
            ]
        ]
    ],

    \app\modules\forum\ForumModule::EVENT_TOPIC_ADD => [
        'label'     => 'New topics',
        'templates' => [
            NotifyTemplate::TYPE_EMAIL => [
                'title' => 'added new topic',
                'body' => '{title}'
            ],
            NotifyTemplate::TYPE_FULL  => [
                'title' => '{title}',
                'body' => 'topic was added by {username}'
            ],
            NotifyTemplate::TYPE_SHORT => [
                'title' => 'added new topic',
                'body' => '{title}'
            ],
            NotifyTemplate::TYPE_GROUP => [
                'title' => '{count} new topic(s) was(ware) add',
                'body' => '{title}'
            ]
        ]
    ],

//    \app\modules\forum\ForumModule::EVENT_TOPIC_EDIT => [
//        'label'     => 'Edited topic',
//        'templates' => [
//            NotifyTemplate::TYPE_EMAIL => [
//                'title' => 'edited topic',
//                'body' => '{title}'
//            ],
//            NotifyTemplate::TYPE_FULL  => [
//                'title' => 'edited topic',
//                'body' => '{title}'
//            ],
//            NotifyTemplate::TYPE_SHORT => [
//                'title' => 'edited topic',
//                'body' => '{title}'
//            ],
//            NotifyTemplate::TYPE_GROUP => [
//                'title' => 'edited topic',
//                'body' => '{title}'
//            ]
//        ]
//    ],

    \app\modules\forum\ForumModule::EVENT_TOPIC_MOVE => [
        'label'     => 'Topic has moved',
        'templates' => [
            NotifyTemplate::TYPE_EMAIL => [
                'title' => 'moved topic',
                'body' => '{title}'
            ],
            NotifyTemplate::TYPE_FULL  => [
                'title' => '{title}',
                'body' => 'topic was moved'
            ],
            NotifyTemplate::TYPE_SHORT => [
                'title' => 'moved topic',
                'body' => '{title}'
            ],
            NotifyTemplate::TYPE_GROUP => [
                'title' => '{count} times topic was moved',
                'body' => '{title}'
            ]
        ]
    ],

];