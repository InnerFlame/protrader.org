<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 * Date: 04.05.2016
 * Time: 12:17
 */

namespace app\modules\forum\events;


class MoveTopicEvent extends ThreadEvent
{
    public $oldCategory_id;

    public $category_id;
}