<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 * Date: 04.05.2016
 * Time: 11:36
 */

namespace app\modules\forum\events;

use app\components\Entity;
use app\modules\forum\models\FrmComment;
use app\modules\notify\components\Notification;

/**
 * Событие, связанное с добавлением нового поста на форуме
 * Привязано с сущности топика.
 * Class CommentThreadEvent
 * @package app\modules\forum\events
 */
class CommentThreadEvent extends Notification
{
    public $post_id;

    private $_post;

    private $_entity;


    /**
     * Возвращает название сущности к которой подвязано событие
     * @return mixed
     */
    public function getTitle()
    {
        if($this->getEntity()) {
            return $this->getEntity()->title;
        }
    }

    /**
     * Возвращает содержание поста, который
     * @return string
     */
    public function getDescription()
    {
        if($this->getPost()) {
            return $this->getPost()->content;
        }
    }

    /**
     * Получить автора данного комментария.
     * @return mixed
     */
    public function getAuthor()
    {
        if($this->getPost()) {
            return $this->getPost()->user->getFullName();
        }
    }

    /**
     * Возвращает topic, так как данное событие
     * относится именно к топику
     * @return mixed
     */
    public function getEntity()
    {
        if($this->getPost()) {
            if (!$this->_entity) {
                $this->_entity = $this->getPost()->topic;
            }
            return $this->_entity;
        }
    }

    /**
     * Возвращает ссылку на непосредственно пост, который был добавлен
     * что бы от него получить автора, содержание и возможно другую информацию
     * @return FrmComment
     */
    public function getPost()
    {
        if (!$this->_post) {
            $this->_post = FrmComment::find()->where('id = :id', [':id' => $this->post_id])->andWhere('deleted = :deleted', [':deleted' => Entity::NOT_DELETED])->one();
        }
        return $this->_post;
    }
}