<?php

namespace app\modules\forum\models;

use app\components\Entity;
use app\components\helpers\StringHelper;
use app\components\validators\EmptyContent;
use app\components\validators\MaxLength;
use app\models\community\Counts;
use app\models\user\User;
use app\modules\forum\components\CommentBehavior;
use creocoder\nestedsets\NestedSetsBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Html;

/**
 * This is the model class for table "frm_comments".
 *
 * @property integer $id
 * @property integer $topic_id
 * @property integer $user_id
 * @property string $content
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 */
class FrmComment extends Entity
{
    public static $_pull = [];
    public $entity;
    public $reply_content;
    public $action;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'frm_comments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content'], 'required'],
            [['content'], EmptyContent::className()],
            [['user_id', 'created_at', 'updated_at'], 'integer'],
            [['content'], 'string'],
            [['content'], MaxLength::className()],
            [['level', 'topic_id', 'user_id', 'reply_id', 'rgt', 'lft', 'published', 'reply_content'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'topic_id'   => 'Topic ID',
            'user_id'    => 'User ID',
            'content'    => 'Content',
            'published'  => 'Published',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getTopic()
    {
        return $this->hasOne(FrmTopic::className(), ['id' => 'topic_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCounts()
    {
        return $this->hasOne(Counts::className(), ['entity_id' => 'id'])->where([
            'entity' => $this->getTypeEntity()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            'tree' => [
                'class'          => NestedSetsBehavior::className(),
                'treeAttribute'  => 'reply_id',
                'leftAttribute'  => 'lft',
                'rightAttribute' => 'rgt',
                'depthAttribute' => 'level',
            ],

            [
                'class' => CommentBehavior::className(),
            ],
        ];
    }

    public function getName()
    {
        return $this->content;
    }

    public static function getGridColumns()
    {
        return [
            [
                'class'          => 'yii\grid\SerialColumn',
                'header'         => '<span>#</span>',
                'contentOptions' => [
                    'width' => 50
                ],
            ],


            [
                'attribute' => 'user_id',
                'format'    => 'raw',
                'value'     => function ($model) {
                    $user = $model->user;
                    if ($user instanceof User) {
                        return Html::a($user->username, $user->getViewUrl());
                    }
                }
            ],
            [
                'attribute' => 'topic_id',
                'format'    => 'raw',
                'value'     => function (FrmComment $model) {
                    $topic = $model->topic;
                    if ($topic && $topic instanceof FrmTopic)
                        return Html::a($topic->title, $topic->getViewUrl());
                }
            ],

            [
                'attribute' => 'content',
                'format'    => 'raw',
                'value'     => function (FrmComment $model) {
                    return Html::a(StringHelper::truncate($model->content, 15), $model->getViewUrl());
                }
            ],

            [
                'class'    => 'app\components\grid\ActionColumnCustom',
                'template' => function ($model) {
                    return '{edit} {delete}';
                },
                'buttons'  => [
                    'edit'   => function ($url, $model, $key, $options) {
                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/post-edit', 'id' => $model->id]);
                        return Html::a('Edit', $url, $options);
                    },
                    'delete' => function ($url, $model, $key, $options) {
                        $options['onclick'] = '
                            if(confirm(\'' . Yii::t('yii', 'Are you sure you want to delete this Category?') . '\'))
                            {
                              return true;
                            } else {
                              return false;
                            }
                        ';
//                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/post-delete', 'id' => $model->id]);
                        return Html::a('Delete', $url, $options);
                    },
                ],
            ],
        ];
    }

    public function hasChildren()
    {
        $children = $this->children()->all();
        return count($children);
    }

    /**
     * Get and return all tree comments for topic,
     * by id
     * @param $topic_id
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getAllTreeCommentsByPostId($topic_id)
    {
        return self::find()->where('topic_id = :topic_id AND deleted = 0', [':topic_id' => $topic_id])->orderBy(['reply_id' => SORT_ASC, 'lft' => SORT_ASC])->all();
    }

    public function getTypeEntity()
    {
        return Entity::FORUM_COMMENT;
    }

    public function getViewUrl()
    {
        if (is_object($this->topic)){
            return $this->topic->getViewUrl() . '#reply' . $this->id;
        }
        return '';
    }

    public function delete()
    {
        return parent::delete(); // TODO: Change the autogenerated stub
        $childrens = $this->children()->all();
        foreach ($childrens as $child) {
            $child->delete();
        }
    }
}