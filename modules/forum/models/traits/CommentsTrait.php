<?php
/**
 * Created by PhpStorm.
 * User: writer
 * Date: 27.01.2016
 * Time: 13:36
 */

namespace app\modules\forum\models\traits;

use app\modules\forum\models\FrmComment;
use Yii;

trait CommentsTrait
{
    public function getComment(){
        $params = Yii::$app->request->post();
        if(isset($params['comment_id'])){
            $comment = FrmComment::findOne((int)$params['comment_id']);
            return $comment ? $comment : null;
        }
        return null;
    }
}