<?php

namespace app\modules\forum\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * FrmCommentSearch represents the model behind the search form about `app\modules\forum\models\FrmComment`.
 */
class FrmCommentSearch extends FrmComment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'topic_id', 'user_id', 'published', 'deleted', 'created_at', 'updated_at', 'reply_id', 'lft', 'rgt', 'level'], 'integer'],
            [['content'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FrmComment::find()->andWhere(['deleted' => 0])->orderBy(['created_at' => SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'id'         => $this->id,
            'topic_id'   => $this->topic_id,
            'user_id'    => $this->user_id,
            'published'  => $this->published,
            'deleted'    => $this->deleted,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'reply_id'   => $this->reply_id,
            'lft'        => $this->lft,
            'rgt'        => $this->rgt,
            'level'      => $this->level,
        ]);

        $query->andFilterWhere(['like', 'content', $this->content]);

        return $dataProvider;
    }
}
