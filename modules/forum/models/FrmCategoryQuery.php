<?php

namespace app\modules\forum\models;
use app\components\Entity;

/**
 * This is the ActiveQuery class for [[FrmCategory]].
 *
 * @see FrmCategory
 */
class FrmCategoryQuery extends \yii\db\ActiveQuery
{
    /**
     * @inheritdoc
     * @return FrmCategory[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return FrmCategory|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @return $this
     */
    public function notDeleted()
    {
        return $this->andWhere(['deleted' => Entity::NOT_DELETED]);
    }

    /**
     * @return $this
     */
    public function notRoot()
    {
        return $this->andWhere(['<>', 'level', FrmCategory::LEVEL_ROOT]);
    }

    /**
     * @return $this
     */
    public function newest()
    {
        return $this->orderBy('created_at DESC');
    }

    public function alias($alias)
    {
        return $this->andWhere(['alias' => $alias]);
    }
}
