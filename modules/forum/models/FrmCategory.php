<?php

namespace app\modules\forum\models;

use app\components\ceo\Ceo;
use app\components\ceo\CeoBehavior;
use app\components\Entity;
use app\models\community\Counts;
use creocoder\nestedsets\NestedSetsBehavior;
use himiklab\sitemap\behaviors\SitemapBehavior;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\Url;
use app\modules\forum\models\FrmTopic;

/**
 * This is the model class for table "tbl_category".
 *
 * @property integer $id
 * @property string $title
 * @property string $weight
 * @property string $description
 * @property integer $root
 * @property integer $lft
 * @property integer $rgt
 * @property integer $level
 * @property integer $status
 * @property integer $created_at
 */
class FrmCategory extends Entity
{
    /*
    |--------------------------------------------------------------------------
    | Constants && properties
    |--------------------------------------------------------------------------
    */

    public static $_pull = [];
    const LEVEL_ROOT = 0;
    const LEVEL_MAIN = 1;
    static $stringAttr = ['title', 'alias'];

    /*
    |--------------------------------------------------------------------------
    | Model configurations
    |--------------------------------------------------------------------------
    */

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'frm_category';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            'tree' => [
                'class'          => NestedSetsBehavior::className(),
                'treeAttribute'  => 'root',
                'leftAttribute'  => 'lft',
                'rightAttribute' => 'rgt',
                'depthAttribute' => 'level',
            ],
            [
                'class'         => SluggableBehavior::className(),
//                'attribute'     => 'title',
                'slugAttribute' => 'alias',
                'immutable'     => 'true',
                'value'         => function($event){
                    $category = FrmCategory::findOne($event->sender->parent_id);
                    return $category->alias . '/' . Inflector::slug($event->sender->title);
                }
            ],
            [
                'class' => CeoBehavior::className(),
            ],

            [
                'class'       => SitemapBehavior::className(),
                'scope'       => function ($model) {
                    /** @var \yii\db\ActiveQuery $model */
                    $except_id = [];

                    $model->select(['id', 'alias', 'updated_at', 'deleted']);

                    foreach($model->all() as $cat){
                        if($cat->deleted == FrmTopic::DELETED){
                            $except_id[] = $cat->id;
                            $childrens = FrmCategory::findOne($cat->id)->children()->all();
                            foreach($childrens as $child){
                                $except_id[] = $child->id;
                            }
                        }

                    }
                    $model->andWhere(['NOT IN', 'id', $except_id]);
                    $model->orderBy(['updated_at' => SORT_DESC]);
                },
                'dataClosure' => function ($model) {
                    /** @var self $model */

                    return [
                        'loc'        => Url::toRoute([$model->getViewUrl()]),
                        'lastmod'    => date('Y-m-d\TH:i:sP', $model->updated_at),
                        'changefreq' => SitemapBehavior::CHANGEFREQ_WEEKLY,
                        'priority'   => 0.9
                    ];
                }
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description'], 'required'],
            [['title'], 'unique'],
            [['root', 'weight', 'created_at'], 'integer'],
            [['title'], 'string', 'max' => 120, 'min' => 3],
            [['description'], 'string', 'max' => 255, 'min' => 3],
            [['level', 'root', 'rgt', 'lft', 'created_at', 'updated_at', 'parent_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('app', 'ID'),
            'title'       => Yii::t('app', 'Title'),
            'alias'       => Yii::t('app', 'Alias'),
            'weight'      => Yii::t('app', 'Weight'),
            'description' => Yii::t('app', 'Description'),
            'root'        => Yii::t('app', 'Root'),
            'lft'         => Yii::t('app', 'Lft'),
            'rgt'         => Yii::t('app', 'Rgt'),
            'level'       => Yii::t('app', 'Level'),
            'published'   => Yii::t('app', 'published'),
            'created_at'  => Yii::t('app', 'Created At'),
            'updated_at'  => Yii::t('app', 'Updated At'),
            'deleted'     => Yii::t('app', 'Deleted'),
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */

    public function getAllParentAlias()
    {
        $url = '';
        $parents = $this->parents()->all();

        foreach ($parents as $parent) {
            if ($parent->level != FrmCategory::LEVEL_ROOT && $parent->alias !== 'forum')
                $url .= $parent->alias . '/';
        }

        return $url . $this->alias;
    }

    public static function getMain()
    {
        return self::find()->where([
            'level' => self::LEVEL_MAIN,
        ])->orderBy('weight ASC')->notDeleted()->all();
    }

    public function getCeo()
    {
        return $this->hasOne(Ceo::className(), ['entity_id' => 'id'])->where([
            'entity' => $this->getTypeEntity()
        ]);
    }

    public function getCounts()
    {
        return $this->hasOne(Counts::className(), ['entity_id' => 'id'])->where([
            'entity' => $this->getTypeEntity()
        ]);
    }

    public function getParent()
    {
        return $this->parents(1)->one();
    }

    public static function getPublicList()
    {
        $models = self::find()->orderBy('weight ASC')->where(['<>', 'level', [self::LEVEL_MAIN, self::LEVEL_ROOT]])->all();

        return ArrayHelper::map($models, 'id', 'title');
    }

    public function getChildren($category_id = null)
    {
        $id = (int)$category_id > 0 ? $category_id : $this->id;

        $categories = self::findOne((int)$id);

        if (is_object($categories))
            return $categories->children(1)->orderBy('weight ASC')->notDeleted()->all();
    }

    public function getTopics()
    {
        return $this->hasMany(FrmTopic::className(), ['category_id' => 'id'])->notDeleted();
    }

    /**
     * @return array
     */
    public function getFreshTopics()
    {
        if (!empty(static::$_pull['get_fresh_topics']))
            return static::$_pull['get_fresh_topics'];

        $hottest_topics = FrmTopic::find()
            ->where(['>=', 'count_comments', 10])
            ->andWhere(['category_id' => $this->id])
            ->notDeleted()
            ->orderBy(['count_comments' => SORT_DESC])
            ->all();

        $fresh_topics = FrmTopic::find()
            ->where(['category_id' => $this->id])
            ->notDeleted()
            ->andWhere(['<', 'count_comments', 10])
            ->orderBy(['updated_at' => SORT_DESC])
            ->all();

        static::$_pull['get_fresh_topics'] = ArrayHelper::merge($hottest_topics, $fresh_topics);

        return static::$_pull['get_fresh_topics'];
    }

    /**
     *
     * @param null $category_id
     * @return bool|null|string
     */
    public function getCountTopics($category_id = null)
    {
        $id = (int)$category_id > 0 ? $category_id : $this->id;
        $sql = "SELECT COUNT(*) FROM " . FrmTopic::tableName() . ' WHERE category_id = ' . $id . ' AND deleted = ' . Entity::NOT_DELETED;

        return Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public function getCountViewTopics()
    {
        $count_view = null;
        $children = $this->children()->all();

        //add current category for + counts her topics view
        array_push($children, $this);

        foreach ($children as $child) {
            foreach ($child->topics as $topic) {
                if (is_object($topic->counts)) {
                    $count_view += $topic->counts->count_view;
                }

            }
        }

        return $count_view;
    }

    public function getName()
    {
        return $this->title;
    }

    public function getLastPostByCategoryId()
    {
        $created_at = null;
        $result = null;

        $сhildren = $this->getChildren($this->id);

        if (count($сhildren) > 0) {
            foreach ($сhildren as $сhild) {
                if (self::findLastCommentByCategory($сhild->id)) {//if in children have no posts we find in children topics
                    $result = self::findLastCommentByCategory($сhild->id);
                } else {
                    $result = self::findLastCommentByCategory($this->id);
                }
            }
        } else {
            $result = self::findLastCommentByCategory($this->id);
        }

        if ($result)
            return $result;

        return null;
    }

    public function findLastCommentByCategory($id)
    {
        $topics = FrmTopic::findAll(['category_id' => $id, 'deleted' => Entity::NOT_DELETED]);
        $created_at = null;
        $last_post = [];
        $post = null;

        foreach ($topics as $topic) {
            $post = $topic->getFreshComment();
            if (isset($post) && $created_at < $post->created_at)
                $last_post = $post;
        }

        if ($last_post)
            return $last_post;

        return null;
    }


    public function findLastTopic()
    {
        $last = null;
        $fresh_topic = null;

        $сhildren = $this->getChildren($this->id);

        if (count($сhildren) > 0) {
            foreach ($сhildren as $сhild) {
                foreach ($сhild->topics as $topic) {
                    if ($topic->created_at > $last) {
                        $last = $topic->created_at;
                        $fresh_topic = $topic;
                    }
                }
            }
        }

        foreach ($this->topics as $topic) {
            if ($topic->created_at > $last) {
                $last = $topic->created_at;
                $fresh_topic = $topic;
            }
        }

        return $fresh_topic;
    }


    public static function getGridColumns()
    {
        return [
            [
                'class'          => 'yii\grid\SerialColumn',
                'header'         => '<span>#</span>',
                'contentOptions' => [
                    'width' => 50
                ],
            ],
            [
                'attribute' => 'title',
                'format'    => 'raw',
                'value'     => function ($model) {
                    return '<a href="' . $model->getViewUrl() . '">' . $model->title . '</a>';
                },
            ],
            [
                'attribute' => 'parent',
                'value'     => function ($model) {
                    //  return self::getParentTitle($model->id);
                },
                'format'    => 'raw',
            ],
            [
                'attribute' => 'alias',
                'format'    => 'raw',
            ], [
                'attribute' => 'weight',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'description',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'level',
                'format'    => 'raw',
            ],
            [
                'class'    => 'app\components\grid\ActionColumnCustom',
                'template' => function ($model) {
                    return '{edit} {delete}';
                },
                'buttons'  => [
                    'edit'   => function ($url, $model, $key, $options) {
                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/category/edit', 'id' => $model->id]);

                        return Html::a('Edit', $url, $options);
                    },
                    'delete' => function ($url, $model, $key, $options) {
                        $options['onclick'] = '
                            if(confirm(\'' . Yii::t('yii', 'Are you sure you want to delete this Category?') . '\'))
                            {
                              return true;
                            } else {
                              return false;
                            }
                        ';
//                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/category/delete', 'id' => $model->id]);

                        return Html::a('Delete', $url, $options);
                    },
                ],
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getViewUrl()
    {
        return '/forum/category/' . $this->alias;
    }

    public function getTypeEntity()
    {
        return Entity::FORUM_CATEGORY;
    }

    /**
     * @inheritdoc
     * @return FrmCategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FrmCategoryQuery(get_called_class());
    }

    /**
     * Меняем URL при перемещении категории, текущей директории, а также всем ее дочерним
     * $this->alias - URL текущей категории которую редактируем
     * $child->alias - URL дочерних категорий
     * @return string
     */
    public function changeAlias()
    {
        $this->changeModelAlias($this);
        $childs = FrmCategory::findOne($this->id)->children()->all();
        foreach ($childs as $child) {
            $this->changeModelAlias($child);
        }
    }

    /**
     * Меняем URL переданной модели категории, добавляем перед ним URL куда переместили
     * Исключаем root alias из полного URL-а
     * $model->alias - новый URL
     * $new_parent_model - модель ноды куда мы переносим
     * @param $model
     */
    public function changeModelAlias($model)
    {
        $new_parent_model = FrmCategory::findOne($model->parent_id);
        $short_alias = end(explode('/', $model->alias));
        $model->alias = ($new_parent_model->alias == 'root') ? $short_alias : $new_parent_model->alias . '/' . $short_alias;
        $model->save();

        $ceo = Ceo::findOne($model->ceo->id);
        $ceo->detachBehavior('lang');# что бы сохранядся каноникал

        $ceo->canonical = Yii::$app->urlManager->createAbsoluteUrl('forum/category/' . $model->alias);
        $ceo->save();

        foreach($model->topics as $topic){
            if($topic instanceof FrmTopic){
                $topic->changeAliasByCategoryModel($model);
            }
        }
    }
}
