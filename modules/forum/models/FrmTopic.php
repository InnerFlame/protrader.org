<?php

namespace app\modules\forum\models;

use app\components\ceo\Ceo;
use app\components\ceo\CeoBehavior;
use app\components\Entity;
use app\components\validators\EmptyContent;
use app\models\community\Counts;
use app\models\user\User;
use app\modules\forum\events\MoveTopicEvent;
use app\modules\forum\events\ThreadEvent;
use app\modules\forum\ForumModule;
use app\modules\notify\components\Event;
use himiklab\sitemap\behaviors\SitemapBehavior;
use Yii;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\SluggableBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * This is the model class for table "frm_topics".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $user_id
 * @property string $title
 * @property string $alias
 * @property integer $published
 * @property integer $content
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property FrmCategory $category
 */
class FrmTopic extends Entity
{
    public static $_pull = [];

    static $stringAttr = ['title', 'alias'];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'frm_topics';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class'         => SluggableBehavior::className(),
//                'attribute'     => 'title',
                'slugAttribute' => 'alias',
                'immutable'     => 'true',
                'ensureUnique'  => true,
                'value'         => function ($event) {
                    $category = FrmCategory::findOne($event->sender->category_id);
                    if (empty($event->sender->alias)) {
                        return $category->alias . '/' . Inflector::slug($event->sender->title);
                    }

                    $short_alias = end(explode('/', $event->sender->alias));

                    return ($event->sender->oldAttributes['category_id'] == $event->sender->category_id) ? $event->sender->alias : $category->alias . '/' . $short_alias;

                }
            ],
            [
                'class' => CeoBehavior::className(),
            ],
            [
                'class'       => SitemapBehavior::className(),
                'scope'       => function ($model) {
                    /** @var \yii\db\ActiveQuery $model */
                    $except_id = [];

                    $model->select(['frm_topics.alias', 'frm_topics.updated_at','frm_topics.created_at', 'frm_topics.id', 'frm_topics.category_id', 'frm_category.deleted']);
                    $model->joinWith('category');

                    foreach (FrmCategory::find()->all() as $cat) {
                        if ($cat->deleted == FrmCategory::DELETED) {
                            if (!in_array($cat->id, $except_id)) {
                                $except_id[] = $cat->id;
                            }
                            $childrens = FrmCategory::findOne($cat->id)->children()->all();
                            foreach ($childrens as $child) {
                                if (!in_array($child->id, $except_id)) {
                                    $except_id[] = $child->id;
                                }
                            }
                        }
                    }

                    $model->andWhere(['NOT IN', 'frm_category.id', $except_id]);
                    $model->andWhere(['NOT IN', 'frm_topics.category_id', $except_id]);
                    $model->andWhere(['frm_topics.deleted' => 0]);
                    $model->orderBy(['frm_topics.updated_at' => SORT_DESC]);

                },
                'dataClosure' => function ($model) {
                    /** @var self $model */

                    return [
                        'loc'        => Url::toRoute([$model->getViewUrl()]),
                        'lastmod'    => date('Y-m-d\TH:i:sP', $model->updated_at), date('Y-m-d\TH:i:sP', $model->created_at),
                        'changefreq' => SitemapBehavior::CHANGEFREQ_WEEKLY,
                        'priority'   => 0.9
                    ];
                }
            ],

            [
                'class'      => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_AFTER_FIND => 'count_comments',
                ],
                'value'      => function () {
                    return $this->getCountComments();
                }
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content'], EmptyContent::className()],

            [['content'], 'string', 'min' => 3],

            [['category_id', 'user_id', 'content', 'title'], 'required'],

            [['id', 'category_id', 'user_id', 'updated_at'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['created_at', 'updated_at'], 'default', 'value' => time()],
            [['published', 'alias', 'count_comments', 'created_at'], 'safe']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id'             => 'ID',
            'category_id'    => 'Category ID',
            'user_id'        => 'User ID',
            'title'          => 'Title',
            'alias'          => 'Alias',
            'content'        => 'Content',
            'count_comments' => 'Comments',
            'published'      => 'Published',
            'created_at'     => 'Created At',
            'updated_at'     => 'Updated At',
        ];
    }

    /**
     * Trigger event after save model
     * @param bool $insert
     * @param array $changedAttributes
     * @return bool
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            //New topic
            Yii::$app->trigger(ForumModule::EVENT_TOPIC_ADD, new Event(['sender' => new ThreadEvent(['topic_id' => $this->id])]));
        } else {
            //if topic has been moved
            if (isset($changedAttributes['category_id']) && $changedAttributes['category_id'] != $this->category_id) {
                $event = new MoveTopicEvent(['oldCategory_id' => $changedAttributes['category_id'], 'category_id' => $this->category_id, 'topic_id' => $this->id]);
                Yii::$app->trigger(ForumModule::EVENT_TOPIC_MOVE, new Event(['sender' => $event]));
                parent::afterSave($insert, $changedAttributes);

                return true;
            }
            //changed model
            Yii::$app->trigger(ForumModule::EVENT_TOPIC_EDIT, new Event(['sender' => new ThreadEvent(['topic_id' => $this->id])]));
        }
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(FrmCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCounts()
    {
        return $this->hasOne(Counts::className(), ['entity_id' => 'id'])->where([
            'entity' => $this->getTypeEntity()
        ]);
    }

    public function getCeo()
    {
        return $this->hasOne(Ceo::className(), ['entity_id' => 'id'])
            ->where(['entity' => $this->getTypeEntity()]);
    }

    public function getComments()
    {
        return $this->hasMany(FrmComment::className(), ['topic_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRedactor()
    {
        return $this->hasOne(User::className(), ['id' => 'update_user_id']);
    }

    /**
     * @inheritdoc
     * @return FrmTopicQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FrmCategoryQuery(get_called_class());
    }

    /**
     * @return array
     * calls 2 times from forum category and from topic
     */
    public static function getFreshDate()
    {
        return date(strtotime("-1 week"));
    }

    public function getFreshComment()
    {
        $created_at = null;
        $fresh_comment = null;

        foreach ($this->comments as $comment) {
            if (($comment->created_at > $created_at) && ($comment->deleted === Entity::NOT_DELETED)) {
                $created_at = $comment->created_at;
                $fresh_comment = $comment;
            }
        }

        return $fresh_comment;
    }

    public function checkUrlOnCategoryExists($short_alias, $full_alias)
    {
        $parent = $this->category->getAllParentAlias();
        $parentArr = explode("/", $parent);
        $array_parents_from_web = explode("/", str_replace($short_alias, '', $full_alias));

        if ($array_parents_from_web[0] == '') {
            throw new HttpException(404);//it will be short link, just alias for topic
        }

        foreach ($parentArr as $parent_alias) {
            if ($parent_alias != '')
                if (!in_array($parent_alias, $array_parents_from_web)) {
                    throw new HttpException(404);
                }
        }
    }

    public function getName()
    {
        return $this->title;
    }

    public function getCountComments()
    {
        $sql = "SELECT COUNT(*) FROM " . FrmComment::tableName() . ' where topic_id = ' . $this->id . ' AND deleted = ' . Entity::NOT_DELETED;

        return (int)Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public static function getGridColumns()
    {
        return [
            [
                'class'          => 'yii\grid\SerialColumn',
                'header'         => '<span>#</span>',
                'contentOptions' => [
                    'width' => 50
                ],
            ],

            [
                'attribute' => 'created_at',
                'format'    => 'date',
            ],

            [
                'attribute' => 'title',
                'format'    => 'raw',
                'value'     => function ($model) {
                    return '<a class="entity-link" href ="' . $model->getViewUrl() . '">' . $model->title . '</a>';
                },
            ],

            [
                'attribute' => 'category_id',
                'format'    => 'raw',
                'value'     => function ($model) {
                    return '<a class="category-link" href ="' . $model->category->getViewUrl() . '">' . $model->category->title . '</a>';
                },
            ],

            [
                'attribute' => 'user_id',
                'format'    => 'raw',
                'value'     => function ($model) {
                    if (is_object($model->user))
                        return $model->user->getLinkUsername($model->user->username);
                }
            ],

            [
                'attribute' => 'count_comments',
                'format'    => 'raw',
                'value'     => function ($model) {
                    return $model->getCountComments();
                },
            ],
            [
                'class'    => 'app\components\grid\ActionColumnCustom',
                'template' => function ($model) {
                    return '{edit} {delete}';
                },
                'buttons'  => [
                    'edit'   => function ($url, $model, $key, $options) {
                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/topic-edit', 'id' => $model->id]);

                        return Html::a('Edit', $url, $options);
                    },
                    'delete' => function ($url, $model, $key, $options) {
                        $options['onclick'] = '
                            if(confirm(\'' . Yii::t('yii', 'Are you sure you want to delete this Topic?') . '\'))
                            {
                              return true;
                            } else {
                              return false;
                            }
                        ';
//                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/topic-delete', 'id' => $model->id]);

                        return Html::a('Delete', $url, $options);
                    },
                ],
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getViewUrl()
    {
        return '/forum/' . $this->alias;
    }

    public function getTypeEntity()
    {
        return Entity::FORUM_TOPIC;
    }

    /**
     * @param $model
     * @return bool
     */
    public function changeAliasByCategoryModel($model)
    {
        $this->alias = $model->alias . '/' . end(explode('/', $this->alias));
        $this->save();

        $ceo = Ceo::findOne($this->ceo->id);
        $ceo->canonical = Yii::$app->urlManager->createAbsoluteUrl('forum/' . $this->alias);
        $ceo->save();
    }
}