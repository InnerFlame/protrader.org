<?php

namespace app\modules\forum\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * FrmCategorySearch represents the model behind the search form about `app\modules\forum\models\FrmCategory`.
 */
class FrmCategorySearch extends FrmCategory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'parent_id', 'weight', 'root', 'lft', 'rgt', 'level', 'published', 'deleted', 'created_at', 'updated_at'], 'integer'],
            [['title', 'alias', 'description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FrmCategory::find()->notDeleted()->notRoot()->newest();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'id'         => $this->id,
            'parent_id'  => $this->parent_id,
            'weight'     => $this->weight,
            'root'       => $this->root,
            'lft'        => $this->lft,
            'rgt'        => $this->rgt,
            'level'      => $this->level,
            'published'  => $this->published,
            'deleted'    => $this->deleted,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'alias', $this->alias])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
