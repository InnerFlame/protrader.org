<?php

namespace app\modules\forum\models;

/**
 * This is the ActiveQuery class for [[FrmComment]].
 *
 * @see FrmComment
 */
class FrmCommentQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return FrmComment[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return FrmComment|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
