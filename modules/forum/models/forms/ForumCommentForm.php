<?php
namespace app\modules\forum\models\forms;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class ForumCommentForm extends Model
{
    public $topic_id;

    public $comment;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['comment'], 'required'],
            [['comment'], 'string', 'max' => 3000],

        ];
    }

    public function attributeLabels()
    {
        return [
            'comment' => 'Comment'
        ];
    }

}
