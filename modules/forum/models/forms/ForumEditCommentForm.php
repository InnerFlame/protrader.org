<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 * Date: 02.02.2016
 * Time: 10:08
 * Form for Edit comment modal form. This form is required
 * for imperavi Widget. Imperavi Widget requirement unique
 * FormModel for every textarea
 * @see https://github.com/vova07/yii2-imperavi-widget#like-an-activeform-widget
 */

namespace app\modules\forum\models\forms;


class ForumEditCommentForm extends ForumCommentForm
{

}