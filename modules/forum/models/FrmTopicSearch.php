<?php

namespace app\modules\forum\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * FrmTopicSearch represents the model behind the search form about `app\modules\forum\models\FrmTopic`.
 */
class FrmTopicSearch extends FrmTopic
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'user_id', 'count_comments', 'status', 'published', 'deleted', 'created_at', 'updated_at', 'update_user_id'], 'integer'],
            [['content', 'title', 'alias'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FrmTopic::find()->notDeleted()->newest();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'id'             => $this->id,
            'category_id'    => $this->category_id,
            'user_id'        => $this->user_id,
            'count_comments' => $this->count_comments,
            'status'         => $this->status,
            'published'      => $this->published,
            'deleted'        => $this->deleted,
            'created_at'     => $this->created_at,
            'updated_at'     => $this->updated_at,
            'update_user_id' => $this->update_user_id,
        ]);

        $query->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'alias', $this->alias]);

        return $dataProvider;
    }
}
