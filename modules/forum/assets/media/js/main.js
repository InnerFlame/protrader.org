var forum = (function(){

    function Forum(){
        var self = this;
        this.edit = function(element){
            var item = $(element),
                topic_id = item.attr('data-name'),
                comment_id = item.attr('data-id');

            var form = $('#forum-edit-comment');
            $(form).find('#edit-comment-id').val(comment_id);
            $(form).find('#topic_id').val(topic_id);

            console.log(form);

            _showEditForm(topic_id, comment_id);

        }

        this.reply = function(){

        }


        function _showEditForm(topic_id, comment_id){
            $.ajax({
                type:"POST",
                url:'/ajax/getComment',
                data:{
                    comment_id:comment_id,
                    topic_id:topic_id
                },
                dataType:'json',
                success:function(response){
                    if(response.status == 'OK'){
                        //show modal window
                        var modalEdit = $('.modal#edit');
                        modalEdit.find('textarea').redactor('code.set', response.data.content);
                        modalEdit.modal('show');
                    }

                },
            });
        }
    }
    return new Forum();
}());