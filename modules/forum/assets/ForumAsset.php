<?php
/**
 * Created by PhpStorm.
 * User: writer
 * Date: 27.01.2016
 * Time: 14:59
 */
namespace app\modules\forum\assets;

use yii\web\AssetBundle;

class ForumAsset  extends AssetBundle
{
    public $sourcePath = '@app/modules/forum/assets/media';

    public $js = [
        'js/main.js?vr=0.1'
    ];

    public $css = [
        //'css/default.min.css'
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}