<?php
/**
 * Created by PhpStorm.
 * User: writer
 * Date: 26.01.2016
 * Time: 18:40
 */

namespace app\modules\forum\controllers;

use app\models\Constants;
use app\models\main\Pages;
use app\modules\forum\components\BaseForumController;
use app\modules\forum\models\FrmCategory;
use app\modules\forum\models\FrmTopic;
use Yii;
use yii\helpers\VarDumper;
use yii\web\HttpException;

class DefaultController extends BaseForumController
{
    /**
     * Return error twig array
     *
     * @return array
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'app\components\FlatErrorAction',
                'view' => 'error.twig'
            ],

            'image-upload' => [
                'class' => 'app\components\ImageUploadAction',
                'url' => '/upload/forum-comments', // Directory URL address, where files are stored.
                'path' => Yii::getAlias('@app') . '/web/upload/forum-comments' // Or absolute path to directory where files are stored.
            ],
            'file-upload' => [
                'class' => 'vova07\imperavi\actions\UploadAction',
                'url' => '/forum/default/file', // Directory URL address, where files are stored.
                'path' => Yii::getAlias('@app') . '/data/upload/forum-comments', // Or absolute path to directory where files are stored.
                'uploadOnlyImage' => false,
            ],
            'file-load' => [
                'class' => 'app\components\DownloadFileAction',
                'path' => Yii::getAlias('@app') . '/data/upload/forum-comments'
            ]
        ];
    }

    public function actionIndex()
    {
        $models = FrmCategory::getMain();

        return $this->render('index.twig',[
            'categories' => $models,
            'model' => Pages::getCeoCurrentPage(),
        ]);
    }

    public function actionCategory($alias)
    {
        $model = FrmCategory::find()->alias($alias)->notDeleted()->one();

        if (!is_object($model)) {
            throw new HttpException(404);
        }
        $this->addBreadCrumbs($model);

        Yii::$app->counter->setCount($model);

        return $this->render('/category/index.twig', [
            'model' => $model,
            'topic_model_add' => new FrmTopic(),
        ]);
    }


    public function actionSearch()
    {
        $models = Yii::$app->search->getCollection('title', Yii::$app->request->get('search'));

        return $this->render('search.twig',[
            'dataProvider' => $this->getDataProvider($models, Constants::SEARCH_COUNT_FRM_TOPIC),
        ]);
    }

    public function actionFile($filename)
    {
        $file = Yii::getAlias('@app') . '/web/upload/forum-comments/'.$filename;
        if(file_exists($file)){
            header('Content-Type: application/octet-stream');
            header('Accept-Ranges: bytes');
            header ("Content-Length: ".filesize($file));
            header ("Content-Disposition: attachment; filename=".$file);
            readfile($file);
        }
    }
}