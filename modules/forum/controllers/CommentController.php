<?php


namespace app\modules\forum\controllers;

use app\models\user\User;
use app\modules\forum\components\BaseForumController;
use app\modules\forum\events\CommentThreadEvent;
use app\modules\forum\events\ReplyEvent;
use app\modules\forum\ForumModule;
use app\modules\forum\models\FrmComment;
use app\modules\forum\models\FrmTopic;
use app\modules\notify\components\Event;
use Yii;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\HttpException;

class CommentController extends BaseForumController
{
    public function actionSave()
    {

        //new
        if(!isset($_POST['FrmComment']['id']) && !isset($_POST['FrmComment']['reply_id'])){
            $model = new FrmComment();
            $model->load(Yii::$app->request->post());
            $model->user_id = Yii::$app->user->identity->id;
            $model->published = date('d.m.Y');
            if($model->validate()){
                $model->makeRoot();
                Yii::$app->trigger(ForumModule::EVENT_COMMENT_ADD, new Event(['sender' => new CommentThreadEvent(['post_id' => $model->id])]));
                return $this->responseAllComments($model->topic_id);
            }
        }


        //edit
        if(isset($_POST['FrmComment']['id']) && $model = FrmComment::findOne($_POST['FrmComment']['id'])){
            $model->content = $_POST['FrmComment']['content'];
           if($model->save()){
               Yii::$app->trigger(ForumModule::EVENT_COMMENT_EDIT, new Event(['sender' => new CommentThreadEvent(['post_id' => $model->id])]));
                return $this->responseAllComments($model->topic_id);
            }
        }
        //reply
        if (isset($_POST['FrmComment']['reply_id']) && $_POST['FrmComment']['action'] == 'reply' &&
            $model = FrmComment::findOne($_POST['FrmComment']['reply_id'])
        ) {

            $username = '';

            if (is_object($model->user))
                $username = $model->user->getFullname();

            //Ответ на комментарий
            $reply_model = new FrmComment();

            $reply_model->load(Yii::$app->request->post());
            $reply_model->user_id = Yii::$app->user->identity->id;
            $reply_model->published = date('d.m.Y');
            $reply_model->topic_id = $model->topic_id;

            $reply_model->content = '';
            $reply_model->content .= '<i>' . $username . ' ' . date('d.m.Y', $model->updated_at) . '</i> ';
            $reply_model->content .= '<br><blockquote>';
            $reply_model->content .= $reply_model->reply_content;
            $reply_model->content .= '</blockquote>';
            $reply_model->content .= $_POST['FrmComment']['content'];


            if($reply_model->validate()){
                $reply_model->makeRoot();
                Yii::$app->trigger(ForumModule::EVENT_COMMENT_ADD, new Event(['sender' => new CommentThreadEvent(['post_id' => $reply_model->id])]));
                return $this->responseAllComments($model->topic_id);
            }
        }
        //quote from Topic
        if (isset($_POST['FrmComment']['reply_id']) && $_POST['FrmComment']['action'] == 'quote' &&
            $model = FrmTopic::findOne($_POST['FrmComment']['reply_id'])
        ) {
            $username = '';

            if (is_object($model->user))
                $username = $model->user->getFullname();

            //Ответ на комментарий
            $reply_model = new FrmComment();

            $reply_model->load(Yii::$app->request->post());
            $reply_model->user_id = Yii::$app->user->identity->id;
            $reply_model->published = date('d.m.Y');
            $reply_model->topic_id = $model->id;

            $reply_model->content = '';
            $reply_model->content .= '<i>' . $username . ' ' . date('d.m.Y', $model->updated_at) . '</i> ';
            $reply_model->content .= '<br><blockquote>';
            $reply_model->content .= $reply_model->reply_content;
            $reply_model->content .= '</blockquote>';
            $reply_model->content .= $_POST['FrmComment']['content'];

            if($reply_model->validate()){
                $reply_model->makeRoot();
                Yii::$app->trigger(ForumModule::EVENT_COMMENT_ADD, new Event(['sender' => new CommentThreadEvent(['post_id' => $reply_model->id])]));
                return $this->responseAllComments($reply_model->topic_id);
            }
        }


        //
        throw new HttpException(400);
    }

    /**
     * Генерирует и возвращает форму для обработки ответа на сообщение
     * @return string
     * @throws HttpException
     */
    public function actionReply()
    {
        if(!Yii::$app->request->isAjax)
            throw new HttpException(404);

        $model = new FrmComment();
        if(!$reply_model = FrmComment::findOne((int)$_POST['id']))
            throw new HttpException(400);

        $model->reply_id = $reply_model->id;

        $username = '';

        if (is_object($model->user))
            $username = $model->user->getFullname();


        if (Yii::$app->request->post('selected_text'))
            $model->reply_content = strip_tags(Yii::$app->request->post('selected_text'), '<blockquote><br><i><a><p><strong><em>');
        else
            $model->reply_content = $reply_model->content;

        $user_model = User::findOne($reply_model->user_id);

        return $this->renderAjax('_modal_form.twig', [
            'model' => $model,
            'user' => $user_model ? $user_model : null
        ]);
    }

    /**
     * Генерирует и возвращает форму для обработки ответа на Topic
     * @return string
     * @throws HttpException
     */
    public function actionQuote()
    {
        if(!Yii::$app->request->isAjax)
            throw new HttpException(404);

        if(!$reply_model = FrmTopic::find()->where(['id' => (int)$_POST['id']])->one())
            throw new HttpException(400);

        $model = new FrmComment();
        $model->reply_id = $reply_model->id;

        $username = '';

        if (is_object($model->user))
            $username = $model->user->getFullname();


        if (Yii::$app->request->post('selected_text'))
            $model->reply_content = strip_tags(Yii::$app->request->post('selected_text'), '<blockquote><br><i><a><p><strong><em>');
        else
            $model->reply_content = $reply_model->content;

        $user_model = User::findOne($reply_model->user_id);

        return $this->renderAjax('_modal_form.twig', [
            'model' => $model,
            'user' => $user_model ? $user_model : null
        ]);
    }

    public function actionAnswer()
    {
        if (!Yii::$app->request->isAjax)
            throw new HttpException(404);

        $model = new FrmComment();

        if (!$reply_model = FrmComment::findOne((int)$_POST['id']))
            throw new HttpException(400);

        $model->reply_id = $reply_model->id;

        $user_model = User::findOne($reply_model->user_id);


        return $this->renderAjax('_modal_form.twig', [
            'model' => $model,
            'user' => $user_model ? $user_model : null
        ]);
    }

    public function actionDelete($id)
    {

        if(!$id > 0) throw new \yii\web\BadRequestHttpException();

        $model = FrmComment::findOne((int) $id);

        if(!$model) throw new \yii\web\BadRequestHttpException();

        if(\Yii::$app->user->isGuest){
            Yii::$app->session->setFlash('warning', 'You can not delete the message. You are not authorized');
            return $this->redirect($model->getViewUrl());
        }
        if ((Yii::$app->user->identity->id === $model->user_id) || Yii::$app->user->identity->isAdmin()) {

            if ($model->delete(Yii::$app->user->identity->id))
                Yii::$app->session->setFlash('success', 'Message was successfully deleted');
            else
                Yii::$app->session->setFlash('warning', 'Message was not successfully deleted');
        }else{
            Yii::$app->session->setFlash('warning', 'You can\'t delete not your comment');
        }
        return $this->redirect($model->getViewUrl());
    }

    /**
     * Генерирует и возвращает форму  для редактирования сообщения
     * @todo проверку на то, что бы редактировать мог либо админ, либо владелец
     * @return string
     * @throws HttpException
     */
    public function actionEdit()
    {
        if(!Yii::$app->request->isAjax)
            throw new HttpException(404);

        $model = FrmComment::findOne((int)$_POST['id']);

        return $this->renderAjax('_modal_form.twig', [
            'model' => $model,
        ]);
    }


    protected function responseAllComments($topic_id)
    {
        $comments = FrmComment::getAllTreeCommentsByPostId($topic_id);
        $output = null;
        foreach($comments as $comment){
            $output .= $this->renderPartial('/topic/_comment', [
                'comment' => $comment
            ]);
        }
        return $output;
    }
    /**
     * @return string to last full url link
     */
    protected function getReturlUrl()
    {
        return Yii::$app->request->referrer;
    }
}