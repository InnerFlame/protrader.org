<?php
/**
 * Created by PhpStorm.
 * User:  writer
 * Date: 27.01.2016
 * Time: 11:08
 */

namespace app\modules\forum\controllers;

use app\modules\forum\components\BaseForumController;
use app\modules\forum\events\AddThreadEvent;
use app\modules\forum\events\ThreadEvent;
use app\modules\forum\ForumModule;
use app\modules\forum\models\forms\ForumEditCommentForm;
use app\modules\forum\models\forms\ForumReplyCommentForm;
use app\modules\forum\models\forms\ReplyCommentForm;
use app\modules\forum\models\FrmCategory;
use app\modules\forum\models\FrmComment;
use app\modules\forum\models\FrmTopic;
use app\modules\notify\components\Event;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\web\HttpException;

class TopicController extends BaseForumController
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'app\components\FlatErrorAction',
                'view' => 'error.twig'
            ],
            'image-upload' => [
                'class' => 'app\components\ImageUploadAction',
                'width' => 650,
                'url' => '/upload/forum-topic', // Directory URL address, where files are stored.
                'path' => Yii::getAlias('@app') . '/web/upload/forum-topic' // Or absolute path to directory where files are stored.
            ],
        ];
    }

    /**
     * Show on topic by alias
     */
    public function actionIndex($alias)
    {
        $topic = FrmTopic::find()->alias($alias)->notDeleted()->one();

        if (!is_object($topic))
            throw new HttpException(404);

        $this->addBreadCrumbs($topic->category);
        Yii::$app->counter->setCount($topic);

        $this->breadcrumbs[] = [
            'label'  => $topic->category->title,
            'url'    => $topic->category->getViewUrl(),
            'active' => 'active',
        ];

        $forum_comment_form = new FrmComment();
        $forum_comment_form->topic_id = $topic->id;

        return $this->render('index.twig', [
            'model'         => $topic,
            'comments'      => FrmComment::getAllTreeCommentsByPostId($topic->id),
            'forum_comment' => $forum_comment_form,
            'reply_form'    => new ForumReplyCommentForm(),
            'edit_form'     => new ForumEditCommentForm()
        ]);
    }

    /**
     * @return array|\yii\web\Response
     */
    public function actionAdd()
    {
        if (isset($_POST['FrmTopic']['id'])) {
            $model = FrmTopic::findOne($_POST['FrmTopic']['id']);
        }

        if (!isset($model))
            $model = new FrmTopic();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if(isset($_POST['FrmTopic']['category_id'])){
            $category = FrmCategory::findOne($_POST['FrmTopic']['category_id']);

            if ($model->load(Yii::$app->request->post())){
                if($model->isNewRecord) {
                    $model->user_id = Yii::$app->user->id;
                    $model->update_user_id = Yii::$app->user->id;
                    $model->updated_at = time();
                }else{
                    $model->update_user_id = Yii::$app->user->id;
                    $model->updated_at = time();
                }
                if($model->save()) {
                    return $this->redirect($model->getViewUrl());
                }else{
                    static::setFlashWithErrors($model->getErrors());
                    return $this->redirect(Yii::$app->request->referrer);
                }
            }
            return $this->redirect($this->indexUrl . $category->id);
        }
        return $this->redirect($this->indexUrl);
    }

    public function actionDelete($id)
    {
        if (!$id > 0) throw new \yii\web\BadRequestHttpException();


        $model = FrmTopic::findOne((int)$id);
        $return_url = $model->category->getViewUrl();

        if(\Yii::$app->user->isGuest || ($model->user_id != \Yii::$app->user->getId() && !\Yii::$app->user->identity->isAdmin() )){
            \Yii::$app->session->setFlash('success', \Yii::t('app', 'You can\'t delete not your topic'));
            return $this->redirect($model->getViewUrl());
        }

        if (!$model) throw new \yii\web\BadRequestHttpException();

        if ((Yii::$app->user->identity->id === $model->user_id) || Yii::$app->user->identity->isAdmin()) {

            if ($model->delete(Yii::$app->user->identity->id)){
                Yii::$app->session->setFlash('success', 'Topic was successfully deleted');
                Yii::$app->trigger(ForumModule::EVENT_TOPIC_DELETED,  new Event(['sender' => new ThreadEvent(['topic_id' => $model->id])]));
            } else {
                Yii::$app->session->setFlash('warning', 'Topic was not successfully deleted');
            }
        }
        return $this->redirect($return_url);
    }
}