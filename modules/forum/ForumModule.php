<?php
/**
 * Created by PhpStorm.
 * User: writer
 * Date: 26.01.2016
 * Time: 18:39
 */

namespace app\modules\forum;

use app\components\Application;
use app\modules\forum\models\FrmComment;
use app\modules\forum\models\FrmTopic;
use app\modules\notify\components\Subscribe;
use yii\base\BootstrapInterface;
use yii\base\Event;
use yii\base\Module;
use yii\db\ActiveRecord;
use yii\db\AfterSaveEvent;

class ForumModule extends Module implements BootstrapInterface
{
    const EVENT_COMMENT_ADD = 'forum.comment.add';

    const EVENT_COMMENT_ADD_TO_AUTHOR = 'forum.comment.add.role.author';

    const EVENT_COMMENT_EDIT = 'forum.comment.edited';

    const EVENT_COMMENT_DELETE = 'forum.comment.deleted';

    const EVENT_COMMENT_REPLY = 'forum.comment.reply';

    const EVENT_TOPIC_ADD = 'forum.topic.add';

    const EVENT_TOPIC_DELETED = 'forum.topic.deleted';

    const EVENT_TOPIC_EDIT = 'forum.topic.edited';

    const EVENT_TOPIC_MOVE = 'forum.topic.move';

    public $controllerNamespace = 'app\modules\forum\controllers';

    public function init(){
        \Yii::configure($this, require(dirname(__FILE__) . '/config/main.php'));
    }

    public function bootstrap($app)
    {
        // TODO: Implement bootstrap() method.
        \Yii::$app->urlManager->addRules(
            [
                'forum/topic-add'                  => 'forum/topic/add', # * http://protrader.my/forum/topic/new-topic
                'forum/image-upload'               => 'forum/default/image-upload', # * http://protrader.my/forum/topic/new-topic
                'forum/topic/image-upload'         => 'forum/topic/image-upload', # * http://protrader.my/forum/topic/new-topic
                'forum/file-upload'                => 'forum/default/file-upload', # * http://protrader.my/forum/topic/new-topic
                'forum/default/file/<filename:.+>' => 'forum/default/file-load', # * http://protrader.my/forum/topic/new-topic
                'forum/category/<alias:.+>'        => 'forum/default/category',  # * http://protrader.my/forum/third
                'forum/comment/add'                => 'forum/comment/add',     # * http://protrader.my/forum/comment/add
                'forum/comment/save'               => 'forum/comment/save',     # * http://protrader.my/forum/comment/save
                'forum/comment/edit'               => 'forum/comment/edit',     # * http://protrader.my/forum/comment/save
                'forum/comment/reply'              => 'forum/comment/reply',     # * http://protrader.my/forum/comment/save
                'forum/comment/answer'             => 'forum/comment/answer',     # * http://protrader.my/forum/comment/answer
                'forum/comment/delete'             => 'forum/comment/delete',     # * http://protrader.my/forum/comment/save
                'forum/comment/quote'              => 'forum/comment/quote',     # * http://protrader.my/forum/topic/quote
                'forum/topic/delete'               => 'forum/topic/delete',     # * http://protrader.my/forum/topic/new-topic
                'forum/<alias:.+>'                 => 'forum/topic',     # * http://protrader.my/forum/topic/new-topic
            ]);
        //Подписать на события, пользователя, если он создал новый комментарий
        //или новый топик
        if($app instanceof Application){
            if($app->getModule('notify')){
                Event::on(FrmTopic::className(), ActiveRecord::EVENT_AFTER_INSERT, function(AfterSaveEvent $event)use($app){
                    $app->getModule('notify')->subscriber->subscribe($event->sender, \Yii::$app->user->identity->id);
                });
                Event::on(FrmComment::className(), ActiveRecord::EVENT_AFTER_INSERT, function(AfterSaveEvent $event)use($app){
                    $app->getModule('notify')->subscriber->subscribe($event->sender, \Yii::$app->user->identity->getId());

                    //Если добавлен новый комментарий, то автоматически подписать на сущность самого топика
                    $app->getModule('notify')->subscriber->subscribe($event->sender->topic, \Yii::$app->user->identity->getId());
                });
            }
        }
    }
    public function trigger($name, Event $event = null)
    {
        return \Yii::$app->trigger($name, $event);
    }

}