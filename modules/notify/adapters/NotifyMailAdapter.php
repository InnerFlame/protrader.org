<?php
/**
 * Created by PhpStorm.
 * User: Serg
 * Date: 24.05.2016
 * Time: 13:31
 */

namespace app\modules\notify\adapters;


use app\models\user\User;
use app\modules\notify\components\Notification;

class NotifyMailAdapter extends Notification
{
    /**
     * @var Notification
     */
    private $_notify;

    private $_user;

    public function __construct(User $user, Notification $notify,  array $config = [])
    {
        parent::__construct($config);
        $this->_user = $user;
        $this->_notify = $notify;
    }

    public function getTitle()
    {
        return $this->_notify->getTitle();
    }

    public function getDescription()
    {
        return $this->_notify->getDescription();
    }

    public function getAuthor()
    {
        return $this->_notify->getAuthor();
    }

    public function getEntity()
    {
        return $this->_notify->getEntity();
    }

    public function getUserName()
    {
        return $this->_user->getFullname();
    }


}