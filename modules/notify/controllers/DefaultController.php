<?php

namespace app\modules\notify\controllers;

use app\models\user\UserSettings;
use app\modules\cabinet\modules\notify\models\NotifyTemplate;
use app\modules\notify\models\EventMessage;
use app\modules\notify\models\EventUserSettings;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class DefaultController extends Controller
{
    /**
     * Делает тоже самое что и self::actionCheck.
     * После компрессии скриптов notify/assets/js
     * URL для обновления ведет, почему-то на index экшен
     * вместо check.
     * @todo решить проблму с компрессией, убрать дублирование
     * @return array
     */
    public function actionIndex()
    {
        return $this->updateMessaes();
    }

    /**
     * Проверяет наличие новых нотифкаций
     * @return array
     */
    public function actionCheck()
    {
        return $this->updateMessaes();
    }

    /**
     * Включает/выключает отци доставки оповещений
     * для текущего пользователя
     */
    public function actionSetting()
    {
        $event_id = (int)$_GET['event_id'];
        $attribute = $_GET['type'];
        $param = EventUserSettings::findOne($event_id);

        if($param && $param->hasAttribute($attribute)){
            if($param->user_id == \Yii::$app->user->identity->id) {
                $param->turn($attribute);
            }
        }
    }

    /**
     * Пометить все как прочитанные
     * @return string
     */
    public function actionRead()
    {
        EventMessage::updateAll([
            'read' => EventMessage::READ_YES
        ], 'user_id = :user_id', [':user_id' => \Yii::$app->user->identity->id]);

        return $this->redirect(\Yii::$app->request->referrer);
    }

    /**
     * Помечает уведомление как прочитанное. Если передан
     * массив идентификторов, то отмечает их все и перенаправляет
     * на Entity
     * @param $id int идентификатор уведомления
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = EventMessage::find()->where(['id' => $id])->one();

        if(!$model){
            throw new NotFoundHttpException();
        }
        if($model->read == EventMessage::READ_YES)
        {
            return $this->redirect($model->getNotify()->getEntity()->getViewUrl());
        }
        try{
            $messages = EventMessage::find()->where(['read' => EventMessage::READ_NO])
                ->andWhere(['entity' => $model->entity])
                ->andWhere(['entity_id' => $model->entity_id])
                ->andWhere(['user_id' => \Yii::$app->user->getId()])
                ->andWhere(['event_id' => $model->event_id])->all();
            foreach($messages as $message){
                $message->read = EventMessage::READ_YES;
                $message->save();
            }
            return $this->redirect($model->getNotify()->getEntity()->getViewUrl());

        }catch (\Exception $e){
            \Yii::error(sprintf("Не удалось пометить сообщения как прочитанные. Сообщение: %d. Ошибка: %s", $id, $e->getMessage()));
            return $this->redirect($model->getNotify()->getEntity()->getViewUrl());
        }
    }

    private function updateMessaes()
    {
        $result = EventMessage::getAllMessagesByUserId(\Yii::$app->user->getId());

        $html = $this->renderPartial('@app/modules/notify/widgets/views/_bell_item.twig', ['count' => $result['count'], 'messages' => $result['messages']]);

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return ['count' => $result['count'], 'html' => $html];
    }
}
