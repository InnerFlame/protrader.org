<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2016
 */

namespace app\modules\notify\console;

use app\models\user\User;
use app\modules\notify\adapters\NotifyMailAdapter;
use app\modules\notify\components\Event;
use app\modules\notify\models\EventMessage;
use app\modules\notify\models\EventQueue;
use app\modules\notify\models\EventSubscribe;
use app\modules\notify\models\EventUserSettings;
use yii\console\Controller;
use yii\helpers\ArrayHelper;

/*
 * 1. Получить всех модеров, кроме автора события, если он модер
 * 2. Получить всех пользователей, кто подписан на текущуюю сущность, кроме автора события (важное замечание: подписался раньше, чем было сгенерировано событие)
 */

class HandlerController extends Controller
{


    public function actionHandle()
    {
        $events = EventQueue::find()->all();
        if (count($events) <= 0) {
            printf("%d processed events", count($events));
            return false;
        }

        $moderators = User::findAllModerators();
        $ids = ArrayHelper::getColumn($moderators, 'id');

        \Yii::info('[Отправка уведомлений]', 'notify');
        foreach ($events as $event) {
            $exclude = $ids;
            //Исключить из модераторов отправителя события, что бы ему не пришло уведомление
            //о том, что он сам и создал событие
            $key = array_search($event->getNotify()->getSenderUserId(), $exclude);
            if ($key !== false) {
                unset($exclude[$key]);
                $exclude = array_values($exclude);
            }
            if($event->getNotify()->mail){
                //Исключить из модераторов получателя события, что бы ему пришло уведомление
                //о том, что лично ему ответили. Если он при этом модератор
                $key = array_search($event->getNotify()->attributes['user_id'], $exclude);
                if ($key !== false) {
                    unset($exclude[$key]);
                    $exclude = array_values($exclude);
                }
            }
            if(!$event->getNotify()->getEntity()){
                continue;
            }
            $event->getNotify()->addIgnoredUsers($exclude);
            $event->push();

            printf("Event name: %s processed. Notify subscribed: %s, mail: %s Queue id: %s\n", $event->event->name, $event->getNotify()->subscribed ? 'true' : 'false', $event->getNotify()->mail ? 'true' : 'false', $event->id);
        }

        $count = EventQueue::deleteAll();
        \Yii::info("[Removed on queue events:  {$count}]", 'notify');
    }

    /**
     * Console command to notify subscribers to mail.
     * If send mail is success, set read marker.
     *
     * @return bool
     */
    public function actionMail()
    {
        $messages = EventMessage::find()
            ->with('emailTemplate', 'user')
            ->innerJoin(EventUserSettings::tableName() . ' settings', 'settings.user_id = notify_messages.user_id AND settings.event_id = notify_messages.event_id AND push_email = 1')
            ->where(['read' => 0, 'sent' => 0])
            ->orderBy(['created_at' => SORT_ASC])
            ->limit(10)
            ->all();

        foreach ($messages as $message) {
            if (!$this->mail($message)) {
                return false;
            }
        }
        $this->update($messages);
    }

    /**
     * @param $message
     * @return bool
     */
    public function mail(EventMessage $message)
    {
        $notify = $message->getNotify();
        $adapter = new NotifyMailAdapter($message->user, $notify);
        $msg = $this->renderPartial('@app/views/mail/layouts/notify.twig', [
            'title' => $message->emailTemplate->getTitle($adapter),
            'body'  => $message->emailTemplate->getBody($adapter)
        ]);

        \Yii::$app->mailer->compose()
            ->setFrom(isset(\Yii::$app->params['adminEmail']) ? \Yii::$app->params['adminEmail'] : '')
            ->setTo($message->user->email)
            ->setSubject($message->emailTemplate->getTitle($adapter))
            ->setHtmlBody($msg)
            ->send();

        echo 'Send email to: ' . $message->user->email . PHP_EOL;

        return true;
    }

    /**
     * @param $messages
     */
    public function update($messages)
    {
        EventMessage::updateAll([
            'sent'           => 1,
            'departure_date' => time()
        ], ['in', 'id', ArrayHelper::getColumn($messages, 'id')]);
    }
}
