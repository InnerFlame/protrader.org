<?php

namespace app\modules\notify\console;

use app\modules\cabinet\modules\notify\models\NotifyTemplate;
use yii\console\Controller;

class SettingsController extends Controller
{
    /**
     * @param $alias
     * @return bool
     */
    public function actionSetUp($alias)
    {
        $path = \Yii::getAlias($alias);
        if(!file_exists($path)){

            \Yii::info(sprintf('File %s not exists in path: %s.', $alias, $path), 'notify');
           return false;
        }
        $settings = require $path;
        foreach($settings as $name => $set){
            if($event = $this->addEvent($name, $set['label'])){
                foreach($set['templates'] as $type => $template){
                    $this->addTemplate($event->id, $type, $template);
                }
            }
        }
    }

    /**
     * Show path example
     */
    public function actionHelp()
    {
        print "yii user-settings/set-up \"@app/modules/<modulename>/settings.php\"";
    }

    /**
     * @param $name
     * @param $label
     * @return bool | \app\modules\notify\models\Event
     */
    protected function addEvent($name, $label)
    {
        $model = new \app\modules\notify\models\Event();
        $model->name = $name;
        $model->label = $label;
        $model->created_at = time();

        if($model->save()){
            return $model;
        }
        \Yii::info(sprintf('Error saved event: %s . Errors: %s', $name, json_encode($model->getErrors())), 'notify');
        return false;
    }

    /**
     * @param $event_id
     * @param $type
     * @param $template
     * @return bool
     */
    protected function addTemplate($event_id, $type, $template)
    {
        $model = new NotifyTemplate();
        $model->event_id = $event_id;
        $model->type = $type;
        $model->tpl_title = $template['title'];
        $model->tpl_body = $template['body'];

        return $model->save();
    }
}
