<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 * Date: 15.02.2016
 * Time: 17:45
 */

namespace app\modules\notify\widgets;


use app\modules\notify\models\EventMessage;
use yii\bootstrap\Widget;

class ListNotifications extends Widget
{
    public function run()
    {
        $messages = EventMessage::find()->with(['user','sender', 'event', 'templates'])->where('user_id = :user_id AND `read` = :read',
            ['user_id' => \Yii::$app->getUser()->id, ':read' => EventMessage::READ_NO])->orderBy(['created_at' => SORT_DESC])->all();
        return $this->render('list', ['items' => $messages]);
    }

}