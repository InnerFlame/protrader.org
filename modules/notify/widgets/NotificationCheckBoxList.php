<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 * Date: 11.02.2016
 * Time: 9:24
 */

namespace app\modules\notify\widgets;


use app\modules\notify\models\Event;
use app\modules\notify\models\EventUserSettings;
use yii\base\Widget;
use yii\helpers\VarDumper;

class NotificationCheckBoxList extends Widget
{

    /**
     * @var \yii\bootstrap\ActiveForm
     */
    public $form;

    /**
     * @var \app\models\user\User
     */
    public $model;


    /**
     * Получает список включенных для управления пользователем
     * событий. Управление подразумевает переключения чекбоксов
     * на странице профиля пользователя. Если пользователь админ
     * то для него доступны дополнительные чекбоксы, которые так
     * же могут быть включены в админке.
     * @return string
     */
    public function run()
    {
        $query = EventUserSettings::find()->joinWith('event')->where([
            'notify_user_settings.enabled' => Event::ENABLED,
            'notify_user_settings.user_id' => \Yii::$app->user->identity->id
        ]);

        if(!\Yii::$app->user->identity->isAdmin()){
            $query = $query->andWhere(['notify_events.enabled' => Event::ENABLED]);
        }

        $settings = $query->all();
        return $this->render('settings.twig', ['settings' => $settings]);
    }
}