<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 */

namespace app\modules\notify\widgets;


use app\modules\notify\models\EventMessage;
use yii\bootstrap\Widget;

class BellDropDown extends Widget
{
    public $mobile = false;

    private $_items = [];

    public function run()
    {
        $layout = $this->mobile ? 'mob_bell.twig' : 'bell.twig';
        if(!$this->_items){
            $this->_items = EventMessage::getAllMessagesByUserId(\Yii::$app->user->getId());
        }
        return $this->render($layout, $this->_items);
    }
}