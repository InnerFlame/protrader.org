$(document).ready(function(){

    setInterval(function(){
        $.ajax({
            url:'/notify/check',
            cache:false,
            success:function(response){
                var count = response.count;
                var html = response.html;
                if(count > 0){
                    $('.fa-bell').addClass('active');
                    $('#notify-counter').html(count);
                    $('.notifications li.body').html(html);
                    $('.notifications .footer a').show();
                    $('.notifications #mark-all-read').show();
                    $('.notifications .body').mCustomScrollbar('update');
                    return true;
                }
                if($('.fa-bell').hasClass('active')){
                    $('.fa-bell').removeClass('active');
                    $('#notify-counter').html('');
                    $('.notifications li.body').html('<div class="noNot">No updates...</div>');
                    $('.notifications .footer a').hide();
                    $('.notifications #mark-all-read').hide();
                }
            }
        });
    }, 10000);

    //turn notifications tab after load edit profile page
    var url = document.location.toString();
    if (url.match('#')) {
        var hash = url.split('#')[1];
        $('.nav-tabs a[href="#'+hash+'"]').tab('show');
    }
    //change hash in url, on click.
    $('.nav-tabs a').on('shown.bs.tab', function (e) {
        window.location.hash = e.target.hash;
    })
});