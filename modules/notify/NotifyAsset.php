<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 */

namespace app\modules\notify;


use yii\web\AssetBundle;

class NotifyAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/notify/assets/js';

    public $js = [
        'notify.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];
}