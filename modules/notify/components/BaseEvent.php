<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2016
 */

namespace app\modules\notify\components;


abstract class BaseEvent extends \yii\base\Event
{
    const TYPE_FREE = 'free';

    const TYPE_MAIL = 'mail';

    const TYPE_SUBSCRIBED = 'subscribed';

    public $type;

    /**
     * Return sender of Notification
     * @return Notification
     */
    public function getNotify()
    {
        if($this->sender instanceof Notification)
            return $this->sender;

    }

}