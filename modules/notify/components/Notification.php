<?php
/**
 * Базовый класс нотификации, который возвращает
 * который обязует реализовать методы необходимые для шаблонизатора
 * @see app\modules\cabinet\modules\notify\models\NotifyTemplate
 * Class Notification
 * @package app\modules\notify\components
 * User: P.Bilik
 * Date: 20.04.2016
 * Time: 13:05
 */

namespace app\modules\notify\components;

use app\models\user\User;
use yii\base\Object;
use yii\console\Application;

abstract class Notification extends Object
{
    /**
     * @var bool данная нотификация будет доставленна только тем кто подписан на сущность
     */
    public $subscribed = true;


    /**
     * @var bool если true то нотификация будет доставлена только администраторам
     */
    public $onlyAdmins = false;

    /**
     * @var bool данная нотификация будет доставленна только одному, конкретному человеку
     */
    public $mail = false;

    /**
     * @var array атрибуты, которые могут быть добавленны для нотификации
     */
    public $attributes = [];

    /**
     * @var int|string ID пользователя который сгенерировал
     */
    public $user_id;

    /**
     * @var int дата создания в формате unix
     */
    protected $date;


    /**
     * @var array массив пользователей которые должны быть проигнорированы
     */
    protected $ignoredUsers = [];




    abstract public function getTitle();


    abstract public function getDescription();


    abstract public function getAuthor();


    public function __construct($config = [])
    {
        $this->date = time();

        $this->user_id = \Yii::$app->user->getId();
        parent::__construct($config);
    }


    public function getLink()
    {
        if(!$this->getEntity()){
            return;
        }
        if (\Yii::$app instanceof Application) {
            return \Yii::$app->params['host'] . $this->getEntity()->getViewUrl();
        }
        return \Yii::$app->urlManager->createAbsoluteUrl($this->getEntity()->getViewUrl());
    }

    /**
     *
     * @return \app\components\Entity
     */
    abstract public function getEntity();



    public function getDate($pattern = 'm.d.Y')
    {
        return date($pattern, $this->date);
    }

    /**
     * Return date and time with string format.
     * Today at 18:50 or Yesterday at 18:50 or
     * 17.04.2015 at 18:50
     * @return string
     */
    public function getDateTime()
    {
        $date = date('d/m/Y', $this->date);

        if ($date == date('d/m/Y')) {
            return 'Today at ' . date('H:i', $this->date);
        } else if ($date == date('d/m/Y', strtotime('yesterday'))) {
            return 'Yesterday at ' . date('H:i', $this->date);
        }
        return date('d.m.Y', $this->date) . ' at ' . date('H:i', $this->date);
    }

    /**
     * Возвращает ID пользователя-отправителя
     * Того, кто инициировал событие
     * @return int
     */
    public function getSenderUserId()
    {
        return $this->user_id;
    }

    /**
     * Возвращает полное ися пользователя-отправителя.
     * Того кто инициировал событие
     * @return string
     */
    public function getUserName()
    {
        return \Yii::$app->user->identity->getFullName();
    }

    /**
     * Return sender of Notification
     * @return Notification
     */
    public function getNotify()
    {
        if($this->sender instanceof Notification)
            return $this->sender;

    }

    /**
     * Добавляет специальных пользователей,
     * которые будут проигнороированы. Бывает удобно
     * когда отправляется два события одновременно, одно
     * для всех, другое специальное для конкретного пользователя
     * в этом случае, удобно, в общем указать игнорирование
     * для этого пользователя.
     * @param $user_id
     */
    public function addIgnoredUser($user_id)
    {
        if(!in_array($user_id, $this->ignoredUsers)){
            $this->ignoredUsers[] = $user_id;
        }
    }

    /**
     * Добавляет сразу массив новых пользователей в игнор,
     * исклюая при этом дубликаты
     * @param $users [1, 15, 18, 20]
     */
    public function addIgnoredUsers($users)
    {
        if(is_array($users) && !empty($users)){
            $result = array_merge($this->ignoredUsers, $users);
            $this->ignoredUsers = array_unique($result);
        }
    }

    /**
     * Возвращает массив пользователей, которые находятся в игноре.
     * @return array массив идентификаторов [23, 11, 24..,95]
     */
    public function getIgnoredUsersList()
    {
        return $this->ignoredUsers;
    }
}