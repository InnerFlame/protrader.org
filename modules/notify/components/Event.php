<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2016
 */

namespace app\modules\notify\components;


/**
 * Обычное уведомление, которое рассылается по происхождении
 * какии либо событий:добавление комментариев, топиков, статей.
 * Высылается тем кто подписан на сущности, которые генерируют
 * такие события
 * Class Event
 * @package app\modules\notify\components
*/
class Event extends BaseEvent
{
    public function getType()
    {
        return self::TYPE_SUBSCRIBED;
    }
}