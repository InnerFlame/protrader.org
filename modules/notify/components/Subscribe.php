<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2016
 */

namespace app\modules\notify\components;


use app\components\Entity;
use app\modules\notify\models\EventSubscribe;
use yii\base\Component;

class Subscribe extends Component
{

    /**
     * @param Entity $entity
     * @param $user_id
     * @return bool
     */
    public function subscribe(Entity $entity, $user_id)
    {
        if(!$this->getSubscribe($entity, $user_id)){
            $subscribe = new EventSubscribe();
            $subscribe->entity = $entity->getTypeEntity();
            $subscribe->entity_id = $entity->id;
            $subscribe->user_id = $user_id;
            return $subscribe->save();
        }
        return false;
    }

    /**
     * @param Entity $entity
     * @param $user_id
     * @return false|int
     * @throws \Exception
     */
    public function unsubscribe(Entity $entity, $user_id)
    {
        $subscribe = $this->getSubscribe($entity, $user_id);
        if($subscribe){
            return $subscribe->delete();
        }
        return false;
    }

    /**
     * Попытаться получить объект подписки
     * @param Entity $entity
     * @param $user_id
     * @return EventSubscribe|array|null
     */
    public function getSubscribe(Entity $entity, $user_id)
    {
        $subscribe = EventSubscribe::find()->where('entity = :entity AND entity_id = :entity_id AND user_id = :user_id', [
            ':entity' => $entity->getTypeEntity(),
            ':entity_id' => $entity->id,
            ':user_id' => $user_id
        ])->one();
        return $subscribe;
    }
}