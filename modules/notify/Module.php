<?php

namespace app\modules\notify;

use app\components\Application;
use app\components\helpers\ArrayHelper;
use app\components\helpers\Serialize;
use app\events\user\RoleChangedEvent;
use app\models\user\User;
use app\modules\notify\NotifyAsset;
use app\modules\notify\components\BaseEvent;
use app\modules\notify\components\Event;
use app\modules\notify\components\Notification;
use app\modules\notify\models\Event as EventModel;
use app\modules\notify\models\EventQueue;
use app\modules\notify\models\EventUserSettings;
use yii\base\BootstrapInterface;
use yii\db\ActiveRecord;
use yii\db\AfterSaveEvent;

class Module extends \yii\base\Module implements BootstrapInterface
{
    public $controllerNamespace = 'app\modules\notify\controllers';
    public $models = [];

    public function bootstrap($app)
    {
        if($app instanceof \yii\console\Application){
            $app->controllerMap['notify'] = [
                'class' => 'app\modules\notify\console\HandlerController'
            ];

            $app->controllerMap['user-settings'] = [
                'class' => 'app\modules\notify\console\SettingsController'
            ];
        }
        $this->setHandler();

        if($app instanceof Application){
            $this->setComponents([
                'subscriber' => [
                    'class' => '\app\modules\notify\components\Subscribe'
                ]
            ]);
            $app->urlManager->addRules([
                '/notify/check' => 'notify/default/check',
                '/notify/setting' => 'notify/default/setting',
                '/notify/read' => 'notify/default/read',
                '/notify/view/<id:\d+>' => 'notify/default/view',
                '/notify/view' => 'notify/default/view',
            ]);
        }

        //При добавлении нового юзера, задать ему настройки для нотификаций
        $this->setHandlerNewUser();

        //Если была изменена роль пользователя
        $this->setHandlerChangeUserRole();

        //При добавлении нового события в коллекцию, пользователь автоматически подписывается
        $this->setHandlerEvent();

        //При обновлении включения/выключения событий, пользовательские настройки нотификаций, привести
        //к соответствию
        $this->setHandlerChangeStatusEvents();

        if(YII_ENV_DEV){
            NotifyAsset::register(\Yii::$app->getView());
        }
    }

    /**
     * Добавляет событие в очередь. На данный момент,
     * очередью является БД
     * @todo поменять очередь с БД на Gearman
     * @param Event $event
     * @param $id
     * @return bool
     */
    public function save(BaseEvent $event, $id)
    {
        if($event->sender instanceof Notification){
            $model = new EventQueue();
            $model->data = Serialize::encode($event->sender, true);
            $model->event_id = $id;
            $model->created_at = time();
            $model->type = $event->type;
            return $model->save();
        }
        return false;
    }

    /**
     * Задает обрабочик события который обрабатывает регистрацию
     * нового пользователя, в следствии чего ему необходимо добавить
     * необходимые настройки событий (чекбоксы).
     */
    protected function setHandlerNewUser()
    {
        Event::on(User::className(), ActiveRecord::EVENT_AFTER_INSERT, function (AfterSaveEvent $event) {
            $sender = $event->sender;
            if ($sender instanceof User) {
                $events = EventModel::find()->where('enabled = :enabled AND optional = :optional',
                    [':enabled' => EventModel::ENABLED, ':optional' => EventModel::ENABLED])->all();
                foreach ($events as $event) {
                    EventUserSettings::register($sender, $event->id);
                }
            }
        });
    }

    /**
     * Задает обработчик, который обрабатывает ситуацию, когда
     * у пользователя был измен статус. Напрмер он стад админом,
     * или обычным пользователем. В зависимости от его статуса
     * меняются настройки: добавляются или удаляются ранее
     * скрытые для обычных пользователей чекбоксы
     */
    protected function setHandlerChangeUserRole()
    {
        Event::on(User::className(), User::EVENT_CHANGED_ROLE, function (RoleChangedEvent $event) {
            $sender = $event->sender;
            if (!($sender instanceof User)) {
                return false;
            }
            $events = EventModel::find()->where(['enabled' => EventModel::ENABLED])->andWhere(['optional' => EventModel::DISABLED])->all();
            if ($event->newRole == User::ROLE_ADMIN || $event->newRole == User::ROLE_MODERATOR) {
                foreach ($events as $e) {
                    EventUserSettings::register($sender, $e->id);
                }
            }
            if ($event->newRole == User::ROLE_USER) {
                EventUserSettings::deleteAll(['and', 'user_id = :user_id', ['in', 'event_id', ArrayHelper::getColumn($events, 'id')]], [':user_id' => $sender->id]);
            }
        });
    }

    /**
     * Задает обработчик события, который
     * обрабатывает ситуацию добавления нового события в коллекцию
     * базу данных.
     */
    protected function setHandlerEvent()
    {
        Event::on(EventModel::className(), ActiveRecord::EVENT_AFTER_INSERT, function (AfterSaveEvent $event) {
            $sender = $event->sender;
            if (($sender instanceof EventModel)) {
                $users = User::find()->all();
                EventUserSettings::register($users, $sender->id);
            }
            $event->handled = true;
        });
    }

    /**
     * Задает обработчик события который выполняет
     * необходимые действия, если в админке, были отключены
     * события. В зависимости от этого, должны быть внесены
     * изменения в настройки пользователей касательно событий.
     * Скрыться чекбоксы, или напротив - добавится.
     */
    protected function setHandlerChangeStatusEvents()
    {
        Event::on(EventModel::className(), ActiveRecord::EVENT_AFTER_UPDATE, function (AfterSaveEvent $event) {

            if (!isset($event->changedAttributes['enabled']) || !isset($event->changedAttributes['optional'])) {
                return false;
            }


            //если реально было изменено состояние enabled
            if (isset($event->changedAttributes['enabled'])) {
                //Если текущее положение выключенно, а раньше было включено
                if ($event->sender->enabled == EventModel::DISABLED && $event->changedAttributes['enabled'] == EventModel::ENABLED) {
                    EventUserSettings::updateAll(['enabled' => EventModel::DISABLED], 'event_id = :event_id', [':event_id' => $event->sender->id]);
                }
                if ($event->sender->enabled == EventModel::ENABLED && $event->changedAttributes['enabled'] == EventModel::DISABLED) {
                    EventUserSettings::updateAll(['enabled' => EventModel::ENABLED], 'event_id = :event_id', [':event_id' => $event->sender->id]);
                }
            }

            //Если реально было изменено состояние optional
            if (isset($event->changedAttributes['optional'])) {
                //Если текущее положение выключенно, а раньше было включено
                if ($event->sender->optional == EventModel::DISABLED && $event->changedAttributes['optional'] == EventModel::ENРABLED) {
                    EventUserSettings::updateAll(['hidden' => EventModel::ENABLED], 'event_id = :event_id', [':event_id' => $event->sender->id]);
                }
                if ($event->sender->enabled == EventModel::ENABLED && $event->changedAttributes['enabled'] == EventModel::DISABLED) {
                    EventUserSettings::updateAll(['hidden' => EventModel::DISABLED], 'event_id = :event_id', [':event_id' => $event->sender->id]);
                }
            }
        });
    }

    /**
     * Задает глобальный обработчик на все события в сестеме.
     * Которые определены в соответствующей таблице. Перехваченные
     * события будут добавлены в очередь.
     */
    protected function setHandler()
    {
        $events = EventModel::find()->where('enabled = :enabled', [':enabled' => EventModel::ENABLED])->all();
        foreach ($events as $event) {
            $id = $event->id;
            \Yii::$app->on($event->name, function (BaseEvent $event) use ($id) {
                \Yii::info('[' . $event->name . '] ' . Serialize::encode($event, true), 'notify');
                $this->save($event, $id);
                \Yii::info('[' . $event->name . '] ' . $event->getNotify()->getDescription(), 'notify');
            });
        }

        //Получение уведомлений которые только для админов
        $adminsEvents = EventModel::find()->where('enabled = :enabled', [':enabled' => EventModel::ENABLED_FOR_ADMINS])->all();
        foreach ($adminsEvents as $event) {
            $id = $event->id;
            \Yii::$app->on($event->name, function (BaseEvent $event) use ($id) {
                $event->getNotify()->onlyAdmins = true;
                $this->save($event, $id);
            });
        }
    }


}
