<?php

namespace app\modules\notify\models;

use app\components\Entity;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * EventSubscribeSearch represents the model behind the search form about `\app\modules\notify\models\EventSubscribe`.
 */
class EventSubscribeSearch extends EventSubscribe
{
    public $userName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'entity', 'entity_id', 'created_at'], 'integer'],
            [['userName'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EventSubscribe::find()->where(['deleted' => Entity::NOT_DELETED]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if(isset($this->userName)) {
            $query->joinWith(['user' => function ($q) {
                $q->where(['LIKE', 'user.username', $this->userName]);
            }]);
        }

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'id'         => $this->id,
            'user_id'    => $this->user_id,
            'entity'     => $this->entity,
            'entity_id'  => $this->entity_id,
            'notify_subscribes.created_at' => $this->created_at,
        ]);

        return $dataProvider;
    }
}
