<?php

namespace app\modules\notify\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\notify\models\EventUserSettings;
use yii\helpers\VarDumper;

/**
 * EventUserSettingsSearch represents the model behind the search form about `app\modules\notify\models\EventUserSettings`.
 */
class EventUserSettingsSearch extends EventUserSettings
{
    public $userName;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'event_id', 'push_alert', 'push_email'], 'integer'],
            [['userName'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EventUserSettings::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        $this->load($params);

        $query->joinWith(['user' => function($q){
            $q->where('user.username LIKE "' . $this->userName.'%"');
        }]);


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            $query->joinWith(['user']);
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'event_id' => $this->event_id,
            'push_alert' => $this->push_alert,
            'push_email' => $this->push_email,
        ]);

        return $dataProvider;
    }
}
