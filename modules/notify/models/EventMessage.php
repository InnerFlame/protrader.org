<?php

namespace app\modules\notify\models;

use app\components\helpers\Serialize;
use app\models\user\User;
use app\modules\cabinet\modules\notify\models\NotifyTemplate;
use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "{{%notify_messages}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $sender_id
 * @property integer $event_id
 * @property integer $sent
 * @property integer $read
 * @property string $data
 * @property integer $created_at
 * @property integer $departure_date
 * @property integer $entity
 * @property integer $entity_id
 *
 * @property NotifyEvents $event
 * @property User $sender
 * @property User $user
 */
class EventMessage extends \yii\db\ActiveRecord
{
    const READ_YES = 1;

    const READ_NO = 0;

    public $count = 0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%notify_messages}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'sender_id', 'event_id', 'data'], 'required'],
            [['user_id', 'sender_id', 'event_id', 'sent', 'read', 'created_at', 'departure_date', 'entity', 'entity_id'], 'integer'],
            [['data'], 'string'],
            [['created_at'], 'default', 'value' => time()]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'sender_id' => Yii::t('app', 'Sender ID'),
            'event_id' => Yii::t('app', 'Event ID'),
            'sent' => Yii::t('app', 'Sent'),
            'read' => Yii::t('app', 'Read'),
            'data' => Yii::t('app', 'Data'),
            'created_at' => Yii::t('app', 'Created At'),
            'departure_date' => Yii::t('app', 'Departure Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::className(), ['id' => 'event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSender()
    {
        return $this->hasOne(User::className(), ['id' => 'sender_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplates()
    {
        return $this->hasMany(NotifyTemplate::className(), ['event_id' => 'event_id']);
    }

    /**
     * Подготавливает запрос для получения шаблона в зависимости
     * от количества сгрупированных по типу сообщений. Если больше
     * одного, то сгенерирует групой шаблон, если один то сокращенный
     * @return \yii\db\ActiveQuery
     */
    public function getTemplate()
    {
        $type = $this->count > 1 ? NotifyTemplate::TYPE_GROUP : NotifyTemplate::TYPE_SHORT;
        return $this->hasOne(NotifyTemplate::className(), ['event_id' => 'event_id'])->where(['type' => $type]);
    }

    /**
     * @return $this
     */
    public function getEmailTemplate()
    {
        return $this->hasOne(NotifyTemplate::className(), ['event_id' => 'event_id'])->where(['type' => NotifyTemplate::TYPE_EMAIL]);
    }

    /**
     * @inheritdoc
     * @return EventMessageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EventMessageQuery(get_called_class());
    }

    /**
     * @return \app\modules\notify\components\Notification
     */
    public function getNotify()
    {
        return Serialize::decode($this->data, true);
    }

    /**
     * Получает все сообщения пользователя, сгрупированныие
     * по типу события и entity.
     * @param $user_id
     * @return array [count => 4, 'messages' => []]
     */
    public static function getAllMessagesByUserId($user_id)
    {
        $query = EventMessage::find()->where('user_id = :user_id AND `read` = :read',
            ['user_id' => $user_id, ':read' => EventMessage::READ_NO]);

        $total = $query->count();

        $messages = $query->select('*, count(id) as count')->groupBy(['event_id', 'entity', 'entity_id'])->orderBy(['created_at' => SORT_DESC])->all();

        return ['count' => $total, 'messages' => $messages];
    }

    /**
     * Return url fo notify read
     * @return string
     */
    public function getViewUrl()
    {
        return '/notify/view/'.$this->id;
    }


}
