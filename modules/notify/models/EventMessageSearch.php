<?php

namespace app\modules\notify\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * EventMessageSearch represents the model behind the search form about `\app\modules\notify\models\EventMessage`.
 */
class EventMessageSearch extends EventMessage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'sender_id', 'event_id', 'sent', 'read', 'created_at', 'departure_date'], 'integer'],
            [['data'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EventMessage::find()->notDeleted();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'             => $this->id,
            'user_id'        => $this->user_id,
            'sender_id'      => $this->sender_id,
            'event_id'       => $this->event_id,
            'sent'           => $this->sent,
            'read'           => $this->read,
            'created_at'     => $this->created_at,
            'departure_date' => $this->departure_date,
        ]);

        $query->andFilterWhere(['like', 'data', $this->data]);

        return $dataProvider;
    }
}
