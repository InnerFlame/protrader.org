<?php

namespace app\modules\notify\models;

use app\components\Entity;
use Yii;

/**
 * This is the model class for table "{{%notify_events}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $enabled
 * @property integer $optional
 * @property string $label
 * @property integer $created_at
 */
class Event extends Entity
{
    const DISABLED = 0;
    const ENABLED = 1;
    const ENABLED_FOR_ADMINS = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%notify_events}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'label', 'created_at'], 'required'],
            [['enabled', 'optional', 'created_at'], 'integer'],
            [['name', 'label'], 'string', 'max' => 255],
            [['name'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('app', 'ID'),
            'name'       => Yii::t('app', 'Name'),
            'enabled'    => Yii::t('app', 'Enabled'),
            'optional'   => Yii::t('app', 'Optional'),
            'label'      => Yii::t('app', 'Label'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotifyMessages()
    {
        return $this->hasMany(EventMessage::className(), ['event_id' => 'id']);
    }

    /**
     * @return int
     */
    public function getTypeEntity()
    {
        return Entity::NOTIFY_EVENT;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getViewUrl()
    {
        // TODO: Implement getViewUrl() method.
    }
}
