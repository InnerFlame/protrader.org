<?php

namespace app\modules\notify\models;
use app\components\Entity;

/**
 * This is the ActiveQuery class for [[EventQueue]].
 *
 * @see EventQueue
 */
class EventQueueQuery extends \yii\db\ActiveQuery
{
    /**
     * @inheritdoc
     * @return EventQueue[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return EventQueue|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @return $this
     */
    public function notDeleted()
    {
        return $this->andWhere(['deleted' => Entity::NOT_DELETED]);
    }
}