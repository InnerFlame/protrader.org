<?php

namespace app\modules\notify\models;
use app\components\Entity;

/**
 * This is the ActiveQuery class for [[EventUserSettings]].
 *
 * @see EventUserSettings
 */
class EventUserSettingsQuery extends \yii\db\ActiveQuery
{
    /**
     * @inheritdoc
     * @return EventUserSettings[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return EventUserSettings|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @return $this
     */
    public function notDeleted()
    {
        return $this->andWhere(['deleted' => Entity::NOT_DELETED]);
    }
}
