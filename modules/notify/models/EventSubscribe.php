<?php

namespace app\modules\notify\models;

use app\components\Entity;
use app\models\user\User;
use Yii;

/**
 * This is the model class for table "notify_subscribes".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $entity
 * @property integer $entity_id
 * @property integer $created_at
 *
 * @property NotifyEvents $event
 * @property User $user
 */
class EventSubscribe extends Entity
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notify_subscribes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'entity', 'entity_id'], 'required'],
            [['user_id', 'entity', 'entity_id', 'created_at'], 'integer'],
            [['user_id', 'entity', 'entity_id'], 'unique', 'targetAttribute' => ['user_id', 'entity', 'entity_id'], 'message' => 'The combination of User ID, Event ID, Entity and Entity ID has already been taken.'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['created_at'], 'required'],
            [['created_at'], 'default', 'value' => time()]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('app', 'ID'),
            'user_id'    => Yii::t('app', 'User ID'),
            'entity'     => Yii::t('app', 'Entity'),
            'entity_id'  => Yii::t('app', 'Entity ID'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return NotifySubscribesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EventSubscribeQuery(get_called_class());
    }

    /**
     * @return string
     */
    public function getUserName()
    {
        return $this->user->username;
    }

    /**
     * @return int
     */
    public function getTypeEntity()
    {
        return Entity::NOTIFY_EVENT_SUBSCRIBE;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'NOTIFY_EVENT_SUBSCRIBE';
    }

    public function getViewUrl()
    {
        // TODO: Implement getViewUrl() method.
    }
}
