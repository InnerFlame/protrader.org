<?php

namespace app\modules\notify\models;

use app\components\Entity;
use app\models\user\User;
use app\modules\forum\ForumModule;
use Yii;

/**
 * This is the model class for table "notify_user_settings".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $event_id
 * @property integer $push_alert
 * @property integer $push_email
 * @property integer $hidden
 *
 * @property NotifyEvents $event
 * @property User $user
 */
class EventUserSettings extends Entity
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notify_user_settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'event_id'], 'required'],
            [['user_id', 'event_id', 'push_alert', 'push_email'], 'integer'],
            [['event_id'], 'exist', 'skipOnError' => true, 'targetClass' => Event::className(), 'targetAttribute' => ['event_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['hidden'], 'default', 'value' => 0]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('app', 'ID'),
            'user_id'    => Yii::t('app', 'User ID'),
            'event_id'   => Yii::t('app', 'Event ID'),
            'push_alert' => Yii::t('app', 'Push Alert'),
            'push_email' => Yii::t('app', 'Push Email'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::className(), ['id' => 'event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return NotifyEventsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EventUserSettingsQuery(get_called_class());
    }

    /**
     * Включает/выключает опцию параметра, доставки
     * оповещений. Если включено - выключает, если выключен - включает
     * @param $attribute
     * @return bool
     */
    public function turn($attribute)
    {
        if ($this->$attribute == 1) {
            $this->$attribute = 0;
            return $this->save();
        }
        $this->$attribute = 1;
        return $this->save();
    }

    /**
     * @return string
     */
    public function getUserName()
    {
        return $this->user->username;
    }

    /**
     * @param $users mixed List User models or one item User
     * @param $event_id int id event
     */
    public static function register($users, $event_id)
    {
        if (!is_array($users)) {
            $users = [$users];
        }
        $eventComment = Event::find()->where(['name' => ForumModule::EVENT_COMMENT_ADD])->one();
        foreach ($users as $user) {
            $model = new self();
            $model->event_id = $event_id;
            $model->user_id = $user->id;
            $model->push_alert = 1;
            $model->push_email = $event_id == $eventComment->id ? 1: 0;
            $model->hidden = 0;
            if(!$model->save()){
                Yii::error(sprintf('Не удалось сохранить настройки для пользователя: %d', $user->id));
            }
        }
    }

    public static function disable()
    {

    }

    public static function enable()
    {

    }

    /**
     * @return int
     */
    public function getTypeEntity()
    {
        return Entity::NOTIFY_EVENT_USER_SETTINGS;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'NOTIFY_EVENT_USER_SETTINGS';
    }

    public function getViewUrl()
    {
        // TODO: Implement getViewUrl() method.
    }
}
