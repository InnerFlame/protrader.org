<?php

namespace app\modules\notify\models;

use app\components\Entity;
use app\components\helpers\ArrayHelper;
use app\components\helpers\Serialize;
use app\models\user\User;
use app\modules\notify\components\BaseEvent;
use app\modules\notify\components\EventMail;
use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "{{%notify_events_queue}}".
 *
 * @property integer $id
 * @property string $data
 * @property integer $created_at
 * @property integer $event_id
 * @property integer $type;
 */
class EventQueue extends Entity
{

    /**
     * @var \app\modules\notify\components\Notification
     */
    private $notify;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%notify_events_queue}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['data', 'created_at', 'event_id'], 'required'],
            [['data'], 'string'],
            [['created_at', 'event_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('app', 'ID'),
            'data'       => Yii::t('app', 'Data'),
            'created_at' => Yii::t('app', 'Created At'),
            'event_id'   => Yii::t('app', 'Event Id'),
        ];
    }

    /**
     * @inheritdoc
     * @return EventQueueQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EventQueueQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::className(), ['id' => 'event_id']);
    }

    /**
     * Возвращает экземпляр оповещения
     * @return \app\modules\notify\components\Notification
     */
    public function getNotify()
    {
        if(!$this->notify){
            $this->notify = Serialize::decode($this->data, true);
        }
        return $this->notify;
    }

    /**
     * @return int
     */
    public function getTypeEntity()
    {
        return Entity::NOTIFY_EVENT_QUEUE;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'NOTIFY_EVENT_QUEUE';
    }

    /**
     * @return string
     */
    public function getViewUrl()
    {
        // TODO: Implement getViewUrl() method.
    }

    /**
     * Рассылает уведомления для пользователей, сохраняя их в виде сообщений.
     */
    public function push()
    {
        //Если нотификация не адресована только админам
        if(!$this->getNotify()->onlyAdmins){
            $users = $this->getUsersDestination();

            $this->send($users);
        }

        if(!$this->getNotify()->mail){
            $query = User::find()
                ->innerJoin(EventUserSettings::tableName() . ' settings', 'settings.user_id = user.id AND settings.event_id = :event_id AND push_alert = 1', [":event_id" => $this->event_id])
                ->where('user.id <> :user_id', [':user_id' => $this->getNotify()->getSenderUserId()])
                ->andWhere(['in', 'role', [User::ROLE_ADMIN, User::ROLE_MODERATOR]]);

            /**
             * Когда отправляется ответ, на комментарий, для всех админов
             * этот ответ приходит как просто добавлен комментарий, как и
             * для других пользователей. Бывает ситуации когда, получателем
             * ответа, является модератор, в этом случае он должен получить
             * ОТВЕТ, но при этом не должен получить событие при добавлении
             * комментария как модератор.
             * @todo переделать архитекруту для того что бы убрать этот костыль
             */
            if(isset($this->getNotify()->attributes['user_id'])){
                $query->andWhere(['not in', 'user.id', [$this->getNotify()->attributes['user_id']]]);
            }
            $moders = $query->all();
            $this->send($moders);
        }
    }

    /**
     * Генерирует пользователей которые должны получить уведомления
     * исходя из их настроек и подписок
     * @return \app\models\user\User[]|array
     */
    protected function getUsersDestination()
    {
        $query = User::find();
        if ($this->getNotify()->subscribed) {
            $query->innerJoin(EventSubscribe::tableName() . ' evt_subscribe',
                'evt_subscribe.entity = :entity AND evt_subscribe.entity_id = :entity_id AND user.id = evt_subscribe.user_id AND evt_subscribe.created_at < :date',
                [':entity' => $this->getNotify()->getEntity()->getTypeEntity(), ':entity_id' => $this->getNotify()->getEntity()->id, ':date' => $this->created_at]);
        }
        if($this->getNotify()->mail){
            $users = $query->innerJoin(EventUserSettings::tableName() . ' settings', 'settings.user_id = user.id AND settings.event_id = :event_id AND push_alert = 1', [":event_id" => $this->event_id])
                ->where(['not in', 'user.id', $this->getNotify()->getIgnoredUsersList()])->andWhere(['user.id' => $this->getNotify()->attributes['user_id']])->all();

            return $users;
        }


        $users = $query->innerJoin(EventUserSettings::tableName() . ' settings', 'settings.user_id = user.id AND settings.event_id = :event_id AND push_alert = 1', [":event_id" => $this->event_id])
            ->where(['not in', 'user.id', ArrayHelper::merge($this->getNotify()->getIgnoredUsersList(), [$this->getNotify()->getSenderUserId()])])->all();

        return $users;
    }

    /**
     * Непосредственно рассылает сообщения (раскидывает в БД)
     * @param $users
     */
    protected function send($users)
    {
        foreach ($users as $user) {
            $message = new EventMessage();
            $message->user_id = $user->id;
            $message->sender_id = $this->getNotify()->getSenderUserId();
            $message->data = $this->data;
            $message->event_id = $this->event_id;
            $message->entity = $this->getNotify()->getEntity()->getTypeEntity();
            $message->entity_id = $this->getNotify()->getEntity()->id;
            if (!$message->save()) {
                \Yii::error(json_encode($message->getErrors()));
            }
        }

        \Yii::info(sprintf("EVENT: name:%s, id_queue:%s ", $this->event->name, $this->id), 'notify');

        $names = ArrayHelper::getColumn($users, 'username');
        $string = implode(',', $names);
        \Yii::info("[Пользователи которые получили уведомления: {$string}]", 'notify');
    }
}
