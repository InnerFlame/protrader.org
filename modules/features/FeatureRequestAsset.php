<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2016
 */

namespace app\modules\features;



use yii\web\AssetBundle;

class FeatureRequestAsset extends AssetBundle
{

    public $sourcePath = '@app/modules/features/assets';

    public $js = [
        'js/filter.js'
    ];

    //'depends': 'yii\\web\\JqueryAsset'
    public $depends = ['yii\web\JqueryAsset'];
    
}