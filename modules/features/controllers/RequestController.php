<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 * Date: 07.03.2016
 * Time: 13:11
 */

namespace app\modules\features\controllers;

use app\components\CustomController;
use app\components\Entity;
use app\modules\features\models\FeatureRequest;
use app\modules\features\models\FeatureRequestSearch;
use app\modules\features\models\FeaturesRequestCategories;
use app\modules\features\models\SearchFeatureRequest;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseUrl;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

class RequestController extends CustomController
{
    public function actions()
    {
        return [
            'upload' => [
                'class' => 'app\components\ImageUploadAction',
                'url'   => '/upload/feature-request', // Directory URL address, where files are stored.
                'path'  => \Yii::getAlias('@app') . '/web/upload/feature-request' // Or absolute path to directory where files are stored.
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new FeatureRequestSearch();
        $searchModel->status = FeatureRequest::STATUS_VOTING;
        $dataProvider = $searchModel->search([$searchModel->formName() => \Yii::$app->request->queryParams]);

        return $this->renderTable($searchModel, $dataProvider);
    }

    public function actionCategory($status)
    {
        if(array_search(ucfirst($status), FeatureRequest::getStatusList()) === false){
            throw new NotFoundHttpException;
        }
        $searchModel = new FeatureRequestSearch();
        $searchModel->status = $status;
        $dataProvider = $searchModel->search([$searchModel->formName() => \Yii::$app->request->queryParams]);

        return $this->renderTable($searchModel, $dataProvider);
    }
    public function actionView($alias)
    {
        //only not removed
        $feature = FeatureRequest::find()->where(['deleted' => Entity::NOT_DELETED])->with(['user', 'votes'])->andWhere(['alias' => $alias])->one();
        if (!$feature)
            throw new NotFoundHttpException;

        $this->breadcrumbs[] = [
            'label' => \Yii::t('app', 'Feature request list'),
            'url'   => '/feature-request'
        ];
        \Yii::$app->counter->setCount($feature);

        return $this->render('view.twig', ['feature' => $feature, 'canonical' => Url::canonical()]);
    }

    /**
     * Save new Idea of request. Handled
     * method post, and save to database.
     * @throws NotFoundHttpException
     * @throws ServerErrorHttpException
     */
    public function actionSave()
    {
        if (\Yii::$app->getUser()->isGuest)
            throw new ForbiddenHttpException();

        $model = new FeatureRequest();
        if (\Yii::$app->request->post() && $model->load(\Yii::$app->request->post())) {
            $model->user_id = \Yii::$app->getUser()->getId();
            if ($model->save()) {
                \Yii::$app->session->setFlash('success', 'Idea was successfully added');
                return $this->redirect($model->getViewUrl());
            }
            throw new ServerErrorHttpException();
        }
        throw new NotFoundHttpException();
    }

    public function actionNew()
    {
        return $this->render('new.twig');
    }

    private function renderTable(FeatureRequestSearch $searchModel, ActiveDataProvider $dataProvider)
    {
        $this->clearRequestQueryParams();

        /* количество реквестов разбитых по стутасам */
        $counts = $searchModel->getQuery()->select('*, count(status) as count')->groupBy(['status'])->all();
        $counts = ArrayHelper::map($counts, 'status', 'count');

        $categoriesList = FeaturesRequestCategories::getListItemsAsArray();


        return $this->render('index.twig', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'columns' => FeatureRequest::getGridColumns($searchModel),
            'countsParams' => $counts,
            'categoriesList' => $categoriesList,
            'controller' => $this,
            //Сранный костыль благодаря сраному твигу. Такак эту ебалу невозможно получить в твиге
            'currentStatusName' => FeatureRequest::getStatusStringById(\Yii::$app->request->get('status')),
            'model' => new FeatureRequest()
        ]);
    }

    /**
     * Если постраничная навигация была перегенированна, после pjax reload.
     * То Pagination при создании ссылок подставляет в параметры атрибут _pjax.
     * Этот лишний параметр мешает при SEO создает так называемые дубли страниц.
     * Когда с точки зрения робота url другой, а контент тот же. Во избежании этой
     * ситуации, этот метод удаляет из параметров запроса ненужный pjax параметр
     * @see Pagination::createUrl
     */
    private function clearRequestQueryParams()
    {
        $params = \Yii::$app->request->getQueryParams();
        if(isset($params['_pjax'])){
            unset($params['_pjax']);
            \Yii::$app->request->setQueryParams($params);
        }
    }
}