<?php

namespace app\modules\features\events;

use app\modules\features\models\FeatureRequest;
use app\modules\notify\components\Notification;

class FeaturesNotify extends Notification
{
    public $model_id;

    private $_model;

    public function getTitle()
    {
        return $this->getEntity()->title;
    }

    public function getDescription()
    {
        return $this->getEntity()->content;
    }

    public function getAuthor()
    {
        return $this->getEntity()->user->getFullName();
    }

    /**
     * @return FeatureRequest
     */
    public function getEntity()
    {
        if (!$this->_model) {
            $this->_model = FeatureRequest::findOne($this->model_id);
        }
        return $this->_model;
    }
}