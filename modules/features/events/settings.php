<?php

use app\modules\cabinet\modules\notify\models\NotifyTemplate;
use \app\modules\features\Module;

return [

    Module::EVENT_FEATURES_ADD => [
        'label'     => 'New feature',
        'templates' => [
            NotifyTemplate::TYPE_EMAIL => [
                'title' => 'added new feature',
                'body' => '{title}'
            ],
            NotifyTemplate::TYPE_FULL  => [
                'title' => '{title}',
                'body' => 'feature was added by {username}'
            ],
            NotifyTemplate::TYPE_SHORT => [
                'title' => 'added new feature',
                'body' => '{title}'
            ],
            NotifyTemplate::TYPE_GROUP => [
                'title' => '{count} new feature(s) was(ware) add',
                'body' => '{title}'
            ]
        ]
    ],

//    Module::EVENT_FEATURES_EDIT => [
//        'label'     => 'Edited feature',
//        'templates' => [
//            NotifyTemplate::TYPE_EMAIL => [
//                'title' => 'edited feature',
//                'body' => '{title}'
//            ],
//            NotifyTemplate::TYPE_FULL  => [
//                'title' => 'edited feature',
//                'body' => '{title}'
//            ],
//            NotifyTemplate::TYPE_SHORT => [
//                'title' => 'edited feature',
//                'body' => '{title}'
//            ],
//            NotifyTemplate::TYPE_GROUP => [
//                'title' => 'edited feature',
//                'body' => '{title}'
//            ]
//        ]
//    ],

    Module::EVENT_CHANGED_STATUS => [
        'label'     => 'Changed feature status',
        'templates' => [
            NotifyTemplate::TYPE_EMAIL => [
                'title' => 'changed feature status',
                'body' => '{title}'
            ],
            NotifyTemplate::TYPE_FULL  => [
                'title' => 'changed feature status',
                'body' => '{title}'
            ],
            NotifyTemplate::TYPE_SHORT => [
                'title' => 'changed feature status',
                'body' => '{title}'
            ],
            NotifyTemplate::TYPE_GROUP => [
                'title' => 'changed feature status',
                'body' => '{title}'
            ]
        ]
    ],


];