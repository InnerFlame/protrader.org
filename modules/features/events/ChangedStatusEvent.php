<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 * Date: 21.04.2016
 * Time: 9:50
 */

namespace app\modules\features\events;


use yii\base\Event;

class ChangedStatusEvent extends Event
{

    public $newStatus;

    public $oldStatus;

    public $userChanged;
}