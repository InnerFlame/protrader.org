$('.page-content').on('change', 'input[type=checkbox]', function(){
    var a = $('.tabs .active a');
    var url = a.attr('href');
    var isAddedFilter = false;
    if($(this).attr('id') == 'checkAll'){
        $('input[type=checkbox]').not(this).prop('checked', $(this).prop('checked'));
    }
    $('input[type=checkbox]:checked').not('#checkAll').each(function(){
        if(isAddedFilter == false){
            url += '?category_ids[]='+$(this).data('id');
            isAddedFilter = true;
        }else{
            url += '&category_ids[]='+$(this).data('id');
        }

    });
    $.pjax.reload('#pjax-features', {url:url, timeout:5000});
});