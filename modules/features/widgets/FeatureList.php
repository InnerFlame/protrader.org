<?php

/**
 * @author Alex Gudenko
 * @copyright 2016
 */

namespace app\modules\features\widgets;

use yii\base\Widget;

class FeatureList extends Widget
{

    /**
     * @var string title of block
     */
    public $title;

    /**
     *
     * @var array models
     */
    public $models = null;

    /**
     * @var bool|string особый шаблон для вывода элементов
     */
    public $layout = false;

    public function run()
    {

        if (count($this->models) == 0) {
            return false;
        }
        $layout = $this->layout ? $this->layout : 'list.twig';
        return $this->render($layout, ['models' => $this->models, 'self' => $this]);
    }

    public function getViewUrl($id)
    {
        $model = \app\modules\features\models\FeatureRequest::find()->where(['id' => $id])->one();
        return ($model) ? $model->getViewUrl() : null;
    }

    public function getVotes($model)
    {
        return (isset($model['votes'])) ? $model['votes'] : 0;
    }

}
