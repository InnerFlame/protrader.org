<?php

namespace app\modules\features\models;

/**
 * This is the ActiveQuery class for [[FeatureRequest]].
 *
 * @see FeatureRequest
 */
class FeatureRequestQuery extends \yii\db\ActiveQuery
{
    /**
     * @inheritdoc
     * @return FeatureRequest[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return FeatureRequest|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @return $this
     */
    public function notDeleted()
    {
        return $this->andWhere('<>', 'deleted', 1);
    }
}