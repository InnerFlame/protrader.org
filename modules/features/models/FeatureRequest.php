<?php

namespace app\modules\features\models;

use app\components\ceo\Ceo;
use app\components\ceo\CeoBehavior;
use app\components\Entity;
use app\components\validators\EmptyContent;
use app\components\validators\MaxLength;
use app\components\widgets\vote\models\Vote;
use app\models\community\Comments;
use app\models\community\Counts;
use app\models\user\User;
use app\modules\features\events\FeaturesNotify;
use app\modules\features\Module;
use app\modules\notify\components\Event;
use app\modules\notify\components\EventFree;
use himiklab\sitemap\behaviors\SitemapBehavior;
use Yii;
use yii\base\ErrorException;
use yii\behaviors\SluggableBehavior;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "features_request".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $title
 * @property string $alias
 * @property string $content
 * @property integer $status
 * @property integer $return_votes
 * @property integer $deleted
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $category_id
 * @property integer $published
 *
 * @property User $user
 */
class FeatureRequest extends \app\components\Entity
{
    const STATUS_VOTING = 'voting';
    const STATUS_DEV = 'development';
    const STATUS_RELEASE = 'released';
    const STATUS_REFUSED = 'refused';

    const STATUS_PUBLISHED = 1;
    const STATUS_NOT_PUBLISHED = 0;

    const PLATFORM_DESKTOP = 1;
    const PLATFORM_MOBILE = 2;
    const PLATFORM_WEB = 3;
    const PLATFORM_ALGO_STUDIO = 4;

    public $count;

    public function behaviors()
    {
        return [
            [
                'class'         => SluggableBehavior::className(),
                'attribute'     => 'title',
                'slugAttribute' => 'alias',
                'immutable'     => 'true',
            ],
            [
                'class' => CeoBehavior::className(),
            ],
            [
                'class'       => SitemapBehavior::className(),
                'scope'       => function ($model) {
                    /** @var \yii\db\ActiveQuery $model */
                    $model->select(['id', 'alias', 'updated_at']);
                    $model->andWhere(['deleted' => 0]);
                    $model->orderBy(['updated_at' => SORT_DESC]);
                },
                'dataClosure' => function ($model) {
                    /** @var self $model */

                    return [
                        'loc'        => Url::toRoute([$model->getViewUrl()]),
                        'lastmod'    => date('Y-m-d\TH:i:sP', $model->updated_at),
                        'changefreq' => SitemapBehavior::CHANGEFREQ_WEEKLY,
                        'priority'   => 0.9
                    ];
                }
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'features_request';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['content', 'filter', 'filter' => 'trim'],
            [['user_id', 'title', 'alias', 'content', 'category_id'], 'required'],
            [['user_id', 'deleted', 'created_at', 'updated_at', 'published'], 'integer'],
            [['created_at', 'updated_at'], 'default', 'value' => time()],
            [['status'], 'default', 'value' => self::STATUS_VOTING],
            [['content'], 'string'],
            [['title'], 'string', 'max' => 60, 'min' => 3],
            [['content'], MaxLength::className(), 'count' => 15000],
            [['content'], EmptyContent::className()],
            [['alias'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'user_id'    => 'User ID',
            'title'      => 'Title',
            'alias'      => 'Alias',
            'content'    => 'Description',
            'category_id' => Yii::t('app', 'Category'),
            'status'     => 'Status',
            'deleted'    => 'Deleted',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCeo()
    {
        return $this->hasOne(Ceo::className(), ['entity_id' => 'id'])
            ->where(['entity' => $this->getTypeEntity()]);
    }

    public function getComments()
    {
        return $this->hasMany(Comments::className(), ['entity_id' => 'id'])->where([
            'entity' => $this->getTypeEntity()
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLikes()
    {
        return $this->hasOne(Counts::className(), ['entity_id' => 'id'])->where(['entity' => $this->getTypeEntity()]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVotes()
    {
        return $this->hasMany(Vote::className(), ['entity_id' => 'id'])->where(['entity' => $this->getTypeEntity()])->andWhere(['>', 'count', 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(FeaturesRequestCategories::className(), ['id' => 'category_id']);
    }
    public function getDescription()
    {
        $desc = strip_tags($this->content);
        return StringHelper::truncate($desc, 65);
    }

    /**
     * @inheritdoc
     * @return FeatureRequestQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FeatureRequestQuery(get_called_class());
    }

    public function getCountLikes()
    {
        return ($this->likes && $this->likes->count_like) ? $this->likes->count_like : 0;
    }

    public function getCountVotes()
    {
        $count = 0;
        foreach ($this->votes as $vote) {
            $count += $vote->count;
        }
        return $count;
    }

    /**
     * Подсчитывает и возвращает количество фейковых
     * голосов. Голоса, который были накрученны из
     * админки
     * @return int
     */
    public function getCountFakeVotes()
    {
        $count = 0;
        foreach ($this->votes as $vote) {
            $count += $vote->count_fake;
        }
        return $count;
    }

    public function getCountVotesByUserId($user_id)
    {
        $count = 0;
        foreach ($this->votes as $vote) {
            if ($vote->user_id == $user_id) {
                $count += $vote->count;
            }
        }
        return $count;
    }

    public function getCommentsCount()
    {
        return count($this->comments);
    }

    public function getDate($type = false)
    {
        if ($type == 'mobile') {
            return Yii::t('app', '{d, date,  mm. d. yyyy}', ['d' => $this->created_at]);
        }
        return Yii::t('app', '{d, date,  MMMM d, yyyy}', ['d' => $this->created_at]);
    }

    /**
     * Get status name
     * @return string status in string format
     */
    public function getStatus()
    {
        return self::getStatusStringById($this->status);
    }

    public function getContent()
    {
        $content = \app\components\helpers\StringHelper::truncateAsWord(strip_tags($this->content), 230, '...', null, true);
        return sprintf('<p>%s</p>', $content);
    }

    public function getTypeEntity()
    {
        return Entity::FEATURES_REQUEST;
    }

    public function getName()
    {
        return $this->title;
    }

    public function getViewUrl()
    {
        return '/request/' . $this->alias;
    }

    /**
     * Return votes all user, that voting
     * by this feature
     */
    public function returnVotes()
    {
        $votes = Vote::find()->with('user')->where(['entity' => $this->getTypeEntity()])->andWhere(['entity_id' => $this->id])->all();
        $transaction = $this->getDb()->beginTransaction();
        try {
            foreach ($votes as $vote) {
                $vote->user->count_votes += $vote->count;
                if ($vote->user->save()) {
                    continue;
                }

            }
            $this->return_votes = 1;
            if (!$this->save()) {
                throw new ErrorException("Не удалось пометить голоса как возвращенные");
            }
            $transaction->commit();
            return true;
        } catch (\Exception $e) {
            $transaction->rollBack();
            return false;
        }

    }

    public static function getGridColumns(FeatureRequestSearch $searchModel = null)
    {
        return [
            [
                'header'         => Yii::$app->controller->getView()->render('@app/modules/features/views/request/_filter-category.twig', ['searchModel' => $searchModel]),
                'enableSorting'  => true,
                'attribute'      => 'content',
                'format'         => 'raw',
                'value'          => function (FeatureRequest $model) {
                    return Yii::$app->controller->getView()->render('_item_content.twig', ['model' => $model]);
                },
                'contentOptions' => [
                    'class' => 'feature'
                ],
                'headerOptions'  => [
                    'class' => 'feature'
                ]
            ],
            [
                'header'         => 'Author',
                'attribute'      => 'user_id',
                'format'         => 'raw',
                'value'          => function ($model) {
                    return '<a data-pjax="0" href ="' . $model->user->getViewUrl() . '">' . $model->user->getFullname() . '</a>';
                },
                'contentOptions' => [
                    'class' => 'author'
                ],
                'headerOptions'  => [
                    'class' => 'author'
                ],
            ],
            [
                'header'         => 'Date',
                'attribute'      => 'created_at',
                'format'         => 'date',
                'contentOptions' => [
                    'class' => 'status'
                ],
                'headerOptions'  => [
                    'class' => 'status'
                ],
            ],
            [
                'header'         => 'Votes',
                'format'         => 'raw',
                'value'          => function (FeatureRequest $model) {
                    $count = Yii::$app->user->isGuest ? 0 : $model->getCountVotesByUserId(Yii::$app->user->identity->getId());
                    if(Yii::$app->user->isGuest && $model->status == FeatureRequest::STATUS_VOTING){
                        //Которые не голосуются выключены
                        $html = sprintf('<div id="btn-vote-%d" class="btn-vote" type="button" data-safe="true" data-id="%d" onclick="vote.up(\'%s\', \'%d\', \'#btn-vote-%d\');">%d</div>',
                            $model->id, $model->id, $model->getTypeEntity(), $model->id, $model->id, $model->getCountVotes(), $model->id);

                        if($count > 0){
                            return sprintf("%s <div class='text'>votes, <span>%s</span> yours</div>", $html, $count);
                        }else{
                            return $html;
                        }
                    }
                    if($model->status == FeatureRequest::STATUS_VOTING && Yii::$app->user->identity->count_votes){
                        $html = sprintf('<div id="btn-vote-%d" class="btn-vote" type="button" data-safe="true" data-id="%d" onclick="vote.up(\'%s\', \'%d\', \'#btn-vote-%d\');">%d</div>',
                            $model->id, $model->id, $model->getTypeEntity(), $model->id, $model->id, $model->getCountVotes(), $model->id);
                        if($count > 0){
                            return sprintf("%s <div class='text'>votes, <span>%s</span> yours</div>", $html, $count);
                        }else{
                            return $html;
                        }
                    }


                    $html =  sprintf('<div class="btn-vote disabled" data-id="%d">%d</div>',
                        $model->id, $model->getCountVotes());
                    if($count > 0){
                        return sprintf("%s <div class='text'>votes, <span>%s</span> yours</div>", $html, $count);
                    }else{
                        return $html;
                    }
                },
                'contentOptions' => [
                    'class' => 'votes'
                ],
                'headerOptions'  => [
                    'class' => 'votes'
                ],
            ],
        ];
    }

    public static function getGridColumnsBackend()
    {
        return [
            [
                'class'          => 'yii\grid\SerialColumn',
                'header'         => '<span>#</span>',
                'contentOptions' => [
                    'width' => 75
                ],
            ],
            [
                'header'    => 'Creator',
                'attribute' => 'user_id',
                'format'    => 'raw',
                'value'     => function ($model) {
                    return '<a href ="' . $model->user->getViewUrl() . '">' . $model->user->getFullname() . '</a>';
                },
            ],
            [
                'attribute' => 'title',
                'format'    => 'raw', 'value' => function ($model) {
                return '<a class="entity-link" href ="' . $model->getViewUrl() . '">' . Html::encode($model->title) . '</a>';
            },
            ],

            [
                'attribute' => 'status',
                'value'     => function ($model) {
                    return $model->getStatus();
                },
                'format'    => 'raw',
            ],
            [
                'attribute' => 'created_at',
                'format'    => 'date',
            ],
            [
                'header'    => 'Votes',
                'attribute' => 'votes',
                'format'    => 'raw',
                'value'     => function ($model) {
                    $options = [
                        'title'      => Yii::t('yii', 'Add Votes'),
                        'aria-label' => Yii::t('yii', 'Add Votes'),
                        'onClick'    => sprintf('show("%s", "%s")', $model->getTypeEntity(), $model->id),
                    ];
                    $output = '<span id="counter-' . $model->id . '">' . $model->getCountVotes() . '</span>';
                    $output .= '&nbsp';
                    $output .= '(<span title="Fake votes" id="counter-fake' . $model->id . '">' . $model->getCountFakeVotes() . '</span>)';
                    $output .= '&nbsp' . Html::a('<span class="glyphicon glyphicon-plus"></span>', 'javascript:void(0);', $options);
                    $options = [
                        'title'      => Yii::t('yii', 'Reset votes'),
                        'aria-label' => Yii::t('yii', 'Reset votes'),
                        'onClick'    => '
                            if(confirm(\'' . Yii::t('yii', 'Are you sure you want to reset all fake votes this Request?') . '\'))
                            {
                              return true;
                            } else {
                              return false;
                            }
                        ',
                    ];
                    $url = sprintf('/cabinet/request-fake-reset?entity=%s&entity_id=%s', $model->getTypeEntity(), $model->id);
                    $output .= '&nbsp&nbsp' . Html::a('<span class="glyphicon glyphicon-repeat"></span>', $url, $options);
//                    $options = [
//                        'title' => Yii::t('yii', 'Editor votes'),
//                        'aria-label' => Yii::t('yii', 'Editor votes'),
//                        'onClick' => 'console.log("editor votes")',
//                    ];
//                    $output .= '&nbsp&nbsp'.Html::a('<span class="glyphicon glyphicon-cog"></span>', 'javascript:void(0);', $options);
                    return $output;
                },
            ],
            [
                'class'    => 'app\components\grid\ActionColumnCustom',
                'template' => function ($model) {
                    return '{edit} {delete}';
                },
                'buttons'  => [
                    'edit'   => function ($url, $model, $key, $options) {
                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/request-edit', 'id' => $model->id]);
                        return Html::a('Edit', $url, $options);
                    },
                    'delete' => function ($url, $model, $key, $options) {
                        $options['onclick'] = '
                            if(confirm(\'' . Yii::t('yii', 'Are you sure you want to delete this Category?') . '\'))
                            {
                              return true;
                            } else {
                              return false;
                            }
                        ';
//                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/request-delete', 'id' => $model->id]);
                        return Html::a('Delete', $url, $options);
                    },
                ],
            ],
        ];
    }

    /**
     * Convert and return integer status to string
     * @param $status
     * @return string
     */
    public static function getStatusStringById($status)
    {
        if ($status == self::STATUS_VOTING)
            return Yii::t('app', 'Voting');

        if ($status == self::STATUS_DEV)
            return Yii::t('app', 'Development');

        if ($status == self::STATUS_RELEASE)
            return Yii::t('app', 'Released');

        if($status == self::STATUS_REFUSED)
            return Yii::t('app', 'Refused');
    }

    public static function getStatusList()
    {
        return [
            self::STATUS_VOTING  => 'Voting',
            self::STATUS_DEV     => 'Development',
            self::STATUS_RELEASE => 'Released',
            self::STATUS_REFUSED => 'Refused',
        ];
    }

    public static function getStatusListJson()
    {
        return json_encode(self::getStatusList());
    }

    public function afterSave($insert, $changedAttributes)
    {
        $repeated = self::find()->where(['alias' => $this->alias])->all();
        if (count($repeated) > 1) {
            $this->alias .= '-' . $this->id;
            $this->update();
        }

        # EVENT_FEATURES_ADD
        if ($insert) {
            $eventName = Module::EVENT_FEATURES_ADD;
        } else {
            # EVENT_CHANGED_STATUS
            if (isset($changedAttributes['status']) && $this->status != $changedAttributes['status']) {
                $eventName = Module::EVENT_CHANGED_STATUS;
            } else {
                # EVENT_FEATURES_EDIT
                $eventName = false;
            }
        }

        if ($eventName) {
            $notify = new FeaturesNotify(['model_id' => $this->id]);
            $notify->subscribed = false;
            $event = new Event(['sender' => $notify]);
            Yii::$app->trigger($eventName, $event);
        }

        return parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }
}
