<?php

namespace app\modules\features\models;

use app\components\Entity;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use app\modules\features\models\FeatureRequestQuery;

/**
 * SearchFeatureRequest represents the model behind the search form about `app\modules\features\models\FeatureRequest`.
 */
class FeatureRequestSearch extends FeatureRequest
{

    public $category_ids = [];
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'deleted', 'created_at', 'updated_at'], 'integer'],
            [['title', 'alias', 'content', 'category_ids', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->getQuery();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['created_at' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'         => $this->id,
            'user_id'    => $this->user_id,
            'status'     => $this->status,
            'deleted'    => $this->deleted,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'alias', $this->alias])
            ->andFilterWhere(['like', 'content', $this->content]);

        $query->andFilterWhere(['in', 'category_id', $this->category_ids]);

        return $dataProvider;
    }

    /**
     * Формирует запрос. Если пользователь гость, вытягивать только
     * опубликованные записи. Если пользователь админ или автор
     * вытягивать даже те, которые не опубликованны
     * @inheritdoc
     * @return FeatureRequestQuery the active query used by this AR class.
     */
    public function getQuery()
    {
        $query = FeatureRequest::find();
        $query->andWhere(['deleted' => Entity::NOT_DELETED]);
        if(Yii::$app->user->isGuest){
            $query->andWhere(['published' => self::STATUS_PUBLISHED]);
            return $query;
        }
        if(Yii::$app->user->identity->isAdmin()){
            return $query;
        }
        $query->andWhere(['published' => self::STATUS_PUBLISHED]);
        $query->orWhere(['user_id' => Yii::$app->user->getId()]);
        return $query;
    }
    public function getSelectedLabels()
    {
        if(!empty($this->category_ids)){
            $models = FeaturesRequestCategories::find()->where(['in', 'id', $this->category_ids])->all();
        }else{
            $models = FeaturesRequestCategories::find()->all();
        }


        $labels = ArrayHelper::getColumn($models, 'name');
        return $labels;
    }

    public function getCategories()
    {
        return FeaturesRequestCategories::find()->all();
    }

    /**
     * Проверяет включена ли данная категория в фильтр
     * по идентификатору
     */
    public function isFilteringCategoryById($id)
    {
        return array_search($id, $this->category_ids) === false? false : true;
    }
}
