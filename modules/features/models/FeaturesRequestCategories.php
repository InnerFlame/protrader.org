<?php

namespace app\modules\features\models;

use app\components\Entity;
use app\components\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "features_request_categories".
 *
 * @property integer $id
 * @property string $name
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted
 *
 * @property FeaturesRequest[] $featuresRequests
 */
class FeaturesRequestCategories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'features_request_categories';
    }

    public static function getListItems()
    {
        return FeaturesRequestCategories::find()->where(['deleted' => Entity::NOT_DELETED])->all();
    }

    public static function getListItemsAsArray()
    {
        $models = self::getListItems();
        return ArrayHelper::map($models, 'id', 'name');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'created_at', 'updated_at'], 'required'],
            [['created_at', 'updated_at', 'deleted'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'deleted' => 'Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeaturesRequests()
    {
        return $this->hasMany(FeatureRequest::className(), ['category_id' => 'id']);
    }
}
