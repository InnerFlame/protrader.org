<?php
namespace app\modules\news\controllers;

use app\components\CustomController;
use app\modules\brokers\models\Improvement;
use app\modules\news\models\News;
use app\modules\news\models\NewsCategory;
use yii\data\ArrayDataProvider;
use yii\web\HttpException;

class DefaultController extends CustomController
{
    public $viewUrl = '/news/';

    public $indexUrl = '/news/';

    /**
     * Return error twig array
     *
     * @return array
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'app\components\FlatErrorAction',
                'view' => 'error.twig'
            ],
        ];
    }

    public function init()
    {
        $this->breadcrumbs[] = [
            'label' => 'News',
            'url' => '/news/',
            'active' => 'active',
        ];
        return parent::init();
    }

    /**
     * Redirect to company/index
     *
     * @return mixed
     */
    public function actionIndex($category_alias = null)
    {
        $models = null;

       NewsCategory::isValidAlias($category_alias, $models);
        //filtering by checkboxes
        // $filter = false;
        //FilterHelper::filterByCategoryGET($models, 'category_id', $filter);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $models,
            'pagination' => [
                'defaultPageSize' => 10,
            ],
        ]);

        return $this->render('index.twig', [
            'dataProvider' => $dataProvider,
            //'filterGET' => $filter,
        ]);
    }


    /**
     * @param $alias
     * @return string
     * @throws HttpException
     */
    public function actionView($alias)
    {
        $model = News::find()->alias($alias)->one(); // modules\news\views\default\view.twig

        if(!is_object($model)){
            throw new HttpException(404);
        }


        \Yii::$app->counter->setCount($model);

        if (isset($model->category))
            $this->breadcrumbs[] = [
                'label' => $model->category->title,
                'url' => $model->category->getViewUrl(),
                'active' => 'active',
            ];

        return $this->render('view.twig', [
            'model' => $model,
        ]);
    }

}