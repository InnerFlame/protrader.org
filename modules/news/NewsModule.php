<?php
namespace app\modules\news;

use yii\base\BootstrapInterface;

class NewsModule extends \yii\base\Module implements BootstrapInterface
{
    public $controllerNamespace = 'app\modules\news\controllers';

    public function init()
    {
        parent::init();

        \Yii::configure($this, require(dirname(__FILE__) . '/config/main.php'));
    }

    public function bootstrap($app)
    {
        // TODO: Implement bootstrap() method.
        \Yii::$app->urlManager->addRules(
            [
                'news/image-upload' => 'news/default/image-upload', # * http://protrader.my/forum/topic/new-topic

                'news/<category>/<alias>' => 'news/default/view',

                'news/<category_alias:.+>' => 'news/default/index',

                'news' => 'news/default/index',  # for article pagination
            ]);
    }
}