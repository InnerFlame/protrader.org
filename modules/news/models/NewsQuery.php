<?php

namespace app\modules\news\models;

use app\components\Entity;

/**
 * This is the ActiveQuery class for [[News]].
 *
 * @see News
 */
class NewsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return News[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return News|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param $alias
     * @return $this
     */
    public function alias($alias)
    {
        return $this->andWhere([
            'alias' => $alias
        ]);
    }

    public function notDeleted()
    {
        $this->andWhere(['deleted' => Entity::NOT_DELETED]);
        return $this;

    }

}