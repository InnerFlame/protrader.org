<?php

namespace app\modules\news\models;

use app\components\ceo\CeoBehavior;
use app\components\customImage\CustomImage;
use app\components\Entity;
use app\components\langInput\LangInputBehavior;
use app\models\community\Counts;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "artcl_articles".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $user_id
 * @property string $title
 * @property string $alias
 * @property string $pictures
 * @property string $content
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ArtclCategory $category
 */
class News extends Entity
{
    public static $_pull = [];

    public $image = null;
    public $crop = null;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news_news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'content', 'title'], 'required'],
            //  [['title'], 'unique'],
            [['id', 'category_id', 'created_at', 'updated_at', 'user_id'], 'integer'],
            [['content'], 'string', 'min' => 3],
            [['alias', 'title'], 'string', 'max' => 255, 'min' => 3],
            [['published', 'pictures', 'user_id'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('app', 'ID'),
            'category_id' => Yii::t('app', 'Category ID'),
            'title'       => Yii::t('app', 'Title'),
            'alias'       => Yii::t('app', 'Alias'),
            'pictures'    => Yii::t('app', 'Pictures'),
            'user_id'     => Yii::t('app', 'User ID'),
            'content'     => Yii::t('app', 'Content'),
            'published'   => Yii::t('app', 'Published'),
            'deleted'     => Yii::t('app', 'Deleted'),
            'created_at'  => Yii::t('app', 'Created At'),
            'updated_at'  => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
            [
                'class'      => LangInputBehavior::className(),
                'attributes' => ['content', 'title'],
            ],
            [
                'class' => CeoBehavior::className(),
            ],
            [
                'class'         => SluggableBehavior::className(),
                'attribute'     => 'title',
                'slugAttribute' => 'alias',
                'immutable'     => 'true',
            ],
        ];
    }

    public function init()
    {
        $this->image = new CustomImage([
            'uploadPath' => isset(Yii::$app->params['newsUploadPath']) ?
                Yii::getAlias('@webroot') . Yii::$app->params['newsUploadPath'] : '',
            'uploadUrl'  => isset(Yii::$app->params['newsUploadPath']) ?
                Yii::getAlias('@web') . Yii::$app->params['newsUploadPath'] : '',
            'model'      => $this,
        ]);

        $this->image->setValidation('pictures', [
            'extensions' => 'png,jpg',
            'maxHeight'  => 4000,
            'maxWidth'   => 4000,
            'maxSize'    => 2048000,
            'minSize'    => 1,
        ]);

        parent::init();
    }

    /**
     * BeforeSave action
     * Checking client images
     *
     * @return mixed
     */
    public function beforeSave($insert)
    {
        if (!$this->image->isChanged('pictures')) unset($this->pictures);
        if ($this->image->isDeleted('pictures')) $this->pictures = '';

        return parent::beforeSave($insert);
    }

    /**
     *
     * After Save
     *
     * @return mixed
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($this->scenario <> 'delete') {
            if ($this->image->getUploadedFile('pictures')) {
                $this->pictures = $this->image->uploadImage('pictures');
                if ($this->pictures)
                    $this->save(false);
            }
        }

        return parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(NewsCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCounts()
    {
        return $this->hasOne(Counts::className(), ['entity_id' => 'id'])->where([
            'entity' => $this->getTypeEntity()
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        //return $this->hasMany(Comments::className(), ['entity_id' => 'id','entity' => self::className()]);
    }

    public function getUser()
    {
        return $this->hasOne(\app\models\user\User::className(), ['id' => 'user_id']);

    }

    /**
     * @inheritdoc
     * @return NewsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new NewsQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryName()
    {
        $category = NewsCategory::find()->where(['id' => $this->category_id])->one();
        if (is_object($category)) {
            return $category->title;
        }
    }

    public function getName()
    {
        return $this->title;
    }

    public static function getListByCategory($except_current_id = null, $category_id = null)
    {
        $models = self::find()->where(['category_id' => $category_id])->limit(10)->all();

        foreach ($models as $key => $model) {
            if ($model->id === $except_current_id)
                unset($models[$key]);
        }

        return ArrayHelper::map($models, 'alias', 'title');
    }
//    public static function getStatusClass($model, $style = '')
//    {
//        $count_like = (isset($model->counts->count_like) && !empty($model->counts->count_like)) ? $model->counts->count_like : 0;
//
//        return Likes::isLike(Constants::ENTITY_NEWS, $model->id) ?
//            '<a style="' . $style . '" class="btn like-btn" href="#" data-id="' . $model->id . '"><i class="fa fa-thumbs-o-up count-like" data-id="' . $model->id . '"> ' . $count_like . '</i></a>'
//            : '<a style="' . $style . '" class="btn like-btn disabled" href="#" data-id="' . $model->id . '"><i class="fa fa-thumbs-o-up count-like" data-id="' . $model->id . '">' . $count_like . '</i></a>';
//    }

    public static function getGridColumns()
    {
        return [
            [
                'class'          => 'yii\grid\SerialColumn',
                'header'         => '<span>#</span>',
                'contentOptions' => [
                    'width' => 50
                ],
            ],
            [
                'attribute' => 'title',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'published',
                'format'    => 'date',
            ],

            [
                'class'    => 'app\components\grid\ActionColumnCustom',
                'template' => function ($model) {
                    return '{edit} {delete}';
                },
                'buttons'  => [
                    'edit'   => function ($url, $model, $key, $options) {
                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/news-edit', 'id' => $model->id]);
                        return Html::a('Edit', $url, $options);
                    },
                    'delete' => function ($url, $model, $key, $options) {
                        $options['onclick'] = '
                            if(confirm(\'' . Yii::t('yii', 'Are you sure you want to delete this Page?') . '\'))
                            {
                              return true;
                            } else {
                              return false;
                            }
                        ';
//                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/news-delete', 'id' => $model->id]);
                        return Html::a('Delete', $url, $options);
                    },

                ],
            ],
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
//    public function getViewUrl()
//    {
//        return '/news/'. $this->alias;
//    }

    public function getViewUrl()
    {
        return '/news/' . $this->category->alias . '/' . $this->alias;
    }

    public function getTypeEntity()
    {
        return Entity::NEWS;
    }
}