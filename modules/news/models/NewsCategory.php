<?php

namespace app\modules\news\models;


use app\components\Entity;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\HttpException;

/**
 * This is the model class for table "news_category".
 *
 * @property integer $id
 * @property string $title
 * @property string $alias
 * @property string $data
 * @property integer $weight
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ArtclArticles[] $artclArticles
 */
class NewsCategory extends Entity
{
    public $viewUrl;
    public static $_pull = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'unique'],
            [['id', 'weight', 'created_at', 'updated_at', 'weight'], 'integer'],
            [['title', 'alias'], 'string', 'max' => 120, 'min' => 3],
            [['alias', 'data', 'published'], 'string', 'max' => 256]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('app', 'ID'),
            'title'      => Yii::t('app', 'Title'),
            'alias'      => Yii::t('app', 'Alias'),
            'data'       => Yii::t('app', 'Data'),
            'weight'     => Yii::t('app', 'Weight'),
            'published'  => Yii::t('app', 'Published'),
            'deleted'    => Yii::t('app', 'Deleted'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
            [
                'class'         => SluggableBehavior::className(),
                'attribute'     => 'title',
                'slugAttribute' => 'alias',
                'immutable'     => 'true',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function getCategoryList()
    {
        //if(isset(self::attributes())//->orderBy(['weight' =>SORT_ASC])
        $models = self::find()->all();

        return ArrayHelper::map($models, 'id', 'title');
    }

    /**
     * @param null $except
     * @return array
     */
    public static function getList($except = null)
    {
        $models = self::find()->all();

        foreach ($models as $key => $model) {
            $model->viewUrl = $model->getViewUrl();
        }

        return ArrayHelper::map($models, 'viewUrl', 'title');
    }

    /**
     * @param null $category_alias
     * @param $models
     * @throws HttpException
     */
    public static function isValidAlias($category_alias = null, &$models)
    {
        if ($category_alias) {
            $category = self::find()->where(['alias' => $category_alias])->one();

            if (!is_object($category))
                throw new HttpException(404);

            $models = News::find()->orderBy('published DESC')->where(['category_id' => $category->id])->all();

        } else {
            $models = News::find()->orderBy('published DESC')->all();
        }
    }

    /**
     * @param string $name
     * @return int|null
     */
    public static function getCategoryIdByName($name = '')
    {
        $model = self::find()->orderBy('weight DESC')->where(['title' => $name])->one();

        if (is_object($model)) {
            return $model->id;
        }
        return null;
    }

    /**
     * @return string
     */
    public function getViewUrl()
    {
        return '/news/' . $this->alias;
    }

    /**
     * @return int
     */
    public function getTypeEntity()
    {
        return Entity::CATEGORY_NEWS;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->title;
    }

    /**
     * @inheritdoc
     * @return NewsCategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new NewsCategoryQuery(get_called_class());
    }

    /**
     * @return array
     */
    public static function getGridColumns()
    {
        return [
            [
                'class'          => 'yii\grid\SerialColumn',
                'header'         => '<span>#</span>',
                'contentOptions' => [
                    'width' => 50
                ],
            ],
            [
                'attribute' => 'title',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'weight',
                'format'    => 'raw',
            ],

            [
                'class'    => 'app\components\grid\ActionColumnCustom',
                'template' => function ($model) {
                    return '{edit} {delete}';
                },
                'buttons'  => [
                    'edit'   => function ($url, $model, $key, $options) {
                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/news-category-edit', 'id' => $model->id]);
                        return Html::a('Edit', $url, $options);
                    },
                    'delete' => function ($url, $model, $key, $options) {
                        $options['onclick'] = '
                            if(confirm(\'' . Yii::t('yii', 'Are you sure you want to delete this Page?') . '\'))
                            {
                              return true;
                            } else {
                              return false;
                            }
                        ';
//                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/news-category-delete', 'id' => $model->id]);
                        return Html::a('Delete', $url, $options);
                    },

                ],
            ],
        ];
    }
}