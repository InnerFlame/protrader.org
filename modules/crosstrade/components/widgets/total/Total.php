<?php

namespace app\modules\crosstrade\components\widgets\total;

use app\modules\crosstrade\models\reports\ReportAo;
use yii\base\Widget;

class Total extends Widget
{
    public function run()
    {
        $models = ReportAo::find()->where(['operation_type' => 'Trading'])->all();
        $total = [];
        $total['total_trades'] = count($models);
        $total['total_profit'] = 0;
        $total['total_loss'] = 0;

        foreach ($models as $model) {
            if ($model->value >= 0) {
                $total['total_profit']++;
            } else {
                $total['total_loss']++;
            }
        }
        $total['best_trade'] = round(ReportAo::find()->where(['operation_type' => 'Trading'])->max('value'), 2);

        return $this->render('total.php', ['total' => $total]);
    }
}