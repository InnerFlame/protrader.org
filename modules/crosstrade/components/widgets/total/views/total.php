<?php
/* @var $this yii\web\View */
/* @var $model app\modules\crosstrade\models\reports\ReportAsr */
/* @var $dataProvider */
/* @var $total array */
?>
<div class="crossTableContainer">
    <table class="crossTable">
        <tbody>
        <tr>
            <td>
                <?= $total['total_trades']; ?>
                <span>Total trades</span>
            </td>
            <td>
                <?= $total['total_profit']; ?>
                <span>Total profit trades</span>
            </td>
            <td>
                <?= $total['total_loss']; ?>
                <span>Total loss trades</span>
            </td>
            <td>
                <?= '$' . Yii::$app->formatter->asDecimal($total['best_trade'], 2); ?>
                <span>Best trade</span>
            </td>
        </tr>
        </tbody>
    </table>
</div>