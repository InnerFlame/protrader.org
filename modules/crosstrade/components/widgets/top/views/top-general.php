<?php

use app\modules\crosstrade\models\reports\ReportAsr;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\modules\crosstrade\models\reports\ReportAsr */
/* @var $dataProvider */
?>

<div class="crosRightSide">
<!--    --><?php //Pjax::begin(); ?>
    <div class="title">TOP 20</div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'crossTable tablesorter'],
        'summary'      => '',
        'rowOptions'   => function ($model, $key, $index) {
            if ($index < 5){
                return ['class' => 'top5'];
//                return ['id' => $model['id'], 'class' => 'top5', 'onclick' => 'alert(this.id);'];
            }
        },
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'username',
                'format'    => 'raw',
                'value'     => function ($model) {
                    if ($model instanceof ReportAsr && $model->crossRegister && $model->crossRegister->user) {
//                        return \yii\helpers\Html::a($model->crossRegister->user->username, ['/crosstrade/stats/' . $model->crossRegister->user->id], ['data-login' => $model->login ]);
                        return \yii\helpers\Html::a($model->crossRegister->user->username, ['/crosstrade/stats/' . $model->crossRegister->user->id]);
                    }
                },
            ],
            [
                'attribute' => 'profit',
                'value'     => function ($model) {
                    if ($model instanceof ReportAsr) {
                        return  $model->getFormatterProfit();
                    }
                },
            ],
            [
                'attribute' => 'profit_percent',
                'value'     => function ($model) {
                    if ($model instanceof ReportAsr) {
                        return Yii::$app->formatter->asDecimal($model->getProfitPercent(), 2) . '%';
                    }
                }
            ],
            [
                'attribute' => 'pf',
                'format'    => 'raw',
                'value'     => function ($model) {
                    if ($model instanceof ReportAsr) {
                        return $model->getPf();
                    }
                }
            ],
            [
                'attribute' => 'deals',
                'value'     => function ($model) {
                    if ($model instanceof ReportAsr) {
                        return count($model->reportsAo) ? count($model->reportsAo) : 0;
                    }
                }
            ],
//                'login',
//                'profit',
//                'profit_percent',
//                'pf',
//                'deals',

//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<!--    --><?php //Pjax::end(); ?>
</div>
