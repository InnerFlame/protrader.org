<?php

namespace app\modules\crosstrade\components\widgets\top;

use app\modules\crosstrade\models\reports\ReportAsrSearch;
use yii\base\Widget;

class Top extends Widget
{
    public $tourneyNumber = null;
    public $username = null;

    public function run()
    {
        $searchModel = new ReportAsrSearch();
        if(is_null($this->tourneyNumber)){
            $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);
            return $this->render('top-general', ['dataProvider' => $dataProvider]);
        }
        $dataProvider = $searchModel->searchCurr(\Yii::$app->request->queryParams);
        $session = \Yii::$app->session;
        $session->set('tourneyNumber', $this->tourneyNumber);
        return $this->render('top-current', ['dataProvider' => $dataProvider, 'username' => $this->username]);
    }
}