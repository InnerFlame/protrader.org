<?php
/* @var $this yii\web\View */
/* @var $model app\modules\crosstrade\models\reports\ReportAsr */
/* @var $dataProvider */
?>

<!--    <div class="sidebarHeader">-->
<!--        Total P/L-->
<!--    </div>-->
<canvas id="chart-pl"></canvas>

<?php
$this->registerJs('

var ctxPl = document.getElementById("chart-pl");
var data = {
    labels: ' . json_encode($result['labels']) . ',
    legend: {
        display: false,
    },
    datasets: [
        {
            label: "Total P/L",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(75,192,192,0.4)",
            borderColor: "#ffbe00",
            borderCapStyle: \'butt\',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: \'miter\',
            pointBorderColor: "#888888",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "#888888",
            pointHoverBorderColor: "#ffbe00",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: ' . json_encode($result['pl_amount']) . ',
            spanGaps: false,
        }
    ]
};
var myLineChart = new Chart(ctxPl, {
    type: \'line\',
    data: data,
    options: {
        legend: {
            display: false,
        },
        tooltips: {
            enabled: true,
            mode: \'single\',
            callbacks: {
                label: function(tooltipItems, data) {
                    return \' $\' + tooltipItems.yLabel;
                }
            }
        },
    }
});

', \yii\web\View::POS_LOAD);
?>