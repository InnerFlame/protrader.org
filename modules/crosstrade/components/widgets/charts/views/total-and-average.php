<?php
/* @var $this yii\web\View */
/* @var $model app\modules\crosstrade\models\reports\ReportAsr */
/* @var $dataProvider */
?>

<div class="sidebarContainer">
    <div class="sidebarHeader">
        Average Win / Loss
    </div>
    <div class="sidebarContent">
        <canvas id="chart-av" width="400" height="400"></canvas>
    </div>
</div>

<?php
$this->registerJs('

var ctxAv = document.getElementById("chart-av").getContext("2d");
var data = {
    labels: ' . $labels . ',
    datasets: [
        {
            label: "Win",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "#009600",
            hoverBackgroundColor: "#00aa00",
            borderColor: "#2E8B57",
            borderCapStyle: \'butt\',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: \'miter\',
            pointBorderColor: "#2E8B57",
            pointBackgroundColor: "#2E8B57",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "#2E8B57",
            pointHoverBorderColor: "#2E8B57",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: ' . $av_vin . ',
            spanGaps: false,
        },
                {
            label: "Loss",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "#e50000",
            hoverBackgroundColor: "#ff0000",
            borderColor: "#FA8072",
            borderCapStyle: \'butt\',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: \'miter\',
            pointBorderColor: "#FA8072",
            pointBackgroundColor: "#FA8072",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "#FA8072",
            pointHoverBorderColor: "#FA8072",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: ' . $av_los . ',
            spanGaps: false,
        }
    ]
};

var myBarChart = new Chart(ctxAv, {
    type: \'bar\',
    data: data,
    options: {
        tooltips: {
            enabled: true,
            mode: \'single\',
            callbacks: {
                label: function(tooltipItems, data) {
                    return \' $\' + tooltipItems.yLabel;
                }
            }
        },
    }
});

', \yii\web\View::POS_LOAD);
?>

<div class="sidebarContainer">
    <div class="sidebarHeader">
        Total P/L
    </div>
    <div class="sidebarContent">
        <canvas id="chart-pl" width="400" height="400"></canvas>
    </div>
</div>

<?php
$this->registerJs('

var ctxPl = document.getElementById("chart-pl");
var data = {
    labels: ' . $labels . ',
    legend: {
        display: false,
    },
    datasets: [
        {
            label: "Total P/L",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(75,192,192,0.4)",
            borderColor: "#ffbe00",
            borderCapStyle: \'butt\',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: \'miter\',
            pointBorderColor: "#888888",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "#888888",
            pointHoverBorderColor: "#ffbe00",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: ' . $pl_amount . ',
            spanGaps: false,
        }
    ]
};
var myLineChart = new Chart(ctxPl, {
    type: \'line\',
    data: data,
    options: {
        legend: {
            display: false,
        },
        tooltips: {
            enabled: true,
            mode: \'single\',
            callbacks: {
                label: function(tooltipItems, data) {
                    if (tooltipItems.yLabel < 0)
                        return \'- $\' + Math.abs(tooltipItems.yLabel);
                    return \' $\' + tooltipItems.yLabel;
                }
            }
        },
    }
});

', \yii\web\View::POS_LOAD);
?>