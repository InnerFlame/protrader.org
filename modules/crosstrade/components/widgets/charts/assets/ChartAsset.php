<?php

namespace app\modules\crosstrade\components\widgets\charts\assets;

use yii\web\AssetBundle;

class ChartAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/crosstrade/components/widgets/charts/assets';

    public $js = [
        'js/Chart.js?vr=0.4'
    ];

    public $css = [
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}