<?php

namespace app\modules\crosstrade\components\widgets\charts;

use app\components\helpers\ArrayHelper;
use app\modules\crosstrade\models\reports\ReportAo;
use yii\base\Widget;

class TotalDetail extends Widget
{
    public $login;

    public function run()
    {
        $result = [];
        $amount = 0;

        $sql = "
            SELECT
                DATE_FORMAT(ao.created_at, \"%d.%m\") AS labels
                , SUM(ao.`value`) AS pl_amount
                , ao.login
            FROM crosstrade_report_ao ao

            WHERE ao.login = '" . $this->login . "'
            GROUP BY DATE(ao.created_at)
        ";

        $rows = \Yii::$app->db->createCommand($sql)->queryAll();
        $labels = $this->getLabels();

        $result['labels'][0] = 'Start';
        $result['pl_amount'][0] = 100000;


        for ($i = 0; $i < count($labels); $i++) {
            $result['labels'][] = $labels[$i];
            $amount += isset($rows[$i]) ? round($rows[$i]['pl_amount'], 2) : 0;
            $result['pl_amount'][] = $amount;
        }

        return $this->render('total-detail.php', ['result' => $result]);
    }

    public function getLabels()
    {
        $models = ReportAo::find()
            ->select(['DATE_FORMAT(created_at, "%d.%m") AS labels'])
            ->where(['operation_type' => 'Trading'])
            ->groupBy('DATE(created_at)')
            ->all();

        return ArrayHelper::getColumn($models, 'labels');
    }
}