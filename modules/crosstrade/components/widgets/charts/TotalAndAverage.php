<?php

namespace app\modules\crosstrade\components\widgets\charts;

use yii\base\Widget;

class TotalAndAverage extends Widget
{
    public function run()
    {
        $labels = [];
        $pl_amount = [];
        $av_vin = [];
        $av_los = [];

        $sql = "
            SELECT
                DATE_FORMAT(ao.created_at, \"%d.%m\") AS labels
                , SUM(ao.`value`) AS pl_amount
                , (SELECT SUM(ao1.`value`)/COUNT(ao1.`value`) FROM crosstrade_report_ao ao1 WHERE DATE_FORMAT(ao1.created_at, \"%d.%m\") = DATE_FORMAT(ao.created_at, \"%d.%m\") AND ao1.`value` >= 0 AND ao1.operation_type = \"Trading\")  AS av_vin
                , (SELECT SUM(ao2.`value`)/COUNT(ao2.`value`) FROM crosstrade_report_ao ao2 WHERE DATE_FORMAT(ao2.created_at, \"%d.%m\") = DATE_FORMAT(ao.created_at, \"%d.%m\") AND ao2.`value` < 0 AND ao2.operation_type = \"Trading\")  AS av_los
            FROM crosstrade_report_ao ao
            WHERE ao.operation_type = \"Trading\"
            GROUP BY DATE(ao.created_at)
        ";
        $rowsPl = \Yii::$app->db->createCommand($sql)->queryAll();
        foreach ($rowsPl as $row) {
            $labels[] = $row['labels'];
            $pl_amount[] = round($row['pl_amount'], 2);
            $av_vin[] = round($row['av_vin'], 2);
            $av_los[] = abs(round($row['av_los'], 2));
        }

        return $this->render('total-and-average.php', [
            'labels'    => json_encode($labels),
            'pl_amount' => json_encode($pl_amount),
            'av_vin'    => json_encode($av_vin),
            'av_los'    => json_encode($av_los),
        ]);
    }
}