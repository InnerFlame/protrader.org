<?php

/* @var $this yii\web\View */
/* @var $model app\modules\crosstrade\models\reports\ReportAsr */
/* @var $number integer */
/* @var $dataProvider */
?>

<div class="sidebarContainer user">
    <div class="sidebarContent">
        <div class="photo">
            <a href="<?= Yii::$app->user->identity->getViewUrl(); ?>">
                <?= Yii::$app->user->identity->getAvatar(115, 115); ?>
            </a>
        </div>
        <div class="number">
            #<?= $model->getTourneyNumber($user->id); ?>
        </div>
        <div class="name">
            <?= Yii::$app->user->identity->fullName; ?>
        </div>
        <table class="crossTable">
            <tbody>
            <tr>
                <td>Gain:</td>
                <td><?= $model->getProfitPercent() . '%'; ?></td>
            </tr>
            <tr>
                <td>Profit:</td>
                <td><?= '$' . Yii::$app->formatter->asDecimal($model->getProfit(), 2); ?></td>
            </tr>
            <tr>
                <td>Equity</td>
                <td><?= '$' . Yii::$app->formatter->asDecimal($model->balance, 2); ?></td>
            </tr>
            </tbody>
        </table>
        <a href="<?= Yii::$app->urlManager->createUrl(['crosstrade/stats/' . Yii::$app->user->identity->getId()]) ?>"
           class="crossBtn">
            My Stats
        </a>
    </div>
</div>