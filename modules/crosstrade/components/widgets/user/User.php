<?php

namespace app\modules\crosstrade\components\widgets\user;

use app\models\user\UserIdentity;
use app\modules\crosstrade\models\reports\ReportAsr;
use yii\base\Widget;

class User extends Widget
{
    public function run()
    {
        $user = \Yii::$app->user->identity;
        if(!$user instanceof UserIdentity){
            return false;#TODO (пользователь не авторизован)
        }

        if ($user && $user->crosstradeRegister && $user->crosstradeRegister->reportAsr) {
            $model = $user->crosstradeRegister->reportAsr;
            if (!$model instanceof ReportAsr){
                return false;#TODO (пользователь не зареган на чемпионат)
            }

            return $this->render('user.php', ['user' => $user, 'model' => $model]);
        }
    }
}