<?php

namespace app\modules\crosstrade\controllers;

use app\assets\CrossTradeAsset;
use app\models\setting\SettingsCustomField;
use app\models\setting\SettingsCustomValue;
use app\models\user\crosstrade\forms\CrosstradeRegisterForm;
use app\models\user\crosstrade\forms\CrosstradeSubscribeForm;
use app\models\user\UserCustomField;
use app\models\user\UserIdentity;
use app\modules\crosstrade\models\reports\ReportAsr;
use \app\models\user\User;
use Yii;
use yii\web\Controller;
use yii\web\HttpException;

/**
 * Default controller for the `crosstrade` module
 */
class CrosstradeController extends Controller
{
    public $layout = false;

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index.twig');
    }

    public function actionView($id)
    {
//        var_dump($id);die();
        $user = User::findOne($id);
        if(!$user ){
            throw new HttpException('404');
        }

        return $this->render('view.twig', ['user' => $user]);
    }

    public function actionAbout()
    {
        $response = false;
        CrossTradeAsset::register(\Yii::$app->getView());
        $field = SettingsCustomField::find()->where(['alias' => 'is_subscription'])->one();
        $settings = SettingsCustomValue::findOne($field->id);
        $user = Yii::$app->user->identity;
        $cross_trade_model = ($settings->value == 1) ? new CrosstradeSubscribeForm() : new CrosstradeRegisterForm();

        if ($user instanceof UserIdentity) {
            if ($settings->value == 0) {
                if ($user->crosstradeRegister) {
                    $response = $this->renderPartial('@app/modules/crosstrade/views/crosstrade/registration_already.twig');
                } else {
                    $cross_trade_model->email = $user->email;
                    if (!empty($user->fullname)) {
                        $path = explode(' ', $user->fullname);
                        $cross_trade_model->first_name = reset($path);
                        $cross_trade_model->last_name = (count($path) > 1) ? end($path) : null;
                    }
                    $cross_trade_model->country = UserCustomField::getDataByAttrName('country');
                }
            }
            if ($settings->value == 1) {
                if ($user->crosstradeSubscriber) {
                    $response = $this->renderPartial('@app/modules/crosstrade/views/crosstrade/subscribe_already.twig');
                } else {
                    $cross_trade_model->email = $user->email;
                    $cross_trade_model->fullname = $user->fullname;
                    $cross_trade_model->country = UserCustomField::getDataByAttrName('country');
                }
            }
        }
        if ($cross_trade_model->load(Yii::$app->request->post()) && $cross_trade_model->validate()) {
            $response = ($settings->value == 1)
                ? $cross_trade_model->getResponse($user, Yii::$app->request->post('CrosstradeSubscribeForm'))
                : $cross_trade_model->getResponse($user, Yii::$app->request->post('CrosstradeRegisterForm'));
        }

        return $this->render('about.twig', [
            'cross_trade_model' => $cross_trade_model,
            'response'          => $response,
            'settings'          => $settings,
        ]);
    }
}
