<?php

namespace app\modules\crosstrade\models\reports;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * StatSearch represents the model behind the search form about `app\modules\crosstrade\models\reports\ReportAsr`.
 */
class ReportAsrSearch extends ReportAsr
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['profit'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ReportAsr::find()->orderBy([new \yii\db\Expression('balance = "100000", balance DESC')])->limit(self::TOP_LIMIT);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => false
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
//            'id'             => $this->id,
//            'trade_id'       => $this->trade_id,
//            'login'          => $this->login,
//            'profit'         => $this->profit,
//            'profit_percent' => $this->profit_percent,
//            'pf'             => $this->pf,
//            'deals'          => $this->deals,
        ]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ActiveDataProvider
     */
    public function searchCurr($params)
    {
        $query = ReportAsr::find()->orderBy([new \yii\db\Expression('balance = "100000", balance DESC')]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => false
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
        ]);

        return $dataProvider;
    }
}
