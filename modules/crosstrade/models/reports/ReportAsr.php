<?php

namespace app\modules\crosstrade\models\reports;

use app\models\user\crosstrade\CrosstradeSubscriber;
use app\models\user\User;
use Yii;

/**
 * This is the model class for table "crosstrade_report_asr".
 *
 * @property integer $id
 * @property string $account
 * @property string $login
 * @property double $balance
 * @property double $projected_balance
 * @property integer $created_at
 *
 * @property double $profit
 * @property double $profit_percent
 * @property double $pf
 * @property integer $deals
 */
class ReportAsr extends \yii\db\ActiveRecord
{
    const BALANCE_START = 100000;
    const OPERATION_TYPE = 'Trading';
    const TOP_LIMIT = 20;

    public $username;
    public $profit;
    public $profit_percent;
    public $pf;
    public $deals;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'crosstrade_report_asr';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at'], 'integer'],
            [['balance', 'projected_balance'], 'number'],
            [['account', 'login'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                => Yii::t('app', 'ID'),
            'account'           => Yii::t('app', 'Account'),
            'login'             => Yii::t('app', 'Login'),
            'balance'           => Yii::t('app', 'Balance'),
            'projected_balance' => Yii::t('app', 'Projected Balance'),
            'created_at'        => Yii::t('app', 'Created At'),

            #report
            'username'          => Yii::t('app', 'Username'),
            'profit'            => Yii::t('app', 'Profit'),
            'profit_percent'    => Yii::t('app', 'Profit Percent'),
            'pf'                => Yii::t('app', 'Profit Factor'),
            'deals'             => Yii::t('app', 'Trades'),
        ];
    }

    /**
     * @inheritdoc
     * @return ReportAsrQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ReportAsrQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportsAo()
    {
        return $this->hasMany(ReportAo::className(), ['login' => 'login'])->andWhere(['operation_type' => self::OPERATION_TYPE]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrossRegister()
    {
        return $this->hasOne(CrosstradeSubscriber::className(), ['demo_login' => 'login'])->andWhere(['status' => CrosstradeSubscriber::STATUS_REGISTERED]);
    }

    /**
     * @return float
     */
    public function getProfit()
    {
        return $this->balance - self::BALANCE_START;
    }

    /**
     * @return string
     */
    public function getFormatterProfit()
    {
        $profit = $this->getProfit();
        return ($profit < 0) ? '- $' . Yii::$app->formatter->asDecimal(abs($profit), 2) : '$' . Yii::$app->formatter->asDecimal($profit, 2);
    }

    /**
     * @return float
     */
    public function getProfitPercent()
    {
        return round($this->getProfit() / self::BALANCE_START * 100, 2);
    }

    /**
     * @return float
     */
    public function getPf()
    {
        $tp_count = 0;
        $tp_sum = 0;
        $sl_count = 0;
        $sl_sum = 0;
        $count = count($this->reportsAo);

        foreach ($this->reportsAo as $ao) {
            if ($ao->value > 0) {# tp
                $tp_count++;
                $tp_sum += $ao->value;
            }
            if ($ao->value < 0) {# sl
                $sl_count++;
                $sl_sum += abs($ao->value);
            }
        }

        if ($count === 0 || $sl_count === 0 || $sl_sum === 0 || $tp_count === 0) {
            return 'N/A';
        }

        $avr_tp = $this->getPfAvr($tp_sum, $tp_count);
        $tp = $this->getPfPercent($tp_count, $count);
        $avr_sl = $this->getPfAvr($sl_sum, $sl_count);
        $sl = $this->getPfPercent($sl_count, $count);

        return round(($avr_tp * $tp) / ($avr_sl * $sl), 2);
    }

    /**
     * Расчитывает размер средней прибыльной/убыточной сделки в деньгах
     * @param $sum
     * @param $count
     * @return float
     */
    public function getPfAvr($sum, $count)
    {
        return $sum / $count;
    }

    /**
     * Расчитывает  вероятность получения прибыльной/убыточной сделки
     * @param $count
     * @param $count_all
     * @return float
     */
    public function getPfPercent($count, $count_all)
    {
        return ($count / $count_all) * 100;
    }

    /**
     * Возвращает номер в общей статистике
     * @return int
     */
    public function getTourneyNumber()
    {
        $reports = self::find()->orderBy([new \yii\db\Expression('balance = "100000", balance DESC')])->all();
        for ($i = 0; $i < count($reports); $i++) {
            if ($reports[$i]->login == $this->crossRegister->demo_login) {
                return $i + 1;
            }
        }
    }

    /**
     * @return mixed
     */
    public function getProfitDeal()
    {
        $max = ReportAo::find()->where(['login' => $this->login])->andWhere(['operation_type' => self::OPERATION_TYPE])->max('value');
        return $max ? $max : 0;
    }

    /**
     * @return mixed
     */
    public function getFormatterProfitDeal()
    {
        $best = $this->getProfitDeal();
        return ($best < 0) ? '- $' . Yii::$app->formatter->asDecimal(abs($best), 2) : '$' . Yii::$app->formatter->asDecimal($best, 2);
    }
}
