<?php

namespace app\modules\crosstrade\models\reports;

use Yii;

/**
 * This is the model class for table "crosstrade_report_ao".
 *
 * @property integer $id
 * @property string $account
 * @property string $login
 * @property integer $operation_id
 * @property integer $order_id
 * @property integer $position_id
 * @property integer $trade_id
 * @property string $operation_type
 * @property double $value
 * @property string $currency
 * @property string $comments
 * @property integer $created_at
 */
class ReportAo extends \yii\db\ActiveRecord
{
    public $labels;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'crosstrade_report_ao';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['operation_id', 'order_id', 'position_id', 'trade_id'], 'integer'],
            [['value'], 'number'],
            [['comments'], 'string'],
            [['account', 'login', 'operation_type'], 'string', 'max' => 30],
            [['currency'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'             => Yii::t('app', 'ID'),
            'account'        => Yii::t('app', 'Account'),
            'login'          => Yii::t('app', 'Login'),
            'operation_id'   => Yii::t('app', 'Operation ID'),
            'order_id'       => Yii::t('app', 'Order ID'),
            'position_id'    => Yii::t('app', 'Position ID'),
            'trade_id'       => Yii::t('app', 'Trade ID'),
            'operation_type' => Yii::t('app', 'Operation Type'),
            'value'          => Yii::t('app', 'Value'),
            'currency'       => Yii::t('app', 'Currency'),
            'comments'       => Yii::t('app', 'Comments'),
            'created_at'     => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * @inheritdoc
     * @return ReportAoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ReportAoQuery(get_called_class());
    }
}
