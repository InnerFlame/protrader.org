<?php

namespace app\modules\crosstrade\models\reports;

/**
 * This is the ActiveQuery class for [[ReportAsr]].
 *
 * @see ReportAsr
 */
class ReportAsrQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ReportAsr[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ReportAsr|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
