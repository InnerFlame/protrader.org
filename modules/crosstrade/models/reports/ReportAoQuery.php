<?php

namespace app\modules\crosstrade\models\reports;

/**
 * This is the ActiveQuery class for [[ReportAo]].
 *
 * @see ReportAo
 */
class ReportAoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ReportAo[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ReportAo|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
