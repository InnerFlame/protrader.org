<?php

namespace app\modules\crosstrade\models;

use app\components\api\NotServiceAvailableException;
use app\components\api\ProtraderApi2;

class ChampStat
{
    const CHAMP_SERVER_1 = 'https://champ1.protrader.org:8443/proftrading/';
    const CHAMP_SERVER_2 = 'https://champ2.protrader.org:8443/proftrading/';
    const CHAMP_API_ALIAS = '/ProTrader.jws?wsdl';
    const CHAMP_REPORT_AO = 'AO';# AO
    const CHAMP_REPORT_BR = 'Balance Report';# Balance Report
    const CHAMP_REPORT_ASR = 'Account Statement Report';# Account Statement Report
    const CHAMP_LOGIN = 'web';
    const CHAMP_PASS = '20ff4756153133a88734370e2ef871e4fcdade9b60e2996c10cae15c3fcc4dca';

    private static $_instance = null;

    private function __construct() {}
    private function __clone() { }

    static public function getInstance() {
        if(is_null(self::$_instance))
        {
            self::$_instance = self::balance();
        }
        return self::$_instance;
    }

    /**
     * @return ProtraderApi2
     * @throws NotServiceAvailableException
     */
    public static function balance()
    {
        $pt1 = new ProtraderApi2(self::CHAMP_SERVER_1);
        if(!$pt1->sever_avaliable){
            $pt2 = new ProtraderApi2(self::CHAMP_SERVER_2);
            if(!$pt2->sever_avaliable){
                throw new NotServiceAvailableException();
            }
            return $pt2;
        }
        return $pt1;
    }

}