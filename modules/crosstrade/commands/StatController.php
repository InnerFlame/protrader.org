<?php

namespace app\modules\crosstrade\commands;

use app\models\user\crosstrade\CrosstradeSubscriber;
use app\modules\crosstrade\models\ChampStat;
use app\modules\crosstrade\models\reports\ReportAo;
use app\modules\crosstrade\models\reports\ReportAsr;
use app\modules\crosstrade\models\stat\Stat;
use yii\console\Controller;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;

class StatController extends Controller
{
//    const BALANCE_START = 100000;

    public function actionUpdate()
    {
        ReportAo::deleteAll();
        $this->setReportAo();

        ReportAsr::deleteAll();
        $this->setReportAsr();
    }

    public function setReportAsr()
    {
        $report_asr = $this->getReport(ChampStat::CHAMP_REPORT_ASR);

        for ($i = 0; $i < count($report_asr['report']['tr']); $i++) {
            if ($i === 0) continue;

            $model = new ReportAsr();
            $model->account = $report_asr['report']['tr'][$i]['td'][0];
            $model->login = $report_asr['report']['tr'][$i]['td'][1];
            $model->balance = $report_asr['report']['tr'][$i]['td'][3];
            echo $report_asr['report']['tr'][$i]['td'][3] . "\n";
            $model->projected_balance = $report_asr['report']['tr'][$i]['td'][6];
            $model->created_at = time();
            $model->save();
            echo "Success report ASR saved!!! acc:{$model->account} \n";
        }
        echo "DONE ASR!!! \n";
    }

    public function setReportAo()
    {
        $report_ao = $this->getReport(ChampStat::CHAMP_REPORT_AO);

        for ($i = 0; $i < count($report_ao['report']['tr']); $i++){
            if ($i === 0) continue;

            $model = new ReportAo();
            $model->account = $report_ao['report']['tr'][$i]['td'][0];
            $model->login = $report_ao['report']['tr'][$i]['td'][1];
            $model->operation_id = $report_ao['report']['tr'][$i]['td'][3];
            $model->order_id = $report_ao['report']['tr'][$i]['td'][4];
            $model->position_id = $report_ao['report']['tr'][$i]['td'][5];
            $model->trade_id = $report_ao['report']['tr'][$i]['td'][6];
            $model->operation_type = $report_ao['report']['tr'][$i]['td'][7];
            $model->value = $report_ao['report']['tr'][$i]['td'][8];
            $model->currency = $report_ao['report']['tr'][$i]['td'][9];
            $model->comments = $report_ao['report']['tr'][$i]['td'][10];

            $model->created_at = date("Y-m-d H:i:s", strtotime($report_ao['report']['tr'][$i]['td'][2]));
            $model->save(false);
            echo "Success report AO saved!!! acc:{$model->account} \n";
        }
        echo "DONE AO!!! \n";
    }

    /**
     * @param $type
     * @return \app\components\api\ProtraderApi2Responce|bool|mixed
     */
    public function getReport($type)
    {
        $pt_api = ChampStat::getInstance();

        $date1 = new \DateTime('01-09-2016 00:00');
        $date2 = new \DateTime('19-09-2016 23:59');

        return $pt_api->report($type, $date1->getTimestamp(), $date2->getTimestamp(), ChampStat::CHAMP_LOGIN, ChampStat::CHAMP_PASS);
    }
}