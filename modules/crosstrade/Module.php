<?php

namespace app\modules\crosstrade;

use app\modules\crosstrade\components\widgets\charts\assets\ChartAsset;
use yii\base\BootstrapInterface;

/**
 * crosstrade module definition class
 */
class Module extends \yii\base\Module implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\crosstrade\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    public function bootstrap($app)
    {
        if($app instanceof \yii\console\Application){
            $app->controllerMap['cross-stat'] = ['class' => 'app\modules\crosstrade\commands\StatController'];
        }

        ChartAsset::register(\Yii::$app->getView());

        $app->urlManager->addRules([
            '/crosstrade/stats'      => 'crosstrade/crosstrade/index',
            '/crosstrade/stats/<id>' => 'crosstrade/crosstrade/view',
            '/crosstrade/about'      => 'crosstrade/crosstrade/about',
        ]);
    }
}
