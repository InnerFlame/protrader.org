<?php

namespace app\modules\cabinet;

use yii\base\BootstrapInterface;
use yii\web\ForbiddenHttpException;


/**
 * Created by PhpStorm.
 * User: igor
 * Date: 03.02.16
 * Time: 13:31
 */
class CabinetModule extends \yii\base\Module implements BootstrapInterface
{
    public $controllerNamespace = 'app\modules\cabinet\controllers';

    public function init()
    {
        \Yii::configure($this, require(dirname(__FILE__) . '/config/main.php'));
    }

    public function beforeAction($action)
    {
        if (\Yii::$app->user->isGuest || !\Yii::$app->user->identity->isAdmin()) {
            throw new ForbiddenHttpException();
        }
        CabinetBundle::register($action->controller->getView());
        return parent::beforeAction($action);
    }

    public function bootstrap($app)
    {
        \Yii::$app->urlManager->addRules([
            'cabinet/notify/'                                  => 'cabinet/notify/default/index',
            'cabinet/notify/<controller>/<action>'             => 'cabinet/notify/<controller>/<action>',
            'cabinet/user/<controller>/<action>'               => 'cabinet/user/<controller>/<action>',
            'cabinet/<action>'                                 => 'cabinet/default/<action>',
            'cabinet/user/<action:\w+>'                        => 'cabinet/default/user-<action>',
            'cabinet/pages/<action:>'                          => 'cabinet/default/pages-<action>',
            'cabinet/broker/<action:\w+>'                      => 'cabinet/default/broker-<action>',
            'cabinet/forum/<action:\w+>'                       => 'cabinet/default/forum-<action>',
            'cabinet/category/<action:\w+>'                    => 'cabinet/default/category-<action>',
            'cabinet/comment/<action:\w+>'                     => 'cabinet/default/comment-<action>',
            'cabinet/faq-category/<action:\w+>'                => 'cabinet/default/faq-category-<action>',
            'cabinet/improvement/<action:\w+>'                 => 'cabinet/default/improvement-<action>',
            'cabinet/request/<action:\w+>'                     => 'cabinet/default/request-<action>',
            'cabinet/broker-improvement/<action:\w+>'          => 'cabinet/default/broker-improvement-<action>',
            'cabinet/broker-feeds/<action:\w+>'                => 'cabinet/default/broker-feeds-<action>',
            'cabinet/improvement-category/<action:\w+>'        => 'cabinet/default/improvement-category-<action>',
            'cabinet/languages/<action:\w+>'                   => 'cabinet/default/languages-<action>',
            'cabinet/translations/<action:\w+>'                => 'cabinet/default/translations-<action>',
            'cabinet/trash/<action:\w+>'                       => 'cabinet/default/trash-<action>',
            'cabinet/handler/<action:\w+>'                     => 'cabinet/default/handler-<action>',
        ]);
    }
}