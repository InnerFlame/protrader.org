<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2016
 */

namespace app\modules\cabinet;


use yii\web\AssetBundle;

class CabinetBundle extends AssetBundle
{

    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'limitless/assets/js/plugins/forms/wizards/stepy.min.js',
        'limitless/assets/js/plugins/forms/selects/bootstrap_multiselect.js',
        //'https://cdnjs.com/libraries/Chart.js',
    ];
    
    public $css = [
        'css/dashboard.css'
    ];

    public $depends = [
        'app\assets\LimitLessAsset',
    ];
}