<?php
return [
    'components' => [
        //
    ],
    'modules'    => [
        'notify' => [
            'class' => 'app\modules\cabinet\modules\notify\Module',
        ],
        'user'   => [
            'class' => 'app\modules\cabinet\modules\user\Module',
        ],
        'secret' => [
            'class' => 'app\modules\cabinet\modules\secret\Secret',
        ],
        'pages' => [
            'class' => 'app\modules\cabinet\modules\pages\Module',
        ],
        'article' => [
            'class' => 'app\modules\cabinet\modules\article\Module',
        ],
        'trash' => [
            'class' => 'app\modules\cabinet\modules\trash\Module',
        ],
        'codebase' => [
            'class' => 'app\modules\cabinet\modules\codebase\Module',
        ],
        'translate' => [
            'class' => 'app\modules\cabinet\modules\translate\Module',
        ],
        'settings' => [
            'class' => 'app\modules\cabinet\modules\settings\Module',
        ],
        'comments' => [
            'class' => 'app\modules\cabinet\modules\comments\Module',
        ],
        'brokers' => [
            'class' => 'app\modules\cabinet\modules\brokers\Module',
        ],
        'features' => [
            'class' => 'app\modules\cabinet\modules\features\Module',
        ],
        'faq' => [
            'class' => 'app\modules\cabinet\modules\faq\Module',
        ],
        'forum' => [
            'class' => 'app\modules\cabinet\modules\forum\Module',
        ],
        'video' => [
            'class' => 'app\modules\cabinet\modules\video\Module',
        ],
        'news' => [
            'class' => 'app\modules\cabinet\modules\news\Module',
        ],
    ]
];