<?php
/**
 * Created by Zaitsev Oleg.
 * User: php
 * Date: 09.09.15
 * Time: 12:12
 */

namespace app\modules\cabinet\controllers\_traits\tag;


//use app\models\rule\RuleAlias;
use app\models\Tag;
use app\models\user\UserRules;
use app\modules\articles\models\ArtclTag;
use Yii;
use yii\data\ActiveDataProvider;

//use yii\data\ArrayDataProvider;
//use yii\base\ErrorException;
//use yii\filters\AccessControl;
//use yii\filters\VerbFilter;
//use yii\helpers\FileHelper;
//use yii\helpers\Json;
//use yii\helpers\Url;
//use yii\web\BadRequestHttpException;
//use app\components\CustomController;
//use yii\web\ForbiddenHttpException;

//use app\models\user\User;

//use app\models\TypeList;
//use app\models\rule\Rule;

trait TagTrait
{

    /*
        |--------------------------------------------------------------------------
        | Properties
        |--------------------------------------------------------------------------
        */
    public function getTagsParams($string = null)
    {

        if (!empty($string)) {

            switch ($string) {
                case 'indexUrl' :
                    return '/cabinet/tag-index';
                case 'addUrl' :
                    return '/cabinet/tag-add';
            }
        }

        return [
            'indexUrl' => '/cabinet/tag-index',
            'addUrl' => '/cabinet/tag-add',
            'path_to_form' => $this->path_to_form . 'tag/',
            'title' => 'Tags',
            'add_title' => 'Add Tags',
        ];
    }

    /*
    |--------------------------------------------------------------------------
    | Actions
    |--------------------------------------------------------------------------
    */

    public function actionTagIndex()
    {
//        var_dump(1);die();
        $model = ArtclTag::find();

        return $this->render($this->indexUrl, [
            'columns' => Tag::getGridColumns(),
            'dataProvider' => new ActiveDataProvider([
                'query' => $model,
            ]),
            'params' => $this->getTagsParams(),
        ]);
    }

    public function actionTagAdd()
    {
        $model = new ArtclTag();

        if (Yii::$app->request->post())
            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) {
                    return $this->redirect($this->getTagsParams('indexUrl'));
                }
            }

        return $this->render($this->addUrl, [
            'model' => $model,
            'params' => $this->getTagsParams(),
        ]);
    }


    public function actionTagEdit($id)
    {
        if (!$id > 0)
            throw new \yii\web\BadRequestHttpException();

        $model = ArtclTag::findOne((int)$id);

        if (!$model)
            throw new \yii\web\BadRequestHttpException();

        if (Yii::$app->request->post())
            if ($model->load(Yii::$app->request->post())) {
                if ($model->save(false)) {
                    return $this->redirect($this->getTagsParams('indexUrl'));
                }
            }

        return $this->render($this->addUrl, [
            'model' => $model,
            'params' => $this->getTagsParams(),
        ]);
    }

    public function actionTagDelete($id)
    {

        if (!$id > 0) throw new \yii\web\BadRequestHttpException();

        $model = ArtclTag::findOne((int)$id);

        if (!$model) throw new \yii\web\BadRequestHttpException();

        if ($model->delete()) Yii::$app->session->setFlash('success', 'Deleted');

        return $this->redirect($this->getTagsParams('indexUrl'));

    }

    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */

}