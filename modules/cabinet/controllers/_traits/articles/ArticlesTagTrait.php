<?php
/**
 * Created by Zaitsev Oleg.
 * User: php
 * Date: 09.09.15
 * Time: 12:12
 */

namespace app\modules\cabinet\controllers\_traits\articles;


use app\models\user\UserRules;
use app\modules\articles\models\ArtclTag;
use app\modules\articles\models\ArtclTagRelations;
use Yii;
use yii\data\ActiveDataProvider;



trait ArticlesTagTrait
{

    /*
        |--------------------------------------------------------------------------
        | Properties
        |--------------------------------------------------------------------------
        */
    public function getArticlesTagsParams($string = null)
    {

        if (!empty($string)) {

            switch ($string) {
                case 'indexUrl' :
                    return '/cabinet/articles-tag-index';
                case 'addUrl' :
                    return '/cabinet/articles-tag-add';
            }
        }

        return [
            'indexUrl' => '/cabinet/articles-tag-index',
            'addUrl' => '/cabinet/articles-tag-add',
            'deleteUrl' => '/cabinet/articles-tag-delete',
            'path_to_form' => $this->path_to_form . 'articles/tag/',
            'title' => 'Tags',
            'add_title' => 'Add Articles Tags',
        ];
    }

    /*
    |--------------------------------------------------------------------------
    | Actions
    |--------------------------------------------------------------------------
    */

    public function actionArticlesTagIndex()
    {

        $model = ArtclTag::find()->where(['deleted_at' => null])->orderBy('created_at DESC');

        return $this->render($this->indexUrl, [
            'columns' => ArtclTag::getGridColumns(),
            'dataProvider' => new ActiveDataProvider([
                'query' => $model,
            ]),
            'params' => $this->getArticlesTagsParams(),
        ]);
    }

    /**
     * @return mixed
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionArticlesTagAdd()
    {

        $tag = new ArtclTag();


        $post = Yii::$app->request->post();
        if ($post)
            if ($tag->load($post)) {

                if (isset($post['ArtclTag']['article_id']) && is_array($post['ArtclTag']['article_id'])) {

                    $tag->count = count($post['ArtclTag']['article_id']);
                    if ($tag->save()) {

                        foreach ($post['ArtclTag']['article_id'] as $article_id) {

                            $model = new ArtclTagRelations();
                            $model->tag_id = $tag->id;
                            $model->article_id = $article_id;

                            if (!$model->save())
                                throw new \yii\web\BadRequestHttpException();
                        }
                        return $this->redirect($this->getArticlesTagsParams('indexUrl'));

                    } elseif (isset($post['ArtclTag']['article_id'])) {

                        $model = new ArtclTagRelations();
                        $model->tag_id = $tag->id;
                        $model->article_id = $post['ArtclTag']['article_id'];

                        if ($model->save()) {

                            return $this->redirect($this->getArticlesTagsParams('indexUrl'));

                        }
                        throw new \yii\web\BadRequestHttpException();
                    }
                }
            }
        // echo '<pre>'; var_dump($tag->getErrors());die;
        return $this->render($this->addUrl, [
            'model' => $tag,
            'params' => $this->getArticlesTagsParams(),
        ]);
    }


    public function actionArticlesTagEdit($id)
    {
        if (!$id > 0)
            throw new \yii\web\BadRequestHttpException();

        $tag = ArtclTag::findOne((int)$id);

        if (!$tag)
            throw new \yii\web\BadRequestHttpException();

        $post = Yii::$app->request->post();
        if ($post)
            if ($tag->load($post)) {

                if ($tag->save()) {

                    ArtclTagRelations::deleteAll(['tag_id' => $tag->id]);

                    if (isset($post['ArtclTag']['article_id']) && is_array($post['ArtclTag']['article_id'])) {

                        foreach ($post['ArtclTag']['article_id'] as $article_id) {


                            $model = ArtclTagRelations::findOne($article_id);

                            if (!is_object($model))
                                $model = new ArtclTagRelations();

                            $model->tag_id = $tag->id;
                            $model->article_id = $article_id;

                            if (!$model->save())
                                throw new \yii\web\ForbiddenHttpException();


                        }
                        return $this->redirect($this->getArticlesTagsParams('indexUrl'));

                    } elseif (isset($post['ArtclTag']['article_id'])) {

                        $model = new ArtclTagRelations();
                        $model->tag_id = $tag->id;
                        $model->article_id = $post['ArtclTag']['article_id'];

                        if (!$model->save())
                            throw new \yii\web\ForbiddenHttpException();

                        return $this->redirect($this->getArticlesTagsParams('indexUrl'));

                    }
                }

            }

        $tag->article_id = $tag->getArtclTags();

        return $this->render($this->addUrl, [
            'model' => $tag,
            'params' => $this->getArticlesTagsParams(),
        ]);
    }

    public function actionArticlesTagDelete($id)
    {

        if (!$id > 0) throw new \yii\web\BadRequestHttpException();

        $model = ArtclTag::findOne((int)$id);

        if (!$model) throw new \yii\web\BadRequestHttpException();

        if ($model->delete()) Yii::$app->session->setFlash('success', 'Deleted');

        return $this->redirect($this->getArticlesTagsParams('indexUrl'));

    }

    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */

}