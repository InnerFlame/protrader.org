<?php

namespace app\modules\cabinet\controllers\_traits\setting;

use app\components\Entity;
use \app\components\widgets\error\models\ErrorPage;
use yii\data\ActiveDataProvider;
use yii\helpers\VarDumper;
use yii\web\NotFoundHttpException;

trait HandlerTrait {

    public function actionHandlerIndex()
    {

        $pages = ErrorPage::find()->where(['deleted' => Entity::NOT_DELETED]);

        return $this->render($this->indexUrl, [
            'columns' => ErrorPage::getGridColumns(),
            'dataProvider' => new ActiveDataProvider([
                'query' => $pages,
            ]),
            'params' => $this->getErrorPagesParams(),
        ]);
    }

    public function actionHandlerAdd()
    {
        $model = new ErrorPage();

        if (\Yii::$app->request->post())
            if ($model->load(\Yii::$app->request->post())) {
                if ($model->save()) {
                    return $this->redirect('/cabinet/handler-index');
                }
            }

        return $this->render($this->addUrl, [
            'model' => $model,
            'params' => $this->getErrorPagesParams(),
        ]);
    }

    public function actionHandlerEdit($id)
    {
        $model = ErrorPage::findOne($id);
        if(!$model)
            throw new NotFoundHttpException;

        if (\Yii::$app->request->post())
            if ($model->load(\Yii::$app->request->post())) {
                if ($model->save()) {
                    return $this->redirect('/cabinet/handler-index');
                }
            }

        return $this->render($this->addUrl, [
            'model' => $model,
            'params' => $this->getErrorPagesParams(),
        ]);
    }

    public function actionHandlerDelete($id)
    {
        $model = ErrorPage::findOne($id);
        if(!$model)
            throw new NotFoundHttpException;

        if ($model->delete())
            \Yii::$app->session->setFlash('success', 'Deleted');

        return $this->redirect('/cabinet/handler-index');
    }
    public function getErrorPagesParams()
    {
        return [
            'indexUrl' => '/cabinet/handler-index',
            'addUrl' => '/cabinet/handler-add',
            'deleteUrl' => '/cabinet/handler-delete',
            'path_to_form' => $this->path_to_form . 'handler/',
            'title' => 'Page errors handler',
            'add_title' => 'Add error handler',
        ];
    }
}