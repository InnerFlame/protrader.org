<?php
/**
 * Created by Zaitsev Oleg.
 * User: php
 * Date: 09.09.15
 * Time: 12:12
 */

namespace app\modules\cabinet\controllers\_traits\setting;

use app\models\forms\GeneralSettingForm;
use app\models\setting\SettingsCustomField;
use app\models\setting\SettingsCustomValue;
use Yii;
use yii\web\HttpException;

trait SettingTrait
{
    public function getSettingParams($string = null)
    {
        if (!empty($string)) {
            switch ($string) {
                case 'indexUrl' :
                    return '/cabinet/setting';
                case 'addUrl' :
                    return '/cabinet/setting-add';
            }
        }

        return [
            'indexUrl'     => '/cabinet/setting',
            'addUrl'       => '/cabinet/setting-add',
            'path_to_form' => $this->path_to_form . 'setting/',
            'title'        => 'Setting',
            'add_title'    => 'Add Setting',
        ];
    }

    public function getSettingCustomParams($string = null)
    {
        if (!empty($string)) {
            switch ($string) {
                case 'indexUrl' :
                    return '/cabinet/setting';
                case 'addUrl' :
                    return '/cabinet/setting-add';
            }
        }

        return [
            'indexUrl'     => '/cabinet/setting',
            'addUrl'       => '/cabinet/setting-add-custom?attribute=',
            'path_to_form' => $this->path_to_form . 'setting/custom/',
            'title'        => 'Setting',
            'add_title'    => 'Add Setting',
        ];
    }

    public function actionSettingAdd()
    {
        $model = new GeneralSettingForm();
        if (isset($_POST['GeneralSettingForm']['article'])) {
            $model->article = $_POST['GeneralSettingForm']['article'];
            if (Yii::$app->request->post())
                if ($model->load(Yii::$app->request->post())) {
                    $model->saveCustomFields();

                    return $this->redirect($this->getSettingCustomParams('indexUrl'));
                }
        }

        $model->article = SettingsCustomValue::getDataByAttrName('article');

        return $this->render($this->addUrl, [
            'model_settings' => $model,
            'params'         => $this->getSettingParams(),
        ]);
    }

    public function actionSettingAddCustom($attribute, $minHeight, $minWidth)
    {
        if (is_null($attribute))
            return $this->redirect($this->getSettingCustomParams('indexUrl'));

        $model = new GeneralSettingForm();

        $model->image->setValidation($attribute, [
            'extensions' => 'png,jpg',
            'minHeight'  => $minHeight,
            'minWidth'   => $minWidth,
            'maxSize'    => 2048000,
            'minSize'    => 1000,
        ]);

        if (Yii::$app->request->post()) {
            if ($model->load(Yii::$app->request->post())) {
                $model->saveCustomFields($attribute);

                return $this->redirect($this->getSettingCustomParams('indexUrl'));
            }
        }

        $model->$attribute = SettingsCustomValue::getDataByAttrName($attribute);

        return $this->render($this->addUrl, [
            'banner_model' => $model,
            'attribute'    => $attribute,
            //'aspectRatio' => $aspectRatio,
            'params'       => $this->getSettingCustomParams(),
        ]);
    }


    public function actionSetting()
    {
        $model = new GeneralSettingForm();

        if (isset($_POST['GeneralSettingForm']['article'])) {
            $model->article = $_POST['GeneralSettingForm']['article'];
            if (Yii::$app->request->post()) {
                if ($model->load(Yii::$app->request->post())) {
                    $model->saveCustomFields('pictures');
                    $model->saveCustomFields('pic_380_150', $model->image_380_150);

                    return $this->redirect($this->getSettingParams('addUrl'));
                }
            }
        }

        $model->article = SettingsCustomValue::getDataByAttrName('article');
        $model->pictures = SettingsCustomValue::getDataByAttrName('pictures');
        $model->pic_380_150 = SettingsCustomValue::getDataByAttrName('pic_380_150');
        $model->live_chat = SettingsCustomValue::getDataByAttrName('live-chat');
        $model->is_subscription = SettingsCustomValue::getDataByAttrName('is_subscription');

        return $this->render('/setting/setting.twig', [
            'setting' => $model,
            'params'  => $this->getSettingParams(),
        ]);
    }

    public function actionSettingsChat($set)
    {
        $live = SettingsCustomField::find()->where(['alias' => 'live-chat'])->one();
        if (!$live) {
            $live = new SettingsCustomField();
            $live->name = 'live-chat';
            $live->alias = 'live-chat';
            if (!$live->save()) {
                throw new HttpException(401, Yii::t('app', 'Not found custom setting field {field}', ['field' => 'live-chat']));
            }
        }
        $filed = SettingsCustomValue::find()->where(['field_id' => $live->id])->one();
        if (!$filed) {
            $filed = new SettingsCustomValue();
            $filed->field_id = $live->id;
            if (!$filed->save()) {
                throw new HttpException(401, Yii::t('app', 'Create default value for filed {field} is not complete', ['field' => 'live-chat']));
            }
        }
        if ($set == 'on') {
            $filed->value = '1';
            $filed->save();
            Yii::$app->session->setFlash('success', Yii::t('app', 'Live chat was on'));
        }
        if ($set == 'off') {
            $filed->value = '0';
            $filed->save();
            Yii::$app->session->setFlash('success', Yii::t('app', 'Live chat was on'));
        }

        return $this->redirect('/cabinet/setting');
    }

    public function actionSettingsCrosstrade($set)
    {
        $model_field = SettingsCustomField::find()->where(['alias' => 'is_subscription'])->one();
        $model_value = SettingsCustomValue::findOne($model_field->id);
        $model_value->value = ($set == 'on') ? '1' : '0';
        $model_value->save();

        return $this->redirect('/cabinet/setting');
    }
}