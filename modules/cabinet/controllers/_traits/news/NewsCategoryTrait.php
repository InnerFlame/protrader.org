<?php

namespace app\modules\cabinet\controllers\_traits\news;

use app\modules\news\models\NewsCategory;
use Yii;
use yii\data\ActiveDataProvider;

trait NewsCategoryTrait
{


    public function getNewsCategoryParams($string = null)
    {

        if (!empty($string)) {

            switch ($string) {
                case 'indexUrl' :
                    return '/cabinet/news-category-index';
                case 'addUrl' :
                    return '/cabinet/news-category-add';
            }
        }

        return [
            'indexUrl' => '/cabinet/news-category-index',
            'addUrl' => '/cabinet/news-category-add',
            'deleteUrl' => '/cabinet/news-category-delete',
            'path_to_form' => $this->path_to_form . 'news/category/',
            'title' => 'News Category',
            'add_title' => 'Add Category news',
        ];
    }


    public function actionNewsCategoryIndex()
    {
        $model = NewsCategory::modelsForCabinet();

        return $this->render($this->indexUrl, [
            'columns' => NewsCategory::getGridColumns(),
            'dataProvider' => new ActiveDataProvider([
                'query' => $model,
            ]),
            'params' => $this->getNewsCategoryParams(),
        ]);
    }

    public function actionNewsCategoryAdd()
    {
        $model = new NewsCategory();

        if (Yii::$app->request->post())
            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) {
                    return $this->redirect($this->getNewsCategoryParams('indexUrl'));
                }
            }

        return $this->render($this->addUrl, [
            'model' => $model,
            'params' => $this->getNewsCategoryParams(),
        ]);
    }


    public function actionNewsCategoryEdit($id)
    {
        if (!$id > 0)
            throw new \yii\web\BadRequestHttpException();

        $model = NewsCategory::findOne((int)$id);

        if (!$model)
            throw new \yii\web\BadRequestHttpException();

        if (Yii::$app->request->post())
            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) {
                    return $this->redirect($this->getNewsCategoryParams('indexUrl'));
                }
            }

        return $this->render($this->addUrl, [
            'model' => $model,
            'params' => $this->getNewsCategoryParams(),
        ]);
    }

    public function actionNewsCategoryDelete($id)
    {

        if (!$id > 0) throw new \yii\web\BadRequestHttpException();

        $model = NewsCategory::findOne((int)$id);

        if (!$model) throw new \yii\web\BadRequestHttpException();

        if ($model->delete()) Yii::$app->session->setFlash('success', 'Deleted');

        return $this->redirect($this->getNewsCategoryParams('indexUrl'));

    }


}