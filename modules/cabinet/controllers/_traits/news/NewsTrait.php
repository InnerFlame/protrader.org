<?php
/**
 * Created by Zaitsev Oleg.
 * User: php
 * Date: 09.09.15
 * Time: 12:12
 */

namespace app\modules\cabinet\controllers\_traits\news;


//use app\models\rule\RuleAlias;
use app\models\user\UserRules;
use app\modules\news\models\News;
use Yii;
use yii\data\ActiveDataProvider;

trait NewsTrait
{

    /*
        |--------------------------------------------------------------------------
        | Properties
        |--------------------------------------------------------------------------
        */
    public function getNewsParams($string = null)
    {

        if (!empty($string)) {

            switch ($string) {
                case 'indexUrl' :
                    return '/cabinet/news-index';
                case 'addUrl' :
                    return '/cabinet/news-add';
            }
        }

        return [
            'indexUrl' => '/cabinet/news-index',
            'addUrl' => '/cabinet/news-add',
            'deleteUrl' => '/cabinet/news-delete',
            'path_to_form' => $this->path_to_form . 'news/',
            'title' => 'News',
            'add_title' => 'Add news',
        ];
    }

    /*
    |--------------------------------------------------------------------------
    | Actions
    |--------------------------------------------------------------------------
    */

    public function actionNewsIndex()
    {
//        var_dump(1);die();
        $model = News::modelsForCabinet();

        return $this->render($this->indexUrl, [
            'columns' => News::getGridColumns(),
            'dataProvider' => new ActiveDataProvider([
                'query' => $model,
            ]),
            'params' => $this->getNewsParams(),
        ]);
    }

    public function actionNewsAdd()
    {
        $model = new News();

        if (Yii::$app->request->post())
            if ($model->load(Yii::$app->request->post())) {

                if ($model->save()) {
                    return $this->redirect($this->getNewsParams('indexUrl'));
                }
                Yii::$app->session->setFlash('error', 'not saved');
            }

        return $this->render($this->addUrl, [
            'model' => $model,
            'params' => $this->getNewsParams(),
        ]);
    }


    public function actionNewsEdit($id)
    {
        if (!$id > 0)
            throw new \yii\web\BadRequestHttpException();

        $model = News::findOne((int)$id);

        if (!$model)
            throw new \yii\web\BadRequestHttpException();

        if (Yii::$app->request->post())
            if ($model->load(Yii::$app->request->post())) {

                if ($model->update()) {
                    return $this->redirect($this->getNewsParams('indexUrl'));
                }
            }

        return $this->render($this->addUrl, [
            'model' => $model,
            'params' => $this->getNewsParams(),
        ]);
    }

    public function actionNewsDelete($id)
    {

        if (!$id > 0) throw new \yii\web\BadRequestHttpException();

        $model = News::findOne((int)$id);

        if (!$model) throw new \yii\web\BadRequestHttpException();

        if ($model->delete()) Yii::$app->session->setFlash('success', 'Deleted');

        return $this->redirect($this->getNewsParams('indexUrl'));

    }

    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */

}