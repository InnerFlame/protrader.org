<?php
//
//namespace app\modules\cabinet\controllers\_traits\lang;
//
//use app\models\lang\Languages;
//use Yii;
//use yii\data\ActiveDataProvider;
//
//trait LanguagesTrait
//{
//
//    /*
//    |--------------------------------------------------------------------------
//    | Properties
//    |--------------------------------------------------------------------------
//    */
//
//    /*
//    |--------------------------------------------------------------------------
//    | Actions
//    |--------------------------------------------------------------------------
//    */
//
//    public function actionLanguagesIndex()
//    {
//
//        $model = Languages::find();
//
//        return $this->render($this->indexUrl, [
//            'columns' => Languages::getGridColumns(),
//            'dataProvider' => new ActiveDataProvider([
//                'query' => $model,
//            ]),
//            'params' => $this->getLanguagesParams(),
//        ]);
//    }
//
//    public function actionLanguagesAdd()
//    {
//        $model = new Languages();
//
//        if (Yii::$app->request->post())
//            if ($model->load(Yii::$app->request->post())) {
//                //                    var_dump($model->errors);die();
//                if ($model->save()) {
//                    return $this->redirect('index');
//                }
//            }
//
//        return $this->render($this->addUrl, [
//            'model' => $model,
//            'params' => $this->getLanguagesParams(),
//        ]);
//    }
//
//    public function actionLanguagesEdit($id)
//    {
//        if (!$id > 0) throw new \yii\web\BadRequestHttpException();
//
//        $model = Languages::findOne((int)$id);
//
//        if (!$model) throw new \yii\web\BadRequestHttpException();
//
//        if (Yii::$app->request->post())
//            if ($model->load(Yii::$app->request->post())) {
//                if ($model->save()) {
//                    return $this->redirect('index');
//                }
//            }
//
//        return $this->render($this->addUrl, [
//            'model' => $model,
//            'params' => $this->getLanguagesParams(),
//        ]);
//    }
//
//    public function actionLanguagesDelete($id)
//    {
//
//        if (!$id > 0) throw new \yii\web\BadRequestHttpException();
//
//        $model = Languages::findOne((int)$id);
//
//        if (!$model) throw new \yii\web\BadRequestHttpException();
//
//        if ($model->delete()) Yii::$app->session->setFlash('success', 'Deleted');
//
//        return $this->redirect('index');
//    }
//
//    /*
//    |--------------------------------------------------------------------------
//    | Methods
//    |--------------------------------------------------------------------------
//    */
//
//    public function getLanguagesParams()
//    {
//        return [
//            'indexUrl' => '/cabinet/languages/index',
//            'addUrl' => '/cabinet/languages/add',
//            'path_to_form' => $this->path_to_form . 'languages/',
//            'title' => 'Languages',
//            'add_title' => 'Add Languages',
//        ];
//    }
//
//}