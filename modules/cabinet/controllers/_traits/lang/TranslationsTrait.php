<?php

namespace app\modules\cabinet\controllers\_traits\lang;

use app\models\lang\Translations;
use Yii;
use yii\data\ActiveDataProvider;

trait TranslationsTrait
{

    /*
    |--------------------------------------------------------------------------
    | Properties
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | Actions
    |--------------------------------------------------------------------------
    */

    public function actionTranslationsIndex()
    {
        $model = Translations::find();

        return $this->render($this->addUrl, [
            'columns' => Translations::getGridColumns(),
            'dataProvider' => new ActiveDataProvider([
                'query' => $model,
            ]),
            'params' => $this->getTranslationsParams(),
        ]);
    }

    public function actionTranslationsAdd()
    {
        $model = new Translations();

        if (Yii::$app->request->post())
            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) {
                    return $this->redirect('index');
                }
            }

        return $this->render($this->addUrl, [
            'model' => $model,
            'params' => $this->getTranslationsParams(),
        ]);
    }

    public function actionTranslationsEdit($id)
    {
        if (!$id > 0) throw new \yii\web\BadRequestHttpException();

        $model = Translations::findOne((int)$id);

        if (!$model) throw new \yii\web\BadRequestHttpException();

        if (Yii::$app->request->post())
            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) {
                    return $this->redirect('index');
                }
            }

        return $this->render($this->addUrl, [
            'model' => $model,
            'params' => $this->getTranslationsParams(),
        ]);
    }

    public function actionTranslationsDelete($id)
    {

        if (!$id > 0) throw new \yii\web\BadRequestHttpException();

        $model = Translations::findOne((int)$id);

        if (!$model) throw new \yii\web\BadRequestHttpException();

        if ($model->delete()) Yii::$app->session->setFlash('success', 'Deleted');

        return $this->redirect('index');
    }

    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */

    public function getTranslationsParams()
    {
        return [
            'indexUrl' => '/cabinet/translations/index',
            'addUrl' => '/cabinet/translations/add',
            'path_to_form' => $this->path_to_form . 'translations/',
            'title' => 'Translations',
            'add_title' => 'Add Translations',
        ];
    }

}