<?php

namespace app\modules\cabinet\controllers\_traits\video;

use app\models\video\Video;
use Yii;
use yii\data\ActiveDataProvider;

trait VideoTrait
{

    public function getParams($string = null)
    {

        if (!empty($string)) {

            switch ($string) {
                case 'indexUrl' :
                    return '/cabinet/video-index';
                case 'addUrl' :
                    return '/cabinet/video-add';
            }
        }

        return [
            'indexUrl' => '/cabinet/video-index',
            'addUrl' => '/cabinet/video-add',
            'deleteUrl' => '/cabinet/video-delete',
            'path_to_form' => $this->path_to_form . 'video/',
            'title' => 'Video List',
            'add_title' => 'Add Video',
        ];
    }


    public function actionVideoIndex()
    {

        $model = Video::find()->notDeleted();

        return $this->render($this->indexUrl, [
            'columns' => Video::getGridColumns(),
            'dataProvider' => new ActiveDataProvider([
                'query' => $model,
            ]),
            'params' => $this->getParams(),
        ]);
    }


    public function actionVideoAdd()
    {
        $model = new Video();

        if (Yii::$app->request->post())
            if ($model->load(Yii::$app->request->post())) {

                if ($model->save()) {

                    if (isset($_POST['Video']['playlist']))
                        $model->savePlaylists($_POST['Video']['playlist']);

                    return $this->redirect($this->getParams('indexUrl'));
                }

                //echo '<pre>'; var_dump($model, Yii::$app->request->post());die;
            }

        return $this->render($this->addUrl, [
            'model' => $model,
            'params' => $this->getParams(),
        ]);
    }


    public function actionVideoEdit($id)
    {
        if (!$id > 0)
            throw new \yii\web\BadRequestHttpException();

        $model = Video::find()->notDeleted()->where(['id' => $id])->one();

        if (!$model)
            throw new \yii\web\BadRequestHttpException();

        if (Yii::$app->request->post())
            if ($model->load(Yii::$app->request->post())) {

                if ($model->save()) {

                    $model->savePlaylists(Yii::$app->request->post());

                    return $this->redirect($this->getParams('indexUrl'));
                }
            }

        $model->getPlaylists();

        return $this->render($this->addUrl, [
            'model' => $model,
            'params' => $this->getParams(),
        ]);
    }


    public function actionVideoDelete($id)
    {

        if (!$id > 0) throw new \yii\web\BadRequestHttpException();

        $model = Video::find()->notDeleted()->where(['id' => $id])->one();

        if (!$model) throw new \yii\web\BadRequestHttpException();

        if ($model->delete()) Yii::$app->session->setFlash('success', 'Deleted');

        return $this->redirect($this->getParams('indexUrl'));

    }
}