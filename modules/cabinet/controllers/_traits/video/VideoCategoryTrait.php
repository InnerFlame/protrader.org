<?php

namespace app\modules\cabinet\controllers\_traits\video;

use app\components\Entity;
use app\models\video\VdCategory;
use Yii;
use yii\data\ActiveDataProvider;

trait VideoCategoryTrait
{
    public function getVideoCategoryParams($string = null)
    {

        if (!empty($string)) {

            switch ($string) {
                case 'indexUrl' :
                    return '/cabinet/video-category-index';
                case 'addUrl' :
                    return '/cabinet/video-category-add';
            }
        }

        return [
            'indexUrl' => '/cabinet/video-category-index',
            'addUrl' => '/cabinet/video-category-add',
            'deleteUrl' => '/cabinet/video-category-delete',
            'path_to_form' => $this->path_to_form . 'video/category/',
            'title' => 'Video Category List',
            'add_title' => 'Add Video Category',
        ];
    }


    public function actionVideoCategoryIndex()
    {
        $model = VdCategory::find()->notDeleted();

        return $this->render($this->indexUrl, [
            'columns' => VdCategory::getGridColumns(),
            'dataProvider' => new ActiveDataProvider([
                'query' => $model,
            ]),
            'params' => $this->getVideoCategoryParams(),
        ]);
    }

    public function actionVideoCategoryAdd()
    {

        $model = new VdCategory();

        if (Yii::$app->request->post())
            if ($model->load(Yii::$app->request->post())) {
                if ($model->save(false)) {
                    return $this->redirect($this->getVideoCategoryParams('indexUrl'));
                }
            }

        return $this->render($this->addUrl, [
            'model' => $model,
            'params' => $this->getVideoCategoryParams(),
        ]);

    }

    public function actionVideoCategoryEdit($id)
    {
        if (!$id > 0)
            throw new \yii\web\BadRequestHttpException();

        $model = VdCategory::find()->notDeleted()->where(['id' => $id])->one();

        if (!$model)
            throw new \yii\web\BadRequestHttpException();

        if (Yii::$app->request->post())
            if ($model->load(Yii::$app->request->post())) {
                if ($model->save(false)) {
                    return $this->redirect($this->getVideoCategoryParams('indexUrl'));
                }
            }

        return $this->render($this->addUrl, [
            'model' => $model,
            'params' => $this->getVideoCategoryParams(),
        ]);
    }

    public function actionVideoCategoryDelete($id)
    {
        if (!$id > 0) throw new \yii\web\BadRequestHttpException();

        $model = VdCategory::find()->notDeleted()->where(['id' => $id])->one();

        if (!$model) throw new \yii\web\BadRequestHttpException();

        if ($model->delete()) Yii::$app->session->setFlash('success', 'Deleted');

        return $this->redirect($this->getVideoCategoryParams('indexUrl'));
    }

    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */

}