<?php

namespace app\modules\cabinet\controllers\_traits\video;

use app\components\Entity;
use app\models\video\Playlist;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\VarDumper;

trait PlayListTrait
{

    public function getPlayListParams($string = null)
    {

        if (!empty($string)) {

            switch ($string) {
                case 'indexUrl' :
                    return '/cabinet/playlist-index';
                case 'addUrl' :
                    return '/cabinet/playlist-add';

            }

        }

        return [
            'indexUrl' => '/cabinet/playlist-index',
            'addUrl' => '/cabinet/playlist-add',
            'deleteUrl' => '/cabinet/playlist-delete',
            'path_to_form' => $this->path_to_form . 'video/playlist/',
            'title' => 'Play List',
            'add_title' => 'Add Playlist',
        ];
    }

    public function actionPlaylistIndex()
    {

        $model = Playlist::find()->where(['deleted' => Entity::NOT_DELETED]);

        return $this->render($this->indexUrl, [
            'columns' => Playlist::getGridColumns(),

            'dataProvider' => new ActiveDataProvider([
                'query' => $model,
            ]),
            'params' => $this->getPlayListParams(),
        ]);
    }

    public function actionPlaylistAdd()
    {
        $model = new Playlist();

        if (Yii::$app->request->post())
            if ($model->load(Yii::$app->request->post())) {

                if ($model->save()) {
                    $model->saveVideolists(Yii::$app->request->post());
                    return $this->redirect($this->getPlayListParams('indexUrl'));
                }

                //echo '<pre>'; var_dump($model, Yii::$app->request->post());die;
            }

        return $this->render($this->addUrl, [
            'model' => $model,
            'params' => $this->getPlayListParams(),
        ]);
    }


    public function actionPlaylistEdit($id)
    {
        if (!$id > 0)
            throw new \yii\web\BadRequestHttpException();

        $model = Playlist::findOne((int)$id);

        if (!$model)
            throw new \yii\web\BadRequestHttpException();

        if (Yii::$app->request->post())
            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) {
                    //echo '<pre>'; var_dump($model->attributes);die;

                    $model->saveVideolists(Yii::$app->request->post());

                    return $this->redirect($this->getPlayListParams('indexUrl'));
                }
            }

        $model->getVideolists();

        return $this->render($this->addUrl, [
            'model' => $model,
            'params' => $this->getPlayListParams(),
        ]);
    }

    public function actionPlaylistDelete($id)
    {

        if (!$id > 0) throw new \yii\web\BadRequestHttpException();

        $model = Playlist::findOne((int)$id);

        if (!$model) throw new \yii\web\BadRequestHttpException();

        if ($model->delete()) Yii::$app->session->setFlash('success', 'Deleted');

        return $this->redirect($this->getPlayListParams('indexUrl'));

    }
}