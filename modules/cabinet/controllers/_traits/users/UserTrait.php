<?php
//
//namespace app\modules\cabinet\controllers\_traits\users;
//
//use app\models\forms\ChangePasswordForm;
//use app\models\forms\ProfileForm;
//use app\models\user\User;
//use app\models\user\UserCustomField;
//use app\models\user\UserIdentity;
//use app\models\user\UserProfile;
//use Yii;
//use yii\data\ActiveDataProvider;
//use yii\helpers\VarDumper;
//use yii\web\HttpException;
//
//trait UserTrait
//{
//
//    public function actionUserIndex()
//    {
//        $model = User::modelsForCabinet();
//
//        return $this->render($this->indexUrl, [
//            'columns' => User::getGridColumns(),
//            'dataProvider' => new ActiveDataProvider([
//                'query' => $model,
//            ]),
//            'params' => $this->getUserParams(),
//        ]);
//    }
//
//
//    /**
//     * @param $id
//     * @return mixed
//     * @throws HttpException
//     */
//    public function actionUserEdit($id)
//    {
//        if (!$id > 0) throw new HttpException(404);
//
//        $user = UserIdentity::findIdentity($id);
//        $profile = UserProfile::findOne((int)$id);
//        $profile_form = new ProfileForm();
//        $password_form = new ChangePasswordForm();
//
//        $password_form->scenario = ChangePasswordForm::SCENARIO_EMPTY;
//
//        if (!$user) throw new HttpException(404);
//        if (!$profile) $profile = new UserProfile();
//
//        if (!empty(Yii::$app->request->post('ChangePasswordForm')['new_password']) ||
//            !empty(Yii::$app->request->post('ChangePasswordForm')['re_password'])
//        ) {
//
//            $password_form->scenario = ChangePasswordForm::SIGN_UP;
//            $password_form->load(Yii::$app->request->post());
//            $password_form->validate();
//
//        }
//
//
//        if ($this->LoadAndValidate($user, $password_form, $profile, $profile_form)) {
//
//                if ($profile->save()) {
//
//                    $profile_form->saveCustomFields($user->id);
//
//                    Yii::$app->session->setFlash('success', 'Saved');
//
//                    return $this->redirect('index');
//
//                    }
//
//            }
//
//        $profile_form->getValues($id);
//
//        return $this->render($this->addUrl, [
//
//            'user' => $user,
//            'profile' => $profile,
//            'profile_form' => $profile_form,
//            'password_form' => $password_form,
//            'params' => $this->getUserParams(),
//
//        ]);
//    }
//
//    public function actionUserDelete($id)
//    {
//
//        if (!$id > 0) throw new \yii\web\BadRequestHttpException();
//
//        $model = User::findOne((int)$id);
//
//        if (!$model) throw new \yii\web\BadRequestHttpException();
//
//        $model->scenario = User::SCENARIO_DELETE;
//
//        if ($model->delete()) Yii::$app->session->setFlash('success', 'Deleted');
//
//        return $this->redirect('index');
//    }
//
//    /*
//    |--------------------------------------------------------------------------
//    | Methods
//    |--------------------------------------------------------------------------
//    */
//
//    public function LoadAndValidate(&$user, &$password_form, &$profile, &$profile_form)
//    {
//        if (Yii::$app->request->post()) {
//            $user->scenario = UserIdentity::SCENARIO_SIGN_UP;
//
//            if (
//                $user->load(Yii::$app->request->post()) &&
//                $profile->load(Yii::$app->request->post()) &&
//                $profile_form->load(Yii::$app->request->post()) &&
//                $password_form->load(Yii::$app->request->post())
//            ) {
//
//                if ($user->validate() && $profile_form->validate() && $password_form->validate()) {
//
//
//                    if ($user->isNewRecord) {
//                        $user->status = User::STATUS_ACTIVE;
//                        $user->role = User::ROLE_USER;
//                    }
//
//                    $user->fullname = Yii::$app->customFunctions->cleanText($profile->last_name . ' ' . $profile->first_name);
//
//                    if (isset($password_form->new_password))
//                        $user->password_hash = Yii::$app->security->generatePasswordHash($password_form->new_password);
//
//
//                    if ($user->save()) {
//
//                        $profile->user_id = $user->id;
//
//                        return true;
//                    }
//                    var_dump($user);
//                }
//            }
//        }
//
//
//        return false;
//    }
//
//
//    public function getUserParams()
//    {
//        return [
//            'indexUrl' => '/cabinet/user/index',
//            'addUrl' => '/cabinet/user/add',
//            'deleteUrl' => '/cabinet/user/delete',
//            'path_to_form' => $this->path_to_form . 'user/',
//            'title' => 'User',
//            'add_title' => 'Add User',
//        ];
//    }
//
//}