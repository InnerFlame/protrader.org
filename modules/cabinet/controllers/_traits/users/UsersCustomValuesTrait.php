<?php
//
//namespace app\modules\cabinet\controllers\_traits\users;
//
//use app\models\user\UserCustomValue;
//use Yii;
//use yii\data\ActiveDataProvider;
//
//trait UsersCustomValuesTrait
//{
//
//    public function getUsersCustomValuesParams($string = null)
//    {
//
//        if (!empty($string)) {
//
//            switch ($string) {
//                case 'indexUrl' :
//                    return '/cabinet/users-custom-values-index';
//                case 'addUrl' :
//                    return '/cabinet/users-custom-values-add';
//            }
//        }
//
//        return [
//            'indexUrl' => '/cabinet/users-custom-values-index',
//            'addUrl' => '/cabinet/users-custom-values-add',
//            'deleteUrl' => '/cabinet/users-custom-values-delete',
//            'path_to_form' => $this->path_to_form . 'users-custom-values/',
//            'title' => 'Users Custom Values Index',
//            'add_title' => 'Add Users Custom Values Index',
//        ];
//    }
//
//
//    public function actionUsersCustomValuesIndex()
//    {
//        $model = UserCustomValue::find();
//
//        return $this->render($this->indexUrl, [
//            'columns' => UserCustomValue::getGridColumns(),
//            'dataProvider' => new ActiveDataProvider([
//                'query' => $model,
//            ]),
//            'params' => $this->getUsersCustomValuesParams(),
//        ]);
//    }
//
//    public function actionUsersCustomValuesAdd()
//    {
//        $model = new UserCustomValue();
//
//        if (Yii::$app->request->post())
//            if ($model->load(Yii::$app->request->post())) {
//                if ($model->save()) {
//                    return $this->redirect($this->getUsersCustomValuesParams('indexUrl'));
//                }
//            }
//
//        return $this->render($this->addUrl, [
//            'user_custom_value' => $model,
//            'params' => $this->getUsersCustomValuesParams(),
//        ]);
//    }
//
//
//    public function actionUsersCustomValuesEdit($id)
//    {
//        if (!$id > 0)
//            throw new \yii\web\BadRequestHttpException();
//
//        $model = UserCustomValue::findOne((int)$id);
//
//        if (!$model)
//            throw new \yii\web\BadRequestHttpException();
//
//        if (Yii::$app->request->post())
//            if ($model->load(Yii::$app->request->post())) {
//                if ($model->save()) {
//                    return $this->redirect($this->getUsersCustomValuesParams('indexUrl'));
//                }
//            }
//
//        return $this->render($this->addUrl, [
//            'user_custom_value' => $model,
//            'params' => $this->getUsersCustomValuesParams(),
//        ]);
//    }
//
//    public function actionUsersCustomValuesDelete($id)
//    {
//
//        if (!$id > 0) throw new \yii\web\BadRequestHttpException();
//
//        $model = UserCustomValue::findOne((int)$id);
//
//        if (!$model) throw new \yii\web\BadRequestHttpException();
//
//        if ($model->delete()) Yii::$app->session->setFlash('success', 'Deleted');
//
//        return $this->redirect($this->getUsersCustomValuesParams('indexUrl'));
//
//    }
//
//}