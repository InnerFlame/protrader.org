<?php

namespace app\modules\cabinet\controllers\_traits\improvement;
/**
 * Created by PhpStorm.
 * User: P.Bilik
 * Date: 22.03.2016
 * Time: 15:28
 */

use app\components\Entity;
use app\components\widgets\vote\models\Vote;
use app\models\user\User;
use app\modules\cabinet\assets\AssetFeautes;
use app\modules\features\models\FeatureRequest;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

trait RequestTrait {

    public function actionRequestIndex()
    {
        $model = FeatureRequest::find()->where(['deleted' => Entity::NOT_DELETED])->orderBy('created_at DESC');

        AssetFeautes::register($this->getView());

        return $this->render($this->indexUrl, [
            'columns' => FeatureRequest::getGridColumnsBackend(),
            'dataProvider' => new ActiveDataProvider([
                'query' => $model,
            ]),
            'params' => $this->getImprovementRequestParams(),
        ]);
    }

    public function actionRequestAdd()
    {
        $model = new FeatureRequest();

        if (\Yii::$app->request->post())
            if ($model->load(\Yii::$app->request->post())) {
                $model->user_id = \Yii::$app->getUser()->id;
                if ($model->save()) {
                    return $this->redirect('/cabinet/request-index');
                }else{
                    VarDumper::dump($model->errors, 10, true);
                    exit;
                }
            }

        return $this->render($this->addUrl, [
            'model' => $model,
            'params' => $this->getImprovementRequestParams(),
        ]);
    }

    public function actionRequestEdit($id)
    {
        $model = FeatureRequest::findOne($id);
        if(!$model)
            throw new NotFoundHttpException;

        if (\Yii::$app->request->post())
            if ($model->load(\Yii::$app->request->post())) {
                if ($model->save()) {
                    return $this->redirect('/cabinet/request-index');
                }else{
                    VarDumper::dump($model->errors, 10, true);
                    exit;
                }
            }

        return $this->render($this->addUrl, [
            'model' => $model,
            'params' => $this->getImprovementRequestParams(),
        ]);
    }

    public function actionRequestDelete($id)
    {
        $model = FeatureRequest::findOne($id);
        if(!$model)
            throw new NotFoundHttpException;

        if ($model->delete())
            \Yii::$app->session->setFlash('success', 'Deleted');

        return $this->redirect('/cabinet/request-index');
    }
    public function getImprovementRequestParams()
    {
        return [
            'indexUrl' => '/cabinet/request-index',
            'addUrl' => '/cabinet/request-add',
            'deleteUrl' => '/cabinet/request-delete',
            'path_to_form' => $this->path_to_form . 'improvement/request/',
            'title' => 'Request',
            'add_title' => 'Add request',
        ];
    }

    /**
     * Отображает диалоговое окно, для генерации
     * фейковых голосов
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionRequestRender()
    {
        $entity = \Yii::$app->request->post('entity');
        $entity_id = \Yii::$app->request->post('entity_id');
        if(!$entity || $entity_id <= 0){
            throw new BadRequestHttpException;
        }
        return $this->renderAjax('/layouts/features/modal', ['entity' => $entity, 'entity_id' => $entity_id]);
    }

    /**
     * Фильтр пользователй для генерации фейковых голосов.
     * Позволяет искать пользователей не среди всех подряд пользователей,
     * а только среди определенных групп: модеры, участники голосования.
     * Возвращает dataprovider для multiselect
     * @link http://davidstutz.github.io/bootstrap-multiselect/#methods
     *
     * var options = [
            {label: 'username', title: 'username', value: 'id'},
            {label: 'username', title: 'username', value: 'id'},
            {label: 'username', title: 'username', value: 'id'},
            ...
        ];
     *
     * @param $entity
     * @param $entity_id
     * @param bool|false $moderators получить модераторов или админов
     * @param bool|false $already получить только участников голосв
     * @return array
     */
    public function actionRequestFilter($entity, $entity_id, $moderators = false, $already = false)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        //Сбросить фильтр, если не выбран не один чекбокс
        if(!$moderators && !$already){
            $users = User::find()->all();
            $response = [];
            foreach($users as $user){
                $response[] = ['label' => $user->username, 'title' => $user->username, 'value' => $user->id];
            }
        }
        $query = User::find();
        if($moderators){
            $query->andWhere(['in', 'role', [User::ROLE_ADMIN, User::ROLE_MODERATOR]]);
        }
        if($already){
            $votes = Vote::find()->where('entity = :entity AND entity_id = :entity_id', [':entity' => $entity, ':entity_id' => $entity_id])->all();
            $ids = ArrayHelper::getColumn($votes, 'user_id');
            $query->andWhere(['in', 'id', $ids]);
        }

        $users = $query->all();

        $response = [];
        foreach($users as $user){
            $response[] = ['label' => $user->username, 'title' => $user->username, 'value' => $user->id];
        }

        return $response;
    }

    /**
     * Генерирует голоса исходя из переданных ID пользователей
     * и общего количества голосов. Распределяет количество голосов
     * максимально равномерно для выбранных пользователей, формирует
     * результатирующую страницу с текстовыми полями для корректировки
     * и возвращает её.
     * @return string HTML код таблицы
     */
    public function actionRequestCount()
    {
        $count = \Yii::$app->request->post('count');
        $ids = \Yii::$app->request->post('users_list');

        $countUsers = count($ids);
        $balance = $count % $countUsers;
        $count = $count - $balance;
        $users = User::find()->where(['in', 'id', $ids])->all();
        $countVote = $count / $countUsers;

        $votes = [];
        foreach($users as $user){
            $votes[] = ['user' => $user, 'count' => $countVote];
        }
        //Распределяю остаток между юзерами
        while($balance > 0){
            $i = $balance - 1;
            if(isset($votes[$i])){
                $votes[$i]['count']++;
                $balance--;
            }
        }

        \Yii::$app->getSession()->set('votes', $votes);
        return $this->renderAjax('/layouts/features/users-vote-count', ['votes' => $votes, 'count' => $count]);
    }

    /**
     * Сохраняет фейковые голоса после предпросмотра и корректировки
     * пользователем.
     * @return array
     * @throws BadRequestHttpException
     * @throws \yii\db\Exception
     */
    public function actionRequestVoteSave()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        //Сохранение результата
        if(\Yii::$app->getSession()->get('votes')){
            $votes = \Yii::$app->getSession()->get('votes');
            \Yii::$app->getSession()->remove('votes');

            $entity = \Yii::$app->request->post('entity');
            $entity_id = \Yii::$app->request->post('entity_id');
            if(!$entity || $entity_id <= 0 || !is_array($votes)){
                throw new BadRequestHttpException;
            }
            $transaction = \Yii::$app->db->beginTransaction();
            foreach($votes as $vote){
                if(!Vote::upFake($entity, $entity_id, $vote['count'], $vote['user']->id)){
                    $transaction->rollBack();
                    return ['result' => 'ERROR', 'message' => \Yii::t('app', 'Error, voting was not complete')];
                }
            }
            $transaction->commit();
            \Yii::$app->session->setFlash('success', \Yii::t('app', 'Fake voice successfully saved'));

            return ['result' => 'OK', 'message' => \Yii::t('app', 'Voting complete!')];
        }
        return ['result' => 'ERROR', 'message' => \Yii::t('app', 'Bad Request')];
    }

    /**
     * Сбрасывает все фейковые голоса (которые были накручены из админки)
     * дя определенной Entity;
     * @param $entity
     * @param $entity_id
     * @return mixed
     */
    public function actionRequestFakeReset($entity, $entity_id)
    {
        if(Vote::downAllFake($entity, $entity_id)){
            \Yii::$app->session->setFlash('success', \Yii::t('app', 'All fake votes reset'));
        }else{
            \Yii::$app->session->setFlash('error', \Yii::t('app', 'Unable to reset the fake votes'));
        }
        return $this->redirect('/cabinet/request-index');
    }
}