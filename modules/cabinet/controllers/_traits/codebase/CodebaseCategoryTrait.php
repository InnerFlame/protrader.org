<?php
//
//namespace app\modules\cabinet\controllers\_traits\codebase;
//
//use app\modules\codebase\models\Category;
//use Yii;
//use yii\data\ActiveDataProvider;
//use yii\web\HttpException;
//
//trait CodebaseCategoryTrait
//{
//    public function getCodebaseCategoryParams($string = null)
//    {
//        if (!empty($string)) {
//
//            switch ($string) {
//                case 'indexUrl' :
//                    return '/cabinet/codebase-category-index';
//                case 'addUrl' :
//                    return '/cabinet/codebase-category-add';
//            }
//        }
//
//        return [
//            'indexUrl' => '/cabinet/codebase-category-index',
//            'addUrl' => '/cabinet/codebase-category-add',
//            'deleteUrl' => '/cabinet/codebase-category-delete',
//            'path_to_form' => $this->path_to_form . 'codebase/category/',
//            'title' => 'Category List',
//            'add_title' => 'Add Category Codebase',
//        ];
//    }
//
//    /**
//     * @return mixed
//     */
//    public function actionCodebaseCategoryIndex()
//    {
//        $model = Category::modelsForCabinet();
//
//        return $this->render($this->indexUrl, [
//            'columns' => Category::getGridColumns(),
//            'dataProvider' => new ActiveDataProvider([
//                'query' => $model,
//            ]),
//            'params' => $this->getCodebaseCategoryParams(),
//        ]);
//    }
//
//    /**
//     * @return mixed
//     */
//    public function actionCodebaseCategoryAdd()
//    {
//        $model = new Category();
//
//        if (Yii::$app->request->post()) {
//            if ($model->load(Yii::$app->request->post()) && $model->save()) {
//                return $this->redirect($this->getCodebaseCategoryParams('indexUrl'));
//            }
//        }
//
//        return $this->render($this->addUrl, [
//            'model' => $model,
//            'params' => $this->getCodebaseCategoryParams(),
//        ]);
//    }
//
//    /**
//     * @param $id
//     * @return mixed
//     * @throws HttpException
//     */
//    public function actionCodebaseCategoryEdit($id)
//    {
//        $model = Category::findOne((int)$id);
//
//        if (!$model){
//            throw new HttpException(404);
//        }
//
//        if (Yii::$app->request->post()){
//            if ($model->load(Yii::$app->request->post()) && $model->save()) {
//                return $this->redirect($this->getCodebaseCategoryParams('indexUrl'));
//            }
//        }
//
//        return $this->render($this->addUrl, [
//            'model' => $model,
//            'params' => $this->getCodebaseCategoryParams(),
//        ]);
//    }
//
//    /**
//     * @param $id
//     * @return mixed
//     * @throws HttpException
//     * @throws \Exception
//     */
//    public function actionCodebaseCategoryDelete($id)
//    {
//        $model = Category::findOne((int)$id);
//
//        if (!$model){
//            throw new HttpException(404);
//        }
//
//        $model->delete();
//
//        return $this->redirect($this->getCodebaseCategoryParams('indexUrl'));
//    }
//}