<?php
//
//namespace app\modules\cabinet\controllers\_traits\codebase;
//
//use app\models\user\UserRules;
//use app\modules\codebase\models\Description;
//use app\modules\codebase\models\File;
//use app\modules\codebase\models\forms\AddForm;
//use Yii;
//use yii\data\ActiveDataProvider;
//use yii\web\HttpException;
//use yii\web\UploadedFile;
//
//trait CodebaseTrait
//{
//    public function getCodebaseParams($string = null)
//    {
//        if (!empty($string)) {
//            switch ($string) {
//                case 'indexUrl' :
//                    return '/cabinet/codebase-index';
//                case 'addUrl' :
//                    return '/cabinet/codebase-add';
//            }
//        }
//
//        return [
//            'indexUrl'     => '/cabinet/codebase-index',
//            'addUrl'       => '/cabinet/codebase-add',
//            'deleteUrl'    => '/cabinet/codebase-delete',
//            'path_to_form' => $this->path_to_form . 'codebase/',
//            'title'        => 'Codebase List',
//            'add_title'    => 'Add Codebase',
//        ];
//    }
//
//    /**
//     * @return mixed
//     */
//    public function actionCodebaseIndex()
//    {
//        $model = Description::modelsForCabinet();
//
//        return $this->render($this->indexUrl, [
//            'columns'      => Description::getGridColumns(),
//            'dataProvider' => new ActiveDataProvider([
//                'query' => $model,
//            ]),
//            'params'       => $this->getCodebaseParams(),
//        ]);
//    }
//
//    /**
//     * @return mixed
//     */
//    public function actionCodebaseAdd()
//    {
//        $model = new Description();
//
//        if (Yii::$app->request->post()) {
//            if ($model->load(Yii::$app->request->post())) {
//                $model->user_id = Yii::$app->user->identity->id;
//                if ($model->save()) {
//                    return $this->redirect($this->getCodebaseParams('indexUrl'));
//                }
//            }
//        }
//
//        return $this->render($this->addUrl, [
//            'model'  => $model,
//            'params' => $this->getCodebaseParams(),
//        ]);
//    }
//
//    /**
//     * @param $id
//     * @return mixed
//     * @throws HttpException
//     */
//    public function actionCodebaseEdit($id)
//    {
//        $model = Description::findOne((int)$id);
//
//        if (!$model) {
//            throw new HttpException(404);
//        }
//
//        if (Yii::$app->request->post()) {
//            if ($model->load(Yii::$app->request->post())) {
//                if ($model->save()) {
//                    return $this->redirect(Yii::$app->request->referrer);
//                }
//            }
//        }
//
//        return $this->render($this->addUrl, [
//            'model'  => $model,
//            'params' => $this->getCodebaseParams(),
//        ]);
//    }
//
//    /**
//     * @param $id
//     * @return mixed
//     * @throws HttpException
//     * @throws \Exception
//     */
//    public function actionCodebaseDelete($id)
//    {
//        $model = Description::findOne($id);
//
//        if (!$model) {
//            throw new HttpException(404);
//        }
//
//        $model->delete();
//
//        return $this->redirect($this->getCodebaseParams('indexUrl'));
//    }
//
//    /**
//     * @param $id
//     * @return mixed
//     * @throws \Exception
//     */
//    public function actionCodebaseFileDelete($id)
//    {
//        $model = File::findOne($id);
//        if(is_object($model)){
//            $model->delete();
//            $model->description->refreshZipFile();
//        }
//        return $this->redirect(Yii::$app->request->referrer);
//    }
//
//}