<?php

namespace app\modules\cabinet\controllers\_traits\forum;


use app\components\Entity;
use app\modules\forum\models\FrmTopic;
use Yii;
use yii\data\ActiveDataProvider;

trait TopicTrait
{
    /*
    |--------------------------------------------------------------------------
    | Properties
    |--------------------------------------------------------------------------
    */

    public function getTopicParams($string = null)
    {

        if (!empty($string)) {

            switch ($string) {
                case 'indexUrl' :
                    return '/cabinet/topic-index';
                case 'addUrl' :
                    return '/cabinet/topic-add';
            }
        }

        return [
            'indexUrl' => '/cabinet/topic-index',
            'addUrl' => '/cabinet/topic-add',
            'deleteUrl' => '/cabinet/topic-delete',
            'path_to_form' => $this->path_to_form . 'forum/topic/',
            'title' => 'Topic',
            'add_title' => 'Add Topic',
        ];
    }

    /*
    |--------------------------------------------------------------------------
    | Actions
    |--------------------------------------------------------------------------
    */


    public function actionTopicIndex()
    {
        $model = FrmTopic::modelsForCabinet();

        return $this->render($this->indexUrl, [
            'columns' => FrmTopic::getGridColumns(),
            'dataProvider' => new ActiveDataProvider([
                'query' => $model,
            ]),
            'params' => $this->getTopicParams(),
        ]);
    }

    public function actionTopicAdd()
    {
        $model = new FrmTopic();

        if (Yii::$app->request->post())

            $model->scenario = Entity::SCENARIO_DEFAULT;

            if ($model->load(Yii::$app->request->post())) {

                if ($model->save()) {
                    return $this->redirect($this->getTopicParams('indexUrl'));
                }//echo '<pre>'; var_dump($model);die;
            }

        return $this->render($this->addUrl, [
            'model' => $model,
            'params' => $this->getTopicParams(),
        ]);
    }


    public function actionTopicEdit($id)
    {
        if (!$id > 0)
            throw new \yii\web\BadRequestHttpException();

        $model = FrmTopic::findOne((int)$id);

        if (!$model)
            throw new \yii\web\BadRequestHttpException();

        if (Yii::$app->request->post())
            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) {
                    return $this->redirect($this->getTopicParams('indexUrl'));
                }
            }

        return $this->render($this->addUrl, [
            'model' => $model,
            'params' => $this->getTopicParams(),
        ]);
    }

    public function actionTopicDelete($id)
    {

        if (!$id > 0) throw new \yii\web\BadRequestHttpException();

        $model = FrmTopic::findOne((int)$id);

        if (!$model) throw new \yii\web\BadRequestHttpException();

        if ($model->delete(Yii::$app->user->identity->id)) Yii::$app->session->setFlash('success', 'Deleted');

        return $this->redirect($this->getTopicParams('indexUrl'));

    }

    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */


}