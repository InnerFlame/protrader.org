<?php

namespace app\modules\cabinet\controllers\_traits\forum;

use app\components\Entity;
use app\modules\forum\models\FrmCategory;
use Yii;
use yii\data\ActiveDataProvider;

trait CategoryTrait
{
    public function getCategoryParams($string = null)
    {

        //  $forum = 'forum';
        //  $admin = 'admin';
        if (!empty($string)) {

            switch ($string) {
                case 'indexUrl' :
                    //if(Yii::$app->controller->id === $admin)
                    return '/cabinet/category-index';
                //  return '/forum/child?id='.$_GET['id'];
                case 'addUrl' :

                    return '/cabinet/category-add';
            }
        }

        return [
            'indexUrl' => '/cabinet/category-index',
            'addUrl' => '/cabinet/category-add',
            'deleteUrl' => '/cabinet/category-delete',
            'path_to_form' => $this->path_to_form . 'forum/category/',
            'title' => 'Categories',
            'add_title' => 'Add Category',
        ];
    }


    public function actionCategoryIndex()
    {
        $model = FrmCategory::find()//
            ->where(['<>', 'level', FrmCategory::LEVEL_ROOT])
            ->andWhere(['deleted' => Entity::NOT_DELETED])
            ->orderBy('created_at DESC');

        return $this->render($this->indexUrl, [
            'columns' => FrmCategory::getGridColumns(),
            'dataProvider' => new ActiveDataProvider([
                'query' => $model,

            ]),
            'params' => $this->getCategoryParams(),
        ]);
    }

    public function actionCategoryAdd()
    {
        $model = new FrmCategory();

        if ($model->load(Yii::$app->request->post())) {

            $parent = ($model->parent_id) ?
                FrmCategory::findOne($model->parent_id) : FrmCategory::find()->where(['=', 'level', FrmCategory::LEVEL_ROOT])->one();

            if ($model->appendTo($parent)) {

                return $this->redirect($this->getCategoryParams('indexUrl'));
            }
        }

        return $this->render($this->addUrl, [
            'model' => $model,
            'params' => $this->getCategoryParams(),
        ]);
    }

    public function actionCategoryEdit($id)
    {
        if (!$id > 0) throw new \yii\web\BadRequestHttpException();

        $model = FrmCategory::findOne((int)$id);

        if (!$model) throw new \yii\web\BadRequestHttpException();

        if ($model->load(Yii::$app->request->post())) {

            $parent = $model->parent_id ?
                FrmCategory::findOne($model->parent_id) : FrmCategory::find()->where(['=', 'level', FrmCategory::LEVEL_ROOT])->one();

            if ($model->appendTo($parent)) {

                return $this->redirect($this->getCategoryParams('indexUrl'));

            }
        }

        return $this->render($this->addUrl, [
            'model' => $model,
            'params' => $this->getCategoryParams(),
        ]);

    }

    public function actionCategoryDelete($id)
    {
        if (!$id > 0) throw new \yii\web\BadRequestHttpException();

        $model = FrmCategory::findOne((int)$id);

        if (!$model) throw new \yii\web\BadRequestHttpException();

        if ($model->level === FrmCategory::LEVEL_ROOT) throw new \yii\web\BadRequestHttpException();

        //if(count($model->children()->all()) > 0){

        //  Yii::$app->session->setFlash('error', 'В категории есть мануалы. Сначала удалите их');
        // }else{
            if ($model->delete())
                Yii::$app->session->setFlash('success', 'Deleted');
        // }
        // echo '<pre>'; var_dump($model,count($model->children()->all()));die;
        return $this->redirect($this->getCategoryParams('indexUrl'));
    }

    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */

}