<?php

namespace app\modules\cabinet\controllers\_traits\forum;

use app\modules\forum\models\FrmComment;
use app\modules\forum\models\FrmPost;
use app\modules\forum\models\FrmTopic;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\VarDumper;

trait PostTrait
{
    public function getPostParams($string = null)
    {
        if (!empty($string)) {

            switch ($string) {
                case 'indexUrl' :
                    return '/cabinet/post-index';
                case 'addUrl' :
                    return '/cabinet/post-add';
            }
        }

        return [
            'indexUrl' => '/cabinet/post-index',
            'addUrl' => '/cabinet/post-add',
            'deleteUrl' => '/cabinet/post-delete',
            'path_to_form' => $this->path_to_form . 'forum/post/',
            'title' => 'Posts',
            'add_title' => 'Add Post',
        ];
    }

    /**
     * @return mixed
     */
    public function actionPostIndex()
    {
        $model = FrmComment::modelsForCabinet();

        return $this->render($this->indexUrl, [
            'columns' => FrmComment::getGridColumns(),
            'dataProvider' => new ActiveDataProvider([
                'query' => $model,
            ]),
            'params' => $this->getPostParams(),
        ]);
    }


    public function actionPostAdd()
    {
        $model = new FrmComment();

        if (Yii::$app->request->post())
            if ($model->load(Yii::$app->request->post())) {

                $model->published = 1;

                if($model->makeRoot())
                    return $this->redirect($this->getPostParams('indexUrl'));

            }

        return $this->render($this->addUrl, [
            'model' => $model,
            'params' => $this->getPostParams(),
        ]);
    }


    public function actionPostEdit($id)
    {
        if (!$id > 0)
            throw new \yii\web\BadRequestHttpException();

        $model = FrmComment::findOne((int)$id);

        if (!$model)
            throw new \yii\web\BadRequestHttpException();

        if (Yii::$app->request->post())
            if ($model->load(Yii::$app->request->post())) {

                if ($model->save()) {

                    return $this->redirect($this->getPostParams('indexUrl'));
                }
            }

        return $this->render($this->addUrl, [
            'model' => $model,
            'params' => $this->getPostParams(),
        ]);
    }

    public function actionPostDelete($id)
    {
        if (!$id > 0) throw new \yii\web\BadRequestHttpException();

        $model = FrmComment::findOne((int)$id);

        if (!$model) throw new \yii\web\BadRequestHttpException();


        if ($model->delete())
            Yii::$app->session->setFlash('success', 'Message was successfully deleted');
        else
            Yii::$app->session->setFlash('warning', 'Message was not successfully deleted');

        return $this->redirect($this->getPostParams('indexUrl'));
    }
}