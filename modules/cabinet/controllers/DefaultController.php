<?php

namespace app\modules\cabinet\controllers;


use app\components\CustomController;
use app\components\Entity;
use app\models\setting\SettingsCustomField;
use app\models\setting\SettingsCustomValue;
use app\models\user\crosstrade\CrosstradeSubscriber;
use app\models\user\User;
use app\models\user\UserIdentity;
use app\modules\forum\models\FrmComment;
use app\modules\forum\models\FrmTopic;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;


/**
 * Created by PhpStorm.
 * User: igor
 * Date: 03.02.16
 * Time: 13:32
 */
class DefaultController extends CustomController
{
    # NewsIndex, NewsAdd, NewsEdit, NewsDelete, NewsView
    use _traits\news\NewsTrait;

    # NewsCategoryIndex, NewsCategoryAdd, NewsCategoryEdit, NewsCategoryDelete, NewsCategoryView
    use _traits\news\NewsCategoryTrait;

    # ArticlesTagIndex, ArticlesTagAdd, ArticlesTagEdit, ArticlesTagDelete, ArticlesTagView
//    use _traits\articles\ArticlesTagTrait;

    # TagIndex,TagAdd,TagEdit,TagDelete,TagView
    use _traits\tag\TagTrait;

    # CategoryIndex, CategoryAdd, CategoryEdit, CategoryDelete, CategoryView
    use _traits\forum\CategoryTrait;

    # TopicIndex, TopicAdd, TopicEdit, TopicDelete, TopicView
    use _traits\forum\TopicTrait;

    # CommentIndex, CommentAdd, Co
    use _traits\forum\PostTrait;

//    use _traits\improvement\RequestTrait;

    # VideoCategoryIndex, VideoCategoryAdd, VideoCategoryEdit, VideoCategoryDelete, VideoCategoryView
    use _traits\video\VideoCategoryTrait;

    # VideoIndex, VideoAdd, VideoEdit, VideoDelete, VideoView
    use _traits\video\VideoTrait;

    # PlayListIndex, PlayListAdd, PlayListEdit, PlayListDelete, PlayListView
    use _traits\video\PlayListTrait;

    # SettingIndex, SettingAdd, SettingEdit, SettingDelete, SettingAdd,
    use _traits\setting\SettingTrait;

    use _traits\setting\HandlerTrait;

    # LanguagesIndex, LanguagesAdd, LanguagesEdit, LanguagesDelete
    use _traits\lang\TranslationsTrait;

    public $addUrl = '/layouts/edit.twig';
    public $indexUrl = '/layouts/index.twig';
    public $path_to_form = '@cab_views/';

    public $search_visible = false;

    /**
     * @var int режим работы 0 - subscribe 1 - registrate
     */
    public $mode = 0;

    /**
     * @return array
     */
    public function behaviors()
    {

        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => [\app\models\user\User::ROLE_ADMIN],
                    ],
                ],
            ],
        ];

    }


    /**
     * Return error twig array
     *
     * @return array
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'app\components\FlatErrorAction',
                'view' => 'error.twig'
            ],
            'image-upload' => [
                'class' => 'app\components\ImageUploadAction',
                'width' => 650,
                'url' => '/upload/', // Directory URL address, where files are stored.
                'path' => \Yii::getAlias('@app') . '/web/upload' // Or absolute path to directory where files are stored.
            ],
        ];
    }


    public function init()
    {
        $this->breadcrumbs[] = [
            'label' => 'Admin Panel',
            'url' => '/cabinet/',
        ];

        $this->url = [
            'base' => '/cabinet',
            'topnav' => [
                'label' => 'Site',
                'link' => '/'
            ],
        ];
        return parent::init();
    }

    /**
     * Index action Show Cabinet Dashboard and MainMenu
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $models = CrosstradeSubscriber::find()->where(['status' => CrosstradeSubscriber::STATUS_REGISTERED])->all();
        $newcomersCount = $this->getNewComerces($models);
        $progress = $this->calcualateProgress($models);

        $field = SettingsCustomField::find()->where(['alias' => 'is_subscription'])->one();
        $settings = SettingsCustomValue::findOne($field->id);
        $this->mode = $settings->value;

        $params = ['models' => $models, 'newcomersCount' => $newcomersCount, 'progress' => $progress, 'self' => $this];
        if(\Yii::$app->request->isAjax){
            return $this->render('_counters.twig', $params);
        }
        return $this->render('index.twig', $params);
    }


    /**
     * Считает количество пользователей, которые зарегестрированы
     * на сайте и активировали свои аккаунты, на основании
     * зарегестрерированных на сайте
     * @param $models CrosstradeSubscriber[]
     * @return int
     */
    public function getCountUsersFullRegistered($models)
    {
        $ids = ArrayHelper::getColumn($models, 'user_id');

        return User::find()->where(['in', 'id', $ids])->andWhere(['status' => UserIdentity::STATUS_ACTIVE])->andWhere(['deleted' => Entity::NOT_DELETED])->count();
    }

    /**
     * Количество постов для топиков за сегодня
     * @return int
     */
    public function getCountRepliesForumToday()
    {
        $comments = FrmComment::find()->where('UNIX_TIMESTAMP(CURRENT_DATE()) < frm_comments.created_at')->andWhere(['deleted' => Entity::NOT_DELETED])->all();
        return count($comments);
    }

    /**
     * Общее количество топиков, которое было создано
     * за сегодня
     * @return int
     */
    public function getCountTopicsToday()
    {
        $topics = FrmTopic::find()->where('UNIX_TIMESTAMP(CURRENT_DATE()) < frm_topics.created_at')->andWhere(['deleted' => Entity::NOT_DELETED])->all();
        return count($topics);

    }
    /**
     * Возвращает пользователей, которые зарегестрированы сегодня
     * а так же были активированы
     * @return int
     */
    public function getCountTodayUsers()
    {
        $users = User::find()->where('UNIX_TIMESTAMP(CURRENT_DATE()) < user.created_at')->andWhere(['status' => UserIdentity::STATUS_ACTIVE])->all();
        return count($users);
    }
    /**
     * Возвращает общее количество аккаунтов зарегестрированных
     * и активированных, а так же которые не удалены.
     * @return int
     */
    public function getCountUsers()
    {
        $users = User::find()->where(['status' => UserIdentity::STATUS_ACTIVE])->andWhere(['deleted' => Entity::NOT_DELETED])->all();
        return count($users);
    }

    /**
     * Возвращает финальное количество подписчиков
     * @return int
     */
    public function getCountFinish()
    {
        return 1000;
    }

    /**
     * Счатает количество только тех, которые
     * зарегистрировались вместе с подпиской
     * @param $models
     * @return int
     */
    private function getNewComerces($models)
    {
        $count = 0;
        foreach ($models as $model) {
            if($model->is_registered){
                $count++;
            }
        }
        return $count;
    }

    /**
     * Считает прогресс до достижения цели в 3000 подписчиков
     * @param $models
     * @return float
     */
    private function calcualateProgress($models)
    {
        $total = count($models);
        $progress = $total*100/$this->getCountFinish();
        return ceil($progress);
    }

    /**
     * Считает и возвращает данные для чарта, общего количества юзеров, которые
     * были зарегестрированы на чемпионат
     * @return array ['dates' => ["2016-08-24","2016-08-25","2016-08-26"], 'counts' => ["1","3","18"]]
     */
    public function getDataChartForSubscribers()
    {
        $sql = "SELECT count(*) AS `count`, DATE(`created_at`) as `date` FROM crosstrade_subscriber WHERE `created_at` IS NULL %s";
        $nulls = \Yii::$app->getDb()->createCommand($this->prepareSqlByStatus($sql))->queryOne();
        $sql = "SELECT count(*) AS `count`, DATE(`created_at`) as `date` FROM crosstrade_subscriber WHERE `created_at` IS NOT NULL AND `created_at` > '2016-09-01 16:00:00' %s GROUP BY `date` ORDER BY `date` ASC";
        $results = \Yii::$app->getDb()->createCommand($this->prepareSqlByStatus($sql))->queryAll();
        return $this->prepareDataForCharts($results,$nulls);
    }

    /**
     * Считает и возвращает данные для чарта, которые
     * были зарегестрированы на чемпионат и зарегестрировались
     * в системе специально для участия
     * @return array ['dates' => ["2016-08-24","2016-08-25","2016-08-26"], 'counts' => ["1","3","18"]]
     */
    public function getDataChartForSubscribersNewComers()
    {
        $sql = "SELECT count(*) AS `count`, DATE(`created_at`) as `date` FROM crosstrade_subscriber WHERE is_registered = 1 AND `created_at` IS NULL %s";
        $nulls = \Yii::$app->getDb()->createCommand($this->prepareSqlByStatus($sql))->queryOne();
        $sql = "SELECT count(*) AS `count`, DATE(`created_at`) as `date` FROM crosstrade_subscriber WHERE is_registered = 1 AND `created_at` IS NOT NULL AND `created_at` > '2016-09-01 16:00:00' %s GROUP BY `date` ORDER BY `date` ASC";
        $results = \Yii::$app->getDb()->createCommand($this->prepareSqlByStatus($sql))->queryAll();
        return $this->prepareDataForCharts($results, $nulls);
    }

    /**
     * Подготавливает данные для чартов. Так как статистика ведется
     * уже 16 дней, а поле дата добавленна только сегодня, было
     * предложено общее количество записей без даты разделить на
     * 16 дней и получить средний, общий, показатель. Вставить его
     * первым элементом в чарте.
     * @param $results ['dates' => ["2016-08-24","2016-08-25","2016-08-26"], 'counts' => ["1","3","18"]]
     * @param $nulls [count => 20, date => null]
     * @return array
     */
    protected function prepareDataForCharts($results, $nulls)
    {
        $dates = ArrayHelper::getColumn($results, 'date');
        array_unshift($dates, $nulls['date']);

        $counts = ArrayHelper::getColumn($results, 'count');
        $null = $nulls['count'] / 16;
        array_unshift($counts, $null);


        $data = [];
        $data['dates'] = Json::encode(ArrayHelper::getColumn($results, 'date'));
        $data['counts'] = Json::encode(ArrayHelper::getColumn($results, 'count'));

        return $data;
    }

    /**
     * В зависимости от режима работы dashboard
     * выбираются разные данные. Процесс выполняется
     * дополнением запроса
     * @param $sql "SELECT count(*) AS `count`, DATE(`created_at`) as `date` FROM crosstrade_subscriber WHERE is_registered = 1 AND `created_at` IS NOT NULL %s"
     * @return string
     */
    protected function prepareSqlByStatus($sql)
    {
        if ($this->mode == 0) {
            $sql = sprintf($sql, 'AND `status`  = "2"');
            return $sql;
        }
        $sql = sprintf($sql, 'AND `status`  = "1"');
        return $sql;
    }


}