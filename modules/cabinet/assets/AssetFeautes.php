<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2016
 */

namespace app\modules\cabinet\assets;


class AssetFeautes extends AssetCabinet
{
    public $js = [
        'features/main.js',
        'plugins/jquery.validate.min.js'
    ];
}