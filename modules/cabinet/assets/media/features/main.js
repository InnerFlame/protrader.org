/**
 * Created by P.Bilik on 25.05.2016.
 */

function show(entity, entity_id){
    $.ajax({
        type: 'POST',
        url: '/cabinet/features/request/render',
        data:{
            'entity': entity,
            'entity_id':entity_id
        },
        success: function(data){
            $('#modal_default').html(data);
            $('#modal_default').modal('show');
        },
    });
}