/**
 * Created by P.Bilik on 25.05.2016.
 */
function voteUp(entity, entity_id, selector, selector_fake)
{
    if(BROKER_ACTION && BROKER_ACTION.UP){
        $.ajax({
            type: 'POST',
            url: BROKER_ACTION.UP,
            data: {
                'entity':entity,
                'entity_id': entity_id
            },
            success: function(data){
                if(selector){
                    $(selector).html(JSON.parse(data).count_like);
                }
                if(selector_fake){
                    $(selector_fake).html(JSON.parse(data).count_like_fake);
                }
            },
        });
    }
}
function voteDown(entity, entity_id, selector, selector_fake)
{
    if(BROKER_ACTION && BROKER_ACTION.DOWN){
        $.ajax({
            type: 'POST',
            url: BROKER_ACTION.DOWN,
            data: {
                'entity':entity,
                'entity_id': entity_id
            },
            success: function(data){
                if(selector){
                    $(selector).html(JSON.parse(data).count_like);
                }
                if(selector_fake){
                    $(selector_fake).html(JSON.parse(data).count_like_fake);
                }
            },
        });
    }
}