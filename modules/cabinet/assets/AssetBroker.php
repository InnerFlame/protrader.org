<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2016
 */

namespace app\modules\cabinet\assets;


class AssetBroker extends AssetCabinet
{
    public $js = [
        'broker/js/vote.js'
    ];

}