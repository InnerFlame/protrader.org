<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2016
 */

namespace app\modules\cabinet\assets;


use yii\web\AssetBundle;

class AssetCabinet extends AssetBundle
{
    public $sourcePath = '@app/modules/cabinet/assets/media';

    public $js = [
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];
}