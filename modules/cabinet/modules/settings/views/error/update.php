<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\components\widgets\error\models\ErrorPage */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Error Page',
]) . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Error Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="error-page-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
