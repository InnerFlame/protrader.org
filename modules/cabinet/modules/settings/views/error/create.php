<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\components\widgets\error\models\ErrorPage */

$this->title = Yii::t('app', 'Create Error Page');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Error Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="error-page-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
