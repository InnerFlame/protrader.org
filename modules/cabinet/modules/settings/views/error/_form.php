<?php

use app\components\langInput\LangInputWidget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\components\widgets\error\models\ErrorPage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="error-page-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= LangInputWidget::widget(['model' => $model, 'type' => 'text_input', 'form' => $form, 'attribute' => 'title']) ?>

    <?= $form->field($model, 'expression')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->radioList([0 => '404', 1 => '301']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
