<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\components\ceo\CeoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'SEO');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ceo-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'entity',
                'format'    => 'raw',
                'value'     => function ($model) {
                    if (is_object($model->object))
                        if (isset($model->object->title))
                            return $model->object->title;

                    if (isset($model->object->name))
                        return $model->object->name;
                },
            ],
            [
                'attribute' => 'title',
                'format'    => 'raw',
                'value'     => function ($model) {
                    if ($model->title)
                        return 'YES';
                },
            ],
            [
                'attribute' => 'description',
                'format'    => 'raw',
                'value'     => function ($model) {
                    if ($model->description)
                        return 'YES';
                },
            ],
            [
                'attribute' => 'keywords',
                'format'    => 'raw',
                'value'     => function ($model) {
                    if ($model->keywords)
                        return 'YES';
                },
            ],
            [
                'attribute' => 'canonical',
                'format'    => 'raw',
                'value'     => function ($model) {
                    if ($model->canonical)
                        return 'YES';
                },
            ],

            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update}',
                'buttons'  => [
                    'update' => function ($url, $model) {
                        $options = [
                            'class'      => 'btn-update',
                            'title'      => Yii::t('yii', 'Update'),
                            'aria-label' => Yii::t('yii', 'Update'),
                            'data-pjax'  => '0',
                        ];
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
                    },
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
