<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\components\ceo\Ceo */

$this->title = Yii::t('app', 'Create Ceo');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ceos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ceo-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
