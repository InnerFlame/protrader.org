<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\langInput\LangInputWidget;

/* @var $this yii\web\View */
/* @var $model app\components\ceo\Ceo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ceo-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= LangInputWidget::widget(['model' => $model, 'type' => 'text_input', 'form' => $form, 'attribute' => 'title']) ?>

    <?= LangInputWidget::widget(['model' => $model, 'type' => 'text_input', 'form' => $form, 'attribute' => 'description']) ?>

    <?= LangInputWidget::widget(['model' => $model, 'type' => 'text_input', 'form' => $form, 'attribute' => 'keywords']) ?>

    <?= LangInputWidget::widget(['model' => $model, 'type' => 'text_input', 'form' => $form, 'attribute' => 'canonical']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
