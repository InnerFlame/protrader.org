<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\user\User;
use app\components\twig\CustomFilters;

/* @var $this yii\web\View */
/* @var $model app\models\community\Comments */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="comments-form">

    <?php $form = ActiveForm::begin(); ?>

    <label><?= Yii::t('app', 'Add Author') ?></label>
    <?= CustomFilters::selectFilter(User::getList(), $model->user_id, 'Comments[user_id]', false)?>

    <?= $form->field($model, 'entity')->dropDownList(\app\components\Entity::getMap()) ?>

    <?= $form->field($model, 'entity_id')->textInput() ?>

    <?= $form->field($model, 'content')->textarea(['rows' => 15, 'cols' => 20]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
