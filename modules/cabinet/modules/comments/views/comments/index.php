<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\community\CommentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Comments');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comments-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<!--    <p>-->
<!--        <?//= Html::a(Yii::t('app', 'Create Comments'), ['create'], ['class' => 'btn btn-success']) ?>-->
<!--    </p>-->
    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'created_at',
                'format'    => 'date',
            ],
            'id',
            [
                'attribute' => 'entity',
                'format'    => 'raw',
                'value'     => function ($model) {
                    if (is_object($model->getEntity()))
                        return '<a class="entity-link" href ="' . $model->getEntity()->getViewUrl() . '">' . $model->getEntity()->title . '</a>';
                },
            ],
            [
                'attribute' => 'content',
                'format'    => 'html',
                'value'     => function ($model) {
                    return substr($model->content, 0, 20);
                },
            ],
            [
                'attribute' => 'user_id',
                'format'    => 'raw',
                'value'     => function ($model) {
                    if (is_object($model->user))
                        return $model->user->getLinkUsername();
                },
            ],
            [
                'attribute' => 'parent',
                'format'    => 'raw',
                'value'     => function ($model) {
                    $parent = $model->parents(1)->one();

                    if (is_object($model->getEntity()) && is_object($parent))
                        return '<a class="entity-link" href ="' . $model->getEntity()->getViewUrl() . '#' . $parent->id . '">Link to parent comment</a>';

                    if (is_object($model->getEntity()))
                        return '<a class="entity-link" href ="' . $model->getEntity()->getViewUrl() . '#' . $model->id . '">Link to comment</a>';

                    return 0;
                },
            ],

            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'buttons'  => [
                    'update' => function ($url, $model) {
                        $options = [
                            'class'      => 'btn-update',
                            'title'      => Yii::t('yii', 'Update'),
                            'aria-label' => Yii::t('yii', 'Update'),
                            'data-pjax'  => '0',
                        ];
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
                    },
                    'delete' => function ($url, $model) {
                        $options = [
                            'class'        => 'btn-delete',
                            'title'        => Yii::t('yii', 'Delete'),
                            'aria-label'   => Yii::t('yii', 'Delete'),
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                            'data-method'  => 'post',
                            'data-pjax'    => '0',
                        ];
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
                    }
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
