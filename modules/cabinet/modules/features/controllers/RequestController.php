<?php

namespace app\modules\cabinet\modules\features\controllers;

use app\components\widgets\vote\models\Vote;
use app\models\user\User;
use app\modules\cabinet\assets\AssetFeautes;
use Yii;
use app\modules\features\models\FeatureRequest;
use app\modules\features\models\FeatureRequestSearch;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * RequestController implements the CRUD actions for FeatureRequest model.
 */
class RequestController extends Controller
{
    public $layout = '@cab_views/layouts/main.twig';

    public function actions()
    {
        return [
            'image-upload' => [
                'class' => 'app\components\ImageUploadAction',
                'url'   => '/upload/feature-request', // Directory URL address, where files are stored.
                'path'  => \Yii::getAlias('@app') . '/web/upload/feature-request' // Or absolute path to directory where files are stored.
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FeatureRequest models.
     * TODO FeatureRequestSearch добавить notDeleted()
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FeatureRequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        AssetFeautes::register($this->getView());

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new FeatureRequest model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FeatureRequest();

        if ($model->load(Yii::$app->request->post())) {
            $model->user_id = \Yii::$app->getUser()->id;
            if($model->save()) {
                return $this->redirect(['index']);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing FeatureRequest model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing FeatureRequest model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Отображает диалоговое окно, для генерации
     * фейковых голосов
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionRender()
    {
        $entity = \Yii::$app->request->post('entity');
        $entity_id = \Yii::$app->request->post('entity_id');
        if(!$entity || $entity_id <= 0){
            throw new BadRequestHttpException;
        }
        return $this->renderAjax('modal', ['entity' => $entity, 'entity_id' => $entity_id]);
    }

    /**
     * Фильтр пользователй для генерации фейковых голосов.
     * Позволяет искать пользователей не среди всех подряд пользователей,
     * а только среди определенных групп: модеры, участники голосования.
     * Возвращает dataprovider для multiselect
     * @link http://davidstutz.github.io/bootstrap-multiselect/#methods
     *
     * var options = [
    {label: 'username', title: 'username', value: 'id'},
    {label: 'username', title: 'username', value: 'id'},
    {label: 'username', title: 'username', value: 'id'},
    ...
    ];
     *
     * @param $entity
     * @param $entity_id
     * @param bool|false $moderators получить модераторов или админов
     * @param bool|false $already получить только участников голосв
     * @return array
     */
    public function actionFilter($entity, $entity_id, $moderators = false, $already = false)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        //Сбросить фильтр, если не выбран не один чекбокс
        if(!$moderators && !$already){
            $users = User::find()->all();
            $response = [];
            foreach($users as $user){
                $response[] = ['label' => $user->username, 'title' => $user->username, 'value' => $user->id];
            }
        }
        $query = User::find();
        if($moderators){
            $query->andWhere(['in', 'role', [User::ROLE_ADMIN, User::ROLE_MODERATOR]]);
        }
        if($already){
            $votes = Vote::find()->where('entity = :entity AND entity_id = :entity_id', [':entity' => $entity, ':entity_id' => $entity_id])->all();
            $ids = ArrayHelper::getColumn($votes, 'user_id');
            $query->andWhere(['in', 'id', $ids]);
        }

        $users = $query->all();

        $response = [];
        foreach($users as $user){
            $response[] = ['label' => $user->username, 'title' => $user->username, 'value' => $user->id];
        }

        return $response;
    }

    /**
     * Генерирует голоса исходя из переданных ID пользователей
     * и общего количества голосов. Распределяет количество голосов
     * максимально равномерно для выбранных пользователей, формирует
     * результатирующую страницу с текстовыми полями для корректировки
     * и возвращает её.
     * @return string HTML код таблицы
     */
    public function actionCount()
    {
        $count = \Yii::$app->request->post('count');
        $ids = \Yii::$app->request->post('users_list');

        $countUsers = count($ids);
        $balance = $count % $countUsers;
        $count = $count - $balance;
        $users = User::find()->where(['in', 'id', $ids])->all();
        $countVote = $count / $countUsers;

        $votes = [];
        foreach($users as $user){
            $votes[] = ['user' => $user, 'count' => $countVote];
        }
        //Распределяю остаток между юзерами
        while($balance > 0){
            $i = $balance - 1;
            if(isset($votes[$i])){
                $votes[$i]['count']++;
                $balance--;
            }
        }

        \Yii::$app->getSession()->set('votes', $votes);
        return $this->renderAjax('users-vote-count', ['votes' => $votes, 'count' => $count]);
    }

    /**
     * Сохраняет фейковые голоса после предпросмотра и корректировки
     * пользователем.
     * @return array
     * @throws BadRequestHttpException
     * @throws \yii\db\Exception
     */
    public function actionVoteSave()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        //Сохранение результата
        if(\Yii::$app->getSession()->get('votes')){
            $votes = \Yii::$app->getSession()->get('votes');
            \Yii::$app->getSession()->remove('votes');

            $entity = \Yii::$app->request->post('entity');
            $entity_id = \Yii::$app->request->post('entity_id');
            if(!$entity || $entity_id <= 0 || !is_array($votes)){
                throw new BadRequestHttpException;
            }
            $transaction = \Yii::$app->db->beginTransaction();
            foreach($votes as $vote){
                if(!Vote::upFake($entity, $entity_id, $vote['count'], $vote['user']->id)){
                    $transaction->rollBack();
                    return ['result' => 'ERROR', 'message' => \Yii::t('app', 'Error, voting was not complete')];
                }
            }
            $transaction->commit();
            \Yii::$app->session->setFlash('success', \Yii::t('app', 'Fake voice successfully saved'));

            return ['result' => 'OK', 'message' => \Yii::t('app', 'Voting complete!')];
        }
        return ['result' => 'ERROR', 'message' => \Yii::t('app', 'Bad Request')];
    }

    /**
     * Сбрасывает все фейковые голоса (которые были накручены из админки)
     * дя определенной Entity;
     * @param $entity
     * @param $entity_id
     * @return mixed
     */
    public function actionFakeReset($entity, $entity_id)
    {
        if(Vote::downAllFake($entity, $entity_id)){
            \Yii::$app->session->setFlash('success', \Yii::t('app', 'All fake votes reset'));
        }else{
            \Yii::$app->session->setFlash('error', \Yii::t('app', 'Unable to reset the fake votes'));
        }
        return $this->redirect(['index']);
    }

    /**
     * Возвращает голоса пользователей
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionReturnVotes($id)
    {
        $model = FeatureRequest::find()->where(['id' => $id])->one();
        if($model->return_votes == 1){
            \Yii::$app->session->setFlash('success', \Yii::t('app', 'Voices have already been returned'));
        }
        if(!$model)
            throw new NotFoundHttpException;

        if($model->returnVotes()){
            \Yii::$app->session->setFlash('success', \Yii::t('app', 'Voice users have been returned'));

        }else{
            \Yii::$app->session->setFlash('error', \Yii::t('app', 'Unable to return to vote'));
        }
        return $this->redirect(['index']);
    }
    /**
     * Finds the FeatureRequest model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FeatureRequest the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FeatureRequest::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
