<?php

namespace app\modules\cabinet\modules\features\controllers;

use app\components\Entity;
use app\modules\brokers\models\FeatureSlide;
use Yii;
use app\modules\brokers\models\Improvement;
use app\modules\brokers\models\ImprovementSearch;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FeaturesController implements the CRUD actions for Improvement model.
 */
class FeaturesController extends Controller
{
    public $layout = '@cab_views/layouts/main.twig';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Improvement models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ImprovementSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Improvement model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Improvement();

        if ($model->load(Yii::$app->request->post()) && $this->saveModel($model)) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Improvement model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $this->saveModel($model)) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Improvement model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param $feature_id
     * @param null $id
     * @return string|\yii\web\Response
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionEditSlide($feature_id, $id = null)
    {
        if (!$feature_id > 0)
            throw new \yii\web\BadRequestHttpException();

        $model = FeatureSlide::find()->where(['entity' => Entity::FEATURES, 'entity_id' => $feature_id, 'id' => (int)$id])->one();

        if (!$model)
            $model = new FeatureSlide;

        if ($model->load(Yii::$app->request->post())) {
            $model->entity_id = $feature_id;
            $model->entity = Entity::FEATURES;

            if ($model->save()) {
                return $this->redirect(['index']);
            }
        }

        return $this->render('/features/slider/update', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionDeleteSlide($id)
    {
        if (!$id > 0)
            throw new \yii\web\BadRequestHttpException();

        $model = FeatureSlide::find()->where(['entity' => Entity::FEATURES, 'id' => (int)$id])->one();

        if (!$model)
            throw new \yii\web\BadRequestHttpException();

        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Improvement model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Improvement the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Improvement::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    /*
     * $return model
     */
    public function saveModel($model)
    {
        if ($model->status == Improvement::STATUS_ACTIVE && $model->published_at == null) {
            $model->published_at = time();
        }
        return $model->save();
    }
}
