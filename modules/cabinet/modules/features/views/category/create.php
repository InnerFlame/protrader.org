<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\brokers\models\ImprovementsCategory */

$this->title = Yii::t('app', 'Create Improvements Category');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Features Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="improvements-category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
