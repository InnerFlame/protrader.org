<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\brokers\models\ImprovementsCategory */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Features Category',
]) . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Improvements Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="improvements-category-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
