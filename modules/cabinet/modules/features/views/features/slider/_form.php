<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\customImage\CustomImageUpload;

/* @var $this yii\web\View */
/* @var $model app\modules\brokers\models\Improvement */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="improvement-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'weight')->textInput(['maxlength' => 255]) ?>

    <?= CustomImageUpload::widget(['form' => $form, 'model' => $model, 'attribute' => 'pictures', 'options' => ['id' => 'pictures']]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php if(!$model->isNewRecord):?>
    <div class="form-group">
        <?= Html::a(Yii::t('app', 'Delete'), Yii::$app->urlManager->createUrl(['/cabinet/features/features/delete-slide', 'id' => $model->id]), ['class' => 'btn btn-danger']) ?>
    </div>
    <?php endif;?>
    <?php ActiveForm::end(); ?>

</div>
