<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\brokers\models\Improvement */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Improvement',
]) . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Improvements'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="improvement-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
