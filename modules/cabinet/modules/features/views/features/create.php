<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\brokers\models\Improvement */

$this->title = Yii::t('app', 'Create Feature');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Improvements'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="improvement-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
