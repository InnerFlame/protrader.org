<?php

use app\modules\brokers\models\Improvement;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\brokers\models\ImprovementSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Features');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="improvement-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Feature'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'category_id',
                'value'     => function ($model) {

                    if (isset($model->category))
                        return $model->category->title;
                },
                'format'    => 'raw',
            ],
            [
                'attribute' => 'title',
                'format'    => 'raw',
            ],

            [
                'attribute' => 'status',
                'value'     => function ($model) {
                    return Improvement::getStatus($model->status);
                },
                'format'    => 'raw',
            ],
            [
                'attribute' => 'is_main',
                'value'     => function ($model) {
                    if ($model->is_main) {
                        return '<span class="label label-success">' . Yii::t('app', 'Active') . '</span>';
                    } else {
                        return '<span class="label label-default">' . Yii::t('app', 'Inactive') . '</span>';
                    }
                },
                'format'    => 'raw',
            ],
            [
                'attribute' => 'is_broker',
                'value'     => function ($model) {
                    if ($model->is_broker) {
                        return '<span class="label label-success">' . Yii::t('app', 'Active') . '</span>';
                    } else {
                        return '<span class="label label-default">' . Yii::t('app', 'Inactive') . '</span>';
                    }
                },
                'format'    => 'raw',
            ],
            [
                'attribute' => 'published_at',
                'format'    => 'date',
            ],
            [
                'attribute' => 'slide',
                'format'    => 'raw',
                'value'     => function ($model) {
                    $slides = $model->getSlide();
                    if (is_array($slides)) {
                        $result = [];
                        foreach ($slides as $slide) {
                            $weight = null;
                            if ($slide->weight) {
                                $weight = ' (weight = ' . $slide->weight . ')';
                            }
                            $url = Html::a('picture #' . (count($result) + 1) . $weight, Yii::$app->urlManager->createUrl(['/cabinet/features/features/edit-slide', 'feature_id' => $model->id, 'id' => $slide->id]));
                            $result[] = $url;
                        }
                    }
                    return implode('<br>', $result);
                },
            ],

            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{add_slide} {update} {delete}',
                'buttons'  => [
                    'update' => function ($url, $model) {
                        $options = [
                            'class'      => 'btn-update',
                            'title'      => Yii::t('yii', 'Update'),
                            'aria-label' => Yii::t('yii', 'Update'),
                            'data-pjax'  => '0',
                        ];
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
                    },
                    'add_slide' => function ($url, $model) {
                        $options = [
                            'class'      => 'btn-update',
                            'title'      => Yii::t('yii', 'Add Slide'),
                            'aria-label' => Yii::t('yii', 'Add Slide'),
                            'data-pjax'  => '0',
                        ];
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/features/features/edit-slide', 'feature_id' => $model->id]);
                        return Html::a('<span class="glyphicon glyphicon-picture"></span>', $url, $options);
                    },
                    'delete' => function ($url, $model) {
                        $options = [
                            'class'        => 'btn-delete',
                            'title'        => Yii::t('yii', 'Delete'),
                            'aria-label'   => Yii::t('yii', 'Delete'),
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                            'data-method'  => 'post',
                            'data-pjax'    => '0',
                        ];
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
                    }
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
