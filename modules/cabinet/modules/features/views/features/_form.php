<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\customImage\CustomImageUpload;
use app\components\ceo\CeoWidget;
use app\components\langInput\LangInputWidget;
use app\modules\brokers\models\ImprovementsCategory;
use app\modules\brokers\models\Improvement;

/* @var $this yii\web\View */
/* @var $model app\modules\brokers\models\Improvement */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="improvement-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= CeoWidget::widget(['model' => $model, 'form' => $form]) ?>

    <?= LangInputWidget::widget(['model' => $model, 'type' => 'text_input', 'form' => $form, 'attribute' => 'title']) ?>

    <?= LangInputWidget::widget(['model' => $model, 'type' => 'imperavi', 'form' => $form, 'attribute' => 'content']) ?>

    <?= $form->field($model, 'category_id')->dropDownList(ImprovementsCategory::getList(), ['prompt' => Yii::t('app', 'Select Category...')]) ?>

    <?= $form->field($model, 'alias')->textInput(['maxlength' => 128]) ?>

    <?= $form->field($model, 'status')->dropDownList(Improvement::getStatusList(), ['prompt'=> Yii::t('app', 'Select status...')]) ?>

    <?= $form->field($model, 'is_main')->checkbox(['checked' => '']) ?>

    <?= $form->field($model, 'is_broker')->checkbox(['checked' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
