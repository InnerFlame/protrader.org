<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\features\models\FeatureRequest */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Feature Request',
]) . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Feature Requests'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="feature-request-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
