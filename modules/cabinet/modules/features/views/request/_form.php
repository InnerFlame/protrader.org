<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\widgets\imperavi\Redactor;
use app\components\ceo\CeoWidget;
use app\components\langInput\LangInputWidget;
use app\modules\features\models\FeatureRequest;
use app\components\twig\CustomFilters;
use app\models\user\User;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model app\modules\features\models\FeatureRequest */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="feature-request-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= CeoWidget::widget(['model' => $model, 'form' => $form]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= Redactor::widget(['model' => $model, 'form' => $form, 'imageUpload' => Url::to('/cabinet/features/request/image-upload'), 'field' => 'content', 'type' => Redactor::TYPE_BACKEND]) ?>

    <?= $form->field($model, 'status')->dropDownList(FeatureRequest::getStatusList(), ['prompt' => Yii::t('app', 'Select status...')]) ?>

    <label class="col-lg-3 control-label" for="brokerimprovement-improvement_id">Improvement</label>
    <?= CustomFilters::selectFilter(User::getList(), $model->user_id, 'FeatureRequest[user_id]', false) ?>

    <?= $form->field($model, 'published')->dropDownList([1 => Yii::t('app', 'Enabled'), 0 => Yii::t('app', 'Disabled')], ['prompt' => Yii::t('app', 'Select status...')]) ?>

    <?= $form->field($model, 'category_id')->dropDownList(\app\modules\features\models\FeaturesRequestCategories::getListItemsAsArray(), ['prompt' => Yii::t('app', 'Select status...')]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
