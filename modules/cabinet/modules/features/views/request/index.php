<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\features\models\FeatureRequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Features Request');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="feature-request-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Feature Request'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'header'    => 'Creator',
                'attribute' => 'user_id',
                'format'    => 'raw',
                'value'     => function ($model) {
                    return '<a href ="' . $model->user->getViewUrl() . '">' . $model->user->getFullname() . '</a>';
                },
            ],
            [
                'attribute' => 'title',
                'format'    => 'raw', 'value' => function ($model) {
                    return '<a class="entity-link" href ="' . $model->getViewUrl() . '">' . Html::encode($model->title) . '</a>';
                },
            ],

            [
                'attribute' => 'status',
                'value'     => function ($model) {
                    return $model->getStatus();
                },
                'format'    => 'raw',
            ],
            [
                'attribute' => 'created_at',
                'format'    => 'date',
            ],
            [
                'header'    => 'Votes',
                'attribute' => 'votes',
                'format'    => 'raw',
                'value'     => function ($model) {
                    $options = [
                        'title'      => Yii::t('yii', 'Add Votes'),
                        'aria-label' => Yii::t('yii', 'Add Votes'),
                        'onClick'    => sprintf('show("%s", "%s")', $model->getTypeEntity(), $model->id),
                    ];
                    $output = '<span id="counter-' . $model->id . '">' . $model->getCountVotes() . '</span>';
                    $output .= '&nbsp';
                    $output .= '(<span title="Fake votes" id="counter-fake' . $model->id . '">' . $model->getCountFakeVotes() . '</span>)';
                    $output .= '&nbsp' . Html::a('<span class="glyphicon glyphicon-plus"></span>', 'javascript:void(0);', $options);
                    $options = [
                        'title'      => Yii::t('yii', 'Reset votes'),
                        'aria-label' => Yii::t('yii', 'Reset votes'),
                        'onClick'    => '
                            if(confirm(\'' . Yii::t('yii', 'Are you sure you want to reset all fake votes this Request?') . '\'))
                            {
                              return true;
                            } else {
                              return false;
                            }
                        ',
                    ];
                    $url = sprintf('/cabinet/features/request/fake-reset?entity=%s&entity_id=%s', $model->getTypeEntity(), $model->id);

                    $output .= '&nbsp&nbsp' . Html::a('<span class="glyphicon glyphicon-repeat"></span>', $url, $options);
                    if($model->return_votes == 0){
                        $options_return_votes = [
                            'title'      => Yii::t('yii', 'Return votes'),
                            'aria-label' => Yii::t('yii', 'Return votes'),
                            'onClick'    => '
                            if(confirm(\'' . Yii::t('yii', 'Are you sure you want to return to vote for this Request?') . '\'))
                            {
                              return true;
                            } else {
                              return false;
                            }
                        ',
                        ];
                        $url_return_votes = sprintf('/cabinet/features/request/return-votes?id=%s', $model->id);
                        $output .= '&nbsp&nbsp' . Html::a('<span class="glyphicon glyphicon-retweet"></span>', $url_return_votes, $options_return_votes);
                    }

//                    $options = [
//                        'title' => Yii::t('yii', 'Editor votes'),
//                        'aria-label' => Yii::t('yii', 'Editor votes'),
//                        'onClick' => 'console.log("editor votes")',
//                    ];
//                    $output .= '&nbsp&nbsp'.Html::a('<span class="glyphicon glyphicon-cog"></span>', 'javascript:void(0);', $options);
                    return $output;
                },
            ],

            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'buttons'  => [
                    'update' => function ($url, $model) {
                        $options = [
                            'class'      => 'btn-update',
                            'title'      => Yii::t('yii', 'Update'),
                            'aria-label' => Yii::t('yii', 'Update'),
                            'data-pjax'  => '0',
                        ];
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
                    },
                    'delete' => function ($url, $model) {
                        $options = [
                            'class'        => 'btn-delete',
                            'title'        => Yii::t('yii', 'Delete'),
                            'aria-label'   => Yii::t('yii', 'Delete'),
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                            'data-method'  => 'post',
                            'data-pjax'    => '0',
                        ];
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
                    }
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>

    <div id="modal_default" class="modal fade in">

    </div>
</div>
