<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2016
 */
?>
<h3><?=Yii::t('app', 'Total: {total}', ['total' => $count])?></h3>
<table style="width: 100%">
    <thead>
    <tr>
        <th style="width: 80%">
            <?=Yii::t('app', 'User')?>
        </th>
        <th style="width: 20%">
            <?=Yii::t('app', 'Count')?>
        </th>
    </tr>
    </thead>
    <?php foreach($votes as $vote): ?>
    <tr>
        <td><?= !empty($vote['user']->fullname) ? $vote['user']->fullname : $vote['user']->username;?></td>
        <td>
            <input type="text" name="count" id="count" value="<?=$vote['count']?>" class="form-control" disabled>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
<br/>
