<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2016
 */

use app\components\multi_select\CustomMultiSelect;
use app\models\user\User;
use yii\helpers\ArrayHelper;
?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h5 class="modal-title"><?=Yii::t('app', 'Fake votes generator')?></h5>
        </div>

        <div class="modal-body">
            <form class="stepy-basic" action="#">
                <fieldset title="1">
                    <legend class="text-semibold"><?=Yii::t('app', 'Generate table votes')?></legend>
                    <div class="form-group">
                        <input id="count" name="count" type="text" class="form-control" placeholder="<?=Yii::t('app', 'Total count votes')?>">
                        <span class="help-block"><?=Yii::t('app', 'Total count votes')?></span>
                    </div>
                    <input id="entity" name="entity" type="hidden" class="form-control" value="<?=$entity?>">
                    <input id="entity" name="entity_id" type="hidden" class="form-control" value="<?=$entity_id?>">
                    <div class="clear"></div>
                    <div class="form-group" id="filter-users">
                        <label class="display-block text-semibold"><?=Yii::t('app', 'Only users as: ')?></label>
                        <label class="checkbox-inline">
                            <input type="checkbox" name="moderators">
                            <?=Yii::t('app', 'Moderators')?>
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" name="already">
                            <?=Yii::t('app', 'Users already vote')?>
                        </label>
                    </div>
                    <div class="form-group">
                        <?=CustomMultiSelect::widget([
                            'data' => ArrayHelper::map(User::find()->all(), 'id', 'username'),
                            'id' => 'users_list',
                            'name' => 'users_list',
                            'options' => ['multiple' => true, 'class' => 'form-control', 'required' => true],
                            'clientOptions' => [
                                'enableCaseInsensitiveFiltering' => true,
                                'numberDisplayed'                => 1,
                                'allSelectedText'                => false,
                                'buttonWidth'                    => '100%',
                                'nonSelectedText'                => Yii::t('app', 'Select users')
                            ]
                        ]);?>
                        <span class="help-block"><?=Yii::t('app', 'Members on whose behalf the form of voice')?></span>
                    </div>
                </fieldset>
                <fieldset title="2">
                    <legend class="text-semibold"><?=Yii::t('app', 'Result')?></legend>
                    <div id="content-step-2" class="notSettings">

                    </div>
                </fieldset>
                <button type="submit" class="btn btn-success stepy-finish"><?=Yii::t('app', 'Finish')?></button>
            </form>
        </div>
    </div>
</div>
<?php
$this->registerJs(<<< EOM_JS
    $(".stepy-basic").stepy({
        nextLabel: 'Next <i class="icon-arrow-right14 position-right"></i>',
        backLabel: '<i class="icon-arrow-left13 position-left"></i> Back',
        validate: true,
        block: true,
        next: function(index) {
            if (!$(".stepy-basic").validate(validate)) {
                return false;
            }
            var content = $(".stepy-basic").find('#content-step-'+index);
            $(content).html('<i class="icon-spinner spinner"></i> Загрузка...');
            var data = $('.stepy-basic').serialize();
            $.ajax({
                'type': 'POST',
                'url': '/cabinet/features/request/count',
                'data':data,
                'success': function(response){
                    $(content).html(response);
                }
            });
        },
        finish: function() {
             var data = $('.stepy-basic').serialize();
            $.ajax({
                'type': 'POST',
                'url': '/cabinet/features/request/vote-save',
                'data':data,
                'dataType':'json',
                'success': function(response){
                    $('.modal-dialog').parent().modal('hide');
                    document.location.href = '/cabinet/features/request';
                }
            });
            return false;
        }
    });
    $('.stepy-step').find('.button-next').addClass('btn btn-primary');
    $('.stepy-step').find('.button-back').addClass('btn btn-default');
    $('.stepy-step').find('input[type="checkbox"]').uniform();
    $('#filter-users').on('change', function(){
        var inputs = $(this).find('input[type="checkbox"]:checked');
        var obj = {};
        var url = '/cabinet/features/request/filter?entity={$entity}&entity_id={$entity_id}';
        $(inputs).each(function(){
            obj[$(this).attr('name')] = 'true';
        });
        var params = $.param(obj);
        if(params && params.length > 0){
            url += '&'+params;
        }
        $.get(url, function(response){
            $('#users_list').multiselect('dataprovider', response);
        }, 'json');
    });
    var validate = {
        highlight: function(element, errorClass) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            element.closest('.form-group').removeClass('has-error').addClass('has-success');
            element.remove();
        },
        rules: {
            count: {
                required: true,
                number:true,
                min:1,
                max:999
            },
            users_list:{
                required: true
            }
        }
    }
EOM_JS
, \yii\web\View::POS_LOAD)
?>