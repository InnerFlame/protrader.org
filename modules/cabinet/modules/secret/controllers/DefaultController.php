<?php

namespace app\modules\cabinet\modules\secret\controllers;

use yii\web\Controller;

/**
 * Default controller for the `secret` module
 */
class DefaultController extends Controller
{
    public $layout = '@cab_views/layouts/main.twig';
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
