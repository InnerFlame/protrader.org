<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\trash\TrashSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Trash');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trash-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'class'          => 'yii\grid\SerialColumn',
                'header'         => '<span>#</span>',
                'contentOptions' => [
                    'width' => 50
                ],
            ],
            [
                'attribute' => 'user_id',
                'value'     => function ($model) {
                    if (is_object($model->user))
                        return $model->user->getFullName();
                },
                'format'    => 'raw',
            ],
            [
                'attribute' => 'entity',
                'value'     => function ($model) {
                    return $model->getEntityCode();
                },
                'format'    => 'raw',
            ],
            [
                'header'    => 'Title',
                'attribute' => 'entity_id',
                'value'     => function ($model) {
                    if (is_object($model->entityModel))
                        return $model->entityModel->getName();
                },
                'format'    => 'raw',
            ],
            [
                'attribute' => 'deleted_at',
                'format'    => 'date',
            ],


            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{restore} {remove}',
                'buttons'  => [
                    'restore' => function ($url, $model) {
                        $options = [
                            'class'        => 'btn-restore',
                            'title'        => Yii::t('yii', 'Restore'),
                            'aria-label'   => Yii::t('yii', 'Restore'),
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to restore this item?'),
                            'data-method'  => 'post',
                            'data-pjax'    => '0',
                        ];
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/trash/trash/restore', 'id' => $model->id]);
                        return Html::a('<span class="glyphicon glyphicon-repeat"></span>', $url, $options);
                    },
                    'remove' => function ($url, $model) {
                        $options = [
                            'class'        => 'btn-restore',
                            'title'        => Yii::t('yii', 'Remove'),
                            'aria-label'   => Yii::t('yii', 'Remove'),
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to remove this item?'),
                            'data-method'  => 'post',
                            'data-pjax'    => '0',
                        ];
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/trash/trash/remove', 'id' => $model->id]);
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
                    }
                ],
            ],

//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
