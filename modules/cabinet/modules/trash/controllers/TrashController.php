<?php

namespace app\modules\cabinet\modules\trash\controllers;

use app\components\Entity;
use app\models\trash\Trash;
use app\models\trash\TrashSearch;
use creocoder\nestedsets\NestedSetsBehavior;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * TrashController implements the CRUD actions for Trash model.
 */
class TrashController extends Controller
{
    public $layout = '@cab_views/layouts/main.twig';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
//                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Trash models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TrashSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Полное удаление Entity из корзины
     * @param $id
     * @return \yii\web\Response
     */
    public function actionRemove($id)
    {
        $model = Trash::find()->where(['id' => $id])->one();
        $entity = Entity::findEntity($model->entity, $model->entity_id);

        if(!$model || !$entity)
            Yii::$app->session->setFlash('warning', 'Failed to remove entity');
        try{
            if($this->removeEntity($entity) && $model->delete()){
                Yii::$app->session->setFlash('success', 'Item has been removed without a restore');
            }else{
                Yii::$app->session->setFlash('warning', 'Failed to remove entity');
            }
        }catch (\Exception $e){
            Yii::$app->session->setFlash('warning', $e->getMessage());
        }

        return $this->redirect(['index']);
    }

    public function actionRestore()
    {
        $trash = $this->findModel(Yii::$app->getRequest()->get('id'));
        $entity = $trash->entityModel;

        if (is_object($entity)) {
            $entity->scenario = Entity::SCENARIO_DELETE;
            $entity->deleted = 0;

            if ($entity->save(false) && $trash->delete()) {
                Yii::$app->session->setFlash('success', 'Restored');
            }
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Trash model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Trash the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Trash::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Если модель реализует поведение @see NestedSetsBehavior
     * и является рутовым элементом, то удалить вместе с потомками.
     * В противном случае, удаление невозможно @url https://github.com/creocoder/yii2-nested-sets/issues/71
     * Если это обычная модель, удаляем штатными средствами ActiveRecord
     * @param Entity $model
     * @return false|int|void
     */
    private function removeEntity(Entity $model)
    {
        if($model->getBehavior('tree') && $model->getBehavior('tree') instanceof NestedSetsBehavior && $model->isRoot()){
            return $model->deleteWithChildren();
        }
        return $model->delete(true);
    }
}
