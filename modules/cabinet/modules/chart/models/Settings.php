<?php

namespace app\modules\cabinet\modules\chart\models;

use Yii;

/**
 * This is the model class for table "chart_panels_settings".
 *
 * @property integer $id
 * @property integer $panel_id
 * @property string $value
 * @property string $default
 *
 * @property ChartPanels $panel
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'chart_panels_settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['panel_id', 'value', 'default'], 'required'],
            [['panel_id'], 'integer'],
            [['value', 'default'], 'string', 'max' => 255],
            [['panel_id'], 'exist', 'skipOnError' => true, 'targetClass' => ChartPanels::className(), 'targetAttribute' => ['panel_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'panel_id' => Yii::t('app', 'Panel ID'),
            'value' => Yii::t('app', 'Value'),
            'default' => Yii::t('app', 'Default'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPanel()
    {
        return $this->hasOne(ChartPanels::className(), ['id' => 'panel_id']);
    }
}
