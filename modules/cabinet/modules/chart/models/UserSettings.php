<?php

namespace app\modules\cabinet\modules\chart\models;

use app\models\user\User;
use Yii;

/**
 * This is the model class for table "chart_user_settings".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $panel_id
 * @property string $value
 *
 * @property User $user
 * @property ChartPanels $panel
 */
class UserSettings extends \yii\db\ActiveRecord
{
    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->user_id = Yii::$app->getUser()->getId();
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'chart_user_settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'panel_id', 'value'], 'required'],
            [['user_id', 'panel_id'], 'integer'],
            [['value'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['panel_id'], 'exist', 'skipOnError' => true, 'targetClass' => Panel::className(), 'targetAttribute' => ['panel_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'panel_id' => Yii::t('app', 'Panel ID'),
            'value' => Yii::t('app', 'Value'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPanel()
    {
        return $this->hasOne(Panel::className(), ['id' => 'panel_id']);
    }
}
