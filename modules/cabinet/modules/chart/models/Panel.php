<?php

namespace app\modules\cabinet\modules\chart\models;

use Yii;

/**
 * This is the model class for table "chart_panels".
 *
 * @property integer $id
 * @property string $name
 * @property string $label
 * @property integer $enabled
 *
 * @property ChartPanelsSettings[] $chartPanelsSettings
 * @property ChartUserSettings[] $chartUserSettings
 */
class Panel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'chart_panels';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'label'], 'required'],
            [['enabled'], 'integer'],
            [['name'], 'string', 'max' => 2],
            [['label'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'label' => Yii::t('app', 'Label'),
            'enabled' => Yii::t('app', 'Enabled'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChartPanelsSettings()
    {
        return $this->hasMany(ChartPanelsSettings::className(), ['panel_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChartUserSettings()
    {
        return $this->hasMany(ChartUserSettings::className(), ['panel_id' => 'id']);
    }
}
