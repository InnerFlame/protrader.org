<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use app\components\ceo\CeoWidget;
use app\components\twig\CustomFilters;
use app\modules\forum\models\FrmCategory;

/* @var $this yii\web\View */
/* @var $model app\modules\forum\models\FrmCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="frm-category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= CeoWidget::widget(['model' => $model, 'form' => $form])  ?>

    <label><?= Yii::t('app', 'Add Parent Category') ?></label>
    <?= CustomFilters::selectFilter(FrmCategory::getList(), $model->parent_id, 'FrmCategory[parent_id]', false, 'frmcategory-parent_id') ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 128]) ?>

    <?= $form->field($model, 'weight')->textInput(['maxlength' => 128]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => 128]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
