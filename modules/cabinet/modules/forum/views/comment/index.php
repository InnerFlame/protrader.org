<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\user\User;
use app\modules\forum\models\FrmComment;
use app\modules\forum\models\FrmTopic;
use app\components\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\forum\models\FrmCommentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Posts');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="frm-comment-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Frm Comment'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'user_id',
                'format'    => 'raw',
                'value'     => function ($model) {
                    $user = $model->user;
                    if ($user instanceof User) {
                        return Html::a($user->username, $user->getViewUrl());
                    }
                }
            ],
            [
                'attribute' => 'topic_id',
                'format'    => 'raw',
                'value'     => function (FrmComment $model) {
                    $topic = $model->topic;
                    if ($topic && $topic instanceof FrmTopic)
                        return Html::a($topic->title, $topic->getViewUrl());
                }
            ],

            [
                'attribute' => 'content',
                'format'    => 'raw',
                'value'     => function (FrmComment $model) {
//                    var_dump($model->getViewUrl());die();
                    return Html::a(StringHelper::truncate(strip_tags($model->content), 15), $model->getViewUrl());
                }
            ],

            [
                'attribute' => 'created_at',
                'format'    => 'date',
            ],
            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'buttons'  => [
                    'update' => function ($url, $model) {
                        $options = [
                            'class'      => 'btn-update',
                            'title'      => Yii::t('yii', 'Update'),
                            'aria-label' => Yii::t('yii', 'Update'),
                            'data-pjax'  => '0',
                        ];
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
                    },
                    'delete' => function ($url, $model) {
                        $options = [
                            'class'        => 'btn-delete',
                            'title'        => Yii::t('yii', 'Delete'),
                            'aria-label'   => Yii::t('yii', 'Delete'),
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                            'data-method'  => 'post',
                            'data-pjax'    => '0',
                        ];
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
                    }
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
