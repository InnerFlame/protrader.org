<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\twig\CustomFilters;
use app\models\user\User;
use app\modules\forum\models\FrmTopic;
use app\components\widgets\imperavi\Redactor;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\forum\models\FrmComment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="frm-comment-form">

    <?php $form = ActiveForm::begin(); ?>

    <label><?= Yii::t('app', 'Add  Author') ?></label>
    <?= CustomFilters::selectFilter(User::getUserList(), $model->user_id, 'FrmComment[user_id]', false) ?>

    <label><?= Yii::t('app', 'Choose topic') ?></label>
    <?= CustomFilters::selectFilter(FrmTopic::getList(), $model->topic_id, 'FrmComment[topic_id]', false) ?>

    <?= Redactor::widget(['model' => $model, 'form' => $form, 'imageUpload' => Url::to('/cabinet/forum/comment/image-upload'), 'field' => 'content', 'type' => Redactor::TYPE_BACKEND]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
