<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\forum\models\FrmTopic */

$this->title = Yii::t('app', 'Create Frm Topic');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Frm Topics'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="frm-topic-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
