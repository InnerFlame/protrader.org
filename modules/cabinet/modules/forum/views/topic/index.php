<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\forum\models\FrmTopicSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Topics');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="frm-topic-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Frm Topic'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'title',
                'format'    => 'raw',
                'value'     => function ($model) {
                    return '<a class="entity-link" href ="' . $model->getViewUrl() . '">' . $model->title . '</a>';
                },
            ],
            [
                'attribute' => 'category_id',
                'format'    => 'raw',
                'value'     => function ($model) {
                    return '<a class="category-link" href ="' . $model->category->getViewUrl() . '">' . $model->category->title . '</a>';
                },
            ],
            [
                'attribute' => 'user_id',
                'format'    => 'raw',
                'value'     => function ($model) {
                    if (is_object($model->user))
                        return $model->user->getLinkUsername($model->user->username);
                }
            ],
            [
                'attribute' => 'count_comments',
                'format'    => 'raw',
                'value'     => function ($model) {
                    return $model->getCountComments();
                },
            ],

            [
                'attribute' => 'created_at',
                'format'    => 'date',
            ],

            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'buttons'  => [
                    'update' => function ($url, $model) {
                        $options = [
                            'class'      => 'btn-update',
                            'title'      => Yii::t('yii', 'Update'),
                            'aria-label' => Yii::t('yii', 'Update'),
                            'data-pjax'  => '0',
                        ];
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
                    },
                    'delete' => function ($url, $model) {
                        $options = [
                            'class'        => 'btn-delete',
                            'title'        => Yii::t('yii', 'Delete'),
                            'aria-label'   => Yii::t('yii', 'Delete'),
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                            'data-method'  => 'post',
                            'data-pjax'    => '0',
                        ];
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
                    }
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
