<?php

use app\components\ceo\CeoWidget;
use app\components\twig\CustomFilters;
use app\components\widgets\imperavi\Redactor;
use app\models\user\User;
use app\modules\forum\models\FrmCategory;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\forum\models\FrmTopic */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="frm-topic-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= CeoWidget::widget(['model' => $model, 'form' => $form]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 128]) ?>

    <?= $form->field($model, 'alias')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'created_at')->textInput(['maxlength' => 60, 'value' => $model->isNewRecord ?  Yii::$app->formatter->asDate(time()) : Yii::$app->formatter->asDate($model->created_at)])?>

    <label><?= Yii::t('app', 'Add Author') ?></label>
    <?= CustomFilters::selectFilter(User::getList(), $model->user_id, 'FrmTopic[user_id]', false) ?>

    <label><?= Yii::t('app', 'Add Category') ?></label>
    <?= CustomFilters::selectFilter(FrmCategory::getList(1), $model->category_id, 'FrmTopic[category_id]', false, 'frmtopic-category_id') ?>

    <?= Redactor::widget(['model' => $model, 'form' => $form, 'imageUpload' => Url::to('/cabinet/forum/topic/image-upload'), 'field' => 'content', 'type' => Redactor::TYPE_BACKEND,]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
