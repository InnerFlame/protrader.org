<?php

namespace app\modules\cabinet\modules\forum\controllers;

use app\modules\forum\models\FrmCategory;
use app\modules\forum\models\FrmCategorySearch;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * CategoryController implements the CRUD actions for FrmCategory model.
 */
class CategoryController extends Controller
{
    public $layout = '@cab_views/layouts/main.twig';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FrmCategory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FrmCategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new FrmCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FrmCategory();

        if ($model->load(Yii::$app->request->post())) {
            $parent = ($model->parent_id) ? FrmCategory::findOne($model->parent_id) : FrmCategory::find()->where(['=', 'level', FrmCategory::LEVEL_ROOT])->one();
            if ($model->appendTo($parent)) {
                return $this->redirect(['index']);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing FrmCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $parent = $model->parent_id ? FrmCategory::findOne($model->parent_id) : FrmCategory::find()->where(['=', 'level', FrmCategory::LEVEL_ROOT])->one();
            if ($model->oldAttributes['parent_id'] != $model->parent_id) {
                $model->changeAlias();
            }
            if ($model->appendTo($parent)) {
                return $this->redirect(['index']);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing FrmCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if ($model->level === FrmCategory::LEVEL_ROOT) {
            throw new \yii\web\BadRequestHttpException();
        }

        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FrmCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FrmCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FrmCategory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
