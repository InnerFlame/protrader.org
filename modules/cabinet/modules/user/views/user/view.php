<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\user\User */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            'fullname',
            'password_hash',
            'password_reset_token',
            'auth_key',
            'email:email',
            'role',
            'status',
            'is_ptmc_team',
//            'motto',
            'pictures',
//            'vkontakte_id',
//            'google_id',
//            'linkedin_id',
//            'facebook_id',
//            'twitter_id',
            'birth_at',# TODO !
//            'gmt',
            'country',# TODO !
//            'Profile in social networks', TODO !
            'deleted',
            'created_at',
            'updated_at',
            'count_votes',
        ],
    ]) ?>

</div>
