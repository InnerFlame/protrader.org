<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \app\models\user\User;
use \yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
//            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'id',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'email',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'username',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'fullname',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'role',
                'filter'    => Html::activeDropDownList($searchModel, 'role', User::getRoleList(), ['prompt' => '---', 'class' => 'form-control']),
                'value'     => function ($model) {
                    return \app\models\user\User::getRole($model->role);
                },
                'format'    => 'raw',
            ],
            [
                'attribute' => 'status',
                'filter'    => Html::activeDropDownList($searchModel, 'status', User::getStatusList(), ['prompt' => '---', 'class' => 'form-control']),
                'value'     => function ($model) {
                    return \app\models\user\User::getStatus($model->status);
                },
                'format'    => 'raw',
            ],
            [
                'attribute' => 'created_at',
                'format'    => 'date',
                'filter'    => false
            ],

            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'buttons'  => [
                    'update' => function ($url, $model) {
                        $options = [
                            'class'      => 'btn-update',
                            'title'      => Yii::t('yii', 'Update'),
                            'aria-label' => Yii::t('yii', 'Update'),
                            'data-pjax'  => '0',
                        ];
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
                    },
                    'delete' => function ($url, $model) {
                        $options = [
                            'class'        => 'btn-delete',
                            'title'        => Yii::t('yii', 'Delete'),
                            'aria-label'   => Yii::t('yii', 'Delete'),
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                            'data-method'  => 'post',
                            'data-pjax'    => '0',
                        ];
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
                    }
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
