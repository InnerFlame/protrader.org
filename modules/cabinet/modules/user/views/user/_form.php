<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\customImage\CustomImageUpload;

/* @var $this yii\web\View */
/* @var $model app\models\user\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($user, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($profile, 'first_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($profile, 'last_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($profile_form, 'website')->textInput(['maxlength' => true]) ?>

    <?= $form->field($profile, 'skype')->textInput(['maxlength' => true]) ?>

    <?= $form->field($profile, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($profile_form, 'born_at')->textInput(['maxlength' => true]) ?>

    <?= $form->field($profile_form, 'social_profile')->textInput(['maxlength' => true]) ?>

    <?= $form->field($profile_form, 'country')->dropDownList(\app\models\forms\ProfileForm::$_countries, ['prompt' => Yii::t('app', 'Select country...')]) ?>

    <?= $form->field($profile_form, 'about_myself')->textArea(['cols' => 30, 'rows' => 5, 'maxlength' => true]) ?>

    <?= $form->field($user, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($password_form, 'new_password')->textInput(['maxlength' => true]) ?>

    <?= $form->field($password_form, 're_password')->textInput(['maxlength' => true]) ?>

    <?= $form->field($user, 'role')->dropDownList($user->getRoleList(), ['prompt' => Yii::t('app', 'Select role...')]) ?>

    <?= $form->field($user, 'status')->dropDownList($user->getStatusList(), ['prompt' => Yii::t('app', 'Select status...')]) ?>

    <?= $form->field($user, 'is_ptmc_team')->checkbox(['maxlength' => true]) ?>

    <?= $form->field($user, 'deleted')->checkbox(['maxlength' => true]) ?>
    <div class="form-group">
        <label>Registration: </label>&nbsp;<strong><?=$user->getMethodRegistration()?></strong>
    </div>
    <?= CustomImageUpload::widget([
        'form'      => $form,
        'model'     => $user,
        'attribute' => 'pictures',
        'crop'      => ['aspectRatio' => '1/1'],
        'options'   => ['id' => 'pictures'],
    ]); ?>


    <div class="form-group">
        <?= Html::submitButton($user->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $user->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
