<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\user\UserCustomValue */

$this->title = Yii::t('app', 'Create User Custom Value');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User Custom Values'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-custom-value-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
