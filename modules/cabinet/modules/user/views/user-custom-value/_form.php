<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\user\UserCustomValue */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-custom-value-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->dropDownList(app\models\user\User::getList(), ['prompt' => Yii::t('app', 'Select user...')]) ?>

    <?= $form->field($model, 'field_id')->dropDownList(app\models\user\UserCustomField::getList(), ['prompt' => Yii::t('app', 'Select field...')]) ?>

    <?= $form->field($model, 'value')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
