<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\user\crosstrade\CrosstradeSubscriber;
/* @var $this yii\web\View */
/* @var $searchModel app\models\user\crosstrade\CrosstradeSubscriberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'CROSS TRADE Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crosstrade-subscriber-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'demo_login',
            [
                'attribute' => 'username',
                'format'    => 'raw',
                'value'     => function ($model) {
                    return '<a href ="' . $model->user->getViewUrl() . '">' . $model->user->username . '</a>';
                },
            ],
            [
                'attribute' => 'fullname',
                'format'    => 'raw',
                'value'     => function ($model) {
                    return '<a href ="' . $model->user->getViewUrl() . '">' . $model->user->getFullName() . '</a>';
                },
            ],
            [
                'attribute' => 'email',
                'format'    => 'raw',
                'value'     => function ($model) {
                    return $model->user->email;
                },
            ],
            [
                'label' => 'Confirmed',
                'filter'    => Html::activeDropDownList($searchModel, 'confirmed', \app\models\user\crosstrade\CrosstradeSubscriber::getConfirmedList(), ['prompt' => 'All', 'class' => 'form-control']),
                'attribute' => 'confirmed',
            ],
            [
                'attribute' => 'status',
                'filter'    => Html::activeDropDownList($searchModel, 'status', \app\models\user\crosstrade\CrosstradeSubscriber::getRoleList(), ['prompt' => 'All', 'class' => 'form-control']),
                'value'     => function ($model) {
                    $mark = $model->is_registered ? '*' : '';
                    return sprintf("%s %s", CrosstradeSubscriber::getStatus($model->status), $mark);
                },
                'format'    => 'raw',
            ],
            'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
