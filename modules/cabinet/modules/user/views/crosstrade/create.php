<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\user\crosstrade\CrosstradeSubscriber */

$this->title = Yii::t('app', 'Create Crosstrade Subscriber');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Crosstrade Subscribers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crosstrade-subscriber-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
