<?php

namespace app\modules\cabinet\modules\user\controllers;

use app\models\forms\ChangePasswordForm;
use app\models\forms\ProfileForm;
use app\models\user\User;
use app\models\user\UserIdentity;
use app\models\user\UserProfile;
use app\models\user\UserSearch;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    public $layout = '@cab_views/layouts/main.twig';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     *  Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @throws HttpException
     */
    public function actionCreate()
    {
        throw new HttpException(404);
//        $model = new User();
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->id]);
//        } else {
//            return $this->render('create', [
//                'model' => $model,
//            ]);
//        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param $id
     * @return string|\yii\web\Response
     * @throws HttpException
     */
    public function actionUpdate($id)
    {
        if (!$id > 0)
            throw new HttpException(404);

        $user = UserIdentity::findIdentity($id);
        $profile = UserProfile::findOne((int)$id);
        $profile_form = new ProfileForm();
        $password_form = new ChangePasswordForm();

        $password_form->scenario = ChangePasswordForm::SCENARIO_EMPTY;

        if (!$user)
            throw new HttpException(404);

        if (!$profile) $profile = new UserProfile();

        if (!empty(Yii::$app->request->post('ChangePasswordForm')['new_password']) || !empty(Yii::$app->request->post('ChangePasswordForm')['re_password'])) {
            $password_form->scenario = ChangePasswordForm::SIGN_UP;
            $password_form->load(Yii::$app->request->post());
            $password_form->validate();
        }

        if ($this->LoadAndValidate($user, $password_form, $profile, $profile_form)) {
            if ($profile->save()) {
                $profile_form->saveCustomFields($user->id);
                Yii::$app->session->setFlash('success', 'Saved');
                return $this->redirect(['index']);
            }
        }

        $profile_form->getValues($id);

        return $this->render('update', [
            'user'          => $user,
            'profile'       => $profile,
            'profile_form'  => $profile_form,
            'password_form' => $password_form,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param $id
     * @return \yii\web\Response
     * @throws \Exception
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function LoadAndValidate(&$user, &$password_form, &$profile, &$profile_form)
    {
        if (Yii::$app->request->post()) {
            $user->scenario = UserIdentity::SCENARIO_SIGN_UP;

            if ($user->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post()) &&
                $profile_form->load(Yii::$app->request->post()) && $password_form->load(Yii::$app->request->post())
            ) {
                if ($user->validate() && $profile_form->validate() && $password_form->validate()) {
                    if ($user->isNewRecord) {
                        $user->status = User::STATUS_ACTIVE;
                        $user->role = User::ROLE_USER;
                    }
                    $user->fullname = Yii::$app->customFunctions->cleanText($profile->last_name . ' ' . $profile->first_name);

                    if (isset($password_form->new_password)) {
                        $user->password_hash = Yii::$app->security->generatePasswordHash($password_form->new_password);
                    }

                    if ($user->save()) {
                        $profile->user_id = $user->id;
                        return true;
                    }
                }
            }
        }

        return false;
    }
}
