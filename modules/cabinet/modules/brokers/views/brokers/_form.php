<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\widgets\imperavi\Redactor;
use app\components\ceo\CeoWidget;
use app\components\customImage\CustomImageUpload;
use app\modules\brokers\models\Broker;

/* @var $this yii\web\View */
/* @var $model app\modules\brokers\models\Broker */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="broker-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= CeoWidget::widget(['model' => $model, 'form' => $form]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= CustomImageUpload::widget(['form' => $form, 'model' => $model, 'attribute' => 'logo', 'options' => ['id' => 'pictures']]) ?>

    <?= $form->field($model, 'status')->dropDownList(Broker::getStatusList()) ?>

    <?= $form->field($model, 'site')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'account')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'regulation')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'traders')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_demo')->checkbox() ?>

    <?= Redactor::widget(['model' => $model, 'form' => $form, 'field' => 'description', 'type' => Redactor::TYPE_BACKEND]) ?>

    <?= $form->field($model, 'minimum_position_size')->textInput(['maxlength' => 128, 'value' => $model->getCustomValue($model, 'minimum_position_size')]) ?>

    <?= $form->field($model, 'maximum_leverage')->textInput(['maxlength' => 128, 'value' => $model->getCustomValue($model, 'maximum_leverage')]) ?>

    <?= $form->field($model, 'account_currencies')->textInput(['maxlength' => 128, 'value' => $model->getCustomValue($model, 'account_currencies')]) ?>

    <?= $form->field($model, 'payment_methods_available')->textInput(['maxlength' => 128, 'value' => $model->getCustomValue($model, 'payment_methods_available')]) ?>

    <?= $form->field($model, 'tradable_instruments')->textInput(['maxlength' => 128, 'value' => $model->getCustomValue($model, 'tradable_instruments')]) ?>

    <?= $form->field($model, 'spread_type')->textInput(['maxlength' => 128, 'value' => $model->getCustomValue($model, 'spread_type')]) ?>

    <?= $form->field($model, 'spread')->textInput(['maxlength' => 128, 'value' => $model->getCustomValue($model, 'spread')]) ?>

    <?= $form->field($model, 'execution_type')->textInput(['maxlength' => 128, 'value' => $model->getCustomValue($model, 'execution_type')]) ?>

    <?= $form->field($model, 'number_of_currency_pairs')->textInput(['maxlength' => 128, 'value' => $model->getCustomValue($model, 'number_of_currency_pairs')]) ?>

    <?= $form->field($model, 'demo_url')->textInput(['maxlength' => 128, 'value' => $model->getCustomValue($model, 'demo_url')]) ?>

    <?= $form->field($model, 'real_url')->textInput(['maxlength' => 128, 'value' => $model->getCustomValue($model, 'real_url')]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
