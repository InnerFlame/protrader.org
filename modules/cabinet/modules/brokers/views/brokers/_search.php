<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\brokers\models\BrokerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="broker-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'alias') ?>

    <?= $form->field($model, 'email') ?>

    <?= $form->field($model, 'site') ?>

    <?php // echo $form->field($model, 'add_link') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'logo') ?>

    <?php // echo $form->field($model, 'account') ?>

    <?php // echo $form->field($model, 'regulation') ?>

    <?php // echo $form->field($model, 'instruments') ?>

    <?php // echo $form->field($model, 'traders') ?>

    <?php // echo $form->field($model, 'votes') ?>

    <?php // echo $form->field($model, 'is_demo') ?>

    <?php // echo $form->field($model, 'deleted') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'type') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
