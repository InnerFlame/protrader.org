<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\widgets\imperavi\Redactor;
use app\components\ceo\CeoWidget;
use app\components\customImage\CustomImageUpload;
use app\modules\brokers\models\Broker;

/* @var $this yii\web\View */
/* @var $model app\modules\brokers\models\Broker */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="broker-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= CeoWidget::widget(['model' => $model, 'form' => $form]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= CustomImageUpload::widget(['form' => $form, 'model' => $model, 'attribute' => 'logo', 'options' => ['id' => 'pictures']]) ?>

    <?= $form->field($model, 'status')->dropDownList(Broker::getStatusList()) ?>

    <?= $form->field($model, 'site')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_demo')->checkbox() ?>

    <?= Redactor::widget(['model' => $model, 'form' => $form, 'field' => 'description', 'type' => Redactor::TYPE_BACKEND]) ?>

    <?= $form->field($model, 'available_instruments')->textInput(['maxlength' => 128, 'value' => $model->getCustomValue($model, 'available_instruments')]) ?>

    <?= $form->field($model, 'real_time_data')->textInput(['maxlength' => 128, 'value' => $model->getCustomValue($model, 'real_time_data')]) ?>

    <?= $form->field($model, 'historical_data')->textInput(['maxlength' => 128, 'value' => $model->getCustomValue($model, 'historical_data')]) ?>

    <?= $form->field($model, 'pricing')->textInput(['maxlength' => 128, 'value' => $model->getCustomValue($model, 'pricing')]) ?>

    <?= $form->field($model, 'demo_url')->textInput(['maxlength' => 128, 'value' => $model->getCustomValue($model, 'demo_url')]) ?>

    <?= $form->field($model, 'pricing_url')->textInput(['maxlength' => 128, 'value' => $model->getCustomValue($model, 'pricing_url')]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
