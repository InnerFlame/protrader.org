<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\brokers\models\BrokerImprovement */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Broker Improvement',
]) . $model->broker_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Broker Improvements'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->broker_id, 'url' => ['view', 'broker_id' => $model->broker_id, 'improvement_id' => $model->improvement_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="broker-improvement-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
