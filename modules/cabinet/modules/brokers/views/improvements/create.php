<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\brokers\models\BrokerImprovement */

$this->title = Yii::t('app', 'Create Broker Improvement');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Broker Improvements'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="broker-improvement-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
