<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\brokers\models\Broker;
use app\components\twig\CustomFilters;
use app\modules\brokers\models\Improvement;

/* @var $this yii\web\View */
/* @var $model app\modules\brokers\models\BrokerImprovement */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="broker-improvement-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if($model->isNewRecord) : ?>
        <?= $form->field($model, 'broker_id')->dropDownList(Broker::getEmptyBrokers()) ?>
    <?php else :?>
        <?= $form->field($model, 'broker_id')->dropDownList(Broker::getList()) ?>
    <?php endif; ?>

    <?= CustomFilters::selectFilter(Improvement::getList(), $model->getImprovementsByBroker($model->broker_id), 'BrokerImprovement[improvement_id]', true) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
