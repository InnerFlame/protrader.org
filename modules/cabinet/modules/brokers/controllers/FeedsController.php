<?php

namespace app\modules\cabinet\modules\brokers\controllers;

use Yii;
use app\modules\brokers\models\Broker;
use app\modules\cabinet\assets\AssetBroker;
use yii\web\BadRequestHttpException;
use yii\web\View;
use app\modules\brokers\models\BrokerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * FeedsController implements the CRUD actions for Broker model.
 */
class FeedsController extends Controller
{
    public $layout = '@cab_views/layouts/main.twig';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Broker models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BrokerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 'typeFeed');

        $this->getView()->registerJs(sprintf('var %s = %s;', 'BROKER_ACTION', Json::encode(['UP' => '/cabinet/brokers/brokers/up', 'DOWN' => '/cabinet/brokers/brokers/down'])), View::POS_HEAD);
        AssetBroker::register($this->getView());

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Broker model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Broker();

        if ($model->load(Yii::$app->request->post())) {
            $model->type = Broker::TYPE_FEED;
            if($model->save()){
                return $this->redirect(['index']);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Broker model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                $model->type = Broker::TYPE_FEED;
            }
            return $this->redirect(['index']);
        } else {
            $model->description = $model->getCustomValue($model, 'description');
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Broker model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Broker model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Broker the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Broker::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
