<?php

namespace app\modules\cabinet\modules\brokers\controllers;

use Yii;
use app\modules\brokers\models\BrokerImprovement;
use app\modules\brokers\models\BrokerImprovementSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ImprovementsController implements the CRUD actions for BrokerImprovement model.
 */
class ImprovementsController extends Controller
{
    public $layout = '@cab_views/layouts/main.twig';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BrokerImprovement models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BrokerImprovementSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new BrokerImprovement model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BrokerImprovement();

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->attachImprovements()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing BrokerImprovement model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $broker_id
     * @param integer $improvement_id
     * @return mixed
     */
    public function actionUpdate($broker_id, $improvement_id)
    {
        $model = $this->findModel($broker_id, $improvement_id);

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->attachImprovements($broker_id)) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing BrokerImprovement model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $broker_id
     * @param integer $improvement_id
     * @return mixed
     */
    public function actionDelete($broker_id, $improvement_id)
    {
        BrokerImprovement::deleteAll(['broker_id' => $broker_id]);

        return $this->redirect(['index']);
    }

    /**
     * Finds the BrokerImprovement model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $broker_id
     * @param integer $improvement_id
     * @return BrokerImprovement the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($broker_id, $improvement_id)
    {
        if (($model = BrokerImprovement::findOne(['broker_id' => $broker_id, 'improvement_id' => $improvement_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
