<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\notify\models\EventSubscribe */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Event Subscribe',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Event Subscribes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="event-subscribe-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
