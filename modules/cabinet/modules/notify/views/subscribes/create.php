<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\notify\models\EventSubscribe */

$this->title = Yii::t('app', 'Create Event Subscribe');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Event Subscribes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-subscribe-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
