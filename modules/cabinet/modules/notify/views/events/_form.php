<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\notify\models\Event */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="event-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'disabled' => true])?>

    <?= $form->field($model, 'enabled')->dropDownList([0 => Yii::t('app', 'Disabled'), 1 => Yii::t('app', 'All Users'), 2 => Yii::t('app', 'Only Admins')], ['prompt' => '---', 'class' => 'form-control']) ?>

    <?= $form->field($model, 'optional')->dropDownList([0 => Yii::t('app', 'Disabled'), 1 => Yii::t('app', 'Enabled')], ['prompt' => '---', 'class' => 'form-control']) ?>

    <?= $form->field($model, 'label')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
