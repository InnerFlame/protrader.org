<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\notify\models\EventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Events');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            [
                'label'     => 'Enabled\Disabled',
                'attribute' => 'enabled',
                'filter'    => Html::activeDropDownList($searchModel, 'enabled', [0 => Yii::t('app', 'All Users'), 1 => Yii::t('app', 'All Users'), 2 => Yii::t('app', 'Only Admins')], ['prompt' => '---', 'class' => 'form-control']),
                'content'   => function ($model) {
                    switch($model->enabled){
                        case \app\modules\notify\models\Event::ENABLED:
                            $html = sprintf("<span class='label label-success'>%s</span>", Yii::t('app', 'All Users'));
                            break;
                        case \app\modules\notify\models\Event::ENABLED_FOR_ADMINS:
                            $html = sprintf("<span class='label label-success'>%s</span>", Yii::t('app', 'Only Admins'));
                            break;
                        default:
                            $html = sprintf("<span class='label label-danger'>%s</span>", Yii::t('app', 'Disabled'));
                            break;
                    }
                    return $html;
                }
            ],
            [
                'label'     => 'Enabled\Disabled (Users)',
                'attribute' => 'enabled',
                'filter'    => Html::activeDropDownList($searchModel, 'optional', [0 => Yii::t('app', 'Disabled'), 1 => Yii::t('app', 'Enabled')], ['prompt' => '---', 'class' => 'form-control']),
                'content'   => function ($model) {
                    return $model->optional == 1 ? '<span class="label label-success">' . Yii::t('app', 'Enabled') . '</span>' : '<span class="label label-danger">' . Yii::t('app', 'Disabled') . '</span>';
                }
            ],
            'label',

            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'buttons'  => [
                    'update' => function ($url, $model) {
                        $options = [
                            'class'      => 'btn-update',
                            'title'      => Yii::t('yii', 'Update'),
                            'aria-label' => Yii::t('yii', 'Update'),
                            'data-pjax'  => '0',
                        ];
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
                    },
                    'delete' => function ($url, $model) {
                        $options = [
                            'class'        => 'btn-delete',
                            'title'        => Yii::t('yii', 'Delete'),
                            'aria-label'   => Yii::t('yii', 'Delete'),
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                            'data-method'  => 'post',
                            'data-pjax'    => '0',
                        ];
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
                    }
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
