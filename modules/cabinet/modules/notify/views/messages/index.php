<?php

use app\modules\notify\models\Event;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\notify\models\EventMessageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Messages');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-message-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'user_id',
                'label'     => Yii::t('app', 'User Destination'),
                'content'   => function ($model) {
                    return $model->user->getFullName();
                }
            ],
            [
                'attribute' => 'sender_id',
                'label'     => Yii::t('app', 'User Sender'),
                'content'   => function ($model) {
                    return $model->sender->getFullName();
                }
            ],
            [
                'label'     => Yii::t('app', 'Event'),
                'attribute' => 'event_id',
                'filter'    => Html::activeDropDownList($searchModel, 'event_id', ArrayHelper::map(Event::find()->all(), 'id', 'name'), ['prompt' => '---', 'class' => 'form-control']),
                'content'   => function ($model) {
                    return $model->event->name;
                },
            ],
            [
                'label'   => Yii::t('app', 'Sent?'),
                'filter'  => Html::activeDropDownList($searchModel, 'sent', [0 => Yii::t('app', 'No'), 1 => Yii::t('app', 'Yes')], ['prompt' => '---', 'class' => 'form-control']),
                'content' => function ($model) {
                    return $model->sent == 1 ? '<span class="label label-success">' . Yii::t('app', 'Yes') . '</span>' : '<span class="label label-danger">' . Yii::t('app', 'No') . '</span>';
                }
            ],
            [
                'label'   => Yii::t('app', 'Read?'),
                'filter'  => Html::activeDropDownList($searchModel, 'read', [0 => Yii::t('app', 'No'), 1 => Yii::t('app', 'Yes')], ['prompt' => '---', 'class' => 'form-control']),
                'content' => function ($model) {
                    return $model->read == 1 ? '<span class="label label-success">' . Yii::t('app', 'Yes') . '</span>' : '<span class="label label-danger">' . Yii::t('app', 'No') . '</span>';
                }
            ],
            [
                'label'     => Yii::t('app', 'Created at'),
                'attribute' => 'created_at',
                'format'    => 'datetime',
            ],
            [
                'label'     => Yii::t('app', 'Departure date'),
                'attribute' => 'departure_date',
                'format'    => 'datetime',
            ],
            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{view} {delete} {preview}',
                'buttons'  => [
                    'view'    => function ($url, $model) {
                        $options = [
                            'class'     => 'btn-view',
                            'title'     => Yii::t('yii', 'View'),
                            'data-pjax' => '0',
                        ];
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, $options);
                    },
                    'preview' => function ($url, $model) {
                        if (is_object($model->getNotify()->getEntity())) {
                            $msg = Yii::$app->controller->renderPartial('@app/views/mail/layouts/notify_preview.twig', [
                                'title' => $model->templates[3]->tpl_title,
                                'body'  => $model->templates[3]->getBody($model->getNotify())
                            ]);

                            return Html::a('<span class="label label-success"></span>', $url, [
                                'data-toggle' => 'modal',
                                'data-target' => '.modal_preview_message',
                                'class'       => 'fa fa-envelope-o btn-preview',
                                'onclick'     => 'getPreviewMessage("' . base64_encode($msg) . '")'
                            ]);
                        }
                    },
                    'delete'  => function ($url, $model) {
                        $options = [
                            'class'        => 'btn-delete',
                            'title'        => Yii::t('yii', 'Delete'),
                            'aria-label'   => Yii::t('yii', 'Delete'),
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                            'data-method'  => 'post',
                            'data-pjax'    => '0',
                        ];
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
                    }
                ],
            ],

        ],
    ]); ?>
    <?php Pjax::end(); ?></div>


<div class="modal ptmc-modal fade modal_preview_message">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">
                <i class="fa fa-close close-modal"></i>
            </button>
            <span class="modal-title h6">EMAIL TEMPLATE</span>
        </div>

        <div class="tabbable preview_message_result"></div>
    </div>
</div>


<?php $this->registerJs('

    function getPreviewMessage(msg){

        $(".preview_message_result").html(atob(msg));
    }

', \yii\web\View::POS_HEAD); ?>

