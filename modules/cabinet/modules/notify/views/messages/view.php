<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\components\notification\models\Notification;
use app\components\helpers\Serialize;

/* @var $this yii\web\View */
/* @var $model app\modules\notify\models\EventMessage */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Event Messages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-message-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php
        $event = Serialize::decode($model->data, true);
    ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => Yii::t('app', 'User Destination'),
                'value' => $model->user->getFullname()
            ],
            [
                'label' => Yii::t('app', 'User Sender'),
                'value' => $model->sender->getFullname()
            ],
            [
                'label' => Yii::t('app', 'Event Name'),
                'value' => $model->event->name
            ],
            [
                'label' => '{title}',
                'value' => $event instanceof Notification ? $event->getTitle() : null
            ],
            [
                'label' => '{author}',
                'value' => $event instanceof Notification ? $event->getAuthor() : null
            ],
            [
                'label' => '{username}',
                'value' => $event instanceof Notification ? $event->getUserName() : null
            ],
            [
                'label' => '{description}',
                'value' => $event instanceof Notification ? $event->getDescription() : null,
                'format' => 'raw'
            ],
            [
                'label' => 'Read?',
                'value' => $model->read == 1 ? '<span class="label label-success">'.Yii::t('app', 'Yes').'</span>' : '<span class="label label-danger">'.Yii::t('app', 'No').'</span>',
                'format' => 'raw'
            ],
            [
                'label' => 'Sent?',
                'value' => $model->sent == 1 ? '<span class="label label-success">'.Yii::t('app', 'Yes').'</span>' : '<span class="label label-danger">'.Yii::t('app', 'No').'</span>',
                'format' => 'raw'
            ],
            'created_at:datetime', // creation date formatted as datetime
            'departure_date:datetime', // creation date formatted as datetime
        ],
    ]) ?>

</div>
