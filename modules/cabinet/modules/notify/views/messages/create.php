<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\notify\models\EventMessage */

$this->title = Yii::t('app', 'Create Event Message');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Event Messages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-message-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
