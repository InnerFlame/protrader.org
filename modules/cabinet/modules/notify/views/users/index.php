<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use app\modules\notify\models\Event;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\notify\EventUserSettingsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users Settings');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-user-settings-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],
            'userName',
            [
                'label'     => Yii::t('app', 'Event'),
                'attribute' => 'event_id',
                'filter'    => Html::activeDropDownList($searchModel, 'event_id', ArrayHelper::map(Event::find()->all(), 'id', 'name'), ['prompt' => '---', 'class' => 'form-control']),
                'content'   => function ($model) {
                    return $model->event->name;
                },
            ],
            [
                'label'     => Yii::t('app', 'Send Alert'),
                'filter'    => Html::activeDropDownList($searchModel, 'push_alert', [0 => Yii::t('app', 'No'), 1 => Yii::t('app', 'Yes')], ['prompt' => '---', 'class' => 'form-control']),
                'attribute' => 'push_alert',
                'content'   => function ($model) {
                    return $model->push_alert == 1 ? '<span class="label label-success">' . Yii::t('app', 'Yes') . '</span>' : '<span class="label label-danger">' . Yii::t('app', 'No') . '</span>';
                }
            ],
            [
                'label'     => Yii::t('app', 'Send Email'),
                'filter'    => Html::activeDropDownList($searchModel, 'push_email', [0 => Yii::t('app', 'No'), 1 => Yii::t('app', 'Yes')], ['prompt' => '---', 'class' => 'form-control']),
                'attribute' => 'push_email',
                'content'   => function ($model) {
                    return $model->push_email == 1 ? '<span class="label label-success">' . Yii::t('app', 'Yes') . '</span>' : '<span class="label label-danger">' . Yii::t('app', 'No') . '</span>';
                }
            ],

            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'buttons'  => [
                    'update' => function ($url, $model) {
                        $options = [
                            'class'      => 'btn-update',
                            'title'      => Yii::t('yii', 'Update'),
                            'aria-label' => Yii::t('yii', 'Update'),
                            'data-pjax'  => '0',
                        ];
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
                    },
                    'delete' => function ($url, $model) {
                        $options = [
                            'class'        => 'btn-delete',
                            'title'        => Yii::t('yii', 'Delete'),
                            'aria-label'   => Yii::t('yii', 'Delete'),
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                            'data-method'  => 'post',
                            'data-pjax'    => '0',
                        ];
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
                    }
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
