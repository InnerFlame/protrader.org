<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\notify\models\Event;

/* @var $this yii\web\View */
/* @var $model app\modules\notify\models\EventUserSettings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="event-user-settings-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'event_id')->dropDownList(ArrayHelper::map(Event::find()->all(), 'id', 'name'), ['prompt' => '---']) ?>

    <?= $form->field($model, 'push_alert')->dropDownList([1 => Yii::t('app', 'Yes'), 0 => Yii::t('app', 'No')], ['prompt' => '---']) ?>

    <?= $form->field($model, 'push_email')->dropDownList([1 => Yii::t('app', 'Yes'), 0 => Yii::t('app', 'No')], ['prompt' => '---']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
