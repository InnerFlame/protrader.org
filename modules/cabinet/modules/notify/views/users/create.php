<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\notify\models\EventUserSettings */

$this->title = Yii::t('app', 'Create Event User Settings');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Event User Settings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-user-settings-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
