<?php

use app\modules\notify\models\Event;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\notify\models\EventQueueSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Queue events');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-queue-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Clear All'), ['clear'], ['class' => 'btn btn-danger', 'data-confirm' => Yii::t('app', 'Are you sure you want to delete all items?')]) ?>
    </p>
    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label'     => Yii::t('app', 'Event'),
                'attribute' => 'event_id',
                'filter'    => Html::activeDropDownList($searchModel, 'event_id', ArrayHelper::map(Event::find()->all(), 'id', 'name'), ['prompt' => '---', 'class' => 'form-control']),
                'content'   => function ($model) {
                    return $model->event->name;
                },
            ],
            [
                'label'     => Yii::t('app', 'Created at'),
                'attribute' => 'created_at',
                'format'    => 'datetime',
                'filter'    => false,
            ],

            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{view} {delete}',
                'buttons'  => [

                    'view'   => function ($url, $model) {
                        $options = [
                            'class'     => 'btn-view',
                            'title'     => Yii::t('yii', 'View'),
                            'data-pjax' => '0',
                        ];
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, $options);
                    },
                    'delete' => function ($url, $model) {
                        $options = [
                            'class'        => 'btn-delete',
                            'title'        => Yii::t('yii', 'Delete'),
                            'aria-label'   => Yii::t('yii', 'Delete'),
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                            'data-method'  => 'post',
                            'data-pjax'    => '0',
                        ];
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
                    }
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
