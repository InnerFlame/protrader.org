<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\components\helpers\Serialize;
use app\modules\notify\components\Notification;

/* @var $this yii\web\View */
/* @var $model app\modules\notify\models\EventQueue */

$this->title = sprintf('%s (%s)', $model->event->label, $model->event->name);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Event Queues'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-queue-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php
    $event = Serialize::decode($model->data, true);
    ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => Yii::t('app', 'Event Name'),
                'value' => $model->event->name
            ],
            [
                'label' => '{title}',
                'value' => $event instanceof Notification ? $event->getTitle() : null
            ],
            [
                'label' => '{author}',
                'value' => $event instanceof Notification ? $event->getAuthor() : null
            ],
            [
                'label' => '{username}',
                'value' => $event instanceof Notification ? $event->getUserName() : null
            ],
            [
                'label' => '{description}',
                'value' => $event instanceof Notification ? $event->getDescription() : null,
                'format' => 'raw'
            ],
            'created_at:datetime', // creation date formatted as datetime
        ],
    ]) ?>

</div>
