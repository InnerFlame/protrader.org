<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\notify\models\EventQueue */

$this->title = Yii::t('app', 'Create Event Queue');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Event Queues'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-queue-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
