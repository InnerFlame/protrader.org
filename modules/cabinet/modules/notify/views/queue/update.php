<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\notify\models\EventQueue */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Event Queue',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Event Queues'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="event-queue-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
