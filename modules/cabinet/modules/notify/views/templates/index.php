<?php

use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use app\modules\notify\models\Event;
use app\modules\cabinet\modules\notify\models\NotifyTemplate;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\cabinet\modules\notify\models\SearchNotifyTemplate */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Templates');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notify-template-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <!--    --><?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Notify Template'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label'     => 'Event',
                'attribute' => 'event_id',
                'filter'    => Html::activeDropDownList($searchModel, 'event_id', ArrayHelper::map(Event::find()->all(), 'id', 'name'), ['prompt' => '---', 'class' => 'form-control']),
                'content'   => function ($model, $key, $index, $column) {
                    return sprintf("(%s) - %s", $model->event->name, $model->event->label);
                },
            ],
            [
                'label'     => 'Type',
                'attribute' => 'type',
                'filter'    => Html::activeDropDownList($searchModel, 'type', NotifyTemplate::getListTypes(), ['prompt' => '---', 'class' => 'form-control']),
                'content'   => function ($model) {
                    return $model->attributeTypeLabel($model->type);
                }
            ],
            'tpl_title',
            'tpl_body',

            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'buttons'  => [
                    'update' => function ($url, $model) {
                        $options = [
                            'class'      => 'btn-update',
                            'title'      => Yii::t('yii', 'Update'),
                            'aria-label' => Yii::t('yii', 'Update'),
                            'data-pjax'  => '0',
                        ];
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
                    },
                    'delete' => function ($url, $model) {
                        $options = [
                            'class'        => 'btn-delete',
                            'title'        => Yii::t('yii', 'Delete'),
                            'aria-label'   => Yii::t('yii', 'Delete'),
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                            'data-method'  => 'post',
                            'data-pjax'    => '0',
                        ];
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
                    }
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
