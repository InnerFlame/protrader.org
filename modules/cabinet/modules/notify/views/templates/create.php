<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\cabinet\modules\notify\models\NotifyTemplate */

$this->title = Yii::t('app', 'Create Notify Template');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Notify Templates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notify-template-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
