<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\notify\models\Event;
use app\modules\cabinet\modules\notify\models\NotifyTemplate;

/* @var $this yii\web\View */
/* @var $model app\modules\cabinet\modules\notify\models\NotifyTemplate */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="notify-template-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'event_id')->dropDownList(ArrayHelper::map(Event::find()->all(), 'id', 'name'), ['prompt' => '---']) ?>

    <?= $form->field($model, 'type')->dropDownList(NotifyTemplate::getListTypes(), ['prompt' => '---']) ?>

    <?= $form->field($model, 'tpl_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tpl_body')->textarea(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
