<?php

namespace app\modules\cabinet\modules\notify\controllers;

use app\components\CustomController;

/**
 * Default controller for the `notify` module
 */
class DefaultController extends CustomController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index.twig');
    }
}
