<?php

namespace app\modules\cabinet\modules\notify\controllers;

use Yii;
use app\modules\notify\models\EventQueue;
use app\modules\notify\models\EventQueueSearch;
use app\components\CustomController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * QueueController implements the CRUD actions for EventQueue model.
 */
class QueueController extends CustomController
{
    public $layout = '@cab_views/layouts/main.twig';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all EventQueue models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EventQueueSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single EventQueue model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Deletes an existing EventQueue model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionClear()
    {
        $result = EventQueue::deleteAll();
        if($result){
            Yii::$app->session->setFlash('success', Yii::t('app', 'Queue successfully cleared'));
            return $this->redirect(Yii::$app->request->referrer);
        }
        Yii::$app->session->setFlash('success', Yii::t('app', 'Could not clear the queue'));
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the EventQueue model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EventQueue the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EventQueue::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
