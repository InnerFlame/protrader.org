<?php

namespace app\modules\cabinet\modules\notify\models;
use app\components\Entity;

/**
 * This is the ActiveQuery class for [[NotifyTemplate]].
 *
 * @see NotifyTemplate
 */
class NotifyTemplateQuery extends \yii\db\ActiveQuery
{
    /**
     * @inheritdoc
     * @return NotifyTemplate[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return NotifyTemplate|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @return $this
     */
    public function notDeleted()
    {
        return $this->andWhere(['deleted' => Entity::NOT_DELETED]);
    }
}
