<?php

namespace app\modules\cabinet\modules\notify\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\cabinet\modules\notify\models\NotifyTemplate;

/**
 * SearchNotifyTemplate represents the model behind the search form about `app\modules\cabinet\modules\notify\models\NotifyTemplate`.
 */
class NotifyTemplateSearch extends NotifyTemplate
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'event_id'], 'integer'],
            [['type', 'tpl_title', 'tpl_body'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NotifyTemplate::find()->notDeleted();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'event_id' => $this->event_id,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'tpl_title', $this->tpl_title])
            ->andFilterWhere(['like', 'tpl_body', $this->tpl_body]);

        return $dataProvider;
    }
}
