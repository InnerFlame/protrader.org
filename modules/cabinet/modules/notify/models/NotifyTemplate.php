<?php

namespace app\modules\cabinet\modules\notify\models;

use app\components\Entity;
use app\modules\notify\components\Notification;
use app\modules\notify\models\Event;
use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "notify_templates".
 *
 * @property integer $id
 * @property integer $event_id
 * @property string $type
 * @property string $tpl_title
 * @property string $tpl_body
 *
 * @property NotifyEvents $event
 */
class NotifyTemplate extends Entity
{
    const TYPE_SHORT = 'SHORT';
    const TYPE_FULL = 'FULL';
    const TYPE_EMAIL = 'EMAIL';
    const TYPE_GROUP = 'GROUP';

    private static $_templatesMap = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notify_templates';
    }

    /**
     * Get translate type
     * @param $type
     * @return string
     */
    public static function attributeTypeLabel($type)
    {
        switch ($type) {
            case self::TYPE_EMAIL:
                $label = Yii::t('app', 'Email');
                break;
            case self::TYPE_FULL:
                $label = Yii::t('app', 'Time Line');
                break;
            case self::TYPE_SHORT:
                $label = Yii::t('app', 'Message');
                break;
            case self::TYPE_GROUP:
                $label = Yii::t('app', 'Group');
                break;
            default:
                $label = Yii::t('app', 'Unknown type');
                break;
        }
        return $label;
    }

    /**
     * Return list types
     * @return array
     */
    public static function getListTypes()
    {
        return [
            self::TYPE_EMAIL => self::attributeTypeLabel(self::TYPE_EMAIL),
            self::TYPE_SHORT => self::attributeTypeLabel(self::TYPE_SHORT),
            self::TYPE_FULL  => self::attributeTypeLabel(self::TYPE_FULL),
            self::TYPE_GROUP => self::attributeTypeLabel(self::TYPE_GROUP),
        ];
    }

    /**
     * Генериует шаблон определенного типа, для определенного
     * типа нотфикации и чсохраняет в буфер, что бы избежать
     * повторого получения данного шаблона, например из базы данных
     * @param $type
     * @param $event_id
     * @return bool
     */
    public static function getTemplate($type, $event_id)
    {
        $key = sprintf("%s-%d", $type, $event_id);
        if(!isset(self::$_templatesMap[$key])){
            $template = self::find()->where(['type' => NotifyTemplate::TYPE_SHORT])->andWhere(['event_id' => $event_id])->one();
            if(!$template){
                return false;
            }
            self::$_templatesMap[$key] = $template;
        }
        return self::$_templatesMap[$key];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_id', 'type', 'tpl_title', 'tpl_body'], 'required'],
            [['event_id'], 'integer'],
            [['type'], 'string'],
            [['tpl_title', 'tpl_body'], 'string', 'max' => 255],
            [['event_id', 'type'], 'unique', 'targetAttribute' => ['event_id', 'type'], 'message' => 'The combination of Event ID and Type has already been taken.'],
            [['event_id'], 'exist', 'skipOnError' => true, 'targetClass' => Event::className(), 'targetAttribute' => ['event_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'        => Yii::t('app', 'Id'),
            'event_id'  => Yii::t('app', 'Event ID'),
            'type'      => Yii::t('app', 'Type'),
            'tpl_title' => Yii::t('app', 'Tpl Title'),
            'tpl_body'  => Yii::t('app', 'Tpl Body'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::className(), ['id' => 'event_id']);
    }

    /**
     * @param Notification $notification
     * @param int $count
     * @return mixed
     */
    public function getTitle(Notification $notification, $count = 0)
    {
        $output =  str_replace(['{title}', '{author}', '{username}', '{description}', '{date}', '{count}', '{link}'],
            [$notification->getTitle(), $notification->getAuthor(), $notification->getUserName(), $notification->getDescription(), $notification->getDate(), $count, $notification->getLink()], $this->tpl_title);
        return Html::encode($output);
    }

    /**
     * @param Notification $notification
     * @param int $count
     * @return mixed
     */
    public function getBody(Notification $notification, $count = 0)
    {
        $output = str_replace(['{title}', '{author}', '{username}', '{description}', '{date}', '{count}', '{link}'],
            [$notification->getTitle(), $notification->getAuthor(), $notification->getUserName(), $notification->getDescription(), $notification->getDate(), $count, $notification->getLink()], $this->tpl_body);

        return Html::encode($output);
    }

    /**
     * @return int
     */
    public function getTypeEntity()
    {
        return Entity::NOTIFY_TEMPLATE;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'NOTIFY_TEMPLATE';
    }

    public function getViewUrl()
    {
        // TODO: Implement getViewUrl() method.
    }


}
