<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\ceo\CeoWidget;
use app\components\langInput\LangInputWidget;

/* @var $this yii\web\View */
/* @var $model app\models\main\Pages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pages-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= CeoWidget::widget(['model' => $model, 'form' => $form]) ?>

    <?= LangInputWidget::widget(['model' => $model, 'type' => 'text_input', 'form' => $form, 'attribute' => 'title']) ?>

    <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

    <?= LangInputWidget::widget(['model' => $model, 'type' => 'text_area', 'form' => $form, 'attribute' => 'content']) ?>

    <?= $form->field($model, 'css')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'js')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
