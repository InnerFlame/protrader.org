<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
use app\modules\articles\models\Articles;
use app\modules\articles\models\ArtclCategory;
use \yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\articles\models\ArticlesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Articles');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articles-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Articles'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [

            [
                'class'          => 'yii\grid\SerialColumn',
                'header'         => '<span>#</span>',
                'contentOptions' => [
                    'width' => 50
                ],
            ],

            [
                'attribute' => 'created_at',
                'format'    => 'date',
            ],

            [
                'attribute' => 'title',
                'format'    => 'raw',

                'value' => function ($model) {
                    return '<a class="entity-link" href ="' . $model->getViewUrl() . '">' . $model->title . '</a>';
                },
            ],

            [
                'attribute' => 'category_id',
                'format'    => 'raw',
                'filter' => Html::activeDropDownList($searchModel, 'category_id', ArrayHelper::map(ArtclCategory::find()->notDeleted()->all(), 'id', 'title'), ['class' =>'form-control', 'prompt' => Yii::t('app', 'Select Category')]),
                'value'     => function ($model) {
                    return '<a class="category-link" href ="' . $model->category->getViewUrl() . '">' . $model->category->title . '</a>';
                },
            ],

            [
                'attribute' => 'user_id',
                'format'    => 'raw',
                'value'     => function ($model) {
                    if (is_object($model->user)) {
                        return '<a class="profile-link" href ="' . $model->user->getProfileLink() . '">' . $model->user->getFullname() . '</a>';
                    }
                },
            ],

            [
                'attribute' => 'stasus',
                'format'    => 'raw',
                'filter' => Html::activeDropDownList($searchModel, 'status', Articles::getStatusList(),['class'=>'form-control', 'prompt' => 'All']),
                'value'     => function ($model) {
                    if($model->status == Articles::STATUS_ENABLE) {
                        return 'Enable';
                    }
                    return 'Disable';
                },
            ],

            [
                'attribute' => 'replies',
                'format'    => 'raw',
                'value'     => function ($model) {
                    if (is_array($model->comments)) {
                        return count($model->comments);
                    }
                },
            ],

            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'buttons'  => [
                    'update' => function ($url, $model) {
                        $options = [
                            'class'      => 'btn-update',
                            'title'      => Yii::t('yii', 'Update'),
                            'aria-label' => Yii::t('yii', 'Update'),
                            'data-pjax'  => '0',
                        ];
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
                    },
                    'delete' => function ($url, $model) {
                        $options = [
                            'class'        => 'btn-delete',
                            'title'        => Yii::t('yii', 'Delete'),
                            'aria-label'   => Yii::t('yii', 'Delete'),
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                            'data-method'  => 'post',
                            'data-pjax'    => '0',
                        ];
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
                    }
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
