<?php

use app\components\ceo\CeoWidget;
use app\components\customImage\CustomImageUpload;
use app\components\langInput\LangInputWidget;
use app\components\twig\CustomFilters;
use app\modules\articles\models\ArtclCategory;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\articles\models\Articles */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="articles-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= CeoWidget::widget(['model' => $model, 'form' => $form]) ?>

    <?= $form->field($model, 'category_id')->dropDownList(ArtclCategory::getCategoryList()) ?>

    <?= LangInputWidget::widget(['model' => $model, 'type' => 'text_input', 'form' => $form, 'attribute' => 'title']) ?>

    <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

    <?= CustomImageUpload::widget(['form' => $form, 'model' => $model, 'attribute' => 'pictures', 'crop' => ['aspectRatio' => 1], 'options' => ['id' => 'pictures']]) ?>

    <?= LangInputWidget::widget(['model' => $model, 'type' => 'imperavi', 'form' => $form, 'attribute' => 'content']) ?>

    <?= $form->field($model, 'status')->dropDownList($model->getStatusList(), ['prompt' => Yii::t('app', 'Select status...')]) ?>

    <?= $form->field($model, 'published')->textInput(['maxlength' => 60, 'value' => $model->isNewRecord ?  Yii::$app->formatter->asDate(time()) : Yii::$app->formatter->asDate($model->published)])?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
