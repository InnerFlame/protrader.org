<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\twig\CustomFilters;
use app\modules\articles\models\Articles;

/* @var $this yii\web\View */
/* @var $model app\modules\articles\models\ArtclTag */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="artcl-tag-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= CustomFilters::selectFilter(Articles::getList(), $model->getArtclTags($model->article_id), 'ArtclTag[article_id]', true) ?>


    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
