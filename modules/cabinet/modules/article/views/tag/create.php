<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\articles\models\ArtclTag */

$this->title = Yii::t('app', 'Create Artcl Tag');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Artcl Tags'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="artcl-tag-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
