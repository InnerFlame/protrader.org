<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\articles\models\ArtclCategory */

$this->title = Yii::t('app', 'Create Artcl Category');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Artcl Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="artcl-category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
