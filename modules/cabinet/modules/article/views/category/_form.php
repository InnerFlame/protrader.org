<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\ceo\CeoWidget;
/* @var $this yii\web\View */
/* @var $model app\modules\articles\models\ArtclCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="artcl-category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= CeoWidget::widget(['model' => $model, 'form' => $form]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
