<?php

namespace app\modules\cabinet\modules\article\controllers;

use app\modules\articles\models\ArtclTag;
use app\modules\articles\models\ArtclTagRelations;
use app\modules\articles\models\ArtclTagSearch;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * TagController implements the CRUD actions for ArtclTag model.
 */
class TagController extends Controller
{
    public $layout = '@cab_views/layouts/main.twig';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ArtclTag models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ArtclTagSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new ArtclTag model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionCreate()
    {
        $tag = new ArtclTag();

        if ($tag->load(Yii::$app->request->post())) {
            $tag->count = isset(Yii::$app->request->post('ArtclTag')['article_id']) ? count(Yii::$app->request->post('ArtclTag')['article_id']) : 0;
            if ($tag->save()) {
                if ($tag->count > 0) {
                    foreach (Yii::$app->request->post('ArtclTag')['article_id'] as $article_id) {
                        $model = new ArtclTagRelations();
                        $model->tag_id = $tag->id;
                        $model->article_id = $article_id;
                        if (!$model->save())
                            throw new \yii\web\BadRequestHttpException();
                    }
                }
                return $this->redirect(['index']);
            }
        } else {
            return $this->render('create', [
                'model' => $tag,
            ]);
        }
    }

    /**
     * Updates an existing ArtclTag model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionUpdate($id)
    {
        $tag = $this->findModel($id);

        if ($tag->load(Yii::$app->request->post())) {
            $tag->count = isset(Yii::$app->request->post('ArtclTag')['article_id']) ? count(Yii::$app->request->post('ArtclTag')['article_id']) : 0;
            if ($tag->save()) {
                ArtclTagRelations::deleteAll(['tag_id' => $tag->id]);

                if(isset(Yii::$app->request->post('ArtclTag')['article_id'])) {
                    foreach (Yii::$app->request->post('ArtclTag')['article_id'] as $article_id) {
                        $model = ArtclTagRelations::findOne($article_id);
                        if (!is_object($model)) {
                            $model = new ArtclTagRelations();
                        }
                        $model->tag_id = $tag->id;
                        $model->article_id = $article_id;
                        if (!$model->save()) {
                            throw new \yii\web\ForbiddenHttpException();
                        }
                    }
                }
                return $this->redirect(['index']);
            }
        } else {
            return $this->render('update', [
                'model' => $tag,
            ]);
        }
    }

    /**
     * Deletes an existing ArtclTag model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ArtclTag model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ArtclTag the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ArtclTag::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
