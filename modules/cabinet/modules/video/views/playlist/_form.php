<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\twig\CustomFilters;
use app\models\video\Video;

/* @var $this yii\web\View */
/* @var $model app\models\video\Playlist */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="playlist-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

    <label><?= Yii::t('app', 'Add to Video to Play List') ?></label>
    <?= CustomFilters::selectFilter(Video::getList(), $model->playlist, 'Playlist[playlist]', true) ?>

    <?= $form->field($model, 'weight')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
