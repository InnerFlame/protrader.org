<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\video\VdCategory;
use app\models\video\Playlist;
use app\components\customImage\CustomImageUpload;
use app\components\twig\CustomFilters;

/* @var $this yii\web\View */
/* @var $model app\models\video\Video */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="video-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'category_id')->dropDownList(VdCategory::getList()) ?>

    <?= CustomImageUpload::widget(['form' => $form, 'model' => $model, 'attribute' => 'pictures', 'crop' => ['aspectRatio'=> 1], 'options' => ['id' => 'pictures']]) ?>

    <label><?= Yii::t('app', 'Add to Play List') ?></label>
    <?= CustomFilters::selectFilter(Playlist::getList(), $model->playlist, 'Video[playlist]', true) ?>

    <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'weight')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
