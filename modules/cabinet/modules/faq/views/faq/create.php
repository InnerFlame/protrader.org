<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\main\Faq */

$this->title = Yii::t('app', 'Create FAQ');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Faqs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="faq-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
