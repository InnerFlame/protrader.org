<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\widgets\imperavi\Redactor;
use app\models\main\FaqCategory;

/* @var $this yii\web\View */
/* @var $model app\models\main\Faq */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="faq-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'category_id')->dropDownList(FaqCategory::getList(), ['prompt'=> Yii::t('app', 'Select catgory...')]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 255, 'class' => 'form-control']) ?>

    <?= Redactor::widget(['model' => $model, 'form' => $form, 'field' => 'answer', 'type' => Redactor::TYPE_BACKEND]) ?>

    <?= $form->field($model, 'weight')->textInput(['maxlength' => 255]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
