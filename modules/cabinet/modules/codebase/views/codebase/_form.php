<?php

use app\components\ceo\CeoWidget;
use app\components\langInput;
use app\components\widgets\imperavi\Redactor;
use app\modules\codebase\models\Category;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\twig\CustomFilters;

/* @var $this yii\web\View */
/* @var $model app\modules\codebase\models\Description */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="description-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= CeoWidget::widget(['model' => $model, 'form' => $form]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

    <?= Redactor::widget(['model' => $model, 'form' => $form, 'field' => 'content', 'type' => Redactor::TYPE_BACKEND, 'imageUpload' => '/cabinet/image-upload']) ?>

    <?= $form->field($model, 'file[]')->fileInput(['multiple' => true]) ?>

    <?php if (!$model->isNewRecord) : ?>
        <table class="table table-bordered">
            <tr class="success">
                <th><?= Yii::t('app', 'File') ?></th>
                <th><?= Yii::t('app', 'Action') ?></th>
            </tr>

            <?php if (is_array($model->files)) : ?>
                <?php foreach ($model->files as $file) : ?>
                    <?php if (!$file->deleted && is_object($file)) : ?>
                        <tr>
                            <td><?= $file->name ?></td>
                            <td>
                                <a onclick="if(!confirm('Realy delete?'))return false;" href="/cabinet/codebase/codebase/file-delete?id=<?= $file->id ?>">
                                    <?= Yii::t('app', 'Delete') ?>
                                </a>
                            </td>
                        </tr>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
        </table>
    <?php endif; ?>

    <?= $form->field($model, 'category_id')->dropDownList(Category::getCategoryList(), ['prompt' => Yii::t('app', 'Select category...')]) ?>

    <?= $form->field($model, 'created_at')->textInput(['maxlength' => 60, 'value' => $model->isNewRecord ?  Yii::$app->formatter->asDate(time()) : Yii::$app->formatter->asDate($model->created_at)])?>

    <?= $form->field($model, 'status')->dropDownList($model->getStatusList(), ['prompt' => Yii::t('app', 'Select status...')]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
