<?php

use app\components\ceo\CeoWidget;
use app\components\customImage\CustomImageUpload;
use app\components\langInput\LangInputWidget;
use app\modules\news\models\NewsCategory;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\news\models\News */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= CeoWidget::widget(['model' => $model, 'form' => $form]) ?>

    <?= LangInputWidget::widget(['model' => $model, 'type' => 'text_input', 'form' => $form, 'attribute' => 'title']) ?>

    <?= $form->field($model, 'alias')->textInput(['maxlength' => 128]) ?>

    <input name="News[user_id]" type="hidden" id="News-user_id" value="<?= Yii::$app->user->identity->id ?>">

    <?= $form->field($model, 'category_id')->dropDownList(NewsCategory::getCategoryList()) ?>

    <?= LangInputWidget::widget(['model' => $model, 'type' => 'imperavi', 'form' => $form, 'attribute' => 'content']) ?>

    <?= CustomImageUpload::widget(['form' => $form, 'model' => $model, 'attribute' => 'pictures', 'crop' => ['aspectRatio' => 1], 'options' => ['id' => 'pictures']]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
