<?php

use app\modules\cabinet\modules\notify\models\NotifyTemplate;
use \app\modules\articles\ArticlesModule;

return [


    ArticlesModule::EVENT_ARTICLE_ADD => [
        'label'     => 'New articles',
        'templates' => [
            NotifyTemplate::TYPE_EMAIL => [
                'title' => 'added new article',
                'body' => '{title}'
            ],
            NotifyTemplate::TYPE_FULL  => [
                'title' => '{title}',
                'body' => 'article was added by {username}'
            ],
            NotifyTemplate::TYPE_SHORT => [
                'title' => 'added new article',
                'body' => '{title}'
            ],
            NotifyTemplate::TYPE_GROUP => [
                'title' => '{count} new article(s) was(ware) add',
                'body' => '{title}'
            ]
        ]
    ],

//    ArticlesModule::EVENT_ARTICLE_EDIT => [
//        'label'     => 'Edited articles',
//        'templates' => [
//            NotifyTemplate::TYPE_EMAIL => [
//                'title' => 'edited article',
//                'body' => '{title}'
//            ],
//            NotifyTemplate::TYPE_FULL  => [
//                'title' => 'edited article',
//                'body' => '{title}'
//            ],
//            NotifyTemplate::TYPE_SHORT => [
//                'title' => 'edited article',
//                'body' => '{title}'
//            ],
//            NotifyTemplate::TYPE_GROUP => [
//                'title' => 'edited article',
//                'body' => '{title}'
//            ]
//        ]
//    ],


];