<?php

namespace app\modules\articles\events;

use app\modules\articles\models\Articles;
use app\modules\notify\components\Notification;

class ArticleNotify extends Notification
{
    public $model_id;

    private $_model;

    public function getTitle()
    {
        return $this->getEntity()->title;
    }

    public function getDescription()
    {
        return $this->getEntity()->content;
    }

    public function getAuthor()
    {
       return $this->getEntity()->user->getFullName();
    }

    /**
     * @return Articles
     */
    public function getEntity()
    {
        if(!$this->_model){
            $this->_model = Articles::findOne($this->model_id);
        }
        return $this->_model;
    }
}