<?php

namespace app\modules\articles\controllers;

use app\components\CustomController;
use app\models\Constants;
use app\models\FilterHelper;
use app\models\main\Pages;
use app\modules\articles\models\ArtclCategory;
use app\modules\articles\models\Articles;
use Yii;
use yii\data\ArrayDataProvider;
use yii\web\HttpException;

class DefaultController extends CustomController
{

    public $viewUrl = '/articles/';
    public $indexUrl = '/articles';

    /**
     * Return error twig array
     *
     * @return array
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'app\components\FlatErrorAction',
                'view' => 'error.twig'
            ],

            'image-upload' => [
                'class' => 'app\components\ImageUploadAction',
                'url' => '/upload/articles-comments', // Directory URL address, where files are stored.
                'path' => Yii::getAlias('@app') . '/web/upload/articles-comments' // Or absolute path to directory where files are stored.
            ],
            'file-upload' => [
                'class' => 'vova07\imperavi\actions\UploadAction',
                'url' => '/articles/default/file', // Directory URL address, where files are stored.
                'path' => Yii::getAlias('@app') . '/data/upload/articles-comments', // Or absolute path to directory where files are stored.
                'uploadOnlyImage' => false,
            ],
            'file-load' => [
                'class' => 'app\components\DownloadFileAction',
                'path' => Yii::getAlias('@app') . '/data/upload/articles-comments'
            ]
        ];
    }

    /**
     * Redirect to company/index
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $models = Articles::find()->notDeleted()->enabled()->orderBy('created_at DESC')->all();

        FilterHelper::filterByDeletedCategory($models);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $models,
            'pagination' => [
                'defaultPageSize' => 10,
            ],
        ]);

        return $this->render('index.twig', [
            'dataProvider' => $dataProvider,
            'model' => Pages::getCeoCurrentPage(),
            //'filterGET' => $filter,
        ]);
    }

    public function actionCategory($category)
    {
        $models = null;

        ArtclCategory::isValidAlias($category, $models, $category);
        //filtering by checkboxes
        // $filter = false;
        //FilterHelper::filterByCategoryGET($models, 'category_id', $filter);

        $this->breadcrumbs[] = [
            'label' => 'Articles',
            'url' => '/articles',
            'active' => 'active',
        ];

        $dataProvider = new ArrayDataProvider([
            'allModels' => $models,
            'pagination' => [
                'defaultPageSize' => 10,
            ],
        ]);

        return $this->render('index.twig', [
            'dataProvider' => $dataProvider,
            'model' => $category,
            //'filterGET' => $filter,
        ]);
    }


    /**
     * @param $category
     * @param $alias
     * @return string
     * @throws HttpException
     */
    public function actionView($category, $alias)
    {
        $model = Articles::find()->notDeleted()->enabled()->alias($alias)->one();

        if (!is_object($model)) {
            throw new HttpException(404);
        }

        Yii::$app->counter->setCount($model);

        $this->breadcrumbs[] = [
            'label' => 'Articles',
            'url' => '/articles',
            'active' => 'active',
        ];

        if (isset($model->category))
            $this->breadcrumbs[] = [
                'label' => $model->category->title,
                'url' => $model->category->getViewUrl(),
                'active' => 'active',
            ];

        return $this->render('view.twig', [
            'model' => $model,//for ceo
            'models' => Articles::find()
                ->where('category_id=' . $model->category_id)
                ->enabled()
                ->andWhere(['<>', 'alias', $model->alias])
                ->limit(10)
                ->notDeleted()
                ->all(),

        ]);
    }

    public function actionSearch()
    {
        $models = Yii::$app->search->getCollection(['title', 'content'], Yii::$app->request->get('search'));

        if (empty($models->all()))
            Yii::$app->session->setFlash('warning', 'Nothing found.');

        return $this->render('search.twig', [
            'dataProvider' => $this->getDataProvider($models, Constants::SEARCH_COUNT_ARTICLES),
        ]);
    }
}
