<?php

namespace app\modules\articles;

use app\components\Application;
use app\models\community\Comments;
use app\modules\articles\models\Articles;
use yii\base\BootstrapInterface;
use yii\base\Event;
use yii\db\ActiveRecord;
use yii\db\AfterSaveEvent;


/**
 * Created by PhpStorm.
 * User: igor
 * Date: 03.02.16
 * Time: 13:31
 */
class ArticlesModule extends \yii\base\Module implements BootstrapInterface
{
    const EVENT_ARTICLE_ADD = 'articles.topic.add';

    const EVENT_ARTICLE_EDIT = 'articles.topic.edit';

    public $controllerNamespace = 'app\modules\articles\controllers';

    public function init()
    {
        \Yii::configure($this, require(dirname(__FILE__) . '/config/main.php'));
    }

    public function bootstrap($app)
    {
        // TODO: Implement bootstrap() method.
        \Yii::$app->urlManager->addRules(
            [
                # Articles /default
                //'articles/<page:\d+>' => 'articles/index',  # for article pagination
                'articles/image-upload' => 'articles/default/image-upload', # * http://protrader.my/forum/topic/new-topic
                'articles/file-upload' => 'articles/default/file-upload', # * http://protrader.my/forum/topic/new-topic
                'articles/file-upload' => 'articles/default/file-upload', # * http://protrader.my/forum/topic/new-topic
                'articles/default/file/<filename:.+>' => 'articles/default/file-load',
                'articles' => 'articles/default/index',  # for article pagination

                'articles/<category>/<alias>' => 'articles/default/view',

                'articles/<category:.+>' => 'articles/default/category',

            ]);

        //Подписать на события, пользователя, если он создал новый комментарий
        //или новый топик
        if($app instanceof Application){
            if($app->getModule('notify')){
                Event::on(Articles::className(), ActiveRecord::EVENT_AFTER_INSERT, function(AfterSaveEvent $event)use($app){
                    $app->getModule('notify')->subscriber->subscribe($event->sender, \Yii::$app->user->identity->id);
                });
                Event::on(Comments::className(), ActiveRecord::EVENT_AFTER_INSERT, function(AfterSaveEvent $event)use($app){
                    $app->getModule('notify')->subscriber->subscribe($event->sender, \Yii::$app->user->identity->getId());

                    //Если добавлен новый комментарий, то автоматически подписать на сущность самого топика
                    $app->getModule('notify')->subscriber->subscribe($event->sender->getEntity(), \Yii::$app->user->identity->getId());
                });
            }
        }

    }


}