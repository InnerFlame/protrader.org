<?php

namespace app\modules\articles\models;
use app\components\Entity;

/**
 * This is the ActiveQuery class for [[ArtclCategory]].
 *
 * @see ArtclCategory
 */
class ArtclCategoryQuery extends \yii\db\ActiveQuery
{
    /**
     * @inheritdoc
     * @return ArtclCategory[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ArtclCategory|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @return $this
     */
    public function notDeleted()
    {
        return $this->andWhere(['deleted' => Entity::NOT_DELETED]);
    }
}
