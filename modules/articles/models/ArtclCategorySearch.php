<?php

namespace app\modules\articles\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ArtclCategorySearch represents the model behind the search form about `app\modules\articles\models\ArtclCategory`.
 */
class ArtclCategorySearch extends ArtclCategory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'parent', 'weight', 'published', 'deleted', 'created_at', 'updated_at'], 'integer'],
            [['title', 'alias', 'data'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ArtclCategory::find()->notDeleted();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'id'         => $this->id,
            'parent'     => $this->parent,
            'weight'     => $this->weight,
            'published'  => $this->published,
            'deleted'    => $this->deleted,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'alias', $this->alias])
            ->andFilterWhere(['like', 'data', $this->data]);

        return $dataProvider;
    }
}
