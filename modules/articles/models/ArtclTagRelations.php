<?php

namespace app\modules\articles\models;

use app\models\CustomModel;
use app\models\Tag;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Html;

/**
 * This is the model class for table "artcl_tag".
 *
 * @property integer $id
 * @property integer $article_id
 * @property integer $tag_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 *
 * @property Tag $tag
 * @property ArtclArticles $article
 */
class ArtclTagRelations extends CustomModel
{
    public static $_pull = [];
    public $title;
    public $published;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'artcl_tag_relations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['article_id', 'tag_id'], 'required'],
            [['title'], 'unique'],
            [['article_id', 'tag_id', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'article_id' => 'Article ID',
            'tag_id' => 'Tag ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'deleted_at' => 'Deleted At',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTag()
    {
        return $this->hasOne(ArtclTag::className(), ['id' => 'tag_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasOne(Articles::className(), ['id' => 'article_id']);
    }

    public static function getGridColumns()
    {
        return [
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => '<span>#</span>',
                'contentOptions' => [
                    'width' => 50
                ],
            ],
            [
                'attribute' => 'article_id',
                'format' => 'raw',
                'value' => function ($model) {
                    if (is_object($model->articles))
                        return $model->articles->title;
                }
            ],

            [
                'attribute' => 'tag_id',
                'format' => 'raw',
                'value' => function ($model) {
                    if (is_object($model->tag))
                        return $model->tag->title;
                }
            ],

            [
                'class' => 'app\components\grid\ActionColumnCustom',
                'template' => function ($model) {
                    return '{edit} {delete}';
                },
                'buttons' => [
                    'edit' => function ($url, $model, $key, $options) {
                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/articles-tag-edit', 'id' => $model->id]);
                        return Html::a('Edit', $url, $options);
                    },
                    'delete' => function ($url, $model, $key, $options) {
                        $options['onclick'] = '
                            if(confirm(\'' . Yii::t('yii', 'Are you sure you want to delete this Page?') . '\'))
                            {
                              return true;
                            } else {
                              return false;
                            }
                        ';
//                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/articles-tag-delete', 'id' => $model->id]);
                        return Html::a('Delete', $url, $options);
                    },

                ],
            ],
        ];
    }
}