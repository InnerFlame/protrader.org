<?php

namespace app\modules\articles\models;

use app\components\ceo\Ceo;
use app\components\ceo\CeoBehavior;
use app\components\Entity;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\HttpException;

/**
 * This is the model class for table "artcl_category".
 *
 * @property integer $id
 * @property string $title
 * @property string $alias
 * @property string $data
 * @property integer $weight
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ArtclArticles[] $artclArticles
 */
class ArtclCategory extends Entity
{
    public static $_pull = [];
    public $viewUrl;
    static $stringAttr = ['title', 'alias'];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'artcl_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'unique'],
            [['id', 'weight', 'created_at', 'updated_at', 'weight'], 'integer'],
            [['title', 'alias'], 'string', 'max' => 120, 'min' => 3],
            [['alias', 'data',], 'string', 'max' => 256],
            ['published', 'safe'],
            ['alias', 'match', 'pattern' => '/^[a-zA-Z0-9\-]+$/']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'title'      => 'Title',
            'alias'      => 'Url',
            'data'       => 'Data',
            'weight'     => 'Weight',
            'published'  => 'Published',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
            [
                'class' => CeoBehavior::className(),
            ],
            [
                'class'         => SluggableBehavior::className(),
                'attribute'     => 'title',
                'slugAttribute' => 'alias',
                'immutable'     => 'true',
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasMany(Articles::className(), ['category_id' => 'id'])->where([
            'deleted' => Entity::NOT_DELETED
        ]);
    }

    /**
     * @return string
     */
    public function getViewUrl()
    {
        return '/articles/' . $this->alias;
    }

    /**
     * @return int
     */
    public function getTypeEntity()
    {
        return Entity::CATEGORY_ARTICLE;
    }

    /**
     * @return $this
     */
    public function getCeo()
    {
        return $this->hasOne(Ceo::className(), ['entity_id' => 'id'])->where([
            'entity' => $this->getTypeEntity()
        ]);
    }

    /**
     * @param string $category_alias
     * @return bool
     */
    public static function isValidCategory($category_alias = '')
    {
        $cat = self::find()->where(['alias' => $category_alias])->one();
        if (is_object($cat))
            return true;
    }

    /**
     * @param string $name
     * @return null
     */
    public static function getCategoryIdByName($name = '')
    {
        $model = self::find()->where(['title' => $name])->one();

        if (is_object($model)) {
            return $model->id;
        }
        return null;
    }

    /**
     * @param null $category_alias
     * @param $models
     * @throws HttpException
     */
    public static function isValidAlias($category_alias = null, &$models, &$category)
    {
        if ($category_alias) {
            $category = ArtclCategory::find()->where(['alias' => $category_alias])->notDeleted()->one();
            if (!is_object($category))
                throw new HttpException(404);
            $models = Articles::find()->orderBy('created_at DESC')->where(['category_id' => $category->id])->notDeleted()->enabled()->all();
        } else {
            $models = Articles::find()->orderBy('created_at DESC')->notDeleted()->enabled()->all();
        }
    }

    /**
     * @param null $except
     * @return mixed
     */
    public static function getList($except = null)
    {
        $models = self::find()->notDeleted()->all();

        foreach ($models as $key => $model) {
            $model->viewUrl = $model->getViewUrl();
        }

        $result['/articles'] = 'All';

        return $result + ArrayHelper::map($models, 'viewUrl', 'title');
    }

    /**
     * @return array
     */
    public static function getCategoryList()
    {
        $models = self::find()->notDeleted()->all();

        return ArrayHelper::map($models, 'id', 'title');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->title;
    }

    public static function getGridColumns()
    {
        return [
            [
                'class'          => 'yii\grid\SerialColumn',
                'header'         => '<span>#</span>',
                'contentOptions' => [
                    'width' => 50
                ],
            ],
            [
                'attribute' => 'title',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'alias',
                'format'    => 'raw',
            ],

            [
                'class'    => 'app\components\grid\ActionColumnCustom',
                'template' => function ($model) {
                    return '{edit} {delete}';
                },
                'buttons'  => [
                    'edit'   => function ($url, $model, $key, $options) {
                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/articles-category-edit', 'id' => $model->id]);
                        return Html::a('Edit', $url, $options);
                    },
                    'delete' => function ($url, $model, $key, $options) {
                        $options['onclick'] = '
                            if(confirm(\'' . Yii::t('yii', 'Are you sure you want to delete this Page?') . '\'))
                            {
                              return true;
                            } else {
                              return false;
                            }
                        ';
//                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/articles-category-delete', 'id' => $model->id]);
                        return Html::a('Delete', $url, $options);
                    },

                ],
            ],
        ];
    }
}