<?php

namespace app\modules\articles\models;

use app\components\ceo\Ceo;
use app\components\ceo\CeoBehavior;
use app\components\customImage\CustomImage;
use app\components\Entity;
use app\components\langInput\LangInputBehavior;
use app\models\community\Comments;
use app\models\community\Counts;
use app\models\Constants;
use app\models\user\User;
use app\modules\articles\ArticlesModule;
use app\modules\articles\events\AddArticleEvent;
use app\modules\articles\events\AddArticleNotify;
use app\modules\articles\events\ArticleNotify;
use app\modules\notify\components\Event;
use app\modules\notify\components\EventFree;
use himiklab\sitemap\behaviors\SitemapBehavior;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "artcl_articles".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $user_id
 * @property string $title
 * @property string $alias
 * @property string $pictures
 * @property string $content
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ArtclCategory $category
 */
class Articles extends Entity
{
    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 0;

    public static $_pull = [];
    static $stringAttr = ['title', 'alias'];

    public $image = null;
    public $crop = null;

    public $sort;
    public $replies;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'artcl_articles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'content', 'title'], 'required'],
            [['id', 'user_id', 'status', 'category_id', 'updated_at'], 'integer'],
            [['alias'], 'string', 'max' => 255, 'min' => 3],
            [['published', 'pictures', 'user_id', 'alias', 'created_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'category_id' => 'Category ID',
            'user_id'     => 'User ID',
            'title'       => 'Title H1',
            'alias'       => 'Url',
            'pictures'    => 'pictures',
            'content'     => 'Content',
            'status'      => 'Status',
            'published'   => 'Published',
            'created_at'  => 'Created',
            'updated_at'  => 'Updated At',
            'replies'     => 'Replies',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
            [
                'class'      => LangInputBehavior::className(),
                'attributes' => ['content', 'title'],
            ],
            [
                'class' => CeoBehavior::className(),
            ],
            [
                'class'         => SluggableBehavior::className(),
                'attribute'     => 'title',
                'slugAttribute' => 'alias',
                'immutable'     => 'true',
                //  'value' => function ($event) {
                //      ActiveRecord::EVENT_BEFORE_INSERT()
                //     return str_replace(' ', '-', $this->title);
                // }
            ],

            [
                'class'       => SitemapBehavior::className(),
                'scope'       => function ($model) {
                    /** @var \yii\db\ActiveQuery $model */
                    $model->select(['alias', 'updated_at', 'id']);
                    $model->andWhere(['status' => self::STATUS_ENABLE]);
                    $model->andWhere(['deleted' => self::STATUS_DISABLE]);
                    $model->orderBy(['updated_at' => SORT_DESC]);
                },
                'dataClosure' => function ($model) {
                    /** @var self $model */

                    return [
                        'loc'        => Url::toRoute(['/articles/' . Articles::findOne($model->id)->category->alias . '/' . $model->alias]),
                        'lastmod'    => date('Y-m-d\TH:i:sP', $model->updated_at),
                        'changefreq' => SitemapBehavior::CHANGEFREQ_WEEKLY,
                        'priority'   => 0.9
                    ];
                }
            ],
        ];
    }

    public function init()
    {
        $this->image = new CustomImage([
            'uploadPath' => isset(Yii::$app->params['articlesUploadPath']) ?
                Yii::getAlias('@webroot') . Yii::$app->params['articlesUploadPath'] : '',
            'uploadUrl'  => isset(Yii::$app->params['articlesUploadPath']) ?
                Yii::getAlias('@web') . Yii::$app->params['articlesUploadPath'] : '',
            'model'      => $this,
        ]);

        $this->image->setValidation('pictures', [
            'extensions' => 'png,jpg',
            'maxHeight'  => 4000,
            'maxWidth'   => 4000,
            'maxSize'    => 2048000,
            'minSize'    => 1,
        ]);

        parent::init();
    }

    /**
     * BeforeSave action
     * Checking client images
     *
     * @return mixed
     */
    public function beforeSave($insert)
    {

        if (!$this->image->isChanged('pictures')) unset($this->pictures);
        if ($this->image->isDeleted('pictures')) {
            $this->pictures = '';
        }

        if (!isset($this->status))
            $this->status = self::STATUS_ENABLE;

        parent::beforeSave($insert);


        $this->published = strtotime($this->published);
        return true;
    }

    /**
     *
     * After Save
     *
     * @return mixed
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($this->scenario <> 'delete') {
            if ($this->image->getUploadedFile('pictures')) {

                $this->pictures = $this->image->uploadImage('pictures', true);

                if ($this->pictures)
                    $this->save(false);
            }
        }


        $eventName = $insert ? ArticlesModule::EVENT_ARTICLE_ADD : ArticlesModule::EVENT_ARTICLE_EDIT;
        $notify = new ArticleNotify(['model_id' => $this->id]);
        $notify->subscribed = false;
        $event = new Event(['sender' => $notify]);
        Yii::$app->trigger($eventName, $event);
        parent::afterSave($insert, $changedAttributes);
        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ArtclCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comments::className(), ['entity_id' => 'id'])->where([
            'entity' => $this->getTypeEntity()
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function findFeature()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'title');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCounts()
    {
        return $this->hasOne(Counts::className(), ['entity_id' => 'id'])->where([
            'entity' => $this->getTypeEntity()
        ]);
    }

    /**
     * @inheritdoc
     * @return ArticlesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ArticlesQuery(get_called_class());
    }

    public static function getListByCategory($except_current_id = null, $category_id = null)
    {
        $models = self::find()->where(['category_id' => $category_id])->limit(10)->all();

        foreach ($models as $key => $model) {
            if ($model->id === $except_current_id)
                unset($models[$key]);
        }


        return ArrayHelper::map($models, 'alias', 'title');

    }

    public function getPictureLink($alias = null, $params_attr = 'userUpload...', $attr = 'pictures')
    {

        $model = self::find()->where(['alias' => $alias])->notDeleted()->one();

        if (!is_object($model)) {
            return 'http://placehold.it/101x101';
        }

        if ($model->id > 0 && $model->$attr) {
            return Yii::$app->params[$params_attr] . '/' . $model->id . '/' . $model->$attr;
        }

        return 'http://placehold.it/101x101';
    }


    public function getCeo()
    {
        return $this->hasOne(Ceo::className(), ['entity_id' => 'id'])->where([
            'entity' => $this->getTypeEntity()
        ]);
    }

    public function getName()
    {
        return $this->title;
    }

    public static function getGridColumns()
    {
        return [
            [
                'class'          => 'yii\grid\SerialColumn',
                'header'         => '<span>#</span>',
                'contentOptions' => [
                    'width' => 50
                ],
            ],

            [
                'attribute' => 'created_at',
                'format'    => 'date',
            ],

            [
                'attribute' => 'title',
                'format'    => 'raw',

                'value' => function ($model) {
                    return '<a class="entity-link" href ="' . $model->getViewUrl() . '">' . $model->title . '</a>';
                },
            ],

            [
                'attribute' => 'category_id',
                'format'    => 'raw',
                'value'     => function ($model) {
                    return '<a class="category-link" href ="' . $model->category->getViewUrl() . '">' . $model->category->title . '</a>';
                },
            ],


            [
                'attribute' => 'user_id',
                'format'    => 'raw',
                'value'     => function ($model) {
                    if (is_object($model->user))
                        return '<a class="profile-link" href ="' . $model->user->getProfileLink() . '">' . $model->user->getFullname() . '</a>';
                },
            ],


            [
                'attribute' => 'replies',
                'format'    => 'raw',
                'value'     => function ($model) {

                    if (is_array($model->comments)) {
                        return count($model->comments);
                    }

                },
            ],

            [
                'class'    => 'app\components\grid\ActionColumnCustom',
                'template' => function ($model) {
                    return '{edit} {delete}';
                },

                'buttons' => [
                    'edit'   => function ($url, $model, $key, $options) {
                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/article-edit', 'id' => $model->id]);
                        return Html::a('Edit', $url, $options);
                    },
                    'delete' => function ($url, $model, $key, $options) {
                        $options['onclick'] = '
                            if(confirm(\'' . Yii::t('yii', 'Are you sure you want to delete this Page?') . '\'))
                            {
                              return true;
                            } else {
                              return false;
                            }
                        ';
//                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/article-delete', 'id' => $model->id]);
                        return Html::a('Delete', $url, $options);
                    },

                ],
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */

    public static function getStatusList()
    {
        return [
            self::STATUS_ENABLE  => 'Enable',
            self::STATUS_DISABLE => 'Disable',
        ];
    }

    public static function getSortList()
    {
        return [
            Constants::SORT_BY_DATE    => 'Date',
            Constants::SORT_BY_POPULAR => 'Popular',
        ];
    }

    public function getDate($template = 'F j, Y')
    {
        return $this->published > 0 ? date($template, $this->published) : date($template, $this->created_at);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getViewUrl()
    {
        return '/articles/' . $this->category->alias . '/' . $this->alias;
    }

    public function getTypeEntity()
    {
        return Entity::ARTICLE;
    }

}