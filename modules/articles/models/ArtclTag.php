<?php

namespace app\modules\articles\models;


use app\models\CustomModel;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "tag".
 *
 * @property integer $id
 * @property string $title
 * @property integer $published
 * @property integer $count
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 *
 * @property ArtclTag[] $artclTags
 */
class ArtclTag extends CustomModel
{
    public static $_pull = [];
    public $article_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'artcl_tag';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title',], 'unique'],
            [['count', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['title'], 'string', 'max' => 255, 'min' => 3],
            [['published'], 'safe',]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'published' => 'Published',
            'count' => 'Count',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'deleted_at' => 'Deleted At',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArtclTags()
    {
        return ArrayHelper::map(ArtclTagRelations::find()->where(['tag_id' => $this->id])->all(), 'id', 'article_id');
    }

    public static function getGridColumns()
    {
        return [
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => '<span>#</span>',
                'contentOptions' => [
                    'width' => 50
                ],
            ],
            [
                'attribute' => 'title',
                'format' => 'raw',
            ],
            [
                'attribute' => 'count',
                'format' => 'raw',
            ],
            [
                'class' => 'app\components\grid\ActionColumnCustom',
                'template' => function ($model) {
                    return '{edit} {delete}';
                },
                'buttons' => [
                    'edit' => function ($url, $model, $key, $options) {
                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/articles-tag-edit', 'id' => $model->id]);
                        return Html::a('Edit', $url, $options);
                    },
                    'delete' => function ($url, $model, $key, $options) {
                        $options['onclick'] = '
                            if(confirm(\'' . Yii::t('yii', 'Are you sure you want to delete this Page?') . '\'))
                            {
                              return true;
                            } else {
                              return false;
                            }
                        ';
//                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/articles-tag-delete', 'id' => $model->id]);
                        return Html::a('Delete', $url, $options);
                    },

                ],
            ],
        ];
    }
}