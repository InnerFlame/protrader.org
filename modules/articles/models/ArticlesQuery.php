<?php

namespace app\modules\articles\models;

/**
 * This is the ActiveQuery class for [[Articles]].
 *
 * @see Articles
 */
class ArticlesQuery extends \yii\db\ActiveQuery
{
    /**
     * @inheritdoc
     * @return Articles[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    public function orderByCreate()
    {
        return $this->orderBy('created_at DESC');
    }

    /**
     * @inheritdoc
     * @return Articles|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param $alias
     * @return $this
     */
    public function alias($alias)
    {
        return $this->andWhere(['alias' => $alias]);
    }

    /**
     * @return $this
     */
    public function notDeleted()
    {
        return $this->andWhere(['<>', 'deleted', 1]);
    }

    /**
     * @return $this
     */
    public function enabled() {

        return $this->andWhere(['status' => Articles::STATUS_ENABLE]);

    }
}