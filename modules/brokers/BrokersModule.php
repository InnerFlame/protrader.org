<?php

namespace app\modules\brokers;

use yii\base\BootstrapInterface;


/**
 * Created by PhpStorm.
 * User: igor
 * Date: 03.02.16
 * Time: 13:31
 */
class BrokersModule extends \yii\base\Module implements BootstrapInterface
{

    public $controllerNamespace = 'app\modules\brokers\controllers';


    public function init()
    {
        \Yii::configure($this, require(dirname(__FILE__) . '/config/main.php'));
    }

    public function bootstrap($app)
    {
        // TODO: Implement bootstrap() method.
        \Yii::$app->urlManager->addRules(
            [
                # Brokers
                'brokers/request-broker' => 'brokers/default/request-broker',
                'brokers/test' => 'brokers/default/test',
                'brokers/<alias>' => 'brokers/default/view',
                'en/member/login' => 'brokers/default/register',

                # Features
                'features' => 'brokers/features/index',
                'features/<category>/<alias>' => 'brokers/features/view',
            ]);

    }


}