<?php

namespace app\modules\brokers\controllers;

use app\components\CustomController;
use app\components\Entity;
use app\components\mailer\CustomMailer;
use app\components\sypexGeo\GeoHelper;
use app\models\community\Counts;
use app\models\community\Likes;
use app\models\main\Pages;
use app\modules\brokers\models\Broker;
use app\modules\brokers\models\forms\RequestBrokerForm;
use app\modules\brokers\models\ImprovementsCategory;
use app\modules\brokers\models\SearchBroker;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\HttpException;

class DefaultController extends CustomController
{
    public $viewUrl = '/brokers/';

    public $indexUrl = '/brokers';

    public $search_visible = false;

    /**
     * Return error twig array
     *
     * @return array
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'app\components\FlatErrorAction',
                'view'  => 'error.twig'
            ],
        ];
    }


    /**
     * Redirect to company/index
     *
     * @return mixed
     */
    public function actionIndex()
    {

        $dataProviderConnected = new ActiveDataProvider([
            'query' => Broker::find()->where(['deleted' => Entity::NOT_DELETED])->andWhere(['status' => Broker::STATUS_CONNECTED]),
            'pagination' => false
        ]);

        return $this->render('index.twig', [
            'request_broker' => new RequestBrokerForm(),
            'model'          => Pages::getCeoCurrentPage(),
            'dataProviderConnected'   => $dataProviderConnected,
        ]);
    }

    /**
     * @param $alias
     * @return string
     * @throws HttpException
     */
    public function actionView($alias)
    {
        $model = Broker::find()->where(['alias' => $alias])->notDeleted()->andWhere('status <> :status', [':status' => Broker::STATUS_REFUSED])->one();

        if (!is_object($model)) {
            throw new HttpException(404);
        }

        $this->breadcrumbs[] = [
            'label'  => 'Brokers',
            'url'    => $this->indexUrl,
            'active' => 'active',
        ];

        $view = $model->type == Broker::TYPE_BROKER ? 'view.twig' : 'data_feed.twig';
        return $this->render($view, [
            'model'      => $model,
            'categories' => $this->getExistCategories($model),
        ]);
    }


    /**
     * window Сообшить об ошибке
     */
    public function actionRequestBroker()
    {
        if (Yii::$app->request->post()) {
            $model = new RequestBrokerForm();

            if ($model->load(Yii::$app->request->post())) {

                $form_data = [
                    'name'    => $model->name,
                    'link'    => $model->link,
                    'comment' => $model->comment,
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'time' => date('d.m.Y H:i:s')
                ];

                //$params = array_merge($form_data, GeoHelper::getGeoDataToMail());

                if (CustomMailer::send(Yii::$app->params['adminEmail'], 'request-broker.twig', $form_data)) {
                    Yii::$app->session->setFlash('success', Yii::t('app', 'Thank you for your request. It has been successfully sent.'));
                } else {
                    Yii::$app->session->setFlash('danger', Yii::t('app', ' It was not successfully sent.'));
                }

                return $this->redirect(Yii::$app->request->referrer);
            }
        }
        Yii::$app->session->setFlash('warning', Yii::t('app', 'Error.'));
        return $this->goHome();
    }

    /**
     *
     */
    public function actionSetLike()
    {
//        Yii::$app->user->identity->id = 8;
        if (Likes::setLike(Entity::NEWS))
            Counts::setCount(Entity::NEWS);

        echo Counts::getCount(Entity::NEWS);
    }

    public function actionRegister()
    {
        $this->breadcrumbs = null;

        return $this->render('register.twig');
    }

    public function getExistCategories($model)
    {
        $categories = [];
        foreach ($model->brokerImprovement as $category) {
            $model = ImprovementsCategory::findOne($category->category_id);

            if (!is_object($model)) {
                throw new HttpException(404);
            }

            if (!in_array($model, $categories))
                $categories[$category->category_id] = $model->title;
        }
        return $categories;
    }


    /**
     * window Сообшить об ошибке
     */
    public function actionTest()
    {
        if (Yii::$app->request->post()) {
            $model = new RequestBrokerForm();

            if ($model->load(Yii::$app->request->post())) {

                $form_data = [
                    'name'    => $model->name,
                    'link'    => $model->link,
                    'comment' => $model->comment,
                ];

                $params = array_merge($form_data, GeoHelper::getGeoDataToMail());

                if (CustomMailer::send(Yii::$app->params['adminEmail'], 'test-geo-data.twig', $params)) {
                    Yii::$app->session->setFlash('success', Yii::t('app', 'Thank you for your request. It has been successfully sent.'));
                } else {
                    Yii::$app->session->setFlash('danger', Yii::t('app', ' It was not successfully sent.'));
                }

                return $this->redirect(Yii::$app->request->referrer);
            }
        }
        Yii::$app->session->setFlash('warning', Yii::t('app', 'Error.'));
        return $this->goHome();
    }
}
