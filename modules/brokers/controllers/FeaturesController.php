<?php

namespace app\modules\brokers\controllers;


use app\components\CustomController;
use app\models\main\Pages;
use app\modules\brokers\models\Broker;
use app\modules\brokers\models\forms\RequestBrokerForm;
use app\modules\brokers\models\Improvement;
use app\modules\brokers\models\ImprovementsCategory;
use Yii;
use yii\web\HttpException;

class FeaturesController extends CustomController
{

    public $search_visible = false;
    public $viewUrl = '/features';

    /**
     * Return error twig array
     *
     * @return array
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'app\components\FlatErrorAction',
                'view' => 'error.twig'
            ],
        ];
    }

    public function init()
    {

        return parent::init();

    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index.twig', [
            'categories' => ImprovementsCategory::find()->orderBy('weight DESC')->all(),
            'brokers' => Broker::find()->where('status = ' . Broker::STATUS_CONNECTED)->notDeleted()->groupBy('name')->all(),
            'model' => Pages::getCeoCurrentPage(),
            'request_broker' => new RequestBrokerForm()
        ]);
    }


    /**
     * @param $alias
     * @return string
     * @throws HttpException
     */
    public function actionView($alias)
    {
        $model = Improvement::find()->notDeleted()->alias($alias)->one();

        if (!is_object($model)) {
            throw new HttpException(404);
        }

        $this->breadcrumbs[] = [
            'label' => 'Features',
            'url' => $this->viewUrl,
        ];


        return $this->render('view.twig', [
            'model' => $model,
        ]);

    }
}
