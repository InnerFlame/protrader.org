<?php

namespace app\modules\brokers\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ImprovementSearch represents the model behind the search form about `app\modules\brokers\models\Improvement`.
 */
class ImprovementSearch extends Improvement
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'category_id', 'is_main', 'is_broker', 'status', 'deleted', 'created_at', 'updated_at', 'published_at'], 'integer'],
            [['title', 'alias', 'content'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Improvement::find()->notDeleted();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'id'           => $this->id,
            'user_id'      => $this->user_id,
            'category_id'  => $this->category_id,
            'is_main'      => $this->is_main,
            'is_broker'    => $this->is_broker,
            'status'       => $this->status,
            'deleted'      => $this->deleted,
            'created_at'   => $this->created_at,
            'updated_at'   => $this->updated_at,
            'published_at' => $this->published_at,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'alias', $this->alias])
            ->andFilterWhere(['like', 'content', $this->content]);

        return $dataProvider;
    }
}
