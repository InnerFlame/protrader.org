<?php

namespace app\modules\brokers\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\brokers\models\BrokerImprovement;

/**
 * BrokerImprovementSearch represents the model behind the search form about `app\modules\brokers\models\BrokerImprovement`.
 */
class BrokerImprovementSearch extends BrokerImprovement
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['broker_id', 'improvement_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BrokerImprovement::find()->select('*, count(*) as count')->groupBy('broker_id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'broker_id' => $this->broker_id,
            'improvement_id' => $this->improvement_id,
        ]);

        return $dataProvider;
    }
}
