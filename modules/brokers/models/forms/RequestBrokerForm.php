<?php

namespace app\modules\brokers\models\forms;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class RequestBrokerForm extends Model
{
    public $name;
    public $comment;
    public $link;

    public $type;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['name', 'link','comment'], 'required'],
            [['link'], 'url', 'message' => 'need url'],
            [['name'], 'string', 'min' => 2, 'max' => 255]
        ];
    }

}
