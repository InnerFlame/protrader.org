<?php

namespace app\modules\brokers\models;

use app\components\Entity;
use app\models\DefaultQuery;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Html;

/**
 * This is the model class for table "frm_improvements_category".
 *
 * @property integer $id
 * @property string $title
 * @property string $alias
 * @property string $data
 * @property integer $weight
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 */
class ImprovementsCategory extends Entity
{
    /*
    |--------------------------------------------------------------------------
    | Constants && properties
    |--------------------------------------------------------------------------
    */

    public static $_pull = [];

    /*
    |--------------------------------------------------------------------------
    | Model configurations
    |--------------------------------------------------------------------------
    */

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'frm_improvements_category';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class'         => SluggableBehavior::className(),
                'attribute'     => 'title',
                'slugAttribute' => 'alias',
                'immutable'     => 'true',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['weight', 'created_at', 'updated_at'], 'integer'],
            [['title'], 'string', 'max' => 120],
            [['alias', 'data'], 'string', 'max' => 256]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('app', 'ID'),
            'title'      => Yii::t('app', 'Title'),
            'alias'      => Yii::t('app', 'Alias'),
            'data'       => Yii::t('app', 'Data'),
            'weight'     => Yii::t('app', 'Weight'),
            'published'  => Yii::t('app', 'Published'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     * @return ImprovementsCategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DefaultQuery(get_called_class());
    }

    /*
    |--------------------------------------------------------------------------
    | Model relations
    |--------------------------------------------------------------------------
    */

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImprovement()
    {
        return $this->hasMany(Improvement::className(), ['category_id' => 'id'])->notDeleted()->isMain();
    }

    public function getViewUrl()
    {
        return '/features/' . $this->alias;
    }

    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */

    public static function getGridColumns()
    {
        return [
            [
                'class'          => 'yii\grid\SerialColumn',
                'header'         => '<span>#</span>',
                'contentOptions' => [
                    'width' => 50
                ],
            ],
            [
                'attribute' => 'title',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'alias',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'weight',
                'format'    => 'raw',
            ],
            [
                'class'    => 'app\components\grid\ActionColumnCustom',
                'template' => function ($model) {
                    return '{edit} {delete}';
                },
                'buttons'  => [
                    'edit'   => function ($url, $model, $key, $options) {
                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/improvement-category/edit', 'id' => $model->id]);
                        return Html::a('Edit', $url, $options);
                    },
                    'delete' => function ($url, $model, $key, $options) {
                        $options['onclick'] = '
                            if(confirm(\'' . Yii::t('yii', 'Are you sure you want to delete this Category?') . '\'))
                            {
                              return true;
                            } else {
                              return false;
                            }
                        ';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/improvement-category/delete', 'id' => $model->id]);
                        return Html::a('Delete', $url, $options);
                    },

                ],
            ],
        ];
    }

    public function getTypeEntity()
    {
        return self::FEATURES_CATEGORY;
    }

    public function getName()
    {
        return $this->title;
    }
}
