<?php

namespace app\modules\brokers\models;

use app\models\abstractes\AbstractCustomValue;
use Yii;

/**
 * This is the model class for table "broker_custom_value".
 *
 * @property integer $broker_id
 * @property integer $field_id
 *
 * @property Broker $broker
 * @property BrokerCustomField $field
 */
class BrokerCustomValue extends AbstractCustomValue
{
    # get entitity attribute
    public static function getEntitityAttr()
    {
        return 'broker_id';
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'broker_custom_value';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['broker_id', 'field_id'], 'required'],
            [['broker_id', 'field_id'], 'integer'],
            [['value'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'broker_id' => 'Broker ID',
            'field_id' => 'Field ID',
            'value' => 'Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBroker()
    {
        return $this->hasOne(Broker::className(), ['id' => 'broker_id']);
    }

    public function getBrokerById($id)
    {
        return Broker::find()->where(['id' => $id])->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function getField()
    {
        return $this->hasOne(BrokerCustomField::className(), ['id' => 'field_id']);
    }

    public static function setValue($field_id, $entitity_id, $value)
    {
        if($field_id > 0 && $entitity_id > 0)
        {
            $model = self::getCustomValueModel($field_id, $entitity_id);
            $model->value = $value;

            return $model->save();
        }

        return false;
    }

}