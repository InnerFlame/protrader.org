<?php

namespace app\modules\brokers\models;
use app\components\Entity;

/**
 * This is the ActiveQuery class for [[Broker]].
 *
 * @see Broker
 */
class BrokerQuery extends \yii\db\ActiveQuery
{
    /**
     * @inheritdoc
     * @return Broker[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Broker|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @return $this
     */
    public function notDeleted()
    {
        return $this->andWhere(['<>', 'deleted', 1]);
    }

    /**
     * @return $this
     */
    public function newest()
    {
        return $this->orderBy('created_at DESC');
    }

    /**
     * @return $this
     */
    public function typeBroker()
    {
        return $this->andWhere('type = :type', [':type' => Broker::TYPE_BROKER]);
    }

    /**
     * @return $this
     */
    public function typeFeed()
    {
        return $this->andWhere('type = :type', [':type' => Broker::TYPE_FEED]);
    }

    public function alias($alias)
    {
        return $this->andWhere(['alias' => $alias]);
    }
}
