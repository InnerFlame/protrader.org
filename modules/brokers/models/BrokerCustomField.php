<?php

namespace app\modules\brokers\models;

use app\models\abstractes\AbstractCustomFields;
use Yii;


/**
 * This is the model class for table "broker_custom_field".
 *
 * @property integer $id
 * @property string $name
 * @property string $alias
 * @property string $data
 *
 * @property BrokerCustomValue[] $brokerCustomValues
 * @property Broker[] $brokers
 */
class BrokerCustomField extends AbstractCustomFields
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'broker_custom_field';
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrokerCustomValues()
    {
        return $this->hasMany(BrokerCustomValue::className(), ['field_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrokers()
    {
        return $this->hasMany(Broker::className(), ['id' => 'broker_id'])->viaTable('broker_custom_value', ['field_id' => 'id']);
    }

    public static function setField($alias, $entitity_id, $value)
    {
        $field = self::getFieldByAlias($alias);

        if(is_object($field))
            $custom_value = BrokerCustomValue::setValue($field->id, $entitity_id, $value);

    }

}