<?php

namespace app\modules\brokers\models;

/**
 * This is the ActiveQuery class for [[ImprovementsCategory]].
 *
 * @see ImprovementsCategory
 */
class ImprovementsCategoryQuery extends \yii\db\ActiveQuery
{
    /**
     * @inheritdoc
     * @return ImprovementsCategory[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ImprovementsCategory|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @return $this
     */
    public function notDeleted()
    {
        return $this->andWhere('<>', 'deleted', 1);
    }
}
