<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2016
 */

namespace app\modules\brokers\models;


use app\components\Entity;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class SearchBroker extends Broker
{

    public function rules()
    {
        return [
            [['id', 'status', 'deleted', 'type', 'created_at', 'updated_at'], 'integer'],
            [['name', 'alias'], 'safe'],
        ];
    }
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Broker::find()
            ->leftJoin('com_counts', 'broker.id = com_counts.entity_id AND com_counts.entity = :entity', [':entity' => Entity::BROKER])
            ->andWhere('status <> :status', [':status' => Broker::STATUS_REFUSED])
            ->andWhere(['deleted' => Entity::NOT_DELETED])
            ->orderBy('status ASC, com_counts.count_like DESC');


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'type' => $this->type,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'alias', $this->alias]);


        return $dataProvider;
    }
}