<?php

namespace app\modules\brokers\models;

/**
 * This is the ActiveQuery class for [[BrokerImprovement]].
 *
 * @see BrokerImprovement
 */
class BrokerImprovementQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return BrokerImprovement[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return BrokerImprovement|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
