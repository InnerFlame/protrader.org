<?php

namespace app\modules\brokers\models;

use app\components\ceo\Ceo;
use app\components\ceo\CeoBehavior;
use app\components\Entity;
use app\components\langInput\LangInputBehavior;
use app\models\community\Counts;
use app\models\community\Likes;
use app\models\DefaultQuery;
use himiklab\sitemap\behaviors\SitemapBehavior;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "frm_improvements".
 *
 * @property integer $id
 * @property string $title
 * @property string $content
 * @property integer $status
 * @property integer $created_at
 * @property integer $published_at
 */
class Improvement extends Entity
{
    /*
    |--------------------------------------------------------------------------
    | Constants && properties
    |--------------------------------------------------------------------------
    */

    const PATH = 'app\modules\brokers\models\Improvement';

    const STATUS_ACTIVE = 0;
    const STATUS_DEVELOP = 1;
    const STATUS_FINISH = 2;

    const IS_MAIN = 1;

    const SORT_POPULAR = 'popular';
    const SORT_DATE = 'date';

    public static $_pull = [];
    public $slide;

    public static $_status_list = [
        self::STATUS_ACTIVE  => 'Active',
        self::STATUS_DEVELOP => 'In Development',
        self::STATUS_FINISH  => 'Finished',
    ];

    /*
    |--------------------------------------------------------------------------
    | Model configurations
    |--------------------------------------------------------------------------
    */

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'frm_improvements';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],

            [
                'class' => CeoBehavior::className(),
            ],
            [
                'class'      => LangInputBehavior::className(),
                'attributes' => ['content', 'title'],
            ],
            [
                'class'         => SluggableBehavior::className(),
                'attribute'     => 'title',
                'slugAttribute' => 'alias',
                'immutable'     => 'true',
            ],
            [
                'class'       => SitemapBehavior::className(),
                'scope'       => function ($model) {
                    /** @var \yii\db\ActiveQuery $model */
                    $model->select(['id', 'alias', 'updated_at']);
                    $model->andWhere(['deleted' => 0]);
                    $model->orderBy(['updated_at' => SORT_DESC]);
                },
                'dataClosure' => function ($model) {
                    /** @var self $model */
                    $improvement = Improvement::findOne($model->id);
                    if (is_object($improvement->category))
                        return [
                            'loc'        => Url::toRoute(['/features/' . $improvement->category->alias . '/' . $model->alias]),
                            'lastmod'    => date('Y-m-d\TH:i:sP', $model->updated_at),
                            'changefreq' => SitemapBehavior::CHANGEFREQ_WEEKLY,
                            'priority'   => 0.2
                        ];
                }
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'content', 'category_id'], 'required'],
            [['content'], 'string'],
            [['status', 'created_at', 'updated_at', 'user_id', 'is_main', 'is_broker'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['published_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'           => Yii::t('app', 'ID'),
            'user_id'      => Yii::t('app', 'User Id'),
            'category_id'  => Yii::t('app', 'Category Id'),
            'is_main'      => Yii::t('app', 'Is Main'),
            'is_broker'    => Yii::t('app', 'Is  Broker'),
            'title'        => Yii::t('app', 'Title H1'),
            'alias'        => Yii::t('app', 'Alias'),
            'content'      => Yii::t('app', 'Content'),
            'status'       => Yii::t('app', 'Status'),
            'created_at'   => Yii::t('app', 'Created At'),
            'updated_at'   => Yii::t('app', 'Updated At'),
            'published_at' => Yii::t('app', 'Published At'),
        ];
    }

    /**
     * @inheritdoc
     * @return ImprovementQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DefaultQuery(get_called_class());
    }

    /*
    |--------------------------------------------------------------------------
    | Model relations
    |--------------------------------------------------------------------------
    */

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ImprovementsCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCounts()
    {
        return $this->hasOne(Counts::className(), ['entity_id' => 'id'])->where([
            'entity' => $this->getTypeEntity()
        ]);
    }

    public function getCeo()
    {
        return $this->hasOne(Ceo::className(), ['entity_id' => 'id'])->where([
            'entity' => $this->getTypeEntity()
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrokerImprovement()
    {
        return $this->hasMany(Broker::className(), ['id' => 'broker_id'])->where(['deleted' => Entity::NOT_DELETED])
            ->viaTable(BrokerImprovement::tableName(), ['improvement_id' => 'id']);
    }


    public function getSlides()
    {
        return $this->hasMany(FeatureSlide::className(), ['entity_id' => 'id'])
            ->where(['entity' => Entity::FEATURES])->andWhere(['deleted' => FeatureSlide::NOT_DELETED])->orderBy(['weight' => SORT_DESC]);
    }

    public function getLastSlide()
    {
        return count($this->slides) > 0 ? $this->slides[0] : null;
    }

    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */

    public static function getStatus($id)
    {
        switch ($id) {
            case self::STATUS_ACTIVE:
                return 'Active';
            case self::STATUS_DEVELOP:
                return 'Development';
            case self::STATUS_FINISH:
                return 'Implemented';
        }
    }

    public function getName()
    {
        return $this->title;
    }

    /**
     * @return Improvement|array|null
     */
    public static function getRandomFeature()
    {
        $user_id = Yii::$app->user->isGuest ? null : Yii::$app->user->identity->id;

        $already_liked = Likes::find()->where(['entity' => Entity::FEATURES, 'user_id' => $user_id])->all();

        $ids = ArrayHelper::getColumn($already_liked, 'entity_id');// ids that user already set like

        $model = self::find()->where(['not in', 'id', $ids])->orderBy('RAND()')->notDeleted()->one();
        if (is_object($model))
            return $model;

        return null;

    }

    public static function getStatusList()
    {
        return [
            self::STATUS_ACTIVE  => 'Active',
            self::STATUS_DEVELOP => 'Development',
            self::STATUS_FINISH  => 'Implemented',
        ];
    }

    public static function getStatusClass($model, $style = '')
    {
        switch ($model->status) {
            case self::STATUS_ACTIVE:
                # is disabled
                return Likes::isLike(Entity::COMMUNITY_COMMENT, $model->id) ?
                    '<a data-class-name="{{ getClassName(model) }}" style="' . $style . '" data-id="' . $model->id . '" type="button" class="btn btn-xs btn-primary like-btn"><i class="fa mr-5 fa-thumbs-o-up"></i> Нравится</a>'
                    : '<a data-class-name="{{ getClassName(model) }}" style="' . $style . '" data-id="' . $model->id . '" type="button" class="btn btn-xs btn-primary disabled like-btn"><i class="fa mr-5 fa-thumbs-o-up"></i> Нравится</a>';
            case self::STATUS_DEVELOP:
                return '<span style="' . $style . '" class="label border-warning text-warning pl-10 pr-10 pt-5 pb-5"><i class="fa fa-gears mr-5"></i> в разработке</span>';
            case self::STATUS_FINISH:
                return '<span style="' . $style . '" class="label border-success text-success pl-10 pr-10 pt-5 pb-5"><i class="fa fa-check-square-o mr-5"></i> Реализован</span>';
        }
    }

    public static function getLike($id)
    {
        $model = Counts::find()->where([
            'entity'    => Entity::COMMUNITY_COMMENT,
            'entity_id' => $id,
        ])->one();

        if (is_object($model))
            return $model->count_like;
        return 0;
    }

    public static function getList($except = null)
    {
        $brokers = Improvement::find()->innerJoin('frm_improvements_category', 'frm_improvements.category_id = frm_improvements_category.id AND frm_improvements_category.deleted = 0')->where(['frm_improvements.is_broker' => 1,
                                                'frm_improvements.deleted' => 0])->all();

        return ArrayHelper::map($brokers, 'id', 'title');
    }

    public function getSlide()
    {
        return FeatureSlide::find()->where(['entity' => Entity::FEATURES, 'entity_id' => $this->id])->notDeleted()->all();
    }


    public static function getFullList()
    {
        $brokers = Improvement::find()->orderBy('title')->all();

        return ArrayHelper::map($brokers, 'id', 'title');
    }

    public static function getGridColumns()
    {
        return [
            [
                'class'          => 'yii\grid\SerialColumn',
                'header'         => '<span>#</span>',
                'contentOptions' => [
                    'width' => 75
                ],
            ],
            [
                'attribute' => 'category_id',
                'value'     => function ($model) {

                    if (isset($model->category))
                        return $model->category->title;
                },
                'format'    => 'raw',
            ],
            [
                'attribute' => 'title',
                'format'    => 'raw',
            ],

            [
                'attribute' => 'status',
                'value'     => function ($model) {
                    return self::getStatus($model->status);
                },
                'format'    => 'raw',
            ],
            [
                'attribute' => 'is_main',
                'value'     => function ($model) {
                    if ($model->is_main)
                        return '<span class="label label-success">Active</span>';
                    else
                        return '<span class="label label-default">Inactive</span>';
                },
                'format'    => 'raw',
            ],
            [
                'attribute' => 'is_broker',
                'value'     => function ($model) {
                    if ($model->is_broker)
                        return '<span class="label label-success">Active</span>';
                    else
                        return '<span class="label label-default">Inactive</span>';
                },
                'format'    => 'raw',
            ],

            [
                'attribute' => 'published_at',
                'format'    => 'date',
            ],
            [
                'attribute' => 'slide',
                'format'    => 'raw',
                'value'     => function ($model) {
                    $slides = $model->getSlide();

                    if (is_array($slides)) {
                        $result = [];
                        foreach ($slides as $slide) {

                            $weight = null;

                            if ($slide->weight)
                                $weight = ' (weight = ' . $slide->weight . ')';


                            $url = Html::a('picture #' . (count($result) + 1) . $weight,
                                Yii::$app->urlManager->createUrl(
                                    [
                                        '/cabinet/improvement-edit-slide', 'feature_id' => $model->id, 'id' => $slide->id
                                    ]
                                )
                            );
                            $result[] = $url;


                        }
                    }
                    return implode('<br>', $result);

                },
            ],
            [
                'class'    => 'app\components\grid\ActionColumnCustom',
                'template' => function ($model) {
                    return '{edit} {add_slide} {delete}';
                },
                'buttons'  => [
                    'edit'      => function ($url, $model, $key, $options) {
                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/improvement/edit', 'id' => $model->id]);
                        return Html::a('Edit', $url, $options);
                    },
                    'add_slide' => function ($url, $model, $key, $options) {
                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/improvement-edit-slide', 'feature_id' => $model->id]);
                        return Html::a('Add slide', $url, $options);
                    },
                    'delete'    => function ($url, $model, $key, $options) {
                        $options['onclick'] = '
                            if(confirm(\'' . Yii::t('yii', 'Are you sure you want to delete this Category?') . '\'))
                            {
                              return true;
                            } else {
                              return false;
                            }
                        ';
//                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/improvement/delete', 'id' => $model->id]);
                        return Html::a('Delete', $url, $options);
                    },
                ],
            ],
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getViewUrl()
    {
        if (is_object($this->category))
            return $this->category->getViewUrl() . '/' . $this->alias;

        return '/features/' . $this->alias;
    }

    public function getTypeEntity()
    {
        return Entity::FEATURES;
    }

}
