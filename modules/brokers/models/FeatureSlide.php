<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 16.12.15
 * Time: 15:54
 */
namespace app\modules\brokers\models;

use app\components\customImage\CustomImage;
use app\components\Entity;
use app\models\abstractes\AbstractSlide;
use Yii;
use yii\helpers\Html;

class FeatureSlide extends AbstractSlide
{

    public function init()
    {
        $this->image = new CustomImage([
            'uploadPath' => isset(Yii::$app->params['featureUploadPath']) ?
                Yii::getAlias('@webroot') . Yii::$app->params['featureUploadPath'] : '',
            'uploadUrl' => isset(Yii::$app->params['featureUploadPath']) ?
                Yii::getAlias('@web') . Yii::$app->params['featureUploadPath'] : '',
            'model' => $this,
        ]);

        $this->image->setValidation('pictures', [
            'extensions' => 'png,jpg',
            'maxHeight' => 4000,
            'maxWidth' => 4000,
            'maxSize' => 2048000,
            'minSize' => 1,
        ]);

        parent::init();
    }


    /**
     *
     * After Save
     *
     * @return mixed
     */
    public function afterSave($insert, $changedAttributes)
    {

        if($this->scenario <> 'delete')
        {
            if ($this->image->getUploadedFile('pictures')) {

                $this->pictures = $this->image->uploadImage('pictures', false);

                if ($this->pictures)
                    $this->save(false);

            }
        }

        return parent::afterSave($insert, $changedAttributes);
    }


    public function getImprovement()
    {
        return $this->hasOne(Improvement::className(), ['id' => 'entity_id']);
    }



    public static function getHomePageImage(Entity $model, $attr = 'pictures'){

        if(is_object($model) ) {

            $slide = self::find()->where(['entity' => $model->getTypeEntity(), 'entity_id' => $model->id])->one();

            if(is_object($slide))
                return Html::img(Yii::$app->params['featureUploadPath'] .'/'. $slide->id .'/'. $slide->$attr);

            return '<img src="http://placehold.it/340x150">';
        }

    }
}