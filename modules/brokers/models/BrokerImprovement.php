<?php

namespace app\modules\brokers\models;

use app\models\CustomModel;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "broker_custom_improvement".
 *
 * @property integer $broker_id
 * @property integer $improvement_id
 *
 * @property Broker $broker
 * @property BrokerCustomField $field
 */
class BrokerImprovement extends CustomModel
{
    /*
    |--------------------------------------------------------------------------
    | Constants && properties
    |--------------------------------------------------------------------------
    */

    public static $_pull = [];

    public $count;
    public $edit_mode;

    /*
    |--------------------------------------------------------------------------
    | Model configurations
    |--------------------------------------------------------------------------
    */

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'broker_improvements';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['broker_id', 'improvement_id'], 'required'],
            [['broker_id'], 'integer'],
        ];
    }

    public function afterValidate()
    {
        if (!isset($_POST['BrokerImprovement']['improvement_id'])) {
            $this->addError('improvement_id', 'Improvement cannot be blank.');
            return false;
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'broker_id'      => 'Broker',
            'improvement_id' => 'Improvement',
        ];
    }

    /*
    |--------------------------------------------------------------------------
    | Model relations
    |--------------------------------------------------------------------------
    */

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBroker()
    {
        return $this->hasOne(Broker::className(), ['id' => 'broker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function getImprovement()
    {
        return $this->hasOne(Improvement::className(), ['id' => 'improvement_id']);
    }

    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */

    public function attachImprovements($broker_id = null)
    {
        $broker_id = is_null($broker_id) ? Yii::$app->request->post('BrokerImprovement')['broker_id'] : $broker_id;

        BrokerImprovement::deleteAll(['broker_id' => $broker_id]);

        if (isset(Yii::$app->request->post('BrokerImprovement')['improvement_id'])) {
            for ($i = 0; $i < count(Yii::$app->request->post('BrokerImprovement')['improvement_id']); $i++) {
                $model = new BrokerImprovement();
                $model->broker_id = $broker_id;
                $model->improvement_id = Yii::$app->request->post('BrokerImprovement')['improvement_id'][$i];
                $model->save();
            }
        }

        return true;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getImprovementsByBroker($id = null)
    {
        if ($id > 0) {
            $models = BrokerImprovement::find()->where('broker_id = ' . $id)->all();
            return ArrayHelper::map($models, 'improvement_id', 'improvement_id');
        }
        return null;
    }


    public function isAttach($broker_id, $improvement_id)
    {
        $model = BrokerImprovement::find()
            ->where('broker_id = ' . (int)$broker_id . ' AND improvement_id = ' . (int)$improvement_id)
            ->one();

        if (is_object($model))
            return true;
        return false;
    }
}