<?php

namespace app\modules\brokers\models;

use app\components\ceo\Ceo;
use app\components\ceo\CeoBehavior;
use app\components\customImage\CustomImage;
use app\components\Entity;
use app\components\widgets\like\Button;
use app\models\community\Counts;
use app\models\community\Likes;
use himiklab\sitemap\behaviors\SitemapBehavior;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\bootstrap\Html;

/**
 * This is the model class for table "broker".
 *
 * @property integer $id
 * @property string $name
 * @property string $logo
 * @property string $email
 * @property string $site
 * @property string $add_link
 * @property integer $status
 * @property string $account
 * @property string $regulation
 * @property string $traders
 * @property integer $is_demo
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted
 *
 */
class Broker extends Entity
{
    /*
    |--------------------------------------------------------------------------
    | Constants && properties
    |--------------------------------------------------------------------------
    */

    const STATUS_CONNECTED = 0;
    const STATUS_IN_PROGRESS = 1;
    const STATUS_IN_THE_VOTING = 2;
    const STATUS_REFUSED = 3;

    const TYPE_BROKER = 0;
    const TYPE_FEED = 1;

    public $image;
    public $crop;

    public $description;
    public $minimum_position_size;
    public $maximum_leverage;
    public $account_currencies;
    public $payment_methods_available;
    public $tradable_instruments;
    public $spread_type;
    public $spread;
    public $execution_type;
    public $number_of_currency_pairs;
    public $demo_url;
    public $real_url;

    public $custom_value_set = false;

    public $available_instruments;
    public $real_time_data;

    public $historical_data;
    public $pricing;
    public $pricing_url;

    /*
    |--------------------------------------------------------------------------
    | Model configurations
    |--------------------------------------------------------------------------
    */

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'broker';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
            [
                'class'         => SluggableBehavior::className(),
                'attribute'     => 'name',
                'slugAttribute' => 'alias',
                'immutable'     => 'true',
            ],
            [
                'class' => CeoBehavior::className(),
            ],
            [
                'class'       => SitemapBehavior::className(),
                'scope'       => function ($model) {
                    /** @var \yii\db\ActiveQuery $model */
                    $model->select(['alias', 'updated_at']);
                    $model->andWhere(['deleted' => 0]);
                    $model->orderBy(['updated_at' => SORT_DESC]);
                },
                'dataClosure' => function ($model) {
                    /** @var self $model */
                    return [
                        'loc'        => Url::toRoute(['/brokers/' . $model->alias]),
                        'lastmod'    => date('Y-m-d\TH:i:sP', $model->updated_at),
                        'changefreq' => SitemapBehavior::CHANGEFREQ_WEEKLY,
                        'priority'   => 0.2
                    ];
                }
            ],
        ];
    }

    /**
     *
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['is_demo', 'status', 'created_at', 'updated_at', 'deleted', 'votes'], 'integer'],
            [['name', 'email', 'site', 'account', 'regulation', 'traders'], 'string', 'max' => 128],
            [['add_link', 'instruments'], 'string', 'max' => 255],

            [['demo_url', 'real_url'], 'url'],

            [['logo', 'votes', 'minimum_position_size', 'maximum_leverage', 'account_currencies',
                'payment_methods_available', 'tradable_instruments', 'spread_type', 'spread',
                'execution_type', 'number_of_currency_pairs', 'description', 'demo_url', 'real_url', 'email',
                'available_instruments', 'real_time_data', 'historical_data', 'pricing', 'pricing_url'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                        => Yii::t('app', 'ID'),
            'name'                      => Yii::t('app', 'Title H1'),
            'alias'                     => Yii::t('app', 'Alias'),
            'email'                     => Yii::t('app', 'Email'),
            'site'                      => Yii::t('app', 'Site'),
            'status'                    => Yii::t('app', 'Status'),
            'add_link'                  => Yii::t('app', 'Add Link'),
            'logo'                      => Yii::t('app', 'Logo'),
            'account'                   => Yii::t('app', 'Minimum account size'),
            'regulation'                => Yii::t('app', 'Regulators'),
            'instruments'               => Yii::t('app', 'Instruments'),
            'traders'                   => Yii::t('app', 'Traders'),
            'is_demo'                   => Yii::t('app', 'Demo'),
            'votes'                     => Yii::t('app', 'Votes'),
            'created_at'                => Yii::t('app', 'Created At'),
            'updated_at'                => Yii::t('app', 'Updated At'),
            'deleted'                   => Yii::t('app', 'Deleted'),

            # custom fields
            'description'               => Yii::t('app', 'Description'),
            'minimum_position_size'     => Yii::t('app', 'Minimum Position Size'),
            'maximum_leverage'          => Yii::t('app', 'Maximum Leverage'),
            'account_currencies'        => Yii::t('app', 'Account Currencies'),
            'payment_methods_available' => Yii::t('app', 'Payment Methods Available'),
            'tradable_instruments'      => Yii::t('app', 'Tradable Instruments'),
            'spread_type'               => Yii::t('app', 'Spread Type'),
            'spread'                    => Yii::t('app', 'Spread'),
            'execution_type'            => Yii::t('app', 'Execution Type'),
            'number_of_currency_pairs'  => Yii::t('app', 'Number of Currency Pairs'),
            'demo_url'                  => Yii::t('app', 'Demo Url'),
            'real_url'                  => Yii::t('app', 'Real Url'),
            'type'                      => Yii::t('app', 'Type'),
            'available_instruments'     => Yii::t('app', 'Available Instruments'),

        ];
    }

    public function init()
    {
        $this->image = new CustomImage([
            'uploadPath' => isset(Yii::$app->params['brokerUploadPath']) ?
                Yii::getAlias('@webroot') . Yii::$app->params['brokerUploadPath'] : '',
            'uploadUrl'  => isset(Yii::$app->params['brokerUploadPath']) ?
                Yii::getAlias('@web') . Yii::$app->params['brokerUploadPath'] : '',
            'model'      => $this,
        ]);
        $this->image->setValidation('logo', [
            'extensions' => 'png,jpg',
            'maxHeight'  => 4000,
            'maxWidth'   => 4000,
            'maxSize'    => 2040,
            'minSize'    => 1,
        ]);

        parent::init();
    }

    /**
     * BeforeSave action
     * Checking client images
     *
     * @return mixed
     */
    public function beforeSave($insert)
    {
        if (!$this->image->isChanged('logo')) unset($this->logo);
        if ($this->image->isDeleted('logo')) $this->logo = '';

        return parent::beforeSave($insert);
    }

    /**
     *
     * After Save
     *
     * @return mixed
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($this->scenario <> 'delete') {
            if ($this->image->getUploadedFile('logo')) {
                $this->logo = $this->image->uploadImage('logo', false);
                if ($this->logo)
                    $this->save(false);
            }

            if (!$this->custom_value_set)
                foreach ($this->getCustomFields() as $field) {

                    /**
                     * if we need unset value
                     */
                    if ($this->$field || $this->$field == '') {
                        BrokerCustomField::setField($field, $this->id, $this->$field);
                    }
                }

            $this->custom_value_set = true;
        }

        return parent::afterSave($insert, $changedAttributes);
    }

    /*
    |--------------------------------------------------------------------------
    | Model relations
    |--------------------------------------------------------------------------
    */

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrokerImprovement()
    {
        return $this->hasMany(Improvement::className(), ['id' => 'improvement_id'])
            ->viaTable(BrokerImprovement::tableName(), ['broker_id' => 'id'])
            ->where('deleted != :status', [":status" => 1]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCounts()
    {
        return $this->hasOne(Counts::className(), ['entity_id' => 'id'])->where([
            'entity' => $this->getTypeEntity()
        ]);
    }


    public function getCeo()
    {
        return $this->hasOne(Ceo::className(), ['entity_id' => 'id'])->where([
            'entity' => $this->getTypeEntity()
        ]);
    }

    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */

    /**
     * @inheritdoc
     * @return BrokerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BrokerQuery(get_called_class());
    }

    public function getName()
    {
        return $this->name;
    }

    public static function getEmptyBrokers()
    {
        $models = BrokerImprovement::find()->all();
        $not_empty = ArrayHelper::map($models, 'broker_id', 'broker_id');

        $brokers = ArrayHelper::map(self::find()->notDeleted()->all(), 'id', 'name');

        foreach ($brokers as $key => $broker) {
            if (in_array($key, $not_empty))
                unset($brokers[$key]);
        }

        return $brokers;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomValue($broker_model, $field_name)
    {
        $field_model = BrokerCustomField::find()->where([
            'name' => $field_name,
        ])->one();

        if (is_object($field_model)) {
            $value_model = BrokerCustomValue::find()->where([
                'broker_id' => $broker_model->id,
                'field_id'  => $field_model->id,
            ])->one();

            if (is_object($value_model))
                return $value_model->value;
        }
    }

    public function getCustomFields()
    {
        if ($this->type == self::TYPE_FEED) {
            return [
                'available_instruments',
                'real_time_data',
                'historical_data',
                'pricing',
                'pricing_url',
                'demo_url',
                'description'
            ];
        }
        return [
            'description',
            'minimum_position_size',
            'maximum_leverage',
            'account_currencies',
            'payment_methods_available',
            'tradable_instruments',
            'spread_type',
            'spread',
            'execution_type',
            'number_of_currency_pairs',
            'demo_url',
            'real_url',
        ];
    }

    public static function getBrokerName($id)
    {
        $broker = Broker::findOne($id);

        if (is_object($broker)) {
            return $broker->name ? $broker->name : $broker->name;
        }

    }

    public function getImageUrl()
    {
        if ($this->logo)
            return $this->getPictureLink($this->id, 'brokerUploadPath', 'logo');

        return 'http://placehold.it/120x40';
    }


    public function getBrokerList()
    {
        $models = Broker::find()->all();
        return ArrayHelper::map($models, 'id', 'name');
    }

    public static function getStatusList()
    {
        return [
            self::STATUS_CONNECTED     => 'Connected',
            self::STATUS_IN_PROGRESS   => 'Development',
            self::STATUS_IN_THE_VOTING => 'Voting',
            self::STATUS_REFUSED       => 'Refused',
        ];
    }

    public function getStatusName()
    {
        return self::getStatus($this->status);
    }

    public static function getStatus($id)
    {
        $list = self::getStatusList();

        if (isset($list[$id]))
            return Yii::t('app', $list[$id]);

        return $list[$id];
    }

    public function getTypeList()
    {
        return [
            self::TYPE_BROKER => Yii::t('app', 'Broker'),
            self::TYPE_FEED   => Yii::t('app', 'Data Feed'),
        ];
    }

    public function getType()
    {
        if ($this->type == self::TYPE_BROKER)
            return Yii::t('app', 'Broker');

        return Yii::t('app', 'Data feed');
    }

    public static function getList($except = null)
    {
        $brokers = Broker::find()->notDeleted()->groupBy('name')->all();

        return ArrayHelper::map($brokers, 'id', 'name');
    }

    public static function getGridColumns($delete_link = '', $edit_link = '')
    {
        return [
            [
                'class'          => 'yii\grid\SerialColumn',
                'header'         => '<span>#</span>',
                'contentOptions' => [
                    'width' => 50
                ],
            ],
            [
                'attribute' => 'name',
                'format'    => 'raw',
                'value'     => function ($model) {
                    return "<a href='" . $model->getViewUrl() . "'>$model->name</a>";
                },
            ],
            [
                'attribute' => 'status',
                'value'     => function ($model) {
                    return self::getStatus($model->status);
                },
                'format'    => 'raw',
            ],
            [
                'attribute' => 'Votes',
                'format'    => 'raw',
                'value'     => function (Broker $model) {
                    $output =  '<span id="vote-counter-'.$model->id.'">'.$model->getCountLikes().'</span>';
                    $output .=  '&nbsp(<span title="Fake votes" id="vote-counter-fake-'.$model->id.'">'.$model->getCountLikesFake().'</span>)';
                    return $output;
                },
            ],
            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{up}&nbsp&nbsp{down}&nbsp&nbsp{reset}',
                'buttons' => [
                    'up' => function($url, $model, $key, $opts = []){
                        if($model instanceof Entity){
                            $options = array_merge([
                                'title' => Yii::t('yii', 'Vote Up'),
                                'aria-label' => Yii::t('yii', 'Vote Up'),
                                'onClick' => sprintf('voteUp("%s", "%s", "%s", "%s")', $model->getTypeEntity(), $model->id, "#vote-counter-".$model->id, "#vote-counter-fake-".$model->id),
                            ], $opts);
                            return Html::a('<span class="glyphicon glyphicon-thumbs-up"></span>', 'javascript:void(0);', $options);
                        }
                    },
                    'down' => function($url, $model, $key, $opts = []){
                        if($model instanceof Entity){
                                $options = array_merge([
                                    'title' => Yii::t('yii', 'Vote Down'),
                                    'aria-label' => Yii::t('yii', 'Vote Down'),
                                    'onClick' => sprintf('voteDown("%s", "%s", "%s", "%s")', $model->getTypeEntity(), $model->id, "#vote-counter-".$model->id, "#vote-counter-fake-".$model->id),
                            ], $opts);
                            return Html::a('<span class="glyphicon glyphicon-thumbs-down"></span>', 'javascript:void(0);', $options);
                        }
                    },
                    'reset' => function($url, $model, $key){
                        $options = [
                            'title' => Yii::t('yii', 'Reset votes'),
                            'aria-label' => Yii::t('yii', 'Reset votes'),
                            'onClick' => '
                            if(confirm(\'' . Yii::t('yii', 'Are you sure you want to reset all fake votes this Broker?') . '\'))
                            {
                              return true;
                            } else {
                              return false;
                            }
                        ',
                        ];
                        $url = sprintf('/cabinet/broker-vote-reset?entity=%s&entity_id=%s', $model->getTypeEntity(), $model->id);
                        return Html::a('<span class="glyphicon glyphicon-repeat"></span>', $url, $options);
                    }
                ]
            ],
            [
                'class'    => 'app\components\grid\ActionColumnCustom',
                'template' => function ($model) {
                    return '{edit} {delete}';
                },
                'buttons'  => [
                    'edit'   => function ($url, $model, $key, $options) use ($edit_link) {
                        $url = Yii::$app->urlManager->createUrl([!empty($edit_link) ? $edit_link : '/cabinet/broker/edit', 'id' => $model->id]);
                        return Html::a('Edit', $url, $options);
                    },
                    'delete' => function ($url, $model, $key, $options) use ($delete_link) {
                        $options['onclick'] = '
                            if(confirm(\'' . Yii::t('yii', 'Are you sure you want to delete this Broker?') . '\'))
                            {
                              return true;
                            } else {
                              return false;
                            }
                        ';
//
                        $url = Yii::$app->urlManager->createUrl([!empty($delete_link) ? $delete_link : '/cabinet/broker/delete', 'id' => $model->id]);
                        return Html::a('Delete', $url, $options);
                    },
                ],
            ],
        ];
    }

    public static function getGridColumnsFrontEnd()
    {
        return [
            [
                'header'         => Yii::t('app', 'Name Voting'),
                'enableSorting'  => true,
                'attribute'      => 'name',
                'format'         => 'raw',
                'value'          => function (Broker $model) {
                    return sprintf('<a href="%s" class="title" data-pjax="0">%s</a>', $model->getViewUrl(), Html::encode($model->name));
                },
                'contentOptions' => [
                    'class' => 'name'
                ],
                'headerOptions'  => [
                    'class' => 'col-lg-2'
                ]
            ],
            [
                'header'         => Yii::t('app', 'Type'),
                'enableSorting'  => true,
                'attribute'      => 'type',
                'format'         => 'raw',
                'value'          => function (Broker $model) {
                    return sprintf('<div class="type %s">%s</div>', $model->type == Broker::TYPE_BROKER ? 'brok' : 'df', $model->getType());
                },
                'contentOptions' => [
                    'class' => 'text-center typ'
                ],
                'headerOptions'  => [
                    'class' => 'col-lg-2'
                ]
            ],
            [
                'header'         => Yii::t('app', 'Instruments'),
                'enableSorting'  => true,
                'attribute'      => 'instruments',
                'format'         => 'raw',
                'value'          => function (Broker $model) {
                    return $model->getInstruments();
                },
                'contentOptions' => [
                    'class' => 'instruments'
                ],
                'headerOptions'  => [
                    'class' => 'col-lg-3 text-center'
                ]
            ],
            [
                'header'         => Yii::t('app', 'Votes'),
                'enableSorting'  => true,
                'attribute'      => 'votes',
                'format'         => 'raw',
                'value'          => function (Broker $model) {
                    if($model->status == Broker::STATUS_IN_THE_VOTING && Likes::isLike($model->getTypeEntity(), $model->id)){
                        return Button::widget(['count' => $model->getCountLikes(), 'entity' => $model]);
                    }
                    //Которые не голосуются выключены
                    return Button::widget(['count' => $model->getCountLikes(), 'entity' => $model]);
                },
                'contentOptions' => [
                    'class' => 'text-center votes'
                ],
                'headerOptions'  => [
                    'class' => 'col-lg-1 text-center'
                ]
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getViewUrl()
    {
        return '/brokers/' . $this->alias;
    }

    public function getTypeEntity()
    {
        return Entity::BROKER;
    }

    public function getInstruments()
    {
        if ($this->type == self::TYPE_FEED) {
            return $this->getCustomValue($this, 'available_instruments');
        }

        if ($this->type == self::TYPE_BROKER) {
            return $this->getCustomValue($this, 'tradable_instruments');
        }
    }

    public function getCountLikes()
    {
        return ($this->counts && $this->counts->count_like) ? $this->counts->count_like : 0;
    }

    public function getCountLikesFake()
    {
        return ($this->counts && $this->counts->count_like_fake) ? $this->counts->count_like_fake : 0;
    }
}
