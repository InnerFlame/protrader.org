<?php

namespace app\modules\brokers\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\brokers\models\Broker;

/**
 * BrokerSearch represents the model behind the search form about `app\modules\brokers\models\Broker`.
 */
class BrokerSearch extends Broker
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'votes', 'is_demo', 'deleted', 'updated_at', 'created_at', 'type'], 'integer'],
            [['name', 'alias', 'email', 'site', 'add_link', 'logo', 'account', 'regulation', 'instruments', 'traders'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $type)
    {
        $query = Broker::find()->notDeleted()->$type()->newest();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'votes' => $this->votes,
            'is_demo' => $this->is_demo,
            'deleted' => $this->deleted,
            'updated_at' => $this->updated_at,
            'created_at' => $this->created_at,
            'type' => $this->type,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'alias', $this->alias])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'site', $this->site])
            ->andFilterWhere(['like', 'add_link', $this->add_link])
            ->andFilterWhere(['like', 'logo', $this->logo])
            ->andFilterWhere(['like', 'account', $this->account])
            ->andFilterWhere(['like', 'regulation', $this->regulation])
            ->andFilterWhere(['like', 'instruments', $this->instruments])
            ->andFilterWhere(['like', 'traders', $this->traders]);

        return $dataProvider;
    }
}
