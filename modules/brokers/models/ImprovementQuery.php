<?php

namespace app\modules\brokers\models;

/**
 * This is the ActiveQuery class for [[Improvement]].
 *
 * @see Improvement
 */
class ImprovementQuery extends \yii\db\ActiveQuery
{
    /**
     * @inheritdoc
     * @return Improvement[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Improvement|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @return $this
     */
    public function notDeleted()
    {
        return $this->andWhere('<>', 'deleted', 1);
    }
}
