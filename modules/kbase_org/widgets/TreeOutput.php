<?php

namespace app\modules\kbase_org\widgets;

use app\modules\kbase\models\KbTree;
use yii\base\Widget;
use yii\helpers\ArrayHelper;

class TreeOutput extends Widget
{
    public $tree;

    public $node;

    public $parents;

    /**
     * @var string html output tree
     */
    protected $result;


    static $_except_old_node = [
        'Broker\'s documentation',
        'Releases',
        'Docs',
        'Android old',
        'iOS old',
        'Web',
        'Mobile',
        'Matrix old'
    ];

    /**
     *
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        return isset($this->node) ? $this->makeTree($this->tree, $this->getParents($this->node)) : $this->makeTree($this->tree);
    }

    /**
     * @param $current
     * @param array $parrents
     * @return string
     */
    public function makeTree($current, $parrents = [])
    {
        if($this->result){
            return $this->result;
        }
        $current_child = $current->children(1)->all();
        $parrent_ids = ArrayHelper::map($parrents, 'id', 'id');

        static::hideSomeFolders($current_child);

        $result = '<ul>';

        foreach($current_child as $current_node){

            if(!$current_node->visible)
                continue;

            $class = $current_node->isFolder ? 'folder ' : 'article ';
            $class .=  in_array($current_node->id, $parrent_ids) ? 'expanded received ' : '';
            $href = $current_node->getViewUrl();


            $result .= '<li class="' . $class . '" data-node="' . $href. '">';
            $result .= '<a href="' . $href . '">' . $current_node->title . '</a>';

            if(in_array($current_node->id, $parrent_ids))
                $result .= $this->makeTree($current_node, $parrents);

            $result .= '</li>';
        }

        $result .= '</ul>';

        $this->result = $result;
        return $result;
    }

    /**
     * @param $node
     * @return mixed
     */
    public function getParents($node)
    {
        $parents = $node->parents()->all();

        foreach($parents as $key => $parent){
            if($parent->level < 2)
                unset($parents[$key]);
        }

        array_push($parents, $node);

        return $parents;
    }

    /**
     * @param $current_child
     *
     * http://tp.pfsoft.net/entity/48411
     *
     */
    public static function hideSomeFolders(&$current_child)
    {

        foreach ($current_child as $key => $current_node) {

            if (in_array($current_node->title, static::$_except_old_node))
                unset($current_child[$key]);

        }

    }

}









































//public function run()
//{
//    $result = '';
//    $currLevel = 1;
//
//    foreach($this->tree as $currNode){
//        // Level down?
//        if ($currNode->level > $currLevel) {
//            // Yes, open <ul>
//            $result .= '<ul>';
//        }else{
//            $result .= '</li>';
//        }
//
//        // Level up?
//        if ($currNode->level < $currLevel) {
//            // Yes, close n open <ul>
//            $result .= str_repeat('</ul>', $currLevel - $currNode->level);
//            $result .= '</li>';
//        }
//
//        // Always add node
//        $result .= $currNode->isFolder ?
//            '<li class="folder">' . $currNode->title :
//            '<li><a href="/kb/en/knowledge-base/' . $currNode->getFullAlias() . '">' . $currNode->title . '</a>';
//        // Adjust current depth
//        $currLevel = $currNode->level;
//    }
//
//    $result .= str_repeat('</li></ul>', $currLevel + 1);
//
//    return $result;
//}






















//{% for item in tree %}
//<ul>
//<li class="folder">
//<a class="folder-name">{{ item.alias }}{{ item.level }}</a>
//<ul>
//<li class="folder"><a class="folder-name">Настройка панелей</a>
//<ul>
//<li><a href="#">Управление столбцами</a></li>
//<li><a href="#">Фильтрация и сортировка</a></li>
//<li><a href="#">Настройки</a></li>
//</ul>
//</li>
//<li><a href="#">Рабочие пространства</a></li>
//<li><a href="#">Строка состояния и лента меню</a></li>
//<li><a href="#">Организация панелей</a></li>
//<li><a href="#">Связь панелей</a></li>
//<li><a href="#">Экспорт из табличных панелей</a></li>
//</ul>
//</li>
//</ul>
//{% endfor %}