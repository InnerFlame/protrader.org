<?php

namespace app\modules\kbase_org\models;

use creocoder\nestedsets\NestedSetsBehavior;
use Yii;

/**
 * This is the model class for table "kb_tree".
 *
 * @property string $id
 * @property string $root
 * @property string $lft
 * @property string $rgt
 * @property integer $level
 * @property string $alias
 * @property string $id_old
 * @property integer $isFolder
 * @property integer $isClass
 * @property integer $pub
 * @property integer $visible
 * @property integer $access_authentication
 * @property integer $access_broker
 * @property integer $access_whitelist
 *
 * @property Articles[] $Articles

 */
class KbTree extends DefaultModel
{
    /*
    |--------------------------------------------------------------------------
    | Constants && properties
    |--------------------------------------------------------------------------
    */

    static $lang = 'en';

    public $title;
    public $content;
    public $article;

    /*
    |--------------------------------------------------------------------------
    | Model configurations
    |--------------------------------------------------------------------------
    */

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kb_tree';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'tree' => [
                'class'          => NestedSetsBehavior::className(),
                'treeAttribute'  => 'root',
                'leftAttribute'  => 'lft',
                'rightAttribute' => 'rgt',
                'depthAttribute' => 'level',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['root', 'lft', 'rgt', 'level', 'id_old', 'isFolder', 'isClass', 'pub', 'visible', 'access_authentication', 'access_broker', 'access_whitelist'], 'integer'],
            [['lft', 'rgt', 'level', 'alias', 'id_old'], 'required'],
            [['alias'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                    => Yii::t('app', 'ID'),
            'root'                  => Yii::t('app', 'Root'),
            'lft'                   => Yii::t('app', 'Lft'),
            'rgt'                   => Yii::t('app', 'Rgt'),
            'level'                 => Yii::t('app', 'Level'),
            'alias'                 => Yii::t('app', 'Alias'),
            'id_old'                => Yii::t('app', 'Id Old'),
            'isFolder'              => Yii::t('app', 'Is Folder'),
            'isClass'               => Yii::t('app', 'Is Class'),
            'pub'                   => Yii::t('app', 'Pub'),
            'visible'               => Yii::t('app', 'Visible'),
            'access_authentication' => Yii::t('app', 'Access Authentication'),
            'access_broker'         => Yii::t('app', 'Access Broker'),
            'access_whitelist'      => Yii::t('app', 'Access Whitelist'),
        ];
    }

    /**
     * @inheritdoc
     * @return KbTreeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new KbTreeQuery(get_called_class());
    }

    /*
    |--------------------------------------------------------------------------
    | Model relations
    |--------------------------------------------------------------------------
    */

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasMany(KbArticle::className(), ['doc_id' => 'id']);
    }

    /**
     * @return null|static
     */
    public function getSourceMessage()
    {
        return SourceMessage::findOne(['message' => 'node_' . $this->id . '_title']);
    }

    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */

    public function findOneByFullAlias($lang, $version, $full_alias, $root)
    {
        self::$lang = $lang;

        $aliasArr = explode('/', $full_alias);

        $models = KbTree::findAll([
            'alias' => $aliasArr[count($aliasArr)-1],
            'root' => $root,
            'level' => count($aliasArr) + 1,
            'visible' => 1
            //'pub' => 1,
        ]);

        if(count($models) > 0)
            foreach($models as $_model) {
                $parents = $_model->parents()->all();

                $is_valid = true;

                if(count($parents) > 0)
                    foreach($parents as $_parent){

                        if($_parent->level == 1)
                            if(($_parent->alias != $version))
                                $is_valid = false;

                        if($_parent->level > 1)
                            if(!in_array($_parent->alias, $aliasArr))
                                $is_valid = false;

                    }

                if($is_valid) return $_model;

            }

    }

    public function getFullAlias()
    {
        $aliasArr = [];

        $aliasArr[] = $this->getParentFullAlias();

        $aliasArr[] = $this->alias;

        return implode('/', $aliasArr);
    }

    public function getParentFullAlias()
    {
        $parents = $this->parents()->all();

        $aliasArr = [];

        if(count($parents) > 0)
            foreach($parents as $_parent) {
                $aliasArr[] = $_parent->alias;
            }

        return implode('/', $aliasArr);
    }
    public function getViewUrl()
    {
        return '/kb/en/knowledge-base/' . $this->getFullAlias();
    }

    /**
     * Generate and return array breadcrumbs
     * for this node
     * @return array
     */
    public function getBreadCrumbs()
    {
        $breadcrumbs = [];

        $parents = $this->parents()->all();

        foreach($parents as $key => $parent){
            if($parent->level < 2)
                unset($parents[$key]);
        }

        foreach($parents as $node){
            $breadcrumbs[] = ['url' => $node->getViewUrl(), 'label' => $node->title];
        }
//        $breadcrumbs[] = ['label' => $this->alias, 'active' => true];

        return $breadcrumbs;
    }

    public function getTranslateMessage($lang = null)
    {
        if(!$lang) $lang = self::$lang;

        $source = $this->getSourceMessage();

        if(is_object($source)) {
            $message = Message::find()
                ->where(['language' => $lang])
                ->andWhere(['id' => $source->id])
                ->one();

            if (is_object($message))
                return $message->translation;

            // if translation is empty
            $message = Message::find()
                ->where(['language' => self::DEFAULT_LANG])
                ->andWhere(['id' => $source->id])
                ->one();

            if (is_object($message))
                return $message->translation;

        }

    }

    public function afterFind()
    {
        parent::afterFind();

        $this->title = self::translateMessage('node_' . $this->id .'_title', self::$lang);

        if(!$this->isFolder) {

            $article = $this->article;

            if(is_object($article))
                $this->content = $article->content;

        }else{

            $this->content = self::translateMessage('node_' . $this->id .'_desc', self::$lang);

            if($this->content == 'node_' . $this->id .'_desc')
                $this->content = null;

        }

    }

    public static function translateMessage($node_title, $lang = 'en')
    {
        $source = SourceMessage::findOne(['message' => $node_title]);

        if(is_object($source)) {
            $message = Message::find()
                ->where(['language' => $lang])
                ->andWhere(['id' => $source->id])
                ->one();

            if (is_object($message))
                return $message->translation;

        }

    }

    public function getArticleContent()
    {
        $article =  KbArticle::findOne([
            'doc_id' => $this->id,
            'lang' => self::$lang
        ]);

        if(is_object($article)) return $article->content;

        // if translation is empty
        $article =  KbArticle::findOne([
            'doc_id' => $this->id,
            'lang' => self::DEFAULT_LANG
        ]);

        if(is_object($article)) return $article->content;

    }
}
