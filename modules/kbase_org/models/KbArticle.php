<?php

namespace app\modules\kbase_org\models;

use Yii;

/**
 * This is the model class for table "kb_article".
 *
 * @property integer $id
 * @property string $doc_id
 * @property string $old_doc_id
 * @property string $old_title
 * @property string $content
 * @property string $lang
 * @property integer $pub
 * @property integer $views
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property KbTree $doc
 */
class KbArticle extends DefaultModel
{

    /*
    |--------------------------------------------------------------------------
    | Constants && properties
    |--------------------------------------------------------------------------
    */


    /*
    |--------------------------------------------------------------------------
    | Model configurations
    |--------------------------------------------------------------------------
    */

    const NODE_BACKOFFICE_PT3 = 1442;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kb_article';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['doc_id', 'old_doc_id', 'created_at', 'updated_at'], 'required'],
            [['doc_id', 'old_doc_id', 'pub', 'views', 'created_at', 'updated_at'], 'integer'],
            [['content', 'lang'], 'string'],
            [['old_title'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('app', 'ID'),
            'doc_id'     => Yii::t('app', 'Doc ID'),
            'old_doc_id' => Yii::t('app', 'Old Doc ID'),
            'old_title'  => Yii::t('app', 'Old Title'),
            'content'    => Yii::t('app', 'Content'),
            'lang'       => Yii::t('app', 'Lang'),
            'pub'        => Yii::t('app', 'Pub'),
            'views'      => Yii::t('app', 'Views'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     * @return KbArticleQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new KbArticleQuery(get_called_class());
    }

    /*
    |--------------------------------------------------------------------------
    | Model relations
    |--------------------------------------------------------------------------
    */

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTree()
    {
        return $this->hasOne(KbTree::className(), ['id' => 'doc_id']);
    }


    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */

    public static function getQuery($node, $lang = 'en')
    {
        return self::find()
            ->from('kb_article t')
            ->join('LEFT JOIN', 'kb_tree doc','doc.id = t.doc_id')
            ->andWhere('doc.root = :root AND doc.isFolder = 0 AND t.lang = :lang AND doc.pub = 1 AND doc.visible = 1')
            ->andWhere('lft > ' . $node->lft . ' AND ' . 'rgt < ' . $node->rgt)
            ->params([':root' => $node->root, ':lang' => $lang])
            ->limit(10);
    }

    public static function getPopular($node)
    {
        return self::getQuery($node)
            ->orderBy('views DESC')
            ->all();
    }

    public static function getUpdates($node)
    {
        return self::getQuery($node)
            ->orderBy('updated_at DESC')
            ->all();
    }

    public static function getStarted($node)
    {
        return self::getQuery($node)
            ->orderBy('doc.lft')
            ->all();
    }

    public function __get($name)
    {
        if ($name == 'content')
        {
            $replace = (isset(Yii::$app->params['kbase']) && isset(Yii::$app->params['kbase']['appName'])) ? Yii::$app->params['kbase']['appName'] : 'protrader';
            return preg_replace('/{application}/i',$replace, $this->getAttribute($name));
        }
        return parent::__get($name);
    }

}
