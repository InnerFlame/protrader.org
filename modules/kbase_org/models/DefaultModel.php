<?php

namespace app\modules\kbase_org\models;

use Yii;

class DefaultModel extends \yii\db\ActiveRecord
{
    /**
     * @return null|object|\yii\db\Connection
     * @throws \yii\base\InvalidConfigException
     */
    public static function getDb()
    {
        return Yii::$app->get('db_kbase');
    }


}