<?php

namespace app\modules\kbase_org\models;

/**
 * This is the ActiveQuery class for [[KbArticle]].
 *
 * @see KbArticle
 */
class KbArticleQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return KbArticle[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return KbArticle|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}