<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 * Date: 03.03.2016
 * Time: 18:39
 */

namespace app\modules\kbase_org\commands;


use yii\console\Controller;

class ContentController extends Controller
{

    /**
     * Replaced text in content by reg ex
     * yii content/replace "/(?<!http:\/\/)protrader/i" "{application}"
     * @param $pattern
     * @param $replace
     */
    public function actionReplace($pattern, $replace)
    {

        $models = \app\modules\kbase_org\models\KbTree::findOne(379)->children()->all();

        $articles = [];
        foreach($models as $model)
        {
            $arts = \app\modules\kbase_org\models\KbArticle::find()->where('doc_id = :doc_id', [':doc_id' => $model->id])->andWhere(['like', 'content', 'protrader'])->all();
            $articles = \yii\helpers\ArrayHelper::merge($arts, $articles);
        }

        foreach($articles as $article)
        {
            $article->content = preg_replace($pattern, $replace, $article->getAttribute('content'));
            $article->save();
        }
    }
}