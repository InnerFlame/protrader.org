<?php

namespace app\modules\kbase_org\controllers;

use app\components\CustomController;
use app\modules\kbase_org\models\KbArticle;
use app\modules\kbase_org\models\KbTree;
use app\modules\kbase_org\widgets\TreeOutput;
use kartik\mpdf\Pdf;
use yii\helpers\VarDumper;
use yii\web\NotFoundHttpException;

class DefaultController extends CustomController
{
    const LEVEL_ROOT = 1;

    const NODE_PT3 = 378;

    const NODE_DESKTOP = 379;

    public $layout = false;

    /**
     * @return string
     */
    public function actionIndex()
    {
//        $node = KbTree::findOne(self::NODE_DESKTOP);
        $tree = KbTree::findOne(self::NODE_PT3);

        //TODO: избивится от данного харкода. Задача, что бы на главной
        //первый пукнт меню из сайдбара был открыт по умолчанию
        $this->redirect('/kb/en/knowledge-base/pt3/desktop');

        return $this->render('index.twig', [
//            'popular' => KbArticle::getPopular($node),
//            'updates' => KbArticle::getUpdates($node),
//            'started' => KbArticle::getStarted($node),
            'tree' => $tree
        ]);
    }
    public function actionSearch()
    {
        $tree = KbTree::findOne(self::NODE_DESKTOP);
        return $this->render('search.twig', ['tree' => $tree]);
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView()
    {
        $tree_pt = KbTree::find()->where([
            'alias' => 'pt3',
            'level' => self::LEVEL_ROOT,
        ])->one();

        $tree = KbTree::findOne(self::NODE_PT3);

        $kb = (new KbTree())->findOneByFullAlias('en', 'pt3', \Yii::$app->request->get('alias'), $tree_pt->id);
        if(!$kb)
            throw new  NotFoundHttpException();

        $this->breadcrumbs = $kb->getBreadCrumbs();

        return $this->render('view.twig', [
            'kb'   => $kb,
            'tree' => $tree,
        ]);
    }

    /**
     * @return string
     */
    public function actionGetNode()
    {
        $node = KbTree::findOne(\Yii::$app->getRequest()->post('node'));

        return TreeOutput::widget([
            'tree' => $node
        ]);
    }

//    public function actionPdf($id)
//    {
//        $node = KbTree::findOne($id);
//
////        $content = $this->isXmlStructureValid($node->getArticleContent()) ? $this->renderPartial('_reportView') : $node->getArticleContent();;
//        $content = $node->getArticleContent();;
//
//        // setup kartik\mpdf\Pdf component
//        $pdf = new Pdf([
//            // set to use core fonts only
//            'mode' => Pdf::MODE_CORE,
//            // A4 paper format
//            'format' => Pdf::FORMAT_A4,
//            // portrait orientation
//            'orientation' => Pdf::ORIENT_PORTRAIT,
//            // stream to browser inline
//            'destination' => Pdf::DEST_BROWSER,
//            // your html content input
//            'content' => $content,
//            // format content from your own css file if needed or use the
//            // enhanced bootstrap css built by Krajee for mPDF formatting
//            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
//            // any css to be embedded if required
//            'cssInline' => '.kv-heading-1{font-size:18px}',
//            // set mPDF properties on the fly
//            'options' => ['title' => 'Krajee Report Title'],
//            // call mPDF methods on the fly
//            'methods' => [
//                'SetHeader'=>['(c) ' . date("Y") . ' Protraider'],
//                'SetFooter'=>['{PAGENO}'],
//            ]
//        ]);
//
//        // return the pdf output as per the destination setting
//        return $pdf->render();
//    }

    /**
     * @param $xml
     * @return bool
     */
    function isXmlStructureValid($xml)
    {
        $prev = libxml_use_internal_errors(true);

        $doc = simplexml_load_string($xml);

        if ($doc) {
            try {
                $xml = new \SimpleXMLElement($xml);
            } catch (\Exception $e) {
                return false;
            }

            if (count(libxml_get_errors()) > 0)
                return false;

            libxml_clear_errors();

            libxml_use_internal_errors($prev);

            return true;
        }

        return false;
    }
}
