<?php

namespace app\modules\kbase_org\assets;

use yii\web\AssetBundle;

class KBaseAsset  extends AssetBundle
{
    public $sourcePath = '@app/modules/kbase_org/assets/media';

    public $js = [
        'js/main.js?vr=0.4'
    ];

    public $css = [
        //'css/default.min.css'
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}