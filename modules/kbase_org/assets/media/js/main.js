'use strict';

$('.category-content').on('click', '.folder', function(e){

    e.stopPropagation();

    var odj = $(this);
    var node = odj.attr('data-node');

    window.location.href = node;
});

$('.category-content').on('click', '.article', function(e){
    e.stopPropagation();
});
$(document).ready(function(){
    $(".page-content img").wrap(function() {
        return '<a href="'+$(this).attr('src')+'" class="link_popup_wrapper"></a>';
    });
    $('a.link_popup_wrapper').fancybox();
});
function createAncorsList() {
    if($('.articleText h2').length){
        var linksBlock = '<div class="articleSubLinks"><div class="artSublCol">';
        var headnum = 0;
        $('.articleText h2').each(function(){
            $(this).attr('id', $(this).text());
            if(headnum == 7){
                headnum = 1;
                linksBlock += '</div><div class="artSublCol"><a href="#'+$(this).text()+'"><i class="fa fa-caret-right"></i>'+ $(this).text() +'</a>';
            } else {
                linksBlock += '<a href="#'+$(this).text()+'"><i class="fa fa-caret-right"></i>'+ $(this).text() +'</a>';
                headnum++;
            }
        });
        linksBlock += '</div></div>';
        $('.articleText').prepend(linksBlock);
    }
}