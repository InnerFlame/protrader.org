<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 * Date: 21.04.2016
 * Time: 15:13
 */

namespace app\modules\codebase\widgets;


use app\modules\codebase\models\Category;
use nodge\eauth\Widget;
use yii\helpers\Url;
use yii\helpers\VarDumper;

class CeoWidget extends Widget
{

    public $categoryName;

    private $_mapSeoCategory = [
        'all' => [
            'title' => 'Trading {category} - PTMC Code Base{page}',
            'description' => 'PTMC Code Base provides source codes of mechanical trading {category} that will help you to automate trading. Download trading {category} for free{page}',
            'keywords' => '{page}{category}, code base, ptmc, trading platform, software, system, pfsoft, protrader'
        ],
        'default' =>[
            'title' => 'Download Trading Code for Better Trade - PTMC Code Base{page}',
            'description' => 'In PTMC Code Base section you can find all for algorithmic trading: electronic advisors, indicators, macroses, scripts and libraries. Click to learn more.{page}',
            'keywords' => '{page}code base, ptmc, trading platform, software, system, pfsoft, protrader'
        ]
    ];

    public function init()
    {
       if(\Yii::$app->request->get('alias')){
           $alias = \Yii::$app->request->get('alias');
           $category = Category::find()->where(['alias' => $alias])->notDeleted()->one();
           if($category){
               $this->categoryName = $category->title;
           }
       }
    }


    public function run()
    {
        $item = empty($this->categoryName) ? $this->_mapSeoCategory['default'] : $this->_mapSeoCategory['all'];
        $page = \Yii::$app->request->get('page');
        $title = str_replace(['{category}', '{page}'], [$this->categoryName, $page ? ' - Page '.$page : ''], $item['title']);
        $description = str_replace(['{category}', '{page}'], [$this->categoryName, $page ? ' - Page '.$page : ''], $item['description']);
        $keywords = str_replace(['{category}', '{page}'], [$this->categoryName, $page ? 'Page '.$page.', ' : ''], $item['keywords']);
        return $this->render('ceo.twig', ['title' => $title, 'description' => $description, 'keywords' => $keywords, 'this' => $this]);
    }

    /**
     * Generate canonical for code base
     * @return string
     */
    public function getCanonical()
    {
        return Url::current([], true);
    }
}