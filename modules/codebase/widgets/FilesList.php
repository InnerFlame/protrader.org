<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 * Date: 19.04.2016
 * Time: 13:43
 */

namespace app\modules\codebase\widgets;

use app\modules\codebase\models\File;
use yii\base\Widget;

class FilesList extends Widget
{

    public $description_id;

    public $description;

    public function run()
    {
        $models = File::findAll('description_id = :cat_id', [':cat_id' => $this->description_id]);
        return $this->render('files-list.twig', ['this' => $this, 'models' => $models]);
    }
}