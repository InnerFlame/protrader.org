<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 * Date: 11.04.2016
 * Time: 10:34
 */

namespace app\modules\codebase\widgets;


use app\components\Entity;
use app\modules\codebase\models\Category;
use yii\base\Widget;

class CategoriesList extends Widget
{
    public $models = [];

    public function run()
    {
        $this->models = Category::find()->with('descriptions')->where(['deleted' => Entity::NOT_DELETED])->all();
        return $this->render('categories-list.twig', ['models' => $this->models, 'this' => $this]);
    }

    /**
     * Check active this category in url
     * @param Category $model
     * @return bool
     */
    public function isActiveModel(Category $model)
    {
        if($this->isMainPage())
            return false;

        return $_GET['alias'] == $model->alias;
    }

    public function isMainPage()
    {
        return !isset($_GET['alias']);
    }

    /**
     * Count all items by array categories count items
     * @return int
     */
    public function getCountAllItems()
    {
        $count = 0;
        foreach($this->models as $model){
            $count += count($model->descriptions);
        }
        return $count;
    }
}