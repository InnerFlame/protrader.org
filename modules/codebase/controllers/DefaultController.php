<?php

namespace app\modules\codebase\controllers;

use app\components\Entity;
use app\modules\codebase\assets\AssetBundle;
use app\modules\codebase\components\Controller;
use app\modules\codebase\models\Category;
use app\modules\codebase\models\Description;
use app\modules\codebase\models\File;
use yii\base\ErrorException;
use yii\bootstrap\ActiveForm;
use yii\data\ArrayDataProvider;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class DefaultController extends Controller
{
    public function actions()
    {
        return [
            'image-upload' => [
                'class' => 'app\components\ImageUploadAction',
                'width' => 650,
                'url'   => '/upload/codebase',
                'path'  => \Yii::getAlias('@app') . '/web/upload/codebase' // Or absolute path to directory where files are stored.
            ],
        ];
    }

    /**
     * @param null $alias
     * @return array|string|Response
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        $model = new Description();
        $model->scenario = 'insert';
        if($model->isNewRecord){
            $model->created_at = time();
        }

        if (\Yii::$app->request->isAjax && $model->load(\Yii::$app->request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if (\Yii::$app->request->isPost && $model->load(\Yii::$app->request->post())) {
            $model->user_id = \Yii::$app->user->id;
            $model->status = Description::STATUS_PENDING;
            $model->created_at = time();
            if (!$model->save()) {
                \Yii::error('Can\'t save ' . get_class($model));
            }
            return $this->redirect($model->getViewUrl());
        }

        $query = Description::find()->with('user', 'category', 'comments')->where('deleted = :deleted', [':deleted' => Entity::NOT_DELETED])->orderBy('created_at DESC')->enabled();


        $dataProvider = new ArrayDataProvider([
            'allModels' => $query->all(),
            'pagination' => [
                'defaultPageSize' => 10,
            ],
        ]);

        return $this->render('index.twig', [
            'dataProvider' => $dataProvider,
            'model'        => $model,
            'category'     => isset($category) ? $category->title : 'default',
            'categories'   => Category::getListCategories()
        ]);
    }

    public function actionCategory($alias)
    {
        $model = new Description();
        $model->scenario = 'insert';
        if($model->isNewRecord){
            $model->created_at = time();
        }

        if (\Yii::$app->request->isAjax && $model->load(\Yii::$app->request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        $query = Description::find()->with('user', 'category', 'comments')->where('deleted = :deleted', [':deleted' => Entity::NOT_DELETED])->orderBy('created_at DESC')->enabled();
        if ($alias) {
            $category = Category::find()->where('alias = :alias AND deleted = 0', [':alias' => $alias])->one();
            if ($category) {
                $query->andWhere('category_id = :cat_id', [':cat_id' => $category->id]);
            } else {
                throw new NotFoundHttpException;
            }
        }
        $dataProvider = new ArrayDataProvider([
            'allModels' => $query->all(),
            'pagination' => [
                'defaultPageSize' => 10,
            ],
        ]);

        return $this->render('index.twig', [
            'dataProvider' => $dataProvider,
            'model'        => $model,
            'category'     => isset($category) ? $category->title : 'default',
            'categories'   => Category::getListCategories()
        ]);
    }

    /**
     * View
     * @param $alias
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($alias)
    {
        $model = Description::find()->where('alias = :alias AND deleted = :deleted', [':deleted' => Entity::NOT_DELETED, ':alias' => $alias])->one();

        $this->canShow($model);

        $model->scenario = 'update';

        if (\Yii::$app->request->isAjax && $model->load(\Yii::$app->request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if (\Yii::$app->request->isPost && $model->load(\Yii::$app->request->post())) {

            if ($model->save()) {
                \Yii::$app->session->setFlash('success', 'Updated');
            }

            if ($model->deleteFiles(json_decode(\Yii::$app->request->post('selected')))) {
                \Yii::$app->session->setFlash('success', 'Updated');
            }

            return $this->redirect($model->getViewUrl());
        }

        $this->breadcrumbs[] = [
            'label' => \Yii::t('app', 'Code Base'),
            'url'   => Url::to('/codebase')
        ];

        $this->breadcrumbs[] = [
            'label' => \Yii::t('app', $model->category->getName()),
            'url'   => $model->category->getViewUrl()
        ];
        $model->updateCounters(['views' => 1]);
        return $this->render('view.twig', [
            'model'      => $model,
            'categories' => Category::getListCategories()
        ]);
    }

    /**
     * Render script file for user in
     * popup window
     * @param $id
     * @return string
     * @throws HttpException
     * @throws NotFoundHttpException
     */
    public function actionFile($id)
    {
        $model = File::findOne($id);

        if (!is_object($model)) {
            throw new HttpException(404);
        }

        $filepath = \Yii::getAlias('@data_codebase/') . $model->description->id . '/' . $model->name;

        if (!$model || !file_exists($filepath))
            throw new NotFoundHttpException;

        $mime = FileHelper::getMimeType($filepath);

        if (!in_array($mime, File::$allow_mime)) {
            throw new NotFoundHttpException;
        }


        AssetBundle::register($this->getView());

        $content = file_get_contents($filepath);

        return $this->renderAjax('viewscript.twig', ['model' => $model, 'content' => $content]);
    }

    /**
     * Package files to zip archive and output
     *
     * @param $id
     * @throws ErrorException
     * @throws NotFoundHttpException
     */
    public function actionDownload($id)
    {
        $description = Description::find()->where('id = :id AND deleted = 0', [':id' => $id])->one();
        $zipfile = $description->getPathToZip();

        if (!$description)
            throw new NotFoundHttpException;

        if ($description->files <= 0 || !file_exists($zipfile))
            throw new NotFoundHttpException;

        $description->updateCounters(['count_download' => 1]);

        header('Content-Type: application/zip');
        header('Content-disposition: attachment; filename=' . 'sources.zip');
        header('Content-Length: ' . filesize($zipfile));
        readfile($zipfile);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws HttpException
     * @throws \Exception
     */
    public function actionDelete($id)
    {
        $model = Description::findOne($id);

        if (\Yii::$app->user->isGuest || ($model->user_id != \Yii::$app->user->getId() && !\Yii::$app->user->identity->isAdmin())) {
            \Yii::$app->session->setFlash('success', \Yii::t('app', 'You can\'t delete not your code'));
            return $this->redirect($model->getViewUrl());
        }

        if (!$model) {
            throw new HttpException(404);
        }

        $model->delete();

        return $this->redirect('/codebase');
    }

    /**
     * TODO need refactoring
     * @param $model
     * @return bool
     * @throws NotFoundHttpException
     */
    public function canShow($model)
    {
        if (!$model) {
            throw new NotFoundHttpException();
        }

        if($model->status == Description::STATUS_ENABLE){
            return true;
        } else {
            if (!\Yii::$app->user->isGuest) {
                if (\Yii::$app->user->identity->isAdmin()) {
                    return true;
                }
                if ($model->user_id == \Yii::$app->user->identity->getId()) {
                    return true;
                }
            }
            throw new NotFoundHttpException();
        }
    }
}
