<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 * Date: 21.04.2016
 * Time: 10:56
 */

namespace app\modules\codebase\controllers;


use app\modules\codebase\components\Controller;
use app\modules\codebase\models\File;
use yii\web\NotFoundHttpException;

class FileController extends Controller
{

    /**
     * Download one file from collection
     * @param $id
     * @throws NotFoundHttpException
     */
    public function actionDownload($id)
    {
        $model = File::findOne($id);
        $file = $model->description->getPathToFile($model->name);
        if(!$model || !file_exists($file)){
            throw new NotFoundHttpException;
        }

        header('Content-Type: '.$model->type);
        header('Content-disposition: attachment; filename=' . $model->name);
        header('Content-Length: ' . filesize($file));
        readfile($file);
    }
}