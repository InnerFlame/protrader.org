<?php

namespace app\modules\codebase\models;

use app\components\Entity;
use himiklab\sitemap\behaviors\SitemapBehavior;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "cdbs_categories".
 *
 * @property integer $id
 * @property string $title
 * @property string $alias
 * @property string $icon
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted
 *
 * @property Description[] $descriptions
 */
class Category extends Entity
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cdbs_categories';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class'       => SitemapBehavior::className(),
                'scope'       => function ($model) {
                    /** @var \yii\db\ActiveQuery $model */
                    $model->select(['id', 'alias', 'updated_at', 'deleted']);
                    $model->andWhere(['deleted' => 0]);
                    $model->orderBy(['updated_at' => SORT_DESC]);
                },
                'dataClosure' => function ($model) {
                    /** @var self $model */
                    return [
                        'loc'        => Url::toRoute([$model->getViewUrl()]),
                        'lastmod'    => date('Y-m-d\TH:i:sP', $model->updated_at),
                        'changefreq' => SitemapBehavior::CHANGEFREQ_WEEKLY,
                        'priority'   => 0.2
                    ];
                }
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['created_at', 'updated_at', 'deleted'], 'integer'],
            [['title', 'alias'], 'string', 'max' => 255],
            [['created_at', 'updated_at'], 'default', 'value' => time()],
            [['icon'], 'string', 'max' => 32]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('app', 'ID'),
            'title'      => Yii::t('app', 'Title'),
            'alias'      => Yii::t('app', 'Alias'),
            'icon'       => Yii::t('app', 'Icon'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted'    => Yii::t('app', 'Deleted'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDescriptions()
    {
        return $this->hasMany(Description::className(), ['category_id' => 'id'])->andWhere(['deleted' => Entity::NOT_DELETED])->enabled();
    }

    /**
     * @param bool $catalog
     * @return string
     */
    public function getIconUrl($catalog = false)
    {
        if (!$catalog) {
            return '/upload/codebase/category/' . $this->icon;
        }
        return '/upload/codebase/category/' . $catalog . '/' . $this->icon;
    }

    /**
     * @inheritdoc
     * @return CategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CategoryQuery(get_called_class());
    }

    /**
     *
     */
    public function getTypeEntity()
    {
        return Entity::CODEBASE_CATEGORY;
    }

    public function getCssIcon()
    {
        if($this->title == 'Plug-Ins' || $this->title == 'Macroses')
            return "cat2";

        if($this->title == 'Indicators')
            return 'cat4';

        if($this->title == 'Strategies')
            return 'cat1';

        if($this->title == 'Libraries')
            return 'cat3';

        return 'cat1';
    }

    /**
     *
     */
    public function getName()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getViewUrl()
    {
        return '/codebase/' . $this->alias;
    }

    /**
     * @return array
     */
    public static function getCategoryList()
    {
        return ArrayHelper::map(self::find()->notDeleted()->all(), 'id', 'title');
    }

    /**
     * @return array
     */
    public static function getListCategories()
    {
        $models = Category::find()->where(['deleted' => Entity::NOT_DELETED])->all();
        return ArrayHelper::map($models, 'id', 'title');
    }

    /**
     * @return array
     */
    public static function getGridColumns()
    {
        return [
            [
                'class'          => 'yii\grid\SerialColumn',
                'header'         => '<span>#</span>',
                'contentOptions' => [
                    'width' => 50
                ],
            ],
            [
                'attribute' => 'title',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'alias',
                'format'    => 'raw',
            ],

            [
                'class'    => 'app\components\grid\ActionColumnCustom',
                'template' => function ($model) {
                    return '{edit} {delete}';
                },
                'buttons'  => [
                    'edit'   => function ($url, $model, $key, $options) {
                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/codebase-category-edit', 'id' => $model->id]);
                        return Html::a('Edit', $url, $options);
                    },
                    'delete' => function ($url, $model, $key, $options) {
                        $options['onclick'] = '
                            if(confirm(\'' . Yii::t('yii', 'Are you sure you want to delete this Codebase category?') . '\'))
                            {
                              return true;
                            } else {
                              return false;
                            }
                        ';
//                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/codebase-category-delete', 'id' => $model->id]);
                        return Html::a('Delete', $url, $options);
                    },

                ],
            ],
        ];
    }
}
