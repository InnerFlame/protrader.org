<?php

namespace app\modules\codebase\models;

use app\components\ceo\Ceo;
use app\components\ceo\CeoBehavior;
use app\components\Entity;
use app\components\helpers\StringHelper;
use app\components\langInput\LangInputBehavior;
use app\components\validators\EmptyContent;
use app\components\validators\MaxLength;
use app\models\community\Comments;
use app\models\user\User;
use app\modules\codebase\components\FilesUploadBehavior;
use app\modules\codebase\events\CodebaseNotify;
use app\modules\codebase\Module;
use app\modules\features\events\StatusChangedEvent;
use app\modules\notify\components\Event;
use app\modules\notify\components\EventFree;
use himiklab\sitemap\behaviors\SitemapBehavior;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\console\Application;
use yii\helpers\Url;
use yii\db\ActiveRecord;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "cdbs_description".
 *
 * @property integer $id
 * @property integer $_id
 * @property string $title
 * @property string $alias
 * @property string $content
 * @property integer $views
 * @property integer $user_id
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted
 *
 * @property Category $category
 * @property User $user
 * @property File[] $cdbsFiles
 */
class Description extends Entity
{

    const STATUS_PENDING = 0;
    const STATUS_ENABLE = 1;
    const STATUS_HIDDEN = 2;


    const EVENT_CHANGED_STATUS = 'codebase.description.changed.status';
    /**
     * @var UploadedFile|Null file attribute
     */
    public $file;
    public $form;

    /**
     * @var
     */
    public $sources;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cdbs_description';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
//            [
//                'class'      => LangInputBehavior::className(),
//                'attributes' => ['content', 'title'],
//            ],
            [
                'class' => CeoBehavior::className(),
            ],
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'slugAttribute' => 'alias',
                'immutable' => false,
                'ensureUnique' => true,
            ],
            [
                'class' => FilesUploadBehavior::className(),
                'attribute_files' => 'file',
                'attribute_id' => 'id',
                'upload_category' => \Yii::getAlias('@app/data/codebase/')
            ],
            [
                'class' => SitemapBehavior::className(),
                'scope' => function ($model) {
                    /** @var \yii\db\ActiveQuery $model */
                    $model->select(['alias', 'updated_at', 'id', 'category_id']);
                },
                'dataClosure' => function ($model) {
                    /** @var self $model */
                    return [
                        'loc' => Url::toRoute([$model->getViewUrl()]),
                        'lastmod' => date('Y-m-d\TH:i:sP', $model->updated_at),
                        'changefreq' => SitemapBehavior::CHANGEFREQ_WEEKLY,
                        'priority' => 0.2
                    ];
                }
            ],
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);


        if ($this->scenario != Entity::SCENARIO_DELETE && $insert) {
            $eventName = $insert ? Module::EVENT_CODEBASE_ADD : Module::EVENT_CODEBASE_EDIT;

            if ($this->status == Description::STATUS_PENDING) {
                $eventName = Module::EVENT_CODEBASE_PENDING;
            }
            $notify = new CodebaseNotify(['model_id' => $this->id]);
            $notify->subscribed = false;
            $event = new Event(['sender' => $notify]);
            Yii::$app->trigger($eventName, $event);
        }

        if (isset($changedAttributes['status'])) {
            $this->trigger(self::EVENT_CHANGED_STATUS, new \app\modules\codebase\events\StatusChangedEvent([
                'oldStatus' => $changedAttributes['status'],
                'newStatus' => $this->status
            ]));
        }
        return true;
    }


    /**
     * @return array
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['insert'] = ['title', 'content', 'user_id', 'category_id', 'file'];
        $scenarios['update'] = ['title', 'content', 'user_id', 'category_id', 'status', 'file'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'content', 'user_id', 'category_id', 'status'], 'required'],
            [['category_id', 'views', 'user_id', 'status', 'updated_at', 'deleted'], 'integer'],
            [['created_at', 'updated_at'], 'default', 'value' => time()],
            [['content'], 'string'],
            [['content'], MaxLength::className(), 'count' => 15000],
            [['content'], EmptyContent::className()],
            [['file'], 'file', 'maxFiles' => 10, 'maxSize' => 1024 * 1024 * 30, 'skipOnEmpty' => true, 'extensions' => 'cs, dll, zip'],
            ['file', 'required', 'on' => 'insert'],
            ['file', 'validateFile', 'on' => 'update'],
            [['title'], 'string', 'min' => 3, 'max' => 60],
            [['created_at'], 'safe'],
        ];
    }

    public function validateFile($attribute, $params)
    {
        $selecteds = json_decode(\Yii::$app->request->post('selected'));
        $uploadeds = json_decode(\Yii::$app->request->post('uploaded'));

        # проверка на уникальность загружаемых файлов
        if (count($uploadeds) > 0) {

            foreach ($uploadeds as $file) {
                $unique = File::find()->where([
                    'description_id' => $this->id,
                    'name' => $file,
                ]);

                if (count($selecteds) > 0) {
                    $unique = $unique->andWhere(['not in', 'id', $selecteds]);
                }

                $unique = $unique->notDeleted()->one();

                if (is_object($unique)) {
                    $this->addError($attribute, Yii::t('app', 'File with name \'' . $file . ' \' already exists.'));
                    return false;
                }
            }
            return true;
        }

        if (count($selecteds) == 0 && count($this->file) > 0) {
            return true;
        }

        $files = File::find()->where(['description_id' => $this->id]);

        if ($selecteds) {
            $files = $files->andWhere(['not in', 'id', $selecteds]);
        }

        $files_count = $files->notDeleted()->count();


        if ((int)$files_count <= 0) {
            $this->addError($attribute, Yii::t('app', 'File cannot be blank.'));
            return false;
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category_id' => Yii::t('app', 'Category ID'),
            'title' => Yii::t('app', 'Title'),
            'alias' => Yii::t('app', 'Alias'),
            'content' => Yii::t('app', 'Content'),
            'views' => Yii::t('app', 'Views'),
            'user_id' => Yii::t('app', 'User ID'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted' => Yii::t('app', 'Deleted'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return $this
     */
    public function getComments()
    {
        return $this->hasMany(Comments::className(), ['entity_id' => 'id'])->where([
            'entity' => $this->getTypeEntity()
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(File::className(), ['description_id' => 'id'])->andWhere(['!=', 'deleted', '1']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCeo()
    {
        return $this->hasOne(Ceo::className(), ['entity_id' => 'id'])->where([
            'entity' => $this->getTypeEntity()
        ]);
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return StringHelper::truncateAsWord($this->content, 150);
    }

    /**
     * @inheritdoc
     * @return DescriptionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DescriptionQuery(get_called_class());
    }

    /**
     * @return int
     */
    public function getTypeEntity()
    {
        return Entity::CODEBASE_DESCRIPTION;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->title;
    }


    /**
     * @return string
     */
    public function getViewUrl()
    {
        if (Yii::$app instanceof Application) {
            return sprintf('/codebase/%s/%s', $this->category->alias, $this->alias);
        }
        return Yii::$app->urlManager->createUrl(['codebase/default/view', 'category' => $this->category->alias, 'alias' => $this->alias]);
    }


    /**
     * @return int
     */
    public function getCountComments()
    {
        return count($this->comments);
    }

    public function updateTime()
    {
        $this->updated_at = time();
        $this->save();
    }

    /**
     * Редактируется ли эта статья или нет
     * Редактировать могут: автор, или пользователи
     * с правами администора
     */
    public function isRedacting()
    {
        if (Yii::$app->user->isGuest) {
            return false;
        }
        return $this->user_id == Yii::$app->user->id || Yii::$app->user->identity->isAdmin();
    }

    /**
     * @param array $ids
     * @return bool
     */
    public function deleteFiles($ids = [])
    {
        if (count($ids) > 0) {
            foreach ($ids as $id) {
                $model = File::find()->where(['id' => $id])->andWhere(['description_id' => $this->id])->one();
                if (is_object($model)) {
                    $model->delete();
                }
            }
            $this->refreshZipFile();
            return true;
        }
        return false;
    }

    public function isActive()
    {
        if ($this->status == self::STATUS_ENABLE) {
            return true;
        }

        if ($this->status == self::STATUS_HIDDEN) {
            return false;
        }

        if ($this->status == self::STATUS_PENDING) {

            if (!Yii::$app->user->isGuest) {

                if ($this->user_id == Yii::$app->user->identity->id) {
                    return true;
                }
                if (Yii::$app->user->identity->isAdmin()) {
                    return true;
                }
            }

            if ($this->user->isAdmin()) {
                return true;
            }

            return false;
        }
    }

    /**
     * @return array
     */
    public static function getStatusList()
    {
        return [
            self::STATUS_PENDING => 'Pending',
            self::STATUS_ENABLE => 'Enable',
            self::STATUS_HIDDEN => 'Hidden',
        ];
    }

    public function getStatusCode()
    {
        if ($this->status == self::STATUS_PENDING) {
            return 'Pending';
        }

        if ($this->status == self::STATUS_ENABLE) {
            return 'Enable';
        }

        if ($this->status == self::STATUS_HIDDEN) {
            return 'Hidden';
        }
    }

}
        