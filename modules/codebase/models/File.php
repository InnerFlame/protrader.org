<?php

namespace app\modules\codebase\models;

use app\components\Entity;
use app\models\trash\Trash;
use Yii;

/**
 * This is the model class for table "cdbs_files".
 *
 * @property integer $id
 * @property integer $description_id
 * @property string $name
 * @property string $type
 * @property string $size
 * @property integer $deleted
 *
 * @property Description $cdbsDescription
 */
class File extends \app\components\Entity
{

    const TYPE_SIZE_KB = 'KB';

    const TYPE_SIZE_MB = 'MB';

    public static $allow_mime = [
        'text/plain',
        'application/octet-stream',
        'inode/x-empty'
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cdbs_files';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description_id', 'name', 'type', 'size'], 'required'],
            [['description_id', 'size', 'deleted'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'             => Yii::t('app', 'ID'),
            'description_id' => Yii::t('app', 'Description ID'),
            'name'           => Yii::t('app', 'Name'),
            'size'           => Yii::t('app', 'Size'),
            'deleted'        => Yii::t('app', 'Deleted'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDescription()
    {
        return $this->hasOne(Description::className(), ['id' => 'description_id']);
    }

    /**
     * @inheritdoc
     * @return FileQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FileQuery(get_called_class());
    }

    /**
     * @return int
     */
    public function getTypeEntity()
    {
        return Entity::CODEBASE_FILE;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function getViewUrl()
    {
        // TODO: Implement getViewUrl() method.
    }

    /**
     * Get size file in type size: KB, MB
     * @return float
     */
    public function getSize($type = self::TYPE_SIZE_KB)
    {
        if($type == self::TYPE_SIZE_KB){
            return round($this->size/1024, 2);
        }
        if($type == self::TYPE_SIZE_MB){
            return round(($this->size/1024)/1024, 2);
        }
    }
}
