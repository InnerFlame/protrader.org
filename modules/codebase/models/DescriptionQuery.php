<?php

namespace app\modules\codebase\models;

use app\components\Entity;

/**
 * This is the ActiveQuery class for [[Description]].
 *
 * @see Description
 */
class DescriptionQuery extends \yii\db\ActiveQuery
{
    /**
     * @inheritdoc
     * @return Description[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Description|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * Формирует запрос на получение включенных записей.
     * Для админа или модератора, все записи считаются включенными
     * независимо от их статуса. Для остальных пользователей, это
     * записи у которых статус включены или они являются владельцами
     * @return $this
     */
    public function enabled()
    {
        if(\Yii::$app->user->identity && \Yii::$app->user->identity->isAdmin()){
            return $this;
        }
        $query = $this->andWhere(['status' => Description::STATUS_ENABLE]);
        if(!\Yii::$app->user->isGuest){
            $query->orWhere(['user_id' => \Yii::$app->user->getId()])->notDeleted();
        }

        return $query;

    }

    /**
     * @return $this
     */
    public function notDeleted()
    {
        return $this->andWhere(['deleted' => Entity::NOT_DELETED]);
    }
}