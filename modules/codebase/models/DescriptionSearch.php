<?php

namespace app\modules\codebase\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * DescriptionSearch represents the model behind the search form about `app\modules\codebase\models\Description`.
 */
class DescriptionSearch extends Description
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'views', 'count_download', 'user_id', 'created_at', 'updated_at', 'deleted', 'status'], 'integer'],
            [['title', 'alias', 'content'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Description::find()->notDeleted();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'id'             => $this->id,
            'category_id'    => $this->category_id,
            'views'          => $this->views,
            'count_download' => $this->count_download,
            'user_id'        => $this->user_id,
            'created_at'     => $this->created_at,
            'updated_at'     => $this->updated_at,
            'deleted'        => $this->deleted,
            'status'         => $this->status,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'alias', $this->alias])
            ->andFilterWhere(['like', 'content', $this->content]);

        return $dataProvider;
    }
}
