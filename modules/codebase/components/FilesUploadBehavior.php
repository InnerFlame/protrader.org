<?php
/**
 * Created by PhpStorm.
 * User: Serg
 * Date: 19.04.2016
 * Time: 10:09
 */

namespace app\modules\codebase\components;

use app\components\Entity;
use app\modules\codebase\models\Description;
use app\modules\codebase\models\File;
use vova07\imperavi\helpers\FileHelper;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\web\UploadedFile;

class FilesUploadBehavior extends Behavior
{
    public $attribute_files;

    public $attribute_id;

    public $upload_category;

    private $_transaction;

    /**
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
            ActiveRecord::EVENT_AFTER_VALIDATE => 'afterValidate',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterUpdate',
        ];
    }

    /**
     * Открываем транзакцию что бы в случае когда файлы не загрузились
     * или не сохранились в базу, откатить описание кода.
     * @see Description
     */
    public function afterValidate()
    {
        $this->_transaction = \Yii::$app->getDb()->beginTransaction();
    }

    /**
     * @param $event
     * @return bool
     */
    public function afterInsert($event)
    {
        if(!$this->uploadFiles()){
            $this->_transaction->rollBack();
            return false;
        }
        return $this->_transaction->commit();
    }

    /**
     * @param $event
     * @return bool
     */
    public function afterUpdate($event)
    {
        if($this->owner->scenario == Entity::SCENARIO_DELETE){
            return true;
        }
        if(!$this->uploadFiles()){
            $this->_transaction->rollBack();
            return false;
        }
        return $this->_transaction->commit();
    }

    /**
     * Return path to zip file sources code
     * @return string
     */
    public function getPathToZip()
    {
        return $this->getCategoryPath() . DIRECTORY_SEPARATOR . 'sources.zip';
    }

    /**
     * Return size zip archive in KB
     * @return int
     */
    public function getSizeZipFile()
    {
        if(file_exists($this->getPathToZip())) {
            return round(filesize($this->getPathToZip()) / 1024, 2);
        }
        return 0;
    }
    /**
     *
     * @param $filename
     * @return string
     */
    public function getPathToFile($filename)
    {
        return $this->getCategoryPath() . DIRECTORY_SEPARATOR . $filename;
    }

    /**
     * Uploaded files to category
     * @return bool
     */
    protected function uploadFiles()
    {
        $files = UploadedFile::getInstances($this->owner, $this->attribute_files);
        if(count($files) <= 0)
            return true;
        foreach($files as $file){
            try{
                $name = $file->baseName.'.'.$file->extension;
                $newFile = $this->getPathToFile($name);
                if($file->saveAs($newFile)){
                    $model = new File();
                    $model->description_id = $this->owner->{$this->attribute_id};
                    $model->name = $name;
                    $model->size = $file->size;
                    $model->type = $file->type;
                    if(!$model->save()){
                        throw new \Exception('Не удалось сохранить файл в базу данных: ' . Json::encode($model->getErrors()));
                    }

                    # when file is uploaded we must updated time()
                    $descr = Description::findOne($model->description_id);
                    $descr->scenario = Entity::SCENARIO_DELETE;
                    $descr->updated_at = time();
                    $descr->save(false);

                    continue;
                }
                return false;
            }catch (\Exception $e){
                \Yii::error($e->getMessage());
                return false;
            }
        }
        $this->refreshZipFile();
        return true;
    }

    /**
     * Refreshed zip file without all uploaded
     * files
     * @return bool
     */
    public function refreshZipFile()
    {
        $files = $this->owner->files;
        if (count($files) <= 0) {
            @unlink($this->getPathToZip());
            return true;
        }
        $zip = new \ZipArchive();
        $zip->open($this->getPathToZip(), \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
        foreach ($files as $file) {
            $zip->addFile($this->getPathToFile($file->name), $file->name);
        }
        $zip->close();
    }

    /**
     * @return string
     * @throws \yii\base\Exception
     */
    protected function getCategoryPath()
    {
        $path = $this->upload_category . $this->owner->{$this->attribute_id};
        if(!file_exists($path)){
            FileHelper::createDirectory($path);
        }
        return $this->upload_category . $this->owner->{$this->attribute_id};
    }
}

