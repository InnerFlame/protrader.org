<?php

use app\modules\cabinet\modules\notify\models\NotifyTemplate;
use \app\modules\codebase\Module;

return [

    Module::EVENT_CODEBASE_PENDING => [
        'label'     => 'New codebase',
        'templates' => [
            NotifyTemplate::TYPE_EMAIL => [
                'title' => 'New Code on premoderation',
                'body' => '{title}'
            ],
            NotifyTemplate::TYPE_FULL  => [
                'title' => 'New Code on premoderation',
                'body' => 'codebase was added by {username}'
            ],
            NotifyTemplate::TYPE_SHORT => [
                'title' => 'New Code on premoderation',
                'body' => '{title}'
            ],
            NotifyTemplate::TYPE_GROUP => [
                'title' => '{count} new codebase(s) was(ware) add',
                'body' => '{title}'
            ]
        ]
    ],
];