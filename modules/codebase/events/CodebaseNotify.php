<?php

namespace app\modules\codebase\events;

use app\modules\codebase\models\Description;
use app\modules\notify\components\Notification;

class CodebaseNotify extends Notification
{
    public $model_id;

    private $_model;

    public function getTitle()
    {
        return $this->getEntity()->title;
    }

    public function getDescription()
    {
        return $this->getEntity()->content;
    }

    public function getAuthor()
    {
        return $this->getEntity()->user->getFullName();
    }

    /**
     * @return Description
     */
    public function getEntity()
    {
        if (!$this->_model) {
            $this->_model = Description::findOne($this->model_id);
        }
        return $this->_model;
    }
}