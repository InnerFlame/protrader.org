<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2016
 */

namespace app\modules\codebase\events;


use yii\base\Event;

class StatusChangedEvent extends Event
{
    public $oldStatus;

    public $newStatus;

}