<?php

namespace app\modules\codebase\assets;


class AssetBundle extends \yii\web\AssetBundle
{
    public $sourcePath = '@app/modules/codebase/assets';

    public $js = [
        'google-code-prettify/prettify.js'
    ];

    public $css = [
        'google-code-prettify/prettify.css'
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}