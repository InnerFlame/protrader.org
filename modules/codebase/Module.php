<?php

namespace app\modules\codebase;

use app\components\Application;
use app\components\helpers\ArrayHelper;
use app\models\community\Comments;
use app\models\user\User;
use app\modules\codebase\assets\AssetBundle;
use app\modules\codebase\events\CodebaseNotify;
use app\modules\codebase\models\Description;
use app\modules\codebase\events\StatusChangedEvent;
use yii\base\BootstrapInterface;
use yii\base\Event;
use yii\db\ActiveRecord;
use yii\db\AfterSaveEvent;
use yii\helpers\VarDumper;

class Module extends \yii\base\Module implements BootstrapInterface
{
    const EVENT_CODEBASE_ADD = 'codebase.topic.add';

    const EVENT_CODEBASE_EDIT = 'codebase.topic.edit';

    const EVENT_CODEBASE_PENDING = 'codebase.topic.pending';


    public $controllerNamespace = 'app\modules\codebase\controllers';

    public function init()
    {
        \Yii::$app->setAliases([
            'codebase' => '@app/modules/codebase'
        ]);
        parent::init();
    }

    /**
     * @param \yii\webApplication $app
     */
    public function bootstrap($app)
    {
        \Yii::setAlias('@data_codebase', '@app/data/codebase');
        \Yii::$app->urlManager->addRules([
                'codebase/delete-file/<id>'   => 'codebase/default/delete-file',
                'codebase/delete/<id>'   => 'codebase/default/delete',
                'codebase/file/<id>'          => 'codebase/default/file',
                'codebase/image-upload'       => 'codebase/default/image-upload',
                'codebase/download/<id>'      => 'codebase/default/download',
                'codebase/download-file/<id>' => 'codebase/file/download',
                'codebase/<category>/<alias>' => 'codebase/default/view',
                'codebase'            => 'codebase/default/index',
                'codebase/<alias>'            => 'codebase/default/category',

            ]
        );

        //Подписать на события, пользователя, если он создал новый комментарий
        //или новый топик
        if($app instanceof Application){
            if($app->getModule('notify')){
                Event::on(Description::className(), ActiveRecord::EVENT_AFTER_INSERT, function(AfterSaveEvent $event)use($app){
                    $app->getModule('notify')->subscriber->subscribe($event->sender, \Yii::$app->user->identity->id);
                });
                Event::on(Comments::className(), ActiveRecord::EVENT_AFTER_INSERT, function(AfterSaveEvent $event)use($app){
                    $app->getModule('notify')->subscriber->subscribe($event->sender, \Yii::$app->user->identity->getId());

                    //Если добавлен новый комментарий, то автоматически подписать на сущность самого топика
                    $app->getModule('notify')->subscriber->subscribe($event->sender->getEntity(), \Yii::$app->user->identity->getId());
                });
            }
        }

        /**
         * Если был изменен статус статьи с pending
         * То необходимо сгенерировать событие добавления новой статьи
         * Модераторы, при этом уже не получают уведомления, так как
         * они видели эту статью когда она папала на премодерацию
         */
        Event::on(Description::className(), Description::EVENT_CHANGED_STATUS, function(StatusChangedEvent $event){
            if($event->sender instanceof Description){
                $notify = new CodebaseNotify(['model_id' => $event->sender->id]);
                $notify->subscribed = false;
                $notify->user_id = $event->sender->user_id;

                //Исключить модераторов так как они ведели эту статью перед модерацией
                $users = User::find()->where(['in', 'role', [User::ROLE_ADMIN, User::ROLE_MODERATOR]])->all();
                $notify->addIgnoredUsers(ArrayHelper::getColumn($users, 'id'));

                $event = new \app\modules\notify\components\Event(['sender' => $notify]);
                \Yii::$app->trigger(self::EVENT_CODEBASE_ADD, $event);
            }
        });
    }
}
