<?php
namespace components;

use app\modules\forum\models\FrmTopic;

class EntityTest extends \PHPUnit_Framework_TestCase
{
    protected function setUp()
    {
    }

    protected function tearDown()
    {
    }

    // tests
    public function testSave()
    {
        $entity = FrmTopic::findOne(185);
        $entity->content = $entity->content.'<br/>';
        $this->assertTrue($entity->save());
    }
}
