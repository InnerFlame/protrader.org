<?php

namespace app\models\main;

use app\components\Entity;
use Yii;
use yii\helpers\Html;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\SluggableBehavior;
use app\models\CustomModel;
use yii\helpers\Url;

/**
 * This is the model class for table "main_faq_category".
 *
 * @property integer $id
 * @property string $title
 * @property string $alias
 * @property string $data
 * @property integer $weight
 * @property integer $published_at
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property MainFaq[] $mainFaqs
 */
class FaqCategory extends Entity
{
    /*
    |--------------------------------------------------------------------------
    | Constants && properties
    |--------------------------------------------------------------------------
    */

    public static $_pull = [];

    /*
    |--------------------------------------------------------------------------
    | Model configurations
    |--------------------------------------------------------------------------
    */

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'main_faq_category';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class'         => SluggableBehavior::className(),
                'attribute'     => 'title',
                'slugAttribute' => 'alias',
                'immutable'     => 'true',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'weight'], 'required'],
            [['id', 'weight', 'created_at', 'updated_at'], 'integer'],
            [['title'], 'string', 'max' => 120],
            [['alias', 'data'], 'string', 'max' => 256],
            [['parent_id', 'published_at'], 'safe'],
        ];
    }

    /**wqe
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'           => 'ID',
            'parent_id'    => 'Parent ID',
            'title'        => 'Title',
            'alias'        => 'Alias',
            'data'         => 'Data',
            'weight'       => 'Weight',
            'published_at' => 'Published At',
            'created_at'   => 'Created At',
            'updated_at'   => 'Updated At',
        ];
    }

    /**
     * @inheritdoc
     * @return PagesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FaqCategoryQuery(get_called_class());
    }

    /*
    |--------------------------------------------------------------------------
    | Model relations
    |--------------------------------------------------------------------------
    */

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFaqs()
    {
        return Faq::find()->where(['category_id' => $this->id])->andWhere(['deleted' => Entity::NOT_DELETED])->orderBy('weight DESC')->all();
    }

    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */

    public static function getGridColumns()
    {
        return [
            [
                'class'          => 'yii\grid\SerialColumn',
                'header'         => '<span>#</span>',
                'contentOptions' => [
                    'width' => 50
                ],
            ],
//            [
//                'attribute' => 'parent_id',
//                'format'    => 'raw',
//            ],

            [
                'attribute' => 'title',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'alias',
                'format'    => 'raw',
            ],
//            [
//                'attribute' => 'data',
//                'format'    => 'raw',
//            ],
            [
                'attribute' => 'weight',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'published_at',
                'format'    => 'date',
            ],
            [
                'class'    => 'app\components\grid\ActionColumnCustom',
                'template' => function ($model) {
                    return '{edit} {delete}';
                },
                'buttons'  => [
                    'edit'   => function ($url, $model, $key, $options) {
                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/faq-category/edit', 'id' => $model->id]);
                        return Html::a('Edit', $url, $options);
                    },
                    'delete' => function ($url, $model, $key, $options) {
                        $options['onclick'] = '
                            if(confirm(\'' . Yii::t('yii', 'Are you sure you want to delete this Page?') . '\'))
                            {
                              return true;
                            } else {
                              return false;
                            }
                        ';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/faq-category/delete', 'id' => $model->id]);
                        return Html::a('Delete', $url, $options);
                    },

                ],
            ],
        ];
    }

    public function getTypeEntity()
    {
        return Entity::FAQ_CATEGORY;
    }

    public function getName()
    {
        return $this->title;
    }

    public function getViewUrl()
    {
        return Url::to('faq');
    }
}