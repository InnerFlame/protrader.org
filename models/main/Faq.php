<?php

namespace app\models\main;

use app\components\Entity;
use Yii;
use yii\helpers\Html;
use app\models\CustomModel;
use yii\helpers\Url;

/**
 * This is the model class for table "main_faq".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $answer
 * @property string $title
 * @property integer $weight
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property FaqCategory $category
 */
class Faq extends Entity
{
    /*
    |--------------------------------------------------------------------------
    | Constants && properties
    |--------------------------------------------------------------------------
    */

    public static $_pull = [];

    /*
    |--------------------------------------------------------------------------
    | Model configurations
    |--------------------------------------------------------------------------
    */

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'main_faq';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'title', 'answer', 'weight'], 'required'],
            [['id', 'category_id', 'weight', 'created_at', 'updated_at'], 'integer'],
            [['answer'], 'string'],
            [['published'], 'safe'],
            [['title'], 'string', 'max' => 120]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'category_id' => 'Category ID',
            'answer'      => 'Answer',
            'title'       => 'Title',
            'weight'      => 'Weight',
            'published'   => 'Published',
            'created_at'  => 'Created At',
            'updated_at'  => 'Updated At',
        ];
    }

    /**
     * @inheritdoc
     * @return PagesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FaqQuery(get_called_class());
    }

    /*
    |--------------------------------------------------------------------------
    | Model relations
    |--------------------------------------------------------------------------
    */

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(FaqCategory::className(), ['id' => 'category_id']);
    }

    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */

    public static function getGridColumns()
    {
        return [
            [
                'class'          => 'yii\grid\SerialColumn',
                'header'         => '<span>#</span>',
                'contentOptions' => [
                    'width' => 50
                ],
            ],
            [
                'attribute' => 'category_id',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'answer',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'title',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'weight',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'published',
                'format'    => 'date',
            ],
            [
                'class'    => 'app\components\grid\ActionColumnCustom',
                'template' => function ($model) {
                    return '{edit} {delete}';
                },
                'buttons'  => [
                    'edit'   => function ($url, $model, $key, $options) {
                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/faq-edit', 'id' => $model->id]);
                        return Html::a('Edit', $url, $options);
                    },
                    'delete' => function ($url, $model, $key, $options) {
                        $options['onclick'] = '
                            if(confirm(\'' . Yii::t('yii', 'Are you sure you want to delete this Page?') . '\'))
                            {
                              return true;
                            } else {
                              return false;
                            }
                        ';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/faq-delete', 'id' => $model->id]);
                        return Html::a('Delete', $url, $options);
                    },

                ],
            ],
        ];
    }

    public function getTypeEntity()
    {
        return Entity::FAQ;
    }

    public function getName()
    {
        return $this->title;
    }

    public function getViewUrl()
    {
        return Url::to('faq');
    }

}