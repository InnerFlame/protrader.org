<?php

namespace app\models\main;

use app\components\ceo\Ceo;
use app\components\ceo\CeoBehavior;
use app\components\Entity;
use app\components\langInput\LangInputBehavior;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\BaseUrl;

/**
 * This is the model class for table "main_pages".
 *
 * @property integer $id
 * @property string $title
 * @property string $alias
 * @property string $content
 * @property string $css
 * @property string $js
 * @property integer $weight
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 */
class Pages extends Entity
{
    /*
    |--------------------------------------------------------------------------
    | Constants && properties
    |--------------------------------------------------------------------------
    */

    public static $_pull = [];
    const CONTROLLER_INDEX = 1;

    /*
    |--------------------------------------------------------------------------
    | Model configurations
    |--------------------------------------------------------------------------
    */

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'main_pages';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class'      => LangInputBehavior::className(),
                'attributes' => ['content', 'title'],
            ],
            [
                'class' => CeoBehavior::className(),
            ],
            [
                'class'         => SluggableBehavior::className(),
                'attribute'     => 'title',
                'slugAttribute' => 'alias',
                'immutable'     => 'true',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'content'], 'required'],
            [['id', 'weight', 'published_at', 'created_at', 'updated_at'], 'integer'],
            [['content'], 'string'],
            [['title', 'alias'], 'string', 'max' => 120],
            [['css', 'js'], 'safe']
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'           => 'ID',
            'title'        => 'Title H1',
            'alias'        => 'Alias',
            'content'      => 'Content',
            'css'          => 'Css',
            'js'           => 'Js',
            'weight'       => 'Weight',
            'published_at' => 'Published At',
            'created_at'   => 'Created At',
            'updated_at'   => 'Updated At',
        ];
    }

    /**
     * @inheritdoc
     * @return PagesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PagesQuery(get_called_class());
    }

    /*
    |--------------------------------------------------------------------------
    | Model relations
    |--------------------------------------------------------------------------
    */

    /**
     * @return Pages|array|null
     */
    public static function getCeoCurrentPage()
    {
        $model = null;
        $urlArr = explode('/', BaseUrl::current());
        if (isset($urlArr[self::CONTROLLER_INDEX]))
            $model = self::find()->where(['alias' => $urlArr[self::CONTROLLER_INDEX]])->notDeleted()->one();

        return $model;
    }

    /**
     * @return $this
     */
    public function getCeo()
    {
        return $this->hasOne(Ceo::className(), ['entity_id' => 'id'])->where([
            'entity' => $this->getTypeEntity()
        ]);
    }

    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */

    /**
     * @return string
     */
    public function getName()
    {
        return $this->title;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getViewUrl()
    {
        return '/' . $this->alias;
    }

    /**
     * @return int
     */
    public function getTypeEntity()
    {
        return Entity::PAGE;
    }
}