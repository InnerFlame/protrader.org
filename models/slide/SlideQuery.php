<?php

namespace app\models\slide;
use app\components\Entity;

/**
 * This is the ActiveQuery class for [[Slide]].
 *
 * @see Slide
 */
class SlideQuery extends \yii\db\ActiveQuery
{
    /**
     * @inheritdoc
     * @return Slide[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Slide|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @return $this
     */
    public function main()
    {
        return $this->andWhere(['is_main' => 1]);
    }

    /**
     * @return $this
     */
    public function notDeleted()
    {
        return $this->andWhere(['deleted' => Entity::NOT_DELETED]);
    }
}
