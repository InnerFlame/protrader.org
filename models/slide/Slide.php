<?php

namespace app\models\slide;

use app\components\customImage\CustomImage;
use app\models\abstractes\AbstractSlide;
use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "slide".
 *
 * @property integer $id
 * @property string $entity
 * @property integer $entity_id
 * @property string $title
 * @property string $pictures
 * @property string $link
 * @property string $description
 * @property string $weight
 * @property string $data
 */
class Slide extends AbstractSlide
{
    public $image = null;
    public $crop = null;

    public $entity_array = [
        // 'Articles' =>'app\modules\articles\models\Articles',
        //'News' =>'app\models\news\News',
        //'Topic' =>'app\modules\forum\models\FrmTopic',
        'Feature' => 'app\modules\brokers\models\Improvement',
    ];

    public function init()
    {
        $this->image = new CustomImage([
            'uploadPath' => isset(Yii::$app->params['slideUploadPath']) ? Yii::getAlias('@webroot') . Yii::$app->params['slideUploadPath'] : '',
            'uploadUrl'  => isset(Yii::$app->params['slideUploadPath']) ? Yii::getAlias('@web') . Yii::$app->params['slideUploadPath'] : '',
            'model'      => $this,
        ]);

        $this->image->setValidation('pictures', [
            'extensions' => 'png,jpg',
            'maxHeight'  => 4000,
            'maxWidth'   => 4000,
            'maxSize'    => 2048000,
            'minSize'    => 1,
        ]);

        parent::init();
    }

    /**
     * BeforeSave action
     * Checking client images
     *
     * @return mixed
     */
    public function beforeSave($insert)
    {
        $this->entity = self::MAIN;
        $this->entity_id = null;

        if (!$this->image->isChanged('pictures')) {
            unset($this->pictures);
        }
        if ($this->image->isDeleted('pictures')) {
            $this->pictures = '';
        }

        return parent::beforeSave($insert);
    }

    /**
     *
     * After Save
     *
     * @return mixed
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($this->scenario <> 'delete') {
            if ($this->image->getUploadedFile('pictures')) {
                $this->pictures = $this->image->uploadImage('pictures', false);
                if ($this->pictures)
                    $this->save(false);
            }
        }

        return parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return Slide[]|array
     */
    public static function getMainSlides()
    {
        return self::find()->limit(7)->where(['is_main' => 1, 'entity' => 'main'])->orderBy('weight DESC')->all();
    }

    /**
     * @inheritdoc
     * @return SlideQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SlideQuery(get_called_class());
    }

    public static function getGridColumns()
    {
        return [
            [
                'class'          => 'yii\grid\SerialColumn',
                'header'         => '<span>#</span>',
                'contentOptions' => [
                    'width' => 50
                ],
            ],

            [
                'attribute' => 'title',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'description',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'link',
                'format'    => 'raw',
            ],

            [
                'attribute' => 'weight',
                'format'    => 'raw',
            ],


            [
                'class'    => 'app\components\grid\ActionColumnCustom',
                'template' => function ($model) {
                    return '{edit} {delete}';
                },
                'buttons'  => [
                    'edit' => function ($url, $model, $key, $options) {
                        //  $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/slide-edit', 'id' => $model->id]);
                        return Html::a('Edit', $url, $options);

                    },

                    'delete' => function ($url, $model, $key, $options) {
                        $options['onclick'] = '
                        if(confirm(\'' . Yii::t('yii', 'Are you sure you want to delete this Page?') . '\'))
                        {
                          return true;
                        } else {
                          return false;
                        }
                    ';
//                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/slide-delete', 'id' => $model->id]);
                        return Html::a('Delete', $url, $options);
                    },

                ],
            ],
        ];
    }
}