<?php
/**
 * Created by PhpStorm.
 * @author: Roman Budnitsky
 * @date: 18.11.14 15:23
 * @copyright Copyright (c) 2014 PFSOFT LLC
 *
 * Class Country
 */

namespace app\models;

use yii;
use yii\base\Model;

class Constants extends ConstantBase
{

    // Rule Constants
    const RULE_GROUP_ACCOUNT = 301;
    const RULE_GROUP_SALARY  = 302;
    const RULE_GROUP_STAFF   = 303;
    const RULE_GROUP_REQUEST = 304;
    const RULE_GROUP_CABINET = 305;

    // Salary Constants
    const SALARY_CONSTANT_MINIMUM_WAGE      = 001;
    const SALARY_CONSTANT_SELF_EMPLOYED_TAX = 002;
    const SALARY_TYPE_CARD                  = 011;
    const SALARY_TYPE_ACCOUNT               = 012;

    // User Salary Type
    const USER_SALARY_TYPE_SELF_EMPLOYED = 111;
    const USER_SALARY_TYPE_MINIMAL       = 112;

    // User Status Type
    const USER_ACTIVE_STATUS  = 121;
    const USER_DISABLE_STATUS = 122;
    const USER_FIRED_STATUS   = 123;

    // Vacation Type
    const VACATION_STANDART_TYPE = 501;

    // Account Activity
    const ACCOUNT_ACTIVITY_TRANSFER_TYPE = 211;

    // Inventory type
    const INVENTORY_TYPE_FURNITURE = 411;
    const INVENTORY_TYPE_TECHNICS  = 412;

    # Entity
    const ENTITY_IMPROVEMENT = 'app\modules\brokers\models\Improvement';
    const ENTITY_VIDEO = 'app\models\video\Video';
    const ENTITY_NEWS = 'app\models\news\News';
    const ENTITY_ARTICLES = 'app\modules\articles\models\Articles';
    const ENTITY_PAGES = 'app\models\main\Pages';
    const ENTITY_DOWNLOAD = 'app\models\forms\Download';
    const ENTITY_FRM_TOPIC = 'app\modules\forum\models\FrmTopic';
    const ENTITY_COMMENT = 'app\models\community\Comments';
    const ENTITY_BROKER = 'app\modules\brokers\models\Broker';
    const ENTITY_FRM_COMMENT = 'app\modules\forum\models\FrmComment';

    # Search limit
    const SEARCH_COUNT_NEWS = 8;
    const SEARCH_COUNT_ARTICLES = 8;
    const SEARCH_COUNT_FAQ = 8;
    const SEARCH_COUNT_VIDEO = 8;
    const SEARCH_COUNT_IMPROVEMENT = 8;
    const SEARCH_COUNT_FRM_TOPIC = 4;

    # Sort
    const SORT_BY_DATE = 'date';
    const SORT_BY_POPULAR = 'popular';

    // Constant Group
    public static $constant_groups = [
        'user_status_type' => [
            self::USER_ACTIVE_STATUS,
            self::USER_DISABLE_STATUS,
        ]
    ];

    // Constant Group
    public static $published = [
        '1' => 'Yes',
        '0' => 'No',
    ];

} 