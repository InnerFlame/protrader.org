<?php
/**
 * Created by PhpStorm.
 * User: Zaitsev Oleg
 * Date: 26.06.15
 * Time: 10:27
 *
 * Class License
 * @package app\modules\knowledgebase\models
 */


namespace app\models;

use yii;
use yii\db\ActiveRecord;
use yii\db\ActiveQuery;
use yii\helpers\Html;
use yii\behaviors\TimestampBehavior;
use app\components\customQuery\CustomQueryActiveRecord;
use app\components\MultiSelectCustom;
use app\models\user\User;
use app\models\user\Vacation;
use app\models\department\Department;
use app\models\Constants;

/**
 * This is the model class for table "type_list"
 *
 * @property integer $id
 * @property string $name
 * @property string $table_name
 * @property string $data
 * @property string $constants
 * @property integer $deleted_at
 *
 */

class TypeList extends CustomQueryActiveRecord
{

    public static $_pull = [];

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'table_name'], 'required'],
            [['deleted_at'], 'integer'],
            [['name'], 'string', 'max' => 128],
            [['table_name'], 'string', 'max' => 32],
            [['constants', 'data'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function getAttributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'table_name' => 'Table',
            'constants' => 'Constants',
            'deleted_at' => 'Deleted At',
        ];
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%type_list}}';
    }

    /**
     * Before Save commands
     * @params boolean $insert
     *
     * @return mixed
     *
     */
    public function beforeSave($insert)
    {
        $this->name = Yii::$app->customFunctions->cleanText($this->name);
        $this->table_name = Yii::$app->customFunctions->cleanText($this->table_name);

        return parent::beforeSave($insert);
    }

    /**
     * After Save commands
     * @params boolean $insert
     * @params $mixed changedAttributes
     *
     * @return mixed
     *
     */
    public function afterSave($insert, $changedAttributes)
    {
        return parent::afterSave($insert, $changedAttributes);
    }

    public static function getTypeList($table_name, $field = 'id')
    {
        if(isset(self::$_pull[$table_name])) return self::$_pull[$table_name];

        $models = TypeList::find()->notdeleted()->andWhere(['table_name' => $table_name])
        ->orderBy('name ASC')->all();

        self::$_pull[$table_name] = yii\helpers\ArrayHelper::map($models, $field, 'name');

        return self::$_pull[$table_name] ;
    }

    public function getConstantsById($id)
    {
        if($id > 0) {
            $type_list = TypeList::findOne($id);
            if (isset($type_list->constants)) return $type_list->constants;
        }

        return false;
    }

    public static function getGridColumns($table_name = null)
    {
        return [
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => '<span>#</span>',
                'contentOptions' => [
                    'width' => 70
                ],
            ],
            [
                'attribute' => 'name',
                'format' => 'raw',
            ],
            [
                'attribute' => 'constants',
                'header' => Yii::t('app', 'Standart Type'),
                'format' => 'raw',
                'value' => function($model)use($table_name){
                        if($table_name == 'user_status_type'){
                            if(isset(User::$defaultTypesList[$model->constants]))
                                return User::$defaultTypesList[$model->constants];
                        }
                        if($table_name == 'vacation'){
                            if(isset(Vacation::$defaultTypesList[$model->constants]))
                                return Vacation::$defaultTypesList[$model->constants];
                        }

                        return null;

                    },
                'visible' => $table_name == 'user_status_type' || $table_name == 'vacation',
            ],
            [
                'attribute' => 'data',
                'header' => Yii::t('app', 'To Card'),
                'format' => 'raw',
                'value' => function($model){
                    return $model->data;
                },
                'visible' => $table_name == 'user_salary_type',
            ],
            [
                'class' => 'app\components\grid\ActionColumnCustom',
                'template' => function($model) {
                    return '{update} {delete}';
                },
                'buttons' => [
                    'update' => function($url, $model, $key, $options){
                        $url = Yii::$app->urlManager->createUrl(['cabinet/edit-type-list', 'id' => $model->id]);
                        return Html::a('Edit', $url, $options);
                    },

                    'delete' => function($url, $model, $key, $options){
                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' type-action';
                        $options['onclick'] = 'return false;';
                        $options['data']['id'] = $model->id;
                        $options['data']['custom-confirm'] = Yii::t('yii', 'Are you sure you want to delete this Type?');

                        $url = Yii::$app->urlManager->createUrl(['cabinet/delete-type-list', 'id' => $model->id]);
                        return Html::a('Delete', $url, $options);
                    },

                ],
            ],
        ];
    }

    public static function getStandartTypeIds()
    {
        if(isset(self::$_pull['standart_type_ids'])) return self::$_pull['standart_type_ids'];

        $models = self::find()->where('table_name = \'vacation\' AND constants =\'' . Constants::VACATION_STANDART_TYPE . '\'')->all();

        $result = array_keys(yii\helpers\ArrayHelper::map($models, 'id' , 'name'));

        self::$_pull['standart_type_ids'] = $result;

        return $result;
    }

}