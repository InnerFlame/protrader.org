<?php
/**
 * Created by PhpStorm.
 * User: Zaitsev Oleg
 * Date: 26.06.15
 * Time: 10:27
 *
 * Class License
 * @package app\modules\knowledgebase\models
 */


namespace app\models\video;

use app\components\customImage\CustomImage;
use app\components\Entity;
use app\models\community\Counts;
use app\models\community\Likes;
use yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Html;

/**
 * This is the model class for table "vd_video"
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $title
 * @property string $pictures
 * @property string $alias
 * @property string $weight
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $published
 *
 */
class Video extends Entity
{
    /*
    |--------------------------------------------------------------------------
    | Constants && properties
    |--------------------------------------------------------------------------
    */

    const TYPE_YOUTUBE = 'youtube.com';
    const TYPE_VIMEO = 'vimeo.com';

    public $playlist = false;
    public static $_pull = [];

    public $image = null;
    public $crop = null;

    /*
    |--------------------------------------------------------------------------
    | Model configurations
    |--------------------------------------------------------------------------
    */

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%vd_video}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'alias', 'link'], 'required'],
            [['title'], 'unique'],
            [['title'], 'string', 'max' => 120],
            [['category_id'], 'integer'],
            [['alias'], 'string', 'max' => 255],
            [['published', 'weight', 'playlist', 'pictures'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('app', 'ID'),
            'category_id' => Yii::t('app', 'Category ID'),
            'title'       => Yii::t('app', 'Title'),
            'pictures'    => Yii::t('app', 'Pictures'),
            'alias'       => Yii::t('app', 'Alias'),
            'link'        => Yii::t('app', 'Link'),
            'weight'      => Yii::t('app', 'Weight'),
            'published'   => Yii::t('app', 'Published'),
            'deleted'     => Yii::t('app', 'Deleted'),
            'created_at'  => Yii::t('app', 'Created At'),
            'updated_at'  => Yii::t('app', 'Updated At'),
        ];
    }

    public function init()
    {
        $this->image = new CustomImage([
            'uploadPath' => isset(Yii::$app->params['videoUploadPath']) ?
                Yii::getAlias('@webroot') . Yii::$app->params['videoUploadPath'] : '',
            'uploadUrl'  => isset(Yii::$app->params['videoUploadPath']) ?
                Yii::getAlias('@web') . Yii::$app->params['videoUploadPath'] : '',
            'model'      => $this,
        ]);

        $this->image->setValidation('pictures', [
            'extensions' => 'png,jpg',
            'minHeight'  => 1,
            'minWidth'   => 1,
            'maxSize'    => 2048000,
            'minSize'    => 1,
            'required'   => true,
        ]);

        parent::init();
    }

    public function beforeValidate()
    {
        $this->image->validateAttr('pictures');

        return parent::beforeValidate();
    }

    /**
     * BeforeSave action
     * Checking client images
     *
     * @return mixed
     */
    public function beforeSave($insert)
    {
        if (!$this->image->isChanged('pictures')) unset($this->pictures);
        if ($this->image->isDeleted('pictures')) $this->pictures = '';

        return parent::beforeSave($insert);
    }

    /**
     *
     * After Save
     *
     * @return mixed
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($this->scenario <> 'delete') {
            if ($this->image->getUploadedFile('pictures')) {

                $this->pictures = $this->image->uploadImage('pictures');
                if ($this->pictures)
                    $this->save(false);
            }
        }

        return parent::afterSave($insert, $changedAttributes);
    }

    /*
    |--------------------------------------------------------------------------
    | Model relations
    |--------------------------------------------------------------------------
    */

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(VdCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntity()
    {
        return $this->hasOne(Counts::className(), ['entity_id' => 'id'])->where('entity = \'' . $this->getTypeEntity() . '\'');
    }

    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */

    /**
     * @param $model
     * @param string $style
     * @return string
     */
    public static function getStatusClass($model, $style = '')
    {
        return Likes::isLike(Entity::VIDEO, $model->id) ?
            '<a style="' . $style . '" data-id="' . $model->id . '" type="button" class="btn btn-xs btn-primary like-btn"><i class="fa mr-5 fa-thumbs-o-up"></i> Нравится</a>'
            : '<a style="' . $style . '" data-id="' . $model->id . '" type="button" class="btn btn-xs btn-primary disabled like-btn"><i class="fa mr-5 fa-thumbs-o-up"></i> Нравится</a>';
    }

    /**
     * @param $_link
     * @param $_width
     * @param $_height
     * @return string
     */
    public static function getIframe($_link, $_width, $_height)
    {
        $iframe = '';

        if (strripos($_link, 'youtube.com') !== false) {
            parse_str(parse_url($_link, PHP_URL_QUERY), $my_array_of_vars);
            $link = 'http://www.youtube.com/embed/' . $my_array_of_vars['v'] . '?title=0&amp;byline=0&amp;portrait=0';
        }

        if (strripos($_link, 'vimeo.com') !== false) {
            $vimeo = substr(parse_url($_link, PHP_URL_PATH), 1);
            $link = 'https://player.vimeo.com/video/' . $vimeo . '?title=0&amp;byline=0&amp;portrait=0';
        }

        if (isset($link)) {
            $iframe = "<iframe width='" . $_width . "' height='" . $_height . "' src='" . $link . "'></iframe>";

            return $iframe;
        }

    }

    /**
     * @return string
     */
    public function getPicLink()
    {
        if ($this->pictures)
            return Yii::$app->params['videoUploadPath'] . '/' . $this->id . '/' . $this->pictures;
        else
            return 'http://placehold.it/340x150';
    }

    /**
     * @param null $post
     */
    public function savePlaylists($post = null)
    {
        $playlist = $post;//['Video']['playlist']

        if (isset($playlist) && is_array($playlist)) {
            foreach ($playlist as $id) {
                $model = Playlist::findOne((int)$id);
                if (is_object($model)) {
                    $data = json_decode($model->data, true);
                    if (is_null($data)) {
                        $data['playlist'][$this->id] = ['weight' => $this->weight];
                    }

                    if (isset($data['playlist'])) {
                        $keys = array_keys($data['playlist']);
                        if (count($keys) > 0 && !in_array($this->id, $keys)) {
                            $data['playlist'][$this->id] = ['weight' => $this->weight];
                        }
                    }
                    $model->data = json_encode($data);
                    $model->save(false);
                }
            }
        }
    }

    public function getPlaylists()
    {
        $models = Playlist::find()->All();

        if (is_array($models)) {
            foreach ($models as $key => $model) {
                $data = json_decode($model->data, true);
                if (!is_null($data)) {
                    if (array_key_exists('playlist', $data)) {
                        $keys = array_keys($data['playlist']);
                        if (in_array($this->id, $keys)) {
                            $this->playlist[] = $model->id;
                        }
                    }
                }
            }
        }
    }

    /**
     * @inheritdoc
     * @return VideoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VideoQuery(get_called_class());
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->title;
    }

    /**
     * @return array
     */
    public static function getGridColumns()
    {
        return [
            [
                'class'          => 'yii\grid\SerialColumn',
                'header'         => '<span>#</span>',
                'contentOptions' => [
                    'width' => 50
                ],
            ],
            [
                'attribute' => 'category_id',
                'format'    => 'raw',
                'value'     => function ($model) {
                    return VdCategory::getTitleById($model->category_id);
                }
            ],
            [
                'attribute' => 'title',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'alias',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'link',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'published',
                'format'    => 'date',
            ],

            [
                'class'    => 'app\components\grid\ActionColumnCustom',
                'template' => function ($model) {
                    return '{edit} {delete}';
                },
                'buttons'  => [
                    'edit'   => function ($url, $model, $key, $options) {
                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/video-edit', 'id' => $model->id]);
                        return Html::a('Edit', $url, $options);
                    },
                    'delete' => function ($url, $model, $key, $options) {
                        $options['onclick'] = '
                            if(confirm(\'' . Yii::t('yii', 'Are you sure you want to delete this Page?') . '\'))
                            {
                              return true;
                            } else {
                              return false;
                            }
                        ';
//                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/video-delete', 'id' => $model->id]);
                        return Html::a('Delete', $url, $options);
                    },

                ],
            ],
        ];
    }

    /**
     * @return int
     */
    public function getTypeEntity()
    {
        return Entity::VIDEO;
    }

    /**
     * @return null
     */
    public function getViewUrl()
    {
        return null;
    }
}