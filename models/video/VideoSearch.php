<?php

namespace app\models\video;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * VideoSearch represents the model behind the search form about `app\models\video\Video`.
 */
class VideoSearch extends Video
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'weight', 'published', 'deleted', 'created_at', 'updated_at'], 'integer'],
            [['title', 'pictures', 'alias', 'link'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Video::find()->notDeleted();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'id'          => $this->id,
            'category_id' => $this->category_id,
            'weight'      => $this->weight,
            'published'   => $this->published,
            'deleted'     => $this->deleted,
            'created_at'  => $this->created_at,
            'updated_at'  => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'pictures', $this->pictures])
            ->andFilterWhere(['like', 'alias', $this->alias])
            ->andFilterWhere(['like', 'link', $this->link]);

        return $dataProvider;
    }
}
