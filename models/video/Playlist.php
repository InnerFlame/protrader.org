<?php

namespace app\models\video;

use app\components\Entity;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Html;

/**
 * This is the model class for table "vd_playlist".
 *
 * @property integer $id
 * @property string $title
 * @property string $alias
 * @property string $data
 * @property integer $weight
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 */
class Playlist extends Entity
{
    public static $_pull = [];

    public $video_list = false;
    public $playlist = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'alias'], 'required'],
            [['title'], 'unique'],
            [['title'], 'string', 'max' => 128],
            [['alias'], 'string', 'max' => 255],
            [['data'], 'string'],
            [['alias', 'data', 'published', 'weight', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('app', 'ID'),
            'title'      => Yii::t('app', 'Title'),
            'alias'      => Yii::t('app', 'Alias'),
            'data'       => Yii::t('app', 'Data'),
            'weight'     => Yii::t('app', 'Weight'),
            'published'  => Yii::t('app', 'Published'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     * @return PlaylistQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PlaylistQuery(get_called_class());
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%vd_playlist}}';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * After Save commands
     * @params boolean $insert
     * @params $mixed changedAttributes
     *
     * @return mixed
     *
     */
    public function afterSave($insert, $changedAttributes)
    {
        return parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @param $post
     */
    public function saveVideolists($post)
    {
        $data = [];
        if (isset($post['Playlist']['playlist']))
            $playlist = $post['Playlist']['playlist'];

        if (isset($playlist) && is_array($playlist)) {
            //    if(!is_null($this->data)){
            $data = json_decode($this->data, true);
            // }else{
            $data = null;
            //   }
            foreach ($playlist as $id) {
                //    if(isset($data['playlist'])){
                //        $keys = array_keys($data['playlist']);
                //    }
                //   if (is_null($data)) {
                $data['playlist'][$id] = ['weight' => ''];
                //      } elseif(!is_null($data)){
                $data['playlist'][$id] = ['weight' => ''];
                //    }
            }
        }

        $this->data = json_encode($data);
        //echo '<pre>'; var_dump($this->data, $data, $playlist);die;
        $this->save(false);
    }

    /**
     * @return array|null
     */
    public function getVideolists()
    {
        $data = json_decode($this->data, true);
        if (!is_null($data))
            if (array_key_exists('playlist', $data))
                return $this->playlist = array_keys($data['playlist']);

        return null;
    }

    /**
     * @return array
     */
    public static function getGridColumns()
    {
        return [
            [
                'class'          => 'yii\grid\SerialColumn',
                'header'         => '<span>#</span>',
                'contentOptions' => [
                    'width' => 50
                ],
            ],
            [
                'attribute' => 'title',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'alias',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'data',
                'format'    => 'raw',
                'value'     => function ($model) {
                    if (is_array($model->getVideolists())) {
                        $model->data = '';
                        foreach ($model->playlist as $id) {
                            $model->data .= Video::getTitleById($id) . ', ';
                        }
                    }
                    return $model->data;
                },
            ],
            [
                'attribute' => 'published',
                'format'    => 'date',
            ],

            [
                'class'    => 'app\components\grid\ActionColumnCustom',
                'template' => function ($model) {
                    return '{edit} {delete}';
                },
                'buttons'  => [
                    'edit'   => function ($url, $model, $key, $options) {
                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/playlist-edit', 'id' => $model->id]);
                        return Html::a('Edit', $url, $options);
                    },
                    'delete' => function ($url, $model, $key, $options) {
                        $options['onclick'] = '
                            if(confirm(\'' . Yii::t('yii', 'Are you sure you want to delete this Page?') . '\'))
                            {
                              return true;
                            } else {
                              return false;
                            }
                        ';
//                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/playlist-delete', 'id' => $model->id]);
                        return Html::a('Delete', $url, $options);
                    },

                ],
            ],
        ];
    }

    /**
     * @return int
     */
    public function getTypeEntity()
    {
        return Entity::VIDEO_PLAYLIST;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->title;
    }

    /**
     * @return null
     */
    public function getViewUrl()
    {
        return null;
    }
}