<?php
/**
 * Created by PhpStorm.
 * User: Zaitsev Oleg
 * Date: 26.06.15
 * Time: 10:27
 *
 * Class License
 * @package app\modules\knowledgebase\models
 */


namespace app\models\video;

use app\components\Entity;
use yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Html;

/**
 * This is the model class for table "vd_category"
 *
 * @property integer $id
 * @property string $title
 * @property string $alias
 * @property string $data
 * @property string $weight
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $published
 *
 */
class VdCategory extends Entity
{
    /*
    |--------------------------------------------------------------------------
    | Constants && properties
    |--------------------------------------------------------------------------
    */

    public static $_pull = [];

    /*
    |--------------------------------------------------------------------------
    | Model configurations
    |--------------------------------------------------------------------------
    */

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'unique'],
            [['title'], 'string', 'max' => 128],
            [['alias'], 'string', 'max' => 255],
            [['alias', 'data', 'weight', 'published'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('app', 'ID'),
            'title'      => Yii::t('app', 'Title'),
            'alias'      => Yii::t('app', 'Alias'),
            'data'       => Yii::t('app', 'Data'),
            'weight'     => Yii::t('app', 'Weight'),
            'published'  => Yii::t('app', 'Published'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%vd_category}}';
    }

    /**
     * After Save commands
     * @params boolean $insert
     * @params $mixed changedAttributes
     *
     * @return mixed
     *
     */
    public function afterSave($insert, $changedAttributes)
    {
        return parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @inheritdoc
     * @return VdCategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VdCategoryQuery(get_called_class());
    }

    /*
    |--------------------------------------------------------------------------
    | Model relations
    |--------------------------------------------------------------------------
    */

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVideo()
    {
        return $this->hasMany(Video::className(), ['category_id' => 'id']);
    }

    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */

    /**
     * @return array
     */
    public static function getGridColumns()
    {
        return [
            [
                'class'          => 'yii\grid\SerialColumn',
                'header'         => '<span>#</span>',
                'contentOptions' => [
                    'width' => 50
                ],
            ],
            [
                'attribute' => 'title',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'alias',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'data',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'published',
                'format'    => 'date',
            ],

            [
                'class'    => 'app\components\grid\ActionColumnCustom',
                'template' => function ($model) {
                    return '{edit} {delete}';
                },
                'buttons'  => [
                    'edit'   => function ($url, $model, $key, $options) {
                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/video-category-edit', 'id' => $model->id]);
                        return Html::a('Edit', $url, $options);
                    },
                    'delete' => function ($url, $model, $key, $options) {
                        $options['onclick'] = '
                            if(confirm(\'' . Yii::t('yii', 'Are you sure you want to delete this Page?') . '\'))
                            {
                              return true;
                            } else {
                              return false;
                            }
                        ';
//                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/video-category-delete', 'id' => $model->id]);
                        return Html::a('Delete', $url, $options);
                    },

                ],
            ],
        ];
    }

    /**
     * @return int
     */
    public function getTypeEntity()
    {
        return Entity::VIDEO_CAT_PLAYLIST;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->title;
    }

    /**
     * @return null
     */
    public function getViewUrl()
    {
        return null;
    }
}