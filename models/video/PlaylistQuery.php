<?php

namespace app\models\video;

use app\components\Entity;

/**
 * This is the ActiveQuery class for [[Playlist]].
 *
 * @see Playlist
 */
class PlaylistQuery extends \yii\db\ActiveQuery
{
    /**
     * @inheritdoc
     * @return Playlist[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Playlist|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @return $this
     */
    public function notDeleted()
    {
        return $this->andWhere(['deleted' => Entity::NOT_DELETED]);
    }
}
