<?php

namespace app\models\community;


use app\components\Entity;
use app\models\DefaultQuery;
use Yii;
use app\models\CustomModel;
use yii\helpers\Html;
use yii\behaviors\TimestampBehavior;
use app\models\user\User;

/**
 * This is the model class for table "com_counts".
 *
 * @property integer $id
 * @property string $entity
 * @property integer $entity_id
 * @property integer $count_view
 * @property integer $count_like
 * @property integer $count_dislike
 * @property integer $count_commment
 * @property integer $created_at
 * @property integer $updated_at
 */
class Counts extends CustomModel
{
    /*
    |--------------------------------------------------------------------------
    | Constants && properties
    |--------------------------------------------------------------------------
    */

    public static $_pull = [];

    /*
    |--------------------------------------------------------------------------
    | Model configurations
    |--------------------------------------------------------------------------
    */

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'com_counts';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entity_id', 'entity', 'count_view', 'count_like', 'count_dislike', 'count_commment', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => Yii::t('app', 'ID'),
            'entity'        => Yii::t('app', 'Entity'),
            'entity_id'     => Yii::t('app', 'Entity ID'),
            'count_view'    => Yii::t('app', 'Count View'),
            'count_like'    => Yii::t('app', 'Count Like'),
            'count_dislike' => Yii::t('app', 'Count Dislike'),
            'count_commment' => Yii::t('app', 'Count Comment'),
            'created_at'    => Yii::t('app', 'Created At'),
            'updated_at'    => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     * @return CountsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DefaultQuery(get_called_class());
    }

    /*
    |--------------------------------------------------------------------------
    | Model relations
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */

    /**
     * @param $entity
     * @param $field
     * @return int
     */
    public static function setCount($entity, $field = 'count_like', $id = null)
    {
        $entity_id = $id ? $id : Yii::$app->request->post('id');

        $model = Counts::find()->where([
            'entity'    => $entity,
            'entity_id' => $entity_id,
        ])->one();

        if (is_object($model)) {
            $model->updateCounters([$field => 1]);

        } else {
            $model = new Counts();
            $model->entity = $entity;
            $model->entity_id = $entity_id;
            $model->$field = 1;
            $model->save();
        }

        return $model->count_like;
    }

    /**
     * @param $entity
     * @return int|mixed
     */
    public static function getCount($entity, $id = null)
    {
        $entity_id = $id ? $id : Yii::$app->request->post('id');
        $model = Counts::find()->where([
            'entity'    => $entity,
            'entity_id' => $entity_id,
        ])->one();

        if (is_object($model))
            return json_encode([
                'count_like'    => $model->count_like,
                'count_like_fake' => $model->count_like_fake,
                'count_dislike' => $model->count_dislike,
                'count_percent' => $model->getPercent(),
            ]);

        return 1;
    }

    /**
     * @return mixed
     */
    public function getMaxLike()
    {
        return self::find()->where(['entity' => $this->entity])->max('count_like');
    }


    /**
     * @return float
     */

    public function getPercent()
    {
        if($this->count_like == 0){
            return 0;
        }
        return round(($this->count_like / $this->getMaxLike()) * 100);
    }

}
