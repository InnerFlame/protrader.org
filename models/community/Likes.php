<?php


namespace app\models\community;

use app\models\DefaultQuery;
use app\models\user\User;
use Yii;
use app\models\CustomModel;
use yii\helpers\Html;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "com_likes".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $user_ip
 * @property string $entity
 * @property integer $entity_id
 * @property integer $created_at
 * @property integer $updated_at
 */
class Likes extends CustomModel
{
    /*
    |--------------------------------------------------------------------------
    | Constants && properties
    |--------------------------------------------------------------------------
    */


    /*
    |--------------------------------------------------------------------------
    | Model configurations
    |--------------------------------------------------------------------------
    */

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'com_likes';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'entity_id', 'created_at', 'updated_at'], 'integer'],
            [['user_ip', 'entity'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('app', 'ID'),
            'user_id'    => Yii::t('app', 'User ID'),
            'user_ip'    => Yii::t('app', 'User Ip'),
            'entity'     => Yii::t('app', 'Entity'),
            'entity_id'  => Yii::t('app', 'Entity ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     * @return LikesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DefaultQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    /*
    |--------------------------------------------------------------------------
    | Model relations
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */

    public static function setLike($entity)
    {
        $model = Likes::find()->where([
            'user_id'   => Yii::$app->user->identity->id,
            'entity'    => $entity,
            'entity_id' => Yii::$app->request->post('id'),
        ])->one();

        if (!is_object($model)) {
            $model = new Likes();
            $model->user_id = Yii::$app->user->identity->id;
            $model->user_ip = $_SERVER["REMOTE_ADDR"];
            $model->entity = $entity;
            $model->entity_id = Yii::$app->request->post('id');
            $model->save();

            return true;
        }

        return false;
    }

    public static function isLike($entity, $entity_id)
    {
        if(isset(Yii::$app->user->identity->id)) {
            $model = Likes::find()->where([
                'user_id'   => Yii::$app->user->identity->id,
                'entity'    => $entity,
                'entity_id' => $entity_id,
            ])->one();

            if (!is_object($model)) {
                return true;
            }
        }

        return false;
    }

//    public static function isLike($entity, $entity_id)
//    {
//        $condition = [
//            'entity'    => $entity,
//            'entity_id' => $entity_id,
//        ];
//
//        $condition = isset(Yii::$app->user->identity->id) ?
//            array_merge($condition, ['user_id'   => Yii::$app->user->identity->id]) :
//            array_merge($condition, ['user_ip'   => $_SERVER['REMOTE_ADDR']]);
//
//        $model = Likes::find()->where($condition)->one();
//
//        if (!is_object($model)) {
//            return true;
//        }
//
//        return false;
//    }

}