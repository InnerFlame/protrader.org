<?php

namespace app\models\community;

use app\components\Entity;
use app\components\validators\EmptyContent;
use app\models\DefaultQuery;
use app\models\user\User;
use app\modules\articles\models\Articles;
use creocoder\nestedsets\NestedSetsBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "com_comments".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $content
 * @property integer $root
 * @property integer $lft
 * @property integer $rgt
 * @property integer $level
 * @property string $entity
 * @property integer $entity_id
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 */
class Comments extends Entity
{
    /*
    |--------------------------------------------------------------------------
    | Constants && properties
    |--------------------------------------------------------------------------
    */

    const LEVEL_ROOT = 0;
    const LEVEL_MAIN = 1;

    /*
    |--------------------------------------------------------------------------
    | Model configurations
    |--------------------------------------------------------------------------
    */

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'com_comments';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
//            'customEvents' => [
//                'class' => CustomEventsBehavior::className(),
//                'type' => CustomEventsBehavior::SUBSCRIBERS_OBSERVER,
//            ],
            TimestampBehavior::className(),
            'tree' => [
                'class'          => NestedSetsBehavior::className(),
                'treeAttribute'  => 'root',
                'leftAttribute'  => 'lft',
                'rightAttribute' => 'rgt',
                'depthAttribute' => 'level',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'content'], 'required'],
            [['content'], EmptyContent::className()],
            [['user_id', 'entity_id', 'published', 'created_at', 'updated_at'], 'integer'],
            [['content'], 'string'],
            [['content'], 'string', 'max' => 3000],
            [['entity'], 'string', 'max' => 255],
            [['level', 'root', 'rgt', 'lft'], 'safe']
        ];

    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('app', 'ID'),
            'user_id'    => Yii::t('app', 'User Name'),
            'content'    => Yii::t('app', 'Content'),
            'root'       => Yii::t('app', 'Root'),
            'lft'        => Yii::t('app', 'Lft'),
            'rgt'        => Yii::t('app', 'Rgt'),
            'level'      => Yii::t('app', 'Level'),
            'entity'     => Yii::t('app', 'Entity'),
            'entity_id'  => Yii::t('app', 'Entity ID'),
            'published'  => Yii::t('app', 'Published'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return CommentsQuery
     */
    public static function find()
    {
        return new DefaultQuery(get_called_class());
    }

    /**
     * @return int
     */
    public function getTypeEntity()
    {
        return Entity::COMMUNITY_COMMENT;
    }

    /**
     * @return string
     */
    public function getViewUrl()
    {
        $entity = $this->getEntity();
        if ($entity && $entity instanceof Entity) {
            return $this->getEntity()->getViewUrl() . '#reply' . $this->id;
        }
    }

    /*
    |--------------------------------------------------------------------------
    | Model relations
    |--------------------------------------------------------------------------
    */

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return Entity
     */
    public function getEntity()
    {
        if ($this->entity) {
            $class_name = Entity::getEntityClassByType($this->entity);
            return $class_name::findOne((int)$this->entity_id);
        }

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasOne(Articles::className(), ['id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCounts()
    {
        return $this->hasOne(Counts::className(), ['entity_id' => 'id'])->where([
            'entity' => Entity::COMMUNITY_COMMENT
        ]);
    }

    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */

    /**
     * @return bool|null|string
     */
    public function getDate()
    {
        if ($this->created_at > 0)
            return date('M d, Y', $this->created_at);

        return null;
    }

    /**
     * @param $id
     * @param $comment
     */
    public static function editComment($id, $comment)
    {
        if ($id > 0) {
            $model = self::findOne((int)$id);
            if (is_object($model))
                if ($model->user_id == Yii::$app->user->identity->id || Yii::$app->user->identity->isAdmin()) {
                    $model->content = $comment;
                    $model->save();
                }
        }
    }

    /**
     * @return bool
     */
    public function hasChildren()
    {
        $children = $this->children()->all();

        if (count($children) > 0)
            return true;

        return false;
    }

    /**
     * @return mixed
     */
    public function getEntityModel()
    {
        $class_name = $this->entity;

        return $class_name::findOne(['id' => (int)$this->entity_id]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->content;
    }

    public function delete($hard = false)
    {
        parent::delete($hard); // TODO: Change the autogenerated stub
        $childrens = $this->children()->all();
        foreach ($childrens as $child) {
            $child->delete($hard);
        }
    }
}
