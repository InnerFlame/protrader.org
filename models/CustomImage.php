<?php

namespace app\models;

use yii;
use yii\base\Model;
use yii\web\UploadedFile;
use yii\helpers\Html;

class CustomImage extends Model
{
    /**
     * @var UploadedFile[]
     */
    public $image;
    public $filenames = [];
    public $storage = [];
    public $uploadPath = null;
    public $uploadUrl = null;
    public $model = null;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function __toString()
    {
        return $this->filename;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * return uploaded file
     * @param string $attribute
     * @return string
     */
    public function getUploadedFile($attribute) {
       return UploadedFile::getInstance($this->model, $attribute);
    }

    /**
     * Process upload of image
     *@param string $attribute
     * @return mixed the uploaded image instance
     */
    public function uploadImage($attribute) {
        if(!isset($this->filenames[$attribute])) {
            // get the uploaded file instance. for multiple file uploads
            // the following data will return an array (you may need to use
            // getInstances method)
            $image = $this->getUploadedFile($attribute);

            // if no image was uploaded abort the upload
            if (empty($image)) {
                return false;
            }

            if (!is_dir(Yii::$app->assetManager->basePath . '/images')) mkdir(Yii::$app->assetManager->basePath . '/images', 0777);
            if (!is_dir($this->uploadPath)) mkdir($this->uploadPath, 0777);
            $file_dir = $this->uploadPath;

            if (isset($this->model->id)) {
                if ($this->model->id > 0) {
                    $file_dir = $this->uploadPath . '/' . $this->model->id;
                    if (!is_dir($file_dir)) mkdir($file_dir, 0777);
                }
            }

            if (isset($image->name)) {

                // store the source file name
                $ext = end((explode(".", $image->name)));

                // generate a unique file name
                $this->filenames[$attribute] = $filename = Yii::$app->security->generateRandomString() . ".{$ext}";

                $image->saveAs($file_dir . '/' . $filename);

                // the uploaded image instance
                return $filename;

            }
        }

        return false;

    }

    /**
     * Return true if attribute is changed
     *
     * @return boolean
     */
    public function isChanged($attribute){
        if($this->model->$attribute) return true;
        return false;
    }

    /**
     * Return true if checked delete value
     *
     * @return boolean
     */
    public function isDeleted($attribute){
        if($this->model->$attribute == 'delete'){
            $this->storage[$attribute] = 'delete';
            return true;
        }
        if(isset($this->storage[$attribute]))
            if($this->storage[$attribute] == 'delete') return true;
        return false;
    }

    /**
     * Process deletion of image
     *
     * @return boolean the status of deletion
     */
    public function deleteImage() {
        $file = $this->getImageFile();

        // check if file exists on server
        if (empty($file) || !file_exists($file)) {
            return false;
        }

        // check if uploaded file can be deleted on server
        if (!unlink($file)) {
            return false;
        }

        // if deletion successful, reset your file attributes
        $this->logo = null;
        $this->filename = null;

        return true;
    }

    /**
     * fetch stored image url
     * @return string
     */
    public function getImageUrl($attribute)
    {
        if(isset($this->model->$attribute)) {

            $uploadDir = $this->uploadUrl;
            if(isset($this->model->id))
                if($this->model->id > 0)
                    $uploadDir .= '/' . $this->model->id;

            return $uploadDir . '/' . $this->model->$attribute;
        }
    }

    /**
     * fetch stored image path
     * @return string
     */
    public function getImagePath($attribute)
    {
        if(isset($this->model->$attribute)) {

            $uploadDir = $this->uploadPath;
            if(isset($this->model->id))
                if($this->model->id > 0)
                    $uploadDir .= '/' . $this->model->id;

            if(file_exists($uploadDir . '/' . $this->model->$attribute))
                if(is_file($uploadDir . '/' . $this->model->$attribute))
                        return $uploadDir . '/' . $this->model->$attribute;

        }
        return false;
    }

    /**
     * delete image path
     * @return boolean
     */
    public function deleteImagePath()
    {
        function removeDirectory($dir) {
            if($objs = scandir($dir))
                foreach($objs as $obj)
                    if($obj <> '.' && $obj <> '..') {
                        $_path = $dir . '/' . $obj;
                        is_dir($_path) ? removeDirectory($_path) : unlink($_path);
                    }

            if(is_dir($dir)) rmdir($dir);
        }

        if($this->uploadPath) {
            $uploadDir = $this->uploadPath;
            if (isset($this->model->id))
                if ($this->model->id > 0) {
                    $uploadDir .= '/' . $this->model->id;
                    if (is_dir($uploadDir))
                        removeDirectory($uploadDir);
                }
        }

        return true;
    }

    /**
     * fetch stored image url and return <img>
     * @return string
     */
    public function getImage($attribute, $width, $height, $empty = false)
    {
        if(isset($this->model->$attribute)) {
            if($this->model->$attribute) {
                $uploadDir = $this->uploadUrl;
                if (isset($this->model->id))
                    if ($this->model->id > 0)
                        $uploadDir .= '/' . $this->model->id;

                return Html::img($uploadDir . '/' . $this->model->$attribute);
            }
        }

        if($empty) return false;
        return Html::img('https://api.fnkr.net/testimg/'. $width .'x'. $height .'/666666/000/?text=logo');
    }
}

?>