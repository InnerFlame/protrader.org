<?php
/**
 * Created by Zaitsev Oleg.
 * User: php
 * Date: 04.09.15
 * Time: 10:18
 */

namespace app\models;

use yii;
use yii\db\ActiveRecord;

class CustomActiveRecord extends ActiveRecord {

    public static $_pull = [];
    public static $cache_timeout = 3600;

//    public static function findOneRedis($condition, $key)
//    {
//        $class_name = static::className();
//
//        if(!strpos($class_name, 'User') && !strpos($class_name, 'Account')) {
//            var_dump($class_name, $condition);
//            exit;
//        }
//
//        $class_name_key = str_replace('\\', '_',  $class_name);
//
//        $output = Yii::$app->redis->get($class_name_key . ':00' . $key . '00');
//
//        if(!$output)
//        {
//            $output = parent::findOne($condition);
//
//            if($output)
//            {
//                $attributes = $output->attributes;
//                Yii::$app->redis->set($class_name_key . ':00' . $key . '00', json_encode($attributes));
//                Yii::$app->redis->expire($class_name_key . ':00' . $key . '00', self::$cache_timeout);
//            }
//
//        }else{
//
//            $model = new $class_name();
//            $_data = json_decode($output, true);
//            $model->attributes = $_data;
//
//            return $model;
//
//        }
//
//        return $output;
//
//    }

    public static function findOne($condition)
    {
        $key_condition = false;
        $output = null;

        if($condition > 0 && !is_array($condition))
        {
            $key_condition = $condition;

        }else {

            if (is_string($condition))
                $key_condition = $condition;

            if (is_array($condition))
                $key_condition = Yii::$app->customFunctions->makeStringKeyFromArr($condition);

        }

        if($key_condition) {
            if (isset(static::$_pull[$key_condition])) {
                return static::$_pull[$key_condition];
            }
//            }else{
//
//                $output = static::findOneRedis($condition, $key_condition);
//
//            }
        }

//        if(!is_object($output))
        $output = parent::findOne($condition);

        if($key_condition)
            static::$_pull[$key_condition] = $output;

        return $output;

    }

}