<?php

namespace app\models\mail;

use app\models\DefaultQuery;
use Yii;

/**
 * This is the model class for table "queue".
 *
 * @property integer $id
 * @property string $entity
 * @property integer $entity_id
 * @property string $event
 * @property integer $operated_at
 */
class Queue extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'queue';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entity', 'entity_id', 'event'], 'required'],
            [['entity_id', 'operated_at'], 'integer'],
            [['entity', 'event'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('app', 'ID'),
            'entity'      => Yii::t('app', 'Entity'),
            'entity_id'   => Yii::t('app', 'Entity ID'),
            'event'       => Yii::t('app', 'Event'),
            'operated_at' => Yii::t('app', 'Operated At'),
        ];
    }

    /**
     * @inheritdoc
     * @return QueueQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DefaultQuery(get_called_class());
    }

    public function getQueueModel()
    {
        $model = $this->entity;

        return $model::findOne($this->entity_id);
    }
}
