<?php

namespace app\models;

use app\components\Entity;
use app\modules\brokers\models\Improvement;

/**
 * This is the ActiveQuery class for [[Pages]].
 *
 * @see Pages
 */
class DefaultQuery extends \yii\db\ActiveQuery
{
    /**
     * @param $attribute
     * @param $ids
     * @return $this
     */
    public function except($attribute, $ids)
    {
        if ($ids) {
            $this->andWhere(['<>', $attribute, $ids]);
            return $this;
        }
        return $this;
    }

    /**
     * @return $this
     */
    public function isMain()
    {
        return $this->andWhere(['is_main' => Improvement::IS_MAIN]);
    }

    /**
     * @return $this
     */
    public function notDeleted()
    {
        return $this->andWhere(['deleted' => Entity::NOT_DELETED]);
    }

    /**
     * @inheritdoc
     * @return Pages[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Pages|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param $alias
     * @return $this
     */
    public function alias($alias)
    {
        return $this->andWhere(['alias' => $alias]);
    }

    /**
     * @return $this
     */
    public function enabled()
    {
        return $this->andWhere('status = 1');
    }
}