<?php

namespace app\models\trash;

use app\components\Entity;
use app\models\user\User;
use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "trash".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $entity
 * @property integer $entity_id
 * @property integer $deleted_at
 */
class Trash extends \yii\db\ActiveRecord
{
    /*
    |--------------------------------------------------------------------------
    | Constants && properties
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | Model configurations
    |--------------------------------------------------------------------------
    */

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'trash';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'entity', 'entity_id', 'deleted_at'], 'required'],
            [['user_id', 'entity', 'entity_id', 'deleted_at'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('app', 'ID'),
            'user_id'    => Yii::t('app', 'User ID'),
            'entity'     => Yii::t('app', 'Entity'),
            'entity_id'  => Yii::t('app', 'Entity ID'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * @inheritdoc
     * @return TrashQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TrashQuery(get_called_class());
    }

    /*
    |--------------------------------------------------------------------------
    | Model relations
    |--------------------------------------------------------------------------
    */

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntityCode()
    {
        $reflection = new \ReflectionClass('app\components\Entity');
        $constants = $reflection->getConstants();

        foreach ($constants as $key => $constant) {
            if ($this->entity == $constant)
                return $key;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntityModel()
    {
        $class = Entity::getEntityByType($this->entity);
        if (!$class)
            return null;

        return $class::findOne($this->entity_id);
    }

    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */

    public static function loadEntity($entity)
    {
        $model = new Trash();
        $model->user_id = Yii::$app->user->identity->id;
        $model->entity = $entity->getTypeEntity();
        $model->entity_id = $entity->id;
        $model->deleted_at = date('U');

        return $model;
    }

    public static function getGridColumns()
    {
        return [
            [
                'class'          => 'yii\grid\SerialColumn',
                'header'         => '<span>#</span>',
                'contentOptions' => [
                    'width' => 50
                ],
            ],
            [
                'attribute' => 'user_id',
                'value'     => function ($model) {
                    if (is_object($model->user))
                        return $model->user->fullname;
                },
                'format'    => 'raw',
            ],
            [
                'attribute' => 'entity',
                'value'     => function ($model) {
                    return $model->getEntityCode();
                },
                'format'    => 'raw',
            ],
            [
                'header'    => 'Title',
                'attribute' => 'entity_id',
                'value'     => function ($model) {
                    if (is_object($model->entityModel)) {
                        return $model->entityModel->getName();

                    }
                    return $model->entity . ' - ' . $model->entity_id;
                },
                'format'    => 'raw',
            ],
            [
                'attribute' => 'deleted_at',
                'format'    => 'date',
            ],
            [
                'class'    => 'app\components\grid\ActionColumnCustom',
                'template' => function ($model) {
//                    return '{view} {restore}';
                    return '{restore}';
                },
                'buttons'  => [
                    'restore' => function ($url, $model, $key, $options) {
                        $options['onclick'] = '
                            if(confirm(\'' . Yii::t('yii', 'Are you sure you want to restore?') . '\'))
                            {
                              return true;
                            } else {
                              return false;
                            }
                        ';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/trash/restore', 'id' => $model->id]);

                        return Html::a('Restore', $url, $options);
                    },
                ],
            ],
        ];
    }
}
