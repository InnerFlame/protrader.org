<?php

namespace app\models\trash;

/**
 * This is the ActiveQuery class for [[Trash]].
 *
 * @see Trash
 */
class TrashQuery extends \yii\db\ActiveQuery
{
    /**
     * @inheritdoc
     * @return Trash[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Trash|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @return $this
     */
    public function newest()
    {
        return $this->orderBy('deleted_at DESC');
    }
}
