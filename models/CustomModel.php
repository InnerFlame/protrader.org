<?php
/**
 * Created by PhpStorm.
 * User: InnerFlame
 * Date: 18.11.2015
 * Time: 17:13
 */

namespace app\models;

use app\components\ceo\Ceo;
use app\components\ceo\CeoWidget;
use app\components\Entity;
use app\models\community\Comments;
use yii;
use yii\helpers\ArrayHelper;
use app\models\user\User;

class CustomModel extends \yii\db\ActiveRecord
{

    /*
    |--------------------------------------------------------------------------
    | Constants && properties
    |--------------------------------------------------------------------------
    */

    public static $_pull = [];

    public  $avatar;

    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */


    public function beforeSave($insert)
    {
        if(!$this->hasAttribute('created_at') && !isset($this->created_at))
            return parent::beforeSave($insert);

        if (isset($this->published) && is_string($this->created_at))
            $this->published = strtotime($this->published);

        if (isset($this->published_at) && is_string($this->created_at))
            $this->published_at = strtotime($this->published_at);

        if (isset($this->birth_date) && is_string($this->created_at))
            $this->birth_date = strtotime($this->birth_date);


//        var_dump($this->created_at);
//        echo '<br>';
//        echo '<br>';
//        $newTime = strtotime($this->created_at);
//        var_dump($newTime);
//
//        echo '<br>';
//        echo '<br>';

//        $time = (int)$this->created_at;?

//        var_dump($time);
//        $time = $newTime;
//        var_dump($time);

        if(isset($this->created_at) && preg_match('#\D+#', $this->created_at) ) {
            $this->created_at = strtotime($this->created_at);
        }
//
//        var_dump($this->created_at);
//        echo '<br>';
//        echo '<hr>';

        if( isset($this->alias ) )
            $this->alias = str_replace(' ', '', Yii::$app->customFunctions->cleanText($this->alias));


        return parent::beforeSave($insert);
    }


    /**
     * added for deleting slashes at output html
     */
    public function afterFind()
    {
        parent::afterFind(); // TODO: Change the autogenerated stub


        if(is_array($this->attributes) && isset($this->attributes)){


            foreach($this->attributes as $attr => $value){

                if(isset(static::$stringAttr)) {

                    if(count(static::$stringAttr) > 0 && in_array($attr, static::$stringAttr) ){

                        $this->$attr = stripslashes($value);

                    }
                }


            }

        }
    }


    /**
     * @param mixed $condition
     * @return CustomModel
     */
    public static function findOne($condition)
    {
        $key_condition = false;
        $output = null;

        $key_condition = $condition;
        if(is_array($condition)) $key_condition = json_encode($condition);

        if($key_condition) {
            if (array_key_exists($key_condition, static::$_pull)) {
                return static::$_pull[$key_condition];
            }
        }

        $output = parent::findOne($condition);

        if($key_condition)
            static::$_pull[$key_condition] = $output;

        return $output;

    }


    public static function getList($except_id = null)
    {
        $db_data = self::find()->notDeleted()->all();
        $models = ArrayHelper::map($db_data, 'id', 'title');

        if ($except_id)
            unset($models[$except_id]);

        return $models;
    }


    public static function getTitleById($id)
    {
        $model = self::findOne($id);

        if(is_object($model)) return $model->title;
    }


    public function getCeo()
    {
        return null;
    }


    /**
     * TODO Тут происходит что-то непонятное. CustomModel генерирует Entity
     * @return \yii\db\ActiveQuery
     */
    public function countComments(){
        $model = new self();
        return count(
            Comments::find()
                ->select('id')
                ->where([
                    'entity' => $model->getTypeEntity(),
                    'entity_id' => $model->id,
                ])
                ->all()
        );

    }

    public function getPictureLink($id = null, $params_attr = 'userUploadPath', $attr = 'pictures')
    {

        $id = $id > 0 ? $id : $this->id;

        if ($id > 0 && $this->$attr)
            return Yii::$app->params[$params_attr] . '/' . $id . '/' . $this->$attr;


        return 'http://placehold.it/101x101';
    }



    public static function getSafeId()
    {
        if (is_object(Yii::$app->user))
            if (is_object(Yii::$app->user->identity))
                return Yii::$app->user->identity->id;

        return null;
    }

    /**
     * @inheritdoc
     * @return FrmCategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DefaultQuery(get_called_class());
    }
}