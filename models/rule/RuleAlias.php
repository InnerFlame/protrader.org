<?php
/**
 * Created by PhpStorm.
 * @author: Roman Budnitsky
 * @date: 18.11.14 15:23
 * @copyright Copyright (c) 2014 PFSOFT LLC
 *
 * Class Country
 */

namespace app\models\rule;

use app\models\account\Account;
use app\models\department\Department;
use yii;
use yii\base\Model;
use app\models\ConstantBase;
use app\models\report\Report;

class RuleAlias extends ConstantBase {

    public static $case_rules = [
        self::RULE_MANAGE_EXACT_ACCOUNT => 'Account',
        self::RULE_VIEW_EXACT_ACCOUNT => 'Account',
        self::RULE_MANAGE_DEPARTMENT_SALARY_ACTIVITY => 'Department',
        self::RULE_VIEW_DEPARTMENT_SALARY_ACTIVITY => 'Department',
        self::RULE_MANAGE_EXACT_REPORT  => 'Report',
        self::RULE_VIEW_EXACT_REPORT => 'Report',
    ];

    public static function getCaseRulesArr(){
        return array_keys(self::$case_rules);
    }

    // Report Rules
    const RULE_MANAGE_REPORT = 'manageAllReports';
    const RULE_MANAGE_EXACT_REPORT = 'manageExactReport';
    const RULE_VIEW_EXACT_REPORT = 'viewExactReport';

    // Account Rules
    const RULE_VIEW_ACCOUNTS = 'viewAccounts';
    const RULE_VIEW_ALL_ACCOUNTS = 'viewAllAccounts';
    const RULE_MANAGE_ACCOUNTS = 'manageAccounts';
    const RULE_CREATE_ACCOUNTS = 'createAccounts';
    const RULE_VIEW_EXACT_ACCOUNT = 'viewExactAccount';
    const RULE_MANAGE_EXACT_ACCOUNT = 'manageExactAccount';
    const RULE_VIEW_ACCOUNTS_ACTIVITY = 'viewAccountsActivity';
    const RULE_MANAGE_ACCOUNTS_ACTIVITY = 'manageAccountsActivity';

    // Salary Rules
    const RULE_VIEW_SALARY = 'viewSalary';
    const RULE_MANAGE_SALARY = 'manageSalary';
    const RULE_MANAGE_DEPARTMENT_SALARY_ACTIVITY = 'manageDepartmentSalary';
    const RULE_VIEW_DEPARTMENT_SALARY_ACTIVITY = 'viewDepartmentSalary';

    // Staff Rules
//    const RULE_VIEW_STAFF = 'viewStaff';
//    const RULE_MANAGE_STAFF = 'manageStaff';

    // Staff Rules
    const RULE_VIEW_CREDITS = 'viewCredits';
    const RULE_MANAGE_CREDITS = 'manageCredits';

    //Employees Rules
    const RULE_VIEW_EMPLOYEES = 'viewEmployees';
    const RULE_MANAGE_EMPLOYEES = 'manageEmployees';

    //COMMENTS Rules
    const RULE_VIEW_COMMENTS = 'viewComments';
    const RULE_MANAGE_COMMENTS = 'manageComments';

    //Vacation Rules
   // const RULE_VIEW_VACATION = 'viewVacation';
   // const RULE_MANAGE_VACATION = 'manageVacation';
    const RULE_VIEW_ABSENCE   = 'viewAbsence';
    const RULE_MANAGE_ABSENCE = 'manageAbsence';

    // Inventory Rules
    const RULE_VIEW_INVENTORY = 'viewInventory';
    const RULE_MANAGE_INVENTORY = 'manageInventory';

    // Cabinet Rules
    const RULE_MANAGE_CABINET = 'manageCabinet';

    // Requests Rules
    const RULE_VIEW_REQUESTS = 'viewRequests';
    const RULE_OUTPUT_REQUESTS = 'outputRequests';
    const RULE_MANAGE_REQUESTS = 'manageRequests';


    public static function getChoice($alias, $params = [])
    {
        if($alias == self::RULE_MANAGE_EXACT_ACCOUNT
            || $alias == self::RULE_VIEW_EXACT_ACCOUNT)
        {
            return
                    Yii::$app->view->renderFile(
                            Yii::getAlias('@views') . '/filters/drop_down_list.twig',
                            array_merge([
                                    'listArr' => Account::getAccountList(),
                            ], $params)
                    );
        }

        if($alias == self::RULE_MANAGE_DEPARTMENT_SALARY_ACTIVITY
            || $alias == self::RULE_VIEW_DEPARTMENT_SALARY_ACTIVITY)
        {
            return
                Yii::$app->view->renderFile(
                    Yii::getAlias('@views') . '/filters/drop_down_list.twig',
                    array_merge([
                        'listArr' => Department::getDepartmentList(),
                    ], $params)
                );
        }

        if($alias == self::RULE_MANAGE_EXACT_REPORT
            || $alias == self::RULE_VIEW_EXACT_REPORT)
        {
            return
                Yii::$app->view->renderFile(
                    Yii::getAlias('@views') . '/filters/drop_down_list.twig',
                    array_merge([
                        'listArr' => Report::getReportList(),
                    ], $params)
                );
        }

        return null;
    }

    public static function getChoiceModels($alias, $params = [])
    {
        if($alias == self::RULE_MANAGE_EXACT_ACCOUNT
            || $alias == self::RULE_VIEW_EXACT_ACCOUNT)
        {
            return Account::getAccountList();
        }

        if($alias == self::RULE_MANAGE_DEPARTMENT_SALARY_ACTIVITY
            || $alias == self::RULE_VIEW_DEPARTMENT_SALARY_ACTIVITY)
        {
            return Department::getDepartmentList();
        }

        if($alias == self::RULE_MANAGE_EXACT_REPORT
            || $alias == self::RULE_VIEW_EXACT_REPORT)
        {
            return Report::getReportList();
        }

        return null;
    }

} 