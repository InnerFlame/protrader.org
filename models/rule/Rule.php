<?php
/**
 * Created by PhpStorm.
 * User: Zaitsev Oleg
 * Date: 26.06.15
 * Time: 10:27
 *
 * Class License
 * @package app\modules\knowledgebase\models
 */


namespace app\models\rule;

use app\models\report\ReportHelper;
use app\models\user\UserRoleRule;
use yii;
use yii\db\ActiveRecord;
use yii\db\ActiveQuery;
use yii\helpers\Html;
use yii\behaviors\TimestampBehavior;
use app\components\MultiSelectCustom;
use app\models\user\User;
use app\models\user\UserRole;
use app\models\user\UserRules;
use app\models\department\Department;
use app\models\TypeList;
use app\models\CustomActiveRecord;

/**
 * This is the model class for table "vacation"
 *
 * @property integer $id
 * @property integer $parent
 * @property integer $type_id
 * @property string $name
 * @property string $alias
 * @property string $data
 * @property string $comment
 * @property integer $created_at
 *
 */

class Rule extends CustomActiveRecord {

    public static $_pull = [];

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'created_at',
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'alias'], 'required'],
            [['created_at', 'parent'], 'integer'],
            [['name'], 'string', 'max' => 128],
            [['alias'], 'string', 'max' => 64],
            [['type_id'], 'string', 'max' => 32],
            [['comment'], 'string', 'max' => 255],
            [['data'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function getAttributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'alias' => 'Url',
            'parent' => 'Parent',
            'type_id' => 'Type',
            'data' => 'Params',
            'comment' => 'Comment',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%rule}}';
    }

    /**
     * Before Save commands
     * @params boolean $insert
     *
     * @return mixed
     *
     */
    public function beforeSave($insert)
    {
        $this->comment = Yii::$app->customFunctions->cleanText($this->comment);
        $this->name = Yii::$app->customFunctions->cleanText($this->name);
        $this->alias = Yii::$app->customFunctions->cleanText($this->alias);

        if(isset($_POST['Params'])) $this->data = json_encode($_POST['Params']);

        return parent::beforeSave($insert);
    }

    /**
     * After Save commands
     * @params boolean $insert
     * @params $mixed changedAttributes
     *
     * @return mixed
     *
     */
    public function afterSave($insert, $changedAttributes)
    {
        return parent::afterSave($insert, $changedAttributes);
    }

    public static function getRuleList()
    {
        $models = Rule::find()->groupBy('alias')->all();

        return yii\helpers\ArrayHelper::map($models, 'id', 'name');
    }

    public static function getModelRuleList()
    {
        return $models = Rule::find()->all();
    }

    public function getRoleRules()
    {
        return $this->hasMany(UserRoleRule::className(), ['rule_id' => 'id']);
    }

    public function getUserRules()
    {
        return $this->hasMany(UserRules::className(), ['rule_id' => 'id']);
    }

    public function getTypeList()
    {
        return TypeList::getTypeList('rule', 'constants');
    }

    public function getRuleAliasList($except = false)
    {
        $rules = RuleAlias::getConstantList();

        if($except) {
            foreach ($except as $_except) {
                foreach ($rules as $key => $_rule) {

                    if ( $_except['alias'] == $key ){
                        unset($rules[$key]);
                    }
                }
            }
            //sort
            $sort = array();
            foreach ($rules as $key => $row)
                $sort[$key] = $row;
            array_multisort($sort, SORT_ASC, $rules);

        }

        return is_array($rules) ? $rules : array();
    }

    public function getDataJson()
    {
        if(is_array($this->data)) return json_encode($this->data);
        return '\'' . $this->data . '\'';
    }

    public function getParentName()
    {
        if($this->parent > 0)
        {
            $rule = Rule::findOne((int) $this->parent);

            if(is_object($rule))
            {
                return $rule->name;
            }

        }

        return null;
    }

    public function getTypeName()
    {
        if($this->type_id > 0)
        {
            $type = TypeList::find()->where(['constants' => (int) $this->type_id])->one();

            if(is_object($type))
            {
                return $type->name;
            }

        }

        return null;
    }

    public static function getRoleRulesModels($role_id)
    {
        if($role_id > 0)
        {
           return Rule::find()->with('roleRules')
                ->leftJoin('role_rules', 'rule_id = id')
                ->where(['role_id' => (int) $role_id])
                ->orderBy('type_id ASC, parent ASC, name ASC')->all();
        }

        return [];

    }

    public static function getUserRulesModels($user_id)
    {
        if($user_id > 0)
        {
            return Rule::find()->with('userRules')
                ->leftJoin('user_rules', 'rule_id = id')
                ->where(['user_id' => (int) $user_id])
                ->orderBy('type_id ASC, parent ASC, name ASC')->all();
        }

        return [];

    }

    /**
     * @param $role_id
     * @param null $rule_id
     * @return array
     */
    public function  getRoleListExceptRoleID($role_id, $rule_id = null)
    {
        $models = Rule::find()->with('roleRules')->orderBy('alias ASC')->distinct('alias')
            ->all();

        foreach ($models as $key => $_model) {

            $_model['name'] .= $_model['data'];

            if(in_array($_model->alias, [RuleAlias::RULE_MANAGE_EXACT_REPORT,
                RuleAlias::RULE_VIEW_EXACT_REPORT, RuleAlias::RULE_MANAGE_EXACT_ACCOUNT,
                RuleAlias::RULE_VIEW_EXACT_ACCOUNT]))
            {

                unset($models[$key]);

            }else {

                if ($_model->roleRules) {
                    $is_unset = false;
                    foreach ($_model->roleRules as $role_rule)
                        if ($role_rule->role_id == $role_id)
                            if ($role_rule->rule_id <> $rule_id)
                                $is_unset = true;

                    if ($is_unset) unset($models[$key]);

                }

            }
        }
        return yii\helpers\ArrayHelper::map($models, 'id', 'name');

    }

    public function  getRoleListExceptUserID($user_id, $rule_id = null)
    {
        $models = Rule::find()
                            ->with('userRules')
                            ->orderBy('alias ASC')
                            ->all();

        foreach($models as $key => $_model) {

            $_model['name'] .= !empty($_model['data']) ? $_model['data'] : '';

            if($_model->alias == RuleAlias::RULE_MANAGE_EXACT_REPORT ||
                $_model->alias == RuleAlias::RULE_VIEW_EXACT_REPORT ){

                unset($models[$key]);

            }else {

                if ($_model->userRules) {
                    $is_unset = false;

                    foreach ($_model->userRules as $user_rule)

                        if ($user_rule->user_id == $user_id) {
                            $rule = Rule::findOne($user_rule->rule_id);

                            if (is_object($rule))
                                if (!in_array($rule->alias, RuleAlias::getCaseRulesArr()))
                                    if ($user_rule->rule_id <> $rule_id)
                                        $is_unset = true;
                        }

                    if ($is_unset) unset($models[$key]);

                }

            }

        }
        return yii\helpers\ArrayHelper::map($models, 'id', 'name');

    }

    public static function mergeRoleAndUserRules($role_rules, $user_rules)
    {
        if(count($user_rules))
            foreach($user_rules as $_exact_rule)
            {
                if(isset($_exact_rule['alias']))
                {
                    $is_new = true;

                    if(count($role_rules) > 0)
                        foreach($role_rules as $key => $_general_rule)
                            if(isset($_general_rule['alias']))
                            {
                                if($_general_rule['alias'] == $_exact_rule['alias']
                                && $_general_rule['data'] == $_exact_rule['data'])
                                {
                                    $role_rules[$key] = $_exact_rule;
                                    $is_new = false;
                                }
                            }

                    if($is_new){
                        $role_rules[] = $_exact_rule;
                    }

                }
            }

        return $role_rules;

    }

    public function getChildAliasArr($rule_id, $rules)
    {
        $childs = [];

        if($rule_id > 0)
            if(count($rules) > 0)
                foreach($rules as $_rule){
                    if(isset($_rule->parent))
                        if($rule_id == $_rule->parent)
                            if(!in_array($_rule->alias, RuleAlias::getCaseRulesArr()))
                                $childs[] = $_rule->alias;
                }

        return $childs;

    }

    public static function getGridColumns()
    {
        return [
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => '<span>#</span>',
                'contentOptions' => [
                    'width' => 70
                ],
            ],
            [
                'attribute' => 'type_id',
                'format' => 'raw',
                'value' => function($model){
                    return $model->getTypeName();
                },
            ],
            [
                'attribute' => 'parent',
                'header' => Yii::t('app','Parent'),
                'format' => 'raw',
                'value' => function($model){
                    return $model->getParentName();
                },
            ],
            [
                'attribute' => 'name',
                'header' => Yii::t('app','Name'),
                'format' => 'raw',
            ],
            [
                'attribute' => 'alias',
                'header' => Yii::t('app','Alias'),
                'format' => 'raw',
            ],
            self::getOnlyDataGridColumn(),
            [
                'attribute' => 'comment',
                'header' => Yii::t('app','comment'),
                'format' => 'raw',
            ],
//            [
//                'attribute' => 'created_at',
//                'format' => 'date',
//            ],
            [
                'class' => 'app\components\grid\ActionColumnCustom',
                'template' => function($model) {
                    return '{update} {delete}';
                },
                'buttons' => [
                    'update' => function($url, $model, $key, $options){

                        $url = Yii::$app->urlManager->createUrl(['cabinet/edit-rule',
                            'id' => $model->id, 'parent' => $model->parent,'alias'=>$model->alias]);
                        return Html::a('Edit', $url, $options);
                    },

                    'delete' => function($url, $model, $key, $options){
                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' rule-action';
                        $options['onclick'] = 'return false;';
                        $options['data']['id'] = $model->id;
                        $options['data']['custom-confirm'] = Yii::t('yii', 'Are you sure you want to delete this Rule?');

                        $url = Yii::$app->urlManager->createUrl(['cabinet/delete-rule', 'id' => $model->id]);
                        return Html::a('Delete', $url, $options);
                    },

                ],
            ],
        ];
    }

    public static function getRoleRulesGridColumns($role_id = null)
    {
        return [
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => '<span>#</span>',
                'contentOptions' => [
                    'width' => 70
                ],
            ],
            [
                'attribute' => 'type_id',
                'format' => 'raw',
                'value' => function($model){
                    return $model->getTypeName();
                },
            ],
            [
                'attribute' => 'parent',
                'header' => Yii::t('app','Parent'),
                'format' => 'raw',
                'value' => function($model){
                    return $model->getParentName();
                },
            ],
            [
                'attribute' => 'name',
                'header' => Yii::t('app','Name'),
                'format' => 'raw',
            ],
            [
                'attribute' => 'alias',
                'header' => Yii::t('app','Alias'),
                'format' => 'raw',
            ],
            [
                'header' => 'Status',
                'header' => Yii::t('app','Status'),
                'format' => 'raw',
                'value' => function($model) use($role_id){
                    $role_rule = UserRoleRule::find()->where([
                        'role_id' => $role_id,
                        'rule_id' => $model->id,
                    ])->one();

                    if(is_object($role_rule))
                        return $role_rule->getStatusName();

                },
            ],
            self::getOnlyDataGridColumn(),
            [
                'attribute' => 'comment',
                'header' => Yii::t('app','Comment'),
                'format' => 'raw',
            ],
//            [
//                'attribute' => 'created_at',
//                'format' => 'date',
//            ],
            [
                'class' => 'app\components\grid\ActionColumnCustom',
                'template' => function($model) {
                    return '{update} {delete}';
                },
                'buttons' => [
                    'update' => function($url, $model, $key, $options) use($role_id)
                    {
                        $url = Yii::$app->urlManager->createUrl([
                            'cabinet/edit-role-rule',
                            'role_id' => $role_id,
                            'rule_id' => $model->id,
                        ]);

                        return Html::a('Edit', $url, $options);
                    },

                    'delete' => function($url, $model, $key, $options) use($role_id)
                    {
                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' rule-action';
                        $options['onclick'] = 'return false;';
                        $options['data']['id'] = $model->id;
                        $options['data']['custom-confirm'] = Yii::t('yii', 'Are you sure you want to delete this Rule?');

                        $url = Yii::$app->urlManager->createUrl([
                            'cabinet/delete-role-rule',
                            'role_id' => $role_id,
                            'rule_id' => $model->id,
                        ]);

                        return Html::a('Delete', $url, $options);
                    },

                ],
            ],
        ];
    }

    public static function getUserRulesGridColumns($user_id = null)
    {
        return [
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => '<span>#</span>',
                'contentOptions' => [
                    'width' => 70
                ],
            ],
            [
                'attribute' => 'type_id',
                'format' => 'raw',
                'value' => function($model){
                    return $model->getTypeName();
                },
            ],
            [
                'attribute' => 'parent',
                'header' => Yii::t('app','Parent'),

                'format' => 'raw',
                'value' => function($model){
                    return $model->getParentName();
                },
            ],
            [
                'attribute' => 'name',
                'header' => Yii::t('app','Name'),
                'format' => 'raw',
            ],
            [
                'attribute' => 'alias',
                'header' => Yii::t('app','Alias'),
                'format' => 'raw',
            ],
            [
                'header' => Yii::t('app','Status'),
                'format' => 'raw',
                'value' => function($model) use($user_id){
                    $user_rule = UserRules::find()->where([
                        'user_id' => $user_id,
                        'rule_id' => $model->id,
                    ])->one();

                    if(is_object($user_rule))
                        return $user_rule->getStatusName();

                },
            ],
            self::getOnlyDataGridColumn(),
            [
                'attribute' => 'comment',
                'header' => Yii::t('app','Comment'),
                'format' => 'raw',
            ],
            [
                'class' => 'app\components\grid\ActionColumnCustom',
                'template' => function($model) {
                    return '{update} {delete}';
                },
                'buttons' => [
                    'update' => function($url, $model, $key, $options) use($user_id)
                    {
                        $url = Yii::$app->urlManager->createUrl([
                            'cabinet/edit-user-rule',
                            'user_id' => $user_id,
                            'rule_id' => $model->id,
                        ]);

                        return Html::a('Edit', $url, $options);
                    },

                    'delete' => function($url, $model, $key, $options) use($user_id)
                    {
                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' rule-action';
                        $options['onclick'] = 'return false;';
                        $options['data']['id'] = $model->id;
                        $options['data']['custom-confirm'] = Yii::t('yii', 'Are you sure you want to delete this Rule?');

                        $url = Yii::$app->urlManager->createUrl([
                            'cabinet/delete-user-rule',
                            'user_id' => $user_id,
                            'rule_id' => $model->id,
                        ]);

                        return Html::a('Delete', $url, $options);
                    },

                ],
            ],
        ];
    }

    /**
     * for grid generate column data for 3 different grid
     * (getUserRulesGridColumns, role rule, rule list)
     * @return array
     */
    public static function  getOnlyDataGridColumn()
    {
       return [
            'attribute' => 'data',
            'header' => Yii::t('app','Report'),
            'format' => 'raw',
            'value' => function($model) {

                $response = isset($model->data) ? json_decode($model->data) : '';

                if (isset(RuleAlias::$case_rules[$model->alias]))
                {
                    if(is_object($response))
                        $response_id = $response->data_id;

                    $model_name =  RuleAlias::$case_rules[$model->alias];
                    //echo '<pre>';print_r();exit;

                    if(ReportHelper::getModelClassName($model_name)){

                        $class_name = ReportHelper::getModelClassName($model_name);
                        $response = $class_name::getModelCaseName($response_id);
                    }


                }

                return $response;
            }

        ];
    }

}