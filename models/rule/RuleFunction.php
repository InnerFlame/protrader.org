<?php

namespace app\models\rule;

use yii;
use yii\base\Model;

class RuleFunction extends Model {

    /**
     * Custom rules function with account_id request parameter.
     * When user have RULE_MANAGE_ACCOUNTS or RULE_MANAGE_EXACT_ACCOUNT permission.
     *
     * return @bool
     */
    public static function customCheckManageAccountActivityRule()
    {
        if(Yii::$app->user->can(RuleAlias::RULE_MANAGE_ACCOUNTS))
            return true;

        if(isset(Yii::$app->request->queryParams['account_id']))
            if(Yii::$app->user->can(RuleAlias::RULE_MANAGE_EXACT_ACCOUNT, ['data_id' => Yii::$app->request->queryParams['account_id']]))
                return true;

        return false;
    }

    /**
     * Custom rules function with id request parameter.
     * When user have RULE_MANAGE_ACCOUNTS or RULE_MANAGE_EXACT_ACCOUNT permission.
     *
     * return @bool
     */
    public static function customCheckManageAccountRule()
    {
        if(Yii::$app->user->can(RuleAlias::RULE_MANAGE_ACCOUNTS))
            return true;

        if(isset(Yii::$app->request->queryParams['id']))
            if(Yii::$app->user->can(RuleAlias::RULE_MANAGE_EXACT_ACCOUNT, ['data_id' => Yii::$app->request->queryParams['id']]))
                return true;

        return false;
    }

} 