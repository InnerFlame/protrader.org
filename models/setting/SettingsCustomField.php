<?php

namespace app\models\setting;

use Yii;
use app\models\CustomModel;
use app\models\abstractes\AbstractCustomFields;

/**
 * This is the model class for table "settings_custom_field".
 *
 * @property integer $id
 * @property string $name
 * @property string $alias
 * @property string $data
 *
 * @property SettingsCustomValue $settingsCustomValue
 */
class SettingsCustomField extends AbstractCustomFields
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'settings_custom_field';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSettingsCustomValue()
    {
        return $this->hasOne(SettingsCustomValue::className(), ['field_id' => 'id']);
    }

    public function getData()
    {
        return $this->hasOne(SettingsCustomValue::className(), ['field_id' => 'id']);
    }
}