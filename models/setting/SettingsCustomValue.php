<?php

namespace app\models\setting;

use app\models\CustomModel;
use Yii;

/**
 * This is the model class for table "settings_custom_value".
 *
 * @property integer $field_id
 * @property string $model
 *
 * @property SettingsCustomField $field
 */
class SettingsCustomValue extends CustomModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'settings_custom_value';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['field_id'], 'required'],
            [['field_id'], 'integer'],
            [['value'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'field_id' => 'Field ID',
            'value'    => 'Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getField()
    {
        return $this->hasOne(SettingsCustomField::className(), ['id' => 'field_id']);
    }

    /**
     * @param null $field_id
     * @param null $value
     * @return bool
     */
    public function saveIdFieldValue($field_id = null, $value = null)
    {
        $model = SettingsCustomValue::find()->where(['field_id' => $field_id])->one();

        if (!is_object($model))
            $model = new SettingsCustomValue();

        $model->field_id = $field_id;
        $model->value = Yii::$app->customFunctions->cleanText($value);

        if ($model->save()) {
            Yii::$app->session->setFlash('success', 'SettingsCustomValue has been saved');

            return true;
        } else {
            Yii::$app->session->setFlash('warning', 'SettingsCustomValue not saved');

            return false;
        }
    }

    /**
     * @param null $field_id
     */
    public function deleteIdFieldValue($field_id = null)
    {
        $model = SettingsCustomValue::find()->where(['field_id' => $field_id])->one();

        if (is_object($model)) {
            return $model->delete();
        }
    }

    /**
     * @param string $attribute
     * @return null
     */
    public static function getDataByAttrName($attribute = '')
    {
        $fields = SettingsCustomField::findAll(['name' => $attribute]);
        if (count($fields) > 0) {
            foreach ($fields as $key => $field) {
                $country = SettingsCustomValue::find()->where(['field_id' => $field->id])->one();
                if (is_object($country)) {
                    return $country->value;
                }
            }
        }

        return null;
    }
}