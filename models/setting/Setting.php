<?php
/**
 * Created by PhpStorm.
 * User: Zaitsev Oleg
 * Date: 26.06.15
 * Time: 10:27
 *
 * Class License
 * @package app\modules\knowledgebase\models
 */


namespace app\models\setting;

use yii;
use yii\db\ActiveRecord;
use yii\db\ActiveQuery;
use yii\helpers\Html;
use yii\behaviors\TimestampBehavior;
use app\components\MultiSelectCustom;
use app\models\user\User;
use app\models\department\Department;
use app\models\Constants;

/**
 * This is the model class for table "type_list"
 *
 * @property integer $id
 * @property string $name
 * @property string $table_name
 * @property string $constants
 * @property string $data
 *
 */

class Setting extends ActiveRecord
{

    public static $_pull = [];

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'data'], 'required'],
            [['name'], 'string', 'max' => 128],
            [['constants', 'data', 'table_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function getAttributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'constants' => 'Constants',
            'table_name' => 'Table',
            'data' => 'Value',
        ];
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%settings}}';
    }

    /**
     * Before Save commands
     * @params boolean $insert
     *
     * @return mixed
     *
     */
    public function beforeSave($insert)
    {
        $this->name = Yii::$app->customFunctions->cleanText($this->name);

        return parent::beforeSave($insert);
    }

    /**
     * After Save commands
     * @params boolean $insert
     * @params $mixed changedAttributes
     *
     * @return mixed
     *
     */
    public function afterSave($insert, $changedAttributes)
    {
        return parent::afterSave($insert, $changedAttributes);
    }

    public function getSettingByConstants($constants)
    {
        if($constants > 0)
        {
            $setting = Setting::findOne(['constants' => (int)$constants]);
            if(isset($setting->data)) return $setting->data;
        }

        return false;

    }

    public static function getGridColumns($table_name)
    {
        return [
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => '<span>#</span>',
                'contentOptions' => [
                    'width' => 70
                ],
            ],
            [
                'attribute' => 'name',
                'header' => Yii::t('app', 'Years'),
                'format' => 'raw',
            ],
            [
                'attribute' => 'constants',
                'format' => 'raw',
                'value' => function($model){
                    return Constants::getConstantName($model->constants);
                },
                'visible' => false,
            ],
            [
                'attribute' => 'data',
                'header' => Yii::t('app', 'Days'),
                'format' => 'raw',
            ],
            [
                'class' => 'app\components\grid\ActionColumnCustom',
                'template' => function($model) {
                    return '{update} {delete}';
                },
                'buttons' => [
                    'update' => function($url, $model, $key, $options) use($table_name){
                        $url = Yii::$app->urlManager->createUrl([
                            'cabinet/edit-setting', 'id' => $model->id,
                            'table_name' => $table_name,
                        ]);
                        return Html::a('Edit', $url, $options);
                    },

                    'delete' => function($url, $model, $key, $options) use($table_name){
                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' setting-action';
                        $options['onclick'] = 'return false;';
                        $options['data']['id'] = $model->id;
                        $options['data']['custom-confirm'] = Yii::t('yii', 'Are you sure you want to delete this Setting?');

                        $url = Yii::$app->urlManager->createUrl([
                            'cabinet/delete-setting', 'id' => $model->id,
                            'table_name' => $table_name,
                        ]);
                        return Html::a('Delete', $url, $options);
                    },

                ],
            ],
        ];
    }

}