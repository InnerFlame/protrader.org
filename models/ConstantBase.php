<?php
/**
 * Created by PhpStorm.
 * @author: Roman Budnitsky
 * @date: 18.11.14 15:23
 * @copyright Copyright (c) 2014 PFSOFT LLC
 *
 * Class Country
 */

namespace app\models;

use yii;
use yii\base\Model;

class ConstantBase extends Model {

    public static $_pull = [];
    public static $constant_groups = [];

    public static function getConstantList($group_list = false)
    {
        if(isset(static::$_pull["{$group_list}"])) return static::$_pull["{$group_list}"];

        $reflect = new \ReflectionClass(get_called_class());

        $_list = $reflect->getConstants();

        $models = [];

        if(count($_list) > 0)
        {
            foreach($_list as $key => $code)
            {
                if(!$group_list)
                    if(!in_array($code, ['default', 'beforeValidate', 'afterValidate']))
                        $models[$code] = $key;

                if($group_list)
                    if(isset(static::$constant_groups[$group_list]))
                    {
                        if(in_array($code, static::$constant_groups[$group_list]))
                            $models[$code] = $key;
                    }

            }
        }

        static::$_pull["{$group_list}"] = $models;

        return $models;
    }

    public static function getConstantName($code)
    {
        $constants = self::getConstantList();

        if(count($constants) > 0)
            foreach($constants as $_code => $_name)
                if($_code == $code) return $_name;

    }

} 