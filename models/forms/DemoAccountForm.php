<?php

namespace app\models\forms;

use app\components\api\NotServiceAvailableException;
use app\components\api\ProtraderApi2;
use app\components\helpers\StringHelper;
use Yii;
use yii\base\Model;
use yii\helpers\Html;

class DemoAccountForm extends Model
{
    public $first_name;
    public $last_name;
    public $email;
    public $login;
    public $password;
    public $phone;
    public $country;
    public $confirm_password;
    public $balance;

    public $checkBox = 'on';

    public $message;

    public $server_message;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['email','login', 'password', 'confirm_password'], 'required'],
            [['login'], 'string', 'min' => 3, 'max' => 20],
            ['login', 'myValidate'],
            ['email', 'email', 'checkDNS' => true],
            [['first_name', 'last_name'], 'default', 'value' => 'demo'],
            [['password','confirm_password'],
                'match',//'new_password_repeat'
                'pattern' => "/^[0-9a-zA-Z]{5,16}$/",
                'message' => 'The password must contain 5 to 16 latin characters, numbers'
            ],
            ['confirm_password', 'compare', 'compareAttribute' => 'password'],
            [['balance'], 'default', 'value' => 100000],
            ['checkBox', 'safe']

        ];
    }

    /**
     * Generate random password, if the user
     * unchecked control.
     */
    public function beforeValidate()
    {
        if($this->checkBox == 'on'){
            $pass = StringHelper::randomString(7);
            $this->password = $pass;
            $this->confirm_password = $pass;
        }
        return true;
    }

    /**
     * @return bool
     */
    public function sendRequest()
    {
        $pt = new ProtraderApi2();
        if($pt->createPT3User($this)->isSuccess) {
            return true;
        }
        $this->message = $pt->createPT3User($this)->message;

        return false;
    }

    /**
     * @return bool|void
     * @throws NotServiceAvailableException
     */
    public function registerDemo()
    {
        $pt1 = new ProtraderApi2('https://champ1.protrader.org:8443/proftrading/');
        if($pt1->sever_avaliable) {
            if($pt1->createPT3UserDemo($this)->isSuccess){
                return true;
            }
            return false;
        }

        $pt2 = new ProtraderApi2('https://champ2.protrader.org:8443/proftrading/');
        if($pt2->sever_avaliable) {
            if($pt2->createPT3UserDemo($this)->isSuccess){
                return true;
            }
            return false;
        }
        throw new NotServiceAvailableException();
    }

    /**
     * Validate unique login in api service
     * @param $attribute
     * @param $params
     * @return bool
     */
    public function myValidate($attribute, $params)
    {

        $pt = new ProtraderApi2();
        if($pt->getUserInfo($this->login)->isFail){
            return true;
        }
        $login = Html::encode($this->login);
        $this->addError($attribute, "User with login {$login} was registered sucessfully");
        return false;
    }
}
