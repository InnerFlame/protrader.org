<?php
/**
 * User: Bilyk Pavel
 * Date: 01.02.2016
 */

namespace app\models\forms;

use app\models\user\UserIdentity;
use yii\base\Model;

class ChangePasswordForm extends Model
{

    public $old_password;

    public $new_password;

    public $re_password;
    CONST SCENARIO_EMPTY = 'empty';
    CONST SIGN_UP = 'sign_up';
    CONST EDIT_PROFILE = 'edit_profile';

    /**
     * @var UserIdentity current user
     */
    private $_user;

    /**
     * @return array
     */
    public function rules()
    {

        return [
            [['old_password', 'new_password', 're_password'], 'required'],
            [['old_password'], 'validatePassword'],
            [['new_password', 're_password'], 'string', 'min' => 5, 'max' => 16],
            ['re_password', 'compare', 'compareAttribute' => 'new_password'],// 'skipOnEmpty'=> true
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();

        $scenarios[self::SCENARIO_EMPTY] = ['deleted'];
        $scenarios[self::SIGN_UP] = ['new_password', 're_password'];
        $scenarios[self::EDIT_PROFILE] = ['new_password', 're_password', 'old_password'];

        return $scenarios;

    }

    public function attributeLabels()
    {
        return [
            'old_password'    => \Yii::t('app', 'Old Password'),
            'new_password'    => \Yii::t('app', 'New Password'),
            're_password'     => \Yii::t('app', 'Repeat Password'),
        ];
    }

    /**
     * Set current user for validate old password
     * @param UserIdentity $user
     */
    public function setUser(UserIdentity $user){
        $this->_user = $user;
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->_user || !$this->_user->validatePassword($this->old_password)) {
            $this->addError($attribute, 'Incorrect password.');
        }
    }
}