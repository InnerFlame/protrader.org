<?php

namespace app\models\forms;

use app\models\user\UserCustomField;
use app\models\user\UserCustomValue;
use app\models\user\UserProfile;
use yii;
use yii\base\Model;

class ProfileForm extends Model
{

    public $about_myself;
    public $born_at;
    public $social_profile;
    public $website;
    public $country;

    public static $_countries = [
        1   => "Afghanistan",
        2   => "Albania",
        3   => "Algeria",
        4   => "American Samoa",
        5   => "Angola",
        6   => "Anguilla",
        7   => "Antartica",
        8   => "Antigua and Barbuda",
        9   => "Argentina",
        10  => "Armenia",
        11  => "Aruba",
        12  => "Ashmore and Cartier Island",
        13  => "Australia",
        14  => "Austria",
        15  => "Azerbaijan",
        16  => "Bahamas",
        17  => "Bahrain",
        18  => "Bangladesh",
        19  => "Barbados",
        20  => "Belarus",
        21  => "Belgium",
        22  => "Belize",
        23  => "Benin",
        24  => "Bermuda",
        25  => "Bhutan",
        26  => "Bolivia",
        27  => "Bosnia and Herzegovina",
        28  => "Botswana",
        29  => "Brazil",
        30  => "British Virgin Islands",
        31  => "Brunei",
        32  => "Bulgaria",
        33  => "Burkina Faso",
        34  => "Burma",
        35  => "Burundi",
        36  => "Cambodia",
        37  => "Cameroon",
        38  => "Canada",
        39  => "Cape Verde",
        40  => "Cayman Islands",
        41  => "Central African Republic",
        42  => "Chad",
        43  => "Chile",
        44  => "China",
        45  => "Christmas Island",
        46  => "Clipperton Island",
        47  => "Cocos (Keeling) Islands",
        48  => "Colombia",
        49  => "Comoros",
        55  => "Costa Rica",
        56  => "Cote d'Ivoire",
        57  => "Croatia",
        58  => "Cuba",
        59  => "Cyprus",
        60  => "Czeck Republic",
        51  => "Democratic Republic of the Congo",
        61  => "Denmark",
        62  => "Djibouti",
        63  => "Dominica",
        64  => "Dominican Republic",
        65  => "Ecuador",
        66  => "Egypt",
        67  => "El Salvador",
        68  => "Equatorial Guinea",
        69  => "Eritrea",
        70  => "Estonia",
        71  => "Ethiopia",
        72  => "Europa Island",
        73  => "Falkland Islands (Islas Malvinas)",
        74  => "Faroe Islands",
        75  => "Fiji",
        76  => "Finland",
        84  => "Federated States of Micronesia",
        145 => "Former Yugoslav Republic of Macedonia",
        77  => "France",
        78  => "French Guiana",
        79  => "French Polynesia",
        80  => "French Southern and Antarctic Lands",
        81  => "Gabon",
        82  => "Gambia",
        85  => "Georgia",
        86  => "Germany",
        87  => "Ghana",
        88  => "Gibraltar",
        89  => "Glorioso Islands",
        90  => "Greece",
        91  => "Greenland",
        92  => "Grenada",
        93  => "Guadeloupe",
        94  => "Guam",
        95  => "Guatemala",
        96  => "Guernsey",
        97  => "Guinea",
        98  => "Guinea-Bissau",
        99  => "Guyana",
        100 => "Haiti",
        101 => "Heard Island and McDonald Islands",
        102 => "Holy See (Vatican City)",
        103 => "Honduras",
        104 => "Hong Kong",
        105 => "Howland Island",
        106 => "Hungary",
        107 => "Iceland",
        108 => "India",
        109 => "Indonesia",
        110 => "Iran",
        111 => "Iraq",
        112 => "Ireland",
        153 => "Isle of Man",
        115 => "Israel",

        116 => "Italy",
        117 => "Jamaica",
        118 => "Jan Mayen",
        119 => "Japan",
        120 => "Jarvis Island",
        121 => "Jersey",
        122 => "Johnston Atoll",
        123 => "Jordan",
        124 => "Juan de Nova Island",
        125 => "Kazakhstan",
        126 => "Kenya",
        127 => "Kiribati",
        132 => "Kuwait",
        133 => "Kyrgyzstan",
        134 => "Laos",
        135 => "Latvia",
        136 => "Lebanon",
        137 => "Lesotho",
        138 => "Liberia",
        139 => "Libya",
        140 => "Liechtenstein",
        141 => "Lithuania",
        142 => "Luxembourg",
        143 => "Macau",

        146 => "Madagascar",
        147 => "Malawi",
        148 => "Malaysia",
        149 => "Maldives",
        150 => "Mali",
        151 => "Malta",
        154 => "Marshall Islands",
        155 => "Martinique",
        156 => "Mauritania",
        157 => "Mauritius",
        158 => "Mayotte",
        159 => "Mexico",
        162 => "Midway Islands",
        163 => "Moldova",
        164 => "Monaco",
        165 => "Mongolia",
        166 => "Montserrat",
        167 => "Morocco",
        168 => "Mozambique",
        169 => "Namibia",
        170 => "Nauru",
        171 => "Nepal",
        172 => "Netherlands",
        173 => "Netherlands Antilles",
        174 => "New Caledonia",
        175 => "New Zealand",
        176 => "Nicaragua",
        177 => "Niger",
        178 => "Nigeria",
        179 => "Niue",
        180 => "Norfolk Island",
        128 => "North Korea",
        181 => "Northern Mariana Islands",
        182 => "Norway",
        183 => "Oman",
        184 => "Pakistan",
        185 => "Palau",
        186 => "Panama",
        187 => "Papua New Guinea",
        188 => "Paraguay",
        189 => "Peru",
        190 => "Philippines",
        191 => "Pitcaim Islands",
        192 => "Poland",
        193 => "Portugal",
        194 => "Puerto Rico",
        195 => "Qatar",
        54  => "Republic of the Cook Islands",
        196 => "Reunion",
        197 => "Romainia",
        198 => "Russia",
        199 => "Rwanda",
        200 => "Saint Helena",
        201 => "Saint Kitts and Nevis",
        202 => "Saint Lucia",
        203 => "Saint Pierre and Miquelon",
        204 => "Saint Vincent and the Grenadines",
        205 => "Samoa",
        206 => "San Marino",
        207 => "Sao Tome and Principe",
        208 => "Saudi Arabia",
        209 => "Scotland",
        210 => "Senegal",
        211 => "Seychelles",
        212 => "Sierra Leone",
        213 => "Singapore",
        214 => "Slovakia",
        215 => "Slovenia",
        216 => "Solomon Islands",
        217 => "Somalia",
        218 => "South Africa",
        219 => "South Georgia and South Sandwich Islands",
        130 => "South Korea",
        220 => "Spain",
        221 => "Spratly Islands",
        222 => "Sri Lanka",
        223 => "Sudan",
        224 => "Suriname",
        225 => "Svalbard",
        226 => "Swaziland",
        227 => "Sweden",
        228 => "Switzerland",
        229 => "Syria",
        230 => "Taiwan",
        231 => "Tajikistan",
        232 => "Tanzania",
        233 => "Thailand",
        262 => "The Gaza Strip",
        234 => "Tobago",
        235 => "Toga",
        236 => "Tokelau",
        237 => "Tonga",
        238 => "Trinidad",
        239 => "Tunisia",
        240 => "Turkey",
        241 => "Turkmenistan",
        242 => "Tuvalu",
        243 => "Uganda",
        244 => "Ukraine",
        245 => "United Arab Emirates",
        246 => "United Kingdom",
        247 => "Uruguay",
        248 => "USA",
        249 => "Uzbekistan",
        250 => "Vanuatu",
        251 => "Venezuela",
        252 => "Vietnam",
        253 => "Virgin Islands",
        254 => "Wales",
        255 => "Wallis and Futuna",
        256 => "West Bank",
        257 => "Western Sahara",
        258 => "Yemen",
        259 => "Yugoslavia",
        260 => "Zambia",
        261 => "Zimbabwe"
    ];


    public function rules()
    {
        return [

            [['social_profile', 'website'], 'string', 'max' => 64],
            [['about_myself',], 'string', 'max' => 256],
            [['born_at'],
                'match',//'new_password_repeat'
                'pattern' => '(\d{1,2}\.\d{1,2}\.\d{4})',
                'message' => 'The Birth Day must format dd.mm.yyyy'
            ],
            [['country'], 'integer'],
            [['website', 'social_profile'], 'url'],

        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'about_myself'   => Yii::t('app', 'About myself'),
            'born_at'        => Yii::t('app', 'Birth date'),
            'social_profile' => Yii::t('app', 'Profile in social networks'),
            'website'        => Yii::t('app', 'Web site'),
            'country'        => Yii::t('app', 'Country'),
        ];
    }


    public static function saveForeach($array = [], $user_id = null)
    {

        $user_id = is_null($user_id) ? Yii::$app->user->getId() : $user_id;

        foreach ($array as $attribute => $value) {

            $field_id = UserCustomField::saveField($attribute);

            if (!is_null($value))
                UserCustomValue::saveValue($field_id, $attribute, $value, $user_id);

        }
    }


    public function saveCustomFields($user_id = null)
    {
        if (is_array($this->attributes)) {
            ProfileForm::saveForeach($this->attributes, $user_id);
        }
    }


    public function getValues($user_id = null)
    {

        $user_id = is_null($user_id) ? Yii::$app->user->getId() : $user_id;

        if (count($this->attributes) > 0) {

            foreach ($this->attributes as $attribute => $value) {

                $this->$attribute = UserCustomField::getDataByAttrName($attribute, $user_id);

            }
        }

    }


    public static function saveCustomFieldsFromEauth(&$user, &$identity, $eauth_field)
    {
        //for vk
        if ($eauth_field === 'vkontakte_id') {

            //echo '<pre>'; var_dump($identity->profile);die;

            $user->username = $identity->profile['username'];
            $user->pictures = $identity->profile['pictures'];
            // $user->birth_at = $identity->profile['birth_at'];

            $profile = $user->profile;

            if (!$user->save(false)) {
                echo '<pre>';
                var_dump($user->attributes, $user->getErrors());
                die;
            }

            unset($identity->profile['username']);
            unset($identity->profile['pictures']);
            //unset($identity->profile['birth_at']);


            if (!is_object($user->profile)) {
                $profile = UserProfile::find()->where(['user_id' => $user->id])->one(); //
                if (!is_object($profile))
                    $profile = new UserProfile();
            }


            //$profile->load($identity->profile);
            $profile->user_id = $user->id;

            $profile->first_name = $identity->profile['first_name'];
            $profile->last_name = $identity->profile['last_name'];
            $profile->address = $identity->profile['address'];
            $profile->birth_date = $identity->profile['birth_at'];


            $profile->save(false);

            ProfileForm::saveForeach($identity->profile, $user->id);
        }
    }


}

?>