<?php

namespace app\models\forms;

use Yii;
use yii\base\Model;
use app\models\user\User;

/**
 * LoginForm is the model behind the login form.
 */
class FeedbackForm extends Model
{
    public $description;
    public $theme;
    public $email;
    public $website;
    public $category_id;
    public static $_category = [
        0 => 'Protrader.org website',
        1 => 'Protrader MC',
        2 => 'Brokers',
        3 => 'Partnership',
        4 => 'Common',
    ];
    /**
     * @return array the validation rules.
     */
    public function getCategoryName()
    {
        return self::$_category[$this->category_id];
    }
    public function rules()
    {
        return [
            [['theme', 'email', 'description', 'category_id'], 'required'],
            ['email', 'email'],
//            [['theme'], 'string', 'max' => 128, 'min' => 5],
            [['description'], 'string', 'max' => 255, 'min' => 5],
        ];
    }

}
