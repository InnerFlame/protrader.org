<?php
namespace app\models\forms;

use app\components\kayako\behaviors\KayakoLoginBehavior;
use app\components\mailer\CustomMailer;
use app\models\user\UserIdentity;
use yii;
use yii\base\ErrorException;
use yii\base\Model;
use app\models\user\User;

/**
 * Rcovery form
 */
class RecoveryForm extends Model
{
    public $password;
    public $confirm_password;
    public $token =  null;

    private $_user = false;

    /**
     * @inheritdoc
     */
    public function init() {

        if(isset(Yii::$app->request->queryParams['key'])){
            $this->token =  Yii::$app->request->queryParams['key'];
            $this->_user = UserIdentity::findByPasswordResetToken($this->token);

            if(!($this->_user instanceof UserIdentity))
                throw new yii\web\NotFoundHttpException('Page is not found!');

        }

        parent::init();


    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['password', 'confirm_password'], 'required'],
            [['token'], 'safe'],
            [['password', 'confirm_password'], 'required', 'when' => function($model) {
                if($model->password == $model->confirm_password) return true;
                $this->addError('password', 'Passwords must be equal.');
            }],
            [['token'], 'required', 'when' => function($model) {
                if(UserIdentity::findByPasswordResetToken($this->token)) return true;
                $this->addError('token', 'Key is not valid.');
            }],
        ];
    }

    /**
     * Attribute Labels
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'token' => 'Key',
            'password' => 'Password',
            'confirm_password' => 'Confirm Password',
        ];
    }

    /**
     * Finds user by [[email]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = UserIdentity::findByPasswordResetToken($this->token);
        }

        return $this->_user;
    }

}
