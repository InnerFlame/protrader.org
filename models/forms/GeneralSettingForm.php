<?php

namespace app\models\forms;

use app\components\customImage\CustomImage;
use app\models\setting\SettingsCustomField;
use app\models\setting\SettingsCustomValue;
use yii;
use yii\base\Model;

class GeneralSettingForm extends Model
{
    public $article;

    public $image = null;
    public $image_380_150 = null;

    public $crop = null;

    public $pictures;
    public $pic_380_150;
    public $broker_banner;
    public $feature_banner;
    public $is_subscription;
    public $live_chat;

    public $id = '';

    public function rules()
    {
        return [
            [['article'], 'required'],
            [['pictures', 'pic_380_150', 'broker_banner', 'feature_banner', 'live_chat', 'is_subscription'], 'safe']
        ];
    }

    public function init()
    {
        $this->image = new CustomImage([
            'uploadPath' => isset(Yii::$app->params['settingsFeatureUploadPath']) ? Yii::getAlias('@webroot') . Yii::$app->params['settingsFeatureUploadPath'] : '',
            'uploadUrl'  => isset(Yii::$app->params['settingsFeatureUploadPath']) ? Yii::getAlias('@web') . Yii::$app->params['settingsFeatureUploadPath'] : '',
            'model'      => $this,
        ]);

        $this->image->setValidation('pictures', [
            'extensions' => 'png,jpg',
            'maxHeight'  => 4000,
            'maxWidth'   => 4000,
            'maxSize'    => 2048000,
            'minSize'    => 1,
        ]);

        parent::init();
    }

    public static function getGridColumns()
    {
        return [
            [
                'class'          => 'yii\grid\SerialColumn',
                'header'         => '<span>#</span>',
                'contentOptions' => [
                    'width' => 50
                ],
            ],
            [
                'attribute' => 'pictures',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'articles',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'pic_380_150',
                'format'    => 'raw',
            ],
        ];
    }

    public static function getPictureUrl($attr = 'pictures', $width = null, $height = null)
    {
        $pictures = SettingsCustomValue::getDataByAttrName($attr);

        if ($pictures) {
            return (isset(Yii::$app->params['settingsFeatureUploadPath']) ? Yii::getAlias('@web') . Yii::$app->params['settingsFeatureUploadPath'] : '') . '/' . $pictures;
        }
        if ($width > 0 && $height > 0) {
            return 'http://placehold.it/' . $width . 'x' . $height;
        }

        return 'http://placehold.it/800x300';
    }

    public function saveImageParts($attribute, $image)
    {
        if (!$image->isChanged($attribute)) $this->$attribute = '';

        if ($image->isDeleted($attribute)) {
            $this->$attribute = '';
            $field = SettingsCustomField::getFieldByAlias($attribute);
            if (is_object($field)) {
                SettingsCustomValue::deleteIdFieldValue($field->id);
            }
        }

        if ($image->getUploadedFile($attribute)) {
            $this->$attribute = $image->uploadImage($attribute, false);
            if ($this->$attribute) {
                $field = SettingsCustomField::getFieldByAlias($attribute);
                if (is_object($field)) {
                    SettingsCustomValue::saveIdFieldValue($field->id, $this->$attribute);
                }
            }
        }
    }

    public function saveCustomFields($_attribute = null, $image = null)
    {
        if (is_string($_attribute)) {
            if ($image === null)
                $image = $this->image;
            //$this->saveImageParts('pictures', $this->image);
            $this->saveImageParts($_attribute, $image);
            // $this->saveImageParts('pic_380_150', $this->image_380_150);
        }

        if ($this->article > 0) {
            $field = SettingsCustomField::getFieldByAlias('article');
            if (is_object($field)) {
                SettingsCustomValue::saveIdFieldValue($field->id, $this->article);
            }
        }
    }
}

?>