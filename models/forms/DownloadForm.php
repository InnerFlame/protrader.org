<?php

namespace app\models\forms;

use app\components\ceo\Ceo;
use app\models\Constants;
use Yii;
use yii\base\Model;
use app\models\user\User;

/**
 * LoginForm is the model behind the login form.
 */
class DownloadForm extends User
{
   // public $email;
    public  $checkbox;
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['email', 'required'],
            ['email', 'email', 'message' => 'Incorrect e-mail address'],
            ['checkbox', 'safe'],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => 'Your e-mail',
            'checkbox'           => 'Register me',
        ];
    }

}
