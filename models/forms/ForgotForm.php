<?php
namespace app\models\forms;

use app\components\kayako\behaviors\KayakoLoginBehavior;
use app\components\mailer\CustomMailer;
use app\models\user\UserIdentity;
use Yii;
use yii\base\ErrorException;
use yii\base\Model;
use app\models\user\User;

/**
 * Frogot form
 */
class ForgotForm extends Model
{
    public $email;

    private $_user = false;

    public function init() {
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // email is required
            ['email', 'required'],
            ['email', 'email'],
        ];
    }

    /**
     * Finds user by [[email]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = UserIdentity::findByUsernameOrEmail($this->email);
        }

        return $this->_user;
    }

    /**
     * Send recovery password mail
     *
     * @return boolean
     */
    public function SendRecoveryMail(){
        $userIdentity = $this->_user;
        if($userIdentity instanceof  UserIdentity){
            $userIdentity->generatePasswordResetToken();
            $userIdentity->save();

            if(CustomMailer::send($this->email, 'recovery-password.twig',
                [
                    'get_link' => Yii::$app->urlManager->createAbsoluteUrl(['user/recovery','key'=> $userIdentity->password_reset_token]),
                    'token' => $userIdentity->password_reset_token,
                ]))
                        return true;

        }

        return false;
    }

}
