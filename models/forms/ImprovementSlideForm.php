<?php

namespace app\models\forms;

use app\models\setting\SettingsCustomField;
use app\models\setting\SettingsCustomValue;
use yii;
use yii\base\Model;
use yii\web\UploadedFile;
use app\models\community\Comments;
use app\components\customImage\CustomImage;

class ImprovementSlideForm extends Model
{


    public $image = null;


    public $crop = null;

    public $id;

    public $pictures;



    public function rules()
    {
        return [
            [['pictures',], 'safe']
        ];
    }



    public function init()
    {
        $this->image = new CustomImage([
            'uploadPath' => isset(Yii::$app->params['settingsFeatureUploadPath']) ?
                Yii::getAlias('@webroot') . Yii::$app->params['settingsFeatureUploadPath'] : '',
            'uploadUrl' => isset(Yii::$app->params['settingsFeatureUploadPath']) ?
                Yii::getAlias('@web') . Yii::$app->params['settingsFeatureUploadPath'] : '',
            'model' => $this,
        ]);


        $this->image->setValidation('pictures', [
            'extensions' => 'png,jpg',
            'maxHeight' => 4000,
            'maxWidth' => 4000,
            'maxSize' => 2048000,
            'minSize' => 1,
        ]);

        parent::init();
    }




    public function saveImageParts($attribute,$image)
    {
        if(!$image->isChanged($attribute)) $this->$attribute = '';

        if($image->isDeleted($attribute))
        {
            $this->$attribute = '';

        }


        if ($image->getUploadedFile($attribute))
        {
            $this->$attribute = $image->uploadImage($attribute);

        }
    }




    public function saveCustomFields()
    {
        $this->saveImageParts('pictures', $this->image);
    }

}

?>