<?php
namespace app\models\forms;

use Yii;

use app\models\user\User;

/**
 * SignUp form
 */
class SignUp extends User
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'required'],
            ['email', 'unique'],
            ['email', 'email'],
        ];
    }

}
