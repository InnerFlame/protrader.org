<?php

namespace app\models\forms;

use Yii;
use yii\helpers\Html;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use app\models\user\User;
use app\models\user\UserIdentity;
use app\models\department\Department;
use app\models\user\UserRole;
use app\models\rule\Rule;
use app\models\rule\RuleAlias;
use app\models\user\UserRoleRule;
use app\models\user\UserRules;

/**
 * ContactForm is the model behind the contact form.
 */
class PermissionForm extends Model
{
    public $active_user_list;
    public $active_role_list;
    public $admin_user_list;
    public $admin_role_list;
    public $forbidden_user_list;
    public $forbidden_role_list;

    public $data_id = false;
    public $rule_alias = false;
    public $rule_admin_alias = false;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['active_user_list', 'active_role_list',
              'forbidden_user_list', 'forbidden_role_list'], 'safe']
        ];
    }

    public function loadModelRules($data_id)
    {
        $rule = Rule::findOne(['alias' => $this->rule_alias,
            'data' => '{"data_id":"'. $data_id .'"}']);

        $rule_admin = Rule::findOne(['alias' => $this->rule_admin_alias,
            'data' => '{"data_id":"'. $data_id .'"}']);

        if($rule instanceof Rule)
        {
            $this->active_user_list = [];
            $this->forbidden_user_list = [];

            $user_rules = UserRules::findAll(['rule_id' => $rule->id]);

            if(count($user_rules) > 0)
                foreach($user_rules as $_urule)
                {
                    if($_urule->status == UserRoleRule::STATUS_ACTIVE_RULE)
                        $this->active_user_list[] = $_urule->user_id;

                    if($_urule->status == UserRoleRule::STATUS_FORBIDDEN_RULE)
                        $this->forbidden_user_list[] = $_urule->user_id;

                }

            $this->active_role_list = [];
            $this->forbidden_role_list = [];

            $user_role_rules = UserRoleRule::findAll(['rule_id' => $rule->id]);

            if(count($user_role_rules) > 0)
                foreach($user_role_rules as $_rrule)
                {
                    if($_rrule->status == UserRoleRule::STATUS_ACTIVE_RULE)
                        $this->active_role_list[] = $_rrule->role_id;
                    if($_rrule->status == UserRoleRule::STATUS_FORBIDDEN_RULE)
                        $this->forbidden_role_list[] = $_rrule->role_id;
                }

        }

        if($rule_admin instanceof Rule)
        {
            $this->admin_user_list = [];

            $user_rules = UserRules::findAll(['rule_id' => $rule_admin->id]);

            if(count($user_rules) > 0)
                foreach($user_rules as $_urule)
                {
                    if($_urule->status == UserRoleRule::STATUS_ACTIVE_RULE)
                        $this->admin_user_list[] = $_urule->user_id;

//                    if($_urule->status == UserRoleRule::STATUS_FORBIDDEN_RULE)
//                        $this->forbidden_user_list[] = $_urule->user_id;

                }

            $this->admin_role_list = [];

            $user_role_rules = UserRoleRule::findAll(['rule_id' => $rule_admin->id]);

            if(count($user_role_rules) > 0)
                foreach($user_role_rules as $_rrule)
                {
                    if($_rrule->status == UserRoleRule::STATUS_ACTIVE_RULE)
                        $this->admin_role_list[] = $_rrule->role_id;

//                    if($_rrule->status == UserRoleRule::STATUS_FORBIDDEN_RULE)
//                        $this->forbidden_role_list[] = $_rrule->role_id;

                }

        }

    }

    public function addUserRule($rule_id, $user_id, $status)
    {
            if($rule_id > 0 && $user_id > 0)
                if($rule_alias = $this->rule_alias)
                {
                        $user_rule = UserRules::findOne([
                            'rule_id' => $rule_id,
                            'user_id' => $user_id
                        ]);

                        if(!$user_rule)
                        {
                            $user_rule =  new UserRules();
                            $user_rule->user_id = $user_id;
                            $user_rule->rule_id = $rule_id;
                        }

                        $user_rule->status = $status;

                        return $user_rule->save();


                }

        return false;

    }

    public function addUserRoleRule($rule_id, $role_id, $status)
    {
            if($rule_id > 0 && $role_id > 0)
                if($rule_alias = $this->rule_alias)
                {

                        $user_role_rule = UserRoleRule::findOne([
                            'rule_id' => $rule_id,
                            'role_id' => $role_id
                        ]);

                        if(!$user_role_rule)
                        {
                            $user_role_rule =  new UserRoleRule();
                            $user_role_rule->role_id = $role_id;
                            $user_role_rule->rule_id = $rule_id;
                        }

                        $user_role_rule->status = $status;

                        return $user_role_rule->save();

                }

        return false;

    }


    public function save()
    {
        if($data_id = $this->data_id)
            if($this->rule_alias && $this->rule_admin_alias) {

                $rule = Rule::findOne(['alias' => $this->rule_alias, 'data' => '{"data_id":"' . $data_id . '"}']);
                $rule_admin = Rule::findOne(['alias' => $this->rule_admin_alias, 'data' => '{"data_id":"' . $data_id . '"}']);

                if (!$rule) {
                    $rule = new Rule();
                    $rule->name = $this->rule_alias;
                    $rule->alias = $this->rule_alias;
                    $rule->data = json_encode(['data_id' => $data_id]);
                    $rule->comment = 'generated be report permission form';
                    $rule->save();
                }else{
                    UserRoleRule::deleteAll(['rule_id' => $rule->id]);
                    UserRules::deleteAll(['rule_id' => $rule->id]);
                }

                if (!$rule_admin) {
                    $rule_admin = new Rule();
                    $rule_admin->name = $this->rule_admin_alias;
                    $rule_admin->alias = $this->rule_admin_alias;
                    $rule_admin->data = json_encode(['data_id' => $data_id]);
                    $rule_admin->comment = 'generated be report permission form';
                    $rule_admin->save();
                }else{
                    UserRoleRule::deleteAll(['rule_id' => $rule_admin->id]);
                    UserRules::deleteAll(['rule_id' => $rule_admin->id]);
                }

                if(isset($_POST['PermissionForm']))
                {
                    foreach ($_POST['PermissionForm'] as $_key => $_value)
                    {
                        if (!(strpos($_key, 'role_') === false)) {
                            $role_id = substr($_key, strpos($_key, '_') + 1);

                            if ($_value == 'admin_role_list')
                                $this->admin_role_list[] = $role_id;

                            if ($_value == 'active_role_list')
                                $this->active_role_list[] = $role_id;

                            if ($_value == 'forbidden_role_list')
                                $this->forbidden_role_list[] = $role_id;

                        }

                    }

                    foreach ($_POST['PermissionForm'] as $_key => $_value)
                    {
                        if (!(strpos($_key, 'user_') === false)) {
                            $user_id = substr($_key, strpos($_key, '_') + 1);

                            if ($_value == 'admin_user_list')
                                $this->admin_user_list[] = $user_id;

                            if ($_value == 'active_user_list')
                                $this->active_user_list[] = $user_id;

                            if ($_value == 'forbidden_user_list')
                                $this->forbidden_user_list[] = $user_id;

                        }

                    }

                }

                if (is_array($this->active_user_list))
                    if (count($this->active_user_list) > 0) {

                        foreach ($this->active_user_list as $user_id) {
                            $this->addUserRule($rule->id, $user_id, UserRules::STATUS_ACTIVE_RULE);
                        }

                    }


                if (is_array($this->admin_user_list))
                    if (count($this->admin_user_list) > 0) {

                        foreach ($this->admin_user_list as $user_id) {
                            $this->addUserRule($rule_admin->id, $user_id, UserRules::STATUS_ACTIVE_RULE);
                        }

                    }

                if (is_array($this->forbidden_user_list))
                    if (count($this->forbidden_user_list) > 0) {
                        foreach ($this->forbidden_user_list as $user_id) {
                            $this->addUserRule($rule->id, $user_id, UserRules::STATUS_FORBIDDEN_RULE);
                        }
                    }

                if (is_array($this->active_role_list))
                    if (count($this->active_role_list) > 0) {
                        foreach ($this->active_role_list as $role_id) {
                            $this->addUserRoleRule($rule->id, $role_id, UserRules::STATUS_ACTIVE_RULE);
                        }
                    }

                if (is_array($this->admin_role_list))
                    if (count($this->admin_role_list) > 0) {
                        foreach ($this->admin_role_list as $role_id) {
                            $this->addUserRoleRule($rule_admin->id, $role_id, UserRules::STATUS_ACTIVE_RULE);
                        }
                    }

                if (is_array($this->forbidden_role_list))
                    if (count($this->forbidden_role_list) > 0) {
                        foreach ($this->forbidden_role_list as $role_id) {
                            $this->addUserRoleRule($rule->id, $role_id, UserRules::STATUS_FORBIDDEN_RULE);
                        }
                    }

            }

        return true;

    }

    public function getRoleList(){
        return UserRole::getRoleList();
    }

    public function getDataRoleProvider()
    {
        $models = UserRole::find()->all();
        return new ArrayDataProvider([
            'allModels' => ($models) ? $models : [],
            'pagination' => false,
        ]);
    }

    public function getUserList(){
        return User::getUserList();
    }

    public function getDataUserProvider()
    {
        $models = User::find()->all();
        return new ArrayDataProvider([
            'allModels' => ($models) ? $models : [],
            'pagination' => false,
        ]);
    }

    public function gridUserPermissionColumns()
    {
        $active_users = is_array($this->active_user_list) ? $this->active_user_list : [];
        $admin_users = is_array($this->admin_user_list) ? $this->admin_user_list : [];
        $forbidden_users = is_array($this->forbidden_user_list) ? $this->forbidden_user_list : [];

        return [
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => '<span>#</span>',
                'contentOptions' => [
                    'width' => 70
                ],
            ],
            [
                'attribute' => 'username',
                'format' => 'raw',
                'contentOptions' => [
                    'width' => '7%'
                ],
            ],
            [
                'attribute' => 'fullname',
                'format' => 'raw',
                'contentOptions' => [
                    'width' => '15%'
                ],
            ],
            [
                'attribute' => 'department_id',
                'format' => 'raw',
                'value' => function($model){
                    if($model->department_id > 0)
                    {
                        $_department = Department::findOne((int) $model->department_id);
                        if(isset($_department->name)) return $_department->name;
                    }
                },
                'contentOptions' => [
                    'width' => '10%'
                ],
            ],
            [
                'header' => 'Role',
                'format' => 'raw',
                'value' => function($model){
                    if(isset($model->role))
                        if($model->role > 0)
                        {
                            $_role = UserRole::findOne((int) $model->role);
                            if(isset($_role->name)) return $_role->name;
                        }
                },
                'contentOptions' => [

                ],
            ],
//            [
//                'header' => 'Active',
//                'format' => 'raw',
//                'value' => function($model)use($active_users){
//                    return Html::checkbox('PermissionForm[active_user_list][]',
//                        in_array($model->id, $active_users),
//                        ['value' => $model->id, 'class' =>'one-tr-check']);
//                },
//                'contentOptions' => [
//                    'width' => '8%'
//                ],
//            ],
            [
                'header' => 'Status',
                'format' => 'raw',
                'value' => function($model)use($active_users, $forbidden_users, $admin_users){

                    $status_id = 'default_user_list';

                    if(in_array($model->id, $active_users)) $status_id = 'active_user_list';
                    if(in_array($model->id, $admin_users)) $status_id = 'admin_user_list';
                    if(in_array($model->id, $forbidden_users)) $status_id = 'forbidden_user_list';

                    return Html::dropDownList('PermissionForm[user_'. $model->id .']',
                        $status_id, [
                            'default_user_list' => 'Default',
                            'active_user_list' => 'View',
                            'admin_user_list' => 'Manage',
                            'forbidden_user_list' => 'Forbidden',
                        ],
                        ['class' =>'form-control']);
                },
                'contentOptions' => [
                    'width' => '12%'
                ],
            ],
        ];

    }

    public function gridRolePermissionColumns()
    {
        $active_roles = is_array($this->active_role_list) ? $this->active_role_list : [];
        $forbidden_roles = is_array($this->forbidden_role_list) ? $this->forbidden_role_list : [];
        $admin_roles = is_array($this->admin_role_list) ? $this->admin_role_list : [];

        return [
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => '<span>#</span>',
                'contentOptions' => [
                    'width' => 70
                ],
            ],
            [
                'attribute' => 'name',
                'format' => 'raw',
                'contentOptions' => [
                    'width' => '17%'
                ],
            ],
            [
                'attribute' => 'alias',
                'format' => 'raw',
                'contentOptions' => [
                    'width' => '17%'
                ],
            ],
            [
                'attribute' => 'comment',
                'format' => 'raw',
            ],
            [
                'header' => 'Status',
                'format' => 'raw',
                'value' => function($model)use($active_roles, $forbidden_roles, $admin_roles){

                    $status_id = 'default_role_list';

                    if(in_array($model->id, $active_roles)) $status_id = 'active_role_list';
                    if(in_array($model->id, $admin_roles)) $status_id = 'admin_role_list';
                    if(in_array($model->id, $forbidden_roles)) $status_id = 'forbidden_role_list';

                    return Html::dropDownList('PermissionForm[role_'. $model->id .']',
                        $status_id, [
                            'default_role_list' => 'Default',
                            'active_role_list' => 'View',
                            'admin_role_list' => 'Manage',
                            'forbidden_role_list' => 'Forbidden',
                        ],
                        ['class' =>'form-control']);
                },
                'contentOptions' => [
                    'width' => '12%'
                ],
            ],
        ];
    }

}
