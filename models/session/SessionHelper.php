<?php
/**
 * Created by PhpStorm.
 * @author: Roman Budnitsky
 * @date: 18.11.14 15:23
 * @copyright Copyright (c) 2014 PFSOFT LLC
 *
 * Class Country
 */

namespace app\models\session;

use yii;
use yii\base\Model;

class SessionHelper extends Model {

    public static function setUserSession($except = [])
    {
        $page_id = Yii::$app->controller->id . '_' . Yii::$app->controller->action->id;

        if(isset($_GET['report_id'])) $page_id = $page_id . '_' . $_GET['report_id'];
        if(isset($_GET['id'])) $page_id = $page_id . '_' . $_GET['id'];

        $params = Yii::$app->request->queryParams;

        $params_get = [];

        if(is_array($except))
            if(count($except) > 0) {

                $array_keys = array_keys($params);
                $array_keys = array_flip(array_diff($array_keys, $except));

                $params_get = array_intersect_key($params, $array_keys);

            }

        if(isset($_SESSION['params_' . $page_id]) && !(count($params_get)))
        {
            $params = $_SESSION['params_' . $page_id];

            if(is_array($params))
            {
               Yii::$app->request->setQueryParams($params);
            }

        }

        $_SESSION['params_' . $page_id] = $params;

    }

} 