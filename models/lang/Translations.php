<?php

namespace app\models\lang;

use app\components\Entity;
use app\models\DefaultQuery;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Html;

/**
 * This is the model class for table "lang_translate".
 *
 * @property integer $id
 * @property integer $lang_id
 * @property integer $entity
 * @property integer $entity_id
 * @property integer $attribute
 * @property integer $content
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Languages $lang
 */
class Translations extends Entity
{

    /*
    |--------------------------------------------------------------------------
    | Constants && properties
    |--------------------------------------------------------------------------
    */

    public static $_pull = [];

    /*
    |--------------------------------------------------------------------------
    | Model configurations
    |--------------------------------------------------------------------------
    */

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lang_translate';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lang_id', 'entity', 'entity_id', 'attribute', 'content'], 'required'],
            [['lang_id', 'entity', 'entity_id', 'attribute', 'content', 'created_at', 'updated_at'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('app', 'ID'),
            'lang_id'    => Yii::t('app', 'Lang ID'),
            'entity'     => Yii::t('app', 'Entity'),
            'entity_id'  => Yii::t('app', 'Entity ID'),
            'attribute'  => Yii::t('app', 'Attribute'),
            'content'    => Yii::t('app', 'Content'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     * @return TranslationsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DefaultQuery(get_called_class());
    }

    /*
    |--------------------------------------------------------------------------
    | Model relations
    |--------------------------------------------------------------------------
    */

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages()
    {
        return $this->hasOne(Languages::className(), ['id' => 'lang_id']);
    }

    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */

//    /**
//     * @return \yii\db\ActiveQuery
//     */
//    public function getViewUrl()
//    {
//        return '/languages/view/'. $this->alias;
//    }

    public function getName()
    {
        //
    }

    /**
     * @return array
     */
    public static function getGridColumns()
    {
        return [
            [
                'class'          => 'yii\grid\SerialColumn',
                'header'         => '<span>#</span>',
                'contentOptions' => [
                    'width' => 50
                ],
            ],
            [
                'attribute' => 'name',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'alias',
                'format'    => 'raw',
            ],
//            [
//                'attribute' => 'icon',
//                'format'    => 'raw',
//            ],
            [
                'class'    => 'app\components\grid\ActionColumnCustom',
                'template' => function ($model) {
                    return '{edit} {delete}';
                },
                'buttons'  => [
                    'edit'   => function ($url, $model, $key, $options) {
                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/languages/edit', 'id' => $model->id]);
                        return Html::a('Edit', $url, $options);
                    },
                    'delete' => function ($url, $model, $key, $options) {
                        $options['onclick'] = '
                            if(confirm(\'' . Yii::t('yii', 'Are you sure you want to delete this Page?') . '\'))
                            {
                              return true;
                            } else {
                              return false;
                            }
                        ';
//                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/languages/delete', 'id' => $model->id]);
                        return Html::a('Delete', $url, $options);
                    },

                ],
            ],
        ];
    }

    public function getTypeEntity()
    {
        return Entity::TRANSLATION;
    }

    public function getViewUrl()
    {
        return null;
    }

}
