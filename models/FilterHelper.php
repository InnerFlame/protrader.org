<?php
/**
 * Created by PhpStorm.
 * @author: Roman Budnitsky
 * @date: 18.11.14 15:23
 * @copyright Copyright (c) 2014 PFSOFT LLC
 *
 * Class Country
 */

namespace app\models;

use app\components\Entity;
use app\models\community\Comments;
use app\modules\forum\models\FrmComment;
use yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;


class FilterHelper extends Model {

    /**
     * @param $models
     * @param string $attr
     */
    public static function filterByCategoryGET(&$models, $attr = '', &$filter)
    {


        /**
         * find  in GET if we have one of categories= it means we need filter
         */
        foreach ($models as $key => $model) {

            if (Yii::$app->request->get(Yii::$app->controller->id . $model->category_id)) {
                $filter = true;
                break;
            }

        }

        if ($filter) {
            foreach ($models as $key => $model) {

                if (!Yii::$app->request->get(Yii::$app->controller->id . $model->$attr))
                    unset($models[$key]);

            }
        }

    }


    /**
     * @param $models
     * @param string $attr
     */
    public static function filterByCategory(&$models, $category_alias)
    {

        foreach ($models as $key => $model) {

            if ($model->category->alias !== $category_alias) {
                unset($models[$key]);
            }

        }
    }

    /**
     * @param $models
     */
    public static function filterByDeletedCategory(&$models)
    {
        foreach ($models as $key => $model) {

            if ($model->category->deleted === Entity::DELETED) {
                unset($models[$key]);
            }
        }
    }


    /**
     * @return array      for homepage, first page
     */
    public static function getLastTenComments(){

        // $frm_comments = FrmComment::find()->limit(5)->orderBy('created_at')->All();
        $comments = Comments::find()->limit(10)->orderBy('created_at')->All();
       // return ArrayHelper::merge($comments,$frm_comments);
        return $comments;
    }

} 