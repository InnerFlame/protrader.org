<?php
/**
 * Created by PhpStorm.
 * User: php
 * Date: 16.12.15
 * Time: 17:29
 */

namespace app\models\abstractes;

use Yii;
use app\models\CustomModel;

abstract class AbstractCustomFields extends CustomModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 64],
            [['alias'], 'string', 'max' => 32],
            [[ 'data'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'alias' => 'Url',
            'data' => 'Data',
        ];
    }

    public static function getFieldByAlias($alias)
    {
        $field = static::find()->where(['alias' => $alias])->one();

        if(!is_object($field))
        {
            $field = new static();
            $field->name = $alias;
            $field->alias = $alias;
            $field->save();
        }

        return $field;

    }

}