<?php
/**
 * Created by PhpStorm.
 * User: php
 * Date: 16.12.15
 * Time: 17:29
 */

namespace app\models\abstractes;

use Yii;
use app\models\CustomModel;

abstract class AbstractCustomValue extends CustomModel
{
    # Return etitity attribute, for example 'broker_id'
    abstract public static function getEntitityAttr();

    public static function getCustomValueModel($field_id, $entitity_id)
    {
        $entitity_attribute =  static::getEntitityAttr();

        if($field_id > 0 && $entitity_id > 0 && $entitity_attribute)
        {
            $model = self::findOne([
                'field_id'              => (int)$field_id,
                $entitity_attribute => (int)$entitity_id
            ]);

            if (!is_object($model))
            {
                $model = new static();
                $model->field_id = $field_id;
                $model->$entitity_attribute = $entitity_id;
                $model->save();
            }

            return $model;

        }

    }

}