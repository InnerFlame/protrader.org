<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 16.12.15
 * Time: 15:54
 */
namespace app\models\abstractes;

use app\components\Entity;
use app\models\CustomModel;
use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "slide".
 *
 * @property integer $id
 * @property string $entity
 * @property integer $entity_id
 * @property string $title
 * @property string $pictures
 * @property string $link
 * @property string $description
 * @property string $weight
 * @property string $data
 */
class AbstractSlide extends Entity
{
    public $image = null;

    public $crop = null;

    const MAIN = 'main';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slide';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 120],
            [['weight'], 'integer'],
            [['weight', 'data', 'link'], 'string', 'max' => 256],
            [['entity', 'entity_id', 'title', 'description', 'pictures'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'title'       => 'Title',
            'link'        => 'Link',
            'pictures'    => 'Pictures',
            'description' => 'Description',
            'weight'      => 'Weight',
            'data'        => 'Data',
            'entity'      => 'Feature',
            'entity_id'   => 'Feature',
            'slide'       => 'slide',
        ];
    }

    /**
     * BeforeSave action
     * Checking client images
     *
     * @return mixed
     */
    public function beforeSave($insert)
    {
        if (!$this->image->isChanged('pictures')) {
            unset($this->pictures);
        }

        if ($this->image->isDeleted('pictures')) {
            $this->pictures = '';
        }

        return parent::beforeSave($insert);
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($this->scenario <> 'delete') {
            if ($this->image->getUploadedFile('pictures')) {
                $this->pictures = $this->image->uploadImage('pictures');
                if ($this->pictures)
                    $this->save(false);
            }
        }

        return parent::afterSave($insert, $changedAttributes);
    }

    public function getTypeEntity()
    {
        return Entity::SLIDE;
    }

    public function getName()
    {
        return $this->title;
    }

    public function getViewUrl()
    {
        // TODO: Implement getViewUrl() method.
    }

    /**
     * @return array
     */
    public static function getGridColumns()
    {
        return [
            [
                'class'          => 'yii\grid\SerialColumn',
                'header'         => '<span>#</span>',
                'contentOptions' => [
                    'width' => 50
                ],
            ],

            [
                'attribute' => 'title',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'description',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'link',
                'format'    => 'raw',
            ],

            [
                'attribute' => 'weight',
                'format'    => 'raw',
            ],

            [
                'class'    => 'app\components\grid\ActionColumnCustom',
                'template' => function ($model) {
                    return '{edit} {delete}';
                },
                'buttons'  => [
                    'edit' => function ($url, $model, $key, $options) {
                        //  $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/slide-edit', 'id' => $model->id]);
                        return Html::a('Edit', $url, $options);
                    },

                    'delete' => function ($url, $model, $key, $options) {
                        $options['onclick'] = '
                        if(confirm(\'' . Yii::t('yii', 'Are you sure you want to delete this Page?') . '\'))
                        {
                          return true;
                        } else {
                          return false;
                        }
                    ';
//                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/slide-delete', 'id' => $model->id]);
                        return Html::a('Delete', $url, $options);
                    },
                ],
            ],
        ];
    }
}