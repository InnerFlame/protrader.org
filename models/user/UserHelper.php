<?php

namespace app\models\user;

use Yii;
use app\components\mailer\CustomMailer;
use yii\base\NotSupportedException;

trait UserHelper
{
    /*
    |--------------------------------------------------------------------------
    | Properties
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | Methods implements IdentityInterface
    |--------------------------------------------------------------------------
    */

    /**
     * Finds user by auth token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByAuthToken($token)
    {
        return static::findOne([
            'auth_key' => $token,
        ]);
    }


    /**
     * @return mixed
     */
    public function banned()
    {
        return $this->andWhere(['status' => User::STATUS_BANNED]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */

    /**
     * Finds user by username or email
     *
     * @param string $attr
     *
     * @return static|null
     */
    public static function findByUsernameOrEmail($attr) {
        return static::find()
            ->andWhere("username = :attr OR email = :attr")
            //          ->andWhere(['status' => 0])
            ->params([':attr' => $attr])->one();
    }

    /**
     * Send mail to recovery password
     * @param $email
     * @param $token
     * @param string $template
     * @return bool
     */
    public static function sendConfirmMail($email, $token, $template = 'sign-up.twig')
    {
        if (CustomMailer::send($email, $template,[
            'get_link' => Yii::$app->urlManager->createAbsoluteUrl(['confirm', 'key' => $token])
        ])) return true;

        return false;
    }


    /**
     * Send mail to recovery password
     *
     * @param $email
     * @param $token
     * @return bool
     */
    public static function sendConfirmRecoveryPasswordMail($email, $token)
    {
        if (CustomMailer::send($email, 'recovery-password.twig',[
            'get_link' => Yii::$app->urlManager->createAbsoluteUrl(['confirm', 'key' => $token])
        ])) return true;

        return false;
    }



    /**
     * Validates password
     *
     * @param $password
     * @param bool $clearPasswordFields
     * @return bool
     */

    public function validatePassword($password, $clearPasswordFields = true)
    {
        $this->password = $password;
        $this->trigger('beforeValidatePassword');
        if($clearPasswordFields) {
            $this->password = null;
            $this->new_password = null;
            $this->new_password_repeat = null;
        }
        if ($this->password_hash) {
            return Yii::$app->security->validatePassword($password, $this->password_hash);
        } else {
            return false;
        }
    }
}