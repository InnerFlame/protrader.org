<?php
/**
 * Created by PhpStorm.
 * User: Zaitsev Oleg
 * Date: 26.06.15
 * Time: 10:27
 *
 * Class License
 * @package app\modules\knowledgebase\models
 */


namespace app\models\user;

use yii;
use yii\db\ActiveRecord;
use yii\db\ActiveQuery;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "salary"
 *
 * @property integer $id
 * @property string $name
 * @property string $alias
 * @property string $comment
 *
 */

class UserRole extends ActiveRecord {

    public static $report_fields = [
        'name',
        'alias',
        'comment',
    ];

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'alias'], 'required'],
            [['name'], 'string', 'max' => 128],
            [['comment'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'alias' => 'Url',
            'comment' => 'Comment',
        ];
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%role}}';
    }

    public static function getRoleList()
    {
        $models = self::find()->all();
        return yii\helpers\ArrayHelper::map($models, 'id', 'name');
    }

    public static function getReportFilter($attr, $index, $value = null)
    {
        return null;
    }

    /**
     * Before Save commands
     * @params boolean $insert
     *
     * @return mixed
     *
     */
    public function beforeSave($insert)
    {
        $this->name = Yii::$app->customFunctions->cleanText($this->name);
        $this->comment = Yii::$app->customFunctions->cleanText($this->comment);

        return parent::beforeSave($insert);
    }

    /**
     * After Save commands
     * @params boolean $insert
     * @params $mixed changedAttributes
     *
     * @return mixed
     *
     */
    public function afterSave($insert, $changedAttributes)
    {
        return parent::afterSave($insert, $changedAttributes);
    }

}