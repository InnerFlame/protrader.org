<?php

namespace app\models\user\crosstrade\forms;

use app\components\MailChimpBehaviors;
use app\models\forms\SignUp;
use app\models\user\crosstrade\CrosstradeSubscriber;
use app\models\user\User;
use app\models\user\UserIdentity;
use Yii;

/**
 * @property integer $fullname
 * @property integer $email
 *
 * @property User $user
 */
class CrosstradeSubscribeForm extends CrosstradeSubscriber
{
    public $email;
    public $fullname;
    public $country;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fullname', 'email'], 'required'],
            [['email'], 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fullname' => Yii::t('app', 'Name'),
            'email'    => Yii::t('app', 'Email'),
        ];
    }

    public function getResponse($user)
    {
        if ($user instanceof UserIdentity) {# если пользователь залогинен
            $this->subscribe($user);
            MailChimpBehaviors::run($user, MailChimpBehaviors::CHANNEL_CROSS_TRADE_SUBSCRIPTION);
            $user->fullname = Yii::$app->request->post('CrosstradeSubscribeForm')['fullname'];
            $user->save();

            return Yii::$app->controller->renderPartial('@views/site/crosstrade/subscribe.twig');
        } else {# если пользователь не залогинен
            $user = SignUp::find()->where(['email' => Yii::$app->request->post('CrosstradeSubscribeForm')['email']])->one();
            if ($user) {# не залогинен, но такой пользователь есть в базе
                if ($user->crosstradeSubscriber) {# не залогинен,такой пользователь есть в базе, уже пожписан
                    return Yii::$app->controller->renderPartial('@views/site/crosstrade/subscribe_already.twig');
                } else {# не залогинен, но такой пользователь есть в базе, не пожписан
                    $this->subscribe($user);
                    MailChimpBehaviors::run($user, MailChimpBehaviors::CHANNEL_CROSS_TRADE_SUBSCRIPTION);

                    return Yii::$app->controller->renderPartial('@views/site/crosstrade/subscribe.twig');
                }
            } else {# не залогинен и такого пользователя нет в базе
                $user = new SignUp();
                $user->email = Yii::$app->request->post('CrosstradeSubscribeForm')['email'];
                $user->channel = 'Cross Trade';
                $user->registration('crosstrade-subscribe.twig');
                $user->fullname = Yii::$app->request->post('CrosstradeSubscribeForm')['fullname'];
                $user->save();

                $this->subscribe($user, true);
                MailChimpBehaviors::run($user, MailChimpBehaviors::CHANNEL_CROSS_TRADE_SUBSCRIPTION);

                return Yii::$app->controller->renderPartial('@views/site/crosstrade/subscribe_and_reg.twig');
            }
        }
    }

    /**
     * @param $user
     * @param bool|false $isRegistered если пользователь был зарегестрировал по причние подписки
     */
    public function subscribe($user, $isRegistered = false)
    {
        $model = new CrosstradeSubscriber();
        $model->user_id = $user->id;
        $model->status = self::STATUS_SUBSCRIBED;
        $model->is_registered = $isRegistered;
        $model->save();
    }
}
