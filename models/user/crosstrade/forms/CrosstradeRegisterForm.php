<?php

namespace app\models\user\crosstrade\forms;

use app\components\api\NotServiceAvailable;
use app\components\api\NotServiceAvailableException;
use app\components\helpers\StringHelper;
use app\components\MailChimpBehaviors;
use app\models\forms\DemoAccountForm;
use app\models\forms\SignUp;
use app\models\user\crosstrade\CrosstradeSubscriber;
use app\models\user\User;
use app\models\user\UserCustomField;
use app\models\user\UserCustomValue;
use app\models\user\UserIdentity;
use app\models\user\UserProfile;
use Yii;

/**
 * @property integer $fullname
 * @property integer $email
 *
 * @property User $user
 */
class CrosstradeRegisterForm extends CrosstradeSubscriber
{
    public $email;
    public $first_name;
    public $last_name;
    public $country;
    public $accept;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'email', 'country'], 'required'],
            [['first_name', 'last_name'], 'match', 'pattern' => '/^[a-zA-Zа-яёА-ЯЁ\s\-\.]+$/u', 'message' => 'Latin & Cyrillic letters, whitespace, dot and single quote are allowed.'],
            ['accept', 'compare', 'compareValue' => 1, 'message' => 'You should accept'],
            [['email'], 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'first_name' => Yii::t('app', 'First Name'),
            'last_name'  => Yii::t('app', 'Last Name'),
            'email'      => Yii::t('app', 'Email'),
            'country'    => Yii::t('app', 'Country'),
        ];
    }

    public function getResponse($user, $data)
    {
        if ($user instanceof UserIdentity) {# если пользователь залогинен
            try{
                $demo = $this->demoRegistration($data);
                if(!$demo){
                    //Вывод ошибки о валидации сервиса
                    return Yii::$app->controller->renderPartial('@app/modules/crosstrade/views/crosstrade/server_is_not_validate.twig');
                }
                $user = $this->userUpdate($user, $data);
                $this->crossRegistration($user, $demo['demo_login'], $demo['demo_password'], false);
                MailChimpBehaviors::run($user, MailChimpBehaviors::CHANNEL_CROSS_TRADE_REGISTRATION);
                return Yii::$app->controller->renderPartial('@app/modules/crosstrade/views/crosstrade/registration.twig', ['demo_login' => $demo['demo_login'], 'demo_password' => $demo['demo_password']]);
            }catch (NotServiceAvailableException $e){
                return Yii::$app->controller->renderPartial('@@app/modules/crosstrade/views/crosstrade/server_is_not_avaliable.twig');
            }
        } else {# если пользователь не залогинен
            $user = SignUp::find()->where(['email' => Yii::$app->request->post('CrosstradeRegisterForm')['email']])->one();
            if ($user) {# зареган на сайте
                if ($user->crosstradeRegister) {# зареган на crosstrade
                    return Yii::$app->controller->renderPartial('@app/modules/crosstrade/views/crosstrade/registration_already.twig');
                } else {
                    try{
                        $demo = $this->demoRegistration($data);
                        if(!$demo){
                            return Yii::$app->controller->renderPartial('@app/modules/crosstrade/views/crosstrade/server_is_not_validate.twig');
                        }
                        $user = $this->userUpdate($user, $data);
                        $this->crossRegistration($user, $demo['demo_login'], $demo['demo_password'], false);
                        MailChimpBehaviors::run($user, MailChimpBehaviors::CHANNEL_CROSS_TRADE_REGISTRATION);
                        return Yii::$app->controller->renderPartial('@app/modules/crosstrade/views/crosstrade/registration.twig', ['demo_login' => $demo['demo_login'], 'demo_password' => $demo['demo_password']]);
                    }catch (NotServiceAvailableException $e){
                        return Yii::$app->controller->renderPartial('@app/modules/crosstrade/views/crosstrade/server_is_not_avaliable.twig');
                    }
                }
            } else {
                try{
                    $demo = $this->demoRegistration($data);
                    if(!$demo){
                        return Yii::$app->controller->renderPartial('@app/modules/crosstrade/views/crosstrade/server_is_not_validate.twig');
                    }
                    $user = $this->userRegistration($data);
                    $user = $this->userUpdate($user, $data);
                    $this->crossRegistration($user, $demo['demo_login'], $demo['demo_password'], true);
                    MailChimpBehaviors::run($user, MailChimpBehaviors::CHANNEL_CROSS_TRADE_REGISTRATION);

                    return Yii::$app->controller->renderPartial('@app/modules/crosstrade/views/crosstrade/registration_and_reg.twig');
                }catch (NotServiceAvailableException $e){
                    return Yii::$app->controller->renderPartial('@app/modules/crosstrade/views/crosstrade/server_is_not_avaliable.twig');
                }
            }
        }
    }

    /**
     * @param $user
     * @param $data
     * @return mixed
     */
    public function userUpdate($user, $data)
    {
        $user->fullname = $data['first_name'] . ' ' . $data['last_name'];
        $profile = UserProfile::find()->where(['user_id' => $user->id])->one();
        if($profile){
            $profile->first_name = $data['first_name'];
            $profile->last_name = $data['last_name'];
            $profile->save();
        }
        $user->save();

        $model_field = UserCustomField::find()->where(['alias' => 'country'])->one();
        if ($model_field->userCustomValue) {
            $model_field->userCustomValue->value = $data['country'];
            $model_field->userCustomValue->save();
        } else {
            $model_value = new UserCustomValue();
            $model_value->user_id = $user->id;
            $model_value->field_id = $model_field->id;
            $model_value->value = $data['country'];
            $model_value->save();
        }

        return $user;
    }

    /**
     * @param $country
     */
    public function userCountryUpdate($value_model, $country)
    {
        $value_model->value = $country;
        $value_model->userCustomValue->save();
    }

    /**
     * @param $data
     * @return mixed
     */
    public function userRegistration($data)
    {
        $user = new SignUp();
        $user->email = $data['email'];
        $user->channel = 'Cross Trade';
        $user->registration();
//        $user->registration('crosstrade-registration.twig');

        return $this->userUpdate($user, $data);
    }

    /**
     * @param $data
     * @return array
     */
    public function demoRegistration($data)
    {
        $model = new DemoAccountForm();
        $model->email = $data['email'];
        $model->login = $this->getUniqueUsername();
        $model->password = $model->confirm_password = 'Ps7' . StringHelper::randomString(12);
        $model->first_name = $data['first_name'];
        $model->last_name = $data['last_name'];

        if ($model->load(['DemoAccountForm' => $data])) {
            if ($model->registerDemo()) {
                return [
                    'demo_login'    => $model->login,
                    'demo_password' => $model->password
                ];
            }
        }
        return false;
    }

    /**
     * @param $user
     * @param $demo_login
     * @param $demo_password
     * @param bool $isRegistered
     * @return bool
     */
    public function crossRegistration($user, $demo_login, $demo_password, $isRegistered = false)
    {
        $model = CrosstradeSubscriber::find()->where(['user_id' => $user->id])->one();
        if (!$model) {
            $model = new CrosstradeSubscriber();
        }

        $model->user_id = $user->id;
        $model->status = self::STATUS_REGISTERED;
        $model->is_registered = $isRegistered;
        $model->demo_login = $demo_login;
        $model->demo_password = $demo_password;

        return $model->save();
    }

    /**
     * Генерирует уникальный Username для demo аккаунта
     * исходя из последнего, максимального, зарегестрированного ID
     * Возвращает username в формате CHAMPION-00001, CHAMPION-00002
     */
    protected function getUniqueUsername()
    {
        return sprintf("CHAMP-%s", floor(microtime(true)));
    }
}
