<?php

namespace app\models\user\crosstrade;

use app\models\user\User;
use app\modules\crosstrade\models\reports\ReportAo;
use app\modules\crosstrade\models\reports\ReportAsr;
use Yii;

/**
 * This is the model class for table "crosstrade_subscriber".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $status
 * @property integer $demo_login
 * @property integer $demo_password
 * @property integer $is_registered
 *
 * @property User $user
 */
class CrosstradeSubscriber extends \yii\db\ActiveRecord
{
    const STATUS_SUBSCRIBED = 1;
    const STATUS_REGISTERED = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'crosstrade_subscriber';
    }

    public static function getRoleList()
    {
        return [
            self::STATUS_SUBSCRIBED => 'Subscribed',
            self::STATUS_REGISTERED => 'Registered'
        ];
    }

    /**
     *
     * @param $statusId
     * @return string
     */
    public static function getStatus($statusId)
    {
        switch($statusId){
            case self::STATUS_REGISTERED:
                return Yii::t('app', 'Registered');
            case self::STATUS_SUBSCRIBED:
                return Yii::t('app', 'Subscribed');
        }
    }

    public static function getConfirmedList()
    {
        return [
            0 => 'Unconfirmed',
            1 => 'Confirmed'
        ];
    }

    /**
     * @return int возвращает статус пользователя: подтвержден, не подтвержден
     */
    public function getConfirmed()
    {
        return $this->user->status ? "Confirmed" : 'Unconfirmed';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'status'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => Yii::t('app', 'ID'),
            'user_id'       => Yii::t('app', 'User ID'),
            'status'        => Yii::t('app', 'Status'),
            'demo_login'    => Yii::t('app', 'Champ Login'),
            'demo_password' => Yii::t('app', 'Demo Password'),
            'is_registered' => Yii::t('app', 'Newcomers'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportAo()
    {
        return $this->hasMany(ReportAo::className(), ['login' => 'demo_login']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportAsr()
    {
        return $this->hasOne(ReportAsr::className(), ['login' => 'demo_login']);
    }

    /**
     * @inheritdoc
     * @return CrosstradeSubscriberQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CrosstradeSubscriberQuery(get_called_class());
    }
}
