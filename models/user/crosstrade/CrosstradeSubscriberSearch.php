<?php

namespace app\models\user\crosstrade;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CrosstradeSubscriberSearch represents the model behind the search form about `app\models\user\crosstrade\CrosstradeSubscriber`.
 */
class CrosstradeSubscriberSearch extends CrosstradeSubscriber
{

    public $username;

    public $confirmed;

    public $fullname;

    public $email;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'status', 'is_registered'], 'integer'],
            [['created_at', 'demo_login', 'demo_password', 'username', 'confirmed', 'fullname', 'email'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CrosstradeSubscriber::find();

        // add conditions that should always apply here

        $query->joinWith(['user']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['username'] = [
            'asc' => ['user.username' => SORT_ASC],
            'desc' => ['user.username' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['fullname'] = [
            'asc' => ['user.fullname' => SORT_ASC],
            'desc' => ['user.fullname' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['email'] = [
            'asc' => ['user.email' => SORT_ASC],
            'desc' => ['user.email' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['confirmed'] = [
            'asc' => ['user.status' => SORT_ASC],
            'desc' => ['user.status' => SORT_DESC],
        ];


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'crosstrade_subscriber.user_id' => $this->user_id,
            'crosstrade_subscriber.status' => $this->status,
            'crosstrade_subscriber.is_registered' => $this->is_registered,
            'crosstrade_subscriber.created_at' => $this->created_at,
            'user.status' => $this->confirmed,
        ]);

        $query->andFilterWhere(['like', 'demo_login', $this->demo_login])
            ->andFilterWhere(['like', 'demo_password', $this->demo_password])
            ->andFilterWhere(['like', 'user.username', $this->username])
            ->andFilterWhere(['like', 'user.email', $this->email])
            ->andFilterWhere(['like', 'user.fullname', $this->fullname]);

//        VarDumper::dump($query, 10, true); exit;
        return $dataProvider;
    }
}
