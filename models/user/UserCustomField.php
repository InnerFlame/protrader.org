<?php

namespace app\models\user;

use app\models\forms\ProfileForm;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "user_custom_field".
 *
 * @property integer $id
 * @property string $name
 * @property string $alias
 * @property string $data
 *
 * @property User $id0
 * @property UserCustomValue[] $userCustomValues
 * @property User[] $users
 */
class UserCustomField extends \yii\db\ActiveRecord
{


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_custom_field';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'alias'], 'required'],
            [['data'], 'string'],
            [['name'], 'string', 'max' => 64],
            [['alias'], 'string', 'max' => 32],


        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'alias' => 'Url',
            'data' => 'Data',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(User::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCustomValues()
    {
        return $this->hasMany(UserCustomValue::className(), ['field_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCustomValue()
    {
        if(Yii::$app->user->isGuest){
            return false;
        }
        return $this->hasOne(UserCustomValue::className(), ['field_id' => 'id'])->andWhere(['user_id' => Yii::$app->user->identity->getId()]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('user_custom_value', ['field_id' => 'id']);
    }


    public static function saveField($attribute = null){

        $field = UserCustomField::find()->where(['name' => $attribute])->one();

        if (!is_object($field)) {
            $field = new UserCustomField();

            $field->name = $attribute;
            $field->alias = $attribute;
            if (!$field->save()) {
                Yii::$app->session->setFlash('warning', 'Field ' . $attribute . '  was not saved');
            }

        }
        return $field->id;
    }


    public static function getCountry($user_id = null)
    {
        $user_id = is_null($user_id) ? Yii::$app->user->getId() : $user_id;

        if(self::getDataByAttrName('country', $user_id) > 0){
            return ProfileForm::$_countries[self::getDataByAttrName('country', $user_id)];
        }
        return null;
    }

    public static function getList($except = null)
    {
        $users = UserCustomField::find()->all();

        return ArrayHelper::map($users, 'id', 'name');
    }

    public static function getFieladIdByAlias($alias)
    {
        $model = UserCustomField::findOne([
            'alias' => $alias
        ]);

        return $model->id;
    }


    /**
     * @param null $user_id
     * @param string $attribute
     * @return null|string
     */
    public static function getDataByAttrName($attribute = '', $user_id =  null)
    {
        $id = is_null($user_id) ? Yii::$app->user->getId() : $user_id;
        $fields = UserCustomField::findAll(['name' => $attribute]);

        if(count($fields) > 0)
            foreach ($fields as $key => $field) {
                $value  = UserCustomValue::find()->where(['field_id' => $field->id, 'user_id' => $id])->one();
                if(is_object($value))
                    return $value->value;
            }

        return null;
    }
}