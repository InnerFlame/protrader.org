<?php

namespace app\models\user;

use Yii;
use yii\web\BadRequestHttpException;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "xmpp_users".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $username
 * @property string $token
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User $user
 */
class XMPPUserRecord extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'xmpp_users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'username', 'token'], 'required'],
            [['user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['username', 'token'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'username' => 'Username',
            'token' => 'Token',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     *  Инициализирует модель на базе модели User
     * @param User $user
     */
    public function setUser(User $user)
    {
        if($this->isNewRecord){
            $this->user_id = $user->id;
        }
        if(!empty($user->email)){
            $this->username = preg_replace('/@/', '[u+0040]', $user->email);
        }else{
            throw new BadRequestHttpException(Yii::t('app', 'Error create new xmpp user with user: {username}. Not found email on user.', ['username' => $user->username]));
        }
        $this->token = \Yii::$app->security->generateRandomString(15);
    }

    /**
     * @param int $id
     * @return array|null|XMPPUserRecord
     */
    public static function findByUserId($id)
    {
        return self::find()->where(['user_id' => $id])->one();
    }
}
