<?php

namespace app\models\user;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "user_settings".
 *
 * @property integer $id
 * @property integer $lang_id
 *
 * @property User $id0
 */
class UserSettings extends ActiveRecord
{
    public $SUBSCRIBE_WHEN_OWN_TOPIC_COMMENTED ;
    public $SUBSCRIBE_WHEN_TOPIC_COMMENTED;
    public $SUBSCRIBE_WHEN_ARTICLE_COMMENTED;
    public $SUBSCRIBE_WHEN_ANSWERED_OWN_COMMENT;
    public $SUBSCRIBE_WHEN_CREATED_ARTICLE_OR_TOPIC;

    public $ceo;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['id', 'lang_id', 'user_id'], 'integer'],
            [['data'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'lang_id' => 'Lang ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(User::className(), ['id' => 'id']);
    }

    public function setValue()
    {
        $user_settings = UserSettings::findOne([
            'user_id' => $this->user_id,
        ]);

        $fields = json_decode($user_settings->data, true);

        foreach($fields as $field => $value){
            $this->$field = $value;
        }
    }
}
