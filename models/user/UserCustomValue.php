<?php

namespace app\models\user;

use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "user_custom_value".
 *
 * @property integer $user_id
 * @property integer $field_id
 * @property integer $value
 *
 * @property User $user
 * @property UserCustomField $field
 */
class UserCustomValue extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_custom_value';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'field_id'], 'required'],
            [['user_id', 'field_id'], 'integer'],
            [['value'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id'  => 'User ID',
            'field_id' => 'Field ID',
            'value'    => 'Value',
        ];
    }

    /**Last Name
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getUserById($id)
    {
        return User::find()->where(['id' => $id])->one();
    }

    public function getUserModel()
    {
        return User::find()->where(['id' => $this->user_id])->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public static function saveValue($field_id, $attribute, $value, $user_id = null)
    {

        $user_id = is_null($user_id) ? Yii::$app->user->getId() : $user_id;

        $user_custom_value = UserCustomValue::find()->where(
            ['user_id' => $user_id, 'field_id' => $field_id])->one();

        if (!is_object($user_custom_value)) {
            $user_custom_value = new UserCustomValue();
        }

        $user_custom_value->value = Yii::$app->customFunctions->cleanText($value);
        $user_custom_value->field_id = $field_id;
        $user_custom_value->user_id = $user_id;

        //  if($user_custom_value->field_id === 6){
        //      echo '<pre>'; var_dump($user_custom_value->attributes,$value);die;
        // }


        if (!$user_custom_value->save()) {
            Yii::$app->session->setFlash('warning', $attribute . ' is not saved');
        }


    }

    public static function createModel($data)
    {
        $model = self::findOne($data);

        if (is_object($model)) return false;

        $model = new UserCustomValue($data);

        $model->save();
    }

    public function getField()
    {
        return $this->hasOne(UserCustomField::className(), ['id' => 'field_id']);
    }

    public static function getGridColumns()
    {
        return [
            [
                'class'          => 'yii\grid\SerialColumn',
                'header'         => '<span>#</span>',
                'contentOptions' => [
                    'width' => 50
                ],
            ],
            [
                'attribute' => 'user_id',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'field_id',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'value',
                'format'    => 'raw',
            ],
            [
                'class'    => 'app\components\grid\ActionColumnCustom',
                'template' => function ($model) {
                    return '{edit} {delete}';
                },
                'buttons'  => [
                    'edit'   => function ($url, $model, $key, $options) {
                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/users-custom-values-edit', 'id' => $model->id]);
                        return Html::a('Edit', $url, $options);
                    },
                    'delete' => function ($url, $model, $key, $options) {
                        $options['onclick'] = '
                            if(confirm(\'' . Yii::t('yii', 'Are you sure you want to delete this User?') . '\'))
                            {
                              return true;
                            } else {
                              return false;
                            }
                        ';
//                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/users-custom-values-delete', 'id' => $model->id]);
                        return Html::a('Delete', $url, $options);
                    },

                ],
            ],
        ];
    }


}