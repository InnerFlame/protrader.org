<?php

namespace app\models\user;

use app\models\CustomModel;
use Yii;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Html;
use app\components\MultiSelectCustom;

/**
 * This is the model class for table "user_profile".
 *
 * @property integer $user_id
 * @property string $avatar
 * @property string $first_name
 * @property string $second_name
 * @property string $last_name
 * @property string $address
 * @property string $phone
 * @property string $skype
 * @property integer $birth_day
 *
 * @property User $id0
 */
class UserProfile extends \yii\db\ActiveRecord
{
    public $fullName;

    public static $report_fields = [
        'first_name', 'second_name', 'address', 'phone', 'skype', 'avatar', 'birth_date',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_profile';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_AFTER_FIND => 'fullName',
                    ActiveRecord::EVENT_AFTER_UPDATE => 'fullName'
                ],
                'value' => function() { return $this->getFullName(); }
            ]
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'first_name', 'last_name'], 'required'],
            [['user_id','phone'], 'integer'],
            [['phone'], 'string', 'min' => 12, 'max' => 60],
            [['address'], 'string', 'max' => 255],
            [['phone', 'skype', 'address', 'birth_date'], 'safe'],
            [['skype'], 'string', 'min' => 5, 'max' => 32],
            [['first_name', 'last_name', 'second_name'], 'string', 'min' => 3, 'max' => 16],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'second_name' => 'Second Name',
            'last_name' => 'Last Name',
            'phone' => 'Phone',
            'skype' => 'Skype',
            'address' => 'Address',
            'fullName' => 'Full Name'
        ];
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getFullName()
    {
        return trim($this->first_name . ' ' . $this->last_name);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id']);
    }

}
