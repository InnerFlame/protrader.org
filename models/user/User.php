<?php

namespace app\models\user;

use app\components\Entity;
use app\components\ImageHelper;
use app\components\ImageResize;
use app\components\MailChimpBehaviors;
use app\components\UserSettings;
use app\events\user\RoleChangedEvent;
use app\models\community\Comments;
use app\models\user\crosstrade\CrosstradeSubscriber;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\VarDumper;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "tbl_user".
 *
 * @property integer $id
 * @property string $username
 * @property string $full_name
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $auth_key
 * @property string $email
 * @property integer $role
 * @property integer $status
 * @property string $motto
 * @property string $pictures
 * @property integer $vkontakte_id
 * @property string $linkedin_id
 * @property integer $google_id
 * @property integer $facebook_id
 * @property integer $twitter_id
 * @property integer $birth_at
 * @property string $gmt
 * @property string $country
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 */
class User extends Entity implements IdentityInterface
{
    # findByUsernameOrEmail

use \app\models\user\UserHelper;

    /*
      |--------------------------------------------------------------------------
      | Constants && properties
      |--------------------------------------------------------------------------
     */

    const EVENT_CHANGED_ROLE = 'user.changed.role';
    const ROLE_USER = 0;
    const ROLE_ADMIN = 1;
    const ROLE_MODERATOR = 2;
    const STATUS_UNCONFIRMED = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_BANNED = 2;

    public $profile;
    public $authKey;
    public $password;
    public $new_password;
    public $new_password_repeat;
    public $validatePassword = true;
    public $channel;
    public static $_pull = [];
    private static $passwordResetTokenExpire = 72000;
    public static $eauth_field;
    public $registration_method;

    const SCENARIO_RECOVERY_PASSWORD = 'recovery_password';
    const SCENARIO_EDIT_PROFILE = 'edit_profile';
    const SCENARIO_CONFIRM = 'confirm';
    const SCENARIO_SIGN_UP = 'sign_up';
    const SCENARIO_DELETE = 'delete';

    /*
      |--------------------------------------------------------------------------
      | Model configurations
      |--------------------------------------------------------------------------
     */

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [TimestampBehavior::className(), MailChimpBehaviors::className()];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role', 'status', 'is_ptmc_team', 'vkontakte_id', 'facebook_id', 'twitter_id', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['email'], 'email'],
            [['email', 'username'], 'unique'],
            [['username'], 'required'],
            [['username'], 'match', 'not' => true, 'pattern' => "/\\s/"],
            [['phone'], 'match', 'pattern' => '/[^\w]/ui', 'message' => 'Please dont use special characters'],
            [['phone'], 'string', 'min' => 11, 'max' => 20],
            [['username'], 'string', 'min' => 5],
            [['username', 'fullname', 'password_hash', 'password_reset_token'], 'string', 'max' => 64],
            [['auth_key', 'email', 'linkedin_id', 'google_id'], 'string', 'max' => 32],
            [['motto', 'pictures'], 'string', 'max' => 128],
            [['count_votes'], 'default', 'value' => 5],
//            [['password', 'username'], 'required', 'on' => 'create_from_admin_panel'],
                //[['pictures'], 'file', 'extensions' => 'png, jpg','message' => Yii::t('app', 'file must have only png, jpg extensions')],
        ];
    }

    public function scenarios()
    {
        return [
            'default'                        => ['*'],
            self::SCENARIO_RECOVERY_PASSWORD => ['email'],
            self::SCENARIO_EDIT_PROFILE      => [
                'username', 'fullname', 'password_hash',
                'password_reset_token', 'auth_key', 'email', 'linkedin_id', 'google_id',
                'pictures', 'role', 'status', 'motto', 'deleted'
            ],
            self::SCENARIO_CONFIRM           => ['password'],
            self::SCENARIO_SIGN_UP           => [
                'username', 'fullname', 'password_hash',
                'password_reset_token', 'auth_key', 'email', 'linkedin_id', 'google_id',
                'pictures', 'role', 'status', 'motto', 'deleted', 'is_ptmc_team'
            ],
            self::SCENARIO_DELETE            => [
                'deleted'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                   => Yii::t('app', 'ID'),
            'username'             => Yii::t('app', 'Username'),
            'fullname'             => Yii::t('app', 'Full Name'),
            'password_hash'        => Yii::t('app', 'Password Hash'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'auth_key'             => Yii::t('app', 'Auth Key'),
            'email'                => Yii::t('app', 'Email'),
            'role'                 => Yii::t('app', 'Role'),
            'status'               => Yii::t('app', 'Status'),
            'is_ptmc_team'         => Yii::t('app', 'Is Ptmc_Team'),
            'motto'                => Yii::t('app', 'Motto'),
            'pictures'             => Yii::t('app', 'Pictures'),
            'vkontakte_id'         => Yii::t('app', 'Vkontakte ID'),
            'linkedin_id'          => Yii::t('app', 'Linkedin ID'),
            'google_id'            => Yii::t('app', 'Google ID'),
            'facebook_id'          => Yii::t('app', 'Facebook ID'),
            'twitter_id'           => Yii::t('app', 'Twitter ID'),
            'birth_at'             => Yii::t('app', 'Birth At'),
            'gmt'                  => Yii::t('app', 'GMT'),
            'country'              => Yii::t('app', 'Country'),
            'created_at'           => Yii::t('app', 'Created'),
            'updated_at'           => Yii::t('app', 'Updated'),
            'deleted'              => Yii::t('app', 'Deleted'),
        ];
    }

    public function beforeValidate()
    {
        if (isset($this->birth_at) && !empty($this->birth_at)) {
            $this->birth_at = strtotime($this->birth_at);
        }

        return parent::beforeValidate();
    }

    /*
      |--------------------------------------------------------------------------
      | Model relations
      |--------------------------------------------------------------------------
     */

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComment()
    {
        return $this->hasOne(Comments::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrosstradeSubscriber()
    {
        return $this->hasOne(CrosstradeSubscriber::className(), ['user_id' => 'id'])->andWhere(['status' => CrosstradeSubscriber::STATUS_SUBSCRIBED]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrosstradeRegister()
    {
        return $this->hasOne(CrosstradeSubscriber::className(), ['user_id' => 'id'])->andWhere(['status' => CrosstradeSubscriber::STATUS_REGISTERED]);
    }

    public static function getUsername($id = null)
    {
        $id = is_null($id) ? static::getSafeId() : $id;
        $user = User::findOne($id);

        if (is_object($user)) {
            return $user->username;
        }
    }

    public function getFullname()
    {
        return $this->fullname ? $this->fullname : $this->username;
    }

    public function getProfileLink($id = null)
    {
        if ($id > 0) {
            $user = UserIdentity::findOne($id);

            if (is_object($user))
                $user->getViewUrl();
        }

        return $this->getViewUrl();
    }

    public function getLinkUsername($id = null)
    {
        $user = UserIdentity::findIdentity($id);
        if (is_object($user))
            return Html::a($user->getFullname(), $user->getViewUrl());

        return Html::a($this->getFullname(), $this->getViewUrl());
    }

    public static function getSafeId()
    {
        if (is_object(Yii::$app->user))
            if (is_object(Yii::$app->user->identity))
                return Yii::$app->user->identity->id;

        return null;
    }

    public function getAvatar($width = null, $height = null)
    {
        # for photo from social profile
        if (isset($this->pictures) && (stripos($this->pictures, "http://") === 0))
            return Html::img($this->pictures);

        $full_path = Yii::getAlias('@webroot') . Yii::$app->params['userUploadPath'] . '/' . $this->id . '/' . $this->pictures;
        $path = Yii::$app->params['userUploadPath'] . '/' . $this->id . '/' . $this->pictures;
        if (is_file($full_path)) {
            if (isset($width, $height))
                return ImageResize::getThumbnail($width, $height, $this->id, $this->pictures);

            return Html::img($path);
        }

        return Yii::$app->view->renderFile(Yii::getAlias('@app') . '/views/layouts/components/gravatar/gravatar.twig', [
            'email'    => $this->email,
            'size'     => $width,
            'fullname' => $this->getFullname(),
        ]);
    }

    public function isAdmin()
    {
        return $this->role == (self::ROLE_ADMIN || self::ROLE_MODERATOR) ? TRUE : FALSE;
    }

    public static function getUserList()
    {
        $models = User::find()->all();
        foreach ($models as $model) {
            if ($model->fullname == '') {
                $model->fullname = $model->email;
            }
        }
        return \yii\helpers\ArrayHelper::map($models, 'id', 'fullname');
    }

    /*
      |--------------------------------------------------------------------------
      | Methods
      |--------------------------------------------------------------------------
     */

    public static function getRole($id)
    {
        switch ($id) {
            case self::ROLE_USER:
                return 'user';
            case self::ROLE_ADMIN:
                return 'admin';
            case self::ROLE_MODERATOR:
                return 'moderator';
        }
    }

    public static function getRoleList()
    {
        return [
            self::ROLE_USER      => 'User',
            self::ROLE_ADMIN     => 'Admin',
            self::ROLE_MODERATOR => 'Moderator'
        ];
    }

    public static function getStatus($id)
    {
        switch ($id) {
            case self::STATUS_UNCONFIRMED:
                return 'Unconfirmed';
            case self::STATUS_ACTIVE:
                return 'Active';
            case self::STATUS_BANNED:
                return 'Banned';
        }
    }

    public static function getStatusList()
    {
        return [
            self::STATUS_UNCONFIRMED => 'Unconfirmed',
            self::STATUS_ACTIVE      => 'Active',
            self::STATUS_BANNED      => 'Banned',
        ];
    }

    public static function getList($except = null)
    {
        $users = User::find()->all();

        //if username have no fullname
        foreach ($users as $key => $user) {
            if (!$user['fullname'])
                $users[$key]['fullname'] = $user['username'];
        }

        return ArrayHelper::map($users, 'id', 'fullname');
    }

    public function getName()
    {
        return $this->email;
    }

    public function getViewUrl()
    {
        return '/user/id' . $this->id;
    }

    public function getTypeEntity()
    {
        return Entity::USER;
    }

    /**
     * 
     * @param integer $exceptId feature id which shouldn't display in the list
     * @return array
     */
    public function getUserFeauters($exceptId = null, $limit = 5, $entity_type = Entity::FEATURES_REQUEST)
    {
        $fr_table_name = \app\modules\features\models\FeatureRequest::tableName();
        $vt_table_name = \app\components\widgets\vote\models\Vote::tableName();
        $feature_status = \app\modules\features\models\FeatureRequest::STATUS_VOTING;
        $command = "SELECT fr.id, fr.title, fr.user_id, sum(count) as votes
            from $fr_table_name as fr
            left join (select * from $vt_table_name) as vt
            on fr.id = vt.entity_id

            where fr.user_id = $this->id and fr.deleted = 0 and fr.status = '$feature_status'";


        if ($entity_type) {
            $command .= " and vt.entity = $entity_type";
        }

        if ($exceptId) {
            $command .= " and fr.id <> $exceptId";
        }

        $command .= " group by fr.id
            order by votes desc";

        if ($limit) {
            $command .= " limit $limit";
        }

        $query = Yii::$app->db->createCommand($command);

        return $query->queryAll();
    }

    /**
     * @return string
     */
    public static function getCountry()
    {
        $response = json_decode(file_get_contents('http://ip-api.com/json/' . $_SERVER['REMOTE_ADDR']));

        return (is_object($response) && $response->status != 'fail') ? $response->country : null;
    }

    public function registration($type = 'ptmc')
    {
        $this->auth_key = md5(microtime() . rand());

        $this->username = explode("@", $this->email)[0];
        $this->role = User::STATUS_UNCONFIRMED;
        $this->gmt = Yii::$app->request->post('SignUp')['gmt'];
        $this->country = self::getCountry();

        if ($this->save()) {
            ($type == 'ptmc') ? $this->sendConfirmMail($this->email, $this->auth_key) : $this->sendConfirmMail($this->email, $this->auth_key, $type);

            Yii::$app->session->setFlash('success', Yii::t('app', 'Link to confirm registration sent to your e-mail'));
            return true;
        } else {
            Yii::$app->session->setFlash('danger', 'This e-mail address already exists.');
            return false;
        }
    }

    public function isPermitted()
    {
        var_dump($this);
        die();
//        UserSettings::findOne(['user_id' => ]);
    }

    public static function getGridColumns()
    {
        return [
            [
                'class'          => 'yii\grid\SerialColumn',
                'header'         => '<span>#</span>',
                'contentOptions' => [
                    'width' => 50
                ],
            ],
            [
                'attribute' => 'created_at',
                'format'    => 'date',
            ],
            [
                'attribute' => 'email',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'username',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'fullname',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'role',
                'value'     => function ($model) {
                    return self::getRole($model->role);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'status',
                'value'     => function ($model) {
                    return self::getStatus($model->status);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'registration_method',
                'format'    => 'raw',
                'value'     => function ($model) {
                    if (strlen($model->linkedin_id) > 3)
                        return 'linkedin';
                    if (strlen($model->google_id) > 3)
                        return 'google';
                    if (strlen($model->facebook_id) > 3)
                        return 'facebook';
                    if (strlen($model->twitter_id) > 3)
                        return 'twitter';
                    return 'mail';
                }
            ],
            [
                'class'    => 'app\components\grid\ActionColumnCustom',
                'template' => function ($model) {
                    return '{edit} {delete}';
                },
                'buttons' => [
                    'edit' => function ($url, $model, $key, $options) {
                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/user/edit', 'id' => $model->id]);
                        return Html::a('Edit', $url, $options);
                    },
                            'delete'  => function ($url, $model, $key, $options) {
                        $options['onclick'] = '
                            if(confirm(\'' . Yii::t('yii', 'Are you sure you want to delete this User?') . '\'))
                            {
                              return true;
                            } else {
                              return false;
                            }
                        ';
//                        $options['class'] = (isset($options['class']) ? $options['class'] : '') . ' account-action';
                        $url = Yii::$app->urlManager->createUrl(['/cabinet/user/delete', 'id' => $model->id]);
                        return Html::a('Delete', $url, $options);
                    },
                        ],
                    ],
                ];
            }

            /**
             * @return UserSettings
             */
            public function getSettings()
            {
                return new UserSettings(['user_id' => $this->id]);
            }

            /**
             * @inheritdoc
             * @return UserQuery the active query used by this AR class.
             */
            public static function find()
            {
                return new UserQuery(get_called_class());
            }

            /**
             * Return all users role administrator and modreator
             * @return array|\app\models\user\User[]
             */
            public static function findAllModerators()
            {
                return User::find()->where(['in', 'role', [User::ROLE_ADMIN, User::ROLE_MODERATOR]])->all();
            }

            /**
             * Return all users moderator and admin role except me
             * @return array|\app\models\user\User[]
             */
            public static function findAllModeratorsExId($user_id = null)
            {
                if (!$user_id) {
                    $user_id = Yii::$app->user->identity->id;
                }
                return User::find()->where(['in', 'role', [User::ROLE_ADMIN, User::ROLE_MODERATOR]])->andWhere('id != :id', [':id' => $user_id])->all();
            }

            public function afterSave($insert, $changedAttributes)
            {
                if ($insert) {
                    parent::afterSave($insert, $changedAttributes);
                }
                //Если была изменена роль
                if (isset($changedAttributes['role'])) {
                    $this->trigger(self::EVENT_CHANGED_ROLE, new RoleChangedEvent([
                        'oldRole' => $changedAttributes['role'],
                        'newRole' => $this->role,
                        'sender'  => $this
                    ]));
                }
            }

            public function getMethodRegistration()
            {
                if ($this->facebook_id)
                    return Yii::t('app', 'Facebook');

                if ($this->linkedin_id)
                    return Yii::t('app', 'Linkedin');

                if ($this->google_id)
                    return Yii::t('app', 'Google');

                return Yii::t('app', 'Email');
            }

        }
        