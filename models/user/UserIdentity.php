<?php
/**
 * Created by PhpStorm.
 * @author: Roman Budnitsky
 * @date: 03.11.14 17:26
 * @copyright Copyright (c) 2014 PFSOFT LLC
 *
 * Class UserIdentity
 * @package app\models\user
 */

namespace app\models\user;

use app\components\customImage\CustomImage;
use app\components\Entity;
use app\components\MailChimpBehaviors;
use app\models\user\crosstrade\CrosstradeSubscriber;
use yii;
use yii\base\NotSupportedException;
use yii\web\IdentityInterface;
use yii\helpers\ArrayHelper;

class UserIdentity extends User implements IdentityInterface
{

    /*
    |--------------------------------------------------------------------------
    | Model constant and properties
    |--------------------------------------------------------------------------
    |
    | Here is where you can write all of the constants and properties for an model.
    |
    */
    public $image = null;
    public $crop = null;

    /*
     * Field name to record in database
     */
    const VKONTACTE_FIELD = 'vkontakte';

    /*
     * Field name to record in database
     */
    const LINKEDIN_FIELD = 'linkedin';

    /*
     * Field name to record in database
     */
    const GOOGLE_FIELD = 'google';

    /*
     * Field name to record in database
     */
    const FACEBOOK_FIELD = 'facebook';

    /*
     * Field name to record in database
     */
    const TWITTER_FIELD = 'twitter';

    const EVENT_BEFORE_VALIDATE_PASSWORD = 'beforeValidatePassword';

    const STATUS_ACTIVE = 1;

    /*
    |--------------------------------------------------------------------------
    | Model configuration
    |--------------------------------------------------------------------------
    |
    | Here is where you can write all of the configuration for an model.
    |
    */

    /**
     * Model Rules
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['new_password', 'new_password_repeat'], 'required', 'on' => ['insert', 'changePassword']],
            // [['pictures'], 'safe'],
            // [['pictures'], 'file',  'skipOnEmpty' => false, 'extensions' => 'png, jpg', 'message' => 'You can upload images with type (.png, .jpg) and no more than 2 mb'],


            [['new_password'],
                'match',//'new_password_repeat'
                'pattern' => "/^[0-9a-zA-Z]{5,16}$/",
                'message' => 'The password must contain 5 to 16 characters, numbers'
            ],
            [['password'], 'required', 'on' => ['changePassword'],
                'when' => function ($model) {
                    return $model->id == Yii::$app->user->getId();
                }],
            ['new_password_repeat', 'compare', 'compareAttribute' => 'new_password', 'on' => ['insert', 'changePassword']],


            [['role', 'status', 'is_ptmc_team', 'vkontakte_id', 'facebook_id', 'twitter_id', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['email'], 'email'],
            [['email', 'username'], 'unique'],
            [['username', 'email'], 'required'],
            [['username'], 'match', 'not' => true, 'pattern' => "/\\s/"],
            [['phone'], 'match', 'pattern' => '/[^\w]/ui', 'message' => 'Please dont use special characters'],
            [['phone'], 'string', 'min' => 11, 'max' => 20],
            [['username'], 'string', 'min' => 5],
            [['username', 'fullname', 'password_hash', 'password_reset_token'], 'string', 'max' => 64],
            [['auth_key', 'email', 'linkedin_id', 'google_id'], 'string', 'max' => 32],
            [['motto', 'pictures'], 'string', 'max' => 128],

        ]);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [MailChimpBehaviors::className()];
    }

    /**
     * Attribute Labels

     * @return array
     */
    public function attributeLabels()
    {
        return array_merge([
            'password' => $this->getScenario() == 'changePassword' ? 'Old Password' : 'Password',
            'new_password' => 'New Password',
            'new_password_repeat' => 'New Password Repeat',
        ], parent::attributeLabels());
    }


    /*
    |--------------------------------------------------------------------------
    | Model relations
    |--------------------------------------------------------------------------
    |
    | Here is where you can write all of the relations for an model.
    |
    */

    /**
     * @return yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'id']);
    }

    /**
     * @return $this
     */
    public static function find()
    {
        return parent::find()->with(['profile']);
    }


    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'deleted' => Entity::NOT_DELETED]);//
    }


    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by username or email
     *
     * @param string $attr
     *
     * @return static|null
     */
    public static function findByUsernameOrEmail($attr)
    {
        return static::find()
            ->andWhere("username = :attr OR email = :attr")
            ->params([':attr' => $attr])->one();
    }

    /**
     * @inheritdoc
     *
     * @param \nodge\eauth\ServiceBase $service
     * @return User
     * @throws ErrorException
     */
    public static function findByEAuth($service)
    {
        if (!$service->getIsAuthenticated()) {
            throw new ErrorException('EAuth user should be authenticated before creating identity.');
        }

        $id = $service->getServiceName() . '-' . $service->getId();
        $attributes = array(
            'id'       => $id,
            'username' => $service->getAttribute('name'),
            'authKey'  => md5($id),
            'profile'  => $service->getAttributes(),
        );

        $attributes['profile']['service'] = $service->getServiceName();
        Yii::$app->getSession()->set('user-' . $id, $attributes);

        return new self($attributes);

    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        //$expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $expire = static::$passwordResetTokenExpire;
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        if ($timestamp + $expire >= time())
            return true;
        return false;
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isAuthTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        return false;
    }

    /**
     * Validates password
     *
     * @param $password
     * @param bool $clearPasswordFields
     * @return bool
     */
    public function validatePassword($password, $clearPasswordFields = true)
    {
        $this->password = $password;
        $this->trigger(self::EVENT_BEFORE_VALIDATE_PASSWORD);
        if($clearPasswordFields) {
            $this->password = null;
            $this->new_password = null;
            $this->new_password_repeat = null;
        }
        if($this->validatePassword) {
            return Yii::$app->security->validatePassword($password, $this->password_hash);
        } else {
            return false;
        }
    }

    /**
     * Generates password hash from password and sets it to the model
     * @param string $password
     * @return void
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
        $this->password = $password;
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Insert eauth data
     *
     * @param $identity
     * @param $eauth_field
     * @return User|array|null|yii\db\ActiveRecord
     */
    public static function insertEauth($identity, $eauth_field)
    {
        $model = new User();
        $model->$eauth_field = $identity->profile['id'];
        $model->fullname = $identity->profile['name'];
        $model->email = isset($identity->profile['email']) ? $identity->profile['email'] : null;
        $model->username = explode("@", $model->email)[0];
        $model->status = self::STATUS_ACTIVE; // active user
        $model->save(false);

        /**
         *
         * addded
         */
        $profile = new UserProfile();

        if (isset($identity->profile['name']))
        list($profile->first_name, $profile->last_name) = explode(" ", $identity->profile['name']);

        $profile->user_id = $model->id;
        $profile->save(false);
        /**
         * addded
         */

        return $model;
    }

    /**
     *
     * Update eauth data
     * @param $identity
     * @param $model
     * @param $eauth_field
     * @return User|array|null|yii\db\ActiveRecord
     */
    public static function updateEauth($identity, $model, $eauth_field)
    {
        $model->$eauth_field = $identity->profile['id'];

        if(empty($model->fullname)){

            $model->fullname = $identity->profile['name'];
        }
        $model->status = self::STATUS_ACTIVE; // active user
        $model->save(false);


        $profile = UserProfile::find()->where(['user_id' => $model->id])->one();

        if (!is_object($profile))
            $profile = new UserProfile();

        list($profile->first_name, $profile->last_name) = explode(" ", $identity->profile['name']);
        $profile->user_id = $model->id;
        $profile->save(false);


        return $model;
    }

    /**
     * @param $identity
     * @param $eauth_field
     * @return mixed
     */
    public static function isEauthIsset($identity, $eauth_field, $eauth = null)
    {
        $condition = [$eauth_field => $identity->profile['id']];
        $model = User::find()->andWhere($condition);
        $update_service_id = false;
        if(isset($eauth) && isset($eauth->email)){
            $model->orWhere(['email' => $eauth->email]);

            $update_service_id = true;
        }

        if($model = $model->one()){
            if($update_service_id && $model->$eauth_field == 0 || empty($model->$eauth_field))
                $model->$eauth_field = $identity->profile['id'];
                $model->save(false);

            return $model;

        }else{
            return false;
        }
    }

    /**
     * Get eauth field to insert into database
     *
     * @param $service_name
     * @return string
     */
    public static function getEauthField($service_name)
    {
        $_id = '_id';

        switch (true){
            case stristr($service_name, self::VKONTACTE_FIELD):
                return self::VKONTACTE_FIELD . $_id;
            case stristr($service_name, self::LINKEDIN_FIELD):
                return self::LINKEDIN_FIELD . $_id;
            case stristr($service_name, self::GOOGLE_FIELD):
                return self::GOOGLE_FIELD . $_id;
            case stristr($service_name, self::FACEBOOK_FIELD):
                return self::FACEBOOK_FIELD . $_id;
            case stristr($service_name, self::TWITTER_FIELD):
                return self::TWITTER_FIELD . $_id;
        }
    }

    public function getFullName()
    {
        return $this->fullname ? $this->fullname : $this->username;
    }


    public function init()
    {
        $this->image = new CustomImage([
            'uploadPath' => isset(Yii::$app->params['userUploadPath']) ?
                Yii::getAlias('@webroot') . Yii::$app->params['userUploadPath'] : '',
            'uploadUrl' => isset(Yii::$app->params['userUploadPath']) ?
                Yii::getAlias('@web') . Yii::$app->params['userUploadPath'] : '',
            'model' => $this,
        ]);

        $this->image->setValidation('pictures', [
            'extensions' => 'png,jpg',
            'maxHeight' => 4000,
            'maxWidth' => 4000,
            'maxSize' => 2000,
            'minSize' => 1,
        ]);

        parent::init();
    }


    public function beforeSave($insert)
    {
        // echo '<pre>'; var_dump($this->pictures);
        if(!$this->image->isChanged('pictures')) unset($this->pictures);
        if($this->image->isDeleted('pictures')) $this->pictures = '';
        // echo '<pre>'; var_dump($this->pictures);die;
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if($this->scenario <> 'delete' && $this->scenario <> 'updateImage')
        {
            if ($this->image->getUploadedFile('pictures')) {

                $this->pictures = $this->image->uploadImage('pictures');

                $this->scenario = 'updateImage';

                $this->save(false);

            }

        }

        return parent::afterSave($insert, $changedAttributes);
    }
} 