<?php

return [
    'components' => [
        'eauth' =>  [
            'class'       => 'nodge\eauth\EAuth',
            'popup'       => true, // Use the popup window instead of redirecting.
            'cache'       => false, // Cache component name or false to disable cache. Defaults to 'cache' on production environments.
            'cacheExpire' => 0, // Cache lifetime. Defaults to 0 - means unlimited.
            'httpClient'  => [
                // uncomment this to use streams in safe_mode
                //'useStreamsFallback' => true,
            ],

            'services'    => [ // You can change the providers and their classes.

//********************PROTRADER.LAN**********************************

                'facebook' => [
                    // register your app here: https://developers.facebook.com/apps/
                    'class' => 'app\components\eauth\CustomFacebookOAuth2Service',
                    'clientId' => '1755778844657690',
                    'clientSecret' => 'f1f5eb42591e0965c6ca32b718026841',
                    'title' => '',
                ],
//                'facebook' => [
//                    // register your app here: https://developers.facebook.com/apps/
//                    'class' => 'app\components\eauth\CustomFacebookOAuth2Service',
//                    'clientId' => '1695557054033521',
//                    'clientSecret' => '43ac407f05a40259b3aeee56e258ade9',
//                    'title' => '',
//                ],
//
//                'google_oauth' => [
//                    // register your app here: https://code.google.com/apis/console/
//                    //http://joxi.ru/DmBlpLXFN4Jy6A
//                    'class' => 'nodge\eauth\services\GoogleOAuth2Service',
//                    'clientId' => '231314303082-i56jrrlrpfqih76kesq6r2ivik5svfod.apps.googleusercontent.com',
//                    'clientSecret' => 'l328JqDCxhZm2893-BOvEeXW',
//                    'title' => '',
//                ],
////
//                'twitter' => [
//                    // register your app here: https://dev.twitter.com/apps/new
//                    'class' => 'nodge\eauth\services\TwitterOAuth1Service',
//                    'key' => 'Fd8SFgCCnpEtgknw0Tkc0vpll',
//                    'secret' => 'hHX0npW8PuftcYbpUoqGgoqKiCiBJPCcWKk0xqjAg7ttbQCHLc',
//                    'title' => '',
//                ],

                'linkedin_oauth2' => [
                    // register your app here: https://www.linkedin.com/secure/developer
                    'class' => 'app\components\eauth\CustomLinkedinOAuth2Service',
                    'clientId' => '77qgg483w1wk7c',
                    'clientSecret' => '8RH2V1Um5m8xak8O',
                    'title' => '',
                ],
//********************PROTRADER.LAN**********************************






            ],
        ],
    ]
];
