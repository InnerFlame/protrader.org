<?php

return [
    'components' => [
        'eauth' =>  [
            'class'       => 'nodge\eauth\EAuth',
            'popup'       => true, // Use the popup window instead of redirecting.
            'cache'       => false, // Cache component name or false to disable cache. Defaults to 'cache' on production environments.
            'cacheExpire' => 0, // Cache lifetime. Defaults to 0 - means unlimited.
            'httpClient'  => [
                // uncomment this to use streams in safe_mode
                //'useStreamsFallback' => true,
            ],
            'services'    => [ // You can change the providers and their classes.

//********************PROTRADER.ORG**********************************
//                //PROTRADER.ORG
//                'google_oauth' => [
//                    // register your app here: https://code.google.com/apis/console/
//                    //http://joxi.ru/DmBlpLXFN4Jy6A
//                    'class' => 'nodge\eauth\services\GoogleOAuth2Service',
//                    'clientId' => '522096019583-ca1tnui5c9nv8rijqem80a7sdfnsa4pq.apps.googleusercontent.com',
//                    'clientSecret' => 'f7ruBDfjtGwXNrJLHcvFLpqx',
//                    'title' => '',
//                ],
//
//                //PROTRADER.ORG
//                'twitter' => [
//                    // register your app here: https://dev.twitter.com/apps/new
//                    'class' => 'nodge\eauth\services\TwitterOAuth1Service',
//                    'key' => 'Fd8SFgCCnpEtgknw0Tkc0vpll',
//                    'secret' => 'hHX0npW8PuftcYbpUoqGgoqKiCiBJPCcWKk0xqjAg7ttbQCHLc',
//                    'title' => '',
//                ],


                //  protrader.org
                'facebook' => [//http://joxi.ru/Grq1oXMiNG4jVr
                    // register your app here: https://developers.facebook.com/apps/
                    'class' => 'app\components\eauth\CustomFacebookOAuth2Service',
                    'clientId' => '1057992947555906',
                    'clientSecret' => 'ca62561b0127f4ec6318b943ffd4e9f9',
                    'title' => '',
                ],

                'linkedin_oauth2' => [
                    // register your app here: https://www.linkedin.com/secure/developer
                    'class' => 'app\components\eauth\CustomLinkedinOAuth2Service',
                    'clientId' => '7794g331hndunl',//new protrader org
                    'clientSecret' => '2nAJl9Qbcn43R765',//new protrader org
                    'title' => '',
                ],
//********************PROTRADER.ORG**********************************


//********************PROTRADER.My**********************************
//                'vkontakte' => [// local protrader.my
//                    // register your app here: https://vk.com/editapp?act=create&site=1
//                    'class' => 'app\components\eauth\CustomVKontakteOAuth2Service',
//                    'clientId' => '5208103',
//                    'clientSecret' => 'q3JVvlw6VFhqtSDSTHrQ',
//                    'title' => '',
//                ],
//********************PROTRADER.My**********************************



            ],
        ],

    ]
];
