<?php
/**
 * Created by PhpStorm.
 * @author: Roman Budnitsky
 * @date: 27.01.15 12:51
 * @copyright Copyright (c) 2015 PFSOFT LLC
 */
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=protrader',
            'username' => 'root',
            'password' => '1111',
            'charset' => 'utf8',
            'enableSchemaCache' => true,
        ],
//        'cache' => [
//            'class' => 'yii\redis\Cache',
//            'redis' => [
//                'hostname' => '<hostname or ip>',
//                'port' => '<port>',
//                'database' => 0,
//            ],
//        ],
    ]
];