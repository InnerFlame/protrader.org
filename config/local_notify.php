<?php
/**
 * Created by PhpStorm.
 * @author: Roman Budnitsky
 * @date: 27.01.15 12:51
 * @copyright Copyright (c) 2015 PFSOFT LLC
 */
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=protrader_notify',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
            'enableSchemaCache' => true,
        ],
        'db_kbase' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=192.168.0.208;dbname=protrader_org',
            'username' => 'root',
            'password' => 'pfs222',
            'charset' => 'utf8',
            'enableSchemaCache' => true,
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => true,
        ],
//        'cache' => [
//            'class' => 'yii\redis\Cache',
//            'redis' => [
//                'hostname' => '<hostname or ip>',
//                'port' => '<port>',
//                'database' => 0,
//            ],
//        ],
    ]
];