<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id'               => 'salary-portal',
    'homeUrl'          => ['/'],
    'basePath'         => dirname(__DIR__),
    'timeZone'         => 'Europe/Kiev',
    'on beforeRequest' => ['app\components\HandleRequest', 'beforeRequest'],
    //'defaultRoute' => '/pages/about-us',
    // 'catchAll' => ['/about-us'],
    'bootstrap' => ['log', 'loader', 'notify', 'cabinet', 'forum', 'articles', 'news', 'brokers', 'kbase_org', 'features', 'codebase', 'crosstrade'],
    'controllerMap' => [
        'comments' => [
            'class' => 'app\components\customComments\CommentsController'
        ],
        'vote'     => [
            'class' => 'app\components\widgets\vote\VoteController'
        ],
        'chart'    => [
            'class' => 'app\components\widgets\charts\ChartController'
        ],
        'redirect' => [
            'class' => 'app\components\widgets\error\RedirectController'
        ]
    ],
    'aliases'       => [
        '@views'   => '@app/views',
        '@mail'    => '@app/views/mail/layouts',
        '@widgets' => '@app/widgets',
    ],
    'modules'       => [

        'kbase_org' => [
            'class' => 'app\modules\kbase_org\Module',
        ],
        'features'  => [
            'class' => 'app\modules\features\Module',
        ],
        'forum'     => [
            'class' => 'app\modules\forum\ForumModule'
        ],
        'cabinet' => [
            'class' => 'app\modules\cabinet\CabinetModule'
        ],
        'articles' => [
            'class' => 'app\modules\articles\ArticlesModule'
        ],
        'news' => [
            'class' => 'app\modules\news\NewsModule'
        ],
        'brokers'  => [
            'class' => 'app\modules\brokers\BrokersModule'
        ],
        'codebase' => [
            'class' => 'app\modules\codebase\Module',
        ],
        'notify'   => [
            'class' => 'app\modules\notify\Module',
        ],
        'crosstrade' => [
            'class' => 'app\modules\crosstrade\Module',
        ],
        'sitemap' => [
            'class'       => 'himiklab\sitemap\Sitemap',
            'models'      => [
                'app\modules\articles\models\Articles',
                'app\modules\brokers\models\Improvement',
                'app\modules\brokers\models\Broker',
                'app\modules\forum\models\FrmTopic',
                'app\modules\forum\models\FrmCategory',
                'app\modules\codebase\models\Description',
                'app\modules\codebase\models\Category',
                'app\modules\features\models\FeatureRequest'
            ],
            'urls'        => [
                [
                    'loc'        => 'http://protrader.org',
                    'lastmod'    => date('Y-m-d\TH:i:sP', 1452590201),
                    'changefreq' => \himiklab\sitemap\behaviors\SitemapBehavior::CHANGEFREQ_DAILY,
                    'priority'   => 1,
                ],
                [
                    'loc'        => '/download',
                    'lastmod'    => date('Y-m-d\TH:i:sP', 1452590201),
                    'changefreq' => \himiklab\sitemap\behaviors\SitemapBehavior::CHANGEFREQ_WEEKLY,
                    'priority'   => 0.6,
                ],
                [
                    'loc'        => '/faq',
                    'lastmod'    => date('Y-m-d\TH:i:sP', 1452590201),
                    'changefreq' => \himiklab\sitemap\behaviors\SitemapBehavior::CHANGEFREQ_WEEKLY,
                    'priority'   => 0.6,
                ],
                [
                    'loc'        => '/forum',
                    'lastmod'    => date('Y-m-d\TH:i:sP', 1452590201),
                    'changefreq' => \himiklab\sitemap\behaviors\SitemapBehavior::CHANGEFREQ_WEEKLY,
                    'priority'   => 0.6,
                ],
                [
                    'loc'        => '/features',
                    'lastmod'    => date('Y-m-d\TH:i:sP', 1452590201),
                    'changefreq' => \himiklab\sitemap\behaviors\SitemapBehavior::CHANGEFREQ_WEEKLY,
                    'priority'   => 0.6,
                ],
                [
                    'loc'        => '/brokers',
                    'lastmod'    => date('Y-m-d\TH:i:sP', 1452590201),
                    'changefreq' => \himiklab\sitemap\behaviors\SitemapBehavior::CHANGEFREQ_WEEKLY,
                    'priority'   => 0.6,
                ],
                [
                    'loc'        => '/articles',
                    'lastmod'    => date('Y-m-d\TH:i:sP', 1452590201),
                    'changefreq' => \himiklab\sitemap\behaviors\SitemapBehavior::CHANGEFREQ_WEEKLY,
                    'priority'   => 0.6,
                ],
                [
                    'loc'        => '/articles/analytics',
                    'lastmod'    => date('Y-m-d\TH:i:sP', 1452590201),
                    'changefreq' => \himiklab\sitemap\behaviors\SitemapBehavior::CHANGEFREQ_WEEKLY,
                    'priority'   => 0.6,
                ],
                [
                    'loc'        => '/articles/reviews',
                    'lastmod'    => date('Y-m-d\TH:i:sP', 1452590201),
                    'changefreq' => \himiklab\sitemap\behaviors\SitemapBehavior::CHANGEFREQ_WEEKLY,
                    'priority'   => 0.6,
                ],
                [
                    'loc'        => '/articles/ptmc-updates',
                    'lastmod'    => date('Y-m-d\TH:i:sP', 1452590201),
                    'changefreq' => \himiklab\sitemap\behaviors\SitemapBehavior::CHANGEFREQ_WEEKLY,
                    'priority'   => 0.6,
                ],
                [
                    'loc'        => '/articles/videos',
                    'lastmod'    => date('Y-m-d\TH:i:sP', 1452590201),
                    'changefreq' => \himiklab\sitemap\behaviors\SitemapBehavior::CHANGEFREQ_WEEKLY,
                    'priority'   => 0.6,
                ],
                [
                    'loc'        => '/codebase',
                    'lastmod'    => date('Y-m-d\TH:i:sP', 1452590201),
                    'changefreq' => \himiklab\sitemap\behaviors\SitemapBehavior::CHANGEFREQ_WEEKLY,
                    'priority'   => 0.6,
                ],
                [
                    'loc'        => '/feature-request',
                    'lastmod'    => date('Y-m-d\TH:i:sP', 1452590201),
                    'changefreq' => \himiklab\sitemap\behaviors\SitemapBehavior::CHANGEFREQ_WEEKLY,
                    'priority'   => 0.6,
                ],
                [
                    'loc'        => '/feature-request/development',
                    'lastmod'    => date('Y-m-d\TH:i:sP', 1452590201),
                    'changefreq' => \himiklab\sitemap\behaviors\SitemapBehavior::CHANGEFREQ_WEEKLY,
                    'priority'   => 0.6,
                ],
                [
                    'loc'        => '/feature-request/released',
                    'lastmod'    => date('Y-m-d\TH:i:sP', 1452590201),
                    'changefreq' => \himiklab\sitemap\behaviors\SitemapBehavior::CHANGEFREQ_WEEKLY,
                    'priority'   => 0.6,
                ],
            ],
            'enableGzip'  => true, // default is false
            'cacheExpire' => 1, // 1 second. Default is 24 hours
        ],
    ],
    'components'    => [
//        'eventListener' => [
//            'class' => 'app\components\events\EventListener',
//        ],
        'request'       => [
            'cookieValidationKey' => 'G-bq-WXW5zLrCpL9VVwKfjRP5pI8GYie',
            'enableCookieValidation' => false,
        ],
        'xmpp'          => [
            'class' => 'app\components\Xmpp'
        ],
        'urlManager'    => [
            'showScriptName'  => false,
            'enablePrettyUrl' => true,
            'rules'           => [

                /*
                  |--------------------------------------------------------------------------
                  | Base
                  |--------------------------------------------------------------------------
                 */


                'sitemap.xml'                                                                                          => 'sitemap/default/index',
                '/'                                                                                                    => 'pages/index',
                '<action:en|ru>'                                                                                       => 'i18n/<action>',
                '<action:login|logout|forgot|recovery|sign-up|edit-profile|confirm|recovery-password|change-password>' => 'user/<action>',
                '/user/settings'                                                                                       => '/user/settings',
                '/user/delete' => '/user/delete',
                '/user/<user_id:id\d+>' => 'user/profile',
                '/user' => 'user/user',
                # Pages
                '/privacy-policy' => 'pages/index',
                # Site
                '/download'     => 'site/download',
                '/crosstrade'   => 'site/crosstrade',
                'demo/<action>' => 'site/demo',
//                '/documentation'                          => 'site/kbase-index',
                'ajax/<method>' => 'ajax/index',
            ],
        ],
        'assetManager' => [
            'bundles' => [

                'yii\bootstrap\BootstrapPluginAsset' => [
                    'js' => []
                ],
            ],
        ],
        'view'         => [
            'class'            => 'app\components\web\View',
            'defaultExtension' => 'twig',
            'renderers'        => [
                'twig' => [
                    'class'      => 'yii\twig\ViewRenderer',
                    'cachePath'  => '@runtime/twig/cache',
                    // Array of twig options:
                    'options'    => [
                        'debug'       => true,
                        'auto_reload' => true,
                    ],
                    'extensions' => [
                        '\Twig_Extension_Debug',
                        'app\components\twig\BaseView',
                        'app\components\twig\CustomFilters'
                    ],
                    'globals'    => [
                        'html' => '\yii\helpers\Html',
                        'url'  => '\yii\helpers\Url',
                        'str'  => '\yii\helpers\StringHelper',
                        'arr'  => '\yii\helpers\ArrayHelper'
                    ],
                    'functions'  => [
                        'get_class'    => 'get_class',
                        'preg_replace' => 'preg_replace'
                    ],
                    'uses'       => [
                        'app\assets',
                        'app\components',
                        'yii\bootstrap',
                    ],
                ],
            ],
        ],
        'user'         => [
            'identityClass'   => 'app\models\user\UserIdentity',
            'enableAutoLogin' => true,
            'loginUrl'        => '/'
        ],
        'loader'          => [
            'class' => 'app\components\ModulesLoader',
        ],
        'authManager'     => [
            'class' => 'app\components\rbac\RbacManager',
        ],
        'errorHandler'    => [
            'errorAction' => 'site/error',
        ],
        'search'          => [
            'class' => 'app\components\search\Search',
        ],
        'counter'         => [
            'class' => 'app\components\count\Counter',
        ],
        'sort'            => [
            'class' => 'app\components\sort\Sortable',
        ],
        'counter_like'    => [
            'class' => 'app\components\likes\CounterLike',
        ],
        'highlight'       => [
            'class' => 'app\components\highlight\Highlight'
        ],
        'notification'    => [
            'class' => 'app\components\notification\Notification'
        ],
        'mailer'          => [
            'class'     => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class'    => 'Swift_SmtpTransport',
                'host'     => 'smtp.gmail.com', // e.g. smtp.mandrillapp.com or smtp.gmail.com
                'username' => 'info@protrader.org',
                'password' => 'cg%%478dhsW',
                'port'       => '587', // Port 25 is a very common port too
                'encryption' => 'tls', // It is often used, check your provider or mail server specs
            ],
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
        ],
        'log'             => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'pdf'             => [
            'class' => 'broobe\yii2Dompdf\ResponseFormatter',
        ],
        'customFunctions' => [
            'class' => 'app\components\CustomFunctions',
        ],
        // 'db' => require(__DIR__ . '/db.php'),
        'i18n'      => [
            'translations' => [
                'eauth' => [
                    'class'    => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@eauth/messages',
                ],
            ],
        ],
        'mailqueue' => [
            'class'         => 'nterms\mailqueue\MailQueue',
            'table'         => '{{%mail_queue}}',
            'mailsPerRound' => 10,
            'maxAttempts'   => 3,
            'transport'     => [
                'class'      => 'Swift_SmtpTransport',
                'host'       => 'smtp.gmail.com', // e.g. smtp.mandrillapp.com or smtp.gmail.com
                'username'   => 'info@protrader.org',
                'password'   => 'cg%%478dhsW',
                'port'       => '587', // Port 25 is a very common port too
                'encryption' => 'tls', // It is often used, check your provider or mail server specs
            ],
        ],
        'formatter' => [
            'dateFormat' => 'dd.MM.yyyy',
            'decimalSeparator' => ',',
            'thousandSeparator' => ' ',
            'currencyCode' => 'en-US',
        ],
    ],
    'params'        => $params,
];
$config['params']['addthis'] = [
    'enable'       => true,
    'blockUrl'     => '',
    'permanentUrl' => '//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b1c86d8de0add6',
    'followUrl' => '//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b1c86d8de0add6'
];

if (YII_ENV_DEV || $_SERVER['HTTP_HOST'] == 'protrader.pfsoft.lan') {
//     configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
    $config['components']['log']['targets'][] = [
        'class'       => 'yii\log\FileTarget',
        'levels'      => ['error'],
        'categories'  => ['notify'],
        'logFile'     => '@app/runtime/logs/notify.log',
        'logVars'     => [],
        'maxFileSize' => 1024 * 2,
        'maxLogFiles' => 20,
    ];
}


if ($_SERVER['HTTP_HOST'] == 'protrader.org')
    $config = \yii\helpers\ArrayHelper::merge($config, require(__DIR__ . '/_social_protrader_org.php'));
elseif ($_SERVER['HTTP_HOST'] == 'protrader.loc')
    $config = \yii\helpers\ArrayHelper::merge($config, require(__DIR__ . '/social_local.php'));
elseif ($_SERVER['HTTP_HOST'] == 'test.protrader.org')
    $config = \yii\helpers\ArrayHelper::merge($config, require(__DIR__ . '/social_protrader_pfsoft_test_org.php'));
else
    $config = \yii\helpers\ArrayHelper::merge($config, require(__DIR__ . '/social_protrader_pfsoft_lan.php'));


$config = \yii\helpers\ArrayHelper::merge($config, require(__DIR__ . '/local.php'));

//$config = \yii\helpers\ArrayHelper::merge($config, require(__DIR__ . '/../modules/kbase/config/kbase.php'));
if (!YII_ENV_DEV) {
    $config['components']['assetManager'] = [
        'bundles'         => require(__DIR__ . '/prod.assets.php'),
        'appendTimestamp' => true,
    ];
}
return $config;
