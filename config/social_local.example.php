<?php

return [
    'components' => [
        'eauth' =>  [
            'class'       => 'nodge\eauth\EAuth',
            'popup'       => true, // Use the popup window instead of redirecting.
            'cache'       => false, // Cache component name or false to disable cache. Defaults to 'cache' on production environments.
            'cacheExpire' => 0, // Cache lifetime. Defaults to 0 - means unlimited.
            'httpClient'  => [
                // uncomment this to use streams in safe_mode
                //'useStreamsFallback' => true,
            ],

            'services'    => [ // You can change the providers and their classes.
                'facebook' => [
                    // register your app here: https://developers.facebook.com/apps/
                    'class' => 'app\components\eauth\CustomFacebookOAuth2Service',
                    'clientId' => '983186915109681',
                    'clientSecret' => '94e1be3e928d9173d141d18bbb899619',
                    'title' => '',
                ],
                'linkedin_oauth2' => [
                    // register your app here: https://www.linkedin.com/secure/developer
                    'class' => 'app\components\eauth\CustomLinkedinOAuth2Service',
                    'clientId' => '77hotrq3eznwik',//new protrader org
                    'clientSecret' => 'SQ3pa24Bx6TZtMbW',//new protrader org
                    'title' => '',
                ],
//                'twitter' => [
//                    // register your app here: https://dev.twitter.com/apps/new
//                    'class' => 'nodge\eauth\services\TwitterOAuth1Service',
//                    'key' => 'bmOTOdDPWVB6cgRm6z9yBkpST',
//                    'secret' => 'dOuVDHwCmGymLIOg13pjrleew0LgTg03y736yrGIQI0A4ZbjUZ',
//                    'title' => '',
//                ],
            ],
        ],
    ]
];
