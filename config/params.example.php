<?php

return [
    'adminEmail' => 'info@protrader.org',
    'host' => 'http://protrader.org',
    'copyright' => '<a href="http://protrader.com/">© '.date('Y').' PFSOFT</a>',
    'articlesUploadPath' => '/models/articles',
    'newsUploadPath' => '/models/news',
    'videoUploadPath' => '/models/video',
    'settingsUploadPath' => '/models/settings',
    'settingsFeatureUploadPath' => '/models/feature',
    'brokerUploadPath' => '/models/broker',
    'slideUploadPath' => '/models/slide',
    'featureUploadPath' => '/models/feature',
    'userUploadPath' => '/models/user',
    'kbase' => [
        'appName' => 'ProtraderMC'
    ]

];
