<?php

Yii::setAlias('@tests', dirname(__DIR__) . '/tests');

$params = require(__DIR__ . '/params.php');
$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'timeZone'         => 'Europe/Kiev',
    'bootstrap' => ['log', 'gii', 'notify', 'codebase', 'crosstrade'],
    'aliases' => [
        '@webroot' => '@app/web',
        '@web' => '@app/web',
        '@views' => '@app/views',
        '@mail' => '@app/views/mail/layouts',
    ],
    'controllerMap' => [
        'mail' => [
            'class' => 'app\components\notification\commands\MailController'
        ],
        'content' => [
            'class' => 'app\modules\kbase_org\commands\ContentController'
        ],
        'migrate' => 'app\controllers\MigrateController'
    ],
    'controllerNamespace' => 'app\commands',
    'modules' => [
        'gii' => 'yii\gii\Module',
        'notify' => 'app\modules\notify\Module',
        'crosstrade' => [
            'class' => 'app\modules\crosstrade\Module',
        ],
        'codebase' => [
            'class' => 'app\modules\codebase\Module',
        ],
    ],
    'components' => [
        'consoleRunner' => [
            'class' => 'app\components\ConsoleRunner',
            'file' => '@app/yii'
        ],
        'authManager' => [
            'class' => 'yii\rbac\PhpManager',
            'itemFile' => '@app/components/rbac/items.php',
            'ruleFile' => '@app/components/rbac/rules.php',
            'assignmentFile' => '@app/components/rbac/assignments.php',
        ],
        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => 'app\models\user\UserIdentity',
            'enableSession' => false,
            'loginUrl' => ['user/login']
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',  // e.g. smtp.mandrillapp.com or smtp.gmail.com
                'username' => 'info@protrader.org',
                'password' => 'cg%%478dhsW',

                'port' => '587', // Port 25 is a very common port too
                'encryption' => 'tls', // It is often used, check your provider or mail server specs
            ],
        ],
        'customFunctions' => [
            'class' => 'app\components\CustomFunctions',
        ],
        'mailqueue' => [
            'class' => 'nterms\mailqueue\MailQueue',
            'table' => '{{%mail_queue}}',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',  // e.g. smtp.mandrillapp.com or smtp.gmail.com
                'username' => 'info@protrader.org',
                'password' => 'cg%%478dhsW',

                'port' => '587', // Port 25 is a very common port too
                'encryption' => 'tls', // It is often used, check your provider or mail server specs
            ],
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'showScriptName' => false,
            'enablePrettyUrl' => true,
            'baseUrl' => 'http://protrader.org'
        ],
        'view' => [
            'class' => 'yii\web\View',
            'defaultExtension' => 'twig',
            'renderers' => [
                'twig' => [
                    'class' => 'yii\twig\ViewRenderer',
                    'cachePath' => '@runtime/twig/cache',
                    // Array of twig options:
                    'options' => [
                        'debug' => true,
                        'auto_reload' => true,
                    ],
                    'extensions' => [
                        '\Twig_Extension_Debug',
                        'app\components\twig\BaseView',
                        'app\components\twig\CustomFilters'
                    ],
                    'globals' => [
                        'html' => '\yii\helpers\Html',
                        'url' => '\yii\helpers\Url',
                        'str' => '\yii\helpers\StringHelper',
                        'arr' => '\yii\helpers\ArrayHelper'
                    ],
                    'functions' => [
                        'get_class' => 'get_class',
                        'preg_replace' => 'preg_replace'
                    ],
                    'uses' => [
                        'app\assets',
                        'app\components',
                        'yii\bootstrap',
                    ],
                ],
            ],
        ],
    ],
    'params' => $params,
];

$config = \yii\helpers\ArrayHelper::merge($config, require(__DIR__ . '/local.php'));
$config['components']['log']['targets'][] = [
    'class' => 'yii\log\FileTarget',
    'levels' => ['info'],
    'categories' => ['notify'],
    'logFile' => '@app/runtime/logs/notify.log',
    'logVars' => [],
    'maxFileSize' => 1024 * 2,
    'maxLogFiles' => 20,
];
return $config;
