<?php
use yii\widgets\ActiveForm;
use app\components\widgets\eauth\Widget;
use app\models\user\XMPPUserRecord;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>PTMC: Protrader Multi-Connect Trading Platform</title>
</head>
<body>
<style type="text/css">
    *{
        padding: 0;
        margin: 0;
        outline: none;
        box-sizing: border-box;
    }
    html{
        width: 100%;
        height: 100%;
    }
    body{
        width: 100%;
        height: 100%;
        padding-top: 65px;
        padding-bottom: 30px;
        background-color: #f5f5f5;
        font-family: arial;
        min-height: 400px;
        min-width: 300px;
        position: relative;
    }
    .header{
        height: 65px;
        width: 100%;
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        background-color: #353535;
        border-bottom: 5px solid #ffbe00;
        text-align: center;
        padding-top: 15px;
    }
    .header a.logo{
        display: block;
        max-width: 230px;
        margin: auto;
    }
    .header .logo img{
        max-width: 100%;
        display: inline-block;
        max-height: 30px;
        line-height: 60px;
    }
    .main{
        width: 100%;
        height: 100%;
    }
    .clear{
        clear: both;
    }
    .footer{
        height: 30px;
        line-height: 30px;
        color: #fff;
        text-align: center;
        width: 100%;
        position: absolute;
        background-color: #353535;
        bottom: 0;
        left: 0;
        right: 0;
        font-size: 12px;
    }
    .wrapper{
        position: absolute;
        top: 50%;
        left: 50%;
        max-width: 300px;
        margin: -120px 0 0 -150px;
        text-align: center;
        width: 100%;
    }
    .wrapper h2{
        font-size: 13px;
        color: #666666;
        font-weight: normal;
        margin-bottom: 17px;
    }
    .wrapper input{
        height: 30px;
        width: 100%;
        background-color: #fff;
        border: 1px solid #808080;
        border-radius: 2px;
        padding-left: 10px;
        padding-right: 10px;
        line-height: 28px;
        margin-bottom: 30px;
    }
    .wrapper input.error{
        border-color: red;
    }
    .wrapper .btn{
        float: right;
        width: 130px;
        background-color: #ffbe00;
        border-color: #ffbe00;
        font-size: 13px;
        color: #353535;
        cursor: pointer;
        margin-bottom: 0;
    }
    .wrapper .btn:hover{
        box-shadow: 0 0 0 100px rgba(0,0,0,.05) inset;
    }
    .wrapper .errorMessage{
        font-size: 11px;
        color: red;
        text-align: left;
        position: absolute;
        top: -15px;
        padding-left: 11px;

    }
    .wrapper .inputArea{
        width: 100%;
        position: relative;
    }
    .wrapper a{
        font-size: 12px;
        color: #848484;
        float: left;
        line-height: 28px;
        text-decoration: none;
    }
    .wrapper a:hover{
        text-decoration: underline;
    }
    .footer a{
        color: #ffbe00;
        text-decoration: none;
    }
    .footer a:hover{
        text-decoration: underline;
    }

    .loader {
        color: #ffbe00;
        font-size: 90px;
        text-indent: -9999em;
        overflow: hidden;
        width: 1em;
        height: 1em;
        border-radius: 50%;
        top: 50%;
        left: 50%;
        margin: -45px 0 0 -45px;
        position: absolute;
        -webkit-transform: translateZ(0);
        -ms-transform: translateZ(0);
        transform: translateZ(0);
        -webkit-animation: load6 1.7s infinite ease;
        animation: load6 1.7s infinite ease;
    }
    @-webkit-keyframes load6 {
        0% {
            -webkit-transform: rotate(0deg);
            transform: rotate(0deg);
            box-shadow: 0 -0.83em 0 -0.4em, 0 -0.83em 0 -0.42em, 0 -0.83em 0 -0.44em, 0 -0.83em 0 -0.46em, 0 -0.83em 0 -0.477em;
        }
        5%,
        95% {
            box-shadow: 0 -0.83em 0 -0.4em, 0 -0.83em 0 -0.42em, 0 -0.83em 0 -0.44em, 0 -0.83em 0 -0.46em, 0 -0.83em 0 -0.477em;
        }
        10%,
        59% {
            box-shadow: 0 -0.83em 0 -0.4em, -0.087em -0.825em 0 -0.42em, -0.173em -0.812em 0 -0.44em, -0.256em -0.789em 0 -0.46em, -0.297em -0.775em 0 -0.477em;
        }
        20% {
            box-shadow: 0 -0.83em 0 -0.4em, -0.338em -0.758em 0 -0.42em, -0.555em -0.617em 0 -0.44em, -0.671em -0.488em 0 -0.46em, -0.749em -0.34em 0 -0.477em;
        }
        38% {
            box-shadow: 0 -0.83em 0 -0.4em, -0.377em -0.74em 0 -0.42em, -0.645em -0.522em 0 -0.44em, -0.775em -0.297em 0 -0.46em, -0.82em -0.09em 0 -0.477em;
        }
        100% {
            -webkit-transform: rotate(360deg);
            transform: rotate(360deg);
            box-shadow: 0 -0.83em 0 -0.4em, 0 -0.83em 0 -0.42em, 0 -0.83em 0 -0.44em, 0 -0.83em 0 -0.46em, 0 -0.83em 0 -0.477em;
        }
    }
    @keyframes load6 {
        0% {
            -webkit-transform: rotate(0deg);
            transform: rotate(0deg);
            box-shadow: 0 -0.83em 0 -0.4em, 0 -0.83em 0 -0.42em, 0 -0.83em 0 -0.44em, 0 -0.83em 0 -0.46em, 0 -0.83em 0 -0.477em;
        }
        5%,
        95% {
            box-shadow: 0 -0.83em 0 -0.4em, 0 -0.83em 0 -0.42em, 0 -0.83em 0 -0.44em, 0 -0.83em 0 -0.46em, 0 -0.83em 0 -0.477em;
        }
        10%,
        59% {
            box-shadow: 0 -0.83em 0 -0.4em, -0.087em -0.825em 0 -0.42em, -0.173em -0.812em 0 -0.44em, -0.256em -0.789em 0 -0.46em, -0.297em -0.775em 0 -0.477em;
        }
        20% {
            box-shadow: 0 -0.83em 0 -0.4em, -0.338em -0.758em 0 -0.42em, -0.555em -0.617em 0 -0.44em, -0.671em -0.488em 0 -0.46em, -0.749em -0.34em 0 -0.477em;
        }
        38% {
            box-shadow: 0 -0.83em 0 -0.4em, -0.377em -0.74em 0 -0.42em, -0.645em -0.522em 0 -0.44em, -0.775em -0.297em 0 -0.46em, -0.82em -0.09em 0 -0.477em;
        }
        100% {
            -webkit-transform: rotate(360deg);
            transform: rotate(360deg);
            box-shadow: 0 -0.83em 0 -0.4em, 0 -0.83em 0 -0.42em, 0 -0.83em 0 -0.44em, 0 -0.83em 0 -0.46em, 0 -0.83em 0 -0.477em;
        }
    }
    .wrapper .or{
        clear: both;
        border-bottom: 1px solid #666666;
        color: #666666;
        font-size: 12px;
        position: relative;
        top: 10px;
        margin-bottom: 30px;
    }
    .wrapper .or span{
        padding: 5px 14px;
        background-color: #f5f5f5;
        position: relative;
        top: 5px;
    }
    .wrapper .socials{
        clear: both;
    }
    .wrapper .socials .btn{
        height: 28px;
        line-height: 28px;
        float: left;
        color: #fff;
    }
    .wrapper .socials .btn.facebook{
        background-color: #445fae;
    }
    .wrapper .socials .btn.linkedin{
        float: right;
        background-color: #0077b5;
    }
</style>

<div class="header">
    <a href="http://protrader.org/" class="logo">
        <img src="http://protrader.org/images/logo.svg" onerror="this.onerror=null; this.src='http://protrader.org/images/logo.png'" alt="Logo PTMC">
    </a>
</div>
<div class="main">
    <?php if(!Yii::$app->user->isGuest): ?>
    <?php $xmpp = XMPPUserRecord::findByUserId(\Yii::$app->user->getId()); ?>
    <div class="loader">Loading...</div>
    <div id="username" style="display: none"><?=$xmpp->username?></div>
    <div id="token" style="display: none"><?=$xmpp->token?></div>
    <?php else: ?>
    <div class="wrapper">
        <h2>Login</h2>
        <?php $form = ActiveForm::begin(); ?>
        <?=$form->field($model, 'username', [
                'template' => '{label} <div class="inputArea">{error}{input}{hint}</div>',
                'errorOptions' => [
                    'class' => 'errorMessage',
                ]
        ])->textInput(['placeholder' => Yii::t('app', 'E-mail')])->label(false);?>
        <?=$form->field($model, 'password', [
            'template' => '{label} <div class="inputArea">{error}{input}{hint}</div>',
            'errorOptions' => [
                'class' => 'errorMessage',
             ]
        ])->passwordInput(['placeholder' => Yii::t('app', 'Password')])->label(false);?>
        <a href="#">Restore password</a>
        <input type="submit" class="btn" value="Log in">
        <?php ActiveForm::end(); ?>
        <div class="or"><span>Or</span></div>
        <?= Widget::widget();?>
        <div class="clear"></div>
    </div>
    <?php endif; ?>
</div>
<div class="footer">
    <p>© 2016. PTMC is based on <a href="http://protrader.com/" target="_blank">PFSOFT Protrader</a> technology.</p>
</div>
</body>
</html>