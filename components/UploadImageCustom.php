<?php
/**
 * Created by PhpStorm.
 * User: Zaitsev Oleg
 * Date: 16.06.15
 * Time: 13:48
 */

namespace app\components;

use yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\widgets\FileInput;

class UploadImageCustom extends Widget
{
    public $form = null;
    public $model = null;
    public $attribute = null;
    public $options = null;
    public $imageUrl = null;


    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $attribute = $this->attribute;
        echo $this->form->field($this->model, $attribute, ['template' => '{label}{input}{error}'])->fileInput();
        // display the image uploaded or show a placeholder

        if($this->model->$attribute) {
            echo'
            <div class="col-lg-3"></div>
            <div class="col-lg-6">'.

                '<div class="checkbox form-control-checkbox">'.

                    $this->form->field($this->model, $this->attribute, ['template' => '{input}'])
                    ->checkBox(['value' => 'delete', 'checked' => false, 'label' => 'Delete uploaded image']) .
                    //Html::input('checkbox', 'Company[delete_'.$this->attribute.']', '') .

                '</div>' .

                Html::img($this->imageUrl, []) .

            '</div>';
        }

    }
}