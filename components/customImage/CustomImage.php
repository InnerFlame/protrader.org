<?php

namespace app\components\customImage;

use yii;
use yii\base\Model;
use yii\web\UploadedFile;
use yii\helpers\Html;

class CustomImage extends Model
{
    /**
     * @var UploadedFile[]
     */
    public $image;
    public $filenames = [];
    public $storage = [];
    public $uploadPath = null;
    public $uploadUrl = null;
    public $model = null;

    public $cropImg = false;

    public $no_image_url = '/images/no-img.jpg';

    public $validation = false;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function __toString()
    {
        return $this->filename;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
        ];
    }

    public function setValidation($attr, $params)
    {

        $this->validation[$attr] = $params;

    }

    public function validateAttr($attr)
    {
       if(isset($this->validation[$attr]['required']))
           if($this->validation[$attr]['required']){
               if(!strlen($this->model->$attr) > 0 && !$this->getUploadedFile($attr)){
                   $this->model->addError($attr, "This field is required");
               }
           }
    }

    /**
     * return uploaded file
     * @param string $attribute
     * @return string
     */
    public function getUploadedFile($attribute) {

        if(isset($_POST[$attribute . '_cropImg']))
            if($_POST[$attribute . '_cropImg'])
                $this->cropImg = $_POST[$attribute . '_cropImg'];

        $uploaded = UploadedFile::getInstance($this->model, $attribute);

        if($uploaded) return $uploaded;

        if($this->cropImg) return $this->cropImg;

        return false;

    }

    /**
     * Process upload of image
     *@param string $attribute
     * @return mixed the uploaded image instance
     */
    public function uploadImage($attribute, $croped = true)
    {
        $image = $this->getUploadedFile($attribute);

        if($image)
        {
            // for Crop Images
            if ($croped)
            {
                if (isset($_POST[$attribute . '_cropX'], $_POST[$attribute . '_cropY'],
                    $_POST[$attribute . '_cropW'], $_POST[$attribute . '_cropH']))
                {
                    return CustomImageUpload::getUploadedCropImage($attribute, $this->model, $this, $image);
                }

            }else{

                // for Standart Image
                if (!isset($this->filenames[$attribute]))
                {
                    // get the uploaded file instance. for multiple file uploads
                    // the following data will return an array (you may need to use
                    // getInstances method)
                    $image = $this->getUploadedFile($attribute);

                    // if no image was uploaded abort the upload
                    if (empty($image))
                    {
                        return false;
                    }

                    if (!is_dir($this->uploadPath))
                        mkdir($this->uploadPath, 0777);
                    $file_dir = $this->uploadPath;

                    if (isset($this->model->id))
                    {
                        //if ($this->model->id > 0) {
                        $file_dir = $this->uploadPath . '/' . $this->model->id;
                        if (!is_dir($file_dir))
                            mkdir($file_dir, 0777);
                        //}
                    }

                    if (isset($image->name))
                    {

                        // store the source file name
                        $ext = end((explode(".", $image->name)));

                        // generate a unique file name
                        $this->filenames[$attribute] = $filename = Yii::$app->security->generateRandomString() . ".{$ext}";

                        $image->saveAs($file_dir . '/' . $filename);

                        // the uploaded image instance
                        return $filename;

                    }

                }

            }
        }

        return false;

    }

    /**
     * Return true if attribute is changed
     *
     * @return boolean
     */
    public function isChanged($attribute){
        if($this->model->$attribute) return true;
        return false;
    }

    /**
     * Return true if checked delete value
     *
     * @return boolean
     */
    public function isDeleted($attribute)
    {
        if($this->model->$attribute == 'delete'){
            $this->storage[$attribute] = 'delete';
            return true;
        }
        if(isset($this->storage[$attribute]))
            if($this->storage[$attribute] == 'delete') return true;
        return false;
    }

    /**
     * Process deletion of image
     *
     * @return boolean the status of deletion
     */
    public function deleteImage() {
        $file = $this->getImageFile();

        // check if file exists on server
        if (empty($file) || !file_exists($file)) {
            return false;
        }

        // check if uploaded file can be deleted on server
        if (!unlink($file)) {
            return false;
        }

        // if deletion successful, reset your file attributes
        $this->logo = null;
        $this->filename = null;

        return true;
    }

    /**
     * fetch stored image url
     * @return string
     */
    public function getImageUrl($attribute)
    {
        if(isset($this->model->$attribute)) {

            $uploadDir = $this->uploadUrl;
            if(isset($this->model->id))
                //if($this->model->id > 0)
                    $uploadDir .= '/' . $this->model->id;

            if((stripos($this->model->$attribute,"http://")===0))
                return $this->model->$attribute;

            return $uploadDir . '/' . $this->model->$attribute;

        }
    }

    public function noImageUrl()
    {
        return $this->no_image_url;
    }

    /**
     * fetch stored image path
     * @return string
     */
    public function getImagePath($attribute)
    {
        if(isset($this->model->$attribute)) {

            $uploadDir = $this->uploadPath;
            if(isset($this->model->id))
                if($this->model->id > 0)
                    $uploadDir .= '/' . $this->model->id;

            if(file_exists($uploadDir . '/' . $this->model->$attribute))
                if(is_file($uploadDir . '/' . $this->model->$attribute))
                        return $uploadDir . '/' . $this->model->$attribute;

        }
        return false;
    }

    /**
     * delete image path
     * @return boolean
     */
    public function deleteImagePath()
    {
        function removeDirectory($dir) {
            if($objs = scandir($dir))
                foreach($objs as $obj)
                    if($obj <> '.' && $obj <> '..') {
                        $_path = $dir . '/' . $obj;
                        is_dir($_path) ? removeDirectory($_path) : unlink($_path);
                    }

            if(is_dir($dir)) rmdir($dir);
        }

        if($this->uploadPath) {
            $uploadDir = $this->uploadPath;
            if (isset($this->model->id))
                if ($this->model->id > 0) {
                    $uploadDir .= '/' . $this->model->id;
                    if (is_dir($uploadDir))
                        removeDirectory($uploadDir);
                }
        }

        return true;
    }

    /**
     * fetch stored image url and return <img>
     * @return string
     */
    public function getImage($attribute, $width, $height, $empty = false)
    {
        if(isset($this->model->$attribute)) {
            if($this->model->$attribute) {
                $uploadDir = $this->uploadUrl;
                if (isset($this->model->id))
                    if ($this->model->id > 0)
                        $uploadDir .= '/' . $this->model->id;

                return Html::img($uploadDir . '/' . $this->model->$attribute);
            }
        }

        if($empty) return false;
        return Html::img('https://api.fnkr.net/testimg/'. $width .'x'. $height .'/666666/000/?text=logo');
    }
}

?>