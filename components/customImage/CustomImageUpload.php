<?php
/**
 * Created by PhpStorm.
 * User: Zaitsev Oleg
 * Date: 16.06.15
 * Time: 13:48
 */

namespace app\components\customImage;

use app\components\customImage\assets\BaseCustomImageAsset;
use Imagine\Image\Box;
use Imagine\Image\Point;
use yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\imagine\Image;
use yii\widgets\FileInput;

class CustomImageUpload extends Widget
{
    public $form = null;
    public $model = null;
    public $attribute = null;
    public $options = null;
    public $imageUrl = null;
    public $crop = false;

    public $customImage = false;

    public $cropArr = [];

    public function init()
    {
        Yii::$app->view->registerAssetBundle(BaseCustomImageAsset::className());
        parent::init();
    }

    public function run()
    {
        $attribute = $this->attribute;

        echo $this->form->field($this->model, $attribute,
            ['template' => '{label}{input}{error}'])
            ->fileInput(['id' => $attribute . '_image_upload', 'accept' => 'image/*']);

//        echo '<span class="help-block picture-help">' .
//            Yii::t('app', 'You can upload images with type (.png, .jpg) and no more than 2 mb') .
//            '</span>';

        // Display the image uploaded or show a placeholder
        if ($this->model->$attribute) {

            echo '<div id="crop_field_' . $attribute . '">';
            echo '<div class="col-lg-9">' . '<div class="checkbox form-control-checkbox" style="margin-bottom:50px;">' . '</div>' . '</div>';
            echo Html::img($this->model->image->getImageUrl($attribute), []);
            echo '</div>';
        }

        if (is_array($this->crop)) {
            echo Html::hiddenInput($attribute . '_cropX');
            echo Html::hiddenInput($attribute . '_cropY');
            echo Html::hiddenInput($attribute . '_cropW');
            echo Html::hiddenInput($attribute . '_cropH');
            echo Html::hiddenInput($attribute . '_cropImg');
            echo Html::hiddenInput('crop-img-upload', $attribute);
            echo '<div id="progress_bar_' . $attribute . '"></div>';

            if (!$this->model->$attribute)
                echo '<div id="crop_field_' . $attribute . '"></div>';

            $cropHtml =
                // '<div class="col-lg-1"></div>'
                '<div class="col-md-10">'
                . '<div class="panel panel-flat">'
                . '<div class="panel-heading">'
                . '<h5 class="panel-title">Image Crop</h5>'
                . '<div class="heading-elements">'
//										.'<a href="javascript:void(0);" class="btn btn-success">'
//										    .'Save Crop'
//										.'</a>'
                . '</div>'
                . '</div>'

                . '<div class="panel-body">'
                . '<div class="image-cropper-container">'
                . '<img src="/images/placeholder.jpg" alt="" class="cropRatio"'
                . 'id="crop_' . $attribute . '">'
                . '</div>'
                . '</div>'
                . '</div>'
                . '</div>';
//            $validation = $this->setValidation($attribute);

            $validation = $this->model->image->validation[$attribute];

            Yii::$app->view->registerJs("

                $(document).ready(function(){


                    function validator(width, height, size)
                    {

                        var validation = " . json_encode(
                    isset($validation) ?
                        $validation : null
                ) . ";


                        if(typeof validation != 'undefined')
                        {

                            if(typeof validation.maxSize  != 'undefined')
                            {
                                if(validation.maxSize < size){

                                    alert('Maximal size must be ' + validation.maxSize + 'kB!');

                                    document.getElementById('" . $attribute . "_image_upload').value = '';

                                    return false;
                                }
                            }

                            if(typeof validation.maxWidth  != 'undefined')
                            {
                                if(validation.maxWidth < width){

                                    alert('Maximal width must be ' + validation.maxWidth + 'px!');

                                    document.getElementById('" . $attribute . "_image_upload').value = '';

                                    return false;
                                }
                            }

                            if(typeof validation.maxHeight  != 'undefined')
                            {
                                if(validation.maxHeight < height){

                                    alert('Maximal height must be ' + validation.maxHeight + 'px!');

                                    document.getElementById('" . $attribute . "_image_upload').value = '';

                                    return false;

                                }
                            }

                        }

                        return true;
                    }

                    function checkValidation(FILES)
                    {
                        var file = FILES[0];

                        var reader = new FileReader();
                        var image  = new Image();

                        reader.readAsDataURL(file);

                        reader.onload = function (_file) {

                            image.src    = _file.target.result;

                            image.onload = function() {

                                if(validator(this.width, this.height, file.size/1024))
                                {
                                    jQuery('#crop_field_{$attribute}').html('{$cropHtml}');
                                    jQuery('#crop_field_{$attribute} img').attr('src', image.src);
                                    addCropJs('#crop_field_{$attribute} img');

                                }

                            }

                        };

                        return false;

                    }

                    function addCropJs(idattr)
                    {

                          jQuery(idattr).cropper({
                            " . substr(json_encode($this->crop), 1, strlen(json_encode($this->crop)) - 2) . ",
                            done: function(data) {"
                . "jQuery('input[name=\"{$this->attribute}_cropX\"]').val(data.x);"
                . "jQuery('input[name=\"{$this->attribute}_cropY\"]').val(data.y);"
                . "jQuery('input[name=\"{$this->attribute}_cropH\"]').val(data.height);"
                . "jQuery('input[name=\"{$this->attribute}_cropW\"]').val(data.width);
                            }

                        });

                    }

                    jQuery(document).delegate('#{$attribute}_image_upload', 'change', function(e){

                            var FILES = this.files;

                            checkValidation(FILES);

                    });

                });


                ");
        }
    }

    public static function getModelSaveDir($model, $uploadPath)
    {
        if (!is_dir(Yii::$app->assetManager->basePath . '/images'))
            mkdir(Yii::$app->assetManager->basePath . '/images', 0777);

        if (!is_dir($uploadPath)) mkdir($uploadPath, 0777);
        $file_dir = $uploadPath;

        if (isset($model->id)) {
            if ($model->id > 0) {
                $file_dir = $uploadPath . '/' . $model->id;
                if (!is_dir($file_dir)) mkdir($file_dir, 0777);
            }
        }

        return $file_dir;
    }

    public static function getUploadedCropImage($attribute, $model, $customImageModel, $image)
    {
        if ($image)
            if (isset($_POST[$attribute . '_cropX'],
                $_POST[$attribute . '_cropY'], $_POST[$attribute . '_cropW'],
                $_POST[$attribute . '_cropH'])) {

                $file_name = $attribute . '-' . substr(Yii::$app->security->generateRandomString(), 0, 4) . '.jpg';

                $crop_x = (int)$_POST[$attribute . '_cropX'];
                $crop_y = (int)$_POST[$attribute . '_cropY'];
                $crop_w = (int)$_POST[$attribute . '_cropW'];
                $crop_h = (int)$_POST[$attribute . '_cropH'];

                $path = $image->tempName;

                if ($image->type === 'image/jpeg' || $image->type === 'image/png') {
                    $image = Image::getImagine()->open($path);
                    $save_path = self::getModelSaveDir($model, $customImageModel->uploadPath) . '/' . $file_name;
                    $size = $image->getSize();
                    $image->crop(
                        new Point($crop_x, $crop_y),
                        new Box($crop_w, $crop_h))
                        ->save($save_path);

                    Image::frame($save_path, 0)
                        // ->thumbnail(new Box(190, 210))
                        ->save($save_path, ['quality' => 100]);

                    // unlink($path);

                    unset($_POST[$attribute . '_cropX']);
                    unset($_POST[$attribute . '_cropY']);
                    unset($_POST[$attribute . '_cropW']);
                    unset($_POST[$attribute . '_cropH']);

                    return $file_name;
                }
            }

        return false;
    }
}