<?php
/**
 * Created by PhpStorm.
 * @author: Roman Budnitsky
 * @date: 23.10.14 14:48
 * @copyright Copyright (c) 2014 PFSOFT LLC
 *
 * Class BaseFlatAsset
 * @package app\assets
 */


namespace app\components\customImage\assets;

use yii\web\AssetBundle;

class BaseCustomImageAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
    ];

    public $js = [
        'limitless/assets/js/plugins/media/cropper.min.js',
    ];

}
