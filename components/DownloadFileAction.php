<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 * Date: 24.02.2016
 * Time: 18:07
 */

namespace app\components;


use yii\base\Action;
use yii\web\NotFoundHttpException;

class DownloadFileAction extends Action
{
    public $path;

    public function run($filename)
    {
        $file = \Yii::getAlias($this->path).DIRECTORY_SEPARATOR.$filename;
        if(file_exists($file)){
            header('Content-Type: application/octet-stream');
            header('Accept-Ranges: bytes');
            header ("Content-Length: ".filesize($file));
            header ("Content-Disposition: attachment; filename=".$file);
            readfile($file);
        }
        throw new NotFoundHttpException;
    }
}