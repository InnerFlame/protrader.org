<?php
namespace app\components\likes;

use Yii;
use yii\base\Component;
use app\models\community\Counts;
use app\models\community\Likes;

class CounterLike extends Component
{
    public function setLike()
    {
        // Yii::$app->user->identity->id = 3;
        if(Likes::setLike(Yii::$app->request->post('entity'), Yii::$app->request->post('cheat', false)))
            Counts::setCount(Yii::$app->request->post('entity'), Yii::$app->request->get('field'));
        echo Counts::getCount(Yii::$app->request->post('entity'));
    }
}