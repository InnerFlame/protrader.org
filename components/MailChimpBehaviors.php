<?php

namespace app\components;

use yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;

class MailChimpBehaviors extends Behavior
{
    const API_ID = '6d3a79cf05';
    const API_LIST = 'lists/subscribe';
    const API_KEY = 'd793078d396a620cf255dfb4c19604e7-us10';

    const CHANNEL_PTMC = 'PTMC user';
    const CHANNEL_CROSS_TRADE_SUBSCRIPTION = 'Cross Trade Subscribtion';
    const CHANNEL_CROSS_TRADE_REGISTRATION = 'Cross Trade Registration';

    /**
     * @param $user
     * @throws \Mailchimp_HttpError
     */
    public static function run($user, $channel)
    {
        $chimp = new \Mailchimp(self::API_KEY);
        $chimp->call(self::API_LIST, [
            'id'                => self::API_ID,
            'email'             => ['email' => $user->email],
            'merge_vars'        => [
                'MERGE15' => $user->username,
                'MERGE16' => null,
                'MERGE17' => $user->getCountry(),
                'MERGE18' => $channel,
                'MERGE1'  => $user->gmt ? $user->gmt : null
            ],
            'double_optin'      => false,
            'update_existing'   => true,
            'replace_interests' => false,
            'send_welcome'      => false,
        ]);
    }
} 