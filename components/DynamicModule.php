<?php
/**
 * Created by PhpStorm.
 * @author: Roman Budnitsky
 * @date: 01.12.14 13:46
 * @copyright Copyright (c) 2014 PFSOFT LLC
 *
 * Class DynamicModule
 */

namespace app\components;

abstract class DynamicModule extends \yii\base\Module {

    public $components = [];
    public static $extendMainMenu = [];
    public static $urlRules = [];

} 