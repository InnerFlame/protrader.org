<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2016
 */

namespace app\components\helpers;


class ArrayHelper extends \yii\helpers\ArrayHelper
{

    /**
     * Объеденяет массив, гарантируя уникальность
     * элементов
     * @param $array1
     * @param $array2
     * @return array
     */
    public static function mergeUnique($array1, $array2)
    {
        return array_unique(array_merge($array1, $array2));
    }
}