<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 *
 * Позволяет сериализовать данные и подготавливать
 * их к сохранению в базе данных, за счет дополнительного
 * кодирования их в формат base64.
 */

namespace app\components\helpers;



use yii\helpers\VarDumper;

class Serialize
{
    /**
     * Сериализует входящщий объект или массив.
     * Если параметр $base64 задан как true, то
     * результат сериализации, будет дополнительно
     * конвертирован в формат base64 для безопасного
     * хранения в БД
     * @param $obj
     * @param bool|false $base64
     * @return string
     */
    public static function encode($obj, $base64 = false)
    {
        $serialize = serialize($obj);
        if($base64){
            return base64_encode($serialize);
        }
        return $serialize;
    }

    /**
     * Распаковывает строку обратно в объект или массив
     * Если параметр $base64, задан как true, то к результуту
     * десиариализации, будет пременена, так же, декодировка из
     * формата base64.
     * @param $str
     * @param bool|false $base64
     * @return mixed|string
     */
    public static function decode($str, $base64 = false)
    {
        if($base64){
            $str = base64_decode($str);
        }
        return unserialize($str);
    }
}