<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 * Date: 16.03.2016
 * Time: 14:23
 */

namespace app\components\helpers;


class StringHelper extends \yii\helpers\StringHelper
{

    public static function randomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randstring = '';

        for ($i = 0; $i < $length; $i++) {
            $randstring .= $characters[rand(0, strlen($characters))];
        }
        return $randstring;
    }

    /**
     * truncate string, and output
     * @param string $string
     * @param int $length
     * @param string $suffix
     * @param null $encoding
     * @param bool|false $asHtml
     * @return string
     */
    public static function truncateAsWord($string, $length, $suffix = '...', $encoding = null, $asHtml = false)
    {
        if((mb_strlen($string, $encoding ?: \Yii::$app->charset) <= $length)){
            return $string;
        }
        $truncate = parent::truncate(strip_tags($string), $length, $suffix, $encoding, $asHtml);
        $words = explode(' ', $truncate);
        array_pop($words);//last spice
        array_pop($words);//last word
        return implode(' ', $words).$suffix;
    }


}