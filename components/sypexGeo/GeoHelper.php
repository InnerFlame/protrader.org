<?php
/***************************************************************************\
 * | Sypex Geo                  version 2.2.2                                  |
 * | (c)2006-2014 zapimir       zapimir@zapimir.net       http://sypex.net/    |
 * | (c)2006-2014 BINOVATOR     info@sypex.net                                 |
 * |---------------------------------------------------------------------------|
 * |     created: 2006.10.17 18:33              modified: 2014.05.29 01:01     |
 * |---------------------------------------------------------------------------|
 * | Sypex Geo is released under the terms of the BSD license                  |
 * |   http://sypex.net/bsd_license.txt                                        |
 * \***************************************************************************/

namespace app\components\sypexGeo;

class GeoHelper
{

    /**
     * @param null $ip
     * @return array
     */
    public static function getGeoDataToMail($ip = null)
    {

        $geo = new Sypexgeo();
        $geo_data = $geo->get();

        return [
            'time' => date('d.m.Y H:i:s'),
            'ip' => $_SERVER['REMOTE_ADDR'],
            'city' => $geo_data['city'],
            'region' => $geo_data['region'],
            'country' => $geo_data['country'],
        ];



        // get by custom IP
        //$geo->get($ip);

    }
}