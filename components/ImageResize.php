<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\components;

use Yii;
use Imagine\Image\Box;
use Imagine\Image\Color;
use Imagine\Image\ImageInterface;
use Imagine\Image\ImagineInterface;
use Imagine\Image\ManipulatorInterface;
use Imagine\Image\Point;
use yii\base\InvalidConfigException;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\imagine\BaseImage;
use yii\imagine\Image;

/**
 * @author php - igor
 * Class ImageResize
 * @package app\components
 */
class ImageResize extends BaseImage
{


    public static function getThumbnail($width, $height, $id, $picture)
    {

        // original image with full size
        $orig_image =
            Yii::$app->params['userUploadPath']
            . '/' . $id
            . '/' . $picture;

        // image with new params
        $new_image =
            Yii::$app->params['userUploadPath']
            . '/' . $id
            . '/' . $width
            . 'x' . $height
            . '-' . $picture;


        $path_thumb = Yii::getAlias('@webroot') . $new_image;
        $path_original = Yii::getAlias('@webroot') . $orig_image;


        //check
        if (!is_file($path_thumb) && is_file($path_original)) {

            $box = new Box($width, $height);

            $img = static::getImagine()->open(Yii::getAlias($path_original));

            //check if weight and height > original size = will return original image
            if (($img->getSize()->getWidth() <= $box->getWidth() && $img->getSize()->getHeight() <= $box->getHeight()) || (!$box->getWidth() && !$box->getHeight()))
                return Html::img($orig_image);


            Image::thumbnail($path_original, $width, $height)->save($path_thumb);

        }

        if (is_file($path_thumb))
            return Html::img($new_image);

        return null;
    }

}
