<?php
/**
 * Created by PhpStorm.
 * @author: Roman Budnitsky
 * @date: 05.11.14 18:01
 * @copyright Copyright (c) 2014 PFSOFT LLC
 *
 * Class ActionColumn
 * @package app\components\grid
 */


namespace app\components\grid;

use yii;
use yii\helpers\Html;

class ActionColumn extends \yii\grid\ActionColumn {

    public $access = [];
    public $dropdown = true;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        //$this->template = '{view} {update} {delete}';
        if($this->dropdown) {
            $this->contentOptions = ['class' => 'grid-action-cell', 'style' => 'min-width:83px;'];
            $this->header = '<span>Actions</span>';
            $this->options['width'] = 80;
        }
        //$this->visible = false;
    }

    /**
     * Initializes the default button rendering callbacks
     */
    protected function initDefaultButtons()
    {
        $first = $this->getFirstButtonName();

        if (!isset($this->buttons['view'])) {
            $this->buttons['view'] = function ($url, $model) use($first) {
                return Html::a( ($this->dropdown) ? Yii::t('yii', 'Details') : '<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                    'title' => Yii::t('yii', 'Details'),
                    'class' => ($first == 'view' && $this->dropdown) ? 'btn btn-default btn-xs' : '',
                    'data-pjax' => 0,
                ]);
            };
        }
        if (!isset($this->buttons['update'])) {
            $this->buttons['update'] = function ($url, $model) use($first) {
                return Html::a( ($this->dropdown) ? Yii::t('yii', 'Update') : '<span class="glyphicon glyphicon-pencil"></span>', $url, [
                    'title' => Yii::t('yii', 'Update'),
                    'class' => ($first == 'update' && $this->dropdown) ? 'btn btn-default btn-xs' : '',
                    'data-pjax' => 0,
                ]);
            };
        }
        if (!isset($this->buttons['delete'])) {
            $this->buttons['delete'] = function ($url, $model) use($first) {
                return Html::a( ($this->dropdown) ? Yii::t('yii', 'Delete') : '<span class="glyphicon glyphicon-trash"></span>', $url, [
                    'title' => Yii::t('yii', 'Delete'),
                    'class' => ($first == 'delete' && $this->dropdown) ? 'btn btn-default btn-xs' : '',
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                    'data-toggle' => 'modal',
                    'data-target' => "#mod-info",
                    'data-method' => 'post',
                    'data-pjax' => 0,
                ]);
            };
        }
    }

    /**
     * Get first button name
     */
    protected function getFirstButtonName()
    {
        $match = null;
        preg_match('\'{(\\w+)}\'', $this->template, $match);
        return isset($match[1]) ? $match[1] : false;
    }

    /**
     * Get access data
     * @params $snap
     * @params $model
     * @params $key
     * @params $index
     *
     * @return mixed $rea
     */
    protected function getAccessData($snap, $model, $key, $index) {
        $res = [];
        foreach($snap as $id => $str) {
            $str = explode('.', $str);
            if(count($str) == 1) {
                $res[$id] = ${$str[0]};
            } elseif(count($str) == 2) {
                $res[$id] = ${$str[0]}->$str[1];
            }
        }
        return $res;
    }

    /**
     * Check permission
     * @params string $name
     * @params string $model
     * @params string $key
     * @params string $index
     *
     * @return boolean
     */
    protected function can($name, $model, $key, $index) {
        return Yii::$app->user->can($this->access[$name][0], $this->getAccessData($this->access[$name][1], $model, $key, $index));
    }

    /**
     * @inheritdoc
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        static $n = 0;
        $first = $this->getFirstButtonName();
        $res = preg_replace_callback('/\\{([\w\-\/]+)\\}/', function ($matches) use ($model, $key, $index, $first, &$n) {
            $name = $matches[1];
            if (isset($this->buttons[$name])  && (!isset($this->access[$name]) || $this->can($name, $model, $key, $index) ) ) {
                $n += 1;
                $url = $this->createUrl($name, $model, $model->id /*$key*/ , $index);
                $button = call_user_func($this->buttons[$name], $url, $model, $key);
                if($this->dropdown) {
                    if($first == $name) {
                        return $button . ' {caret}';
                    } else {
                        return Html::tag('li', $button);
                    }
                } else {
                    return $button;
                }
            } else {
                return '';
            }
        }, $this->template);
        if($this->dropdown && $n > 0) {
            $caret = '<button data-toggle="dropdown" class="btn btn-xs dropdown-toggle ' . (($n == 1) ? 'disabled' : 'btn-primary') . '" type="button">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button> <ul role="menu" class="dropdown-menu">';
            $res = str_replace('{caret}', $caret, '<div class="btn-group">' . $res . '</ul></div>');
        }
        $n = 0;
        if(!trim($res)) {
            return '<div class="btn-group">
                <a class="btn btn-default btn-xs disabled" href="#">No actions</a>
                    <button data-toggle="dropdown" class="btn btn-xs dropdown-toggle disabled" type="button">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul role="menu" class="dropdown-menu"></ul>
                </div>';
        }
        return $res;
    }

} 