<?php
/**
 * Created by PhpStorm.
 * @author: Roman Budnitsky
 * @date: 07.11.14 12:53
 * @copyright Copyright (c) 2014 PFSOFT LLC
 *
 * Class DataColumn
 * @package app\components\grid
 */


namespace app\components\grid;


use yii\helpers\Html;
use yii\helpers\ArrayHelper;

class DataColumn extends \yii\grid\DataColumn {

    public $extended = [];
    public $after = null;

    // Custom Propetries
    public $sortable = false;
    public $filterArr = false;

    public function renderHeaderCell()
    {
        $label = $this->renderHeaderCellContent();
        if(strlen($label) == strlen(strip_tags($label))) $label = Html::tag('span', $label);
        return Html::tag('th', $label, $this->headerOptions);
    }

    public function getDataCellValue($model, $key, $index)
    {
        if ($this->value !== null) {
            if (is_string($this->value)) {
                if(strpos($this->value, 'eval:') !== false) {
                    return eval('return ' . substr($this->value, 5) . ';');
                } else {
                    return ArrayHelper::getValue($model, $this->value);
                }
            } else {
                return call_user_func($this->value, $model, $key, $index, $this);
            }
        } elseif ($this->attribute !== null) {
            return ArrayHelper::getValue($model, $this->attribute);
        }
        return null;
    }

}