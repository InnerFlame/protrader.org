<?php
/**
 * Created by PhpStorm.
 * @author: Roman Budnitsky
 * @date: 05.11.14 18:01
 * @copyright Copyright (c) 2014 PFSOFT LLC
 *
 * Class ActionColumn
 * @package app\components\grid
 */


namespace app\components\grid;

use yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\web\Link;
use yii\web\Linkable;
use yii\web\Request;

class PaginationColumn extends Widget {

    public $list = [];
    public $pageSize;
    public $defaultPageSize = 20;
    public $pageSizeParam = 'pageSize';
    public $id;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $queryParams = Yii::$app->request->queryParams;
        $this->pageSize = isset($queryParams[$this->pageSizeParam]) ? $queryParams[$this->pageSizeParam] : $this->defaultPageSize;
    }

    public function run(){
        $view = $this->getView();
        $params = Yii::$app->request->queryParams;

        $params[0] = Yii::$app->controller->getRoute();
        $urlManager = Yii::$app->getUrlManager();

        //if(isset($params['page'])) unset($params['page']);
        if(isset($params[$this->pageSizeParam])) unset($params[$this->pageSizeParam]);

        $url = $urlManager->createUrl($params);

        $js = "jQuery(document).delegate('#" .$this->id. "-perPage', 'change', function(){
            url_current = '{$url}';
            separator = url_current.indexOf('?') != -1 ? '&' : '?';
            url_get = url_current+separator+'{$this->pageSizeParam}='+jQuery(this).val();
            document.location = url_get;
        });";
        $view->registerJs($js);

        return Html::dropDownList('pageSize', $this->pageSize, $this->list, ['class' => 'form-control pull-left pagination-perPage', 'id' => $this->id . '-perPage']);
    }
} 