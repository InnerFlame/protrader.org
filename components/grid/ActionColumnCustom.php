<?php
/**
 * Created by PhpStorm.
 * @author: Roman Budnitsky
 * @date: 06.02.15 12:17
 * @copyright Copyright (c) 2015 PFSOFT LLC
 *
 * Class ActionColumn2
 * @package 
 */


namespace app\components\grid;

use yii;
use yii\helpers\Html;

class ActionColumnCustom extends \yii\grid\ActionColumn {

    public $access = [];
    public $dropdown = true;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        if($this->dropdown) {
            $this->contentOptions = ['class' => 'grid-action-cell admin-action'];
            $this->header = '<span>' . Yii::t('app', 'Action') .'</span>';
            if(!isset($this->options['width']))
                $this->options['width'] = 90;
        }
    }

    /**
     * Initializes the default button rendering callbacks.
     */
    protected function initDefaultButtons()
    {
        if (!isset($this->buttons['view'])) {
            $this->buttons['view'] = function ($url, $model, $key, $options) {
                $options = array_merge([
                    'title' => Yii::t('yii', 'View'),
                    'data-pjax' => '0',
                ], $options);
                return Html::a($this->dropdown ? Yii::t('yii', 'Details') : '<span class="glyphicon glyphicon-eye-open"></span>', $url, $options);
            };
        }
        if (!isset($this->buttons['update'])) {
            $this->buttons['update'] = function ($url, $model, $key, $options) {
                $options = array_merge([
                    'title' => Yii::t('yii', 'Update'),
                    'data-pjax' => '0',
                ], $options);
                return Html::a($this->dropdown ? Yii::t('yii', 'Update') : '<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
            };
        }
        if (!isset($this->buttons['delete'])) {
            $this->buttons['delete'] = function ($url, $model, $key, $options) {
                $options = array_merge([
                    'title' => Yii::t('yii', 'Delete'),
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                    'data-method' => 'post',
                    'data-pjax' => '0',
                ], $options);
                return Html::a($this->dropdown ? Yii::t('yii', 'Delete') : '<span class="glyphicon glyphicon-trash"></span>', $url, $options);
            };
        }
    }

    protected function renderDropDownCaret($disable = false)
    {
        return Html::button('<span class="caret"></span><span class="sr-only">Toggle Dropdown</span>', [
            'type' => 'button',
            'class' => "btn btn-xs dropdown-toggle carret-action" . ($disable ? ' disabled' : ' btn-primary'),
            'data-toggle' => 'dropdown'
        ]);
    }

    protected function getAccessData($snap, $model, $key, $index) {
        $res = [];
        foreach($snap as $id => $str) {
            $str = explode('.', $str);
            if(count($str) == 1) {
                $res[$id] = ${$str[0]};
            } elseif(count($str) == 2) {
                $res[$id] = ${$str[0]}->$str[1];
            }
        }
        return $res;
    }

    protected function can($name, $model, $key, $index) {
        return Yii::$app->user->can($this->access[$name][0], $this->getAccessData($this->access[$name][1], $model, $key, $index));
    }

    /**
     * @inheritdoc
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        $first = true;
        $res = preg_replace_callback('/\\{([\w\-\/]+)\\}/', function ($matches) use ($model, $key, $index, &$first) {
            $name = $matches[1];
            if (isset($this->buttons[$name]) && (!isset($this->access[$name]) || $this->can($name, $model, $key, $index) )) {
                $url = $this->createUrl($name, $model, $key, $index);
                $options = [];
                if($this->dropdown && $first) {
                    $options['class'] = 'btn btn-default btn-xs';
                }
                $button = call_user_func($this->buttons[$name], $url, $model, $key, $options);
                if($first) {
                    $button = $button . $this->renderDropDownCaret() .
                        Html::beginTag('ul', ['class' => 'dropdown-menu dropdown-menu-right', 'role' => 'menu']);
                } else {
                    $button = Html::tag('li', $button);
                }
                $first = false;
                return $button;
            } else {
                return '';
            }
        }, $this->getTemplate($model, $key, $index), -1, $count);

        if ($this->dropdown) {
            if (!trim($res)) {
                $res = Html::a('No actions', '#', ['class' => 'btn btn-default btn-xs disabled']) .
                    Html::ul([], ['class' => 'dropdown-menu', 'role' => 'menu']);
            } else {
                if($count == 1)
                    $res = str_replace('dropdown-toggle btn-primary', 'dropdown-toggle disabled', $res);
                $res = $res . Html::endTag('ul');
            }
            $res = Html::tag('div', $res, ['class' => 'btn-group']);
        }
        return $res;
    }

    /**
     * @inheritdoc
     */
    protected function getTemplate($model, $key, $index)
    {
        if($this->template instanceof \Closure) {
            $func = $this->template;
            return $func($model, $key, $index);
        } else
            return $this->template;
    }


}