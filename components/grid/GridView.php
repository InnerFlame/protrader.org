<?php
/**
 * Created by PhpStorm.
 * @author: Roman Budnitsky
 * @date: 31.10.14 13:23
 * @copyright Copyright (c) 2014 PFSOFT LLC
 *
 * Class GridView
 * @package app\components
 */


namespace app\components\grid;

use app\models\report\ReportHelper;
use yii;
use yii\helpers\Html;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\widgets\LinkPager;

class GridView extends \yii\grid\GridView {

    // Aggregate Functions Constants
    Const TOTAL_GROUP_FUNC = 1;
    Const TOTAL_GROUP_FUNC_POSITIVE = 12;
    Const TOTAL_GROUP_FUNC_NEGATIVE = 14;
    Const MAX_GROUP_FUNC = 2;
    Const MIN_GROUP_FUNC = 3;
    Const AVERAGE_GROUP_FUNC = 4;
    Const COUNT_GROUP_FUNC = 5;

    // None sort constant
    Const NONE_SORT = 0;

    public $dataColumnClass = 'app\components\grid\DataColumn';

    public $tableOptions = ['class' => 'table datatable-basic'];
    public $theadOptions = ['class' => 'no-border'];
    public $tbodyOptions = ['class' => 'no-border-x'];

    public $footerContent = null;

    public $footableEnable = true;
    public $footableOptions = [];
    public $footableColumnsHide = [];

    public $tableWrapper = 'div';
    public $tableWrapperOptions = ['class' => 'content table-responsive'];

    public $maxPageSize = 99999;
    public $minPageSize = 10;
    public $perpageList = [10 => '10', 20 => '20', 50 => '50', 100 => 'All'];

    public $showGroup = false;
    public $groupAttr = false;
    public $groupModel = false;
    public $groupAttrFunctions = false;
    public $groupColumnFunctions = false;
    public $groupRowFunctions = false;

    private $_start_group = false;
    private $_current_group_value = false;
    private $_row_group_value = false;
    private $_current_row_options = false;
    private $_group_rows = false;

    public function init()
    {
        if(!(count($this->groupAttr) > 0)) $this->groupAttr = false;
        if(!(count($this->groupModel) > 0)) $this->groupModel = false;

        if($this->showGroup && $this->groupAttr && $this->groupModel)
            $this->dataProvider->pagination = false;

        if(isset($this->dataProvider->pagination))
        {
            if(is_object($this->dataProvider->pagination))
            {
                $this->dataProvider->pagination->pageSizeLimit[1] = $this->maxPageSize;
                $queryParams = Yii::$app->request->queryParams;
                $pageSizeParam = $this->dataProvider->pagination->pageSizeParam;
                $pageParam = $this->dataProvider->pagination->pageParam;

                if (isset($queryParams[$pageSizeParam]))
                {
                        $pageSizeParam_old = isset($_SESSION[$pageSizeParam]) ? $_SESSION[$pageSizeParam] : $this->dataProvider->pagination->defaultPageSize;
                        if($pageSizeParam_old <> $queryParams[$pageSizeParam]) $_SESSION['old_perpage'] = $pageSizeParam_old;
                        $old_perpage = isset($_SESSION['old_perpage']) ? $_SESSION['old_perpage'] : $pageSizeParam_old;

                        $_SESSION[$pageSizeParam] = $queryParams[$pageSizeParam];
                        $page_old = isset($queryParams[$pageParam]) ? $queryParams[$pageParam] : 0;

                        if($queryParams[$pageSizeParam] > 0)
                        {
                            $page_new = $page_old > 1 ? (floor((($page_old - 1) * $old_perpage + 1) / $queryParams[$pageSizeParam])) : 0;
                            $this->dataProvider->pagination->setPage($page_new);

                            if(isset($queryParams[$pageSizeParam])) unset($queryParams[$pageSizeParam]);
                            $queryParams[$pageParam] = $page_new + 1;

                            Yii::$app->request->queryParams = $queryParams;
                        }
                }

                if (isset($_SESSION[$pageSizeParam]))
                {
                    $this->dataProvider->pagination->defaultPageSize = $_SESSION[$pageSizeParam];
                }
            }
        }

        // save page in session for reload if it needs
        if(isset(Yii::$app->controller->id) && isset(Yii::$app->controller->action->id)
            && isset($pageParam)){
            if(isset($queryParams[$pageParam]))
            {
                $_SESSION['page_' . Yii::$app->controller->id . '_' . Yii::$app->controller->action->id]
                    = $queryParams[$pageParam];
            }

        }

        parent::init();
    }

    public function run() {
        parent::run();
        if($this->footableEnable) {
            $id = $this->options['id'];
            $options = yii\helpers\Json::encode($this->footableOptions);
            $this->getView()->registerJs("jQuery('#$id table').footable($options)
            .bind({'footable_row_expanded' : function(e){
                    jQuery('span.not-set').parents('.footable-row-detail-row').remove();
                }
            });
            ;");
        }
    }
    /**
     * Creates column objects and initializes them.
     */
    protected function initColumns()
    {
        if (empty($this->columns)) {
            $this->guessColumns();
        }
        foreach ($this->columns as $i => $column) { 
            if (is_string($column)) {
                $column = $this->createDataColumn($column);
            } elseif(is_array($column)) {
                $column = Yii::createObject(array_merge([
                    'class' => $this->dataColumnClass ? : DataColumn::className(),
                    'grid' => $this,
                ], $column));
            }
            if($column instanceof yii\grid\Column) {
                if (!$column->visible) {
                    unset($this->columns[$i]);
                    continue;
                }
                if($this->footableEnable) {
                    switch(true) {
                        case $column instanceof yii\grid\SerialColumn:
                            $attr = '__serial';
                            break;
                        case $column instanceof yii\grid\CheckboxColumn:
                            $attr = '__checkbox';
                            break;
                        case $column instanceof yii\grid\ActionColumn:
                            $attr = '__actions';
                            break;
                        case $column->attribute !== null:
                            $attr = $column->attribute;
                            break;
                        default:
                            $attr = null;
                    }
                    if($attr !== null) {
                        $hideBreakpoints = [];
                        foreach ($this->footableColumnsHide as $breakpoint => $colName) {
                            if ((is_array($colName) && in_array($attr, $colName)) ||
                                (is_string($colName) && $colName == $attr)
                            ) {
                                $hideBreakpoints[] = $breakpoint;
                            }
                        }
                        $column->headerOptions = ArrayHelper::merge($column->headerOptions, [
                            'data' => ['hide' => implode(',', $hideBreakpoints)]
                        ]);
                    }
                }
                $this->columns[$i] = $column;
            } else {
                unset($this->columns[$i]);
            }
        }

        foreach($this->columns as $i => $column) {
            if($column instanceof DataColumn && $column->after !== null) {
                $newColumns = [];
                $columns = $this->columns;
                foreach($columns as $j => $col) {
                    if($col != $column)
                        $newColumns[] = $col;
                    if($col->attribute == $column->after) {
                        $newColumns[] = $column;
                        unset($this->columns[$i]);
                    }
                }
                $this->columns = $newColumns;
            }
        }
    }

    /**
     * Renders the table header.
     * @return string the rendering result.
     */
    public function renderTableHeader()
    {
        return Html::tag('thead', str_replace(array('<thead>','</thead>'), '', parent::renderTableHeader()), $this->theadOptions);
    }

    /**
     * Agregate group Functions
     * @param $signArr
     * @param $function_code
     * @return float|int|mixed|null
     */
    public static function agregateArr(&$signArr, $function_code)
    {
        if($function_code > 0)
            if(is_array($signArr))
            {
                if($function_code == self::TOTAL_GROUP_FUNC)
                {
                    $response = round(array_sum($signArr), 2);
                }

                if($function_code == self::TOTAL_GROUP_FUNC_POSITIVE)
                {
                    $total = 0;

                    if(count($signArr) > 0)
                        foreach($signArr as $number)
                        {
                            if($number > 0)
                                $total += $number;
                        }

                    $response = round($total, 2);
                }

                if($function_code == self::TOTAL_GROUP_FUNC_NEGATIVE)
                {
                    $total = 0;

                    if(count($signArr) > 0)
                        foreach($signArr as $number)
                        {
                            if($number < 0)
                                $total += $number;
                        }

                    $response = round($total, 2);
                }

                if($function_code == self::MAX_GROUP_FUNC)
                {
                    $response = max($signArr);
                }

                if($function_code == self::MIN_GROUP_FUNC)
                {
                    $response = min($signArr);
                }

                if($function_code == self::COUNT_GROUP_FUNC)
                {
                    $response = count($signArr);
                }

                if($function_code == self::AVERAGE_GROUP_FUNC)
                {
                    if(count($signArr) > 0)
                        $response = round( array_sum($signArr) / count($signArr), 2);
                }

                return $response;

            }

        return null;
    }

    public function agregateAttr($signArr, $attr, $key_column)
    {
        $response = false;

        $function_code = false;

        if(!is_array($this->groupAttrFunctions[$attr]))
            $function_code = $this->groupAttrFunctions[$attr];

        // -1 cause in groupFunction we send index
        if(isset($this->groupAttrFunctions[$attr][$key_column - 1]))
            $function_code = $this->groupAttrFunctions[$attr][$key_column - 1];

        return self::agregateArr($signArr, $function_code);

    }

    /**
     * Return Footer Agergate Result Row
     * @param mixed $model
     * @param string $key
     * @param integer $index
     * @param mixed $models
     *
     * @return string
     */
    public function renderFooterAgregateRow($model, $key, $index, $models)
    {

        $cells = [];
        /* @var $column Column */
        foreach ($this->columns as $key_column =>  $column)
        {
                $_column_content = '-';

                $this->renderGroupFunctions($_column_content, $model, $key_column, $index, $models, 0, $key);

                if($_column_content <> '-') {
                    $_column_content = '<td class="footer-total right-align">' . $_column_content . '</td>';
                }else{
                    $_column_content = '<td>'. $_column_content .'</td>';
                }

            $cells[] = $_column_content;
        }

        return Html::tag('tr', implode('', $cells));
    }

    /**
     * Agregate Group Column if isset group attr function
     * @param string $_column_content
     * @param mixed $model
     * @param string $key_column
     * @param integer $index
     * @param mixed $models
     * @param integer $start_key
     * @param integer $end_key
     * @return bool|float|int|mixed|null|number
     */
    public function renderGroupFunctions(&$_column_content, $model, $key_column, $index,
                                         $models, $start_key, $end_key)
    {
        $response = null;

        if(isset($this->columns[$key_column]))
            if($this->groupAttrFunctions)
                if(count($this->groupAttrFunctions) > 0)
                {
                    $column = $this->columns[$key_column];

                    if(isset($column->attribute))
                    if($attr = $column->attribute){ // set $attr

                        if(isset($this->groupAttrFunctions[$attr]) &&
                            (isset($model[$attr]) || isset($model->$attr))){

                            $signArr = [];

                            if(count($models) > 0)
                                foreach($models as $_key => $_row)
                                    if($_key >= $start_key && $_key <= $end_key){
                                        if(isset($_row[$attr]) || isset($_row->$attr)) {
                                            $signArr[] = $_row[$attr];
                                        }
                                    }

                            $response = null;

                            if(count($signArr))
                            {
                                $response = $this->agregateAttr($signArr, $attr, $key_column);

                                if($response)
                                    $_column_content = ReportHelper::standartAttrValue($response, $attr);

                                return $response;

                            }

                        }
                    }

                }

        return $response;
    }

    public function checkGroupRowFunction($_data, $sign)
    {
        if(is_array($this->groupRowFunctions)) {

            $_model = false;
            $_attr = false;

            ReportHelper::getAttrModelFromString($_model, $_attr, $sign);

            if($_attr && $_model)
                if (isset($this->groupRowFunctions[$_attr]))
                {
                   $_function = $this->groupRowFunctions[$_attr];

                   if($_function == "day")
                       return date('M d, Y', $_data);

                   if($_function == "month")
                       return date('M Y', $_data);

                   if($_function == "year")
                       return date('Y', $_data);

                }

        }

        return $_data;

    }

    public function checkColumnGroupRowFunction(&$column_content, $column, $row)
    {
        if(is_array($this->groupRowFunctions) && isset($column->attribute))
            if(isset($row[$column->attribute]))
            {
                $_attribute = $column->attribute;

                $_model = false;
                $_attr = false;

                ReportHelper::getAttrModelFromString($_model, $_attr, $_attribute);

                if($_attr && $_model)
                    if (isset($this->groupRowFunctions[$_attr]))
                    {
                       $_function = $this->groupRowFunctions[$_attr];

                       if($_function == "day")
                           $column_content = '<td>' . date('M d, Y', $row[$_attribute]) . '</td>';

                       if($_function == "month")
                           $column_content = '<td>' . date('M Y', $row[$_attribute]) . '</td>';

                       if($_function == "year")
                           $column_content = '<td>' . date('Y', $row[$_attribute]) . '</td>';

                    }

            }

        return true;
    }

    /**
     * Renders a group table row with the given data model and key.
     * @param mixed $model the data model to be rendered
     * @param mixed $key the key associated with the data model
     * @param integer $index the zero-based index of the data model among the model array returned by [[dataProvider]].
     * @return string the rendering result
     */
    public function renderGroupTableRow($model, $start_key, $index, $models)
    {
        // for object we must clone to leave $model without changes
        if(is_object($model)) {
            $model_get = clone $model;
        }else{
            $model_get = $model;
        }

        $attrArr = $this->groupAttr;

        $current_row = true;
        $end_key = $start_key;

        // agregate attributes
        $getAttrArr = [];

        // Empty Columns


        // for agregate attributes get array
        if(is_array($this->groupAttrFunctions))
        {
            $getAttrArr = array_keys($this->groupAttrFunctions);
            if(!(count($getAttrArr) > 0)) $getAttrArr = [];
        }

        // Get $end_key for current group
        foreach($models as $_key => $_row)
        {
            if($_key >= $start_key && $current_row){

                // Foreach $attrArr element
                foreach($attrArr as $sign)
                {
                    if($this->_current_group_value[$sign]
                        != $this->checkGroupRowFunction($_row[$sign], $sign))
                        $current_row = false;
                }

                // Row of group
                if($current_row)
                {
                    $end_key = $_key;
                }

            }
        }

        $cells = [];
        /* @var $column Column */
        foreach ($this->columns as $key_column =>  $column)
        {
            $_column_content = '<td></td>'; // empty group column

            if(!(strpos(get_class($column), 'SerialColumn') == false)){
                $_column_content = '<td><i class="fa fa-plus group-plus"></i>
                <i class="fa fa-minus group-minus"></i></td>';
            }

            if(strpos(get_class($column), 'ActionColumn') == false
                && strpos(get_class($column), 'SerialColumn') == false)
            {
                $_column_content = $column->renderDataCell($model_get, $key_column, $index);

                $_attribute = isset($column->attribute) ? $column->attribute : false;

                // For not Group Attr
                if(!in_array($_attribute, $attrArr))
                {
                    $_column_content = '<td>-</td>';
                // For Group Attr
                }else{
                    $this->checkColumnGroupRowFunction($_column_content, $column, $model_get);
                }

                if (strpos($_column_content, 'not set') > 0 || ($_attribute && in_array($_attribute, $getAttrArr)))
                {
                    $_column_content = '-';

                    $response = $this->renderGroupFunctions($_column_content, $model_get,
                        $key_column, $index, $models, $start_key, $end_key);

                    if($response)
                    {
                        $_column_content = '<td class="right-align">' . $_column_content . '</td>';
                    }else{
                        $_column_content = '<td>-</td>';
                    }

                }

            }

            $cells[] = $_column_content;
        }

        if ($this->rowOptions instanceof Closure) {
            $options = call_user_func($this->rowOptions, $model, $key_column, $index, $this);
        } else {
            $options = $this->rowOptions;
        }

        $options['data-key'] = is_array($key_column) ? json_encode($key_column) : (string) $key_column;

        return Html::tag('tr', implode('', $cells), $options);

    }

    function startCollapse(&$rows, $model, $key, $index, $models)
    {
        $_attr = $this->groupAttr;

        $this->_start_group = true;
        $is_null_attr = false;
        $this->_current_group_value = false;
        $this->_row_group_value = false;

        // Get current group $attrs
        if(count($_attr) > 0)
            foreach($_attr as $sign)
            {
                $this->_current_group_value[$sign] =
                    (isset($model[$sign]) || isset($model->$sign)) ?
                        $this->checkGroupRowFunction($model[$sign], $sign) : null;

                $this->_row_group_value[$sign] = $this->_current_group_value[$sign];

                if($this->_current_group_value[$sign] == null) $is_null_attr = true;

            }

        $this->_current_row_options =  $this->rowOptions;

        $this->rowOptions = ['class' => 'collapsed',
            'data-toggle'=> 'collapse', 'data-target' => '.collapse_' . $index,
                'aria-expanded' => 'false', 'aria-controls' => 'collapse_' . $index];

        $rows[] = $this->renderGroupTableRow($model, $key, $index, $models);

        $this->rowOptions = [
            'class' => 'collapse collapse_'. $index
        ];

        Yii::$app->customFunctions->clearArrAttr($model, $_attr, '');
    }

    /**
     * Retunr Row Before Group
     * @param mixed $rows
     * @param mixed $model
     * @param string $key
     * @param index $index
     * @param mixed$models
     *
     * @return string
     */
    public function renderShowGroupBeforeRow(&$rows, $model, $key, $index, $models)
    {
        // get Group Attrs
        $_attr = $this->groupAttr;

        if($this->groupAttr && $this->groupModel)
        {

            if (!$this->_start_group)
            {
                // Start Collapse of group
                $this->startCollapse($rows, $model, $key, $index, $models);

            }else{

                if(count($_attr) > 0)
                    foreach($_attr as $sign)
                    {
                        $this->_row_group_value[$sign] = (isset($model[$sign]) || $model->$sign) ?
                            $this->checkGroupRowFunction($model[$sign], $sign) : null;
                    }

                $is_current_sign = true;

                if($this->_row_group_value != $this->_current_group_value)
                    $is_current_sign = false;

                if($is_current_sign)
                {
                   Yii::$app->customFunctions->clearArrAttr($model, $_attr, '');

                }else{

                    $this->startCollapse($rows, $model, $key, $index, $models);

                }

            }

        }

    }

    function endCollapse(&$rows)
    {
        // Custom Function for After Group Adding
    }

    public function renderShowGroupAfterRow(&$rows, $model, $key, $index, $models)
    {

        $_attr = $this->groupAttr;

        if($this->groupAttr && $this->groupModel)
        {
            if($this->_row_group_value != $this->_current_group_value)
            {
                if($this->_start_group) $this->endCollapse($rows);

                $this->_start_group = false;
                $this->_current_group_value = false;
            }

        }else{

            if($this->_start_group) $this->endCollapse($rows);

            $this->_start_group = false;
            $this->_current_group_value = false;
        }
    }

    /**
     * Renders the table body.
     * @return string the rendering result.
     */
    public function customRenderTableBody()
    {
        $models = array_values($this->dataProvider->getModels());
        $keys = $this->dataProvider->getKeys();
        $rows = [];
        foreach ($models as $index => $model)
        {
            $key = $keys[$index];
            if ($this->beforeRow !== null)
            {
                $row = call_user_func($this->beforeRow, $model, $key, $index, $this);
                if (!empty($row)) {
                    $rows[] = $row;
                }
            }

            if($this->showGroup)
                $this->renderShowGroupBeforeRow($rows, $model, $key, $index, $models);

            $rows[] = $this->renderTableRow($model, $key, $index);

            if($this->showGroup)
                $this->renderShowGroupAfterRow($rows, $model, $key, $index, $models);

            if ($this->afterRow !== null) {
                $row = call_user_func($this->afterRow, $model, $key, $index, $this);
                if (!empty($row)) {
                    $rows[] = $row;
                }
            }
        }

        if(count($rows) > 0)
        if($this->groupAttrFunctions)
            if(count($this->groupAttrFunctions) > 0)
                $rows[] = $this->renderFooterAgregateRow($model, $key, $index, $models);

        if (empty($rows)) {
            $colspan = count($this->columns);

            return "<tbody>\n<tr><td colspan=\"$colspan\">" . $this->renderEmpty() . "</td></tr>\n</tbody>";
        } else {
            return "<tbody>\n" . implode("\n", $rows) . "\n</tbody>";
        }
    }

    public function renderTableBody()
    {
        return Html::tag('tbody', str_replace(array('<tbody>','</tbody>'), '', $this->customRenderTableBody()), $this->tbodyOptions);
    }

    public function renderTableFooter() {
        if($this->footerContent !== null) {
            return Html::tag('tfoot', $this->footerContent);
        } else {
            return parent::renderTableFooter();
        }
    }

    public function renderItems() {
        $table = parent::renderItems();
        if($this->tableWrapper) {
            return Html::tag('div', $table, $this->tableWrapperOptions);
        } else
            return $table;
    }

    public function renderPager()
    {
        $this->pager['firstPageLabel'] = true;
        $this->pager['lastPageLabel'] = true;
        $this->pager['nextPageLabel'] = '&#8250;';
        $this->pager['prevPageLabel'] = '&#8249;';
        $this->pager['firstPageLabel'] = '&laquo;';
        $this->pager['lastPageLabel'] = '&raquo;';


        $pager = parent::renderPager();
        $pageSizeOutPut = '';

        if(isset($this->dataProvider->allModels))
        {

            if(is_object($this->dataProvider->pagination))
            {

                if(count($this->dataProvider->allModels) > $this->minPageSize) {
                    $queryParams = Yii::$app->request->queryParams;
                    $defaultPageSize = $this->dataProvider->pagination->defaultPageSize;
                    $pageSizeParam = $this->dataProvider->pagination->pageSizeParam;

                    if(count($this->dataProvider->allModels) > 100) $this->perpageList[100] = '100';

                    $pageSizeOutPut = PaginationColumn::widget([
                        'id' => $this->id,
                        'pageSizeParam' => $pageSizeParam,
                        'defaultPageSize' => $defaultPageSize,
                        'list' => $this->perpageList,
                    ]);
                }
            }

        }

        return $pager .$pageSizeOutPut;

  }

} 