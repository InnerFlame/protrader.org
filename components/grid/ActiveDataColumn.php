<?php
/**
 * Created by PhpStorm.
 * @author: devbrom (Roman Budnitsky)
 * @date: 13.02.15 12:09
 */

namespace app\components\grid;

use kartik\date\DatePicker;
use yii;
use yii\helpers\Html;
use app\components\MultiSelectCustom;
use devbrom\releases\models\Note;

class ActiveDataColumn extends yii\grid\DataColumn {

    public $type = 'input';
    public $controlOptions = ['class' => 'form-control'];
    public $selectOptions = null;
    public $multiSelectOptions = null;
    public $format = 'raw';

    public $condition = null;
    protected static $staticCondition = null;


    public function renderHeaderCell()
    {
        $label = $this->renderHeaderCellContent();
        if(strlen($label) == strlen(strip_tags($label))) $label = Html::tag('span', $label);
        return Html::tag('th', $label, $this->headerOptions);
    }

    /**
     * @inheritdoc
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        if($this->condition === null) {
            if(self::$staticCondition === null) {
                throw new \HttpInvalidParamException('condition can\'t be blank');
            } elseif(($func = self::$staticCondition)  instanceof \Closure) {
                /** @var \Closure $func */
                $condition = $func($model, $key, $index);
            } else {
                $condition = self::$staticCondition;
            }
        } elseif(($func = $this->condition) instanceof \Closure) {
            /** @var \Closure $func */
            $condition = $func($model, $key, $index);
        } else {
            $condition = $this->condition;
        }

        if($condition) {
            return call_user_func([$this, 'render' . $this->type], $model);
        } else {
            if($this->type == 'datepicker') $this->format = 'date';
            return parent::renderDataCellContent($model, $key, $index);
        }
    }

    protected function renderInput($model)
    {
        $attr = $this->attribute;
        return Html::input('text', $attr, $model->$attr, $this->controlOptions);
    }

    protected function renderSelect($model)
    {
        if($this->selectOptions === null)
            throw new \HttpInvalidParamException('selectOption can\'t be blank');
        $attr = $this->attribute;
        return Html::dropDownList($attr, $model->$attr, $this->selectOptions, $this->controlOptions);
    }

    protected function renderMultiSelect($model)
    {
        if($this->selectOptions === null)
            throw new \HttpInvalidParamException('selectOption can\'t be blank');
        $attr = $this->attribute;

        $values = is_array($model->$attr) ? $model->$attr : [];

        if($model instanceof Note) {
            if ($model->$attr) {
                $values = $model->visabilityToArray($model->$attr);
            }
        }

        $multiSelectOptions = isset($this->multiSelectOptions) ? $this->multiSelectOptions : null;

        return MultiSelectCustom::widget([
                                    'id' => $model->id,
                                    'options' => ['multiple' => 'multiple'],
                                    'data' => $this->selectOptions,
                                    'value' => $values,
                                    'name' => $attr,
                                    'clientOptions' => [
                                        'includeSelectAllOption' => isset($multiSelectOptions['includeSelectAllOption']) ? $multiSelectOptions['includeSelectAllOption'] : true,
                                        'numberDisplayed' => isset($multiSelectOptions['numberDisplayed']) ? $multiSelectOptions['numberDisplayed'] : 2,
                                        'buttonContainer' => '<div class="grid-multiselect"/>',
                                        'buttonClass' => 'form-control',
                                        'nonSelectedText' => isset($multiSelectOptions['nonSelectedText']) ? $multiSelectOptions['nonSelectedText'] : 'All',
                                        'allSelectedText' => isset($multiSelectOptions['nonSelectedText']) ? $multiSelectOptions['nonSelectedText'] : 'All',
                                        'onDropdownHide' =>  isset($multiSelectOptions['onDropdownHide']) ? $multiSelectOptions['onDropdownHide'] : null,
                                        'onDropdownShow' =>  isset($multiSelectOptions['onDropdownShow']) ? $multiSelectOptions['onDropdownShow'] : null,
                                        'onChange' =>  isset($multiSelectOptions['onChange']) ? $multiSelectOptions['onChange'] : null,
                                    ],
        ]);
    }

    protected function renderDatePicker($model)
    {
        $attr = $this->attribute;
        return DatePicker::widget([
            'name' => $attr,
            'type' => DatePicker::TYPE_INPUT,
            'value' => $this->grid->formatter->asDate((int) $model->$attr),
            'options' => $this->controlOptions,
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'M d, yyyy',
            ]
        ]);
    }

    public static function setStaticCondition($value) {
        self::$staticCondition = $value;
    }

}