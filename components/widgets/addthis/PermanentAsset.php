<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2016
 */

namespace app\components\widgets\addthis;


use yii\web\View;

class PermanentAsset extends \yii\web\AssetBundle
{
    public $js = [
        '//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b1c86d8de0add6'
    ];

    public $jsOptions = [
        'position' => View::POS_END
    ];
    
    
    
}