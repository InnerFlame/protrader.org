<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2016
 */

namespace app\components\widgets\addthis;


use yii\web\View;

class AssetBundle extends \yii\web\AssetBundle
{
    public $js = [
        '//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57750c9db8c3d9ea'
    ];

    public $jsOptions = [
        'position' => View::POS_END
    ];
}