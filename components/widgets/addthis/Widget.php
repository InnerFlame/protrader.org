<?php

/**
 * Виджет добавляющий кнопки соц сетей, для шаринга материалов
 * с сайта
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @updated_by A.Gudenko in 15.08.2016
 * @copyright 2016
 */

namespace app\components\widgets\addthis;

use yii\bootstrap\Html;
use Yii;

/**
 * USAGE:
 * 
 * // <to enable widget>
 *  'addthis' => [
 *      'enable' => true,
 *      'blockUrl' => 'your_url',
 *      'permanentUrl' => 'your_url'
 *  ],
 * 
 * // <to add element as custom block.>
 * 
 *   Widget::widget([
 *       'position' => 'block',
 *       'wrapper' => 'div',
 *       'className' => 'addthis_sharing_toolbox'
 *   ]);
 * 
 * // <to add element as right / left fixed block. The param 'exceptUrl' will be disabled widget for specifed route.>
 * 
 *  Widget::widget([
 *      'position' => 'permanent',
 *      'exceptUrl' => Url::to('some/route')
 *  ]);
 * 
 * // <also it possible to add a few urls to exception by setting attribute 'exceptUrls'.>
 *  Widget::widget([
 *      'position' => 'permanent',
 *      'exceptUrls' => [ 
 *          Url::to('some/route'), 
 *          Url::to('some/another/route') 
 *      ]
 *  ]);
 */
class Widget extends \yii\base\Widget
{

    const POSITION_BLOCK = 'block';
    const POSITION_PERMANENT = 'permanent';
    const POSITION_CUSTOM = 'custom';

    public $position = 'block';
    public $className = 'addthis_sharing_toolbox';
    public $wrapper = 'div';
    public $exceptUrl = null;
    public $exceptUrls = null;
    public $onlyUrl = null;
    public $url = '';
    public $exceptRegexp = null;
    protected $_html = '';
    protected $_error = false;

    /**
     * 
     * @return string
     */
    public function run()
    {
        $this->checkErrors();

        if (!$this->getError()) {
            if ($this->position == self::POSITION_BLOCK) {
                AssetBundle::register($this->getView());
                $this->setHtml(Html::tag($this->wrapper, '', ['class' => $this->className]));
            }

            if ($this->position == self::POSITION_PERMANENT) {
                $this->getView()->registerJsFile($this->url);
                //PermanentAsset::register($this->getView());
            }

            if ($this->position == self::POSITION_CUSTOM) {
                Yii::$app->getView()->registerJsFile($this->url);
            }
        }

        return $this->getHtml();
    }

    /**
     * getting html
     * @return string
     */
    protected function getHtml()
    {
        return $this->_html;
    }

    /**
     * setting new html by overwriting existed html
     * @param string $string
     */
    protected function setHtml($string)
    {
        $this->_html = $string;
    }

    /**
     * append html to existed html
     * @param string $string
     */
    protected function appendHtml($string)
    {
        $this->_html .= $string;
    }

    /**
     * checking errors:
     * enabling and exceptions
     * @return void
     */
    protected function checkErrors()
    {
        $errors = [];

        array_push($errors, $this->checkEnable());
        array_push($errors, $this->checkExceptions());

        foreach ($errors as $error) {
            if ($error === true) {
                $this->setError();
                return;
            }
        }
    }

    /**
     * To enable feature add to your params
     * 'addthis' => [
     *      'enable' => true,
     *      'blockUrl' => 'your_url',
     *      'permanentUrl' => 'your_url'
     *  ],
     * @return boolean
     */
    protected function checkEnable()
    {
        if (isset(Yii::$app->params['addthis'])) {
//            if (isset(Yii::$app->params['addthis'][$this->position . 'Url'])) {
                return false;
//            }
        }

        return true;
    }

    /**
     * checking setted exceptions
     * @return boolean
     */
    protected function checkExceptions()
    {
        $urls = [];
        
        $current_url = \yii\helpers\Url::current();
        
        if($this->exceptRegexp && preg_match($this->exceptRegexp, $current_url)){
            return true;
        }

        if ($e_urls = $this->exceptUrls) {
            $urls = array_merge($urls, $e_urls);
        }

        if ($e_url = $this->exceptUrl) {
            $urls = array_merge($urls, [$this->exceptUrl]);
        }

        if (!empty($urls)) {
            foreach ($urls as $url) {
                if ($url == $current_url) {
                    return true;
                }
            }
        }
        
        

        return false;
    }

    /**
     * error setters & getters
     */
    protected function setError()
    {
        $this->_error = true;
    }

    protected function getError()
    {
        return $this->_error;
    }

    protected function resetError()
    {
        $this->_error = false;
    }

}
