<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 * Date: 11.04.2016
 * Time: 14:57
 */

namespace app\components\widgets;


use yii\base\Widget;

class Button extends Widget
{
    public $title;

    public $selector;

    public function run()
    {
        return $this->render('button.twig', ['model' => $this]);
    }
}