<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 * Date: 31.03.2016
 * Time: 14:59
 */

namespace app\components\widgets\error;


use app\components\widgets\error\models\ErrorPage;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class RedirectController extends Controller
{

    public function actionShow($id)
    {
        $page = ErrorPage::findOne($id);
        if(!$page)
            throw new NotFoundHttpException;

        $page->updateCounters(['jumps' => 1]);
        $this->redirect(Url::to($page->url));
    }
}