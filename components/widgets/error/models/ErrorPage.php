<?php

namespace app\components\widgets\error\models;

use app\components\Entity;
use app\components\langInput\LangInputBehavior;
use Yii;
use yii\bootstrap\Html;

/**
 * This is the model class for table "error_pages".
 *
 * @property integer $id
 * @property string $expression
 * @property string $title
 * @property string $url
 * @property integer $view
 * @property integer $jumps
 */
class ErrorPage extends Entity
{
    const TYPE_404 = 0;
    const TYPE_301 = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'error_pages';
    }

    public function behaviors()
    {
        return [
            [
                'class' => LangInputBehavior::className(),
                'attributes' => ['title'],
            ],
        ];
    }

    public static function getListStatus()
    {
        return [
            self::TYPE_301 => '301',
            self::TYPE_404 => '404'
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['expression', 'title', 'url', 'type'], 'required'],
            [['view', 'jumps', 'type'], 'integer'],
            [['expression', 'url'], 'string', 'max' => 255],
            [['title'], 'string', 'max' => 128]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'expression' => 'Expression',
            'title' => 'Title',
            'url' => 'Url',
            'view' => 'View',
            'jumps' => 'Jumps',
            'type' => 'Type Handler'
        ];
    }

    public function getTypeEntity()
    {
        return self::HANDLER_ERROR_PAGE;
    }

    public function getName()
    {
        return $this->title;
    }

    public function getViewUrl()
    {
        return '/redirect/show?id='.$this->id;
    }

}
