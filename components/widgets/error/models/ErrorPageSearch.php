<?php

namespace app\components\widgets\error\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ErrorPageSearch represents the model behind the search form about `app\components\widgets\error\models\ErrorPage`.
 */
class ErrorPageSearch extends ErrorPage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'view', 'jumps', 'deleted', 'type'], 'integer'],
            [['expression', 'title', 'url'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ErrorPage::find()->notDeleted();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'id'      => $this->id,
            'view'    => $this->view,
            'jumps'   => $this->jumps,
            'deleted' => $this->deleted,
            'type'    => $this->type,
        ]);

        $query->andFilterWhere(['like', 'expression', $this->expression])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'url', $this->url]);

        return $dataProvider;
    }
}
