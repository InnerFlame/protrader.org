<?php

namespace app\components\widgets\error\models;

/**
 * This is the ActiveQuery class for [[ErrorPage]].
 *
 * @see ErrorPage
 */
class ErrorPagesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ErrorPage[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ErrorPage|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @return $this
     */
    public function notDeleted()
    {
        return $this->andWhere(['<>', 'deleted', 1]);
    }
}
