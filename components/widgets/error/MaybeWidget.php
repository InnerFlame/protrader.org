<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 * Date: 31.03.2016
 * Time: 14:46
 */

namespace app\components\widgets\error;


use app\components\Entity;
use app\components\widgets\error\models\ErrorPage;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;

class MaybeWidget extends Widget
{

    public function run()
    {
        $foundPages = [];

        $url = \Yii::$app->request->getAbsoluteUrl();
        $pages = ErrorPage::find()->where(['deleted' => Entity::NOT_DELETED])->all();
        foreach($pages as $page){
            if(preg_match($page->expression, $url)){
                if($page->type == ErrorPage::TYPE_301){
                    \Yii::$app->controller->redirect(Url::to($page->url), 301);
                    \Yii::$app->end();
                    break;
                }
                if($page->updateCounters(['view' => 1])){
                    $foundPages[] = $page;
                }
            }
        }
        return $this->render('links.twig', ['pages' => $foundPages, 'output' => $this->outputListUrl($foundPages)]);
    }

    /**
     * Generate list url for 404 page with separator ","
     * and add "or" if count links more 4
     * Example: Articles, Forum, Articles-Protrader or News
     * @param $pages
     * @return string
     */
    protected function outputListUrl($pages)
    {
        $output = '';
        $count = count($pages);
        for($i = 0; $i < $count; $i++){
            if($i == 3){
                $output .= ' or ';
            }
            $output .= Html::a($pages[$i]->title, $pages[$i]->getViewUrl());
            if(($i+1) < $count && $i != 2){
                $output .=', ';
            }
        }
        return $output;
    }
}