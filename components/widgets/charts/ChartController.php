<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2016
 */

namespace app\components\widgets\charts;


use app\modules\cabinet\modules\chart\models\UserSettings;
use yii\web\Controller;

class ChartController extends Controller
{

    public function actionSave()
    {
        $settings = $_POST['data'];

        $settings = new UserSettings();
        $settings->value = $settings;
        $settings->panel_id = $_POST['panel'];

    }
}