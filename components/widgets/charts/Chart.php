<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2016
 */

namespace app\components\widgets\charts;


use yii\base\Widget;

class Chart extends Widget
{

    public $host = '192.168.0.200:8888/';

    public function run()
    {

        return $this->render('chart', ['chart' => $this]);
    }
}