<?php $this->registerJsFile('/js/webx.js') ?>
<?php $this->registerCssFile('Theme/light/light.min.css') ?>
<div class="chartBox">
	
	<div id="chart" class="chartContainer">

		<div class="chartHead">
<!-- 			<div class="dropBtn">
				<img src="/images/charts/derevo.png" alt="Tree">
			</div>
			<div class="dropMenu">
				<div class="dropHead">
					<div class="dropBtn">
						<img src="/images/charts/derevo.png" alt="Tree">
					</div>
					<div class="searchField">
						<input type="search" value="DXM16" class="search">
					</div>
				</div>
				<div class="dropBody">
					
				</div>
			</div> -->
			<div class="titile">
				<span class="name">DXM16</span> <span class="description">(U.S. Dollar index June 2016)</span>
				<div class="pos">
					<span class="cord">
						O:
					</span>
					<span id="pos-o">---</span>
					<span class="cord">
						H:
					</span>
					<span id="pos-h">---</span>
					<span class="cord">
						L:
					</span>
					<span id="pos-l">---</span>
					<span class="cord">
						C:
					</span>
					<span id="pos-c">---</span>
				</div>
			</div>
			<div class="selects disabled">
				<div class="select">
					<ul class="icons">
						<li><img src="/images/charts/line.png" alt="Line"></li>
						<li><img src="/images/charts/bar.png" alt="Bar"></li>
						<li class="active"><img src="/images/charts/candle.png" alt="Candle"></li>
						<li class="disabled"><img src="/images/charts/dots.png" alt="Dots"></li>
						<li><img src="/images/charts/dotted_line.png" alt="Dotted line"></li>
						<li><img src="/images/charts/forest.png" alt="Forest"></li>
						<li><img src="/images/charts/area.png" alt="Area"></li>
					</ul>
					<ul class="list">
						<li onclick="web.setDrawingStyle(TerceraChartDrawingType.Line);"><img src="/images/charts/line.png" alt="Line"> Line</li>
						<li onclick="web.setDrawingStyle(TerceraChartDrawingType.Bar);"><img src="/images/charts/bar.png" alt="Bar"> Bar</li>
						<li onclick="web.setDrawingStyle(TerceraChartDrawingType.Candle);"class="active"><img src="/images/charts/candle.png" alt="Candle"> Candle</li>
						<li class="disabled" onclick="web.setDrawingStyle(TerceraChartDrawingType.Dot);"><img src="/images/charts/dots.png" alt="Dots"> Dots</li>
						<li onclick="web.setDrawingStyle(TerceraChartDrawingType.DotLine);"><img src="/images/charts/dotted_line.png" alt="Dotted line"> Dotted line</li>
						<li onclick="web.setDrawingStyle(TerceraChartDrawingType.Forest);"><img src="/images/charts/forest.png" alt="Forest"> Forest</li>
						<li onclick="web.setDrawingStyle(TerceraChartDrawingType.Solid);"><img src="/images/charts/area.png" alt="Area"> Area</li>
					</ul>
				</div>
				<div class="select wi">
					<ul class="icons">
						<li class="active"><span>1M</span></li>
						<li><span>1H</span></li>
						<li><span>1D</span></li>
					</ul>
					<ul class="list">
						<li class="active" onclick="web.setPeriod(Periods.MIN);">1M</li>
						<li onclick="web.setPeriod(Periods.HOUR);">1H</li>
						<li onclick="web.setPeriod(Periods.DAY);">1D</li>
					</ul>
				</div>
			</div>

		</div>

		<div class="chartBody" id="chart">

		</div>

		<div class="chartFooter">
			<div class="icons">
				<div id="shareChart" class="icon">
					<img src="/images/charts/share.png" alt="Share">
				</div>

				<div id="downloadChart" class="icon">
					<img src="/images/charts/download.png" alt="Download">
				</div>

				<div id="printChart" class="icon">
					<img src="/images/charts/print.png" alt="Print">
				</div>
			</div>

			<div class="iconsRight">
				<div id="chpok" class="icon ant">
					<img src="/images/charts/anti-chpok.png" class="normal" alt="Anti-Chpok">
					<img src="/images/charts/chpok.png" class="anti" alt="chpok">
				</div>

				<div id="fullCHarts" class="icon">
					<img src="/images/charts/full.png" class="normal" alt="full">
					<img src="/images/charts/anti-full.png" class="anti" alt="anti-full">
				</div>
			</div>
		</div>

	</div>

	<div class="clear"></div>

	<div class="popularChartsWrapper">
		<div class="chartSidebar">
		<div class="sidebarInner">
			<div class="headSidebar">
				<div class="title">
					Watchlist <span>(28)</span>
					<div class="pull-right">
						<!-- <div class="add">
							<img src="/images/charts/plus.png" alt="Plus">
						</div> -->
					</div>
				</div>
			</div>
			<div style="height: 100%;" id="wl">

			</div>
		</div>
	</div>
		<div class="popularCharts">
			<h2 class="h2">
				Popular charts
			</h2>
			<div class="panel popularPanel">

				<div class="popWrap">
					<div class="popInfo">
						<div class="h2">Forex</div>
					</div>
					<div class="popBody">
						<div class="slider">
							<div class="slide">
								<div class="title">
									EUR/USD
								</div>
								<div class="descr">
									H:1280.80 L:1266.30
								</div>
								<div class="img">
									<img src="https://api.fnkr.net/testimg/165x100/FFFFFF/438ed1/?text=EUR/USD">
								</div>
							</div>

							<div class="slide">
								<div class="title">
									EUR/USD
								</div>
								<div class="descr">
									H:1280.80 L:1266.30
								</div>
								<div class="img">
									<img src="https://api.fnkr.net/testimg/165x100/FFFFFF/438ed1/?text=EUR/USD">
								</div>
							</div>

							<div class="slide">
								<div class="title">
									EUR/USD
								</div>
								<div class="descr">
									H:1280.80 L:1266.30
								</div>
								<div class="img">
									<img src="https://api.fnkr.net/testimg/165x100/FFFFFF/438ed1/?text=EUR/USD">
								</div>
							</div>

							<div class="slide">
								<div class="title">
									EUR/USD
								</div>
								<div class="descr">
									H:1280.80 L:1266.30
								</div>
								<div class="img">
									<img src="https://api.fnkr.net/testimg/165x100/FFFFFF/438ed1/?text=EUR/USD">
								</div>
							</div>
							
							<div class="slide">
								<div class="title">
									EUR/USD
								</div>
								<div class="descr">
									H:1280.80 L:1266.30
								</div>
								<div class="img">
									<img src="https://api.fnkr.net/testimg/165x100/FFFFFF/438ed1/?text=EUR/USD">
								</div>
							</div>
							
							<div class="slide">
								<div class="title">
									EUR/USD
								</div>
								<div class="descr">
									H:1280.80 L:1266.30
								</div>
								<div class="img">
									<img src="https://api.fnkr.net/testimg/165x100/FFFFFF/438ed1/?text=EUR/USD">
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="popWrap">
					<div class="popInfo">
						<div class="h2">CFDs</div>
					</div>
					<div class="popBody">
						<div class="slider">
							<div class="slide">
								<div class="title">
									EUR/USD
								</div>
								<div class="descr">
									H:1280.80 L:1266.30
								</div>
								<div class="img">
									<img src="https://api.fnkr.net/testimg/165x100/FFFFFF/438ed1/?text=EUR/USD">
								</div>
							</div>

							<div class="slide">
								<div class="title">
									EUR/USD
								</div>
								<div class="descr">
									H:1280.80 L:1266.30
								</div>
								<div class="img">
									<img src="https://api.fnkr.net/testimg/165x100/FFFFFF/438ed1/?text=EUR/USD">
								</div>
							</div>

							<div class="slide">
								<div class="title">
									EUR/USD
								</div>
								<div class="descr">
									H:1280.80 L:1266.30
								</div>
								<div class="img">
									<img src="https://api.fnkr.net/testimg/165x100/FFFFFF/438ed1/?text=EUR/USD">
								</div>
							</div>

							<div class="slide">
								<div class="title">
									EUR/USD
								</div>
								<div class="descr">
									H:1280.80 L:1266.30
								</div>
								<div class="img">
									<img src="https://api.fnkr.net/testimg/165x100/FFFFFF/438ed1/?text=EUR/USD">
								</div>
							</div>
							
							<div class="slide">
								<div class="title">
									EUR/USD
								</div>
								<div class="descr">
									H:1280.80 L:1266.30
								</div>
								<div class="img">
									<img src="https://api.fnkr.net/testimg/165x100/FFFFFF/438ed1/?text=EUR/USD">
								</div>
							</div>
							
							<div class="slide">
								<div class="title">
									EUR/USD
								</div>
								<div class="descr">
									H:1280.80 L:1266.30
								</div>
								<div class="img">
									<img src="https://api.fnkr.net/testimg/165x100/FFFFFF/438ed1/?text=EUR/USD">
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="popWrap">
					<div class="popInfo">
						<div class="h2">Option</div>
					</div>
					<div class="popBody">
						<div class="slider">
							<div class="slide">
								<div class="title">
									EUR/USD
								</div>
								<div class="descr">
									H:1280.80 L:1266.30
								</div>
								<div class="img">
									<img src="https://api.fnkr.net/testimg/165x100/FFFFFF/438ed1/?text=EUR/USD">
								</div>
							</div>

							<div class="slide">
								<div class="title">
									EUR/USD
								</div>
								<div class="descr">
									H:1280.80 L:1266.30
								</div>
								<div class="img">
									<img src="https://api.fnkr.net/testimg/165x100/FFFFFF/438ed1/?text=EUR/USD">
								</div>
							</div>

							<div class="slide">
								<div class="title">
									EUR/USD
								</div>
								<div class="descr">
									H:1280.80 L:1266.30
								</div>
								<div class="img">
									<img src="https://api.fnkr.net/testimg/165x100/FFFFFF/438ed1/?text=EUR/USD">
								</div>
							</div>

							<div class="slide">
								<div class="title">
									EUR/USD
								</div>
								<div class="descr">
									H:1280.80 L:1266.30
								</div>
								<div class="img">
									<img src="https://api.fnkr.net/testimg/165x100/FFFFFF/438ed1/?text=EUR/USD">
								</div>
							</div>

							<div class="slide">
								<div class="title">
									EUR/USD
								</div>
								<div class="descr">
									H:1280.80 L:1266.30
								</div>
								<div class="img">
									<img src="https://api.fnkr.net/testimg/165x100/FFFFFF/438ed1/?text=EUR/USD">
								</div>
							</div>

							<div class="slide">
								<div class="title">
									EUR/USD
								</div>
								<div class="descr">
									H:1280.80 L:1266.30
								</div>
								<div class="img">
									<img src="https://api.fnkr.net/testimg/165x100/FFFFFF/438ed1/?text=EUR/USD">
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
		<div class="clear"></div>
	</div>

</div>
<?php
$this->registerJs( <<< EOT_JS
    App.Initialize(Application.ApplicationTypes.SIMPLEWEB);
    App.SetSiteAddres('$chart->host');
    ThemeManager.initAdditionalThemeDefaults();
    var web = SimpleWeb.addPanel('chart', SimpleWeb.PanelsTypes.Chart);
    var web2 = SimpleWeb.addPanel('wl', SimpleWeb.PanelsTypes.WatchList);
    var ochl = {};
    SimpleWeb.Start(true);
    App.SimpleWeb.OnLoaded.Subscribe(function(){
        App.SimpleWeb.panelsHash[SimpleWeb.PanelsTypes.WatchList].OnInstrumentChange.Subscribe(function(ins){window.web.setInstrument(ins)});
        web2.setInstruments('EUR/USD:FX Route');
        web.setInstrument('EUR/USD:FX Route');
        web2.setVisibleColumns([SimpleWeb.WatchListColumns.Symbol, SimpleWeb.WatchListColumns.Last, SimpleWeb.WatchListColumns.Change])
        web.OnOCHLChange.Subscribe(function(data){
        	if(JSON.stringify(data) != JSON.stringify(ochl)){
        		ochl = data;
        		$('.pos').find('#pos-h').html(ochl.H);
				$('.pos').find('#pos-o').html(ochl.O);
				$('.pos').find('#pos-c').html(ochl.C);
				$('.pos').find('#pos-l').html(ochl.L);
				return;
        	}
        });
        web.OnSettingChange.Subscribe(function(){
        	console.log(JSON.parse(web.getSettings()));
        	$.post('/chart/save', {data:web.getSettings()}, function(data){
        		console.log(data);
        	});
        });
        $('.selects').removeClass('disabled');
    });
EOT_JS
    , \yii\web\View::POS_END);
?>