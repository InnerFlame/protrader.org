<?php $this->registerJsFile('/js/webx.js') ?>
<?php $this->registerCssFile('Theme/light/light.min.css') ?>
<div id="forChart" class="js-simple-web-block-style"></div>
<div id="forWL" class="js-simple-web-block-style"></div>
<?php
$this->registerJs("
    App.Initialize(Application.ApplicationTypes.SIMPLEWEB);
    App.SetSiteAddres(\"".$chart->host."\");
    ThemeManager.initAdditionalThemeDefaults();
    var web = SimpleWeb.addPanel('forChart', SimpleWeb.PanelsTypes.Chart);
    var web2 = SimpleWeb.addPanel('forWL', SimpleWeb.PanelsTypes.WatchList);
    SimpleWeb.Start(true);
    App.SimpleWeb.OnLoaded.Subscribe(function(){
         App.SimpleWeb.panelsHash[SimpleWeb.PanelsTypes.WatchList].OnInstrumentChange.Subscribe(function(ins){window.web.setInstrument(ins)});
         web2.setInstruments('EUR/USD:LMAX');
         web.setInstrument('EUR/USD:LMAX');
    });
", \yii\web\View::POS_END);
?>