<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 * Date: 24.03.2016
 * Time: 10:52
 */

namespace app\components\widgets\vote;


use yii\web\AssetBundle;

class VoteAsset extends AssetBundle
{

    public $sourcePath = '@app/components/widgets/vote/assets';

    public $js = [
        'js/vote.js'
    ];
}