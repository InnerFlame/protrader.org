var vote = (function(){

    var ACTION = {
        UP: '/vote/up',
        DOWN: '/vote/down',
    };


    function Vote(){
        var self = this;
        this.up = function(entity, id, selector){
            $.ajax({
                type:"POST",
                url: ACTION.UP,
                data:{entity:entity, entity_id:id},
                success:function(response){
                    $(self).trigger('increased', [response]);
                    $(self).trigger('update', [response]);
                    if(selector && response.count){
                        $(selector).html(response.count);
                    }
                }
            });
        }
        this.down = function(entity, id, selector){
            $.ajax({
                type:"POST",
                url: ACTION.DOWN,
                data:{entity:entity, entity_id:id},
                success:function(response){
                    $(self).trigger('reduced', [response]);
                    $(self).trigger('update', [response]);
                    if(selector && response.count){
                        $(selector).html(response.count);
                    }
                }
            });
        }
    }
    return new Vote();
}());