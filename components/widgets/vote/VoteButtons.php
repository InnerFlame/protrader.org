<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 * Date: 22.03.2016
 * Time: 12:55
 */

namespace app\components\widgets\vote;

class VoteButtons extends Vote
{
    /**
     * @var \app\modules\features\models\FeatureRequest
     */
    public $model;


    public function run()
    {
        return $this->render('buttons.twig', ['model' => $this->model]);
    }
}