<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 * Date: 24.03.2016
 * Time: 13:41
 */

namespace app\components\widgets\vote;


use yii\base\Widget;

class VotePanel extends Widget
{

    public $model;

    public function run()
    {
        return $this->render('panel.twig', ['model' => $this->model]);
    }
}