<?php

namespace app\components\widgets\vote\models;

use app\models\user\User;
use Yii;
use yii\web\BadRequestHttpException;

/**
 * This is the model class for table "com_votes".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $entity
 * @property integer $entity_id
 * @property integer $count
 * @property integer $count_fake
 * @property integer $created_at
 * @property integer $updated_at
 */
class Vote extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'com_votes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'entity', 'entity_id'], 'required'],
            [['user_id', 'entity', 'entity_id', 'count', 'created_at', 'updated_at'], 'integer'],
            [['created_at', 'updated_at'], 'default', 'value' => time()],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'entity' => 'Entity',
            'entity_id' => 'Entity ID',
            'count' => 'Count',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @inheritdoc
     * @return VoteQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VoteQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Find or create new Vote item and
     * increase counter
     * @param $entity
     * @param $entity_id
     * @param null $user_id
     * @return Vote|array|null
     * @throws BadRequestHttpException
     * @throws \yii\db\Exception
     */
    public static function upVote($entity, $entity_id, $user_id = null)
    {
        if(!$user_id){
            $user = Yii::$app->user->identity;
        }else{
            $user = User::findOne($user_id);
        }

        if($user->count_votes <= 0){
            throw new BadRequestHttpException;
        }
        $user->count_votes--;
        $transaction = \Yii::$app->getDb()->beginTransaction();

        $model = Vote::find()->where([
            'entity_id' => $entity_id,
            'entity' => $entity,
            'user_id' => $user->getId()
        ])->one();

        if($model && $user->save()){
            $model->updateCounters(['count' => 1]);
            $transaction->commit();
            return $model;
        }
        $model = new Vote();
        $model->entity = $entity;
        $model->entity_id = $entity_id;
        $model->user_id = $user->getId();
        $model->updated_at = time();
        $model->count = 1;
        if($model->save() && $user->save()){
            $transaction->commit();
            return $model;
        }
        $transaction->rollBack();
        throw new BadRequestHttpException;
    }

    /**
     * Down count vote entity
     * @param $entity
     * @param $entity_id
     * @param null $user_id
     * @return Vote|array|null
     * @throws BadRequestHttpException
     */
    public static function downVote($entity, $entity_id, $user_id = null)
    {
        if(!$user_id){
            $user = Yii::$app->user->identity;
        }else{
            $user = User::findOne($user_id);
        }

        $user->count_votes++;
        $transaction = \Yii::$app->getDb()->beginTransaction();


        $model = Vote::find()->where([
            'entity_id' => $entity_id,
            'entity' => $entity,
            'user_id' => $user->getId()
        ])->andWhere(['>', 'count', 0])->one();

        if(!$model)
            throw new BadRequestHttpException;

        $model->count--;
        $model->updated_at = time();
        if($model->save() && $user->save()){
            $transaction->commit();
            return $model;
        }
        $transaction->rollBack();
        throw new BadRequestHttpException;
    }

    /**
     * Find or create new Vote item and
     * increase counter and fake counter
     * @param $entity
     * @param $entity_id
     * @param int $count count votes
     * @param null $user_id
     * @return Vote|array|null
     */
    public static function upFake($entity, $entity_id, $count = 1, $user_id = null) {
        if (!$user_id) {
            $user_id = Yii::$app->user->identity->id;
        }

        $model = Vote::find()->where([
            'entity_id' => $entity_id,
            'entity' => $entity,
            'user_id' => $user_id
        ])->one();

        if ($model) {
            $model->updateCounters(['count_fake' => $count]);
            $model->updateCounters(['count' => $count]);
            return $model;
        }
        $model = new Vote();
        $model->entity = $entity;
        $model->entity_id = $entity_id;
        $model->user_id = $user_id;
        $model->updated_at = time();
        $model->count = $count;
        $model->count_fake = $count;
        if($model->save()){
            return $model;
        }

    }

    /**
     * Down count vote and fake count vote entity
     * @param $entity
     * @param $entity_id
     * @param int $count
     * @param null $user_id
     * @return Vote|array|null
     * @throws BadRequestHttpException
     */
    public static function downFake($entity, $entity_id, $count = 1, $user_id = null)
    {
        if (!$user_id) {
            $user_id = Yii::$app->user->identity->id;
        }

        $model = Vote::find()->where([
            'entity_id' => $entity_id,
            'entity' => $entity,
            'user_id' => $user_id
        ])->andWhere(['>', 'count_fake', 0])->andWhere(['<=', 'count_fake', $count])
            ->andWhere(['>', 'count', 0])->andWhere(['<=', 'count', $count])->one();

        if(!$model)
            throw new BadRequestHttpException;

        $model->count = $model->count - $count;
        $model->count_fake = $model->count_fake - $count;
        //Если голосов не осталось, то удалить запись вообще
        if($model->count <= 0 && $model->delete()){
            return $model;
        }
        if($model->save()){
            return $model;
        }
    }

    /**
     * Reset all fake votes for entity
     * @param $entity
     * @param $entity_id
     * @return bool
     */
    public static function downAllFake($entity, $entity_id)
    {
        $models = Vote::find()->where([
            'entity_id' => $entity_id,
            'entity' => $entity,
        ])->all();

        $transaction = Yii::$app->db->beginTransaction();
        foreach($models as $model){
            $model->count = $model->count - $model->count_fake;
            $model->count_fake = 0;
            //Если голосов не осталось, то удалить запись вообще
            if($model->count <= 0 && $model->delete()){
                continue;
            }
            if(!$model->save()){
                $transaction->rollBack();
                return false;
            }
        }
        $transaction->commit();
        return true;
    }

    /**
     * Caclculate all count votes by
     * Entity Type and Entity Id
     * @param $entity
     * @param $id
     * @return int
     */
    public static function getAllCount($entity, $id){
        $models = Vote::find()->where(['entity' => $entity, 'entity_id' => $id])->all();
        $count = 0;

        foreach($models as $vote){
            $count += $vote->count;
        }
        return $count;
    }
}
