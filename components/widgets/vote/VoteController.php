<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 * Date: 22.03.2016
 * Time: 12:58
 */

namespace app\components\widgets\vote;


use app\components\widgets\vote\models\Vote;
use app\modules\features\models\FeatureRequest;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;

class VoteController extends Controller
{

    public function actionUp()
    {
        try{
            $vote = Vote::upVote(\Yii::$app->request->post('entity'), \Yii::$app->request->post('entity_id'));
            \Yii::$app->response->format = Response::FORMAT_JSON;
            $feature = FeatureRequest::findOne(\Yii::$app->request->post('entity_id'));
            $panel = $this->renderAjax('panel.twig', ['model' => $feature]);
            return ['vote' => $vote, 'user' => $vote->user, 'count' => Vote::getAllCount(\Yii::$app->request->post('entity'), \Yii::$app->request->post('entity_id')), 'panel' => $panel];
        }catch (\Exception $e){
            throw new BadRequestHttpException;
        }
    }

    public function actionDown()
    {
        try {
            $vote = Vote::downVote(\Yii::$app->request->post('entity'), \Yii::$app->request->post('entity_id'));
            \Yii::$app->response->format = Response::FORMAT_JSON;
            $feature = FeatureRequest::findOne(\Yii::$app->request->post('entity_id'));
            $panel = $this->renderAjax('panel.twig', ['model' => $feature]);
            return ['vote' => $vote, 'user' => $vote->user, 'count' => Vote::getAllCount(\Yii::$app->request->post('entity'), \Yii::$app->request->post('entity_id')), 'panel' => $panel];
        } catch (\Exception $e) {
            throw new BadRequestHttpException;
        }
    }

    public function actionUpdate()
    {
        $model = FeatureRequest::findOne(\Yii::$app->request->post('id'));
        return $this->renderAjax('panel.twig', ['model' => $model]);
    }

    public function getViewPath()
    {
        return dirname(__FILE__).DIRECTORY_SEPARATOR.'views';
    }
}