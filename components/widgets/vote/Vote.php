<?php
/**
 * Подключает скрипт JS голосвалки
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2016
 */

namespace app\components\widgets\vote;


use yii\base\Widget;

class Vote extends Widget
{
    public function init()
    {
        parent::init();
        VoteAsset::register($this->getView());
    }
}