<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2016
 */

namespace app\components\widgets\eauth;


class Widget extends \nodge\eauth\Widget
{
    public $serviceToCssClassMap = [
        'facebook' => 'facebook',
        'linkedin_oauth2' => 'linkedin'
    ];

    public $serviceToNameMap = [
        'facebook' => 'Facebook',
        'linkedin_oauth2' => 'Linkedin'
    ];

    public function getCssClassByService($service)
    {
        if(isset($this->serviceToCssClassMap[$service])){
            return $this->serviceToCssClassMap[$service];
        }
        return $service;
    }

    public function getNameByService($service)
    {
        if(isset($this->serviceToNameMap[$service])){
            return $this->serviceToNameMap[$service];
        }
        return $service;
    }

    public function run()
    {
        return $this->render('widget', [
            'id' => $this->getId(),
            'services' => $this->services,
            'action' => $this->action,
            'popup' => $this->popup,
            'assetBundle' => $this->assetBundle,
            'self' => $this
        ]);
    }
}