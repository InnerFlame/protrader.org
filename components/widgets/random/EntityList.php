<?php

/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2016
 */

namespace app\components\widgets\random;

use app\components\Entity;
use yii\base\Widget;

class EntityList extends Widget
{

    /**
     * @var string заглавие сайдбара
     */
    public $title;

    /**
     * @var int количество случайных значений
     */
    public $count = 5;

    /**
     * @var string название класса ActiveRecord
     */
    public $className;

    /**
     * @var int статус реквестов, которыебудут попадать под запрос если задать false или null будет искать по всем
     */
    public $status;

    /**
     * @var Entity исключить из выборки данный элемент
     */
    public $entity;

    /**
     * 
     * @var string аттрибут который, значение которого необходимо исключить. Значение атрибуты берется из $this->entity
     */
    public $exceptAttribute = null;

    /**
     * @var bool|string особый шаблон для вывода элементов
     */
    public $layout = false;

    public function run()
    {
        $class = $this->className;
        try {
            $query = $class::find()->where(['deleted' => Entity::NOT_DELETED]);
        } catch (\Exception $e) {
            \Yii::error(\Yii::t('error', 'Error create ActiveQuery with class :class. Reason: :reason', [':class' => $class, ':reason' => $e->getMessage()]));
            return null;
        }

        if ($this->entity instanceof Entity) {
            $query->andWhere('id <> :id', [':id' => $this->entity->id]);
            if ($this->exceptAttribute && ( $this->entity->hasAttribute($this->exceptAttribute) || property_exists($this->entity->className(), $this->exceptAttribute) )) {
                $attr = $this->exceptAttribute;
                $query->andWhere("$attr <> :attr", [':attr' => $this->entity->$attr]);
            }
        }
        if ($this->status) {
            $query->andWhere(['status' => $this->status]);
        }

        $requests = $query->orderBy('RAND()')->limit($this->count)->all();

        if (count($requests) == 0) {
            return null;
        }
        $layout = $this->layout ? $this->layout : 'entity-list-new.twig';
        return $this->render($layout, ['models' => $requests, 'self' => $this]);
    }

}
