<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2016
 */

namespace app\components\widgets\like;


use app\components\Entity;
use yii\base\Widget;

/**
 * Class Button
 * Генерирует кнопку для установки лайков,
 * подключает необходимые скрипты
 * @package app\components\widgets\like
 */
class Button extends Widget
{
    const ENABLED = 1;

    const DISABLED = 0;
    /**
     * @var Entity
     */
    public $entity;

    /**
     * @var int состояние кнопки, включена выключена
     */
    public $state = self::ENABLED;

    /**
     * @var int количество лайков
     */
    public $count = 0;

    public function run()
    {
        if($this->state == self::ENABLED){
            Asset::register($this->getView());
            return sprintf('<div id="btn-vote-%d" class="btn-vote" type="button" data-safe="true" data-id="%d" data-class-name="%s" data-selector-counter="#btn-vote-%d" onclick="like(this)">%d</div>',
                $this->entity->id, $this->entity->id, $this->entity->getTypeEntity(), $this->entity->id, $this->count);
        }
        return sprintf('<div class="btn-vote disabled" data-id="%d">%d</div>',
            $this->entity->id, $this->count);
    }
}