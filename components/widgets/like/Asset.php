<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2016
 */

namespace app\components\widgets\like;


use yii\web\AssetBundle;

class Asset extends AssetBundle
{
    public $sourcePath = '@app/components/widgets/like/assets';

    public $js = [
        'js/like.js'
    ];
}