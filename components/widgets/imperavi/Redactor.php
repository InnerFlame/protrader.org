<?php

namespace app\components\widgets\imperavi;

use app\components\Entity;
use app\components\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;

class Redactor extends \yii\base\Widget
{
    const TYPE_BASIC = 1;

    const TYPE_BACKEND = 2;

    const TYPE_FRONTEND = 3;

    /**
     * @var Entity
     */
    public $model;

    /**
     * @var ActiveForm
     */
    public $form;

    /**
     * @var string имя поля
     */
    public $field;

    /**
     * Редактор может быть базовым, для бекенда
     * или фронтенда
     * @var int тип редактора
     */
    public $type;

    /**
     * @var string selector элемента, для которого будет создан редактор
     */
    public $input_id;


    /**
     * @var string ID атрибут для поля ввода
     */
    public $option_id;

    /**
     * @var array пользовательский набор кнопок
     */
    public $buttons = [];

    /**
     * @var array пользовательский набор плагинов
     */
    public $plugins = [];

    /**
     * @var array пользовательский набор настроек
     */
    public $settings = [];

    /**
     * @var string | null url куда передавать загружаемую картинку
     */
    public $imageUpload;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('index.twig', [
            'this' => $this,
        ]);
    }

    /**
     * @return array
     */
    public function getSettings()
    {
        $settings =  [
            'lang'                => 'en',
            'dragFileUpload'      => false,
            'dragImageUpload'     => false,
            'replaceDivs'         => false,
            'minHeight'           => 250,
            'buttons'             => $this->_getButtons(),
            'plugins'             => $this->_getPlugins(),
        ];

        if(!empty($this->settings)){
            foreach($this->settings as $key => $value){
                $settings[$key] = $value;
            }
        }

        //Только если imageUpload задан
        if($this->imageUpload){
            if ($this->imageUpload) {
                $settings['buttons'] = ArrayHelper::mergeUnique($settings['buttons'], ['image']);
            }
            $settings['imageUpload'] = $this->imageUpload;
            $settings['imageUploadCallback'] = new \yii\web\JsExpression('function(e, response){$(e).wrap(\'<a href="\'+response.original+\'" data-popup="lightbox">\')}');
        }


        return $settings;
    }

    /**
     * Формирует набор кнопок исходя из типа редактора
     * а так же набора кнопок, который передал пользователь
     * @return array
     */
    private function _getButtons()
    {
        //Базовый формируется в любых ситуациях
        $buttons = $this->_getButtonsOfType(self::TYPE_BASIC);

        //Если задан url для картинки
        if($this->imageUpload){
            $buttons[] = 'image';
        }

        //Базовый + что передано руками
        if($this->type == self::TYPE_BASIC && !empty($this->buttons)){
            return ArrayHelper::mergeUnique($buttons, $this->buttons);
        }
        //руками ничего не передавали, базовый + набор по типу
        if(empty($this->buttons)){
            return ArrayHelper::mergeUnique($buttons, $this->_getButtonsOfType($this->type));
        }

        //Базовый набор + набор по типу
        $buttons = ArrayHelper::mergeUnique($buttons, $this->_getButtonsOfType($this->type));

        //Базовый + набор по типу + то, что передано руками
        return ArrayHelper::mergeUnique($buttons, $this->buttons);
    }

    /**
     * Формирует уникальный набор плагинов исходя из типа
     * редактора, а так же набора плагинов, которые передал
     * пользователь
     * @return array
     */
    private function _getPlugins()
    {
        //Базовый формируется в любых ситуациях
        $plugins = $this->_getPluginsOfType(self::TYPE_BASIC);

        //Базовый + что передано руками
        if($this->type == self::TYPE_BASIC && !empty($this->plugins)){
            return ArrayHelper::mergeUnique($plugins, $this->plugins);
        }
        //руками ничего не передавали, базовый + набор по типу
        if(empty($this->plugins)){
            return ArrayHelper::mergeUnique($plugins, $this->_getPluginsOfType($this->type));
        }

        //Базовый набор + набор по типу
        $plugins = ArrayHelper::mergeUnique($plugins, $this->_getPluginsOfType($this->type));

        //Базовый + набор по типу + то, что передано руками
        return ArrayHelper::mergeUnique($plugins, $this->plugins);
    }


    /**
     * Возвращает набор кнопок для для переданного типа
     * @param $type
     * @return array
     */
    private function _getButtonsOfType($type)
    {
        $buttons = [];

        switch($type){
            case self::TYPE_BASIC:
                $buttons[] = 'bold';
                $buttons[] = 'italic';
                $buttons[] = 'link';
                $buttons[] = 'formatting';
                break;
            case self::TYPE_BACKEND:
                $buttons[] = 'html';
                $buttons[] = 'deleted';
                $buttons[] = 'unorderedlist';
                $buttons[] = 'orderedlist';
                $buttons[] = 'outdent';
                $buttons[] = 'indent';
                $buttons[] = 'file';
                $buttons[] = 'alignment';
                $buttons[] = 'horizontalrule';
                break;
        }

        return $buttons;
    }
    /**
     * Генерирует набор плагинов исходя из типа
     * @return array
     */
    private function _getPluginsOfType($type)
    {
        $plugins = [];
        switch($type){
            case self::TYPE_BACKEND:
                $plugins[] = 'clips';
                $plugins[] = 'fullscreen';
                break;
        }

        return $plugins;
    }
}