<?php
/**
 * Created by PhpStorm.
 * @author: Roman Budnitsky
 * @date: 24.10.14 13:57
 * @copyright Copyright (c) 2014 PFSOFT LLC
 *
 * Class FlatActiveField
 * @package app\components
 */

namespace app\components;

use yii\bootstrap\ActiveField;
use yii\helpers\Html;

class FlatActiveField extends ActiveField{

    public $icon = false;

    public function init()
    {
        if($this->icon) {
            $this->inputTemplate = "<div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-".$this->icon."\"></i></span>{input}</div>";
        }
        if($this->inputOptions['placeholder']) {
            $this->inputOptions['placeholder'] = $this->model->getAttributeLabel($this->attribute);
        }

        parent::init();
    }

} 