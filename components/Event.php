<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 * Date: 08.04.2016
 * Time: 11:17
 */

namespace app\components;


use app\models\user\User;

abstract class Event extends \yii\base\Event
{
    public $type;

    public $entity_id;

    public $entity;

    public $user_sender_id;


    abstract public function getTitle();

    abstract public function getMessage();

    /**
     * @return \app\models\user\User
     */
    public function getUserSender(){
        return User::findOne($this->user_sender_id);
    }

    public function getType()
    {
        return $this->type;
    }

}