<?php
/**
 * Created by PhpStorm.
 * @author: Roman Budnitsky
 * @date: 24.10.14 16:30
 * @copyright Copyright (c) 2014 PFSOFT LLC
 *
 * Class FlatErrorAction
 * @package app\components
 */

namespace app\components;

use Yii;
use yii\base\Action;
use yii\base\Exception;
use yii\base\UserException;
use yii\web\HttpException;

class FlatErrorAction  extends Action {

    public $view;
    public $defaultName;
    public $defaultMessage;

    public function run()
    {
        if (($exception = Yii::$app->getErrorHandler()->exception) === null) {
            return '';
        }

        if ($exception instanceof HttpException) {
            $code = $exception->statusCode;
        } else {
            $code = $exception->getCode();
        }

        if ($exception instanceof Exception) {
            $name = $exception->getName();
        } else {
            $name = $this->defaultName ?: Yii::t('yii', 'Error');
        }

        if ($exception instanceof UserException) {
            $message = $exception->getMessage();
        } else {
            $message = $this->defaultMessage ?: Yii::t('yii', 'An internal server error occurred.');
        }

//        if($code) {
//            switch($code) {
//                case 404:
//                    Yii::$app->session->addFlash('Sorry, but this page doesn\'t exists!\'');
//                    Yii::$app->controller->redirect(Yii::$app->request->referrer);
//                    $message = 'Sorry, but this page doesn\'t exists!';
//                    break;
//            }
//        }

        if (Yii::$app->getRequest()->getIsAjax()) {
            return "$name: $message";
        } else {
            return $this->controller->render($this->view ?: $this->id, [
                'code' => $code,
                'title' => $name,
                'message' => $message,
                'exception' => $exception,
            ]);
        }
    }
}