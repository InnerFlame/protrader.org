<?php
/**
 * Управляет кастомными настройками пользователя
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2016
 */

namespace app\components;


use app\models\user\UserCustomField;
use app\models\user\UserCustomValue;
use yii\base\Component;

class UserSettings extends Component
{

    public $user_id;


    /**
     * Создание нового кастомного параметра для настроек пользователя
     * @param $name string имя параметра
     * @param null $alias string
     * @return bool
     */
    public function create($name, $alias = null)
    {
        $field = new UserCustomField();
        $field->name = $name;
        $field->alias = $alias ? $alias : $name;
        return $field->save();
    }

    /**
     * Удаление кастомного параметра и значений у всех пользователей для него
     * @param $alias
     * @return bool|false|int
     * @throws \Exception
     */
    public function remove($alias)
    {
        $field = UserCustomField::find()->where(['alias' => $alias])->one();
        if($field){
            UserCustomValue::deleteAll('field_id = :field_id', [':field_id' => $field->id]);
            return $field->delete();
        }
        return false;
    }

    public function set($param, $value)
    {
        if($field = $this->getValueOfField($param)){
            $field->value = $value;
            return $field->save();
        }
        return false;
    }

    public function get($param)
    {
        $field = $this->getValueOfField($param);
        return $field ? $field->value : null;
    }

    /**
     * Вовзвращает модель кастомного поля по названию и текущему пользователю
     * @param $alias
     * @return array|bool|null|UserCustomValue
     */
    protected function getValueOfField($alias)
    {
        $field = UserCustomField::find()->where('alias = :alias',[':alias' => $alias])->one();
        if(!$field)
            return false;

        $value = UserCustomValue::find()->where('user_id = :user_id AND field_id = :field_id',[':user_id' => $this->user_id, ':field_id' => $field->id])->one();
        if(!$value){
            $value = new UserCustomValue();
            $value->user_id = $this->user_id;
            $value->field_id = $field->id;
        }
        return $value;
    }
}