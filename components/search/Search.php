<?php
namespace app\components\search;

use Yii;
use yii\base\Component;

class Search extends Component
{
    public $model = null;

    public $set = [
        '\app\modules\articles\models\Articles' => 'articles',
        '\app\models\main\Faq'          => 'faq',
        '\app\models\video\Video'       => 'video-lessons',
        // '\app\modules\brokers\models\Improvement' => 'features',
        '\app\modules\brokers\models\ImprovementsCategory' => 'features',
        '\app\models\news\News'         => 'news',
        '\app\modules\forum\models\FrmTopic'    => 'forum',
        '\app\modules\brokers\models\Broker' => 'brokers',
    ];

    public function __construct()
    {
        $part = explode("/", Yii::$app->request->url);
        $this->model = array_keys($this->set, $part[1])[0];
    }

    public function getCollection($param, $val)
    {
        $model = $this->model;
        $val = $val ? $val : 'A B C D E F G H I K L M N O P Q R S T V X Y Z';

        if(is_string($param)){
            return $model::find()->where($param . ' LIKE \'%' . $val . '%\'');
        }

        if(is_array($param) && count($param) > 0){

            $query = [];
            foreach($param as $attr){
                $query[] = $attr . ' LIKE \'%' . $val . '%\'';
            }

            return $model::find()->where(implode(' or ', $query));
        }
    }
}