<?php
/**
 * Created by PhpStorm.
 * @author: Roman Budnitsky
 * @date: 18.02.15 16:49
 * @copyright Copyright (c) 2015 PFSOFT LLC
 *
 * Class DepCheckBoxList
 * @package 
 */


namespace app\components;

use yii\base\Widget;
use yii\helpers\Html;

class DependentCheckBoxList extends Widget {

    public $name;
    public $items = [];
    public $options = [];
    public $labelOptions = [];
    public $closureFunctions = [];

    public function run()
    {
        echo Html::beginTag('div', array_merge($this->options, ['id' => $this->id]));
        echo $this->renderNods($this->items, null);
        echo Html::endTag('div');
    }

    public function renderNods($items, $name = null, $level = 0, &$n = 0)
    {
        if($name === null) $name = $this->name;
        $i = 0;
        $out = '';
        foreach($items as $title => $nextItems) {
            $n++;
            if(is_array($nextItems)) {
                if(isset($this->closureFunctions[$level]) && $this->closureFunctions[$level] instanceof \Closure) {
                    $func = $this->closureFunctions[$level];
                    $out .= $func($this, $name."[$i]", $title, $nextItems, $level, $n);
                } else {
                    $out .= Html::checkbox($name."[$i]", false, ['id' => $this->name."_$n"]);
                    $out .= Html::label($title, $this->name."_$n", $this->labelOptions);
                }
                $out .= Html::tag('div', $this->renderNods($nextItems, $name."[$i]", $level + 1, $n), [
                    'style' => 'padding-left: 20px;'
                ]);
            }
            $i++;
        }
        return $out;
    }


}