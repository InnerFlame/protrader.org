<?php
/**
 * Created by PhpStorm.
 * User: writer
 * Date: 26.01.2016
 * Time: 12:52
 */

namespace app\components\highlight;


use yii\web\AssetBundle;

class AssetsHighlight extends AssetBundle
{
    public $sourcePath = '@app/components/highlight/media';

    public $js = [
        'js/highlight.min.js'
    ];

    public $css = [
        'css/github.css'
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}