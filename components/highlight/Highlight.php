<?php
namespace app\components\highlight;

use yii\bootstrap\Widget;

/**
 * Created by PhpStorm.
 * Syntax highlighting plugin, fo the articles
 * <a href="https://highlightjs.org">highlight</a>
 * User: B.Pavel
 * Date: 26.01.2016
 * Time: 12:26
 */
class Highlight extends Widget
{

    public function run()
    {

        $this->registerPlugin();
    }

    protected function registerPlugin($name = null)
    {
        $view = $this->getView();

        AssetsHighlight::register($view);

        $js = " $('.panel-body pre').each(function(i, block) {
                    hljs.highlightBlock(block);
                });";
        $view->registerJs($js);
    }
}