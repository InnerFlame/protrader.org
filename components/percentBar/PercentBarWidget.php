<?php
namespace app\components\percentBar;

use Yii;
use yii\base\Widget;

class PercentBarWidget extends Widget
{
    public $amount;
    public $type;
    public $template;
    public $model;

    public function init()
    {
        parent::init();

        $this->setType();

        $this->setAmount();
    }

    public function run()
    {
        return Yii::$app->getView()->render('@app/components/percentBar/views/' . $this->template . '.twig',[
            'model' => $this->model,
            'type' => $this->type,
            'amount' => $this->amount,
        ]);
    }

    public function setAmount()
    {
        if(is_null($this->amount))
            $this->amount = 1;
        $this->amount .= '%';
    }

    public function setType()
    {
        if($this->amount > 66) {
            $this->type = 'progress-bar-success';
        }elseif($this->amount > 33){
            $this->type = 'progress-bar-info';
        }else{
            $this->type = 'progress-bar-warning';
        }
    }
}