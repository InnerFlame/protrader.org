<?php
 namespace app\components\rbac;
/**
 * Created by PhpStorm.
 * @author: Roman Budnitsky
 * @date: 06.11.14 16:20
 * @copyright Copyright (c) 2014 PFSOFT LLC
 *
 * Class RbacManager
 */



use app\models\user\User;
use app\components\ModulesLoader;
use yii;
use yii\rbac\Item;
use yii\rbac\Permission;
use yii\rbac\Role;
use yii\rbac\Assignment;
use yii\rbac\PhpManager;
use yii\base\InvalidParamException;
use yii\base\InvalidCallException;

class RbacManager extends PhpManager {

    const TYPE_COMPANY_OWN = 'own';
    const TYPE_COMPANY_CLIENT = 'client';
    const TYPE_COMPANY_PROVIDER = 'provider';

    public $tmpAssigments = [
        1 => 'root',
        3 => 'user',
        14 => 'user',
        291 => 'user',
        442 => 'admin'
    ];
    public $itemFile = '@app/components/rbac/items.php';
    public $ruleFile = '@app/components/rbac/rules.php';

    /**
     * @inheritdoc
     */
    public function getAssignments($userId, $reload = false)
    {
        if($reload && isset($this->assignments[$userId])) unset($this->assignments[$userId]);
        if(!isset($this->assignments[$userId])) {
            /** @var User $user */
            if(!Yii::$app->user->isGuest && Yii::$app->user->getId() == $userId) {
                $user = Yii::$app->user->identity;
            } else {
                $user = User::findOne($userId);
            }

            if($user === null) return [];

            $this->assignments[$user->id][$user->getRole($user->id)] = new Assignment([// added igor 11:43  15 december $user->id to get role
                'userId' => $user->id,
                'roleName' => $user->role,
                'createdAt' => time(),
            ]);
        }
        return isset($this->assignments[$userId]) ? $this->assignments[$userId] : [];
    }

    /**
     * Loads authorization data from persistent storage.
     */
    protected function load()
    {
        $this->children = [];
        $this->rules = [];
        $this->assignments = [];
        $this->items = [];

        $items = $this->loadFromFile($this->itemFile);
        $itemsMtime = @filemtime($this->itemFile);
        $rules = $this->loadFromFile($this->ruleFile);

        if(method_exists(Yii::$app, 'getModules')) {
            $modules = Yii::$app->modules;
            if(count($modules) > 0)
                foreach($modules as $id => $_module) {
                    if(is_array($_module)) {
                        if (isset($_module['itemFile'])) {
                            $items = yii\helpers\ArrayHelper::merge($items, $this->loadFromFile(Yii::getAlias($_module['itemFile'])));
                        }
                        if (isset($_module['ruleFile'])) {
                            $rules = yii\helpers\ArrayHelper::merge($rules, $this->loadFromFile(Yii::getAlias($_module['ruleFile'])));
                        }
                    }
                    if(is_object($_module)) {
                        if (isset($_module->itemFile)) {
                            $items = yii\helpers\ArrayHelper::merge($items, $this->loadFromFile(Yii::getAlias($_module->itemFile)));
                        }
                        if (isset($_module->ruleFile)) {
                            $rules = yii\helpers\ArrayHelper::merge($rules, $this->loadFromFile(Yii::getAlias($_module->ruleFile)));
                        }
                    }
                }
        }


        foreach ($items as $name => $item) {
            $class = $item['type'] == Item::TYPE_PERMISSION ? Permission::className() : Role::className();

            $this->items[$name] = new $class([
                'name' => $name,
                'description' => isset($item['description']) ? $item['description'] : null,
                'ruleName' => isset($item['ruleName']) ? $item['ruleName'] : null,
                'data' => isset($item['data']) ? $item['data'] : null,
                'createdAt' => $itemsMtime,
                'updatedAt' => $itemsMtime,
            ]);
        }

        foreach ($items as $name => $item) {
            if (isset($item['children'])) {
                foreach ($item['children'] as $childName) {
                    if (isset($this->items[$childName])) {
                        $this->children[$name][$childName] = $this->items[$childName];
                    }
                }
            }
        }

        foreach ($rules as $name => $ruleData) {
            $this->rules[$name] = unserialize($ruleData);
        }

    }

    public function modifyItemsFromGroup($group_id, $type = 'own') {
        $arr = $this->loadFromFile(Yii::getAlias('@app/components/rbac/groups/group_' . $group_id . '.php'));
        if(isset($arr[$type])) {
            foreach($arr[$type] as $mod) {
                if($mod[2] === false) {
                    $this->unsetChild($mod[0], $mod[1]);
                } else {
                    $this->addChild($this->getPermission($mod[0]), $this->getPermission($mod[1]), false);
                }
            }
        }
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function assign($role, $userId)
    {
        /*if (!isset($this->items[$role->name])) {
            throw new InvalidParamException("Unknown role '{$role->name}'.");
        } elseif (isset($this->getAssignments($userId)[$role->name])) {
            throw new InvalidParamException("Authorization item '{$role->name}' has already been assigned to user '$userId'.");
        } else {
            if((new UserPermission(['id' => $userId, 'name' => $role->name]))->save()) {
                return $this->getAssignments($userId, true);
            }
        }
        return false;*/
        return true;
    }

    /**
     * @return bool|void
     */
    protected function saveAssignments()
    {
        return true;
    }

    /**
     * @param Item $parent
     * @param Item $child
     * @return bool
     */
    /*protected function detectLoop($parent, $child)
    {
        if ($child->name === $parent->name) {
            return true;
        }
        return false;
    }*/


    public function checkAccess($userId, $permissionName, $params = [])
    {
        if($permissionName === true) return true;
        if($permissionName === false) return false;

        if(is_object(Yii::$app->user->identity))
        if(Yii::$app->user->identity->isAdmin()) return true;

        $user = (!Yii::$app->user->isGuest && Yii::$app->user->getId() == $userId) ?
            Yii::$app->user->identity : User::findOne($userId);

        $assignments = []; //$this->getDBAssignments($user);
        // $rules_list = Rule::getModelRuleList();

        //return $this->checkDBAccessRecursive($permissionName, $user, $assignments, $rules_list, $params);
        return $this->checkAccessRecursive($user, $permissionName, $params, $assignments);

        if(isset($user->role))
            if($user->role == 1 || $user->role == 0) return true;

//        $params['queryPermissionName'] = $permissionName;
//        if($user === null) return false;
//        return $this->checkAccessRecursive($user, $permissionName, $params, $assignments);
    }

    /**
     * @param int|string $user
     * @param string $itemName
     * @param array $params
     * @param yii\rbac\Assignment[] $assignments
     * @return bool
     * @throws yii\base\InvalidConfigException
     */
    protected function checkAccessRecursive($user, $itemName, $params, $assignments)
    {
        if (!isset($this->items[$itemName])) {
            return false;
        }

        /* @var $item Item */
        $item = $this->items[$itemName];
        //Yii::trace($item instanceof Role ? "Checking role: $itemName" : "Checking permission : $itemName", __METHOD__);

        if (!$this->executeRule($user, $item, $params)) {
            return false;
        }

        if (isset($assignments[$itemName]) || in_array($itemName, $this->defaultRoles)) {
            return true;
        }


        foreach ($this->children as $parentName => $children) {

            if (isset($children[$itemName]) && $this->checkAccessRecursive($user, $parentName, $params, $assignments)) {
                return true;
            }
        }

        return false;
    }

    public function unsetChild($parent, $child)
    {
        if (isset($this->children[$parent][$child])) {
            unset($this->children[$parent][$child]);
            return true;
        } else {
            return false;
        }
    }

    public function addChild($parent, $child, $save = true) {
        if (!isset($this->items[$parent->name], $this->items[$child->name])) {
            throw new InvalidParamException("Either '{$parent->name}' or '{$child->name}' does not exist.");
        }

        if ($parent->name == $child->name) {
            throw new InvalidParamException("Cannot add '{$parent->name} ' as a child of itself.");
        }
        if ($parent instanceof Permission && $child instanceof Role) {
            throw new InvalidParamException("Cannot add a role as a child of a permission.");
        }

        if ($this->detectLoop($parent, $child)) {
            throw new InvalidCallException("Cannot add '{$child->name}' as a child of '{$parent->name}'. A loop has been detected.");
        }
        if (isset($this->children[$parent->name][$child->name])) {
            throw new InvalidCallException("The item '{$parent->name}' already has a child '{$child->name}'.");
        }
        $this->children[$parent->name][$child->name] = $this->items[$child->name];
        if($save) $this->saveItems();
        return true;
    }
} 