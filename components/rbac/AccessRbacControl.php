<?php
/**
 * Created by PhpStorm.
 * @author: Roman Budnitsky
 * @date: 05.11.14 11:07
 * @copyright Copyright (c) 2014 PFSOFT LLC
 *
 * Class AccessRbacControl
 * @package app\components\rbac
 */


namespace app\components\rbac;

use yii;
use yii\di\Instance;
use yii\base\Action;
use yii\web\User;
use yii\web\Request;
use yii\filters\AccessControl;
use yii\filters\AccessRule;
use app\components\userlog\Log;

class AccessRbacControl extends AccessControl {

    /**
     * Initializes the [[rules]] array by instantiating rule objects from configurations.
     */
    public function init()
    {
        $this->user = Instance::ensure($this->user, User::className());
        foreach ($this->rules as $i => $rule) {
            if (is_array($rule)) {
                $tmp_rule = new AccessRbacRule();
                $tmp_rule->allow = true;
                $tmp_rule->actions = is_array($rule[0]) ? $rule[0] : [$rule[0]];
                if(isset($rule[1])) {
                    $tmp_rule->roles = is_array($rule[1]) ? $rule[1] : [$rule[1]];
                    if(isset($rule[2])) $tmp_rule->rulesParams = $rule[2];
                } elseif($i != 0) {
                    $tmp_rule->roles = ['@'];
                } else {
                    $tmp_rule->roles = ['?'];
                }
                $this->rules[$i] = $tmp_rule;
            }
        }

        if(!Yii::$app->user->isGuest) {
            if(!isset($_SESSION['user_session'])) {
              //  $_SESSION['user_session'] = true;
              //  Log::setLog('access', 'init', true);
            }
        }

    }

}

class AccessRbacRule extends AccessRule {

    public $rulesParams = [];

    /**
     * Checks whether the Web user is allowed to perform the specified action.
     * @param Action $action the action to be performed
     * @param User $user the user object
     * @param Request $request
     * @return boolean|null true if the user is allowed, false if the user is denied, null if the rule does not apply to the user
     */
    public function allows($action, $user, $request)
    {
        if ($this->matchAction($action)
            && $this->matchRole($user, $action, $request)
            && $this->matchIP($request->getUserIP())
            && $this->matchVerb($request->getMethod())
            && $this->matchController($action->controller)
            && $this->matchCustom($action)
        ) {
            return $this->allow ? true : false;
        } else {
            return null;
        }
    }

    /**
     * @param User $user the user object
     * @param Action $action the action to be performed
     * @param Request $request
     * @return boolean whether the rule applies to the role
     */
    protected function matchRole($user, $action, $request)
    {
        if (empty($this->roles)) {
            return true;
        }
        foreach ($this->roles as $role) {
            if ($role === '?') {
                if ($user->getIsGuest()) {
                    return true;
                }
            } elseif ($role === '@') {
                if (!$user->getIsGuest()) {
                    return true;
                }
            } else {
                foreach($this->rulesParams as $key => $value) {
                    $method = null;
                    $params = $request->getQueryParams();
                    if(is_array($value) && isset($value[0]) && is_string($value[0])) {
                        $method = $value[0];
                        if(isset($value[1]) && is_array($value[1])) {
                            $params = yii\helpers\ArrayHelper::merge($params, $value[1]);
                        }
                    } elseif(is_string($value)) {
                        $method = $value;
                    }
                    if($method !== null && $action->controller->hasMethod($method))
                        $this->rulesParams[$key] = call_user_func_asoc([$action->controller, $method], $params);
                }
                if ($user->can($role, $this->rulesParams)) {
                    return true;
                }
            }
        }
        return false;
    }

}