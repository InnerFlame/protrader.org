<?php
return [
    'root' => [
        'type' => 1,
        'children' => [
            'admin',
        ],
    ],
    'admin' => [
        'type' => 1,
        'children' => [
            'lead',
        ],
    ],
    'lead' => [
        'type' => 1,
        'children' => [
            'user',
        ],
    ],
    'user' => [
        'type' => 1,
        'children' => [
        ],
    ],
];
