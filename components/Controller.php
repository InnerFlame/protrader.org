<?php

namespace app\components;

use app\components\web\View;
use yii;

class Controller extends \yii\web\Controller {

    public $layout = false;

}
