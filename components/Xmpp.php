<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2016
 */

namespace app\components;


use app\commands\XmppController;
use app\models\user\User;
use app\models\user\XMPPUserRecord;
use app\components\OpenFireRestApi;
use yii\base\BootstrapInterface;
use yii\base\Component;
use yii\base\Event;
use yii\db\AfterSaveEvent;

class Xmpp extends Component implements BootstrapInterface
{
    /**
     * @var OpenFireRestApi
     */
    protected $api;

    /**
     * Повесить на очереди
     * @param User $user
     * @return XMPPUserRecord|bool
     */
    public function addUser(User $user)
    {
        try{
            $xmpp = new XMPPUserRecord();
            $xmpp->user = $user;
            if($xmpp->save()){
                $this->getApi()->addUser($xmpp->username, $xmpp->token, $user->getFullname(), $user->email, XmppController::XMPP_GROUP_NAME);
            }else{
                \Yii::error(\Yii::t('app', "Error adding user {username}. Error validate: {errors}", ['username' => $xmpp->username, 'errors' => json_encode($xmpp->getErrors())]));
                return false;
            }
        }catch (\Exception $ex){
            \Yii::error(\Yii::t('app', "Error adding user {username}. Reason: {reason}", ['username' => $user->username, 'reason' => $ex->getMessage()]));
            return false;
        }

        return $xmpp;
    }

    public function updateUser(User $user)
    {
        $xmpp = XMPPUserRecord::find()->where(['user_ud' => $user->id])->one();
        if(!$xmpp){
            return false;
        }
        try{
            $xmpp->user = $user;
            if($xmpp->save()){
                return  true;
            }else{
                \Yii::error(\Yii::t('app', "Error update user {username}. Error validate: {errors}", ['username' => $xmpp->username, 'errors' => json_encode($xmpp->getErrors())]));
                return false;
            }
        }catch (\Exception $ex){
            \Yii::error(\Yii::t('app', "Error update user {username}. Reason: {reason}", ['username' => $user->username, 'reason' => $ex->getMessage()]));
            return false;
        }

    }

    public function deleteUser(User $user)
    {
        $xmpp = XMPPUserRecord::find()->where(['user_id' => $user->id])->one();
        if(!$xmpp){
            return false;
        }
        try{
            $this->getApi()->deleteUser($xmpp->username);
            return $xmpp->delete();
        }catch (\Exception $ex){
            \Yii::error(\Yii::t('app', "Error deleted user {username}. Reason: {reason}", ['username' => $xmpp->username, 'reason' => $ex->getMessage()]));
        }
    }

    public function bootstrap($app)
    {
        Event::on(User::className(), User::EVENT_AFTER_INSERT, function(AfterSaveEvent $event){
            $user = $event->sender;
            if($user instanceof User){
                $this->addUser($user);
            }
        });

        Event::on(User::className(), User::EVENT_AFTER_UPDATE, function(AfterSaveEvent $event){
            $user = $event->sender;
            if($user instanceof User){
                $this->updateUser($user);
            }
        });

        Event::on(User::className(), User::EVENT_AFTER_DELETE, function(Event $event){
            $user = $event->sender;
            if($user instanceof User){
                $this->deleteUser($user);
            }
        });
    }

    /**
     * @return OpenFireRestApi
     */
    protected function getApi()
    {
        if(!$this->api){
            $this->api = new OpenFireRestApi();

            $this->api->secret = '9251j5Bk32UT5b7S';

            $this->api->host = '138.201.48.228';
            $this->api->useSSL = false;

            $this->api->plugin = "/plugins/restapi/v1";
        }

        return $this->api;
    }
}