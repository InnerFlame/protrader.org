<?php

namespace app\components\count;

use app\models\community\Counts;
use yii\helpers\VarDumper;

class Counter
{
    public function setCount($entity)
    {
        if(is_object($entity)){

            $model = Counts::find()->where([
                'entity'    => $entity->getTypeEntity(),
                'entity_id' => $entity->id,
            ])->one();

            if (is_object($model)) {
                $model->updateCounters(['count_view' => 1]);
            } else {
                $count = new Counts();
                $count->entity = $entity->getTypeEntity();
                $count->entity_id = $entity->id;
                $count->count_view = 1;
                $count->save();

            }

        }
    }

    public function getCount($entity)
    {
        $model =  Counts::find()->where([
            'entity'    => $entity->getTypeEntity(),
            'entity_id' => $entity->id,
        ])->one();

        if (is_object($model))
            return $model->count_view;
    }
}