<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2016
 */

namespace app\components;


class OpenFireRestApi extends \Gidkom\OpenFireRestApi\OpenFireRestApi
{

    /**
     * Костыль, по причине того, что вендор, в вбазом классе
     * этот метод, зачем-то сделал приватным. Что делает его
     * вызов из потомка невозможным.
     *
     * @param   string          $type           Request method
     * @param   string          $endpoint       Api request endpoint
     * @param   array           $params         Parameters
     * @return  array|false                     Array with data or error, or False when something went fully wrong
     */

    protected function doRequest($type, $endpoint, $params=array())
    {
        $base = ($this->useSSL) ? "https" : "http";
        $url = $base . "://" . $this->host . ":" .$this->port.$this->plugin.$endpoint;
        $headers = array(
            'Accept' => 'application/json',
            'Authorization' => $this->secret
        );

        $body = json_encode($params);

        file_put_contents('temp.json', $body);
        switch ($type) {
            case 'get':
                $result = $this->client->get($url, compact('headers'));
                break;
            case 'post':
                $headers += ['Content-Type'=>'application/json'];
                $result = $this->client->post($url, compact('headers','body'));
                break;
            case 'delete':
                $headers += ['Content-Type'=>'application/json'];
                $result = $this->client->delete($url, compact('headers','body'));
                break;
            case 'put':
                $headers += ['Content-Type'=>'application/json'];
                $result = $this->client->put($url, compact('headers','body'));
                break;
            default:
                $result = null;
                break;
        }

        if ($result->getStatusCode() == 200 || $result->getStatusCode() == 201) {
            return array('status'=>true, 'message'=>json_decode($result->getBody()));
        }
        return array('status'=>false, 'message'=>json_decode($result->getBody()));

    }

    /**
     * @param $username
     * @param $group
     * @return array|false
     */
    public function addUserToGroup($username, $group)
    {
        $endpoint = sprintf('/users/%s/groups/%s',$username, $group);
        return $this->doRequest('post', $endpoint);
    }

    /**
     * Добавляет пользователя
     * @param $username
     * @param $groups
     * @return array|false
     */
    public function addUserToGroups($username, $groups)
    {
        $endpoint = sprintf('/users/%s/groups', $username);
        return $this->doRequest('post', $endpoint, compact('groups'));
    }
    public function addUser($username, $password, $name = false, $email = false, $groups = false)
    {
        try{
            $result =  parent::addUser($username, $password, $name, $email, $groups);
            if($result['status'] == false){
                return false;
            }
            if(is_array($groups)){
                return $this->addUserToGroups($username, $groups);
            }
            if(is_string($groups)){
                return $this->addUserToGroup($username, $groups);
            }
        }catch (\Exception $ex){
            \Yii::error(sprintf('Не удалось добавить пользователя и присвоить ему группу. Причина: %s', $ex->getMessage()));
            return false;
        }
    }


}