<?php

namespace app\components;

use app\controllers\CabinetController;
use yii;
use yii\data\ActiveDataProvider;

class CustomController extends \yii\web\Controller
{
    public $layout = false;
    public $viewUrl = '/articles/';
    public $indexUrl = '/articles/';
//    public $deleteCommentUrl = '/admin/comments-delete?id=';
    public $deleteCommentUrl = '/comments/comments-delete?id=';

    public $search_visible = true;

    public $url = [
        'base' => '/',
        'topnav' => [
            'label' => 'Admin Panel',
            'link'  => '/admin'
        ],
    ];

    public $breadcrumbs = [];

    /**
     * @param yii\base\Action $action
     * @return bool
     * @throws yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if (Yii::$app->session->has('lang')) {
            Yii::$app->language = Yii::$app->session->get('lang');
        } else {
            Yii::$app->language = 'en-US';
            Yii::$app->session->set('lang', 'en-US');
        }
        if($action->id != 'login' && $action->id != 'error')
            Yii::$app->getUser()->setReturnUrl(Yii::$app->request->getPathInfo());

        if(!Yii::$app->user->isGuest){
            $user = yii\helpers\ArrayHelper::toArray(Yii::$app->user->identity, ['app\models\user\UserIdentity' => ['id', 'username']]);
            $this->getView()->registerJs(sprintf("var user = %s;", yii\helpers\Json::encode($user)), yii\web\View::POS_HEAD);
        }
        return parent::beforeAction($action);
    }

    public function getIndexUrl()
    {
        $part = explode("-", Yii::$app->request->url);

        return $part[0] . '-index/';
    }

    public function getSearchAction()
    {
        $part = explode("/", Yii::$app->request->url);

        return '/' . $part[1] . '/search';
    }

    public function getDataProvider($query, $pageSize)
    {
        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $pageSize,
            ],
        ]);
    }

    /**
     * @param string $attr
     * @return mixed|null
     */
    public static function checkCurrentSessionAttr($attr = '', $array_category_id = null)
    {

        if (is_string($attr) && $attr) {
            if (isset($_GET[$attr])) {
                Yii::$app->session->set($attr, $_GET[$attr]);
            }
            if (Yii::$app->session->has($attr))
                return Yii::$app->session->get($attr);
        }
        return null;
    }

    /**
     * @param array $errors
     */
    public static function setFlashWithErrors($errors = [])
    {


        $errorStr = [];
        foreach($errors as $field) {
            foreach($field as $error) {
                $errorStr[] = $error;
            }
        }
        Yii::$app->session->setFlash('danger', implode("<br>", $errorStr));

    }

}