<?php
/**
 * Created by PhpStorm.
 * @author: Roman Budnitsky
 * @date: 13.11.14 10:35
 * @copyright Copyright (c) 2014 PFSOFT LLC
 *
 * Class ActiveRecord
 */

namespace app\components;

use yii;
use yii\db\ActiveRecord;

abstract class ARWithDynamicExtensions extends ActiveRecord {

    private $_components = null;
    public function getComponents() {
        if($this->_components === null) {
            $this->_components = [];
            $components = $this->getDynamicComponentsConfig();
            foreach($components as $id => $component) {
                $this->_components[$id] = Yii::createObject($component);
            }
        }
        return $this->_components;
    }

    public function getComponent($id) {
        if($this->_components !== null && isset($this->_components[$id])) return $this->_components[$id];
        return null;
    }

    abstract public function getDynamicComponentsConfig();

    /*public static function find()
    {
        $query = parent::find();
        $called_class = get_called_class();
        $behaviors = (new $called_class)->getBehaviors();
        foreach ($behaviors as $object) {
            if ($object->hasMethod('modifyFind')) {
                return call_user_func_array([$object, 'modifyFind'], [$query]);
            }
        }
        return $query;
    }*/

} 