<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2016
 */

namespace app\components\web;


class View extends \yii\web\View
{
    /**
     * Региструет JS переменную
     * @param $var имя переменной
     * @param $value значение переменной
     */
    public function registerJsVar($var, $value)
    {
        $this->registerJs(sprintf('var %s = "%s";', $var, $value), self::POS_HEAD);
    }
}