<?php
/**
 * Created by PhpStorm.
 * @author: Roman Budnitsky
 * @date: 05.11.14 13:09
 * @copyright Copyright (c) 2014 PFSOFT LLC
 */

/**
 * @param array $func
 * @param array $params
 * @return string
 */
function call_user_func_asoc($func, $params = []) {
    if(is_array($func) && isset($func[0]) && isset($func[1])) {
        $func_params = (new \ReflectionMethod($func[0], $func[1]))->getParameters();
        foreach($func_params as $n => $param) {
            if(isset($params[$param->getName()])) {
                $func_params[$n] = $params[$param->getName()];
            } else {
                if($param->isDefaultValueAvailable()) {
                    $func_params[$n] = $param->getDefaultValue();
                } else {
                    throw new \yii\base\ErrorException('Missing argument '.($n+1).' for '.$func[0]::className().'::'.$func[1].'()', E_WARNING);
                }
            }
        }
        return call_user_func_array([$func[0], $func[1]], $func_params);
    }
    return false;
}