<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 */

namespace app\components\notification\assets;


use yii\web\AssetBundle;

class NotifyAsset extends AssetBundle
{
    public $sourcePath = '@app/components/notification/assets';

    public $js = [
        'js/notify.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];
}