<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 * Date: 15.02.2016
 * Time: 17:25
 */

namespace app\components\notification\commands;


use app\components\notification\models\Notification;
use yii\console\Controller;
use Yii;
use yii\helpers\ArrayHelper;

class MailController extends Controller
{

    public function actionSend()
    {

        $notifications = Notification::find()->where("'read' = 0 AND 'send' = 0")->orderBy(['created_at' => SORT_ASC])->limit(10)->all();
        foreach($notifications as $notify){
            $message = \Yii::$app->view->renderFile(Yii::getAlias('@mail') . '/' . 'notify.twig', [
                'content' => $notify->getMessage()->getBody(),
                'url' => $notify->getMessage()->getCallBackUrl()
            ]);

            \Yii::$app->mailer->compose()
                ->setFrom(isset(\Yii::$app->params['adminEmail']) ? \Yii::$app->params['adminEmail'] : '')
                ->setTo($notify->getMessage()->getUser()->email)
                ->setSubject($notify->getMessage()->getSubject())
                ->setHtmlBody($message)
                ->send();
        }
        Notification::updateAll([
            'send' => 1,
            'send_date' => time()
        ],['in', 'id', ArrayHelper::getColumn($notifications, 'id')]);
        return true;
    }
}