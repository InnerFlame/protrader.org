<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 * @package app\components\notification
 */

namespace app\components\notification;


use app\components\customComments\CustomComments;
use app\components\notification\assets\NotifyAsset;
use app\components\notification\handlers\ArticleEventHandler;
use app\components\notification\handlers\CommentsEventHandler;
use app\components\notification\handlers\ForumEventHandler;
use app\components\notification\models\NotificationType;
use app\components\notification\models\UserNotification;
use app\models\user\User;
use app\modules\articles\ArticlesModule;
use app\modules\forum\ForumModule;
use yii\base\Component;
use yii\base\Event;

class Notification extends Component
{

    public function init()
    {
        Event::on(ForumModule::className(), ForumModule::EVENT_COMMENT_ADD, [new ForumEventHandler(), 'addComment']);
        Event::on(ForumModule::className(), ForumModule::EVENT_COMMENT_REPLY, [new ForumEventHandler(), 'replyComment']);
        Event::on(ForumModule::className(), ForumModule::EVENT_TOPIC_MOVE, [new ForumEventHandler(), 'moveTopic']);
        Event::on(ForumModule::className(), ForumModule::EVENT_TOPIC_ADD, [new ForumEventHandler(), 'addTopic']);
        Event::on(ArticlesModule::className(), ArticlesModule::EVENT_ARTICLE_ADD, [new ArticleEventHandler(), 'addTopic']);
        Event::on(CustomComments::className(), CustomComments::EVENT_COMMENT_ADD, [new CommentsEventHandler(), 'add']);
        Event::on(CustomComments::className(), CustomComments::EVENT_COMMENT_REPLY, [new CommentsEventHandler(), 'reply']);
        Event::on(User::className(), User::EVENT_AFTER_UPDATE, [$this, 'updateUserForm']);
        Event::on(User::className(), User::EVENT_AFTER_INSERT, [$this, 'insertUserForm']);

    }

    /**
     * Get All enabled events in system and
     * subscribe user.
     * @param $event
     */
    public function insertUserForm($event)
    {
        $all = NotificationType::find()->enabled()->all();
        foreach($all as $notificationModel){
            $model = new UserNotification();
            $model->type_id = $notificationModel->id;
            $model->user_id = $event->sender->id;
            $model->enabled = 1;
            $model->save();
        }
    }
    /**
     * Update subscribe notification user.
     * @param $event
     */
    public function updateUserForm($event)
    {
        if(!isset($_POST['NotificationsForm']) || !isset($_POST['NotificationsForm']['notifications']))
            return;

        $notifications = $_POST['NotificationsForm']['notifications'];
        $all = NotificationType::find()->enabled()->all();

        foreach($all as $notificationModel){
            $model = UserNotification::find()->where('user_id = :user_id AND type_id = :type_id',
                [
                    ':user_id' => $event->sender->id,
                    ':type_id' => $notificationModel->id
                ])->one();
            if(!$model){
                $model = new UserNotification();
                $model->type_id = $notificationModel->id;
                $model->user_id = $event->sender->id;
            }
            $model->enabled = 0;
            //если форма была не передана вообще, значит создавали пользователя
            //администраторы. Нужно включить все данные.
            if((!isset($_POST['NotificationsForm'])) || (!empty($notifications) && array_search($model->type_id, $notifications) !== false)){
                $model->enabled = 1;
            }

            $model->save();
        }
    }
}