<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 * Date: 17.02.2016
 * Time: 13:28
 */

namespace app\components\notification\models;



class NotificationQuery extends \yii\db\ActiveQuery
{
    public function unread()
    {
        return $this->andWhere('`read` = 0');
    }
}