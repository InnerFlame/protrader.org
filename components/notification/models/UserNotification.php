<?php

namespace app\components\notification\models;

use Yii;

/**
 * This is the model class for table "user_notifications".
 *
 * @property integer $type_id
 * @property integer $user_id
 * @property integer $enabled
 *
 * @property User $user
 * @property NotificationsType $type
 */
class UserNotification extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_notifications';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id', 'user_id'], 'required'],
            [['type_id', 'user_id', 'enabled'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'type_id' => 'Type ID',
            'user_id' => 'User ID',
            'enabled' => 'Enabled',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(NotificationsType::className(), ['id' => 'type_id']);
    }
}
