<?php

namespace app\components\notification\models;

use app\components\customComments\CustomComments;
use app\modules\articles\ArticlesModule;
use app\modules\forum\ForumModule;
use Yii;
use app\components\notification\models\messages\Message;
use app\models\user\User;
use yii\helpers\StringHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "notifications".
 *
 * @property integer $id
 * @property integer $type_id
 * @property integer $user_id
 * @property string $data
 * @property integer $send
 * @property integer $read
 * @property integer $created_at
 * @property integer $send_date
 * @property integer $sender_id
 *
 * @property NotificationsType $type
 * @property User $sender
 * @property User $user
 */
class Notification extends \yii\db\ActiveRecord
{
    private $_htmlClassTitle = [
        ForumModule::EVENT_COMMENT_ADD => 'new-rep',
        ForumModule::EVENT_COMMENT_ADD_TO_AUTHOR => 'new-rep',
        ForumModule::EVENT_COMMENT_REPLY => 'new-ans',
        ForumModule::EVENT_TOPIC_ADD => 'new-art',
        CustomComments::EVENT_COMMENT_ADD => 'new-rep',
        CustomComments::EVENT_COMMENT_REPLY => 'new-ans',
        ArticlesModule::EVENT_ARTICLE_ADD => 'new-art',
    ];

    private $_labels = [
        ForumModule::EVENT_COMMENT_ADD => 'New comment in',
        ForumModule::EVENT_COMMENT_ADD_TO_AUTHOR => 'New comment in',
        ForumModule::EVENT_COMMENT_REPLY => 'New reply in',
        ForumModule::EVENT_TOPIC_ADD => 'New Thread',
        CustomComments::EVENT_COMMENT_ADD => 'New comment in',
        CustomComments::EVENT_COMMENT_REPLY => 'New answer in',
        ArticlesModule::EVENT_ARTICLE_ADD => 'New Article',
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notifications';
    }

    /**
     * @param Message $message
     * @return Notification
     */
    public static function modelByMessage(Message $message) {

        $notification = new Notification();
        $notification->sender_id = $message->getSenderId();
        $notification->type_id = $message->getTypeId();
        $notification->user_id = $message->getUser()->id;
        $notification->data = serialize($message);
        return $notification;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id', 'user_id', 'sender_id'], 'required'],
            [['type_id', 'user_id', 'send', 'read', 'created_at', 'send_date', 'sender_id'], 'integer'],
            [['send', 'read'],'default', 'value' => 0],
            [['created_at'],'default', 'value' => time()],
            [['data'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type_id' => 'Type ID',
            'user_id' => 'User ID',
            'data' => 'Data',
            'send' => 'Send',
            'read' => 'Read',
            'created_at' => 'Created At',
            'send_date' => 'Send Date',
            'sender_id' => 'Sender ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(NotificationType::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSender()
    {
        return $this->hasOne(User::className(), ['id' => 'sender_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Unserialized and return message
     * @return Message
     */
    public function getMessage()
    {
        return unserialize($this->data);

    }

    /**
     * Return title label in activity notification
     * @return string
     */
    public function getLabel()
    {
        $eventName = $this->type->name;
        return $this->_labels[$eventName] ? $this->_labels[$eventName] : 'New Activity';
    }
    /**
     * Return class for title notification.
     * Color changed depending by type notification
     * @return string
     */
    public function getTitleHtmlClass()
    {
        $eventName = $this->type->name;
        return $this->_htmlClassTitle[$eventName] ? $this->_htmlClassTitle[$eventName] : 'new-art';
    }
    /**
     * Truncate content with length. Default length is 65
     * characters. This method wrapper on Message::getContent()
     * @return string
     */
    public function getDescription($length = 65)
    {
        return StringHelper::truncate($this->getMessage()->getTitle(),$length);
    }

    public function getViewUrl()
    {
        return '/notify/view/'.$this->id;
    }

    public static function find()
    {
        return new NotificationQuery(get_called_class());
    }




}
