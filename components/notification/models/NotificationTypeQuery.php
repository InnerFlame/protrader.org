<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 * Date: 11.02.2016
 * Time: 14:13
 */

namespace app\components\notification\models;


class NotificationTypeQuery extends \yii\db\ActiveQuery
{
    public function enabled()
    {
        $this->andWhere('enabled = 1');
        return $this;
    }
}