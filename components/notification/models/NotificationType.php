<?php

namespace app\components\notification\models;

use Yii;

/**
 * This is the model class for table "notifications_type".
 *
 * @property integer $id
 * @property string $name
 * @property integer $enabled
 *
 * @property Notifications[] $notifications
 * @property UserNotifications[] $userNotifications
 */
class NotificationType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notifications_type';
    }

    /**
     * Return ID event type
     * @param $eventName
     * @return array|null|NotificationType
     */
    public static function getTypeIdByName($eventName)
    {
        return self::find()->where('name = :name', [':name' => $eventName])->one();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['enabled'], 'integer'],
            [['name'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'enabled' => 'Enabled',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotifications()
    {
        return $this->hasMany(Notifications::className(), ['type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserNotifications()
    {
        return $this->hasMany(UserNotifications::className(), ['type_id' => 'id']);
    }

    public static function find()
    {
        return new NotificationTypeQuery(get_called_class());
    }


}
