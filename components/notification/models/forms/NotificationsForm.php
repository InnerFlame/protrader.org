<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 * Date: 11.02.2016
 * Time: 11:49
 */

namespace app\components\notification\models\forms;


use app\components\notification\models\UserNotification;
use app\models\user\User;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

class NotificationsForm extends Model
{
    public $notifications = [];


    public function __construct(User $user)
    {
        $user_types = UserNotification::find()->where('user_id = :user_id AND enabled = 1', [':user_id' => $user->id])->all();
        $this->notifications = ArrayHelper::getColumn($user_types, 'type_id');
    }

    public function rules()
    {
        return [
            [['notifications'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'notifications' => \Yii::t('app', 'Send me notifications about'),
            'forum.comment.add' => \Yii::t('app', 'New comments in those threads where I have left comments'),
            'forum.comment.add.role.author' => \Yii::t('app', 'New comments in the threads that I have created'),
            'forum.comment.reply' => \Yii::t('app', 'New replies to my comment in the forum'),
            'forum.topic.move' => \Yii::t('app', 'Move topic that I have created to another thread'),
            'custom.comment.add' => \Yii::t('app', 'New comments in those publications where I have left comments'),
            'custom.comment.reply' => \Yii::t('app', 'New replies to my comment in the publications'),
            'articles.topic.add' => \Yii::t('app', 'New articles and reviews'),
        ];
    }

    public function generateAttributeLabels($data)
    {
        $types = [];
        foreach($data as $item){
            $labels = $this->attributeLabels();
            $types[$item['id']] = $labels[$item['name']] ? $labels[$item['name']] : $item['name'];
        }
        return $types;
    }

}