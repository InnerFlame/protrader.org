<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 *
 * This abstract class is structure for notify messages.
 *
 *
 */

namespace app\components\notification\models\messages;


use app\models\user\User;
use yii\base\Object;

class Message extends Object implements \Serializable
{

    /**
     * User id, by iniciator this notify
     * @var
     */
    protected $sender_id;

    /**
     * @var \app\models\user\User $user
     */
    protected $user;

    /**
     * @var int $type_id Type Id notification
     */
    protected $type_id;

    /**
     * @var string Title by entity
     */
    protected $title;

    /**
     * @var string content of entity
     */
    protected $content;

    /**
     * @var string route for UrlManager
     * @see \yii\base\UrlManager
     */
    protected $route;

    /**
     * Template text subject for email
     * @var null|string
     */
    protected $templateSubject = 'New activity in: {header}';

    /**
     * Template text body for email
     * @var array
     */
    protected $templateBody = 'Hi {username}, new activity i “{header}”';


    /**
     * Generate collection messages by collection users
     * @param $users \app\models\user\User[]
     * @param $sender_id
     * @param $title
     * @param $content
     * @param $type_id
     * @param null $route
     * @param null $templateSubject
     * @param null $templateBody
     * @return array
     */
    public static function generateByUsers($users, $sender_id, $title, $content, $type_id = null, $route = null, $templateSubject = null, $templateBody = null)
    {
        $messages = [];

        if(!is_array($users)){
            return $messages[] = new Message($users, $sender_id, $title, $content, $type_id, $route, $templateSubject, $templateBody);
        }
        foreach ($users as $user) {
            $messages[] = new Message($user, $sender_id, $title, $content, $type_id, $route, $templateSubject, $templateBody);
        }
        return $messages;
    }

    /**
     * @param \app\models\user\User $user
     * @param $sender_id
     * @param $title
     * @param $content
     * @param $type_id
     * @param null $route
     * @param string $templateSubject
     * @param string $templateBody
     */
    public function __construct($user, $sender_id, $title, $content, $type_id = null, $route = null, $templateSubject = null, $templateBody = null){
        $this->user = $user;
        $this->sender_id = $sender_id;
        $this->title = $title;
        $this->content = $content;
        $this->route = $route;
        $this->type_id = $type_id;
        if($templateSubject)
            $this->templateSubject = $templateSubject;

        if($templateBody)
            $this->templateBody = $templateBody;

    }
    /**
     * User sender iniciator event
     * @param $id
     */
    public function setSenderId($id)
    {
        $this->sender_id = $id;
    }

    /**
     * Set username recipient
     * @param \app\models\user\User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * Set title for Entity
     * @param $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Set route for entity sender
     * @param $route
     */
    public function setRoute($route)
    {
        $this->route - $route;
    }

    /**
     * Set text name for event
     * @param $id
     */
    public function setTypeId($id)
    {
        $this->type_id = $id;
    }
    /**
     * Set text template for body
     * @param $template
     */
    public function setBodyTemplate($template)
    {
        $this->templateBody = $template;
    }

    /**
     * Set text template for subject
     * @param $template
     */
    public function setSubjectTemplate($template)
    {
        $this->templateSubject = $template;
    }
    /**
     * Return content message, description article or
     * content comments
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return \app\models\user\User
     */
    public function getUser()
    {
        return $this->user;
    }


    /**
     * This id for \app\models\user\User
     * @return int sender id
     */
    public function getSenderId()
    {
        return $this->sender_id;
    }

    /**
     * Return name for Event
     * @return mixed
     */
    public function getTypeId()
    {
        return $this->type_id;
    }
    /**
     * Return text subject by template
     * @return mixed
     */
    public function getSubject()
    {
        return str_replace(['{username}', '{header}'], [$this->user->username, $this->title], $this->templateSubject);
    }

    /**
     * Return text body for email by template
     * @return mixed
     */
    public function getBody()
    {
        return str_replace(['{username}', '{header}'], [$this->user->username, $this->title], $this->templateBody);
    }

    /**
     * @return string title of Entity
     */
    public function getTitle()
    {
        return $this->title;
    }
    /**
     * Return callback absolute url
     * @return string
     */
    public function getCallBackUrl($absolute = true)
    {
        return $this->route;
    }

    public function serialize()
    {
        $this->user = serialize($this->user->attributes);
        return serialize(get_object_vars($this));
    }

    public function unserialize($serialized)
    {
        $data = unserialize($serialized);
        foreach ($data as $key => $value) {
            if($key == 'user'){
                $user = new User();
                $user->setAttributes(unserialize($value), false);
                $this->user = $user;
                continue;
            }
            $this->$key = $value;
        }
    }


}