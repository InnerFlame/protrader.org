<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 *
 * Custom Message for MoveTopic messages
 */

namespace app\components\notification\models\messages;


class MoveTopicMessage extends Message
{
    public $oldTitle;

    public function __construct($user, $sender_id, $title, $content, $type_id, $oldTitle, $route = null, $templateSubject = null, $templateBody = null)
    {
        $this->oldTitle = $oldTitle;
        parent::__construct($user, $sender_id, $title, $content, $type_id, $oldTitle, $route = null, $templateSubject = null, $templateBody = null);
    }


    public function getContent()
    {
        return str_replace(['{username}', '{header}', '{oldheader}'], [$this->user->username, $this->title, $this->oldTitle], $this->templateSubject);
    }

    public function getSubject()
    {
        return str_replace('{oldheader}', $this->oldTitle, parent::getSubject());
    }


}