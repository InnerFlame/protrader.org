<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 * Date: 15.02.2016
 * Time: 17:45
 */

namespace app\components\notification\widgets;


use app\components\notification\models\Notification;
use yii\bootstrap\Widget;

class ListNotifications extends Widget
{
    public function run()
    {
        $notifications = Notification::find()->with(['type', 'user','sender'])->where('user_id = :user_id', [':user_id' => \Yii::$app->getUser()->id])->unread()->orderBy(['created_at' => SORT_DESC])->all();
        return $this->render('list', ['items' => $notifications]);
    }

}