<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 */

namespace app\components\notification\widgets;


use app\components\notification\assets\NotifyAsset;
use app\modules\cabinet\modules\notify\models\NotifyTemplate;
use app\modules\notify\models\EventMessage;
use yii\bootstrap\Widget;

class BellDropDown extends Widget
{
    public $mobile = false;

    public function run()
    {
        $items = [];
        $messages = EventMessage::find()->where('user_id = :user_id', ['user_id' => \Yii::$app->user->id])->all();


        foreach($messages as $message){
            $groupName = $message->event_id.'-'.$message->getNotify()->getEntity()->getTypeEntity().'-'.$message->getNotify()->getEntity()->id;
            if(!isset($items[$groupName])){
                $items[$groupName] = [];
            }
            $template = NotifyTemplate::find()->where(['type' => NotifyTemplate::TYPE_SHORT])->andWhere(['event_id' => $message->event_id])->one();
            $groupTemplate = NotifyTemplate::find()->where(['type' => NotifyTemplate::TYPE_GROUP])->andWhere(['event_id' => $message->event_id])->one();
            $items[$groupName]['messages'][] = $message;
            $items[$groupName]['template'] = (isset($items[$groupName]['messages']) && count($items[$groupName]['messages']) > 1) ?  $groupTemplate : $template;
        }


//        NotifyAsset::register(\Yii::$app->getView());
        $layout = $this->mobile ? 'mob_bell.twig' : 'bell.twig';
        return $this->render($layout, ['count' => count($messages), 'messages' => $items]);
    }
}