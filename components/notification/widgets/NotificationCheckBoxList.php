<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 * Date: 11.02.2016
 * Time: 9:24
 */

namespace app\components\notification\widgets;


use app\components\notification\models\forms\NotificationsForm;
use app\components\notification\models\NotificationType;
use yii\base\Widget;

class NotificationCheckBoxList extends Widget
{

    /**
     * @var \yii\bootstrap\ActiveForm
     */
    public $form;

    /**
     * @var \app\models\user\User
     */
    public $model;

    public function run()
    {
        $model = new NotificationsForm($this->model);
        $result = NotificationType::find()->asArray()->enabled()->all();
        $types = $model->generateAttributeLabels($result);
        echo $this->form->field($model, 'notifications')->checkboxList($types);
    }
}