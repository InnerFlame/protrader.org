<?php
/**
 * User: P.Bilik
 * Date: 08.02.2016
 *
 * Handler for all events by ForumModule
 * @see \app\modules\forum\ForumModule
 */

namespace app\components\notification\handlers;


use app\components\notification\models\messages\Message;
use app\components\notification\models\messages\MoveTopicMessage;
use app\components\notification\models\NotificationType;
use app\models\user\User;
use app\modules\forum\events\MoveTopicEvent;
use app\modules\forum\events\ReplyEvent;
use app\modules\forum\ForumModule;
use app\modules\forum\models\FrmCategory;
use app\modules\forum\models\FrmTopic;
use yii\base\Event;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

class ForumEventHandler extends NotificationEventHandler
{

    /**
     * Handler for Forum Add new Comment
     * Notify all users that published comments in
     * this topic, and subscribe on this event. As
     * well as notify administrators, moderators users except
     * this user.
     * @param $event
     */
    public function addComment($event)
    {
        $comment = $event->sender;

        $moderators = User::findAllModeratorsExMe();
        $ignoredIds =  [$comment->topic->user_id, $comment->user_id];


        /**
         * Notify all users that subscribe on this event, except
         * author Topic and except all administrators and moderators
         */
        $users = $this->findUsers(ForumModule::EVENT_COMMENT_ADD)
            ->innerJoin('frm_topics ft', 'ft.id = :id', [':id' => $comment->topic->id])
            ->innerJoin('frm_comments fc', 'fc.user_id = user.id AND fc.topic_id = ft.id')
            ->where(['not in', 'user.id', ArrayHelper::merge($ignoredIds, ArrayHelper::getColumn($moderators, 'id'))])->all();


        $messages = Message::generateByUsers($users, $comment->user_id, $comment->topic->title, $comment->content, $this->getCurrentType()->id,
            \Yii::$app->request->referrer.'#reply'.$comment->id, 'New activity in: {header}',
                    'Hi {username}, there is a new comment in the thread "{header}":');
        $this->sendAll($messages);


        /**
         * Notify author Topic that subscribe on event ForumModule::EVENT_COMMENT_ADD_TO_AUTHOR
         * @see \app\modules\forum\ForumModule::EVENT_COMMENT_ADD_TO_AUTHOR
         */
        $user = $this->findUsers(ForumModule::EVENT_COMMENT_ADD_TO_AUTHOR)
            ->innerJoin('frm_topics fc', 'fc.user_id = user.id')
            ->where('user.id = :id', [':id' => $comment->topic->user_id])->one();


        if($user && ($user->role != User::ROLE_ADMIN && $user->role != User::ROLE_MODERATOR)){
            $message = new Message($user, $comment->user_id, $comment->topic->title, $comment->content, $this->getCurrentType()->id,
                \Yii::$app->request->referrer.'#reply'.$comment->id, 'New activity in: {header}',
                'Hi {username}, there is a new comment in the thread "{header}":');
            $this->send($message);
        }

        /**
         * Notify moderators and administrators
         */
        $messages = Message::generateByUsers($moderators, $comment->user_id, $comment->topic->title, $comment->content, $this->getCurrentType()->id,
            \Yii::$app->request->referrer.'#reply'.$comment->id, 'New activity in: {header}',
            'Hi {username}, there is a new comment in the thread "{header}":');

        $this->sendAll($messages);
    }

    /**
     * Handler Event by reply message
     * Notify the user destination or subscribe on this event.
     * And notify all moderators and administrators
     * @param ReplyEvent $event
     */
    public function replyComment(ReplyEvent $event)
    {
        $topic = $event->comment->topic;

        $moderators = User::findAllModeratorsExMe();
        $ignoredIds =  [$topic->user_id, $event->reply->user_id, $event->comment->user_id];

        $user = $this->findUsers(ForumModule::EVENT_COMMENT_REPLY)
            ->where('user.id = :id', [':id' => $event->comment->user_id])->one();

        $message = new Message($user, $event->reply->user_id, $topic->title, $event->comment->content, $this->getCurrentType()->id,
            \Yii::$app->request->referrer.'#reply'.$event->reply->id, 'New activity in: {header}', 'Hi {username}, there is a new reply on the topic "{header}":');
        $this->send($message);

        //other users send event as add comment, because for them this not reply
        $users = $this->findUsers(ForumModule::EVENT_COMMENT_ADD)
            ->innerJoin('frm_topics ft', 'ft.id = :id', [':id' => $topic->id])
            ->innerJoin('frm_comments fc', 'fc.user_id = user.id AND fc.topic_id = ft.id')
            ->where(['not in', 'user.id', ArrayHelper::merge([$event->reply->user_id, $event->comment->user_id], ArrayHelper::getColumn($moderators, 'id'))])->all();

        $messages = Message::generateByUsers($users, $event->reply->user_id, $topic->title, $event->reply->content, $this->getCurrentType()->id,
            \Yii::$app->request->referrer.'#reply'.$event->reply->id, 'New activity in: {header}',
            'Hi {username}, there is a new comment in the thread "{header}":');
        $this->sendAll($messages);

        //send notify for author user or not moderator and not administrator
        $user = $this->findUsers(ForumModule::EVENT_COMMENT_ADD_TO_AUTHOR)->where('user.id = :user_id', [':user_id' => $topic->user_id])->one();
        if($user && ($user->role != User::ROLE_MODERATOR || $user->role != User::ROLE_ADMIN)){
            $message = Message::generateByUsers($user, $event->reply->user_id, $topic->title, $event->reply->content, $this->getCurrentType()->id,
                \Yii::$app->request->referrer.'#reply'.$event->reply->id, 'New activity in: {header}',
                'Hi {username}, there is a new comment in the thread "{header}":');
            $this->send($message);
        }

        $messages = Message::generateByUsers($moderators, $event->reply->user_id, $topic->title, $event->reply->content, $this->getCurrentType()->id,
            \Yii::$app->request->referrer.'#reply'.$event->reply->id, 'New activity in: {header}',
            'Hi {username}, there is a new comment in the thread "{header}":');
        $this->sendAll($messages);
    }

    /**
     * Обработка событий, если добавлен новый топик.
     * @param $event
     */
    public function addTopic(Event $event)
    {
        $moderators = User::findAllModeratorsExMe();
        $type = NotificationType::getTypeIdByName(ForumModule::EVENT_TOPIC_ADD);
        $messages = Message::generateByUsers($moderators, $event->sender->user_id, $event->sender->title, $event->sender->content, $type->id,
            \Yii::$app->urlManager->createAbsoluteUrl($event->sender->getViewUrl()), 'New activity in: {header}',
            'Hi {username}, create new thread "{header}":');
        $this->sendAll($messages);
    }

    /**
     * Handler event by move topic to the other category
     * Get old and new Category and notify author user.
     * @param MoveTopicEvent $event
     * @return bool
     */
    public function moveTopic(MoveTopicEvent $event)
    {
        $user = $event->topic->getUser()
            ->innerJoin('notifications_type', 'notifications_type.name = :event', [':event' => ForumModule::EVENT_TOPIC_MOVE])
            ->innerJoin('user_notifications un', 'user.id = un.user_id AND un.type_id = notifications_type.id AND un.enabled = 1')
            ->one();

        $oldCat = FrmCategory::findOne($event->oldCategoryId);
        $newCat = $event->topic->getCategory()->one();

        if(!$user || ($user->role == User::ROLE_MODERATOR || $user->role == User::ROLE_ADMIN) || !$oldCat || !$newCat)
            return false;


        $type = NotificationType::getTypeIdByName(ForumModule::EVENT_TOPIC_MOVE);
        //$user, $sender_id, $title, $content, $type_id, $oldTitle, $route = null, $templateSubject = null, $templateBody = null
        $message = new MoveTopicMessage($user, $event->user_id, $newCat->title, $newCat->description, $type->id, $oldCat->title, \Yii::$app->urlManager->createAbsoluteUrl($event->topic->getViewUrl()),
            'Your topic {oldheader} has been moved', 'Hi {username}, your topic "{oldheader}" has been moved to  "{header}" thread');

        $message->setSenderId($event->user_id);
        $this->send($message);

    }
}