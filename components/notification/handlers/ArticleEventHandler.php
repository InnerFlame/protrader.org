<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 * Handler for all events generated ArticlesModule
 * @see \app\modules\Articles\ArticlesModule.php
 */

namespace app\components\notification\handlers;


use app\components\notification\models\messages\Message;
use app\components\notification\models\NotificationType;
use app\models\user\User;
use app\modules\articles\ArticlesModule;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

class ArticleEventHandler extends NotificationEventHandler
{

    /**
     * Handler event add new topic
     * Notify all users that subscribe on this event, except
     * moderators and administrators.
     * @param $event
     */
    public function addTopic($event)
    {
        $moderators = User::findAllModerators();

        $users = $this->findUsers(ArticlesModule::EVENT_ARTICLE_ADD)
            ->where(['not in', 'user.id', ArrayHelper::getColumn($moderators, 'id')])->all();

        $messages = Message::generateByUsers($users, $event->sender->user_id, $event->sender->title, $event->sender->content, $this->getCurrentType()->id,
            \Yii::$app->urlManager->createAbsoluteUrl($event->sender->getViewUrl()), "{header} - PTMC Articles", "Hi {username}, there is a new article you are subscribed to:");

        $this->sendAll($messages);
    }
}