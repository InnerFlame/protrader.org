<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 *
 * Handler for all events generated CustomComments component.
 * @see \app\components\customComments\CustomComments.php
 */

namespace app\components\notification\handlers;


use app\components\customComments\CustomComments;
use app\components\customComments\events\ReplyEvent;
use app\components\Entity;
use app\components\notification\models\messages\Message;
use app\models\community\Comments;
use app\models\user\User;
use yii\base\Event;
use yii\helpers\ArrayHelper;

class CommentsEventHandler extends NotificationEventHandler
{

    /**
     * Handler event by add new message
     * Notify all users, except sender this message, that published messages in this
     * article and subscribe on this event. And notify all administrators and moderators
     * @param $event
     * @return null
     */
    public function add($event)
    {
        if(!$comment = $this->getSender($event))
            return null;

        if(!$entity = Entity::findEntity($comment->entity, $comment->entity_id))
            return null;


        $moderators = User::findAllModeratorsExMe();
        $ignoredIds =  [$entity->user_id, $comment->user_id];


        //Notify all users except moderators, administrators, and sender user
        $users = $this->findUsers(CustomComments::EVENT_COMMENT_ADD)
            ->innerJoin('com_comments cc', 'cc.user_id = user.id AND entity_id = :entity_id', [':entity_id' => $entity->id])
            ->where(['not in', 'user.id', ArrayHelper::merge($ignoredIds, ArrayHelper::getColumn($moderators, 'id'))])
            ->all();


        $messages = Message::generateByUsers($users, $comment->user_id, $entity->title, $comment->content, $this->getCurrentType()->id,
            \Yii::$app->request->referrer.'#'.$comment->id, null, 'Hi {username}, there is a new comment on the article "{header}"');
        $this->sendAll($messages);



        $messages = Message::generateByUsers($moderators, $comment->user_id, $entity->title, $comment->content, $this->getCurrentType()->id,
            \Yii::$app->request->referrer.'#'.$comment->id, null,
            'Hi {username}, there is a new comment on the article "{header}"');
        $this->sendAll($messages);
    }

    /**
     * Handler Event by reply message
     * Notify the user destination or subscribe on this event.
     * And notify all moderators and administrators
     * @param ReplyEvent $event
     * @return null
     */
    public function reply(ReplyEvent $event){

        if(!$entity = Entity::findEntity($event->comment->entity, $event->comment->entity_id))
            return null;

        $user = $this->findUsers(CustomComments::EVENT_COMMENT_REPLY)
            ->where('user.id = :id', [':id' => $event->comment->user_id])->one();

        $moderators = User::findAllModeratorsExMe();
        $ignoredIds =  [$entity->user_id, $event->comment->user_id, $event->reply->user_id];



        $message = new Message($user, $event->reply->user_id, $entity->title, $event->comment->content,
            $this->getCurrentType()->id, \Yii::$app->request->referrer.'#'.$event->reply->id, 'New activity in: {header}', 'Hi {username}, there is a new reply on the article "{header}":');
        $this->send($message);

        //other users send event as add comment, because for them this not reply
        //Notify all users except moderators, administrators, and sender user
        $users = $this->findUsers(CustomComments::EVENT_COMMENT_ADD)
            ->innerJoin('com_comments cc', 'cc.user_id = user.id AND entity_id = :entity_id', [':entity_id' => $entity->id])
            ->where(['not in', 'user.id', ArrayHelper::merge($ignoredIds, ArrayHelper::getColumn($moderators, 'id'))])
            ->all();

        //$users, $sender_id, $title, $content, $type_id = null, $route = null, $templateSubject = null, $templateBody = null
        $messages = Message::generateByUsers($users, $event->reply->user_id, $entity->title, $event->reply->content, $this->getCurrentType()->id,
            \Yii::$app->request->referrer.'#'.$event->reply->id, null, 'Hi {username}, there is a new comment on the article "{header}"');
        $this->sendAll($messages);


        $messages = Message::generateByUsers($moderators, $event->reply->user_id, $entity->title, $event->reply->content, $this->getCurrentType()->id,
            \Yii::$app->request->referrer.'#'.$event->reply->id, 'New activity in: {header}', 'Hi {username}, there is a new reply on the article "{header}":');
        $this->sendAll($messages);
    }

    /**
     * Get sender object and convert to Message object
     * @see \app\models\CustomModel
     * @param Event $event
     * @return null | Comments
     */
    protected function getSender(Event $event)
    {
        $namespace = Comments::className();
        if(!$event->sender || !$event->sender instanceof $namespace)
            return null;

        return $event->sender;
    }

}