<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 */

namespace app\components\notification;


use app\components\notification\models\Notification as Notify;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;

class NotificationController extends Controller
{
    /**
     * Mark notification as read, and redirect user
     * to entity url
     * @param $id
     * @return bool
     */
    public function actionView($id)
    {
        $n = Notify::findOne($id);
        if($n->user_id == \Yii::$app->getUser()->id && $n->read == 0){
            $n->read = 1;
            $n->save();
            $this->redirect($n->getMessage()->getCallBackUrl());
            return true;
        }
        $this->redirect(Url::home());
    }

    /**
     * change new notifications for user
     */
    public function actionIndex()
    {
        $notifyes = Notify::find()->where('user_id = :uid', [':uid' => \Yii::$app->getUser()->id])->unread()->orderBy(['created_at' => SORT_DESC])->all();
        $count = count($notifyes);
        $html = $this->renderPartial('_bell_item.twig',[
            'items' => $notifyes,
            'count' => $count
        ]);
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return ['count' => count($notifyes), 'html' => $html];
    }

    /**
     * Mark all notifcations as read
     */
    public function actionAll()
    {
        Notify::updateAll([
            'read' => 1
        ], 'user_id = :user_id', [':user_id' => \Yii::$app->user->identity->id]);

        $this->redirect(\Yii::$app->request->referrer);
    }

    public function getViewPath()
    {
        return dirname(__FILE__).DIRECTORY_SEPARATOR.'widgets'.DIRECTORY_SEPARATOR.'views';
    }



}