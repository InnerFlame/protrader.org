<?php
namespace app\components\sort;

use app\models\Constants;
use Yii;
use yii\base\Component;

class Sortable extends Component
{
    public function run($model)
    {

        if (Yii::$app->request->get('sort') === 'popular') {
            return $model->joinWith('counts')
                ->groupBy('id')
                ->orderBy('com_counts.count_like DESC, com_counts.count_dislike ASC');

        }

        if (Yii::$app->request->get('sort') === '-popular') {
            return $model->joinWith('counts')
                ->groupBy('id')
                ->orderBy('com_counts.count_like ASC, com_counts.count_dislike DESC');

        }

        if (Yii::$app->request->get('sort') === 'date') {
            return $model->orderBy('created_at DESC');
        }

        if (Yii::$app->request->get('sort') === '-date') {
            return $model->orderBy('created_at ASC');
        }

        return $model;
    }
}