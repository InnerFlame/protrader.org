<?php
/**
 * LinkedinOAuth2Service class file.
 *
 * Register application: https://www.linkedin.com/secure/developer
 * Note: Integration URL should be filled with a valid callback url.
 *
 * @author Maxim Zemskov <nodge@yandex.ru>
 * @link http://github.com/Nodge/yii2-eauth/
 * @license http://www.opensource.org/licenses/bsd-license.php
 */

namespace app\components\eauth;

use nodge\eauth\services\FacebookOAuth2Service;
use yii\helpers\VarDumper;

/**
 * LinkedIn provider class.
 *
 * @package application.extensions.eauth.services
 */
class CustomFacebookOAuth2Service extends FacebookOAuth2Service
{
    protected $scope = 'email,user_birthday,user_hometown,user_location';


    protected $scopes = [self::SCOPE_EMAIL];

    protected function fetchAttributes() {
        $this->attributes = (array)$this->makeSignedRequest('https://graph.facebook.com/v2.5/me', array(
            'query' => array(
                'fields' => join(',', array(
                    'id',
                    'name',
                    'link',
                    'email',
                    'verified',
                    'first_name',
                    'last_name',
                    'gender',
                    'birthday',
                    'hometown',
                    'location',
                    'locale',
                    'timezone',
                    'updated_time',
                ))
            )
        ));
    }

}