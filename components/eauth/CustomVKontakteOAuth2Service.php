<?php
/**
 * VKontakteOAuth2Service class file.
 *
 * Register application: http://vk.com/editapp?act=create&site=1
 *
 * @author Maxim Zemskov <nodge@yandex.ru>
 * @link http://github.com/Nodge/yii2-eauth/
 * @license http://www.opensource.org/licenses/bsd-license.php
 */

namespace app\components\eauth;

use nodge\eauth\oauth2\Service;
use nodge\eauth\services\VKontakteOAuth2Service;
use OAuth\OAuth2\Service\ServiceInterface;

/**
 * VKontakte provider class.
 *
 * @package application.extensions.eauth.services
 */
class CustomVKontakteOAuth2Service extends VKontakteOAuth2Service
{

    protected function fetchAttributes()
    {
        $tokenData = $this->getAccessTokenData();
        $info = $this->makeSignedRequest('users.get.json', [
            'query' => [
                'uids' => $tokenData['params']['user_id'],
              //  'fields' => '', // uid, first_name and last_name is always available
                'fields' => 'nickname, sex, bdate, city, country, timezone, photo, photo_medium, photo_big, photo_rec,photo_id, verified, sex, bdate, city, country, home_town, has_photo, photo_50, photo_100, photo_200_orig, photo_200, photo_400_orig, photo_max, photo_max_orig, online, lists, domain, has_mobile, contacts, site, education, universities, schools, status, last_seen, followers_count, common_count, occupation, nickname, relatives, relation, personal, connections, exports, wall_comments, activities, interests, music, movies, tv, books, games, about, quotes, can_post, can_see_all_posts, can_see_audio, can_write_private_message, can_send_friend_request, is_favorite, is_hidden_from_feed, timezone, screen_name, maiden_name, crop_photo, is_friend, friend_status, career, military, blacklisted, blacklisted_by_me',
            ],
        ]);

//        echo '<pre>'; var_dump($info);die;

        $info = $info['response'][0];


        $this->attributes['name'] = $info['first_name'] . ' ' . $info['last_name'];
        $this->attributes['first_name'] = $info['first_name'];
        $this->attributes['last_name'] =  $info['last_name'];

        $this->attributes['url'] = 'http://vk.com/id' . $info['uid'];

        if(isset($info['uid']))
            $this->attributes['id'] = $info['uid'];

       if (!empty($info['domain']))
            $this->attributes['username'] = $info['domain'];
        else
            $this->attributes['username'] = $info['screen_name'];

                if(isset($info['sex']))
               $this->attributes['gender'] = $info['sex'] == 1 ? 'F' : 'M';


               $this->attributes['address'] = $info['home_town'];
               $this->attributes['university_name'] = $info['university_name'];
               $this->attributes['faculty_name'] = $info['faculty_name'];
               $this->attributes['education_form'] = $info['education_form'];
               $this->attributes['education_status'] = $info['education_status'];
               $this->attributes['count_friends'] = $info['common_count'];

               $this->attributes['birth_at'] = $info['bdate'];

               $this->attributes['country'] = $info['country'];

               $this->attributes['timezone'] = timezone_name_from_abbr('', $info['timezone']*3600, date('I'));;

               $this->attributes['photo'] = $info['photo'];
               $this->attributes['photo_medium'] = $info['photo_medium'];

               $this->attributes['pictures'] = $info['photo_max_orig'];

              $this->attributes['photo_rec'] = $info['photo_rec'];

        return true;
    }

}