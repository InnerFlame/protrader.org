<?php
/**
 * LinkedinOAuth2Service class file.
 *
 * Register application: https://www.linkedin.com/secure/developer
 * Note: Integration URL should be filled with a valid callback url.
 *
 * @author Maxim Zemskov <nodge@yandex.ru>
 * @link http://github.com/Nodge/yii2-eauth/
 * @license http://www.opensource.org/licenses/bsd-license.php
 */

namespace app\components\eauth;

use nodge\eauth\services\LinkedinOAuth2Service;
use OAuth\OAuth2\Service\ServiceInterface;
use nodge\eauth\oauth2\Service;
use yii\helpers\VarDumper;

/**
 * LinkedIn provider class.
 *
 * @package application.extensions.eauth.services
 */
class CustomLinkedinOAuth2Service extends LinkedinOAuth2Service{

    protected $scopes = [self::SCOPE_R_BASICPROFILE, self::SCOPE_R_EMAILADDRESS];

    protected function fetchAttributes()
    {
        $info = $this->makeSignedRequest('people/~:(id,first-name,last-name,public-profile-url,email-address)', [
            'query' => [
                'format' => 'json',
            ],
        ]);

        if(isset($info['id']))
            $this->attributes['id'] = $info['id'];

        if(isset($info['firstName']) && isset($info['lastName']))
            $this->attributes['name'] = $info['firstName'] . ' ' . $info['lastName'];


        if(isset($info['publicProfileUrl']))
            $this->attributes[''] = $info['publicProfileUrl'];

        if(isset($info['emailAddress']))
            $this->attributes['email'] = $info['emailAddress'];

        return true;
    }
}
