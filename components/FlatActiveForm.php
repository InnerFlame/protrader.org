<?php
/**
 * Created by PhpStorm.
 * @author: Roman Budnitsky
 * @date: 24.10.14 13:25
 * @copyright Copyright (c) 2014 PFSOFT LLC
 *
 * Class FlatActiveForm
 */

namespace app\components;

use yii\bootstrap\ActiveForm;

class FlatActiveForm extends ActiveForm {

    public function init()
    {
        $this->fieldClass = 'app\components\FlatActiveField';
        $this->layout = 'horizontal';
        $this->fieldConfig = [
            'template' => "\n<div class=\"col-sm-12\">{input}</div>",
            'inputOptions' => [
                'placeholder' => true
            ]
        ];
        parent::init();
    }

} 