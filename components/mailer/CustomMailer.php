<?php
/**
 * Created by PhpStorm.
 * @author: Zaitsev Oleg
 * @date: 09.06.15 14:00
 * @copyright Copyright (c) 2015 PFSOFT LLC
 *
 * Class Mailer
 * @package app/components
 */


namespace app\components\mailer;

use yii;
use yii\base\Component;
use yii\base\InvalidConfigException;

class CustomMailer extends Component
{

    public static function send($mailto, $template, $params){

        $message = Yii::$app->view->renderFile(Yii::getAlias('@mail') . '/' . $template, $params);

        preg_match('/<subject>(.*)<\/subject>/i', $message, $match);
        $subject = isset($match[1]) ? $match[1] : 'Message from Client Portal.';

        $message = trim(preg_replace('/<subject>.*<\/subject>/i', '', $message));

        Yii::$app->mailer->compose()
            ->setFrom(isset(Yii::$app->params['adminEmail']) ? Yii::$app->params['adminEmail'] : '')
            ->setTo($mailto)
            ->setSubject($subject)
            ->setHtmlBody($message)
            ->send();


        return true;

    }

    /**
     * Send email from user email.
     * @param $from
     * @param $mailto
     * @param $template
     * @param $params
     * @return bool
     */
    public static function sendFrom($from,$mailto, $template, $params)
    {
        $message = Yii::$app->view->renderFile(Yii::getAlias('@mail') . '/' . $template, $params);

        preg_match('/<subject>(.*)<\/subject>/i', $message, $match);
        $subject = isset($match[1]) ? $match[1] : 'Message from Client Portal.';

        $message = trim(preg_replace('/<subject>.*<\/subject>/i', '', $message));

        Yii::$app->mailer->compose()
            ->setFrom($from)
            ->setTo($mailto)
            ->setSubject($subject)
            ->setHtmlBody($message)
            ->send();
        return true;
    }


}