<?php
namespace app\components;

use app\models\CustomModel;
use app\models\trash\Trash;
use yii\redis\ActiveRecord;

abstract class Entity extends CustomModel
{
    const BROKER = 300; //app/models/broker/Broker
    const FEATURES = 301; //app/modules/forum/models/Improvement
    const FEATURES_REQUEST = 302; //app/modules/brokers/models/FeatureRequest
    const FEATURES_CATEGORY = 303; //app/modules/forum/models/Improvement
    const FORUM_CATEGORY = 201; //app/modules/forum/models/FrmCategory
    const FORUM_COMMENT = 202; //app/modules/forum/models/FrmComment
    const FORUM_TOPIC = 203; //app/modules/forum/models/FrmTopic
    const CEO = 101; //app/components/ceo/Ceo
    const ARTICLE = 102; //app/models/articles/Articles
    const CATEGORY_ARTICLE = 112; //app\modules\articles\models\VdCategory
    const COMMUNITY_COMMENT = 103; //app/models/community/Comments
    const TRANSLATION = 104; //app/models/lang/Translations
    const PAGE = 105; //app/models/main/Pages
    const NEWS = 106; //app/models/news/News
    const CATEGORY_NEWS = 116; //app/models/news/News
    const VIDEO = 400; //app/models/video/Video
    const VIDEO_PLAYLIST = 401; //app/models/video/PlayList
    const VIDEO_CAT_PLAYLIST = 402; //app/models/video/PlayList
    const USER = 500; //app\models\user/User
    const HANDLER_ERROR_PAGE = 600; //app\components\widgets\error\models\ErrorPage
    const CODEBASE_DESCRIPTION = 700;//app\modules\codebase\models\Description
    const CODEBASE_CATEGORY = 701;//app\modules\codebase\models\Category
    const CODEBASE_FILE = 702;//app\modules\codebase\models\File
    const FAQ = 800; //app/models/main/Faq
    const FAQ_CATEGORY = 801; //app/models/main/Faq
    const SLIDE = 1001;
    const NOTIFY_EVENT = 901;
    const NOTIFY_EVENT_MESSAGE = 902;
    const NOTIFY_EVENT_QUEUE = 903;
    const NOTIFY_EVENT_SUBSCRIBE = 904;
    const NOTIFY_EVENT_USER_SETTINGS = 905;
    const NOTIFY_TEMPLATE = 906;


    const SCENARIO_DELETE = 'delete'; //app\models\user/User
    const NOT_DELETED = 0;
    const DELETED = 1;


    private static $_map = [
        self::BROKER                     => 'app\modules\brokers\models\Broker',
        self::BROKER                     => 'app\modules\brokers\models\Broker',
        self::FEATURES                   => 'app\modules\brokers\models\Improvement',
        self::FEATURES_CATEGORY          => 'app\modules\brokers\models\ImprovementsCategory',
        self::FORUM_CATEGORY             => 'app\modules\forum\models\FrmCategory',
        self::FORUM_COMMENT              => 'app\modules\forum\models\FrmComment',
        self::FORUM_TOPIC                => 'app\modules\forum\models\FrmTopic',
        self::CEO                        => 'app\components\ceo\Ceo',
        self::ARTICLE                    => 'app\modules\articles\models\Articles',
        self::CATEGORY_ARTICLE           => 'app\modules\articles\models\ArtclCategory',
        self::COMMUNITY_COMMENT          => 'app\models\community\Comments',
        self::TRANSLATION                => 'app\models\lang\Translations',
        self::PAGE                       => 'app\models\main\Pages',
        self::NEWS                       => 'app\modules\news\models\News',
        self::CATEGORY_NEWS              => 'app\modules\news\models\NewsCategory',
        self::VIDEO                      => 'app\models\video\Video',
        self::VIDEO_PLAYLIST             => 'app\models\video\Playlist',
        self::VIDEO_CAT_PLAYLIST         => 'app\models\video\VdCategory',
        self::USER                       => 'app\models\user\User',
        self::FEATURES_REQUEST           => 'app\modules\features\models\FeatureRequest',
        self::HANDLER_ERROR_PAGE         => 'app\components\widgets\error\models\ErrorPage',
        self::CODEBASE_DESCRIPTION       => 'app\modules\codebase\models\Description',
        self::CODEBASE_CATEGORY          => 'app\modules\codebase\models\Category',
        self::CODEBASE_FILE              => 'app\modules\codebase\models\File',
        self::FAQ                        => 'app\models\main\Faq',
        self::FAQ_CATEGORY               => 'app\models\main\FaqCategory',
        self::SLIDE                      => 'app\models\slide\Slide',
        self::NOTIFY_EVENT               => 'app\modules\notify\models\Event',
        self::NOTIFY_EVENT_MESSAGE       => 'app\modules\notify\models\EventMessage',
        self::NOTIFY_EVENT_QUEUE         => 'app\modules\notify\models\EventQueue',
        self::NOTIFY_EVENT_SUBSCRIBE     => 'app\modules\notify\models\EventSubscribe',
        self::NOTIFY_EVENT_USER_SETTINGS => 'app\modules\notify\models\EventUserSettings',
        self::NOTIFY_TEMPLATE            => 'app\modules\cabinet\modules\notify\models\NotifyTemplate',
    ];

    abstract public function getTypeEntity();

    abstract public function getName();

    abstract public function getViewUrl();

    /**
     * Find and return Entity by type and id
     * @param $type int
     * @param $id int
     * @return null | Entity
     */
    public static function findEntity($type, $id)
    {
        $class = self::getEntityClassByType($type);
        return $class::findOne($id);
    }

    /**
     * Search type entity in map array
     * and create Entity object
     * @param $type int
     * @return Entity | null
     */
    public static function getEntityByType($type)
    {
        if (isset(self::$_map[$type])) {
            $class = self::$_map[$type];
            return new $class;
        }
        return null;
    }

    /**
     * Search type entity in map array
     * and return namespace to class
     * @param $type int
     * @return string namespace
     */
    public static function getEntityClassByType($type)
    {
        if (isset(self::$_map[$type])) {
            $class = self::$_map[$type];
            return $class;
        }
        return null;
    }

    /**
     * Records information about deletions entity
     * If you want attach model to this behavior, make sure, your model must extend Entity
     *
     * @param bool $hard полное удаление
     * @return false|int|void
     */
    public function delete($hard = false)
    {
        if($hard){
            return parent::delete();
        }
        $this->deleted = 1;
        $this->scenario = Entity::SCENARIO_DELETE;

        if ($this->save(false)) {
            $current = Trash::find()->where([
                'user_id'   => \Yii::$app->user->identity->id,
                'entity'    => $this->getTypeEntity(),
                'entity_id' => $this->id,
            ])->one();

            \Yii::$app->session->setFlash('success', 'Deleted');

            if (!is_object($current)) {
                $model = Trash::loadEntity($this);
                $model->save();
            }
            return $this;
        }
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();

        $scenarios['delete'] = ['deleted'];

        return $scenarios;
    }

    public static function modelsForCabinet()
    {
        return self::find()->notDeleted()->orderBy('created_at DESC');
    }

    public static function getMap()
    {
        return self::$_map;
    }
}