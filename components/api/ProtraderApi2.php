<?php

namespace app\components\api;

use app\models\forms\DemoAccountForm;
use Yii;
use yii\base\Component;
use yii\base\Exception;


/**
 * Created by PhpStorm.
 * User: leoben
 * Date: 28.07.14
 * Time: 13:44
 */
class ProtraderApi2 extends Component
{

    const APIALIAS = '/ProTrader.jws?wsdl';

    const DEFAULT_SERVER = 'http://pt3.protrader.net:8090/proftrading';
    const DEFAULT_ADMIN_LOGIN = 'registation';
    const DEFAULT_ADMIN_PASS = 'registation221';

    const OPERATION_TRADING = 1;
    const OPERATION_DEPOSIT = 2;
    const OPERATION_WITHDRAW = 3;
    const OPERATION_COMMISSION = 4;
    const OPERATION_SWAP = 5;
    const OPERATION_AGENT_COMMISSION = 6;

    const REPORT_CHAMP_STATS = 'Champ stats report';

    const DEFAULT_BALANCE = 100000;
    const DEFAULT_CURRENCY = 'USD';

    private $_api;
    private $_admLogin;
    private $_admPass;

//    public $serverlabel;
    public static $lastlogin;
    public $sever_avaliable = true;

    public function __construct($url = self::DEFAULT_SERVER, $login = self::DEFAULT_ADMIN_LOGIN, $pass = self::DEFAULT_ADMIN_PASS)
    {
        // регистрация ошибок
        set_error_handler([$this, 'OtherErrorCatcher']);
        // перехват критических ошибок
        register_shutdown_function([$this, 'FatalErrorCatcher']);
        // создание буфера вывода
        ob_start();

        if(!$this->checkServerAvailible($url)) {
            $this->sever_avaliable = false;
            return $this->sever_avaliable;
        }

        $this->_api = new \SoapClient($url . self::APIALIAS, ['exceptions' => FALSE]);
        $this->_admLogin = $login;
        $this->_admPass = $pass;
        ob_end_clean();
    }

    private function getOperationName($k)
    {
        $array = [
            1 => 'OPERATION_TRADING',
            2 => 'OPERATION_DEPOSIT',
            3 => 'OPERATION_WITHDRAW',
            4 => 'OPERATION_COMMISSION',
            5 => 'OPERATION_SWAP',
            6 => 'OPERATION_AGENT_COMMISSION',
        ];

        return $array[$k];
    }

    public function registerReal(DemoAccountForm $demo_account_form, $accountName, $balance = self::DEFAULT_BALANCE, $currency = self::DEFAULT_CURRENCY, $userGroupId = 5796066, $accountType = 0)
    {
        $soap = $this->_api->registerReal(
            $this->_admLogin,   //string $login,
            $this->_admPass,    //string $password,
            $demo_account_form->login,       //string $realLogin,
            $demo_account_form->password,    // $realPassword,
            $demo_account_form->email,       //string $email,
            $demo_account_form->first_name,   //string $firstName,
            null,               //string $secondName,
            $demo_account_form->last_name,   //string $lastName,
            null,               //string $gender,
            Yii::$app->language, //string $lang,
// OLD           $this->getCountryCode( Country::getCountryById($user->profile->country, 'en') ), //string $countryId,
            $demo_account_form->country, //string $countryId,
            $demo_account_form->phone, //string $phone,
            null,               //string $phonePassword,
            null,               //string $address,
            null,               //string $city,
            null,               //string $state,
            null,               //string $zip,
            null,   //string $birthday,
            null,               //string $comments,
            null,               //string $extFields,
            $userGroupId,       //string $userGroupId,
            null,               //string $blockingStatus,
            $accountName,       //string $accountName, CFD-FX-6000
            null,               //string $accountComment,
            null,               //string $accountStatus,
            null,               //string $accountMode,
            $currency,          //string $accountCurrency,
            $demo_account_form->balance,           //string $balance,
            null,               //string $extAccFields,
            null,               //string $externalAccount,
            null,               //string $IBroker,
            null,               //string $IBrokerRate,
            null,               //string $investors,
            null,               //string $isOnePosition,
            $accountType,       //string $accountType,
            null                //string $chargePlanId
        );

        $isError = $this->ifError($soap, [$demo_account_form->login]);
        if ($isError) return $isError;

        if (preg_match('/id=(\d+)/', $soap, $resp_soap)) {
            return new ProtraderApi2Responce(true, strip_tags($soap) . ' | ' . $demo_account_form->login, ['userId' => $resp_soap[1]]);
        } else {
            return new ProtraderApi2Responce(false, $soap . ' | ' . $demo_account_form->login);
        }

    }

    public function createAccount(DemoAccountForm $user, $accountName, $ruleValues, $balance = self::DEFAULT_BALANCE, $currency = self::DEFAULT_CURRENCY, $description = 'auto')
    {
        $soap = $this->_api->createAccount(
            $this->_admLogin,       //string $login,
            $this->_admPass,        //string $password,
            $user->protrader_id,    //string $userID,
            $accountName,            //string $accountName, 'EXCH-6000'
            $currency, //string $currency,
            $balance,  //string $balance,
            null,                   //string $mode,
            null,                   //string $status,
            $description,           //string $description,
            $ruleValues,            //string $ruleValues,
            //'<instrument-list><rule name="FUNCTION_ONE_POSITION" value="true" overrided="true" /><rule name="VISIBILITY_INSTRUMENT_TYPE" value="865|980|806" overrided="true" /> <rule name="VISIBILITY_ROUTE" value="2005|839" overrided="true" /></instrument-list>',
            null,                   //string $extAccFields,
            null                    //string $type
        );

        $isError = $this->ifError($soap);
        if ($isError) return $isError;

        return new ProtraderApi2Responce(true, strip_tags($soap) . ' | ' . $user->login);
    }

    public function createPT3User(DemoAccountForm $user)
    {
        $demo_num = self::getDemoNumber();

        $registerUser = $this->registerReal($user, 'DEMO-' . $demo_num);

        return $registerUser;
    }

    public function createPT3UserDemo(DemoAccountForm $user)
    {
        $demo_num = self::getDemoNumber();

        $registerUser = $this->registerDemo($user, 'DEMO-' . $demo_num);

        return $registerUser;
    }

    function checkServerAvailible($domain)
    {
        $curlInit = curl_init($domain . self::APIALIAS);
        curl_setopt($curlInit, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($curlInit, CURLOPT_HEADER, true);
        curl_setopt($curlInit, CURLOPT_NOBODY, true);
        curl_setopt($curlInit, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($curlInit);
        curl_close($curlInit);

        if ($response){
            return true;
        }
        return false;
    }

    public function getUserInfo($login, $log = true)
    {

        $soap = $this->_api->getUserInfo(
            $this->_admLogin,       //string $login,
            $this->_admPass,        //string $password,
            $login           //string $userLogin
        );
        $isError = $this->ifError($soap, [], $log);
        if ($isError) return $isError;

        if (strripos($soap, "Can't get charging account for user") !== false) return new ProtraderApi2Responce(false, $soap, [], $log);
        $xml = new \SimpleXMLElement($soap);
        $dataArray = json_decode(json_encode((array)$xml), 1);
        if (is_array($dataArray['Accounts']['account'][0]))
            $dataArray['Accounts'] = $dataArray['Accounts']['account'];
        else
            $dataArray['Accounts'] = ['0' => $dataArray['Accounts']['account']];

        return new ProtraderApi2Responce(true, 'Received information about the user | ' . $login, $dataArray, $log);
    }

    public function accountOperation($accName, $operation, $sum, $comment = '')
    {
        $soap = $this->_api->accountOperation(
            $this->_admLogin,       //string $login,
            $this->_admPass,        //string $password,
            $accName,               //string $userLogin,
            $operation,             //string $typeId,
            $sum,                   //string $sum,
            $comment,               //string $basis,
            null                    //string $extFields
        );

        $isError = $this->ifError($soap);
        if ($isError) return $isError;

        return new ProtraderApi2Responce(true, strip_tags($soap) . ' | ' . $this->getOperationName($operation) . ' (' . $sum . ')' . ' | ' . $accName);
    }

    public function report($reportName, $beginDate, $endDate = null, $login = null, $pass = null)
    {
        $endDate = ($endDate === null) ? time() : $endDate;
        $soap = $this->_api->report(
            is_null($login)? $this->_admLogin : $login,       //string $login,
            is_null($pass)? $this->_admPass : $pass,        //string $password,
            null,                   //string $userLogin,
            99,                     //int $reportType,
            date('d-m-Y H:i', $beginDate),  //string $beginDate,
            date('d-m-Y H:i', $endDate),    //string $endDate,
            $reportName,            //string $reportName,
            null,                   //string $groupId,
            null,                   //string $instrumentGroup,
            null,                   //string $params,
            0,                   //string $isFullXml,
            null,                   //string $isNew,
            null                    //string $userGroupName
        );

        $isError = $this->ifError($soap);
        if ($isError) {
            return $isError;
        }

        $xml = new \SimpleXMLElement($soap);
        $dataArray = json_decode(json_encode((array)$xml), 1);

        return $dataArray;

//        if ($reportName == self::REPORT_CHAMP_STATS) {
//            $tmpArray = [];
//            foreach ($dataArray['report']['tr'] as $krow => $row) {
//                if ($krow) {
//                    $tmpArray[] = $row['td'];
//                }
//            }
//            $dataArray = $tmpArray;
//        }
//
//        return new ProtraderApi2Responce(true, '', $dataArray, false);
    }

    public function removeUser($idOrLogin, $removeType = 'full')
    {
        $userID = null;
        $userLogin = null;
        if (is_int($idOrLogin)) {
            $userLogin = ChampUser::model()->findByAttributes(['id_pt_user' => $idOrLogin])->member->login;
        } else {
            $userLogin = $idOrLogin;
        }
        $soap = $this->_api->removeUser(
            $this->_admLogin,       //string $login,
            $this->_admPass,        //string $password,
            $userLogin,             //string $userLogin,
            $userID,                //string $userID,
            $removeType             //string $removeType
        );

        $isError = $this->ifError($soap);
        if ($isError) return $isError;

        return new ProtraderApi2Responce(true, strip_tags($soap) . ' | ' . $userLogin);
    }

    public function getFunctions()
    {
        return $soap = $this->_api->__getFunctions();
    }

    public function setUserAccountBalance($login, $accName, $defaultBalance = self::DEFAULT_BALANCE)
    {
        $userinfo = $this->getUserInfo($login);
        if ($userinfo->isSuccess) {
            foreach ($userinfo->data['Accounts'] as $acc) {
                if ($acc['name'] == $accName) {
                    $newBalance = $defaultBalance - (float)$acc['balance'];
                    if ($newBalance >= 0) {
                        return $this->accountOperation(
                            $accName,
                            self::OPERATION_DEPOSIT,
                            $newBalance,
                            'Set account balance to ' . $defaultBalance . '.'
                        );
                    } else {
                        return $this->accountOperation(
                            $accName,
                            self::OPERATION_WITHDRAW,
                            $newBalance,
                            'Set account balance to ' . $defaultBalance . '.'
                        );
                    }
                }
            }

            return new ProtraderApi2Responce(false, 'User ' . $login . ' is no such account ' . $accName);
        }

        return $userinfo;
    }

    private function ifError($str, $param = [], $log = true)
    {
        if (preg_match('/<error\ code=["|\'](\d+)["|\']\ ?>([A-Za-z0-9_\-\s\,.;=]+)<\/error>/', $str, $resp)) {
            return new ProtraderApi2Responce(false, $resp[2] . ((count($param)) ? (' | ' . implode(' | ', $param)) : ''), ['errorCode' => (int)$resp[1]], $log);
        }

        return false;
    }

    public function getCountryCode($name)
    {
        $countries = [
            "Afghanistan"                    => "1",
            "Albania"                        => "2",
            "Algeria"                        => "3",
            "American Samoa"                 => "4",
            "Andorra"                        => "5",
            "Angola"                         => "6",
            "Anguilla"                       => "7",
            "Antarctica"                     => "8",
            "Antigua and Barbuda"            => "9",
            "Argentina"                      => "10",
            "Armenia"                        => "11",
            "Aruba"                          => "12",
            "Australia"                      => "13",
            "Austria"                        => "14",
            "Azerbaijan"                     => "15",
            "Bahamas"                        => "16",
            "Bahrain"                        => "17",
            "Bangladesh"                     => "18",
            "Barbados"                       => "19",
            "Belarus"                        => "20",
            "Belgium"                        => "21",
            "Belize"                         => "22",
            "Benin"                          => "23",
            "Bermuda"                        => "24",
            "Bhutan"                         => "25",
            "Bolivia"                        => "26",
            "Bosnia and Herzegowina"         => "27",
            "Botswana"                       => "28",
            "Bouvet island"                  => "29",
            "Brazil"                         => "30",
            "British Indian Ocean Territory" => "31",
            "Brunei Darussalam"              => "32",
            "Bulgaria"                       => "33",
            "Burkina Faso"                   => "34",
            "Burundi"                        => "35",
            "Cambodia"                       => "36",
            "Cameroon"                       => "37",
            "Canada"                         => "38",
            "Cape Verde"                     => "39",
            "Cayman Islands"                 => "40",
            "CentraL African Republic"       => "41",
            "Chad"                           => "42",
            "Chile"                          => "43",
            "China"                          => "44",
            "Christmas Island"               => "45",
            "Cocos (Keeling) Islands"        => "46",
            "Colombia"                       => "47",
            "Comoros"                        => "48",
            "Congo"                          => "49",
            "Congo"                          => "50",
            "Zaire"                          => "51",
            "Cook Islands"                   => "52",
            "Costa Rica"                     => "53",
            "Cote DIvoire"                   => "54",
            "Croatia"                        => "55",
            "Cuba"                           => "56",
            "Cyprus"                         => "57",
            "Czech Republic"                 => "58",
            "Denmark"                        => "59",
            "Djibouti"                       => "60",
            "Dominica"                       => "61",
            "Dominican Republic"             => "62",
            "East Timor"                     => "63",
            "Ecuador"                        => "64",
            "Egypt"                          => "65",
            "El Salvador"                    => "66",
            "Equatorial Guinea"              => "67",
            "Eritrea"                        => "68",
            "Estonia"                        => "69",
            "Ethiopia"                       => "70",
            "Falkland Islands (Malvinas)"    => "71",
            "Faroe Islands"                  => "72",
            "Fiji"                           => "73",
            "Finland"                        => "74",
            "France"                         => "75",
            "France, Metropolitan"           => "76",
            "French Guiana"                  => "77",
            "French Polynesia"               => "78",
            "French Southern Territories"    => "79",
            "Gabon"                          => "80",
            "Gambia"                         => "81",
            "georgia"                        => "82",
            "Germany"                        => "83",
            "Ghana"                          => "84",
            "Gibraltar"                      => "85",
            "Greece"                         => "86",
            "Greenland"                      => "87",
            "Grenada"                        => "88",
            "Guadeloupe"                     => "89",
            "Guam"                           => "90",
            "Guatemala"                      => "91",
            "Guinea"                         => "92",
            "Guinea-Bissau"                  => "93",
            "Guyana"                         => "94",
            "Haiti"                          => "95",
            "Heard and Mc Donald Islands"    => "96",
            "Honduras"                       => "97",
            "Hong Kong"                      => "98",
            "Hungary"                        => "99",
            "Iceland"                        => "100",
            "India"                          => "101",
            "Indonesia"                      => "102",
            "Iran"                           => "103",
            "Iraq"                           => "104",
            "Ireland"                        => "105",
            "Israel"                         => "106",
            "Italy"                          => "107",
            "Jamaica"                        => "108",
            "Japan"                          => "109",
            "Jordan"                         => "110",
            "Kazakhstan"                     => "111",
            "Kenya"                          => "112",
            "Kiribati"                       => "113",
            "Korea"                          => "114",
            "Kuwait"                         => "116",
            "Kyrgyzstan"                     => "117",
            "Lao"                            => "118",
            "Latvia"                         => "119",
            "Lebanon"                        => "120",
            "Lesotho"                        => "121",
            "Liberia"                        => "122",
            "Libyan Arab Jamahiriya"         => "123",
            "Liechtenstein"                  => "124",
            "Lithuania"                      => "125",
            "Luxembourg"                     => "126",
            "Macau"                          => "127",
            "Macedonia"                      => "128",
            "Madagascar"                     => "129",
            "Malawi"                         => "130",
            "Malaysia"                       => "131",
            "Maldives"                       => "132",
            "Mali"                           => "133",
            "Malta"                          => "134",
            "Marshall Islands"               => "135",
            "Martinique"                     => "136",
            "Mauritania"                     => "137",
            "Mauritius"                      => "138",
            "Mayotte"                        => "139",
            "Mexico"                         => "140",
            "Micronesia"                     => "141",
            "Moldova"                        => "142",
            "Monaco"                         => "143",
            "Mongolia"                       => "144",
            "Montserrat"                     => "145",
            "Morocco"                        => "146",
            "Mozambique"                     => "147",
            "Myanmar"                        => "148",
            "Namibia"                        => "149",
            "Nauru"                          => "150",
            "Nepal"                          => "151",
            "Netherlands"                    => "152",
            "Netherlands Antilles"           => "153",
            "New Caledonia"                  => "154",
            "New Zealand"                    => "155",
            "Nicaragua"                      => "156",
            "Niger"                          => "157",
            "Nigeria"                        => "158",
            "Niue"                           => "159",
            "Norfolk Island"                 => "160",
            "Northern Mariana Islands"       => "161",
            "Norway"                         => "162",
            "Oman"                           => "163",
            "Pakistan"                       => "164",
            "Palau"                          => "165",
            "Panama"                         => "166",
            "Papua New Guinea"               => "167",
            "Paraguay"                       => "168",
            "Peru"                           => "169",
            "Philippines"                    => "170",
            "Pitcairn"                       => "171",
            "Poland"                         => "172",
            "Portugal"                       => "173",
            "Puerto Rico"                    => "174",
            "Qatar"                          => "175",
            "Reunion"                        => "176",
            "Romania"                        => "177",
            "Russian Federation"             => "178",
            "Rwanda"                         => "179",
            "St. Helena"                     => "180",
            "Saint Kitts and Nevis"          => "181",
            "Saint Lucia"                    => "182",
            "St. Pierre and Miquelon"        => "183",
            "St. Vincent and The Grenadines" => "184",
            "Samoa"                          => "185",
            "San Marino"                     => "186",
            "Sao Tome and Principe"          => "187",
            "Saudi Arabia"                   => "188",
            "Senegal"                        => "189",
            "Seychelles"                     => "190",
            "Sierra Leone"                   => "191",
            "Singapore"                      => "192",
            "Slovakia"                       => "193",
            "Slovenia"                       => "194",
            "Solomon Islands"                => "195",
            "Somalia"                        => "196",
            "South Africa"                   => "197",
            "Spain"                          => "199",
            "Sri Lanka"                      => "200",
            "Sudan"                          => "201",
            "Suriname"                       => "202",
            "Svalbard and Jan Mayen Islands" => "203",
            "Swaziland"                      => "204",
            "Sweden"                         => "205",
            "Switzerland"                    => "206",
            "Syrian Arab Republic"           => "207",
            "Taiwan"                         => "208",
            "Tajikistan"                     => "209",
            "Tanzania"                       => "210",
            "Thailand"                       => "211",
            "Togo"                           => "212",
            "Tokelau"                        => "213",
            "Tonga"                          => "214",
            "Trinidad and Tobago"            => "215",
            "Tunisia"                        => "216",
            "Turkey"                         => "217",
            "Turkmenistan"                   => "218",
            "Turks and Caicos Islands"       => "219",
            "Tuvalu"                         => "220",
            "Uganda"                         => "221",
            "Ukraine"                        => "222",
            "United Arab Emirates"           => "223",
            "United Kingdom"                 => "224",
            "United States"                  => "225",
            "US Minor Outlying Islands"      => "226",
            "Uruguay"                        => "227",
            "Uzbekistan"                     => "228",
            "Vanuatu"                        => "229",
            "Vatican City State"             => "230",
            "Venezuela"                      => "231",
            "Viet Nam"                       => "232",
            "Virgin Islands (British)"       => "233",
            "Virgin Islands (U.S.)"          => "234",
            "Wallis and Futuna Islands"      => "235",
            "Western Sahara"                 => "236",
            "Yemen"                          => "237",
            "Yugoslavia"                     => "238",
            "Zambia"                         => "239",
            "Zimbabwe"                       => "240"
        ];

        $code = null;
        $name = trim($name);
        $code = $countries[$name];

        if ($code != null) {
            return $code;
        } else {
            return '224';
        }
    }

    public static function getDemoNumber()
    {
        $file = dirname(__FILE__) . '/protrader/demo_login_number.txt';
        $num = intval(@file_get_contents($file)) + 1;
        @file_put_contents($file, $num);

        return 1000 + $num;
    }

    public function OtherErrorCatcher($errno, $errstr)
    {
        return false;
    }

    public function FatalErrorCatcher()
    {
        $error = error_get_last();
        if (isset($error)) {
            if ($error['type'] == E_ERROR || $error['type'] == E_PARSE || $error['type'] == E_COMPILE_ERROR || $error['type'] == E_CORE_ERROR) {
                ob_end_clean();    // сбросить буфер, завершить работу буфера

                $demo_num = self::getDemoNumber();
                $message = new YiiMailMessage(Yii::app()->params['mailFrom'], Yii::app()->params['mailFrom'], 'text/html', 'UTF-8');
                $message->addTo(Yii::app()->params['mailFrom']);
                $message->from = Yii::app()->params['mailFrom'];
                $message->setSubject('WSDL Error: account not created. Server not responded.');
                $message->setBody('The user with a login - ' . self::$lastlogin . ' could not create DEMO-' . $demo_num . ' account !!! <br/><br/>');
                Yii::app()->mail->send($message);

                header("Location: /member/login/error/1");

            }
        }
    }

    public function registerDemo(DemoAccountForm $demo_account_form, $accountName)
    {
        $soap = $this->_api->registerDemo(
            $demo_account_form->login,
            $demo_account_form->password,
            $demo_account_form->email,
            $demo_account_form->first_name,
            null,
            $demo_account_form->last_name,
            null,
            null,
            null,
            null,
            'USD=100000',
            null,
            null,
            null,
            null,
            null,
            'crosschamp',
            null,
            null,
            null
        );

        $isError = $this->ifError($soap, [$demo_account_form->login]);
        if ($isError) return $isError;

        if (preg_match('/userId=(\d+)/', $soap, $resp_soap)) {
            return new ProtraderApi2Responce(true, strip_tags($soap) . ' | ' . $demo_account_form->login, ['userId' => $resp_soap[1]]);
        } else {
            return new ProtraderApi2Responce(false, $soap . ' | ' . $demo_account_form->login);
        }
    }
}

class ProtraderApi2Responce
{

    public $isSuccess = false;
    public $isFail = true;
    public $message = '';
    public $data = [];

    public function __construct($result, $message = '', $data = [], $log = true)
    {
        if ($result) {
            $this->isSuccess = true;
            $this->isFail = false;
        }
        $this->message = $message;
        $this->data = $data;

        $trace = debug_backtrace();
        array_shift($trace);

        $calledfunction = ($trace[0]['function'] == 'ifError') ? $trace[1]['function'] : $trace[0]['function'];

//        if( $log ) Yii::log( $trace[0]['object']->serverlabel . '->' . $calledfunction . ' | ' . $message, ($this->isSuccess) ? 'info' : 'error', 'ptapi2');
    }

}

class NotServiceAvailableException extends Exception {}