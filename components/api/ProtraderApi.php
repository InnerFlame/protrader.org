<?php

namespace app\components\api;

use app\models\Constants;
use Yii;
use yii\base\Component;

class ProtraderApi extends Component
{
	const PROTRADER_URL = 'http://pt3.protraderfx.net:8090/proftrading/ProTrader.jws';

	const PROTRADER_R1_URL = 'http://pt3.2.protraderfx.net:8090/proftrading/ProTrader.jws';

	const PT3_LOGIN = 'registation';
	const PT3_PASS = 'registation221';

	const PT3_DEFAULT_CURRENCY = 'USD';
	const PT3_DEFAULT_BALANCE = 100000;

	public $debug = 0;


	public function RegisterMember( Member $user )
	{
		$firstName = rawurlencode($user->real_name);
		$lastName = rawurlencode($user->last_name);
		$email = rawurlencode($user->email);
		$phone = '';

		//$countryId = $this->getCountryCode('');
		$countryId = 0;

		$accountInfo = self::PT3_DEFAULT_CURRENCY;
		$balance = self::PT3_DEFAULT_BALANCE;

	    $hedging = 'false';

		$lng_reg = Yii::app()->language;

		$curlang = Yii::app()->language;

		$groupID = 0;

		// set group id statically
		$groupID_str = '&userGroupId=' . '0';
		//$groupID_str = '';

		$demo_num = $this->_getDemoNumber();
		//$real_login = 'DEMO-' . $demo_num;
		//$real_pass = _generatePassword( 10 );

		$real_login = $user->login;
		$real_pass = $user->password ? $user->password : $user->generatePassword( 10 );

		$real_login = rawurlencode( $real_login );
		$real_pass_raw = $real_pass;
		$real_pass = rawurlencode( $real_pass );

		$admin_login = self::PT3_LOGIN;
		$admin_pass = self::PT3_PASS;

		$multiple = 'CFD-FX-' . $demo_num;

		$servers_list = array(
			self::PROTRADER_URL,
			self::PROTRADER_R1_URL,
		);
		$server_name = $servers_list[0];

		$soap = $server_name.'?method=registerReal&realLogin='.$real_login.
            '&realPassword='.$real_pass.
            '&login='.$admin_login.
            '&password='.$admin_pass.
            '&lang='.$lng_reg.
            '&email='.$email.
            '&firstName='.$firstName.
            '&lastName='.$lastName.
            '&countryId='.$countryId.
            '&phone='.$phone.
            '&accountName='.$multiple.
            '&accountCurrency='.$accountInfo.
            '&accountType=0&groupID='.$groupID.
            '&balance='.$balance.$groupID_str;

		// Send SOAP data to server to register DEMO account

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $soap);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
		curl_setopt($ch, CURLOPT_TIMEOUT, 15);
		$resp = curl_exec($ch);

		if( $this->debug )
		{
			mail( 'devartc@gmail.com', 'register_test_1', $soap . "\n\nresp:" . $resp . '!' );
		}

		$resp = strip_tags($resp);

		// get user id created
		$u_created_id = 0;
		if( preg_match( "#was registered sucessfully with id=(\d+)#i", $resp, $match ) )
		{
			$u_created_id = intval( $match[1] );
		}

		$cnt = 1;
		while( !$u_created_id )
		{
			if( isset( $servers_list[$cnt] ) )
			{
				$server_name = $servers_list[$cnt];
			}
			else
			{
				break;
			}

			$soap = $server_name.'?method=registerReal&realLogin='.$real_login.'&realPassword='.$real_pass.'&login='.$admin_login.'&password='.$admin_pass.'&lang='.$lng_reg.'&email='.$email.'&firstName='.$firstName.'&lastName='.$lastName.'&countryId='.$countryId.'&phone='.$phone.'&accountName='.$multiple.'&accountCurrency='.$accountInfo.'&groupID='.$groupID.'&balance='.$balance.$groupID_str;

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $soap);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
			curl_setopt($ch, CURLOPT_TIMEOUT, 15);
			$resp = curl_exec($ch);

			$resp = strip_tags($resp);

			$u_created_id = 0;
			if( preg_match( "#was registered sucessfully with id=(\d+)#i", $resp, $match ) )
			{
				$u_created_id = intval( $match[1] );
			}

			if( $this->debug )
			{
				mail( 'devartc@gmail.com', 'register_test_11', $soap . "\n\nresp:" . $resp . '!' );
			}

			$cnt++;
		}

		if( $u_created_id )
		{
			// register account
			$admin_login2 = self::PT3_LOGIN;
			$admin_pass2 = self::PT3_PASS;

			$account_name_open = 'EXCH-' . $demo_num;

			$soap2 = $server_name . '?method=createAccount&login='.$admin_login2.'&password='.$admin_pass2.'&userID='.$u_created_id.'&accountName='.$account_name_open.'&currency='.$accountInfo.'&balance='.$balance;
			//$soap2.= '&description=auto&ruleValues=%26lt;instrument-list%26gt;%26lt;rule%20name=%26quot;FUNCTION_ONE_POSITION%26quot;%20value=%26quot;true%26quot;%20overrided=%26quot;true%26quot;%20/%26gt;%26lt;rule%20name=%26quot;VISIBILITY_INSTRUMENT_TYPE%26quot;%20value=%26quot;865|980|806%26quot;%20overrided=%26quot;true%26quot;%20/%26gt;%26lt;rule%20name=%26quot;VALUE_LEVERAGE%26quot;%20value=%26quot;100000%26quot;%20overrided=%26quot;true%26quot;/%26gt;%26lt;/instrument-list%26gt;';
			$soap2.= '&description=auto&ruleValues=%26lt;instrument-list%26gt;%26lt;rule%20name=%26quot;FUNCTION_ONE_POSITION%26quot;%20value=%26quot;true%26quot;%20overrided=%26quot;true%26quot;%20/%26gt;%26lt;rule%20name=%26quot;VISIBILITY_INSTRUMENT_TYPE%26quot;%20value=%26quot;865|980|806%26quot;%20overrided=%26quot;true%26quot;%20/%26gt;%20%26lt;rule%20name=%26quot;VISIBILITY_ROUTE%26quot;%20value=%26quot;2005|839%26quot;%20overrided=%26quot;true%26quot;%20/%26gt;%26lt;/instrument-list%26gt;';

			$ch2 = curl_init();
			curl_setopt($ch2, CURLOPT_URL, $soap2);
			curl_setopt($ch2, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch2, CURLOPT_CONNECTTIMEOUT, 15);
			curl_setopt($ch2, CURLOPT_TIMEOUT, 15);
			$resp2 = curl_exec($ch2);

			//$resp2 = strip_tags($resp2);
		}

		//mail( 'devartc@gmail.com', 'req1', $soap );
		//mail( 'devartc@gmail.com', 'req2', $soap2 );

  		//mail( 'devartc@gmail.com', 'resp1', $resp );

  		if( $this->debug )
		{
			mail( 'devartc@gmail.com', 'register_test_2', $soap2 . "\n\nresp:" . $resp2 );
		}

		if( !$u_created_id )
		{
			return false;
		}
		else if( !stristr( $resp2, 'error' ) && !stristr( $resp2, 'fault' ) )
		{
			$user->isNewRecord = false;
			$user->saveAttributes( array( 'protrader_id' => $u_created_id ) );
			//$user->protrader_id = $u_created_id;

			// send mail about platform registration
			$template = MailTemplate::load( 'pt3_account_created' );
			if ( $template )
			{
				$data = array(
					'login' => $user->login,
					'pass' => $real_pass_raw,
					'email' => $user->email,
				);
				$template->send( $user->email, $data );
			} else {
				throw new CHttpException(404, "MailTamplate 'pt3_account_created' is not found");
			}

		    return true;
		}
		else
		{
		    return false;
		}
	}

	public function getBalance( Member $user, $cnt = 0 )
	{
		$servers_list = array(
			self::PROTRADER_URL,
			self::PROTRADER_R1_URL,
		);

		if( !isset( $servers_list[$cnt] ) )
		{
			return false;
		}

		$server_name = $servers_list[$cnt];

		$admin_login = self::PT3_LOGIN;
		$admin_pass = self::PT3_PASS;

		$soap = $server_name.'?method=getUserInfo&login='.$admin_login.'&password='.$admin_pass.'&userLogin='.rawurlencode($user->login);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $soap);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		$resp = curl_exec($ch);

		$resp_xml = htmlspecialchars_decode( strip_tags( $resp ) );
		$xml = simplexml_load_string( $resp_xml );

		$list = false;

		if( $xml->Accounts && $xml->Accounts->account )
		{
			$list = array();
			foreach( $xml->Accounts->account as $account )
			{
				$list[] = array(
					'id' => (int) $account->id,
					'name' => (string) $account->name,
					'currency' => (string) $account->currency,
					'created' => (string) $account->created,
					'balance' => (float) $account->balance,
					'equity' => (float) $account->equity,
				);
			}
		}

		if( !$list )
		{
			$cnt++;
			return $this->getBalance( $user, $cnt );
		}

		return $list;
	}

	public function BalanceRefill( Member $user, $account_name, $account_currency, $cnt = 0 )
	{
		$servers_list = array(
			self::PROTRADER_URL,
			self::PROTRADER_R1_URL,
		);

		if( !isset( $servers_list[$cnt] ) )
		{
			return false;
		}

		$server_name = $servers_list[$cnt];

		$admin_login = self::PT3_LOGIN;
		$admin_pass = self::PT3_PASS;

		$soap = $server_name.'?method=accountOperation
		    &login='.$admin_login.'
		    &password='.$admin_pass.'
		    &typeId=2
		    &sum='.self::PT3_DEFAULT_BALANCE.'
		    &userLogin='.rawurlencode($account_name).'
		    &basis='.rawurlencode($account_currency);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $soap);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		$resp = curl_exec($ch);

		return true;
	}

	public function UpdateMember( Member $member, $cnt = 0 )
	{
		if( !$member->protrader_id )
		{
			return false;
		}

		$servers_list = array(
			self::PROTRADER_URL,
			self::PROTRADER_R1_URL,
		);

		if( !isset( $servers_list[$cnt] ) )
		{
			return false;
		}

		$server_name = $servers_list[$cnt];

		$admin_login = self::PT3_LOGIN;
		$admin_pass = self::PT3_PASS;

		$soap = $server_name.'?method=editUser&login='.$admin_login.'&password='.$admin_pass.'&userId='.$member->protrader_id.'&lastName='.rawurlencode($member->last_name).'&firstName='.rawurlencode($member->real_name).'&email='.rawurlencode($member->email);
		if( $member->password )
		{
			$soap .= '&newPassword=' . rawurlencode($member->password);
		}

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $soap);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		$resp = curl_exec($ch);

		// todo: parse response ?

		return true;
	}

	public function DeleteMember( Member $member, $cnt = 0 )
	{
		if( !$member->protrader_id )
		{
			return false;
		}

		$servers_list = array(
			self::PROTRADER_URL,
			self::PROTRADER_R1_URL,
		);

		if( !isset( $servers_list[$cnt] ) )
		{
			return false;
		}

		$server_name = $servers_list[$cnt];

		$admin_login = self::PT3_LOGIN;
		$admin_pass = self::PT3_PASS;

		$soap = $server_name.'?method=removeUser&login='.$admin_login.'&password='.$admin_pass.'&userId='.$member->protrader_id.'&removeType=full';

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $soap);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		$resp = curl_exec($ch);

		$member->isNewRecord = false;
		$member->saveAttributes( array( 'protrader_id' => 0 ) );

		// todo: parse response ?

		return true;
	}

	private function _getDemoNumber()
	{
		$file = dirname( __FILE__ ) . '/protrader/demo_login_number.txt';
		$num = intval( @file_get_contents( $file ) ) + 1;
		@file_put_contents( $file, $num );

		return 1000 + $num;
	}

	public function getCountryCode( $name )
	{
	    $countries = array (
	        "Afghanistan" => "1",
	        "Albania" => "2",
	        "Algeria" => "3",
	        "American Samoa" => "4",
	        "Andorra" => "5",
	        "Angola" => "6",
	        "Anguilla" => "7",
	        "Antarctica" => "8",
	        "Antigua and Barbuda" => "9",
	        "Argentina" => "10",
	        "Armenia" => "11",
	        "Aruba" => "12",
	        "Australia" => "13",
	        "Austria" => "14",
	        "Azerbaijan" => "15",
	        "Bahamas" => "16",
	        "Bahrain" => "17",
	        "Bangladesh" => "18",
	        "Barbados" => "19",
	        "Belarus" => "20",
	        "Belgium" => "21",
	        "Belize" => "22",
	        "Benin" => "23",
	        "Bermuda" => "24",
	        "Bhutan" => "25",
	        "Bolivia" => "26",
	        "Bosnia and Herzegowina" => "27",
	        "Botswana" => "28",
	        "Bouvet island" => "29",
	        "Brazil" => "30",
	        "British Indian Ocean Territory" => "31",
	        "Brunei Darussalam" => "32",
	        "Bulgaria" => "33",
	        "Burkina Faso" => "34",
	        "Burundi" => "35",
	        "Cambodia" => "36",
	        "Cameroon" => "37",
	        "Canada" => "38",
	        "Cape Verde" => "39",
	        "Cayman Islands" => "40",
	        "CentraL African Republic" => "41",
	        "Chad" => "42",
	        "Chile" => "43",
	        "China" => "44",
	        "Christmas Island" => "45",
	        "Cocos (Keeling) Islands" => "46",
	        "Colombia" => "47",
	        "Comoros" => "48",
	        "Congo" => "49",
	        "Congo" => "50",
	        "Zaire" => "51",
	        "Cook Islands" => "52",
	        "Costa Rica" => "53",
	        "Cote DIvoire" => "54",
	        "Croatia" => "55",
	        "Cuba" => "56",
	        "Cyprus" => "57",
	        "Czech Republic" => "58",
	        "Denmark" => "59",
	        "Djibouti" => "60",
	        "Dominica" => "61",
	        "Dominican Republic" => "62",
	        "East Timor" => "63",
	        "Ecuador" => "64",
	        "Egypt" => "65",
	        "El Salvador" => "66",
	        "Equatorial Guinea" => "67",
	        "Eritrea" => "68",
	        "Estonia" => "69",
	        "Ethiopia" => "70",
	        "Falkland Islands (Malvinas)" => "71",
	        "Faroe Islands" => "72",
	        "Fiji" => "73",
	        "Finland" => "74",
	        "France" => "75",
	        "France, Metropolitan" => "76",
	        "French Guiana" => "77",
	        "French Polynesia" => "78",
	        "French Southern Territories" => "79",
	        "Gabon" => "80",
	        "Gambia" => "81",
	        "georgia" => "82",
	        "Germany" => "83",
	        "Ghana" => "84",
	        "Gibraltar" => "85",
	        "Greece" => "86",
	        "Greenland" => "87",
	        "Grenada" => "88",
	        "Guadeloupe" => "89",
	        "Guam" => "90",
	        "Guatemala" => "91",
	        "Guinea" => "92",
	        "Guinea-Bissau" => "93",
	        "Guyana" => "94",
	        "Haiti" => "95",
	        "Heard and Mc Donald Islands" => "96",
	        "Honduras" => "97",
	        "Hong Kong" => "98",
	        "Hungary" => "99",
	        "Iceland" => "100",
	        "India" => "101",
	        "Indonesia" => "102",
	        "Iran" => "103",
	        "Iraq" => "104",
	        "Ireland" => "105",
	        "Israel" => "106",
	        "Italy" => "107",
	        "Jamaica" => "108",
	        "Japan" => "109",
	        "Jordan" => "110",
	        "Kazakhstan" => "111",
	        "Kenya" => "112",
	        "Kiribati" => "113",
	        "Korea" => "114",
	        "Kuwait" => "116",
	        "Kyrgyzstan" => "117",
	        "Lao" => "118",
	        "Latvia" => "119",
	        "Lebanon" => "120",
	        "Lesotho" => "121",
	        "Liberia" => "122",
	        "Libyan Arab Jamahiriya" => "123",
	        "Liechtenstein" => "124",
	        "Lithuania" => "125",
	        "Luxembourg" => "126",
	        "Macau" => "127",
	        "Macedonia" => "128",
	        "Madagascar" => "129",
	        "Malawi" => "130",
	        "Malaysia" => "131",
	        "Maldives" => "132",
	        "Mali" => "133",
	        "Malta" => "134",
	        "Marshall Islands" => "135",
	        "Martinique" => "136",
	        "Mauritania" => "137",
	        "Mauritius" => "138",
	        "Mayotte" => "139",
	        "Mexico" => "140",
	        "Micronesia" => "141",
	        "Moldova" => "142",
	        "Monaco" => "143",
	        "Mongolia" => "144",
	        "Montserrat" => "145",
	        "Morocco" => "146",
	        "Mozambique" => "147",
	        "Myanmar" => "148",
	        "Namibia" => "149",
	        "Nauru" => "150",
	        "Nepal" => "151",
	        "Netherlands" => "152",
	        "Netherlands Antilles" => "153",
	        "New Caledonia" => "154",
	        "New Zealand" => "155",
	        "Nicaragua" => "156",
	        "Niger" => "157",
	        "Nigeria" => "158",
	        "Niue" => "159",
	        "Norfolk Island" => "160",
	        "Northern Mariana Islands" => "161",
	        "Norway" => "162",
	        "Oman" => "163",
	        "Pakistan" => "164",
	        "Palau" => "165",
	        "Panama" => "166",
	        "Papua New Guinea" => "167",
	        "Paraguay" => "168",
	        "Peru" => "169",
	        "Philippines" => "170",
	        "Pitcairn" => "171",
	        "Poland" => "172",
	        "Portugal" => "173",
	        "Puerto Rico" => "174",
	        "Qatar" => "175",
	        "Reunion" => "176",
	        "Romania" => "177",
	        "Russian Federation" => "178",
	        "Rwanda" => "179",
	        "St. Helena" => "180",
	        "Saint Kitts and Nevis" => "181",
	        "Saint Lucia" => "182",
	        "St. Pierre and Miquelon" => "183",
	        "St. Vincent and The Grenadines" => "184",
	        "Samoa" => "185",
	        "San Marino" => "186",
	        "Sao Tome and Principe" => "187",
	        "Saudi Arabia" => "188",
	        "Senegal" => "189",
	        "Seychelles" => "190",
	        "Sierra Leone" => "191",
	        "Singapore" => "192",
	        "Slovakia" => "193",
	        "Slovenia" => "194",
	        "Solomon Islands" => "195",
	        "Somalia" => "196",
	        "South Africa" => "197",
	        "Spain" => "199",
	        "Sri Lanka" => "200",
	        "Sudan" => "201",
	        "Suriname" => "202",
	        "Svalbard and Jan Mayen Islands" => "203",
	        "Swaziland" => "204",
	        "Sweden" => "205",
	        "Switzerland" => "206",
	        "Syrian Arab Republic" => "207",
	        "Taiwan" => "208",
	        "Tajikistan" => "209",
	        "Tanzania" => "210",
	        "Thailand" => "211",
	        "Togo" => "212",
	        "Tokelau" => "213",
	        "Tonga" => "214",
	        "Trinidad and Tobago" => "215",
	        "Tunisia" => "216",
	        "Turkey" => "217",
	        "Turkmenistan" => "218",
	        "Turks and Caicos Islands" => "219",
	        "Tuvalu" => "220",
	        "Uganda" => "221",
	        "Ukraine" => "222",
	        "United Arab Emirates" => "223",
	        "United Kingdom" => "224",
	        "United States" => "225",
	        "US Minor Outlying Islands" => "226",
	        "Uruguay" => "227",
	        "Uzbekistan" => "228",
	        "Vanuatu" => "229",
	        "Vatican City State" => "230",
	        "Venezuela" => "231",
	        "Viet Nam" => "232",
	        "Virgin Islands (British)" => "233",
	        "Virgin Islands (U.S.)" => "234",
	        "Wallis and Futuna Islands" => "235",
	        "Western Sahara" => "236",
	        "Yemen" => "237",
	        "Yugoslavia" => "238",
	        "Zambia" => "239",
	        "Zimbabwe" => "240"
	    );

	    $code = NULL;
	    $name = trim($name);
	    $code = $countries[$name];

	    if($code != NULL){
	        return $code;
	    } else {
	        return '224';
	    }
	}

}