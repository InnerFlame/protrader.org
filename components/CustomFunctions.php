<?php
/**
 * Created by Zaitsev Oleg.
 * User: php
 * Date: 18.06.15
 * Time: 10:33
 */

namespace app\components;


use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;

class CustomFunctions extends Component
{
    /**
     * Return array of models sorted by attribute
     *
     * @param mixed $models
     * @param string $attribute
     * @param string $order
     * @return mixed $sort_models
     */
    public function sortModelsArray($models, $attribute, $order = SORT_DESC){

        $keys = [];

        foreach($models as $_model){
            if(isset($_model->$attribute))
                $keys[] = $_model->$attribute;
        }

        if($order == SORT_DESC || $order == 'SORT_DESC'){ rsort($keys); }else{ sort($keys); }

        $sort_models = [];

        foreach($keys as $_key){
            foreach($models as $_model){
                if(isset($_model->$attribute)){
                    if($_model->$attribute == $_key) $sort_models[] = $_model;
                }
            }
        }

        return $sort_models ? $sort_models : $models;

    }

    public function groupModelsByAttr($_attr, $_models)
    {

        $_output = [];
        $_output_keys = [];

        if(count($_models) > 0)
            foreach($_models as $key => $_model){
               if(count($_model) > 0)
                   foreach($_model as $_key_attr => $_value)
                   {
                        if($_key_attr == $_attr)
                        {
                            if(!isset($_output_keys[$_model[$_attr]]))
                            {
                                //echo '<br>';
                               // var_dump($_output_keys);
                               // echo '<br>';
                               // echo $key . ' ' . $_key_attr . ' ' . $_model[$_attr] .  '<br>';

                                $_output[] = $_model;
                                $_output_keys[$_model[$_attr]] = true;
                                foreach($_models as $_key_list => $_model_list)
                                {
                                    if($_key_list <> $key && isset($_model_list[$_attr]))
                                    {
                                        if($_model_list[$_attr] == $_model[$_attr])
                                        {
                                            //echo '-----' .$_attr . ' ' . $_model_list[$_attr] . '==' . $_model[$_attr] .'<br>';
                                            $_output[] = $_model_list;
                                        }
                                    }
                                }
                            }
                        }
                   }
            }

        //echo '<br>';
       // var_dump($_output); exit;

        return $_output;

    }

    public function cleanText($string)
    {
        if(is_array($string)) {
            $arr = [];
            foreach ($string as $key => $val) {
                $arr[$key] = $val;
            }
            return $arr;
        }

        return $string;
    }

    /**
     * Return full dump info
     *
     * @param mixed $data
     * @return mixed
     */

    public static function dump($data, $exit = true){
        echo "<h1>File:".debug_backtrace()[0]['file'];
        echo "<br>Line:".debug_backtrace()[0]['line'];
        echo"<h1>Dump:</h1><font size='5'>";
        echo"<pre>";
        echo nl2br(var_dump($data));
        echo"</font>";
        if($exit) exit;
    }

    /**
     * @param string $title
     * @param integer $size
     * @return string
     */
    public function getWrap($title, $size = 27){
        $words = explode(' ', $title);
        $output = '';
        if(count($words)) {
            foreach ($words as $key=> $_w) $words[$key] = wordwrap($_w, $size, "<br>", 1);
            $output = implode(' ', $words);
        }

        return $output;
    }

    function crc16($string) {
        $crc = 0xFFFF;
        for ($x = 0; $x < strlen ($string); $x++) {
            $crc = $crc ^ ord($string[$x]);
            for ($y = 0; $y < 8; $y++) {
                if (($crc & 0x0001) == 0x0001) {
                    $crc = (($crc >> 1) ^ 0xA001);
                } else { $crc = $crc >> 1; }
            }
        }
        return $crc;
    }

    function crc32($string) {

        function __crc32_reflect($ref, $ch) {        // Reflects CRC bits in the lookup table
            $value=0;

            // Swap bit 0 for bit 7, bit 1 for bit 6, etc.
            for($i=1;$i<($ch+1);++$i) {
                if($ref & 1) $value |= (1 << ($ch-$i));
                $ref = (($ref >> 1) & 0x7fffffff);
            }
            return $value;
        }


            $_crc32_table = array();        // Lookup table array

            // Builds lookup table array
            // This is the official polynomial used by
            // CRC-32 in PKZip, WinZip and Ethernet.
            $polynomial = 0x04c11db7;

            // 256 values representing ASCII character codes.
            for($i=0;$i <= 0xFF;++$i) {
                $_crc32_table[$i]=(__crc32_reflect($i,8) << 24);
                for($j=0;$j < 8;++$j) {
                    $_crc32_table[$i]=(($_crc32_table[$i] << 1) ^
                        (($_crc32_table[$i] & (1 << 31))?$polynomial:0));
                }
                $_crc32_table[$i] = __crc32_reflect($_crc32_table[$i], 32);
            }


            // Once the lookup table has been filled in by the two functions above,
            // this function creates all CRCs using only the lookup table.

            // You need unsigned variables because negative values
            // introduce high bits where zero bits are required.
            // PHP doesn't have unsigned integers:
            // I've solved this problem by doing a '&' after a '>>'.

            // Start out with all bits set high.
            $crc=0xffffffff;
            $len=strlen($string);

            // Perform the algorithm on each character in the string,
            // using the lookup table values.
            for($i=0;$i < $len;++$i) {
                $crc=(($crc >> 8) & 0x00ffffff) ^ $_crc32_table[($crc & 0xFF) ^ ord($string{$i})];
            }

            // Exclusive OR the result with the beginning value.
            return $crc ^ 0xffffffff;
        
    }

    public function addEmptyToArr($_arr)
    {
        $_result = ['' => ''];
        if(count($_arr) > 0)
                foreach($_arr as $key => $value)
                            $_result[$key] = $value;
        return $_result;
    }

    public function  getIndexFromStr($str, $index = 0, $separator = ' ')
    {
       $_arr = explode($separator, $str);
       if(isset($_arr[$index])) return  $_arr[$index];
       return null;
    }

    public function clearArrAttr(&$model, &$attrArr, $clear = '')
    {

      if(count($model) > 0)
          foreach($model as $_attr => $val)
              if(count($attrArr) > 0)
                foreach($attrArr as $_clear_attr)
                    if($_attr == $_clear_attr) $model[$_clear_attr] = $clear;

    }

    public function makeStringKeyFromArr($arr)
    {
        $key = str_replace(['"', '\'', ':'], '', json_encode($arr));
        return $key;
    }

    public static function getShortLanguage()
    {
        if(isset(Yii::$app->language))
            return Yii::$app->language == 'ru-RU' ? 'ru' : 'en';
    }

}