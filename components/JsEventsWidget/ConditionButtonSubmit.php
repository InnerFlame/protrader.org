<?php
namespace app\components\JsEventsWidget;

use yii\bootstrap\Widget;

/**
 * Created by PhpStorm.
 * Syntax highlighting plugin, fo the articles
 * <a href="https://highlightjs.org">highlight</a>
 * User: B.Pavel
 * Date: 26.01.2016
 * Time: 12:26
 */
class ConditionButtonSubmit extends Widget
{
    public $changed_category_id;
    public $button_id_save;
    public $form_id;
    public $clear_field_id;
    public $params;



    public function run()
    {

        return $this->registerPlugin();

    }

    protected function registerPlugin()
    {

        $view = $this->getView();

        $js = "  jQuery(document).ready(function(){";


        $js .= "     var flag_category_id_changed = false;

                     $('" . $this->changed_category_id . "').change(function(){

                             flag_category_id_changed = true;
                                console.log(flag_category_id_changed);
                     });";


        /**
         * part where we choose do we need just submit or need open modal window
         */

        $js .= " $('" . $this->button_id_save . "').click(function(e) {

                if(flag_category_id_changed){

                    $('#event_change_category').modal();

                }
                else
                {
                    $('" . $this->form_id . "').submit();
                }

            });";


        /**
         * part where we just submit the form if answer is NO
         */

        $js .= " $( '#no' ).on( 'click', function() {

                 $('" . $this->form_id . "').submit();

            });";


        /**
         * part where we just submit the form if answer is YES and clear field
         */

        $js .= "  $( '#yes' ).on( 'click', function() {

                document.getElementById('" . $this->clear_field_id . "').value = '';

                //now only for forum categories
                document.getElementById('clear_canonical').value = 'true';

                 $('" . $this->form_id . "').submit();

            })";


        $js .= " });";

        $view->registerJs($js);

        return \Yii::$app->getView()->render('@app/components/JsEventsWidget/views/modal_form_change_category.twig', ['params' => $this->params]);
    }
}