<?php
/**
 * Created by PhpStorm.
 * @author: Roman Budnitsky
 * @date: 17.11.14 10:59
 * @copyright Copyright (c) 2014 PFSOFT LLC
 *
 * Class DynamicModelBehaviors
 */

namespace app\components;

use app\models\user\User;
use yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use app\models\company\Company;

class DynamicBehaviors extends Behavior {

    const EVENT_AFTER_FIND = 'dynamicBehaviorAfterFind';
    const EVENT_BEFORE_INSERT = 'dynamicBehaviorBeforeInsert';
    const EVENT_AFTER_INSERT = ActiveRecord::EVENT_AFTER_INSERT;//'dynamicBehaviorAfterInsert';
    const EVENT_AFTER_UPDATE = ActiveRecord::EVENT_AFTER_UPDATE;
    const EVENT_AFTER_DELETE = ActiveRecord::EVENT_AFTER_DELETE;

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_FIND => 'afterFind',
            //ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeInsert'
        ];
    }

    public function afterFind()
    {
        $this->initDynamicBehaviors();
        $this->owner->trigger(self::EVENT_AFTER_FIND);
    }

    /*public function afterInsert()
    {
        $this->initDynamicBehaviors();
        $this->owner->trigger(self::EVENT_AFTER_INSERT);
    }*/

    public function beforeInsert($event)
    {
        $this->initDynamicBehaviors();
        $this->owner->trigger(self::EVENT_BEFORE_INSERT, $event);
    }

    public function initDynamicBehaviors()
    {
        if($this->owner instanceof User) {
            //$this->attachBehaviors($this->owner->company);
        }
    }

} 