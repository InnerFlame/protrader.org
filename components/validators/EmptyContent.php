<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 * Date: 29.02.2016
 * Time: 12:37
 */

namespace app\components\validators;


use yii\validators\Validator;

class EmptyContent extends Validator
{
    public $message = 'Content cannot be blank.';

    public function validateAttribute($model, $attribute)
    {
//        if (empty(strip_tags($model->$attribute))) {
//            $this->addError($model, $attribute, $this->message);
//        }
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        $message = json_encode($this->message, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        return "
        if(value.replace(/<\\/?[^>]+>/gi, '').trim().length == 0 && $(value).find('img').length == 0 && $(value).find('iframe').length == 0){
            messages.push($message);
        }";
    }


}