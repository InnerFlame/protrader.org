<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 * Date: 21.03.2016
 * Time: 14:37
 */

namespace app\components\validators;

use yii\validators\Validator;

class MaxLength extends Validator
{
    public $message = 'Content should contain at most {count} characters';

    public $count = 3000;

    public function validateAttribute($model, $attribute)
    {
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        $this->message = \Yii::t('app', $this->message, ['count' => $this->count]);
        $message = json_encode($this->message, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $count = json_encode($this->count, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        return "
        if(value.replace(/<\\/?[^>]+>/gi, '').trim().length > $count){
            messages.push($message);
        }";
    }
}