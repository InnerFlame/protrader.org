<?php


namespace app\components\customQuery;

use yii;
use yii\db\ActiveRecord;
use yii\db\ActiveQuery;


/**
 * Class CustomQueryActiveRecord
 * @package app\components\customQuery
 */
class CustomQueryActiveRecord extends ActiveRecord
{
    /**
     * @return CustomQuery
     */
    public static function find()
    {
        return new CustomQuery(get_called_class());
    }

    /**
     *
     */
    public function deleteModel(){
//        $this->deleted = date('U');
//        return $this->save();
    }
}

/**
 * Class CustomQuery
 * @package app\components\customQuery
 */
class CustomQuery extends ActiveQuery
{

    /**
     * @return $this
     */
    public function notdeleted() {

        return $this->andWhere('deleted = 0');

    }
}