<?php
/**
 * Created by PhpStorm.
 * User: php
 * Date: 07.05.15
 * Time: 9:56
 */

namespace app\components;

use yii\base\Widget;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\helpers\Json;
use dosamigos\multiselect\MultiSelect;
use dosamigos\multiselect\MultiSelectAsset;

class MultiSelectCustom extends MultiSelect{

    public function init()
    {
        $this->options['style'] = 'display:none;';
        if(!count($this->data)) $this->data = [''];
        parent::init();
    }

    /**
     * Registers MultiSelect Bootstrap plugin and the related events
     */
    protected function registerPlugin()
    {
        $view = $this->getView();

        MultiSelectAsset::register($view);

        $id = $this->options['id'];

        $options = [
                    'includeSelectAllOption' => isset($this->clientOptions['includeSelectAllOption']) ? $this->clientOptions['includeSelectAllOption'] : true,
                    'numberDisplayed' => isset($this->clientOptions['numberDisplayed']) ? $this->clientOptions['numberDisplayed'] : 2,
                    'buttonContainer' => isset($this->clientOptions['buttonContainer']) ? $this->clientOptions['buttonContainer'] : '<div class="form-group"/>',
                    'buttonClass' => isset($this->clientOptions['buttonClass']) ? $this->clientOptions['buttonClass'] : 'form-control',
                    'nonSelectedText' => isset($this->clientOptions['nonSelectedText']) ? $this->clientOptions['nonSelectedText'] : 'None selected',
                    'allSelectedText' => isset($this->clientOptions['allSelectedText']) ? $this->clientOptions['allSelectedText'] : 'All selected',
                    'onDropdownHide' => isset($this->clientOptions['onDropdownHide']) ?
                        new JsExpression($this->clientOptions['onDropdownHide']) : new JsExpression('function(event){}'),
                    'onDropdownShow' => isset($this->clientOptions['onDropdownShow']) ?
                        new JsExpression($this->clientOptions['onDropdownShow']) : new JsExpression('function(event){}'),
                    'onChange' => isset($this->clientOptions['onChange']) ?
                        new JsExpression($this->clientOptions['onChange']) : new JsExpression('function(event){}'),
                ];


        $options =  Json::encode($options);

        $js = "jQuery('#$id').multiselect($options);";
        $view->registerJs($js);
    }
}
