<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2016
 */

namespace app\components;


use app\models\setting\SettingsCustomField;
use yii\base\Event;
use yii\helpers\VarDumper;

class HandleRequest
{
    /**
     * Загоняет настройки конфига в params
     * @param Event $event
     */
    public static function beforeRequest(Event $event)
    {
        $fields = SettingsCustomField::find()->with('data')->all();
        foreach($fields as $field){
            if($field && $field->settingsCustomValue){
                \Yii::$app->params[$field->alias] = $field->settingsCustomValue->value;
            }
        }
    }
}