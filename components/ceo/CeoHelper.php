<?php

namespace app\components\ceo;

use app\models\user\UserIdentity;
use Yii;
use yii\base\Model;
use yii\helpers\BaseUrl;

/**
 * This is the model class for table "ceo".
 *
 * @property integer $id
 * @property string $entity
 * @property integer $entity_id
 * @property string $title
 * @property string $description
 * @property string $keywords
 * @property string $h1
 * @property string $canonical
 */
class CeoHelper extends Model
{
    const CATEGORY_INDEX = 2;

    public static function getCeoData($attribute)
    {
        $controller = ucfirst(explode('/', BaseUrl::current())[1]);

        $controller_name = parse_url($controller);
        if (isset($controller_name['path']))
            $controller_name = $controller_name['path'];

        //only for features because it is in module brokers
        if (isset(explode('/', BaseUrl::current())[2]) && ucfirst(explode('/', BaseUrl::current())[2]) === 'Features')
            $controller_name = 'Features';

        $page_number = false;

        if (isset($_GET['page']))
            if ($_GET['page'] !== '1')
                $page_number = str_replace('=', ' ', $_GET['page']);

        switch ($controller_name) {
            case 'Articles':
                return self::getCeoArticles($attribute, $page_number, $controller_name);
            case 'Forum':
                return self::getCeoForum($attribute);
            case 'Brokers':
                return self::getCeoBrokers($attribute);
            case 'Download':
                return self::getCeoDownload($attribute);
            case 'Faq':
                return self::getCeoFaq($attribute);
            case 'Features':
                return self::getCeoFeatures($attribute);
            case 'User':
                return self::getCeoUser($attribute);
        }
    }

    public static function getCeoForum($attribute)
    {
        if (BaseUrl::current() == '/forum/default/index') {

            if ($attribute === 'title')
                return 'Forum - Protrader MC';

            if ($attribute === 'description')
                return 'Discussion forum for the PTMC trading platform. This forum help traders by openly sharing indicators, trading strategies and to find useful information';

            if ($attribute === 'keywords')
                return "forum, ptmc, trading platform, software, system, pfsoft, protrader";
        }

        # trying to find in url category or topic

        $url = str_replace('%2F', '/', BaseUrl::current());//last category not with slash (((

        $categoryArr = explode('/', $url);

        $last_category_with_dash = ucfirst(end($categoryArr));

        $last_category = str_replace('-', ' ', $last_category_with_dash);

        $cat = explode('/', BaseUrl::current())[2];

        if ($cat == 'category') {
            if ($attribute === 'title')
                return $last_category . ' - PTMC Forum';

            if ($attribute === 'description')
                return 'Let\'s discuss "' . $last_category . '" in PTMC forum with other traders. This forum help traders by openly sharing indicators, trading strategies and to find useful information.';

            if ($attribute === 'keywords')
                return "forum, ptmc, trading platform, software, system, pfsoft, protrader";
        }

        //topic part
        if ($attribute === 'title')
            return $last_category . ' - PTMC Forum';

        if ($attribute === 'description')
            return 'Let\'s discuss "' . $last_category . '" in PTMC forum with other traders. This forum help traders by openly sharing indicators, trading strategies and to find useful information.';

        // it is the same in forum
        if ($attribute === 'keywords')
            return '';
    }

    public static function getCeoUser($attribute)
    {
        $user_id = ucfirst(str_replace('/' . explode('/', BaseUrl::current())[1] . '/', '', BaseUrl::current()));

        $id = str_replace(['Id', 'id'], '', $user_id);

        $user = UserIdentity::findIdentity($id);

        $username = is_object($user) ? $user->getFullname() : $user_id;

        if ($attribute === 'title')
            return $username . ' on PTMC';

        if ($attribute === 'description')
            return $username . ' has been joined the Protrader MC (PTMC) community. PTMC is a professional trading software from creators of Protrader.';

        if ($attribute === 'keywords')
            return $username . ', profile, trading platform, software, system, PFSOFT, Protrader';
    }


    public static function getCeoFeatures($attribute)
    {
        if ($attribute === 'title')
            return 'Featured Functionality List of Protrader MC - PTMC';

        if ($attribute === 'description')
            return 'PTMC offers features specific to the beginner and professional traders\' needs. Use our comparison table to find feature that supported by broker';

        if ($attribute === 'keywords')
            return "features, ptmc, trading platform, software, system, pfsoft, protrader";
    }

    public static function getCeoFaq($attribute)
    {
        if ($attribute === 'title')
            return 'FAQ - Protrader MC';

        if ($attribute === 'description')
            return 'Find answers to frequently asked questions about PTMC trading platform and related brokers. If you can\'t find answer on your question, please contact us.';
        if ($attribute === 'keywords')
            return "faq, ptmc, trading platform, software, system, pfsoft, protrader";
    }

    public static function getCeoDownload($attribute)
    {
        if ($attribute === 'title')
            return 'Download PTMC Trading Platform for Desktop';

        if ($attribute === 'description')
            return 'Download Protrader MC trading software for free and start using professional tools for the active trader with full customization and intuitive interface';
        if ($attribute === 'keywords')
            return "download, ptmc, trading platform, software, system, pfsoft, protrader";
    }

    public static function getCeoBrokers($attribute)
    {
        if ($attribute === 'title')
            return 'Brokers - List of Brokers with Protrader MC - PTMC';

        if ($attribute === 'description')
            return 'PTMC provides reliable connections to a wide list of high quality online brokers and providers of trading service';

        if ($attribute === 'keywords')
            return "brokers, ptmc, trading platform, software, system, pfsoft, protrader";
    }

    /**
     * @param $attribute
     * @param $page_number
     * @param $controller_name
     * @return string
     */
    public static function getCeoArticles($attribute, $page_number, $controller_name)
    {
        $urlArr = explode('/', str_replace(Yii::$app->urlManager->hostInfo, '', BaseUrl::canonical()));

        /**
         * sample of $m
         *
         * Array
         * (
         * [0] =>
         * [1] => articles    - controller name
         * [2] => idf         - category alias
         * )
         */

        if (count($urlArr) === 3) {
            $category = ucfirst($urlArr[self::CATEGORY_INDEX]);
            $category_mini = $urlArr[self::CATEGORY_INDEX];

            if ($attribute === 'title') {
                if ($page_number)
                    return $category . ' - Page ' . $page_number . ' - PTMC';

                return $category . ' in Protrader MC';
            }

            if ($attribute === 'description') {
                if ($page_number)
                    return "On page " . $page_number . " in $category category we prepared useful educational trading materials that you can use in your real practice.";

                return "In section {$category} we prepared useful educational trading materials that you can use in your real practice";
            }
        }

        if ($attribute === 'title') {
            if ($page_number)
                return $controller_name . ' - Page ' . $page_number . ' - PTMC';

            return $controller_name . ' - Trading Analytics and Reviews - PTMC';
        }

        if ($attribute === 'description') {
            if ($page_number)
                return "On page " . $page_number . " of trading articles and reviews we prepared useful educational trading materials that you can use in your real practice.";

            return 'In section of trading articles and reviews we prepared useful educational trading materials that you can use in your real practice';
        }

        if ($attribute === 'keywords') {
            if (isset($category_mini) && $page_number)
                return "page " . $page_number . ', ' . $category_mini . ", ptmc, trading platform, software, system, pfsoft, protrader";

            if ($page_number)
                return "page " . $page_number . ", articles, reviews, ptmc, trading platform, software, system, pfsoft, protrader";

            if (isset($category_mini))
                return "$category_mini, ptmc, trading platform, software, system, pfsoft, protrader";

            return "articles, reviews, ptmc, trading platform, software, system, pfsoft, protrader";
        }
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */

    public static function getCanonical()
    {
        $result_canonical = str_replace(['/index', '/default'], '', str_replace('%2F', '/', BaseUrl::current()));

        return Yii::$app->getUrlManager()->getHostInfo() . '' . $result_canonical;
    }
}