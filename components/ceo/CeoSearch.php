<?php

namespace app\components\ceo;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CeoSearch represents the model behind the search form about `app\components\ceo\Ceo`.
 */
class CeoSearch extends Ceo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'entity_id'], 'integer'],
            [['entity', 'title', 'description', 'keywords', 'h1', 'canonical'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ceo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'id'        => $this->id,
            'entity_id' => $this->entity_id,
        ]);

        $query->andFilterWhere(['like', 'entity', $this->entity])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'keywords', $this->keywords])
            ->andFilterWhere(['like', 'h1', $this->h1])
            ->andFilterWhere(['like', 'canonical', $this->canonical]);

        return $dataProvider;
    }
}
