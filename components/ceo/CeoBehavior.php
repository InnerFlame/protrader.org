<?php
namespace app\components\ceo;

use app\components\langInput\LangInputBehavior;
use app\modules\codebase\models\Description;
use app\modules\forum\models\FrmTopic;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\helpers\VarDumper;

class CeoBehavior extends Behavior
{
    /*
    |--------------------------------------------------------------------------
    | Properties & constants
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | Configuration
    |--------------------------------------------------------------------------
    */

    /**
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterUpdate',
        ];
    }

    /**
     * @param $event
     */
    public function afterInsert($event)
    {
        if ($_POST)// added 10 february igor
            self::saveCeo($this->owner);
    }

    /**
     * @param $event
     */
    public function afterUpdate($event)
    {
        if ($_POST) // added 10 february igor
            self::saveCeo($this->owner);
    }

    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */

    /**
     * @param $model
     * @return bool
     */
    public static function saveCeo($model)
    {
        $ceo = Ceo::findEntity($model->getTypeEntity(), $model->id);

        if (!is_object($ceo))
            $ceo = new Ceo();

        $ceo->entity = "{$model->getTypeEntity()}";
        $ceo->entity_id = $model->id;

        if ($_POST)
            $ceo->load($_POST);

        self::saveTitle($ceo);
        self::saveCanonical($ceo, $model);

        if (!$ceo->save()) {
//             echo '<pre>';
//             print_r($ceo->getErrors());
//             print_r($ceo->attributes);
//             die;
            return false;
        }

        return true;
    }

    /**
     * @param $ceo
     * @param $model
     */
    public static function saveCanonical(&$ceo, $model)
    {
        if($model instanceof FrmTopic){
            $ceo->canonical = 'http://' . $_SERVER['HTTP_HOST'] . $model->getViewUrl();
        }
        if (!$ceo->canonical || $ceo->canonical == '') {
            $ceo->canonical = 'http://' . $_SERVER['HTTP_HOST'] . $model->getViewUrl();
        }
    }

    /**
     * @param $ceo
     */
    public static function saveTitle(&$ceo)
    {
        if (isset($ceo->title[LangInputBehavior::DEFAULT_LANGUAGE]) && isset($ceo->title[LangInputBehavior::RUSSIAN_LANGUAGE]))
            if ((!$ceo->title[LangInputBehavior::DEFAULT_LANGUAGE] || !$ceo->title[LangInputBehavior::RUSSIAN_LANGUAGE])) {
                if (isset($_POST['News'])) {
                    $ceo->title = $_POST['News']['title'];
                }

                if (isset($_POST['Articles'])) {
                    $ceo->title = $_POST['Articles']['title'];
                }

                if (isset($_POST['Pages'])) {
                    $ceo->title = $_POST['Pages']['title'];
                }

                if (isset($_POST['FrmCategory'])) {
                    //this need to do cause we have title not an array but ceo title is an array
                    // and we modify post title to array
                    $title = $_POST['FrmCategory']['title'];

                    $_POST['FrmCategory']['title'] = array();
                    $_POST['FrmCategory']['title'][LangInputBehavior::DEFAULT_LANGUAGE] = $title;
                    $_POST['FrmCategory']['title'][LangInputBehavior::RUSSIAN_LANGUAGE] = $title;

                    $ceo->title = $_POST['FrmCategory']['title'];
                }

                //cause we have lang input array
                if (isset($_POST['FrmTopic'])) {
                    if (!is_array($_POST['FrmTopic']['title'])) {
                        $title_topic = $_POST['FrmTopic']['title'];
                        $_POST['FrmTopic']['title'] = array();
                        $_POST['FrmTopic']['title'][LangInputBehavior::DEFAULT_LANGUAGE] = $title_topic;
                        $_POST['FrmTopic']['title'][LangInputBehavior::RUSSIAN_LANGUAGE] = $title_topic;
                    }
                    $ceo->title = $_POST['FrmTopic']['title'];
                }

                //cause we have lang input array
                if (isset($_POST['Broker'])) {
                    if (!is_array($_POST['Broker']['name'])) {
                        $title_topic = $_POST['Broker']['name'];
                        $_POST['Broker']['name'] = array();
                        $_POST['Broker']['name'][LangInputBehavior::DEFAULT_LANGUAGE] = $title_topic;
                        $_POST['Broker']['name'][LangInputBehavior::RUSSIAN_LANGUAGE] = $title_topic;
                    }
                    $ceo->title = $_POST['Broker']['name'];
                }

                if (isset($_POST['Improvement'])) {
                    $ceo->title = $_POST['Improvement']['title'];
                }
            }
    }
}

