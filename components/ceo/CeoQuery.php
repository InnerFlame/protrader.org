<?php

namespace app\components\ceo;
use app\components\Entity;

/**
 * This is the ActiveQuery class for [[Ceo]].
 *
 * @see Ceo
 */
class CeoQuery extends \yii\db\ActiveQuery
{
    /**
     * @inheritdoc
     * @return Ceo[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Ceo|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

//    /**
//     * @return $this
//     */
//    public function notDeleted()
//    {
//        return $this->andWhere(['deleted' => Entity::NOT_DELETED]);
//    }
}
