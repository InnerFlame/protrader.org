<?php

namespace app\components\ceo;

use app\components\Entity;
use app\components\langInput\LangInputBehavior;
use Yii;

/**
 * This is the model class for table "ceo".
 *
 * @property integer $id
 * @property string $entity
 * @property integer $entity_id
 * @property string $title
 * @property string $description
 * @property string $keywords
 * @property string $h1
 * @property string $canonical
 */
class Ceo extends Entity
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ceo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entity_id'], 'integer'],
            [['description', 'keywords'], 'string', 'max' => 1024],
            [['entity'], 'string', 'max' => 128],
            [['title', 'h1', 'canonical', 'entity'], 'string', 'max' => 1024]
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'lang' => [
                'class'      => LangInputBehavior::className(),
                'attributes' => ['title', 'description', 'keywords', 'canonical'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'entity'      => 'Entity',
            'entity_id'   => 'Entity ID',
            'title'       => 'Ceo Title',
            'description' => 'Description',
            'keywords'    => 'Keywords',
            'h1'          => 'H1',
            'canonical'   => 'Canonical',
        ];
    }

    public static function findEntity($type, $id)
    {
        return Ceo::find()->where(['entity_id' => $id, 'entity' => $type])->one();
    }

    public function getTypeEntity()
    {
        return Entity::CEO;
    }

    public function getViewUrl()
    {
        return null;
    }

    public function getObject()
    {
        $class_name = Entity::getEntityByType($this->entity);

        if(is_object($class_name))
            return $this->hasOne($class_name::className(), ['id' => 'entity_id']);
        return null;
    }

    public function getName()
    {

    }

    /**
     * @inheritdoc
     * @return CeoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CeoQuery(get_called_class());
    }
}