<?php

namespace app\components\ceo;

use yii;
use yii\base\Widget;

class CeoWidget extends Widget
{
    public $model;
    public $form;

    public $options;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $ceo = Ceo::findEntity($this->model->getTypeEntity(), $this->model->id);

        if(!is_object($ceo)) {
            $ceo = new Ceo();
        }

        return Yii::$app->getView()->render('@app/components/ceo/views/ceo.twig', [
           'ceo' => $ceo,
           'form' => $this->form,
       ]);
    }
}