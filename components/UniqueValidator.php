<?php
/**
 * Created by PhpStorm.
 * @author: Roman Budnitsky
 * @date: 27.11.14 16:54
 * @copyright Copyright (c) 2014 PFSOFT LLC
 *
 * Class UniqueValidatorFix
 */

namespace app\components;

use yii\db\ActiveRecordInterface;

class UniqueValidator extends \yii\validators\UniqueValidator {

    /**
     * @inheritdoc
     */
    public function validateAttribute($object, $attribute)
    {
        /* @var $targetClass ActiveRecordInterface */
        $targetClass = $this->targetClass === null ? get_class($object) : $this->targetClass;
        $targetAttribute = $this->targetAttribute === null ? $attribute : $this->targetAttribute;

        if (is_array($targetAttribute)) {
            $params = [];
            foreach ($targetAttribute as $k => $v) {
                $params[$v] = is_integer($k) ? $object->$v : $object->$k;
            }
        } else {
            $params = [$targetAttribute => $object->$attribute];
        }

        foreach ($params as $value) {
            if (is_array($value)) {
                $this->addError($object, $attribute, Yii::t('yii', '{attribute} is invalid.'));

                return;
            }
        }

        $query = $targetClass::find();
        $query->with = null;
        $query->andWhere($params);

        if ($this->filter instanceof \Closure) {
            call_user_func($this->filter, $query);
        } elseif ($this->filter !== null) {
            $query->andWhere($this->filter);
        }

        if (!$object instanceof ActiveRecordInterface || $object->getIsNewRecord()) {
            // if current $object isn't in the database yet then it's OK just to call exists()
            $exists = $query->exists();
        } else {
            // if current $object is in the database already we can't use exists()
            /* @var $objects ActiveRecordInterface[] */
            $objects = $query->limit(2)->all();
            $n = count($objects);
            if ($n === 1) {
                $keys = array_keys($params);
                $pks = $targetClass::primaryKey();
                sort($keys);
                sort($pks);
                if ($keys === $pks) {
                    // primary key is modified and not unique
                    $exists = $object->getOldPrimaryKey() != $object->getPrimaryKey();
                } else {
                    // non-primary key, need to exclude the current record based on PK
                    $exists = $objects[0]->getPrimaryKey() != $object->getOldPrimaryKey();
                }
            } else {
                $exists = $n > 1;
            }
        }

        if ($exists) {
            $this->addError($object, $attribute, $this->message);
        }
    }

} 