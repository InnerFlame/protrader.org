<?php

namespace app\components\customEvents\_subscriber;

use app\modules\articles\models\Articles;
use app\models\community\Comments;
use app\models\mail\Queue;
use app\models\user\UserCustomValue;
use yii;
use app\components\customEvents\_interface\ICustomEventsObserver;
use app\components\customEvents\_subscriber\Subscribers;

class SubscribersObserver implements ICustomEventsObserver
{
    /*
    |--------------------------------------------------------------------------
    | Constants && properties
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */

    public function fireEvent($eventModel)
    {
        $subscribers = new Subscribers();
        $queueAttr = $subscribers->getQueueAttr($eventModel);

        $queue = new Queue();
        $data = [];
        $data['Queue']['entity'] = $queueAttr['entity'];
        $data['Queue']['entity_id'] = $queueAttr['entity_id'];
        $data['Queue']['event'] = $queueAttr['event'];

        if($queue->load($data) && $queue->save()) {
            //var_dump(1);die();
        }

        //var_dump($queue->errors);die();

    }
}