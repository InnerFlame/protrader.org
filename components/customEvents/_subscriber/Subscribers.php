<?php

namespace app\components\customEvents\_subscriber;

use app\modules\articles\models\Articles;
use app\models\community\Comments;
use app\models\Constants;
use app\models\forum\FrmTopic;
use app\models\user\User;
use app\models\user\UserCustomField;
use app\models\user\UserCustomValue;
use yii;

class Subscribers
{
    /*
    |--------------------------------------------------------------------------
    | Constants && properties
    |--------------------------------------------------------------------------
    */

    /**
     * На форуме, я создал топик, кто-то оставил на него коммент
     */
    const SUBSCRIBE_WHEN_OWN_TOPIC_COMMENTED = 'SUBSCRIBE_WHEN_OWN_TOPIC_COMMENTED';

    /**
     * На форуме, кто-то откомментировал где и я комментировал
     */
    const SUBSCRIBE_WHEN_TOPIC_COMMENTED = 'SUBSCRIBE_WHEN_TOPIC_COMMENTED';

    /**
     * В статьях, кто-то откомментировал где и я комментировал
     */
    const SUBSCRIBE_WHEN_ARTICLE_COMMENTED = 'SUBSCRIBE_WHEN_ARTICLE_COMMENTED';

    /**
     * В статьях и на форуме, кто-то ответил на мой коммент
     */
    const SUBSCRIBE_WHEN_ANSWERED_OWN_COMMENT = 'SUBSCRIBE_WHEN_ANSWERED_OWN_COMMENT';

    /**
     * В статьях и на форуме, создана новая статья или топик
     */
    const SUBSCRIBE_WHEN_CREATED_ARTICLE_OR_TOPIC = 'SUBSCRIBE_WHEN_CREATED_ARTICLE_OR_TOPIC';

    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */

    /**
     * @return array
     */
    public static function getConstants()
    {
        $reflect = new \ReflectionClass(__CLASS__);

        return $reflect->getConstants();
    }

    /**
     * @param $model
     * @return string
     */
    public function getType($model)
    {
        if(get_class($model) == Constants::ENTITY_COMMENT)
            return Subscribers::SUBSCRIBE_WHEN_ARTICLE_COMMENTED;

    }

    /**
     * @return array
     */
    public function getQueueAttr($model)
    {
        if($this->getType($model) == Subscribers::SUBSCRIBE_WHEN_ARTICLE_COMMENTED)
            return $this->getSubscribeWhenArticleCommentedAttributes($model);
    }

    public function getSubscribers($eventModel, &$subscribers)
    {
        // SUBSCRIBE_WHEN_CREATED_ARTICLE - nothing to do
        // SUBSCRIBE_WHEN_CREATED_TOPIC - nothing to do

        if($eventModel instanceof Comments)
        {

        }


        if($eventModel instanceof Comments)
        {
            // SUBSCRIBE_WHEN_OWN_TOPIC_COMMENTED

            $entityModel = $eventModel->getEntityModel();

            if($entityModel instanceof FrmTopic)
            {
                $user_id = $entityModel->user_id;

                $_user_own = User::findOne($user_id);

                if($_user_own->isPermitted('SUBSCRIBE_WHEN_OWN_TOPIC_COMMENTED'))
                    $subscribers[] = ['user' => $_user_own, 'Your topic was commented.'];


            }

            // SUBSCRIBE_WHEN_TOPIC_COMMENTED

            if($entityModel instanceof FrmTopic)
            {
                $topicComments = $entityModel->getComments();

                $field_id = UserCustomField::getFieladIdByAlias('SUBSCRIBE_WHEN_TOPIC_COMMENTED');

                if(count($topicComments) > 0)
                    foreach($topicComments as $_comment)
                    {
                        // add users
                    }

            }

            // SUBSCRIBE_WHEN_ARTICLE_COMMENTED

            if($entityModel instanceof Articles)
            {
                $topicComments = $entityModel->getComments()->all();

                $field_id = UserCustomField::getFieladIdByAlias('SUBSCRIBE_WHEN_ARTICLE_COMMENTED');

                if(count($topicComments) > 0)
                    foreach($topicComments as $_comment)
                        if(is_object($_comment))
                        {
                            // add userss
                        }

            }
        }
    }

//    /**
//     * @param $model
//     * @return static[]
//     */
//    public function getSubscribers($eventModel)
//    {
//        $subscribersList = [];
//        /*
//         * [
//         *  [
//         *      'user' => model,
//         *      'message' => string           /////////////////////'template' => file_path,         *
//         *  ]
//         * ]
//         */
//
//        $entity = $eventModel->getEntityModel();
//
//        // SUBSCRIBE_WHEN_CREATED_ARTICLE
//        // SUBSCRIBE_WHEN_CREATED_TOPIC
//
//        if($eventModel instanceof Articles)
//        {
//            $usersCreatedNewArticle = [];
//
//
//            $_users = User::find()->all();
//
//            // foreach $_users as $_user
//                //if($_user->isPermitted('SUBSCRIBE_WHEN_CREATED_ARTICLE'))
//                    // $usersCreatedNewArticle[] = ['user' => $_user, 'New Article was created.']
//
//
//            $subscribersList[] = array_merge($subscribersList, $usersCreatedNewArticle);
//
//        }
//
//        if($eventModel instanceof Comments) {
//            // SUBSCRIBE_WHEN_OWN_TOPIC_COMMENTED
//
//            if ($entity instanceof FrmTopic) {
//
//                $usersOwnTopic = [];
//
//                $_users_custom_values = UserCustomValue::findAll(
//                    [
//                        'field_id' => UserCustomField::getFieladIdByAlias('SUBSCRIBE_WHEN_OWN_TOPIC_COMMENTED'),
//                        'value'    => $entity->id,
//                    ]
//                );
//
//                //foreach ($_users_custom_values as $_custom_value) {
//                //   if ($_custom_value->getUser()->isPermitted('SUBSCRIBE_WHEN_OWN_TOPIC_COMMENTED'))
//                //       $usersOwnTopic[] = ['user' => $_custom_value, 'Your Own Topic was commented.'];
//
//
//                $subscribersList[] = array_merge($subscribersList, $usersOwnTopic);
//            }
//
//
//            // SUBSCRIBE_WHEN_TOPIC_COMMENTED
//
//            if ($entity instanceof FrmTopic) {
//                $usersCommentedTopic = [];
//
//                $_users = UserCustomValue::findAll(
//                    [
//                        'field_id' => UserCustomField::getFieladIdByAlias('SUBSCRIBE_WHEN_TOPIC_COMMENTED'),
//                        'value'    => $entity->id,
//                    ]
//                );
//
//                // foreach $_users as $_user
//                //if($_user->isPermitted('SUBSCRIBE_WHEN_OWN_TOPIC_COMMENTED'))
//                // $usersCommentedTopic[] = ['user' => $_user, 'Your Commented Topic was commented.']
//
//                $subscribersList[] = array_merge($subscribersList, $usersCommentedTopic);
//
//            }
//
//            // SUBSCRIBE_WHEN_ARTICLE_COMMENTED
//
//            if ($entity instanceof Articles) {
//
//                $usersCommentedArticle = [];
//
//                $_users = UserCustomValue::findAll(
//                    [
//                        'field_id' => UserCustomField::getFieladIdByAlias('SUBSCRIBE_WHEN_ARTICLE_COMMENTED'),
//                        'value'    => $entity->id,
//                    ]
//                );
//
//                foreach ($_users as $_user)
//                    if ($_user->getUserModel()->isPermitted('SUBSCRIBE_WHEN_ARTICLE_COMMENTED'))
//                        $usersCommentedArticle[] = ['user' => $_user, 'Your Commented Article was commented.'];
//
//                $subscribersList[] = array_merge($subscribersList, $usersCommentedArticle);
//
//            }
//
//            // SUBSCRIBE_WHEN_ANSWERED_OWN_COMMENT
//
//            $usersCommentRelied = [];
//
//            $_users = UserCustomValue::findAll(
//                [
//                    'field_id' => UserCustomField::getFieladIdByAlias('SUBSCRIBE_WHEN_ANSWERED_OWN_COMMENT'),
//                    'value'    => $entity->id,
//                ]
//            );
//
//            // foreach $_users as $_user
//            //if($_user->isPermitted('SUBSCRIBE_WHEN_ANSWERED_OWN_COMMENT'))
//            // $usersCommentRelied[] = ['user' => $_user, 'Your Commented was replied.']
//
//            $subscribersList[] = array_merge($subscribersList, $usersCommentRelied);
//
//        }
//
//        return $subscribersList;
//    }

    /**
     * @param $model
     * @return array
     */
    public function getSubscribeWhenArticleCommentedAttributes($model)
    {
        $model_field = UserCustomField::findOne([
            'alias' => Subscribers::SUBSCRIBE_WHEN_ARTICLE_COMMENTED,
        ]);

        $subscribers_set = UserCustomValue::findAll([
            'field_id' =>  $model_field->id
        ]);

        return [
            'subscribers_set' => $subscribers_set,
            'entity'          => get_class($model),
            'entity_id'       => $model->id,
            'event'           => 'created'
        ];
    }

}