<?php

namespace app\components\customEvents\_interface;

interface ICustomEventsObserver
{
    public function fireEvent($eventModel);
}