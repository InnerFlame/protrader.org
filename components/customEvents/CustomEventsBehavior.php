<?php

namespace app\components\customEvents;

use yii\base\Behavior;
use yii\db\ActiveRecord;
use app\components\customEvents\_subscriber\SubscribersObserver;

class CustomEventsBehavior extends Behavior
{
    /*
    |--------------------------------------------------------------------------
    | Constants && properties
    |--------------------------------------------------------------------------
    */

    const SUBSCRIBERS_OBSERVER = 'subscribers';

    public $observers = [];

    public $type = null;

    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */

    public function init()
    {
        if($this->type == self::SUBSCRIBERS_OBSERVER)
            $this->observers[] = new SubscribersObserver();
    }

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterInsertEvents',
        ];
    }

    public function afterInsertEvents()
    {
        foreach($this->observers as $observer){
            $observer->fireEvent($this->owner);
        }
    }
}