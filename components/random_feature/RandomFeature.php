<?php
namespace app\components\random_feature;

use app\components\Entity;
use app\models\community\Likes;
use app\modules\brokers\models\Improvement;
use yii\bootstrap\Widget;
use yii\helpers\ArrayHelper;

/**
 * Class RandomFeature
 * @package app\components\JsEventsWidget
 */
class RandomFeature extends Widget
{
    private $random_feature;

    public function run()
    {
        $this->random_feature = Improvement::getRandomFeature();

        if (is_object($this->random_feature))
            return $this->registerPlugin();

    }

    protected function registerPlugin($name = null)
    {
        if (is_object($this->random_feature))
            return \Yii::$app->getView()->render('@app/components/random_feature/views/_random.twig', ['random_feature' => $this->random_feature]);

        return null;
    }
}
