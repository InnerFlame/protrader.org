<?php
/**
 * Created by PhpStorm.
 * @author: Roman Budnitsky
 * @date: 16.02.15 14:00
 * @copyright Copyright (c) 2015 PFSOFT LLC
 *
 * Class ConsoleRunner
 * @package app/components
 */


namespace app\components;

use yii;
use yii\base\Component;
use yii\base\InvalidConfigException;

class ConsoleRunner extends Component
{
    public $file;

    public function init()
    {
        parent::init();
        if ($this->file === null) {
            throw new InvalidConfigException('The "file" property must be set.');
        }
    }

    public function run($cmd)
    {
        $cmd = PHP_BINDIR . '/php ' . Yii::getAlias($this->file) . ' ' . $cmd;
        /*if ($this->isWindows() === true) {
            pclose(popen('start /b ' . $cmd, 'r'));
        } else {
            pclose(popen($cmd . ' /dev/null &', 'r'));
        }*/
        exec($cmd);
        return true;
    }

    protected function isWindows()
    {
        if (PHP_OS == 'WINNT' || PHP_OS == 'WIN32') {
            return true;
        } else {
            return false;
        }
    }
}