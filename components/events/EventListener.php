<?php

namespace app\components\events;

use app\modules\brokers\models\FeatureSlide;
use yii\base\Event;
use yii\db\ActiveRecord;
use yii\db\AfterSaveEvent;

class EventListener extends \yii\base\Component implements \yii\base\BootstrapInterface
{

    public function bootstrap($app)
    {
        // test event
        Event::on(FeatureSlide::className(), ActiveRecord::EVENT_AFTER_UPDATE, function(AfterSaveEvent $event) {
           
        });
    }

}
