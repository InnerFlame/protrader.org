<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 * Date: 03.02.2016
 * Time: 11:13
 */

namespace app\components\customComments;


use app\components\customComments\events\CommentEvent;
use app\components\customComments\events\ReplyEvent;
use app\components\customComments\forms\CommentForm;
use app\components\Entity;
use app\models\community\Comments;
use app\models\user\User;
use app\modules\notify\components\Event;
use app\modules\notify\components\EventMail;
use app\modules\notify\components\EventUser;
use Yii;
use yii\web\Controller;
use yii\web\HttpException;

class CommentsController extends Controller
{

    public $layout = false;

    public function actionLoad()
    {
        if(!\Yii::$app->request->isAjax)
            throw new \yii\web\HttpException(404);

        $model = new CommentForm();

        $model->load(\Yii::$app->request->post(), '');

        return $this->renderAjax('_include/_dialog.twig', [
            'model' => $model
        ]);
    }

    public function actionSave()
    {
        //new
        if(!isset($_POST['Comments']['id']) && !isset($_POST['Comments']['root'])){
            $model = new Comments();
            $model->user_id = $model->user_id = \Yii::$app->user->identity->id;
            $model->load(\Yii::$app->request->post());
            if($model->validate()){
                $model->makeRoot();
                Yii::$app->trigger(CustomComments::EVENT_COMMENT_ADD, new Event(['sender' => new CommentEvent(['post_id' => $model->id])]));
            }
            return $this->responseAllComments($model);
        }
        //edit
        if(isset($_POST['Comments']['id']) && $model = Comments::findOne($_POST['Comments']['id'])){
            $model->content = $_POST['Comments']['content'];
            $model->save();
            return $this->responseAllComments($model);
        }
        //reply
        if(isset($_POST['Comments']['root']) && $model = Comments::findOne($_POST['Comments']['root'])){
            $reply_model = new Comments();
            $reply_model->load(\Yii::$app->request->post());
            $reply_model->user_id = \Yii::$app->user->identity->id;
            $reply_model->entity_id = $model->entity_id;
            $reply_model->entity = $model->entity;
            if($reply_model->validate()){
                $reply_model->appendTo($model);
                $this->pushReplyEvents($model, $reply_model);
                return $this->responseAllComments($reply_model);
            }
        }
    }

    public function actionEdit()
    {
        if(!\Yii::$app->request->isAjax)
            throw new HttpException(404);

        if(!isset($_POST['id']) || (int)$_POST['id'] == 0)
            throw new HttpException(400);

        $model = Comments::findOne((int)$_POST['id']);

        return $this->renderAjax('_include/_dialog.twig', [
            'model' => $model
        ]);
    }

    public function actionReply()
    {
        if(!\Yii::$app->request->isAjax)
            throw new HttpException(404);

        if(!isset($_POST['id']) || (int)$_POST['id'] == 0)
            throw new HttpException(400);

        if(!$reply_model = Comments::findOne((int)$_POST['id']))
            throw new HttpException(400);

        $user = User::findOne($reply_model->user_id);

        $model = new Comments();
        $model->entity = $reply_model->entity;
        $model->entity_id = $reply_model->entity_id;
        $model->user_id = \Yii::$app->user->identity->id;
        $model->root = $reply_model->id;

        return $this->renderAjax('_include/_dialog.twig', [
            'model' => $model,
            'reply_message' => $reply_model->content,
            'user' => is_object($user) ? $user : ''
        ]);
    }

    public function actionDelete($id)
    {
        if (!$id > 0) throw new HttpException(400);

        $model = Comments::findOne((int)$id);

        if (!$model) throw new HttpException(400);

        if(\Yii::$app->user->isGuest){
            Yii::$app->session->setFlash('warning', 'You can not delete the message. You are not authorized');
            return $this->redirect($model->getViewUrl());
        }

        if ((Yii::$app->user->identity->id === $model->user_id) || Yii::$app->user->identity->isAdmin()) {

            $model->delete();
            \Yii::$app->session->setFlash('success', 'Message was successfully deleted');
        }else{
            \Yii::$app->session->setFlash('success', 'You can\'t delete not your comment');
        }

        return $this->redirect($model->getViewUrl());
    }

    public function getViewPath()
    {
        return \Yii::getAlias("@app").DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'customComments'.DIRECTORY_SEPARATOR.'views';
    }


    protected function responseAllComments($model){
        $models = Comments::find()->where([
            'entity'    => $model->entity,
            'entity_id' => $model->entity_id,
            'level'     => 0,
            'deleted' => Entity::NOT_DELETED,
        ])->orderBy(['created_at' => SORT_ASC])->all();

        if(count($models) == 0)
            throw new HttpException(400);

        return $this->renderPartial('_include/_comments_list.twig', [
            'models' => $models,
        ]);
    }

    /**
     * Отправляет необходимые уведомления.
     * @param $model
     * @param $reply_model
     */
    protected function pushReplyEvents($model, $reply_model)
    {
        $event = new ReplyEvent();
        $event->comment = $model;
        $event->reply = $reply_model;
        $sender = new CommentEvent(['post_id' => $reply_model->id]);
        $sender->mail = true;
        $sender->attributes['sender_id'] = $reply_model->user_id;
        $sender->attributes['user_id'] = $model->user_id;
        Yii::$app->trigger(CustomComments::EVENT_COMMENT_REPLY, new Event(['sender' => $sender]));

        $sender->addIgnoredUser($model->user_id);
        $sender->mail = false;
        Yii::$app->trigger(CustomComments::EVENT_COMMENT_ADD, new Event(['sender' => $sender]));
    }
}