<?php

namespace app\components\customComments\forms;

use app\models\community\Comments;
use app\models\community\Counts;
use yii;
use yii\base\Model;
use yii\base\Event;

class CustomCommentForm extends Model
{
    public $id;
    public $comment;
    public $class_name;

    public $answer_comment_id = false;

    public function rules()
    {
        return [
           [['comment'], 'required'],
            [['comment'], 'string', 'max' => 3000],
            [['id', 'class_name', 'answer_comment_id'], 'safe']
        ];
    }

    public function saveComment()
    {
        if (Yii::$app->request->post()) {

            $this->load(Yii::$app->request->post());

            if ($this->comment && $this->class_name && $this->id) {

                    $comment = new Comments();
                    $comment->content = $this->comment;
                    $comment->user_id =  Yii::$app->user->identity->id;
                    $comment->entity = $this->class_name;
                    $comment->entity_id = $this->id;

                    if($this->answer_comment_id > 0){
                        $root_comment = Comments::findOne((int) $this->answer_comment_id);

                        if(is_object($root_comment)){

                            $comment->appendTo($root_comment);

                            Counts::setCount($this->class_name, 'count_commment');

                            Yii::$app->session->setFlash('success',
                                Yii::t('app', 'Your answer was successfully added.'));

                            return true;
                        }

                    }else{

                        if ($comment->makeRoot()){

                            Counts::setCount($this->class_name, 'count_commment', $this->id);

                            Yii::$app->session->setFlash('success',
                                Yii::t('app', 'Your comment was successfully added.'));

                            echo 'dasdas'; exit;
                            Event::trigger(CustomComments::className(), CustomComments::EVENT_COMMENT_ADD, new Event(['sender' => $comment]));
                            return true;
                        }
                    }
                }
            // }
            // echo '<pre>'; var_dump(Yii::$app->request->post(),$this,$this->load(Yii::$app->request->post()),$comment);die;

        }

        Yii::$app->session->setFlash('danger',
            Yii::t('app', 'Error of adding comment.'));

        return false;
    }

}
