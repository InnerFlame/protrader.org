<?php

namespace app\components\customComments\models\traits;

use app\models\community\Comments;

trait CustomCommentsTrait {

    public function getCommentByEntity()
    {
        $params = \Yii::$app->request->post();
        if(isset($params['comment_id']) && isset($params['entity'])){
            $comment = Comments::find()->where([
                'entity'    => $params['entity'],
                'id' => $params['comment_id'],
            ])->one();

            return $comment ? $comment : null;
        }
    }
}