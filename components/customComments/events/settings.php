<?php

use app\modules\cabinet\modules\notify\models\NotifyTemplate;
use app\components\customComments\CustomComments;

return [

    CustomComments::EVENT_COMMENT_ADD => [
        'label'     => 'Add new comments',
        'templates' => [
            NotifyTemplate::TYPE_EMAIL => [
                'title' => 'Ne activity in: {title}',
                'body' => 'Hi {username}, there is a new comment in {title}:<br>{description}<br> {link}'
            ],
            NotifyTemplate::TYPE_FULL  => [
                'title' => '{title}',
                'body' => '{username} has added a new comment {description}'
            ],
            NotifyTemplate::TYPE_SHORT => [
                'title' => 'commented in',
                'body' => '{title}'
            ],
            NotifyTemplate::TYPE_GROUP => [
                'title' => '{count} new comments in',
                'body' => '{title}'
            ]
        ]
    ],

    CustomComments::EVENT_COMMENT_REPLY => [
        'label'     => 'New answers to my comments',
        'templates' => [
            NotifyTemplate::TYPE_EMAIL => [
                'title' => 'New activity in: {title}',
                'body' => 'Hi {username}, there is a new comment in {title}:<br>{description}<br> {link}'
            ],
            NotifyTemplate::TYPE_FULL  => [
                'title' => '{title}',
                'body' => '{username} has added a new comment {description}'
            ],
            NotifyTemplate::TYPE_SHORT => [
                'title' => 'answered to your comment in:',
                'body' => '{title}'
            ],
            NotifyTemplate::TYPE_GROUP => [
                'title' => '{count} new answered in',
                'body' => '{title}'
            ]
        ]
    ],


];