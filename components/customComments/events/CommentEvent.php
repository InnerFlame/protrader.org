<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2016
 */

namespace app\components\customComments\events;


use app\modules\notify\components\Notification;
use app\models\community\Comments;
use app\components\Entity;

class CommentEvent extends Notification
{
    public $post_id;


    private $_entity;

    private $_post;

    /**
     * Возвращает заголовок статьи к которой комментарий
     * @return mixed
     */
    public function getTitle()
    {
        if($this->getEntity()){
            return $this->getEntity()->title;
        }
    }


    public function getDescription()
    {
        if($this->getPost()){
            return $this->getPost()->content;
        }
    }

    public function getAuthor()
    {
        if($this->getPost()){
            return $this->getPost()->user->getFullName();
        }
    }

    /**
     * @return Entity
     */
    public function getEntity()
    {
        if($this->getPost()){
            if(!$this->_entity){
                $this->_entity = $this->getPost()->getEntity();
            }
            return $this->_entity;
        }
    }

    /**
     * @return Comments
     */
    public function getPost()
    {
        if(!$this->_post){
            $this->_post = Comments::find()->where(['id' => $this->post_id])->andWhere(['deleted' => Entity::NOT_DELETED])->one();
        }
        return $this->_post;
    }


}