<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 * Date: 11.02.2016
 * Time: 17:27
 */

namespace app\components\customComments\events;


use yii\base\Event;

class ReplyEvent extends Event
{

    /**
     * @var \app\models\community\Comments
     */
    public $comment;

    /**
     * @var \app\models\community\Comments
     */
    public $reply;

}