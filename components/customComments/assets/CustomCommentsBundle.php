<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 * Date: 02.02.2016
 * Time: 14:34
 */

namespace app\components\customComments\assets;


use yii\web\AssetBundle;

class CustomCommentsBundle extends AssetBundle
{

    public $sourcePath = '@app/components/customComments/assets';

    public $js = [
        'js/custom-comments.js'
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}