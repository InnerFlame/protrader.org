var comment = (function(){

    function CustomComments(){
        var self = this;

        var container = $("<div id='comments-modal-container'></div>");

        $('body').append(container);

        this.edit = function(element){
            var item = $(element),
                entity = item.attr('data-text'),
                comment_id = item.attr('data-id');

            _showEditForm(entity, comment_id);

        }
        this.reply = function(){

        }

        /**
         * Load and show dialog comment form
         * @param id Unique ID comment
         * @param comment body comment
         * @param post_id relation to post,article,features
         * @param parent_id or this comment is reply
         */
        this.show = function(id, comment, post_id, parent_id){
            $.ajax({
                type:"POST",
                url:'/comments/load',
                data:{
                    id:id,
                    comment:comment,
                    post_id:post_id,
                    parent_id:parent_id
                },
                success:function(data){
                    container.html(data);
                    container.find('#comment-modal-form').modal('show');
                },
            });
        }

        function _showEditForm(entity, comment_id){
            $.ajax({
                type:"POST",
                url:'/ajax/getCommentByEntity',
                data:{
                    comment_id:comment_id,
                    entity:entity
                },
                dataType:'json',
                success:function(response){
                    if(response.status == 'OK'){
                        //show modal window
                        var modalEdit = $('.modal#edit');
                        modalEdit.find('textarea').redactor('code.set', response.data.content);
                        modalEdit.modal('show');
                    }

                },
            });
        }
    }
    return new CustomComments();
}());