<?php

namespace app\components\customComments;

use app\components\customComments\assets\CustomCommentsBundle;
use app\components\customComments\forms\CustomCommentForm;
use app\components\Entity;
use app\models\community\Comments;
use yii;
use yii\base\Widget;

class CustomComments extends Widget
{
    const EVENT_COMMENT_ADD = 'custom.comment.add';

    const EVENT_COMMENT_REPLY = 'custom.comment.reply';

    public $model;

    public $options;

    public $form = false;

    public function init()
    {
        self::checkPostData();
        parent::init();
    }

    public static function checkPostData()
    {
        $form = new CustomCommentForm();

        if (Yii::$app->request->post()) {
            if ($form->load(Yii::$app->request->post())) {
                if ($form->comment) {
                    $form->saveComment();
                }

                //for cancel save the previos comment
                Yii::$app->controller->redirect(Yii::$app->request->referrer);
            }

            if (isset($_POST['edit_comment'], $_POST['edit_comment_id'])) {
                Comments::editComment($_POST['edit_comment_id'], $_POST['edit_comment']);
            }

            unset($_POST['CustomCommentForm'], $_POST['comment']);
        }
    }

    public function run()
    {
        $models = Comments::find()->where([
            'entity'    => $this->model->getTypeEntity(),
            'entity_id' => $this->model->id,
            'level'     => 0,
            'deleted'   => Entity::NOT_DELETED,
        ])->orderBy(['created_at' => SORT_ASC])->all();

        $countModels = self::getCount($this->model->getTypeEntity(), $this->model->id);
        $view = Yii::$app->getView();
        CustomCommentsBundle::register($view);
        $model = new Comments();
        $model->entity = $this->model->getTypeEntity();
        $model->entity_id = $this->model->id;
        return $view->render('@app/components/customComments/views/comments.twig', [
            'model'       => $model,
            'models'      => $models,
            'countModels' => $countModels,
        ]);
    }

    public static function getCount($entity, $entity_id)
    {
        return count(
            Comments::find()->select('id')->where([
                'entity'    => $entity,
                'entity_id' => $entity_id,
                'deleted'   => Entity::NOT_DELETED,
            ])->all()
        );
    }

    public static function getModelCount($model)
    {
        return self::getCount($model->getTypeEntity(), $model->id);
    }
}
