<?php

namespace app\components\multi_select;

use dosamigos\multiselect\MultiSelect;
use dosamigos\multiselect\MultiSelectAsset;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\InputWidget;
use yii\base\InvalidConfigException;
use Yii;

class CustomMultiSelect extends MultiSelect
{

    /**
     * Registers MultiSelect Bootstrap plugin and the related events
     */
    protected function registerPlugin()
    {
        $view = $this->getView();

       // MultiSelectAsset::register($view); //problem

        $id = $this->options['id'];

        $options = $this->clientOptions !== false && !empty($this->clientOptions)
            ? Json::encode($this->clientOptions)
            : '';

        $js = "jQuery('#$id').multiselect($options);";
        $view->registerJs($js);
    }
}
