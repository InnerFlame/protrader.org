<?php
/**
 * Created by PhpStorm.
 * @author: Roman Budnitsky
 * @date: 05.11.14 16:14
 * @copyright Copyright (c) 2014 PFSOFT LLC
 *
 * Class AccessFilter
 * @package app\components\twig
 */

namespace app\components\twig;

use app\components\ceo\CeoHelper;
use app\components\Entity;
use app\components\helpers\StringHelper;
use app\components\langInput\LangInput;
use app\components\langInput\LangInputBehavior;
use app\components\MultiSelectCustom;
use app\models\community\Likes;
use app\models\forms\BugReportForm;
use app\models\forms\FeedbackForm;
use app\models\forms\FeedbackFormButtom;
use app\models\forms\GeneralSettingForm;
use app\models\lang\Languages;
use app\models\user\User;
use app\modules\forum\models\FrmComment;
use kartik\date\DatePicker;
use yii;
use yii\helpers\HtmlPurifier;
use yii\widgets\ListView;

class CustomFilters extends \Twig_Extension
{
    public function getFilters()
    {
        return [
            'can'             => new \Twig_Filter_Method($this, 'isUserCan'),
            'cannot'          => new \Twig_Filter_Method($this, 'isUserCannot'),
            'isCondition'     => new \Twig_Filter_Method($this, 'isCondition'),
            'getWrap'         => new \Twig_Filter_Method($this, 'getWrap'),
            'getLength'       => new \Twig_Filter_Method($this, 'getLength'),
            'getHtmlPurifier' => new \Twig_Filter_Method($this, 'getHtmlPurifier'),
            'clearHead'       => new \Twig_Filter_Method($this, 'clearHead'),
            //'preg_replace' => new \Twig_Filter_Method($this, 'pregReplace'),
        ];
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('isUserPermission', [$this, 'isUserPermission']),
            new \Twig_SimpleFunction('inArray', [$this, 'inArray']),
            new \Twig_SimpleFunction('staticMethod', [$this, 'staticMethod']),


            new \Twig_SimpleFunction('getStatic', [$this, 'getStatic']),
            new \Twig_SimpleFunction('getCeoTwig', [$this, 'getCeoTwig']),
            new \Twig_SimpleFunction('get_class_name', [$this, 'get_class_name']),
            new \Twig_SimpleFunction('getViewCounter', [$this, 'getViewCounter']),
            new \Twig_SimpleFunction('getImage', [$this, 'getImage']),
            new \Twig_SimpleFunction('getPreviewText', [$this, 'getPreviewText']),
            new \Twig_SimpleFunction('getPreviewTextWords', [$this, 'getPreviewTextWords']),
            new \Twig_SimpleFunction('getLanguage', [$this, 'getLanguage']),
            new \Twig_SimpleFunction('getShortLang', [$this, 'getShortLang']),
            new \Twig_SimpleFunction('getListView', [$this, 'getListView']),
            new \Twig_SimpleFunction('getClassName', [$this, 'getClassName']),
            new \Twig_SimpleFunction('getSearchValue', [$this, 'getSearchValue']),
            new \Twig_SimpleFunction('getPagesUrl', [$this, 'getPagesUrl']),
            new \Twig_SimpleFunction('getFeedbackForm', [$this, 'getFeedbackForm']),
            new \Twig_SimpleFunction('getCommentModel', [$this, 'getCommentModel']),
            new \Twig_SimpleFunction('getLangInputValue', [$this, 'getLangInputValue']),
            new \Twig_SimpleFunction('getQueryParams', [$this, 'getQueryParams']),


            new \Twig_SimpleFunction('createUrl', [$this, 'createUrl']),
            new \Twig_SimpleFunction('registerFlashes', [$this, 'registerFlashes']),
            new \Twig_SimpleFunction('createDocumentSearchUrl', [$this, 'createDocumentSearchUrl']),
            new \Twig_SimpleFunction('isMinimal', [$this, 'isMinimal']),
            new \Twig_SimpleFunction('initDatePickerAssets', [$this, 'initDatePickerAssets']),
            new \Twig_SimpleFunction('initMultiSelectAssets', [$this, 'initMultiSelectAssets']),
            new \Twig_SimpleFunction('dateFormat', [$this, 'dateFormat']),
            new \Twig_SimpleFunction('dateUTCFormat', [$this, 'dateUTCFormat']),
            new \Twig_SimpleFunction('absolute', [$this, 'absolute']),
            new \Twig_SimpleFunction('count', [$this, 'count']),
            new \Twig_SimpleFunction('translate', [$this, 'translate']),
            new \Twig_SimpleFunction('selectFilter', [$this, 'selectFilter']),
            new \Twig_SimpleFunction('selectFilterForm', [$this, 'selectFilterForm']),
            new \Twig_SimpleFunction('stripTag', [$this, 'stripTag']),
            new \Twig_SimpleFunction('issetGET', [$this, 'issetGET']),
            new \Twig_SimpleFunction('canLike', [$this, 'canLike']),
            new \Twig_SimpleFunction('canLikePjax', [$this, 'canLikePjax']),
            new \Twig_SimpleFunction('issetPOST', [$this, 'issetPOST']),
            new \Twig_SimpleFunction('currentLang', [$this, 'currentLang']),
            new \Twig_SimpleFunction('currentLangCeo', [$this, 'currentLangCeo']),
            new \Twig_SimpleFunction('currentYear', [$this, 'currentYear']),
            new \Twig_SimpleFunction('noIndex', [$this, 'noIndex']),
            new \Twig_SimpleFunction('currentURL', [$this, 'currentURL']),
            new \Twig_SimpleFunction('strReplace', [$this, 'strReplace']),
            new \Twig_SimpleFunction('demoAccountForm', [$this, 'demoAccountForm']),
            new \Twig_SimpleFunction('isPtmcTeam', [$this, 'isPtmcTeam']),
            new \Twig_SimpleFunction('simpleXMLElement', [$this, 'simpleXMLElement']),
            new \Twig_SimpleFunction('jsexpression', [$this, 'jsexpression']),

        ];
    }

    public function getLangInputValue($model, $attribute, $lang_id)
    {


        if ($lang_id == LangInputBehavior::DEFAULT_LANGUAGE) {
            $value = $model->$attribute;

            //echo '<pre>'; var_dump($value,$attribute,LangInput::getTranslate($model, $attribute, $lang_id),$model->attributes);die;
            return $value[$lang_id];
        }


        return LangInput::getTranslate($model, $attribute, $lang_id);
    }

    public function currentLang($model, $attribute)
    {
        if (Yii::$app->language == 'en-US' && isset($model->$attribute))
            return $model->$attribute;

        $lang_id = Languages::getCurrentLangId();

        return LangInput::getTranslate($model, $attribute, $lang_id);

    }

    public function currentLangCeo($model, $attribute)
    {
        if (!$this->currentLang($model, $attribute)) {
            return CeoHelper::getCeoData($attribute);
        }

        /**
         * part where ceo added from admin and we add some thing like ' - PTMC'
         */
        $controller_name = ucfirst(explode('/', yii\helpers\BaseUrl::current())[1]);

        if ($this->currentLang($model, $attribute) && $attribute === 'title' && $controller_name === 'Articles')
            return $this->currentLang($model, $attribute) . ' - PTMC';

        if ($this->currentLang($model, $attribute) && $attribute === 'title' && $controller_name === 'Forum')
            return $this->currentLang($model, $attribute) . ' - PTMC Forum';


        if ($this->currentLang($model, $attribute))
            return $this->currentLang($model, $attribute);

        return 'Protrader Company';
    }


    /**
     * @param $model
     * @param $attribute
     * @return mixed|string
     */
    public function getCeoTwig($model, $attribute)
    {
        // $ceo = \app\components\ceo\Ceo::findEntity($model);
        if (is_object($model) && $model->$attribute) {
            return $model->$attribute;
        }

        if ($attribute === 'canonical') {
//            return  \yii\helpers\Url::canonical(); // changes old weird method to base framework method
            return CeoHelper::getCanonical();
        }
    }

    /**
     * @param $string
     * @return mixed
     */
    public function strReplace($string)
    {
        return str_replace('/index', '', $string);
    }


    public function registerFlashes()
    {
        Yii::$app->getView()->registerJs("flashes = JSON.parse('" . \yii\helpers\Json::encode(Yii::$app->session->getAllFlashes()) . "');");
    }


    /**
     * @param $model
     * @return mixed
     *
     * take from \app\models\Articles only Articles for first part of form
     *
     * sample  Articles[attr][lang_id]
     */
    public function get_class_name($model)
    {
        $string = get_class($model);

        $explode = explode('\\', $string);

        return $explode[3];
    }

    public function stripTag($html)
    {
        return strip_tags($html);
    }

    public function issetPOST($attr)
    {
        if (Yii::$app->request->post($attr)) return true;

        return false;
    }

    public function issetGET($attr)
    {
        if (Yii::$app->request->get($attr)) return true;

        return false;
    }


    /**
     * @return null
     */
    public function getQueryParams($key = '')
    {
        $id = isset($_GET[$key]) ? $_GET[$key] : null;

        return $id;
    }

    /**
     * @param array $params
     *
     * @return string
     */
    public function createUrl($params = [])
    {
        return Yii::$app->urlManager->createUrl($params);
    }

    /**
     * @param string $_time
     * @param string $format
     *
     * @return string
     */
    public function dateFormat($_time, $format = 'd.m.Y')
    {
        if (!$_time) return date($format);

        return date($format, strtotime($_time));
    }

    /**
     * @param string $_time
     * @param string $format
     *
     * @return string
     */
    public function dateUTCFormat($_time, $format = 'd.m.Y')
    {
        if (!($_time > 0)) return date($format);

        if (is_integer($_time))
            return date($format, $_time);

        return $_time;

    }

    /**
     * @param string $lang
     * @param string $version
     *
     * @return string
     */
    public function createDocumentSearchUrl($lang, $version)
    {
        return Yii::$app->urlManager->createUrl(['knowledgebase/default/search', 'lang' => $lang, 'version' => $version == '2' ? 'pt2' : 'pt3']);
    }

    /**
     * @param string $source
     * @param string $condition
     * @param array $params = []
     * @return string
     */
    public function isCondition($source, $condition)
    {
        if ($condition === true) {
            return $source;
        }

        return false;
    }

    /**
     * @param string $source
     * @param string $role
     * @param array $params = []
     * @return string
     */
    public function isUserCan($source, $role_constant, $params = [])
    {
        if (!@constant('app\models\rule\RuleAlias::' . $role_constant)) return $source;

        $role = constant('app\models\rule\RuleAlias::' . $role_constant);

        if (Yii::$app->user->can($role, $params)) {
            return $source;
        }

        return false;

    }

    /**
     * @param string $source
     * @param string $role
     * @param array $params = []
     * @return string
     */
    public function isUserCannot($source, $role_constant, $params = [])
    {
        if (!@constant('app\models\rule\RuleAlias::' . $role_constant)) return $source;

        $role = constant('app\models\rule\RuleAlias::' . $role_constant);

        if (!Yii::$app->user->can($role, $params)) {
            return $source;
        }

        return false;
    }

    /**
     * @param string $source
     * @param string $role
     * @param array $params = []
     * @return string
     */
    public function isUserPermission($role_constant, $params = [])
    {
        if (!@constant('app\models\rule\RuleAlias::' . $role_constant)) return true;

        $role = constant('app\models\rule\RuleAlias::' . $role_constant);

        if (Yii::$app->user->can($role, $params)) {
            return true;
        }

        return false;
    }

    /**
     * @param string $title
     * @param integer $size
     * @return string
     */
    public function getWrap($title, $size = 27)
    {
        $words = explode(' ', $title);
        $output = '';
        if (count($words)) {
            foreach ($words as $key => $_w) $words[$key] = wordwrap($_w, $size, "<br>", 1);
            $output = implode(' ', $words);
        }

        return $output;
    }


    /**
     * @param string $title
     * @param integer $size
     * @return string
     */
    public function getLength($title, $size = 27)
    {

        return substr($title, 0, $size);
    }

    /**
     * @param string $content
     * @return string
     */
    public function clearHead($source)
    {

        $source = nl2br(trim(strip_tags($source, '<br><br/><p></p><a></a><b></b><strong></strong><img><span></span><table></table><tr></tr><td></td><th></th><tbody></tbody><ul></ul><li></li><tfoot></tfoot><thead></thead><caption></caption><div></div><style></style>')));
        $source = preg_replace('/(?:<br[^>]*>\s*)+/i', "<br/>", $source);
        $source = preg_replace('/(<\/p><br[^>]*>)+/i', "</p>", $source);

        return $source;
    }

    /**
     * @param string $content
     * @return string
     */
    public function getHtmlPurifier($content)
    {
        return HtmlPurifier::process($content);
    }

    public function getName()
    {
        return 'custom_filters';
    }

    /**
     * @param class $class
     * @param string $function
     * @param array $args
     * @return mixed|null
     *    {{ app.user.identity.getUsername(model.user_id) |raw}}
     *   {{ app.user.identity.КgetAvatarLink() |raw}}
     */
    function staticMethod($class, $function, $args = [])
    {
        if (class_exists($class) && method_exists($class, $function))
            return call_user_func_array([$class, $function], $args);

        return null;
    }

    /**
     * @param
     */
    function getStatic($class, $propetry)
    {
        // var_dump(class_exists($class), property_exists($class, $propetry));
        if (class_exists($class) && property_exists($class, $propetry))
            return $class::$$propetry;

        return null;
    }

    public function inArray($needle, $arr)
    {
        if (is_array($arr))
            return in_array($needle, $arr);

        return false;
    }

    public function isMinimal()
    {
        if (isset($_SESSION['minimal']))
            if ($_SESSION['minimal'] == 'true') return 'aside-toggle';

        return 'aside-toggle aside';
    }

    public function initDatePickerAssets()
    {
        $datapicker = new DatePicker([
            'name'          => 'prepare-datepicker',
            'type'          => DatePicker::TYPE_INPUT,
            'value'         => '',
            'options'       => ['style' => 'display:none;'],
            'pluginOptions' => [
                'autoclose' => true,
                'format'    => 'M d, yyyy',
            ]
        ]);
    }

    public function initMultiSelectAssets()
    {
        $datapicker = MultiSelectCustom::widget([
            'id'            => 'prepare-select',
            'options'       => ['multiple' => 'multiple'],
            'data'          => [],
            'value'         => null,
            'name'          => 'prepare-select',
            'clientOptions' => [],
        ]);
    }

    /**
     * Get abs of $var
     * @param $var
     * @return number
     */
    public function absolute($var)
    {
        return abs($var);
    }

    /**
     * Get count of $arr
     * @param $arr
     * @return int
     */
    public function count($arr)
    {
        return count($arr);
    }

    /**
     * @param $string_to_translate
     * @return array|string
     */
    public function translate($string_to_translate, $category = 'app')
    {
        if (is_array($string_to_translate)) {
            $translated_arr = [];

            foreach ($string_to_translate as $key => $to_translate) {
                $translated_arr[$key] = Yii::t($category, $to_translate);
            }

            return $translated_arr;

        } else {
            return \Yii::t('app', $string_to_translate);
        }
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return Yii::$app->language;
    }

    /**
     * @return string
     */
    public function getShortLang()
    {
        return Yii::$app->language == 'ru-RU' ? 'ru' : 'en';
    }

    /**
     * as default attribute for Search using is USERNAME
     * but every page works with this normally(seems like)
     * @return string
     */
    public static function selectFilter($arr = [], $current = null, $name = null, $multiple = null, $id = null)
    {

        if (!count($arr)) {
            $str = '<input placeholder="no data" disabled class="form-control">';

        } else {
            $str = \app\components\multi_select\CustomMultiSelect::widget([
                'data'  => $arr,
                'value' => $current,
                'id'    => $id,

                'name' => $name,

                'options'       => ['multiple' => $multiple, 'class' => 'form-group clear field-video-category_id has-success'],
                'clientOptions' => [
                    'enableCaseInsensitiveFiltering' => true,
                    'numberDisplayed'                => 1,
                    'allSelectedText'                => false
                ],

            ]);
        }

        return $str;
    }


    /**
     * as default attribute for Search using is USERNAME
     * but every page works with this normally(seems like)
     * @return string
     */
    public function selectFilterForm($arr = [], $id = null, $name, $params = [])
    {
        //prompt not working
        $arr[0] = Yii::t('app', 'Select Country');
        ksort($arr);

        $str = \app\components\multi_select\CustomMultiSelect::widget([// \dosamigos\multiselect\MultiSelect ///
            //$str = \app\components\MultiSelectCustom::widget([// ///
            'data'          => $arr,
            'value'         => $id,
            'name'          => $name,
            'options'       => [
                'id'       => $id,
                'multiple' => false,
                // 'class' => 'select-border-color border-warning',
            ],
            'clientOptions' => [
                'enableCaseInsensitiveFiltering' => true,
                'numberDisplayed'                => 1,
                'allSelectedText'                => false,
                // 'autofocus'=>true,
                $params,
                'buttonWidth'                    => '100%',

            ],

        ]);

        return $str;
    }

    public function getListView($dataProvider, $template, $pager, $filter = false)
    {
        return ListView::widget([
            'dataProvider' => $dataProvider,
            'summary'      => false,
            'pager'        => $pager,
            'itemView'     => function ($model, $key, $index, $widget) use ($filter, $template) {

                if ($filter === false)
                    return Yii::$app->getView()->render($template, ['model' => $model]);

                if ($filter === 'first-column')
                    if (($index + 1) % 2 == 1)
                        return Yii::$app->getView()->render($template, ['model' => $model]);

                if ($filter === 'second-column')
                    if (($index + 1) % 2 == 0)
                        return Yii::$app->getView()->render($template, ['model' => $model]);
            },
        ]);
    }

    public function getViewCounter($model)
    {
        return Yii::$app->counter->getCount($model);
    }

    public function getImage($attr = null, $width = 800, $height = 300)
    {
        return GeneralSettingForm::getPictureUrl($attr, $width, $height);

    }

    public function getPreviewText($text, $length)
    {
        return StringHelper::truncate(strip_tags($text), $length);
    }

    public function getPreviewTextWords($text, $length)
    {
        return StringHelper::truncateWords(strip_tags($text), $length);
    }

    public function getClassName($model)
    {
        if (is_object($model))
            return get_class($model);
    }

    public function getSearchValue()
    {
        return Yii::$app->request->get('search');
    }


    public function getFeedbackForm()
    {

        return new FeedbackForm();

    }


    public function getCommentModel()
    {

        return new FrmComment();

    }


    public function canLike(Entity $entity)
    {
        if (Yii::$app->getUser()->isGuest)
            return true;

        if (isset(Yii::$app->user->identity->id) && !empty(Yii::$app->user->identity->id)) {
            $model = Likes::find()->where([
                'entity'    => $entity->getTypeEntity(),
                'entity_id' => $entity->id,
                'user_id'   => Yii::$app->user->identity->id,
            ])->one();

            //if($entity->counts->getPercent() == 100)
            //    return 'disabled';

            if (!is_object($model))
                return true;
        }

        return 'disabled';
    }

    /**
     * @param $alias
     * @return string
     */
    public function getPagesUrl($alias)
    {
        return '/' . $alias;
    }

    public function currentYear()
    {
        return date("Y");
    }

    public function noIndex()
    {
        if (stripos(yii\helpers\Url::to(''), '?page') !== false)
            return '';

        if (stripos(yii\helpers\Url::to(''), '?') !== false)
            return '<meta name="robots" content="noindex, nofollow"/>';

        return '';
    }

    public function currentURL()
    {
        return yii\helpers\Url::to('');
    }

    public function demoAccountForm()
    {
        return new \app\models\forms\DemoAccountForm();
    }

    public function isPtmcTeam($user_id)
    {
        $model = User::findOne($user_id);

        if (is_object($model))
            if ($model->is_ptmc_team === 1)
                return true;

        return false;
    }

    function simpleXMLElement($content)
    {
        return new \SimpleXMLElement($content);
    }

    function jsexpression($js)
    {
        return new yii\web\JsExpression($js);
    }

}