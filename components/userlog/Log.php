<?php
/**
 * Created by Zaitsev Oleg.
 * User: php
 * Date: 02.07.15
 * Time: 16:16
 */


namespace app\components\userlog;

use yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use app\models\user\UserLog;

class Log extends Component
{
    public static $actions = [
      '0' => 'delete',
      '1' => 'create',
      '2' => 'update',
    ];

    public function setLog($model, $action, $data = null, $comment = null){
        $userLog = new UserLog();
        $userLog->model = $model;
        $userLog->action = $action;
        $userLog->ip_address = $_SERVER['REMOTE_ADDR'];
        $userLog->user_agent = $_SERVER['HTTP_USER_AGENT'];
        $userLog->user_id = isset(Yii::$app->user->identity->id) ? Yii::$app->user->identity->id : null;
        $userLog->company_id = isset(Yii::$app->user->identity->company->id) ? Yii::$app->user->identity->company->id : null;
        $userLog->data = json_encode($data);
        $userLog->comment = $comment;
        $userLog->save();
    }

}