<?php
/**
 * Created by PhpStorm.
 * @author: Roman Budnitsky
 * @date: 17.11.14 12:06
 * @copyright Copyright (c) 2014 PFSOFT LLC
 *
 * Class DynamicModule
 */

namespace app\components;

use app\models\user\User;
use yii;
use yii\base\BootstrapInterface;
use yii\base\Component;

class ModulesLoader extends Component implements BootstrapInterface {

    public $extendMainMenu = [];

    public function setModules($modules) {
        if(!count($modules)) return;
        $reload = false;
        $session = Yii::$app->session;
        $loadedModules = ($session->has('_modules') && $str = $session->get('_modules')) ? $this->decodeModules( $str ) : [];
        foreach($modules as $id => $module) {
            if(!isset($loadedModules[$id])) {
                $loadedModules[$id] = $module;
                $reload = true;
            }
        }
        $session->set('_modules', $this->encodeModules($loadedModules));
        if($reload) Yii::$app->response->refresh();
    }

    public function decodeModules($str) {
        try {
            $modules = $str;
            $modules = base64_decode($modules);
            $modules = str_replace(md5(Yii::$app->session->id), '', $modules);
            $modules = base64_decode($modules);
            $modules = str_replace(sha1(Yii::$app->session->id), '', $modules);
            $modules = yii\helpers\Json::decode($modules);
            return $modules;
        } catch(\Exception $e) {
            Yii::$app->session->set('_modules', null);
            Yii::$app->response->refresh();
        }
    }

    public static function encodeModules($modules) {
        return base64_encode(base64_encode(sha1(Yii::$app->session->id) . yii\helpers\Json::encode($modules)) . md5(Yii::$app->session->id));
    }

    public function bootstrap($app) {
        if($app->session->has('_modules')) {
            $modules = $this->decodeModules( $app->session->get('_modules') );
            foreach($modules as $id => $module) {
                Yii::$app->setModule($id, $module);
            }
        } else {
            //if (!Yii::$app->user->isGuest) Yii::$app->user->identity->loadModules();
        }

        // Adding menu and url rules of modules
        if(method_exists(Yii::$app, 'getModules')) {
            $modules = Yii::$app->modules;
            if (count($modules) > 0)
                foreach ($modules as $id => $_module) {
                    if (is_array($_module)) {
                        if (isset($_module['class']::$extendMainMenu))
                            $this->extendMainMenu = yii\helpers\ArrayHelper::merge($this->extendMainMenu, $_module['class']::$extendMainMenu);
                        if (isset($_module['class']::$urlRules))
                            Yii::$app->urlManager->addRules($_module['class']::$urlRules);
                    }
                }
        }

    }

    public function extendMenuItems($items = [], $module_id = null) {
        $extItems = $this->extendMainMenu;
        foreach($extItems as $i => $item) {
            if($item['module'] == $module_id) {
                $extItems[$i]['active'] = true;
            }
            unset($extItems[$i]['module']);
            if(yii::$app->user->can($item['can'], ['company' => Yii::$app->user->identity->company])) {
                unset($extItems[$i]['can']);
            } else {
                unset($extItems[$i]);
            }
        }
        return yii\helpers\ArrayHelper::merge($items, $extItems);
    }

} 