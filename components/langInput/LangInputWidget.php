<?php
namespace app\components\langInput;

use Yii;
use yii\base\Widget;
use app\models\lang\Languages;

class LangInputWidget extends Widget
{
    public $type;
    public $model;
    public $attribute;
    public $form;
    public $type_set = [
        'text_input',
        'text_area',
        'imperavi'
    ];

    public static $input_id = 100;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        self::$input_id++;

        $languages = Languages::find()->all();
        $attribute = $this->attribute;
        $translation = [];

        if(count($languages) > 0)
            foreach($languages as $_lang)
                if(isset($_lang->id)) {

                    if($_lang->id == LangInputBehavior::DEFAULT_LANGUAGE )
                        $translation[$_lang->id] = $this->model->$attribute;

                    if($_lang->id != LangInputBehavior::DEFAULT_LANGUAGE)
                        $translation[$_lang->id] =
                        LangInput::getTranslate($this->model, $attribute, $_lang->id);

                }

        $this->model->$attribute = $translation;

        if(in_array($this->type, $this->type_set))
            return Yii::$app->getView()->render('@app/components/langInput/views/index.twig',[
                'languages' => $languages,
                'type'      => $this->type,
                'model'     => $this->model,
                'attribute' => $this->attribute,
                'attribute_label' => $this->model->getAttributeLabel($attribute),
                'form'      => $this->form,
                'input_id'  => self::$input_id,
            ]);

    }

}