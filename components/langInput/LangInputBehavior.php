<?php

namespace app\components\langInput;

use app\components\Entity;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use app\models\lang\Translations;
use yii\helpers\VarDumper;

class LangInputBehavior extends Behavior
{
    /*
    |--------------------------------------------------------------------------
    | Properties & constants
    |--------------------------------------------------------------------------
    */

    const DEFAULT_LANGUAGE = 1; # EN
    const RUSSIAN_LANGUAGE = 2; # RU

    public $values;

    public $attributes;

    /*
    |--------------------------------------------------------------------------
    | Configuration
    |--------------------------------------------------------------------------
    */

    /**
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',

            ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterUpdate',
        ];
    }

    /**
     * @param $event
     */
    public function beforeValidate($event)
    {

        $this->setValues($event, $this);

        $this->replaceAttribute($event);
    }

    /**
     * @param $event
     */
    public function afterInsert($event)
    {

        if (is_array($this->values) && count($this->values) > 0) //added igor 17 february
            foreach ($this->values as $attribute => $languages) {

                if (is_array($languages) && count($languages) > 0) {

                    foreach ($languages as $language => $content) {

                        $translation = new Translations();
                        $this->saveTranslations($event->sender, $translation, $language, $attribute, $content);
                    }

                }

            }
    }

    /**
     * TODO гарантироваго в $event->sender должен быть Entity
     * @param $event
     */
    public function afterUpdate($event)
    {
        if (is_array($this->values) && count($this->values) > 0)
            foreach ($this->values as $attribute => $languages) {
                if (is_array($languages) && count($languages) > 0)
                    foreach ($languages as $language => $content) {

                        $translation = Translations::find()->where([
                            'lang_id'   => $language,
                            'entity'    => $event->sender->getTypeEntity(),
                            'entity_id' => $event->sender->id,
                            'attribute' => $attribute,
                        ])->one();

                        if (!is_object($translation))
                            $translation = new Translations();

                        $this->saveTranslations($event->sender, $translation, $language, $attribute, $content);

                    }
            }

    }


    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */

    /**
     * @param $event
     */
    public function setValues($event)
    {
        //if (is_array($this->values) && count($this->values) > 0)
        foreach ($this->attributes as $attribute) {
            $this->values[$attribute] = $event->sender->$attribute;
        }
    }

    /**
     * @return mixed
     */
    public function setAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param $event
     */
    public function replaceAttribute(&$event)
    {
        if ($event->sender->scenario <> 'delete') {

            //don't add here if array...will bwe exception
            foreach ($this->attributes as $key => $val) {

                if(!is_array($event->sender->$val))
                    continue;

                $index = $event->sender->$val;

                if (isset($index[self::DEFAULT_LANGUAGE]))
                    $event->sender->$val = $index[self::DEFAULT_LANGUAGE];

            }

        }
    }

    /**
     * @param Entity $entity
     * @param $translation
     * @param $language
     * @param $attribute
     * @param $content
     */
    public function saveTranslations(Entity $entity, $translation, $language, $attribute, $content)
    {
        $translation->lang_id = $language;
        $translation->entity = $entity->getTypeEntity();
        $translation->entity_id = $entity->id;
        $translation->attribute = $attribute;
        $translation->content = $content;
        $translation->save(false);
    }

}

