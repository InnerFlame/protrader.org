<?php
/**
 * Created by PhpStorm.
 * User: php
 * Date: 31.12.15
 * Time: 11:55
 */

namespace app\components\langInput;

use app\components\Entity;
use app\models\CustomModel;
use app\models\lang\Translations;

class LangInput extends Translations
{
    /*
    |--------------------------------------------------------------------------
    | Constants && properties
    |--------------------------------------------------------------------------
    */

    public static $_pull = [];

    /*
    |--------------------------------------------------------------------------
    | Model configurations
    |--------------------------------------------------------------------------
    */

    public static function getTranslate($model, $attribute, $lang)
    {
           if($lang > 0 && isset($model->id, $attribute))
           {
                $translate = Translations::findOne([
                   'entity' => $model->getTypeEntity(),
                   'entity_id' => $model->id,
                   'attribute' => $attribute,
                   'lang_id' => (int) $lang
                ]);

               if(is_object($translate)) return $translate->content;
           }
    }

}