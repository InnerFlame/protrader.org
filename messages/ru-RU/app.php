<?php

return [


    # Menu
    'Home'                => 'Главная',
    'Knowledge Base'      => 'База знаний',
    'About PTMC' => 'Про PTMC',
    'PTMC'                => 'PTMC',
    'Admin Panel'         => 'Админ часть',
    'Common' => 'Основные',
    'Category Features' => 'Категории фич',
    'About Application'   => 'О приложении',
    'Brokers'             => 'Брокеры',
    'Download'            => 'Скачать',
    'Documentation'       => 'Документация',
    'Video Lessons'       => 'Видео-уроки',
    'FAQ'                 => 'FAQ',
    'Analytics'           => 'Аналитика',
    'Articles'            => 'Статьи',
    'Articles List'       => 'Статьи',
    'Chart'               => 'Чарт',
    'Reviews'             => 'Обзоры',
    'Communication'       => 'Общение',
    'Forum/Blog'          => 'Форум/Блог',
    'Query Features'      => 'Запрос фич',
    'Dashboard'           => 'Панель управления',
    'Static Pages'        => 'Статические страницы',
    'Pages List'          => 'Список cтраниц',
    'Improvments RTMC'    => 'Улучшения RTMC',
    'Tags'                => 'Ключевые слова',
    'Articles Category'   => 'Категории статей',
    'News Category'       => 'Категории новостей',
    'Video List'          => 'Список видео',
    'Video-Lessons'       => 'Видео уроки',
    'Сommon'              => 'Основные',
    'Slider'              => 'Слайдер',
    'Video Category List' => 'Видео Список категорий',
    'Play List'           => 'Список воспроизведения',
    'Forum'               => 'Форум',
    'Category'            => 'Категории',
    'Topic'               => 'Темы',
    'Posts'               => 'Посты',
    'FAQ Category'        => 'FAQ категории',
    'Settings'            => 'Настройки',
    'Translations'        => 'Переводы',
    'Advertising'         => 'Реклама',
    'Installation'        => 'Установки',
    'Market'              => 'Маркет',
    'News'                => 'Новости',
    'Functionality'          => 'Функционал',
    'Features'          => 'Фичи',
    'Languages'           => 'Языки',
    'Users'               => 'Пользователи',

    # Buttons
    //

    # Messages
    //

    'Log in'              => 'Авторизация',
    'Registration'        => 'Регистрация',
    'Password'            => 'Пароль',
    'Answer'              => 'Ответ',
    'Cancel'              => 'Отмена',
    'Save'                => 'Сохранить',
    'Add'                 => 'Добавить',
    'Edit'                => 'Редактирование',
    'Link to confirm registration sent to your e-mail' => 'Ссылка для подтверждения регистрации отправлена вам на e-mail',

];
