<?php
/**
 * Created by PhpStorm.
 * @author: Roman Budnitsky
 * @date: 23.10.14 14:48
 * @copyright Copyright (c) 2014 PFSOFT LLC
 *
 * Class BaseFlatAsset
 * @package app\assets
 */


namespace app\assets;

use yii\web\AssetBundle;

class BaseFlatAsset extends AssetBundle {
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800',
        'https://fonts.googleapis.com/css?family=Raleway:300,200,100',
        'flat/fonts/font-awesome-4/css/font-awesome.min.css',
    ];
    public $js = [
        'js/main.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
    public $jsOptions = [
        'type' => 'text/javascript'
    ];
}
