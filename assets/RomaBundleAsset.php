<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2016
 */

namespace app\assets;


use yii\web\AssetBundle;

class RomaBundleAsset extends AssetBundle
{

    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/style.css',
    ];
    public $depends = [
        'app\assets\LimitLessAsset',
    ];

}