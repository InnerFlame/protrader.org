<?php
/**
 * Created by PhpStorm.
 * @author: Roman Budnitsky
 * @date: 24.10.14 15:45
 * @copyright Copyright (c) 2014 PFSOFT LLC
 *
 * Class FlatAsset
 * @package app\assets
 */


namespace app\assets;

use yii\web\AssetBundle;

class DualLimitlessAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/limitless';

    public $css = [
        // Global stylesheets
        'assets/css/icons/icomoon/styles.css',
        'assets/css/minified/bootstrap.min.css',
        'assets/css/minified/core.min.css',
        'assets/css/minified/components.min.css',
        'assets/css/minified/colors.min.css',
        // end global stylesheets
    ];

    public $js = [
//        <!-- Core JS files -->
        'assets/js/plugins/loaders/pace.min.js',
        //'assets/js/core/libraries/jquery.min.js',
        'assets/js/core/libraries/bootstrap.min.js',
        'assets/js/plugins/loaders/blockui.min.js',
//        <!-- /core JS files -->
//
//        <!-- Theme JS files -->
        'assets/js/plugins/forms/styling/switchery.min.js',
	    'assets/js/plugins/ui/prism.min.js',
        'assets/js/core/app.js',
        'assets/js/pages/sidebar_dual.js',
//        <!-- /theme JS files -->
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\jui\JuiAsset'
    ];
    public $jsOptions = [
        'type' => 'text/javascript'
    ];
}