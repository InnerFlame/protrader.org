<?php
/**
 * Created by PhpStorm.
 * @author: Roman Budnitsky
 * @date: 23.10.14 14:48
 * @copyright Copyright (c) 2014 PFSOFT LLC
 *
 * Class BaseFlatAsset
 * @package app\assets
 */


namespace app\assets;

use yii\web\AssetBundle;

class BaseLimitLessAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'limitless/fonts/fonts.css',
    ];

    public $js = [
        'js/main.js',
        'js/ajax.js',
        'js/application.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];
    public $jsOptions = [
        'type' => 'text/javascript'
    ];

}
