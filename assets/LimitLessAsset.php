<?php
/**
 * Created by PhpStorm.
 * @author: Roman Budnitsky
 * @date: 24.10.14 15:45
 * @copyright Copyright (c) 2014 PFSOFT LLC
 *
 * Class FlatAsset
 * @package app\assets
 */


namespace app\assets;

use yii\web\AssetBundle;

class LimitlessAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        // Global stylesheets
        // 'assets/css/minified/bootstrap.min.css',
        'limitless/assets/css/minified/core.min.css',
        'limitless/assets/css/minified/components.min.css',
        'limitless/assets/css/icons/icomoon/styles.css',
        'limitless/assets/css/icons/fontawesome/styles.min.css',
        'limitless/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.min.css',

        // 'assets/css/minified/bootstrap.min.css',
        // 'assets/css/bootstrap-datepicker3.css',
        // 'assets/css/datepicker-kv.css',
        // 'assets/css/minified/kv-widgets.css',

        // 'assets/css/minified/colors.min.css',
        // '/css/owl.carousel.css',
        // '/css/custom.css?ver=1.3',
        //'/css/extra-roman.css?ver=1.2',
        // end global stylesheets
    ];


    public $js = [
//        <!-- Core JS files -->
        // 'assets/js/plugins/loaders/pace.min.js',
        //'assets/js/core/libraries/jquery.min.js',
        // 'assets/js/core/libraries/bootstrap.min.js',
        'limitless/assets/js/plugins/loaders/blockui.min.js',
//        <!-- /core JS files -->
//n
//        <!-- Theme JS files -->
        // 'assets/js/core/app.js',
//        <!-- /theme JS files -->
//        <!-- Custom JS files -->
        //'js/custom.js',
//        <!-- /theme JS files -->


        //'assets/custom/main_script.js',



        // 'assets/js/plugins/ui/nicescroll.min.js',

        // 'assets/js/pages/form_bootstrap_select.js',

        // 'assets/js/plugins/forms/selects/bootstrap_select.min.js',

      //  'assets/js/ckeditor/ckeditor.js',


        // 'assets/js/plugins/ui/drilldown.js',

        'limitless/assets/js/plugins/forms/styling/uniform.min.js',

       // 'assets/js/plugins/forms/styling/switchery.min.js',

        // 'assets/js/plugins/forms/styling/switch.min.js',

        // 'assets/js/plugins/ui/headroom/headroom_jquery.min.js',

        // 'assets/js/plugins/ui/headroom/headroom.min.js',

        // 'assets/js/plugins/buttons/hover_dropdown.min.js',

         'limitless/assets/js/plugins/media/fancybox.min.js',

        // 'assets/js/pages/components_thumbnails.js',

        // 'assets/js/plugins/forms/wizards/stepy.min.js',


        'limitless/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.js',

      // 'assets/js/pages/form_checkboxes_radios.js',




        'limitless/assets/js/plugins/notifications/sweet_alert.min.js',


        // 'assets/js/plugins/forms/selects/bootstrap_multiselect.js', edit profile

        //  'assets/js/plugins/pickers/datepicker.js',//for multiselect drop down
        //   'assets/js/plugins/pickers/datepicker-kv.js',//for multiselect drop down

        # https://github.com/google/code-prettify/blob/master/docs/getting_started.md
        // 'https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js',

        // 'js/jquery.colorbox.js',
        // 'js/mobile-scripts.js',
        // 'js/jquery.maskedinput.min.js',
        // 'js/owl/owl.carousel.min.js',
        'limitless/js/main.js',

        //for selection partly comments
        // '/js/jquery.selection.js',
        // 'assets/js/plugins/uploaders/fileinput.min.js',
        // 'assets/js/plugins/uploaders/plupload/plupload.full.min.js',
        // 'assets/js/plugins/uploaders/plupload/plupload.queue.min.js',
        // 'assets/js/pages/uploader_plupload.js',



    ];
    public $jsOptions = [
        'type' => 'text/javascript',
    ];

    public $depends = [
        'app\assets\_vendor\CustomYiiAsset',
        'app\assets\_vendor\CustomJuiAsset',
    ];

}