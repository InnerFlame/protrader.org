<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class CrossTradeAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'cross/cross.css',
        'http://webfonts.ru/import/leaguegothic.css',
        'http://webfonts.ru/import/opensans.css',
    ];

    public $js = [
        'cross/crossMain.js',
    ];

    public $depends = [
        'app\assets\RomaBundleAsset',
    ];

    public $jsOptions = [
        'type' => 'text/javascript'
    ];
}