<?php
/**
 * Created by PhpStorm.
 * User: P.Bilik
 * Date: 04.02.2016
 * Time: 18:55
 */

namespace app\assets;


use yii\web\AssetBundle;

class ImperaviAsset extends AssetBundle
{
    public $baseUrl = '@web/js/plugins/imperavi';

    public $css = [
        'alignment/alignment.css',
        'clips/clips.css'
    ];
    public $js = [
        'spoiler.js',
        'alignment/alignment.js',
        'clips/clips.js',
    ];
}