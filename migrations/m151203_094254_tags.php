<?php

use yii\db\Schema;
use yii\db\Migration;

class m151203_094254_tags extends Migration
{


    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable( '{{%artcl_tag}}', [
            'id'         => Schema::TYPE_INTEGER . '(11) NOT NULL AUTO_INCREMENT',
            'title'      => Schema::TYPE_STRING . '(255) NOT NULL',
            'published'     => Schema::TYPE_INTEGER  . '(11) NULL',
            'count'      => Schema::TYPE_INTEGER . '(11) DEFAULT 0',
            'created_at' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'updated_at' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'deleted_at' => Schema::TYPE_INTEGER . '(11) DEFAULT NULL',
            'PRIMARY KEY (`id`)'
        ], $tableOptions);
    
       
        $this->createTable('{{%artcl_tag_relations}}', [
            'id'         => Schema::TYPE_INTEGER . '(11) NOT NULL AUTO_INCREMENT',
            'article_id'  => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'tag_id'     => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'created_at' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'updated_at' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'deleted_at' => Schema::TYPE_INTEGER . '(11) DEFAULT NULL',
            'PRIMARY KEY (`id`)'
        ], $tableOptions);

        $this->createIndex('article_id', '{{%artcl_tag_relations}}', 'article_id', false);
        $this->createIndex('tag_id', '{{%artcl_tag_relations}}', 'tag_id', false);

        $this->addForeignKey('fk_tbl_article_tag_article_id', '{{%artcl_tag_relations}}', 'article_id', 'artcl_articles', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_tbl_article_tag_tag_id', '{{%artcl_tag_relations}}', 'tag_id', 'artcl_tag', 'id', 'CASCADE', 'CASCADE');
    }


    public function down()
    {
        $this->dropTable('{{%artcl_tag_relations}}');
        $this->dropTable('{{%artcl_tag}}');
    }
}
