<?php

use yii\db\Schema;
use yii\db\Migration;

class m151224_165518_add_column_alias_table_broker_ extends Migration
{
    public function up()
    {
        $this->addColumn('{{%broker}}', 'alias', "VARCHAR(60) NOT NULL  AFTER `name`");
        $this->addColumn('{{%frm_improvements}}', 'alias', "VARCHAR(60) NOT NULL  AFTER `title`");

        $this->db->schema->refresh();
    }

    public function down()
    {
        $this->dropColumn('{{%broker}}', 'alias');
        $this->dropColumn('{{%frm_improvements}}', 'alias');
    }
}
