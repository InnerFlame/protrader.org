<?php

use yii\db\Migration;

class m160216_102428_add_send_to_notifications_table extends Migration
{
    public function up()
    {
        $this->addColumn('{{%notifications}}', 'send', 'tinyint(1) NOT NULL DEFAULT 0');
        $this->dropColumn('{{%notifications}}', 'read');
        $this->addColumn('{{%notifications}}', 'read', 'tinyint(1) NOT NULL DEFAULT 0');
        $this->addColumn('{{%notifications}}', 'send_date', 'timestamp');
    }

    public function down()
    {
        $this->dropColumn('{{%notifications}}', 'send');
        $this->dropColumn('{{%notifications}}', 'send_date');
        $this->dropColumn('{{%notifications}}', 'read');
        $this->addColumn('{{%notifications}}', 'read', 'tinyint(1) DEFAULT 0');
    }
}
