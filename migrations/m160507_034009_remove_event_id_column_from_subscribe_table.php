<?php

use yii\db\Migration;

class m160507_034009_remove_event_id_column_from_subscribe_table extends Migration
{
    const TBL_NAME = '{{%notify_subscribes}}';

    public function up()
    {
        $this->dropForeignKey('fk_subscribes_to_events', self::TBL_NAME);
        $this->dropColumn(self::TBL_NAME, 'event_id');
    }

    public function down()
    {
        $this->addColumn(self::TBL_NAME, 'event_id', 'INT(5) NOT NULL');
        $this->addForeignKey('fk_subscribes_to_events', self::TBL_NAME, 'event_id', '{{%notify_events}}', 'id', 'CASCADE', 'CASCADE');
        return true;
    }

}
