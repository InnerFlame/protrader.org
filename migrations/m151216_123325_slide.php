<?php

use yii\db\Schema;
use yii\db\Migration;

class m151216_123325_slide extends Migration
{
    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }


        $this->createTable('{{%slide}}', [
            'id' => 'INT(8) NOT NULL AUTO_INCREMENT',
            'title' => 'VARCHAR(64) NOT NULL',
            'pictures' => 'VARCHAR(128) NOT NULL',
            'link' => 'VARCHAR(128) NOT NULL',
            'description' => 'VARCHAR(128) NOT NULL',
            'weight' => 'VARCHAR(256) NOT NULL',
            'data' => 'text',
            'PRIMARY KEY (`id`),
                     INDEX fk_slide_custom_field_settings_idx (`id` ASC)'
        ], $tableOptions);

    }



    public function down()
    {
        $this->dropTable('{{%slide}}');

    }

}


