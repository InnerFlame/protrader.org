<?php

use yii\db\Schema;
use yii\db\Migration;

class m160212_153504_update_field_entity2 extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        #news_news
        $this->addColumn('{{%news_news}}', 'deleted', "smallint(4)  DEFAULT 0 AFTER `published`");

        #news_category
        $this->addColumn('{{%news_category}}', 'deleted', "smallint(4)  DEFAULT 0 AFTER `published`");

        #news_news
        $this->dropColumn('{{%user}}', 'deleted_at');
        $this->addColumn('{{%user}}', 'deleted', "smallint(4)  DEFAULT 0 AFTER `birth_at`");

        $this->db->schema->refresh();
    }

    public function safeDown()
    {
        #news_news
        $this->dropColumn('{{%news_news}}', 'deleted');

        #news_category
        $this->dropColumn('{{%news_category}}', 'deleted');

        #news_news
        $this->dropColumn('{{%user}}', 'deleted');
        $this->addColumn('{{%user}}', 'deleted_at', "int(11) DEFAULT NULL AFTER `created_at`");
    }
}
