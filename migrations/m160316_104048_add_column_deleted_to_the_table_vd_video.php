<?php

use yii\db\Schema;
use yii\db\Migration;

class m160316_104048_add_column_deleted_to_the_table_vd_video extends Migration
{
    public function up()
    {
        $this->addColumn('{{%vd_video}}', 'deleted', "smallint(4)  DEFAULT 0 AFTER `published`");
    }

    public function down()
    {
        $this->dropColumn('{{%vd_video}}', 'deleted');
    }


}
