<?php

use yii\db\Schema;
use yii\db\Migration;

class m160127_152251_queue extends Migration
{
    public $table_name = '{{%queue}}';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table_name, [
            'id'          => Schema::TYPE_INTEGER . '(11) NOT NULL AUTO_INCREMENT',
            'entity'      => Schema::TYPE_STRING . '(255) NOT NULL',
            'entity_id'   => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'event'       => Schema::TYPE_STRING . '(255) NOT NULL',
            'operated_at' => Schema::TYPE_INTEGER . '(11) DEFAULT NULL',
            'PRIMARY KEY (`id`)'
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable($this->table_name);
    }
}
