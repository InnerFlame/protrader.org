<?php

use yii\db\Migration;

class m160706_120317_init_xmpp_tables extends Migration
{
    public function up()
    {
        $this->createTable('{{%xmpp_users}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'username' => $this->string()->notNull(),
            'token' => $this->string(255)->notNull(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp()
        ]);

        $this->addForeignKey('{{fk_xmpp_users_to_user}}', '{{%xmpp_users}}', 'user_id', 'user', 'id', 'CASCADE');

        $result = $this->getDb()->createCommand('SELECT * FROM `user` WHERE  `email` IS NOT NULL')->queryAll();

        $users = \yii\helpers\ArrayHelper::map($result, 'id', function($item){
            $username = preg_replace('/@/', '[u+0040]', $item['email']);
            return ['user_id' => $item['id'], 'username' => $username, 'token' => Yii::$app->security->generateRandomString(15)];
        });

        $this->getDb()->createCommand()->batchInsert('{{%xmpp_users}}', ['user_id', 'username', 'token'], $users)->execute();
        return true;
    }

    public function down()
    {
        $this->dropForeignKey('{{fk_xmpp_users_to_user}}', '{{%xmpp_users}}');
        $this->dropTable('{{%xmpp_users}}');
        return true;
    }
}
