<?php

use yii\db\Schema;
use yii\db\Migration;

class m151209_084505_role extends Migration
{

    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
            $tableOptionsMyISAM = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=MyISAM';
        }

        $this->createTable('{{%role}}', [
            'id' => 'INT(8) NOT NULL AUTO_INCREMENT',
            'name' => 'VARCHAR(64) NOT NULL',
            'alias' => 'VARCHAR(64) NOT NULL',
            'comment' => 'VARCHAR(128) DEFAULT NULL',
            'PRIMARY KEY (`id`, `alias`)'
        ], $tableOptions);

        $this->createTable('{{%rule}}', [
            'id' => 'INT(8) NOT NULL AUTO_INCREMENT',
            'parent' => 'INT(8) DEFAULT NULL',
            'type_id' => 'INT(8) DEFAULT NULL',
            'name' => 'VARCHAR(64) NOT NULL',
            'alias' => 'VARCHAR(64) NOT NULL',
            'comment' => 'VARCHAR(128) NOT NULL',
            'data' => 'TEXT DEFAULT NULL',
            'created_at' => 'INT(11) NOT NULL',
            'PRIMARY KEY (`id`, `alias`)'
        ], $tableOptions);

        $this->createTable('{{%role_rules}}', [
            'role_id' => 'INT(8) NOT NULL',
            'rule_id' => 'INT(8) NOT NULL',
            'status' => 'INT(1) DEFAULT 1',
            'PRIMARY KEY (`role_id`, `rule_id`),
             INDEX fk_role_rules_role_idx (`role_id` ASC),
             CONSTRAINT `fk_role_rules_role`
                FOREIGN KEY (`role_id`)
                REFERENCES {{%role}} (`id`)
                ON DELETE CASCADE
                ON UPDATE CASCADE,
             INDEX fk_role_rules_rule_idx (`rule_id` ASC),
             CONSTRAINT `fk_role_rules_rule`
                FOREIGN KEY (`rule_id`)
                REFERENCES {{%rule}} (`id`)
                ON DELETE CASCADE
                ON UPDATE CASCADE'
        ], $tableOptions);

        $this->createTable('{{%user_rules}}', [
            'user_id' => 'INT(8) NOT NULL',
            'rule_id' => 'INT(8) NOT NULL',
            'status' => 'INT(1) DEFAULT 1',
            'PRIMARY KEY (`user_id`, `rule_id`),
             INDEX fk_user_rules_user_idx (`user_id` ASC),
             CONSTRAINT `fk_user_rules_user`
                FOREIGN KEY (`user_id`)
                REFERENCES {{%user}} (`id`)
                ON DELETE CASCADE
                ON UPDATE CASCADE,
             INDEX fk_user_rules_rule_idx (`rule_id` ASC),
             CONSTRAINT `fk_user_rules_rule`
                FOREIGN KEY (`rule_id`)
                REFERENCES {{%rule}} (`id`)
                ON DELETE CASCADE
                ON UPDATE CASCADE'
        ], $tableOptions);


        $this->db->schema->refresh();

        $initDataArr = require(__DIR__ . '/../config/init_migration.php');

        foreach($initDataArr['role'] as $_role)
            $this->insert('{{%role}}', $_role);

    }

    public function down()
    {
        $this->dropTable('{{%user_rules}}');
        $this->dropTable('{{%role_rules}}');
        $this->dropTable('{{%rule}}');
        $this->dropTable('{{%role}}');
    }
}
