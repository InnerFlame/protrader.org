<?php

use yii\db\Schema;
use yii\db\Migration;

class m151221_141708_create_frm_improvements_category extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
            $tableOptionsMyISAM = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=MyISAM';
        }

        $this->createTable('{{%frm_improvements_category}}', [
            'id'         => 'INT(8) NOT NULL AUTO_INCREMENT',
            'title'      => 'VARCHAR(60) NULL DEFAULT NULL',
            'alias'      => 'VARCHAR(256) NULL DEFAULT NULL',
            'data'       => 'VARCHAR(256) NULL DEFAULT NULL',
            'weight'     => 'INT(8) NULL DEFAULT NULL',
            'published'  => 'INT(11) NULL DEFAULT NULL',
            'created_at' => 'INT(11) NULL DEFAULT NULL',
            'updated_at' => 'INT(11) NULL DEFAULT NULL',

            'PRIMARY KEY (`id`)'

        ], $tableOptions);

        $this->addColumn('{{%frm_improvements}}', 'category_id', "INT(8) NOT NULL AFTER `user_id`");

        $this->addColumn('{{%frm_improvements}}', 'is_main', "INT(8) DEFAULT NULL AFTER `category_id`");

        $this->addColumn('{{%frm_improvements}}', 'is_broker', "INT(8) DEFAULT NULL AFTER `is_main`");

        $this->db->schema->refresh();

    }

    public function down()
    {
        $this->dropTable('{{%frm_improvements_category}}');

        $this->dropColumn('{{%frm_improvements}}', 'category_id');

        $this->dropColumn('{{%frm_improvements}}', 'is_main');

        $this->dropColumn('{{%frm_improvements}}', 'is_broker');
    }
}
