<?php

use yii\db\Migration;

class m160825_111804_crosstrade_add_field_country extends Migration
{
    public function up()
    {
        $this->addColumn('{{%crosstrade_subscriber}}', 'demo_login', $this->string(100));
        $this->addColumn('{{%crosstrade_subscriber}}', 'demo_password', $this->string(100));
    }

    public function down()
    {
        $this->dropColumn('{{%crosstrade_subscriber}}', 'demo_login');
        $this->dropColumn('{{%crosstrade_subscriber}}', 'demo_password');
    }
}
