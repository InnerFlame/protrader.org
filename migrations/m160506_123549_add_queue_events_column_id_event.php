<?php

use yii\db\Migration;

class m160506_123549_add_queue_events_column_id_event extends Migration
{
    const TABLE_NAME = '{{%notify_events_queue}}';
    public function up()
    {
        $this->delete(self::TABLE_NAME);
        $this->addColumn(self::TABLE_NAME, 'event_id', 'INT NOT NULL');
        $this->addForeignKey('fk_events_queue_to_events', self::TABLE_NAME, 'event_id', '{{%notify_events}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_events_queue_to_events', self::TABLE_NAME);
        $this->dropColumn(self::TABLE_NAME, 'event_id');
        return true;
    }
}
