 <?php

use yii\db\Schema;
use yii\db\Migration;

class m151216_091255_settings extends Migration
{
    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }


        $this->createTable('{{%settings_custom_field}}', [
            'id' => 'INT(8) NOT NULL AUTO_INCREMENT',
            'name' => 'VARCHAR(64) NOT NULL',
            'alias' => 'VARCHAR(32) NOT NULL',
            'data' => 'VARCHAR(256) NOT NULL',
            'PRIMARY KEY (`id`),
                     INDEX fk_settings_custom_field_settings_idx (`id` ASC)'
        ], $tableOptions);



        $this->createTable('{{%settings_custom_value}}', [
            'field_id' => 'INT(8) NOT NULL',
            'value' => 'text',
            'PRIMARY KEY ( `field_id`),
                     INDEX fk_settings_custom_value_settings_field_idx (`field_id` ASC),
                     CONSTRAINT `fk_settings_custom_value_settings_field`
                        FOREIGN KEY (`field_id`)
                        REFERENCES {{%settings_custom_field}} (`id`)
                        ON DELETE CASCADE
                        ON UPDATE CASCADE'
        ], $tableOptions);
    }


    public function down()
    {
        $this->dropTable('{{%settings_custom_value}}');
        $this->dropTable('{{%settings_custom_field}}');

    }
}
