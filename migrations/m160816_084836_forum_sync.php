<?php

use yii\db\Migration;
use app\modules\forum\models\FrmCategory;
use app\modules\forum\models\FrmTopic;

class m160816_084836_forum_sync extends Migration
{
    public function safeUp()
    {
        $this->syncCategory();
        $this->syncTopic();
        return true;
    }

    public function safeDown()
    {
        return true;
    }

    public function syncCategory()
    {
        $models = FrmCategory::find()->orderBy(['level' => SORT_DESC])->all();

        foreach($models as $model){
            if($model->alias != 'root'){
                $model->alias = $model->getAllParentAlias();
                $model->save(false);
            }
        }
    }

    public function syncTopic()
    {
        $models = FrmTopic::find()->all();

        foreach($models as $model){
            $model->alias = $this->topicAlias($model);
            $model->save(false);
        }
    }

    public function topicAlias($model)
    {
        return $model->category->alias . '/' . $model->alias;
    }
}
