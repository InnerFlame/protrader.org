<?php

use yii\db\Migration;

class m160127_144319_add_nested_seat_to_comments_table extends Migration
{
    public function up()
    {
        $this->dropColumn('{{%frm_comments}}', 'reply_id');
        $this->addColumn('{{%frm_comments}}', 'reply_id', 'INT(11) NOT NULL DEFAULT 0');
        $this->addColumn('{{%frm_comments}}', 'lft', 'INT(11) NOT NULL DEFAULT 0');
        $this->addColumn('{{%frm_comments}}', 'rgt', 'INT(11) NOT NULL DEFAULT 0');
        $this->addColumn('{{%frm_comments}}', 'level', 'INT(11) NOT NULL DEFAULT 0');

        $this->initNestedSets('{{%frm_comments}}');

    }

    public function down()
    {
        $this->dropColumn('{{%frm_comments}}', 'lft');
        $this->dropColumn('{{%frm_comments}}', 'rgt');
        $this->dropColumn('{{%frm_comments}}', 'level');
    }

    protected function initNestedSets($tablename)
    {
        $connection = Yii::$app->getDb();
        $cmd = $connection->createCommand("UPDATE $tablename SET reply_id = id, lft = '1', rgt = '2'");
        $cmd->query();
    }
}
