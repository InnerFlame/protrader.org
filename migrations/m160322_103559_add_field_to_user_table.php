<?php

use yii\db\Migration;

class m160322_103559_add_field_to_user_table extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}','count_votes', 'INT(11) DEFAULT 0');

        $this->update('{{%user}}', [
            'count_votes' => 5
        ]);
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'count_votes');
    }
}
