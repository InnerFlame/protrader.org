<?php

use yii\db\Schema;
use yii\db\Migration;

class m151225_121555_add_column_js_css_main_pages extends Migration
{
    public function up()
    {
        $this->addColumn('{{%main_pages}}', 'css', "TEXT NOT NULL  AFTER `content`");
        $this->addColumn('{{%main_pages}}', 'js', "TEXT NOT NULL  AFTER `css`");


        $this->db->schema->refresh();
    }

    public function down()
    {
        $this->dropColumn('{{%main_pages}}', 'css');
        $this->dropColumn('{{%main_pages}}', 'js');

    }
}
