<?php

use yii\db\Migration;

class m160410_110206_init_codebase extends Migration
{
    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
            $tableOptionsMyISAM = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=MyISAM';
        }

        $this->createTable('{{%cdbs_description}}',[
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'title' => $this->string(255)->notNull(),
            'alias' => $this->string(255)->notNull(),
            'content' => $this->text()->notNull(),
            'views' => $this->integer()->defaultValue(0),
            'count_download' => $this->integer()->defaultValue(0),
            'user_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'deleted' => $this->smallInteger(1)->defaultValue(0),
        ], $tableOptions);

        $this->createTable('{{%cdbs_categories}}',[
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'alias' => $this->string()->notNull(),
            'icon' => $this->string(32)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'deleted' => $this->smallInteger(1)->defaultValue(0),
        ], $tableOptions);

        $this->createTable('{{%cdbs_files}}',[
            'id' => $this->primaryKey(),
            'description_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'type' => $this->string()->notNull(),
            'size' => $this->integer(5)->notNull(),
            'deleted' => $this->smallInteger(1)->defaultValue(0),
        ], $tableOptions);

        $this->addForeignKey('fk_cdbs_description_to_user', '{{%cdbs_description}}', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_cdbs_description_to_cdbs_categories', '{{%cdbs_description}}', 'category_id', '{{%cdbs_categories}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_cdbs_files_to_cdbs_description', '{{%cdbs_files}}', 'description_id', '{{%cdbs_description}}', 'id', 'CASCADE', 'CASCADE');
        $this->createIndex('idx_unique_alias', '{{%cdbs_description}}', ['alias', 'category_id'], true);

    }

    public function down()
    {
        $this->dropForeignKey('fk_cdbs_description_to_user', '{{%cdbs_description}}');
        $this->dropForeignKey('fk_cdbs_description_to_cdbs_categories', '{{%cdbs_description}}');
        $this->dropForeignKey('fk_cdbs_files_to_cdbs_description', '{{%cdbs_files}}');
        $this->dropTable('{{%cdbs_description}}');
        $this->dropTable('{{%cdbs_categories}}');
        $this->dropTable('{{%cdbs_files}}');
        return true;
    }
}
