<?php

use yii\db\Migration;

class m160708_113011_change_column_status_feature_request extends Migration
{
    public function up()
    {
        $items = $this->getDb()->createCommand("SELECT id, status FROM features_request")->queryAll();
        $rows = \yii\helpers\ArrayHelper::map($items, 'id', function($item){
            if($item['status'] == \app\modules\features\models\FeatureRequest::STATUS_DEV){
                $item['status'] = 'development';
            }
            if($item['status'] == \app\modules\features\models\FeatureRequest::STATUS_RELEASE){
                $item['status'] = 'released';
            }
            if($item['status'] == \app\modules\features\models\FeatureRequest::STATUS_VOTING){
                $item['status'] = 'voting';
            }
            if($item['status'] == \app\modules\features\models\FeatureRequest::STATUS_REFUSED){
                $item['status'] = 'refused';
            }
            return $item;
        });

        $this->dropColumn('features_request', 'status');

        $this->addColumn('features_request', 'status', 'ENUM("voting", "released", "development", "refused")');

        foreach($rows as $row){
            $this->update('features_request', ['status' => $row['status']], ['id' => $row['id']]);
        }

    }

    public function down()
    {
        echo "m160708_113011_change_column_status_feature_request cannot be reverted.\n";

        return true;
    }
}
