<?php

use yii\db\Migration;
use app\components\Entity;

class m160128_165737_changed_entities_namespased_to_constants_for_all_tables extends Migration
{
    public function up()
    {
        $this->updateCeoTable();
        $this->updateCommentsTable();
        $this->updateCountsTable();
        $this->updateLikesTable();
        $this->updateTranslateTable();
        $this->updateSlideTable();
        return true;
    }

    public function down()
    {
        $this->rollbackCeoTable();
        $this->rollbackCommentsTable();
        $this->rollbackCountsTable();
        $this->rollbackLikesTable();
        $this->rollbackTranslateTable();
        $this->rollbackSlideTable();
        return true;
    }

    /**
     * Changed entity namespaces to constants
     * for table ceo
     */
    protected function updateCeoTable()
    {
        $this->update('{{%ceo}}', ['entity' => Entity::FEATURES],
            ['entity' => 'app\modules\brokers\models\Improvement']);

        $this->update('{{%ceo}}', ['entity' => Entity::FORUM_TOPIC],
            ['entity' => 'app\modules\forum\models\FrmTopic']);

        $this->update('{{%ceo}}', ['entity' => Entity::FORUM_CATEGORY],
            ['entity' => 'app\modules\forum\models\FrmCategory']);


        $this->update('{{%ceo}}', ['entity' => Entity::ARTICLE],
            ['entity' => 'app\modules\articles\models\Articles']);

        $this->update('{{%ceo}}', ['entity' => Entity::BROKER],
            ['entity' => 'app\modules\brokers\models\Broker']);

        $this->update('{{%ceo}}', ['entity' => Entity::TRANSLATION],
            ['entity' => 'app\models\lang\Translations']);

        $this->update('{{%ceo}}', ['entity' => Entity::PAGE],
            ['entity' => 'app\models\main\Pages']);

        $this->update('{{%ceo}}', ['entity' => Entity::NEWS],
            ['entity' => 'app\models\news\News']);
    }

    /**
     * Changed entity namespaces to constants
     * for table com_comments
     */
    protected function updateCommentsTable()
    {
        $this->update('{{%com_comments}}', ['entity' => Entity::ARTICLE],
            ['entity' => 'app\modules\articles\models\Articles']);

        $this->update('{{%com_comments}}', ['entity' => Entity::FEATURES],
            ['entity' => 'app\modules\brokers\models\Improvement']);
    }

    /**
     * Changed entity namespaces to constants
     * for table com_counts
     */
    protected function updateCountsTable()
    {
        $this->update('{{%com_counts}}', ['entity' => Entity::ARTICLE],
            ['entity' => 'app\modules\articles\models\Articles']);

        $this->update('{{%com_counts}}', ['entity' => Entity::BROKER],
            ['entity' => 'app\modules\brokers\models\Broker']);

        $this->update('{{%com_counts}}', ['entity' => Entity::FEATURES],
            ['entity' => 'app\modules\brokers\models\Improvement']);

        $this->update('{{%com_counts}}', ['entity' => Entity::FORUM_COMMENT],
            ['entity' => 'app\modules\forum\models\FrmComment']);

        $this->update('{{%com_counts}}', ['entity' => Entity::FORUM_TOPIC],
            ['entity' => 'app\modules\forum\models\FrmTopic']);

        $this->update('{{%com_counts}}', ['entity' => Entity::COMMUNITY_COMMENT],
            ['entity' => 'app\models\community\Comments']);


    }

    /**
     * Changed entity namespaces to constants
     * for table com_likes
     */
    protected function updateLikesTable()
    {
        $this->update('{{%com_likes}}', ['entity' => Entity::ARTICLE],
            ['entity' => 'app\modules\articles\models\Articles']);

        $this->update('{{%com_likes}}', ['entity' => Entity::BROKER],
            ['entity' => 'app\modules\brokers\models\Broker']);

        $this->update('{{%com_likes}}', ['entity' => Entity::FEATURES],
            ['entity' => 'app\modules\brokers\models\Improvement']);

        $this->update('{{%com_likes}}', ['entity' => Entity::FORUM_COMMENT],
            ['entity' => 'app\modules\forum\models\FrmComment']);

        $this->update('{{%com_likes}}', ['entity' => Entity::FORUM_TOPIC],
            ['entity' => 'app\modules\forum\models\FrmTopic']);

        $this->update('{{%com_likes}}', ['entity' => Entity::COMMUNITY_COMMENT],
            ['entity' => 'app\models\community\Comments']);
    }

    /**
     * Changed entity namespaces to constants
     * for table lang_translate
     */
    protected function updateTranslateTable()
    {
        $this->update('{{%lang_translate}}', ['entity' => Entity::CEO],
            ['entity' => 'app\components\ceo\Ceo']);

        $this->update('{{%lang_translate}}', ['entity' => Entity::ARTICLE],
            ['entity' => 'app\modules\articles\models\Articles']);

        $this->update('{{%lang_translate}}', ['entity' => Entity::PAGE],
            ['entity' => 'app\models\main\Pages']);

        $this->update('{{%lang_translate}}', ['entity' => Entity::NEWS],
            ['entity' => 'app\models\news\News']);

        $this->update('{{%lang_translate}}', ['entity' => Entity::FEATURES],
            ['entity' => 'app\modules\brokers\models\Improvement']);
    }

    /**
     * Changed entity namespaces to constants
     * for table slide
     */
    protected function updateSlideTable()
    {
        $this->update('{{%slide}}', ['entity' => Entity::FEATURES],
            ['entity' => 'app\modules\brokers\models\Improvement']);
    }

    /**
     * Change constants to namespaces for table ceo
     */
    protected function rollbackCeoTable()
    {
        $this->update('{{%ceo}}', ['entity' => 'app\modules\brokers\models\Improvement'],
            ['entity' => Entity::FEATURES]);

        $this->update('{{%ceo}}', ['entity' => 'app\modules\forum\models\FrmTopic'],
            ['entity' => Entity::FORUM_TOPIC]);

        $this->update('{{%ceo}}', ['entity' => 'app\modules\forum\models\FrmCategory'],
            ['entity' => Entity::FORUM_CATEGORY]);


        $this->update('{{%ceo}}', ['entity' => 'app\modules\articles\models\Articles'],
            ['entity' => Entity::ARTICLE]);

        $this->update('{{%ceo}}', ['entity' => 'app\modules\brokers\models\Broker'],
            ['entity' => Entity::BROKER]);

        $this->update('{{%ceo}}', ['entity' => 'app\models\lang\Translations'],
            ['entity' => Entity::TRANSLATION]);

        $this->update('{{%ceo}}', ['entity' => 'app\models\main\Pages'],
            ['entity' => Entity::PAGE]);

        $this->update('{{%ceo}}', ['entity' => 'app\models\news\News'],
            ['entity' => Entity::NEWS]);
    }

    /**
     * Change constants to namespaces for table com_comments
     */
    protected function rollbackCommentsTable()
    {
        $this->update('{{%com_comments}}', ['entity' => 'app\modules\articles\models\Articles'],
            ['entity' => Entity::ARTICLE]);

        $this->update('{{%com_comments}}', ['entity' => 'app\modules\brokers\models\Improvement'],
            ['entity' => Entity::FEATURES]);
    }

    /**
     * Change constants to namespaces for table com_counts
     */
    protected function rollbackCountsTable()
    {
        $this->update('{{%com_counts}}', ['entity' => 'app\modules\articles\models\Articles'],
            ['entity' => Entity::ARTICLE]);

        $this->update('{{%com_counts}}', ['entity' => 'app\modules\brokers\models\Broker'],
            ['entity' => Entity::BROKER]);

        $this->update('{{%com_counts}}', ['entity' => 'app\modules\brokers\models\Improvement'],
            ['entity' => Entity::FEATURES]);

        $this->update('{{%com_counts}}', ['entity' => 'app\modules\forum\models\FrmComment'],
            ['entity' => Entity::FORUM_COMMENT]);

        $this->update('{{%com_counts}}', ['entity' => 'app\modules\forum\models\FrmTopic'],
            ['entity' => Entity::FORUM_TOPIC]);

        $this->update('{{%com_counts}}', ['entity' => 'app\models\community\Comments'],
            ['entity' => Entity::COMMUNITY_COMMENT]);
    }

    /**
     * Change constants to namespaces for table com_likes
     */
    protected function rollbackLikesTable()
    {
        $this->update('{{%com_likes}}', ['entity' => 'app\modules\articles\models\Articles'],
            ['entity' => Entity::ARTICLE]);

        $this->update('{{%com_likes}}', ['entity' => 'app\modules\brokers\models\Broker'],
            ['entity' => Entity::BROKER]);

        $this->update('{{%com_likes}}', ['entity' => 'app\modules\brokers\models\Improvement'],
            ['entity' => Entity::FEATURES]);

        $this->update('{{%com_likes}}', ['entity' => 'app\modules\forum\models\FrmComment'],
            ['entity' => Entity::FORUM_COMMENT]);

        $this->update('{{%com_likes}}', ['entity' => 'app\modules\forum\models\FrmTopic'],
            ['entity' => Entity::FORUM_TOPIC]);

        $this->update('{{%com_likes}}', ['entity' => 'app\models\community\Comments'],
            ['entity' => Entity::COMMUNITY_COMMENT]);
    }

    /**
     * Change constants to namespaces for table lang_translate
     */
    protected function rollbackTranslateTable()
    {
        $this->update('{{%lang_translate}}', ['entity' => 'app\components\ceo\Ceo'],
            ['entity' => Entity::CEO]);

        $this->update('{{%lang_translate}}', ['entity' => 'app\modules\articles\models\Articles'],
            ['entity' => Entity::ARTICLE]);

        $this->update('{{%lang_translate}}', ['entity' => 'app\models\main\Pages'],
            ['entity' => Entity::PAGE]);

        $this->update('{{%lang_translate}}', ['entity' => 'app\models\news\News'],
            ['entity' => Entity::NEWS]);

        $this->update('{{%lang_translate}}', ['entity' => 'app\modules\brokers\models\Improvement'],
            ['entity' => Entity::FEATURES]);
    }
    /**
     * Change constants to namespaces for table slide
     */
    protected function rollbackSlideTable()
    {
        $this->update('{{%slide}}', ['entity' => 'app\modules\brokers\models\Improvement'],
            ['entity' => Entity::FEATURES]);
    }
}
