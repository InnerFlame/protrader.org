<?php

use yii\db\Schema;
use yii\db\Migration;

class m160208_160628_create_table_notifications extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%notifications}}', [
            'id' => $this->primaryKey(),
            'type_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'read' => $this->boolean()->defaultValue(false),
            'create_at' => $this->timestamp()
        ]);

        $this->createTable('{{%notifications_type}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50)->notNull(),
            'enabled' => $this->boolean()->defaultValue(true)
        ]);

        $this->createTable('{{%user_notifications}}', [
            'id' => $this->primaryKey(),
            'type_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'enabled' => $this->boolean()->defaultValue(false)
        ]);

        $this->db->schema->refresh();

        $this->batchInsert('{{%notifications_type}}', ['name', 'enabled'], [
            [
                'name' => 'forum.comment.add',
                'enabled' => 1
            ],
            [
                'name' => 'forum.comment.add.role.author',
                'enabled' => 1
            ],
            [
                'name' => 'forum.comment.reply',
                'enabled' => 1
            ],
            [
                'name' => 'forum.topic.add',
                'enabled' => 0
            ],
            [
                'name' => 'forum.topic.edit',
                'enabled' => 0
            ],
            [
                'name' => 'forum.topic.deleted',
                'enabled' => 0
            ],
            [
                'name' => 'forum.topic.move',
                'enabled' => 1
            ],
            [
                'name' => 'custom.comment.add',
                'enabled' => 1
            ],
            [
                'name' => 'custom.comment.edit',
                'enabled' => 0
            ],
            [
                'name' => 'custom.comment.reply',
                'enabled' => 1
            ],
            [
                'name' => 'articles.topic.add',
                'enabled' => 1
            ],
            [
                'name' => 'articles.topic.edit',
                'enabled' => 0
            ],
            [
                'name' => 'articles.topic.deleted',
                'enabled' => 0
            ]
        ]);

    }

    public function safeDown()
    {
        $this->dropTable('{{%notifications}}');
        $this->dropTable('{{%notifications_type}}');
        $this->dropTable('{{%user_notifications}}');
    }
}
