<?php

use yii\db\Migration;

class m160525_114800_add_fake_voutes_column extends Migration
{
    public function up()
    {
        $this->addColumn('{{%com_votes}}', 'count_fake', 'INT(11) DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn('{{%com_votes}}', 'count_fake');
        return true;
    }
}
