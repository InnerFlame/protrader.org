<?php

use yii\db\Migration;

class m160907_083247_crosstrade_report_asr extends Migration
{
    public $table_name = '{{%crosstrade_report_asr}}';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table_name, [
            'id'                => $this->primaryKey(),
            'account'           => $this->string(30),
            'login'             => $this->string(30),
            'balance'           => $this->decimal(10, 2),
            'projected_balance' => $this->decimal(10, 2),
            'created_at'        => $this->integer(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable($this->table_name);
    }
}
