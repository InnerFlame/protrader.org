<?php

use yii\db\Migration;

/**
 * Проверяет наличие поломанной целостности, между таблицами feature_request
 * и user. Если таковые имеются, то найти их и удалить вручную. После чего,
 * удалить внешний ключ и пересоздать его с каскадным удалением
 * Class m160831_123422_chek_and_fixed_relations_feature_request
 */
class m160831_123422_chek_and_fixed_relations_feature_request extends Migration
{
    public function up()
    {
        $ids = $this->getDb()->createCommand("SELECT c.id FROM features_request c LEFT JOIN `user` p ON (c.`user_id` =  p.id) WHERE p.id IS NULL;")->queryAll();
        if(count($ids) > 0){
            $this->fixedData($ids);
        }
        $this->dropForeignKey('fk_features_request_to_users', '{{%features_request}}');
        $this->addForeignKey('fk_features_request_to_users', '{{%features_request}}', 'user_id', '{{%user}}', 'id', 'CASCADE');
        return true;

    }

    public function down()
    {
        echo "m160831_123422_chek_and_fixed_relations_feature_request cannot be reverted.\n";
        return true;
    }

    /**
     * Восстанавливает целостность данных. Записи, которые
     * ссылаются на несосуществующих пользователей, будут
     * удалены
     * @param $ids
     */
    protected function fixedData($ids)
    {
        $ids = \app\components\helpers\ArrayHelper::getColumn($ids, 'id');
        $sql = sprintf("DELETE FROM `user` WHERE `id` IN (%s)", implode(',', $ids));
        $this->getDb()->createCommand($sql)->query();
    }
}
