<?php

use yii\db\Schema;
use yii\db\Migration;

class m160115_092952_add_status_articles extends Migration
{
    public function up()
    {
        $this->addColumn('{{%artcl_articles}}', 'status', "INT(4) NULL DEFAULT 0 AFTER `content`");

        $this->db->schema->refresh();
    }

    public function down()
    {
        $this->dropColumn('{{%artcl_articles}}', 'status');
    }
}
