<?php

use yii\db\Migration;

class m160808_111124_crosstrade_subscribe extends Migration
{
    const TBL_CROSS = '{{%crosstrade_subscriber}}';

    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable(self::TBL_CROSS, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'status' => $this->smallInteger(),
        ],$tableOptions);

        $this->addForeignKey('fk_crosstrade_subscriber_user_id', self::TBL_CROSS, 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_crosstrade_subscriber_user_id', self::TBL_CROSS);
        $this->dropTable(self::TBL_CROSS);
    }
}
