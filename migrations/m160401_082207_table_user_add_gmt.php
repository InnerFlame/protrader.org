<?php

use yii\db\Migration;

class m160401_082207_table_user_add_gmt extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'gmt', "varchar(20) DEFAULT NULL AFTER `birth_at`");
        $this->addColumn('{{%user}}', 'country', "varchar(50) DEFAULT NULL AFTER `gmt`");
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'gmt');
        $this->dropColumn('{{%user}}', 'country');
        return true;
    }
}
