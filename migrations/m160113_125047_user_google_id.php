<?php

use yii\db\Schema;
use yii\db\Migration;

class m160113_125047_user_google_id extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%user}}', 'vkontakte_id', "INT(11) DEFAULT NULL AFTER `pictures`");
        $this->alterColumn('{{%user}}', 'google_id', "VARCHAR(32) DEFAULT NULL AFTER `vkontakte_id`");
        $this->alterColumn('{{%user}}', 'linkedin_id', "VARCHAR(32) DEFAULT NULL AFTER `google_id`");
        $this->alterColumn('{{%user}}', 'facebook_id', "INT(11) DEFAULT NULL AFTER `linkedin_id`");
        $this->alterColumn('{{%user}}', 'twitter_id', "INT(11) DEFAULT NULL AFTER `facebook_id`");
    }


    public function down()
    {
        return true;
    }
}
