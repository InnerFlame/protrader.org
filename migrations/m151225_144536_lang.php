<?php

use yii\db\Schema;
use yii\db\Migration;

class m151225_144536_lang extends Migration
{
    public $table_lang = '{{%lang_lang}}';
    public $table_translate = '{{%lang_translate}}';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table_lang, [
            'id'         => Schema::TYPE_INTEGER . '(11) NOT NULL AUTO_INCREMENT',
            'name'       => Schema::TYPE_STRING . '(255) NOT NULL',
            'alias'      => Schema::TYPE_STRING . '(255) NOT NULL',
            'icon'       => Schema::TYPE_STRING . '(255) DEFAULT NULL',
            'created_at' => Schema::TYPE_INTEGER . '(11) DEFAULT NULL',
            'updated_at' => Schema::TYPE_INTEGER . '(11) DEFAULT NULL',
            'PRIMARY KEY (`id`)'
        ], $tableOptions);

        $this->createTable($this->table_translate, [
            'id'         => Schema::TYPE_INTEGER . '(11) NOT NULL AUTO_INCREMENT',
            'lang_id'    => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'entity'     => Schema::TYPE_STRING . '(255) NOT NULL',
            'entity_id'  => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'attribute'  => Schema::TYPE_STRING . '(255) NOT NULL',
            'content'    => Schema::TYPE_TEXT . ' NOT NULL',
            'created_at' => Schema::TYPE_INTEGER . '(11) DEFAULT NULL',
            'updated_at' => Schema::TYPE_INTEGER . '(11) DEFAULT NULL',
            'PRIMARY KEY (`id`)'
        ], $tableOptions);

        $this->createIndex('lang_id', $this->table_translate, 'lang_id', false);

        $this->addForeignKey('fk_lang_translate_lang_id', $this->table_translate, 'lang_id', $this->table_lang, 'id', 'CASCADE', 'CASCADE');

    }

    public function down()
    {
        $this->dropForeignKey('fk_lang_translate_lang_id', $this->table_translate);

        $this->dropIndex('lang_id', $this->table_translate);

        $this->dropTable($this->table_lang);
        $this->dropTable($this->table_translate);
    }
}
