<?php

use yii\db\Migration;

class m160405_104757_add_column_erros_handler_urls_for_redirect extends Migration
{
    public function up()
    {
        $this->addColumn('{{%error_pages}}', 'type', 'TINYINT(1) NOT NULL DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn('{{%error_pages}}', 'type');
        return true;
    }
}
