<?php

use yii\db\Schema;
use yii\db\Migration;

class m160205_085029_add_remove_column_to_tables_forum extends Migration
{


    public function up()
    {
        $this->addColumn('{{%frm_category}}','author_deleted', 'INT');
        $this->addColumn('{{%frm_comments}}','deleted_at', 'INT');
        $this->addColumn('{{%frm_comments}}','author_deleted', 'INT');
        $this->addColumn('{{%frm_topics}}','deleted_at', 'INT');
        $this->addColumn('{{%frm_topics}}','author_deleted', 'INT');
    }

    public function down()
    {
        $this->dropColumn('{{%frm_category}}','author_deleted');
        $this->dropColumn('{{%frm_comments}}','deleted_at');
        $this->dropColumn('{{%frm_comments}}','author_deleted');
        $this->dropColumn('{{%frm_topics}}','deleted_at');
        $this->dropColumn('{{%frm_topics}}','author_deleted');
        return true;
    }
}
