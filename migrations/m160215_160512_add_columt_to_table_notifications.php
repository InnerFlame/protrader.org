<?php

use yii\db\Migration;

class m160215_160512_add_columt_to_table_notifications extends Migration
{
    public function up()
    {
        $this->addColumn('{{%notifications}}', 'data', 'text');
    }

    public function down()
    {
        $this->dropColumn('{{%notifications}}');

    }
}
