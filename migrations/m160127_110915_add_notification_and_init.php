<?php

use yii\db\Schema;
use yii\db\Migration;
use app\components\customEvents\_subscriber\Subscribers;

class m160127_110915_add_notification_and_init extends Migration
{
    public function before()
    {
        $consts = Subscribers::getConstants();

        foreach($consts as $const){
            $this->insert('{{%user_custom_field}}', [
                'name'  => $const,
                'alias' => $const,
            ]);
        }
    }

    public function up()
    {
        $this->dropPrimaryKey('user_id, field_id', 'user_custom_value');

        $this->addColumn('{{%user_settings}}', 'data', "TEXT DEFAULT NULL AFTER `lang_id`");

        $this->addColumn('{{%user_custom_value}}', 'id', "INT(8) NOT NULL AUTO_INCREMENT, ADD PRIMARY KEY (`id`)");

        $this->before();

        $this->db->schema->refresh();
    }

    public function down()
    {
        $this->dropColumn('{{%user_settings}}', 'data');

//        $this->dropPrimaryKey('id', 'user_custom_value');

        $this->dropColumn('{{%user_custom_value}}', 'id');
    }
}
