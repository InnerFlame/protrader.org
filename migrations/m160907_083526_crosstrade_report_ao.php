<?php

use yii\db\Migration;

class m160907_083526_crosstrade_report_ao extends Migration
{
    public $table_name = '{{%crosstrade_report_ao}}';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table_name, [
            'id'             => $this->primaryKey(),
            'account'        => $this->string(30),
            'login'          => $this->string(30),
            'operation_id'   => $this->integer(),
            'order_id'       => $this->integer(),
            'position_id'    => $this->integer(),
            'trade_id'       => $this->integer(),
            'operation_type' => $this->string(30),
            'value'          => $this->float(),
            'currency'       => $this->string(10),
            'comments'       => $this->text(),
            'created_at'     => $this->dateTime(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable($this->table_name);
    }
}
