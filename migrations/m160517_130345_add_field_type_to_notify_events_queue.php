<?php

use yii\db\Migration;

class m160517_130345_add_field_type_to_notify_events_queue extends Migration
{
    const TABLE_NAME = '{{%notify_events_queue}}';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'type', 'ENUM("subsribed", "specific")  DEFAULT "subsribed"');
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'type');
    }
}
