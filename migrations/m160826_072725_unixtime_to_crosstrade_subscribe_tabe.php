<?php

use yii\db\Migration;

class m160826_072725_unixtime_to_crosstrade_subscribe_tabe extends Migration
{
    public function up()
    {
        $this->addColumn('{{%crosstrade_subscriber}}', 'created_at', 'TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP');
    }

    public function down()
    {
        $this->dropColumn('{{%crosstrade_subscriber}}', 'created_at');
        return true;
    }
}
