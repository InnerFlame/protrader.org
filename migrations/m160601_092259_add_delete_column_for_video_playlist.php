<?php

use yii\db\Migration;

class m160601_092259_add_delete_column_for_video_playlist extends Migration
{
    public function up()
    {
        $this->addColumn(\app\models\video\Playlist::tableName(), 'deleted', 'TINYINT(1) DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn(\app\models\video\Playlist::tableName(), 'deleted');
        return true;
    }
}
