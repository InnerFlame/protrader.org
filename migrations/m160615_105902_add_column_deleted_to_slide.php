<?php

use yii\db\Migration;

class m160615_105902_add_column_deleted_to_slide extends Migration
{
    public function up()
    {
        $this->addColumn(\app\models\slide\Slide::tableName(), 'deleted', 'TINYINT(1) DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn(\app\models\slide\Slide::tableName(), 'deleted');
    }
}
