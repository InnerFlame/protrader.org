<?php

use yii\db\Schema;
use yii\db\Migration;

class m151127_122930_likes extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
            $tableOptionsMyISAM = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=MyISAM';
        }


        $this->createTable('{{%com_likes}}', [
            'id' => 'INT(8) NOT NULL AUTO_INCREMENT',
            'entity' => 'VARCHAR(255) NULL DEFAULT NULL',
            'entity_id' => 'INT(8) NULL DEFAULT NULL',
            'user_id' => 'INT(8) NULL DEFAULT NULL',
            'user_ip' => 'VARCHAR(255) NULL DEFAULT NULL',
            'created_at' => 'INT(11) NULL DEFAULT NULL',
            'updated_at' => 'INT(11) NULL DEFAULT NULL',
            'PRIMARY KEY (`id`)',

        ], $tableOptions);

        $this->createTable('{{%com_counts}}', [
            'id' => 'INT(8) NOT NULL AUTO_INCREMENT',
            'entity' => 'VARCHAR(255) NULL DEFAULT NULL',
            'entity_id' => 'INT(8) NULL DEFAULT NULL',
            'count_view' => 'INT(8) NULL DEFAULT 0',
            'count_like' => 'INT(8) NULL DEFAULT 0',
            'count_commment' => 'INT(8) NULL DEFAULT 0',
            'created_at' => 'INT(11) NULL DEFAULT NULL',
            'updated_at' => 'INT(11) NULL DEFAULT NULL',
            'PRIMARY KEY (`id`)',

        ], $tableOptions);

        $this->createTable('{{%com_comments}}', [
            'id' => 'INT(8) NOT NULL AUTO_INCREMENT',
            'entity' => 'VARCHAR(255) NULL DEFAULT NULL',
            'entity_id' => 'INT(8) NULL DEFAULT NULL',
            'user_id' => 'INT(8) NOT NULL',
            'content' => 'TEXT NULL DEFAULT NULL',
            'root'        => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'lft'         => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'rgt'         => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'level'       => Schema::TYPE_SMALLINT . '(4) NOT NULL',
            'published' => 'INT(11) NULL DEFAULT NULL',
            'created_at' => 'INT(11) NULL DEFAULT NULL',
            'updated_at' => 'INT(11) NULL DEFAULT NULL',
            'PRIMARY KEY (`id`)',

        ], $tableOptions);




        $this->db->schema->refresh();

    }

    public function down()
    {
        $this->dropTable('{{%com_likes}}');
        $this->dropTable('{{%com_counts}}');
        $this->dropTable('{{%com_comments}}');

    }
}
