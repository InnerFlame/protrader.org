<?php

use yii\db\Migration;

class m160816_124823_add_column_is_registrated extends Migration
{
    public function up()
    {

        $this->addColumn('{{%crosstrade_subscriber}}', 'is_registered', 'TINYINT(1) DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn('{{%crosstrade_subscriber}}', 'is_registered');
        return true;
    }
}
