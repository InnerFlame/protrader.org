<?php

use yii\db\Schema;
use yii\db\Migration;

class m160211_131914_update_field_entity extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        #broker
        $this->dropColumn('{{%broker}}', 'deleted_at');
        $this->addColumn('{{%broker}}', 'deleted', "smallint(4)  DEFAULT 0 AFTER `is_demo`");

        #frm_improvements
        $this->addColumn('{{%frm_improvements}}', 'deleted', "smallint(4)  DEFAULT 0 AFTER `status`");

        #artcl_articles
        $this->addColumn('{{%artcl_articles}}', 'deleted', "smallint(4)  DEFAULT 0 AFTER `published`");

        #frm_topics
        $this->dropColumn('{{%frm_topics}}', 'deleted_at');
        $this->dropColumn('{{%frm_topics}}', 'author_deleted');
        $this->addColumn('{{%frm_topics}}', 'deleted', "smallint(4)  DEFAULT 0 AFTER `published`");

        #frm_category
        $this->dropColumn('{{%frm_category}}', 'deleted_at');
        $this->dropColumn('{{%frm_category}}', 'author_deleted');
        $this->addColumn('{{%frm_category}}', 'deleted', "smallint(4)  DEFAULT 0 AFTER `published`");

        #artcl_category
        $this->addColumn('{{%artcl_category}}', 'deleted', "smallint(4)  DEFAULT 0 AFTER `published`");

        #com_comments
        $this->addColumn('{{%com_comments}}', 'deleted', "smallint(4)  DEFAULT 0 AFTER `published`");

        #main_pages
        $this->addColumn('{{%main_pages}}', 'deleted', "smallint(4)  DEFAULT 0 AFTER `published_at`");

        #frm_comments
        $this->dropColumn('{{%frm_comments}}', 'deleted_at');
        $this->dropColumn('{{%frm_comments}}', 'author_deleted');
        $this->addColumn('{{%frm_comments}}', 'deleted', "smallint(4)  DEFAULT 0 AFTER `published`");

        $this->db->schema->refresh();
    }

    public function safeDown()
    {
        #broker
        $this->dropColumn('{{%broker}}', 'deleted');
        $this->addColumn('{{%broker}}', 'deleted_at', "int(11) DEFAULT NULL AFTER `created_at`");

        #frm_improvements
        $this->dropColumn('{{%frm_improvements}}', 'deleted');

        #artcl_articles
        $this->dropColumn('{{%artcl_articles}}', 'deleted');

        #frm_topics
        $this->addColumn('{{%frm_topics}}', 'deleted_at', "int(11) DEFAULT NULL AFTER `updated_at`");
        $this->addColumn('{{%frm_topics}}', 'author_deleted', "int(11) DEFAULT NULL AFTER `deleted_at`");
        $this->dropColumn('{{%frm_topics}}', 'deleted');

        #frm_category
        $this->addColumn('{{%frm_category}}', 'deleted_at', "int(11) DEFAULT NULL AFTER `updated_at`");
        $this->addColumn('{{%frm_category}}', 'author_deleted', "int(11) DEFAULT NULL AFTER `deleted_at`");
        $this->dropColumn('{{%frm_category}}', 'deleted');

        #artcl_category
        $this->dropColumn('{{%artcl_category}}', 'deleted');

        #com_comments
        $this->dropColumn('{{%com_comments}}', 'deleted');

        #main_pages
        $this->dropColumn('{{%main_pages}}', 'deleted');

        #frm_comments
        $this->addColumn('{{%frm_comments}}', 'deleted_at', "int(11) DEFAULT NULL AFTER `updated_at`");
        $this->addColumn('{{%frm_comments}}', 'author_deleted', "int(11) DEFAULT NULL AFTER `deleted_at`");
        $this->dropColumn('{{%frm_comments}}', 'deleted');
    }

}
