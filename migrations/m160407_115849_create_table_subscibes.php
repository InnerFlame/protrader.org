<?php

use yii\db\Migration;

class m160407_115849_create_table_subscibes extends Migration
{
    const TBL_NOTIFY_TEMPLATES = '{{%notify_templates}}';

    const TBL_NOTIFY_SUBSCRIBE = '{{%notify_subscribes}}';

    const TBL_NOTIFY_EVENTS = '{{%notify_events}}';

    const TBL_NOTIFY_USER_SETTINGS = '{{%notify_user_settings}}';

    const TBL_NOTIFY_EVENTS_QUEUE = '{{%notify_events_queue}}';

    const TBL_NOTIFY_MESSAGES = '{{%notify_messages}}';

    public function up()
    {
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable(self::TBL_NOTIFY_SUBSCRIBE, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'event_id' => $this->integer()->notNull(),
            'entity' => $this->integer(5)->notNull(),
            'entity_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
        ],$tableOptions);

        /**
         * Events table for all application. Custom module,
         * insert rows in the table after installed in application.
         */
        $this->createTable(self::TBL_NOTIFY_EVENTS, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'enabled' => $this->smallInteger(1)->defaultValue(1), //enabled/disabled this event all app
            'optional' => $this->smallInteger(1)->defaultValue(1), //User can disabled/enabled this event in profile
            'label' => $this->string()->notNull(),
            'created_at' => $this->integer()->notNull(),
        ],$tableOptions);

        /**
         * Queue for events notifications. Special component,
         * get rows from this table and push notification for users.
         *
         */
        $this->createTable(self::TBL_NOTIFY_EVENTS_QUEUE, [
            'id' => $this->primaryKey(),
            'data' => $this->text()->notNull(),
            'created_at' => $this->integer()->notNull()
        ],$tableOptions);

        /**
         * Messages for notifications widget bell.
         */
        $this->createTable(self::TBL_NOTIFY_MESSAGES,[
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'sender_id' => $this->integer()->notNull(),
            'event_id' => $this->integer()->notNull(),
            'sent' => $this->smallInteger()->defaultValue(0),
            'read' => $this->smallInteger()->defaultValue(0),
            'data' => $this->text()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'departure_date' => $this->integer(),
        ],$tableOptions);

        /**
         * Notify settings for users. Enabled push messages
         * or enabled email or push messages in widget, and email
         */
        $this->createTable(self::TBL_NOTIFY_USER_SETTINGS,[
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'event_id' => $this->integer()->notNull(),
            'push_alert' => $this->smallInteger()->defaultValue(1),
            'push_email' => $this->smallInteger()->defaultValue(1),
        ],$tableOptions);

        /**
         * Templates messages notifications for users
         */
        $this->createTable(self::TBL_NOTIFY_TEMPLATES, [
            'pk' => $this->primaryKey(),
            'event_id' => $this->integer()->notNull(),
            'type' => 'ENUM("GROUP", "SHORT", "FULL", "EMAIL") DEFAULT "SHORT"',
            'tpl_title' => $this->string()->notNull(),
            'tpl_body' => $this->string()->notNull()
        ],$tableOptions);


        $this->dropTable('notifications');
        $this->dropTable('notifications_type');
        $this->dropTable('user_notifications');

        $this->addForeignKey('fk_subscribes_to_users', self::TBL_NOTIFY_SUBSCRIBE, 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_subscribes_to_events', self::TBL_NOTIFY_SUBSCRIBE, 'event_id', self::TBL_NOTIFY_EVENTS, 'id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('fk_user_settings_to_events', self::TBL_NOTIFY_USER_SETTINGS, 'event_id', self::TBL_NOTIFY_EVENTS, 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_user_settings_to_user', self::TBL_NOTIFY_USER_SETTINGS, 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('fk_messages_to_user', self::TBL_NOTIFY_MESSAGES, 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_messages_to_sender', self::TBL_NOTIFY_MESSAGES, 'sender_id', 'user', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_messages_to_event', self::TBL_NOTIFY_MESSAGES, 'event_id', self::TBL_NOTIFY_EVENTS, 'id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('fk_templates_to_events', self::TBL_NOTIFY_TEMPLATES, 'event_id', self::TBL_NOTIFY_EVENTS, 'id', 'CASCADE', 'CASCADE');

        $this->createIndex('idx_entity',self::TBL_NOTIFY_SUBSCRIBE, ['user_id','event_id','entity', 'entity_id'], true);
        $this->createIndex('idx_name',self::TBL_NOTIFY_EVENTS, 'name', true);
        $this->createIndex('idx_template', self::TBL_NOTIFY_TEMPLATES, ['event_id', 'type'], true);
    }

    public function down()
    {
        $this->dropForeignKey('fk_subscribes_to_users', self::TBL_NOTIFY_SUBSCRIBE);
        $this->dropForeignKey('fk_subscribes_to_events', self::TBL_NOTIFY_SUBSCRIBE);
        $this->dropForeignKey('fk_user_settings_to_events', self::TBL_NOTIFY_USER_SETTINGS);
        $this->dropForeignKey('fk_user_settings_to_user', self::TBL_NOTIFY_USER_SETTINGS);
        $this->dropForeignKey('fk_messages_to_user', self::TBL_NOTIFY_MESSAGES);
        $this->dropForeignKey('fk_messages_to_sender', self::TBL_NOTIFY_MESSAGES);
        $this->dropForeignKey('fk_templates_to_events', self::TBL_NOTIFY_TEMPLATES);
        $this->dropTable(self::TBL_NOTIFY_SUBSCRIBE);
        $this->dropTable(self::TBL_NOTIFY_EVENTS);
        $this->dropTable(self::TBL_NOTIFY_MESSAGES);
        $this->dropTable(self::TBL_NOTIFY_EVENTS_QUEUE);
        $this->dropTable(self::TBL_NOTIFY_USER_SETTINGS);
        $this->dropTable(self::TBL_NOTIFY_TEMPLATES);

        $this->createTable('{{%notifications}}', [
            'id' => $this->primaryKey(),
            'type_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'read' => $this->boolean()->defaultValue(false),
            'create_at' => $this->timestamp()
        ]);

        $this->createTable('{{%notifications_type}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50)->notNull(),
            'enabled' => $this->boolean()->defaultValue(true)
        ]);

        $this->createTable('{{%user_notifications}}', [
            'id' => $this->primaryKey(),
            'type_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'enabled' => $this->boolean()->defaultValue(false)
        ]);

        $this->batchInsert('{{%notifications_type}}', ['name', 'enabled'], [
            [
                'name' => 'forum.comment.add',
                'enabled' => 1
            ],
            [
                'name' => 'forum.comment.add.role.author',
                'enabled' => 1
            ],
            [
                'name' => 'forum.comment.reply',
                'enabled' => 1
            ],
            [
                'name' => 'forum.topic.add',
                'enabled' => 0
            ],
            [
                'name' => 'forum.topic.edit',
                'enabled' => 0
            ],
            [
                'name' => 'forum.topic.deleted',
                'enabled' => 0
            ],
            [
                'name' => 'forum.topic.move',
                'enabled' => 1
            ],
            [
                'name' => 'custom.comment.add',
                'enabled' => 1
            ],
            [
                'name' => 'custom.comment.edit',
                'enabled' => 0
            ],
            [
                'name' => 'custom.comment.reply',
                'enabled' => 1
            ],
            [
                'name' => 'articles.topic.add',
                'enabled' => 1
            ],
            [
                'name' => 'articles.topic.edit',
                'enabled' => 0
            ],
            [
                'name' => 'articles.topic.deleted',
                'enabled' => 0
            ]
        ]);
    }
}
