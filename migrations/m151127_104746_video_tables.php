<?php

use yii\db\Schema;
use yii\db\Migration;

class m151127_104746_video_tables extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
            $tableOptionsMyISAM = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=MyISAM';
        }


        $this->createTable('{{%vd_category}}', [
            'id' => 'INT(8) NOT NULL AUTO_INCREMENT',
            'title' => 'VARCHAR(60) NULL DEFAULT NULL',
            'alias' => 'VARCHAR(256) NULL DEFAULT NULL',
            'data' => 'VARCHAR(256) NULL DEFAULT NULL',
            'weight' => 'INT(8) NULL DEFAULT 0',
            'published' => 'INT(11) NULL DEFAULT NULL',
            'created_at' => 'INT(11) NULL DEFAULT NULL',
            'updated_at' => 'INT(11) NULL DEFAULT NULL',

            'PRIMARY KEY (`id`)'

        ], $tableOptions);


        $this->createTable('{{%vd_playlist}}', [
            'id' => 'INT(8) NOT NULL AUTO_INCREMENT',
            'title' => 'VARCHAR(60) NULL DEFAULT NULL',
            'alias' => 'VARCHAR(256) NULL DEFAULT NULL',
            'data' => 'VARCHAR(256) NULL DEFAULT NULL',
            'weight' => 'INT(8) NULL DEFAULT 0',
            'published' => 'INT(11) NULL DEFAULT NULL',
            'created_at' => 'INT(11) NULL DEFAULT NULL',
            'updated_at' => 'INT(11) NULL DEFAULT NULL',
            'PRIMARY KEY (`id`)'
        ], $tableOptions);


        $this->createTable('{{%vd_video}}', [
            'id' => 'INT(8) NOT NULL AUTO_INCREMENT',
            'category_id' => 'INT(8) NOT NULL',
            'title' => 'VARCHAR(60) NULL DEFAULT NULL',
            'alias' => 'VARCHAR(256) NULL DEFAULT NULL',
            'link' => 'VARCHAR(256) NULL DEFAULT NULL',
            'weight' => 'INT(8) NULL DEFAULT 0',
            'published' => 'INT(11) NULL DEFAULT NULL',
            'created_at' => 'INT(11) NULL DEFAULT NULL',
            'updated_at' => 'INT(11) NULL DEFAULT NULL',
            'PRIMARY KEY (`id`),
             INDEX fk_video_vd_category (`id` ASC),
             CONSTRAINT `fk_user_vd_category`
                FOREIGN KEY (`category_id`)
                REFERENCES {{%vd_category}} (`id`)
                ON DELETE CASCADE
                ON UPDATE CASCADE'

        ], $tableOptions);



        $this->db->schema->refresh();

    }

    public function down()
    {
        $this->dropTable('{{%vd_video}}');
        $this->dropTable('{{%vd_category}}');
        $this->dropTable('{{%vd_playlist}}');

    }
}
