<?php

use yii\db\Migration;
use app\components\widgets\vote\models\Vote;
use app\components\Entity;

class m160404_122948_add_votes_by_userslist extends Migration
{
    public function up()
    {
        $this->upVote(6, 2, 2);
        $this->upVote(6, 3, 1);
        $this->upVote(6, 130, 1);
        $this->upVote(6, 541, 2);
        $this->upVote(6, 290, 3);


        $this->upVote(5, 2, 2);
        $this->upVote(5, 3, 2);
        $this->upVote(5, 130, 3);
        $this->upVote(5, 541, 2);
        $this->upVote(5, 368, 2);
        $this->upVote(6, 290, 2);

        $this->upVote(4, 2, 2);
        $this->upVote(4, 3, 2);
        $this->upVote(4, 130, 3);
        $this->upVote(4, 541, 4);
        $this->upVote(4, 371, 3);
    }

    public function upVote($entity_id, $user_id, $count)
    {
        $model = Vote::find()->where(['entity' => Entity::FEATURES_REQUEST])->andWhere(['entity_id' => $entity_id])->andWhere(['user_id' => $user_id])->one();
        if(!$model){
            $model = new Vote();
            $model->count = 0;
        }
        $model->entity = Entity::FEATURES_REQUEST;
        $model->entity_id = $entity_id;
        $model->user_id = $user_id;
        $model->updated_at = time();
        $model->count = $model->count + $count;
        $model->save();
    }
    public function down()
    {
        return true;
    }
}
