<?php

use yii\db\Migration;
use app\components\widgets\vote\models\Vote;
use app\components\Entity;

class m160405_085256_fixed_votes_users extends Migration
{
    public function up()
    {
        $this->upVote(5, 290, 2);
    }

    public function upVote($entity_id, $user_id, $count)
    {
        $model = Vote::find()->where(['entity' => Entity::FEATURES_REQUEST])->andWhere(['entity_id' => $entity_id])->andWhere(['user_id' => $user_id])->one();
        if(!$model){
            $model = new Vote();
            $model->count = 0;
        }
        $model->entity = Entity::FEATURES_REQUEST;
        $model->entity_id = $entity_id;
        $model->user_id = $user_id;
        $model->updated_at = time();
        $model->count = $model->count + $count;
        $model->save();
    }
    public function down()
    {
        return true;
    }
}
