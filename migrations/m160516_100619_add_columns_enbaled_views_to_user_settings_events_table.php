<?php

use yii\db\Migration;

class m160516_100619_add_columns_enbaled_views_to_user_settings_events_table extends Migration
{
    public function up()
    {
        $this->addColumn('{{%notify_user_settings}}', 'enabled', 'TINYINT(1) DEFAULT 1');
        $this->addColumn('{{%notify_user_settings}}', 'hidden', 'TINYINT(1) DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn('{{%notify_user_settings}}', 'enabled' );
        $this->dropColumn('{{%notify_user_settings}}', 'hidden' );
    }
}
