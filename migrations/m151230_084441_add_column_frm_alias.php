<?php

use yii\db\Schema;
use yii\db\Migration;

class m151230_084441_add_column_frm_alias extends Migration
{
    public function up()
    {
        $this->addColumn('{{%frm_topics}}', 'alias', "VARCHAR(255) NULL DEFAULT NULL AFTER `title`");

        $this->db->schema->refresh();
    }

    public function down()
    {
        $this->dropColumn('{{%frm_topics}}', 'alias');
    }
}
