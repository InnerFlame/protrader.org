<?php

use yii\db\Migration;

class m160606_111315_add_delete_columnt_in_video_playlist_category extends Migration
{
    public function up()
    {
        $this->addColumn(\app\models\video\VdCategory::tableName(), 'deleted', 'TINYINT(1) DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn(\app\models\video\VdCategory::tableName(), 'deleted');
        return true;
    }
}
