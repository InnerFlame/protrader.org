<?php

use yii\db\Schema;
use yii\db\Migration;

class m151223_105702_slide_add_entity extends Migration
{
    public function up()
    {

        $this->addColumn('{{%slide}}', 'entity', "VARCHAR(255) DEFAULT NULL AFTER `id`");
        $this->addColumn('{{%slide}}', 'entity_id', "INT(8) DEFAULT NULL AFTER `entity`");
        $this->addColumn('{{%slide}}', 'is_main', "INT(8) DEFAULT NULL AFTER `entity_id`");

    }

    public function down()
    {
        $this->dropColumn('{{%slide}}', 'entity');
        $this->dropColumn('{{%slide}}', 'entity_id');
        $this->dropColumn('{{%slide}}', 'is_main');

    }
}
