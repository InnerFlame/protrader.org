<?php

use yii\db\Migration;

class m160128_122148_replace_entity_column_for_all_columns extends Migration
{
    public function up()
    {
        $transaction = $this->getDb()->beginTransaction();
        try{
            //ceo
            $this->update('{{%ceo}}', ['entity' => 'app\modules\brokers\models\Improvement'],
                ['entity' => 'app\models\forum\Improvement']);

            $this->update('{{%ceo}}', ['entity' => 'app\modules\forum\models\FrmTopic'],
                ['entity' => 'app\models\forum\FrmTopic']);

            $this->update('{{%ceo}}', ['entity' => 'app\modules\forum\models\FrmCategory'],
                ['entity' => 'app\models\forum\FrmCategory']);

            //com_count
            $this->update('{{%com_counts}}', ['entity' => 'app\modules\brokers\models\Improvement'],
                ['entity' => 'app\models\forum\Improvement']);

            $this->update('{{%com_counts}}', ['entity' => 'app\modules\forum\models\FrmTopic'],
                ['entity' => 'app\models\forum\FrmTopic']);

            $this->update('{{%com_counts}}', ['entity' => 'app\modules\forum\models\FrmCategory'],
                ['entity' => 'app\models\forum\FrmCategory']);

            $this->update('{{%com_counts}}', ['entity' => 'app\modules\forum\models\FrmComment'],
                ['entity' => 'app\models\forum\FrmComment']);

            //com_likes
            $this->update('{{%com_likes}}', ['entity' => 'app\modules\brokers\models\Improvement'],
                ['entity' => 'app\models\forum\Improvement']);

            $this->update('{{%com_likes}}', ['entity' => 'app\modules\forum\models\FrmTopic'],
                ['entity' => 'app\models\forum\FrmTopic']);


            $this->update('{{%com_likes}}', ['entity' => 'app\modules\forum\models\FrmComment'],
                ['entity' => 'app\models\forum\FrmComment']);

            //lang_translate
            $this->update('{{%lang_translate}}', ['entity' => 'app\modules\brokers\models\Improvement'],
                ['entity' => 'app\models\forum\Improvement']);

            //slide
            $this->update('{{%slide}}', ['entity' => 'app\modules\brokers\models\Improvement'],
                ['entity' => 'app\models\forum\Improvement']);

            $transaction->commit();
        }catch (\Exception $e){
            $transaction->rollBack();
            echo $e->getMessage();
            return false;
        }

    }

    public function down()
    {
        $transaction = $this->getDb()->beginTransaction();
        try{
            //ceo
            $this->update('{{%ceo}}', ['entity' => 'app\models\forum\Improvement'],
                ['entity' => 'app\modules\brokers\models\Improvement']);

            $this->update('{{%ceo}}', ['entity' => 'app\models\forum\FrmTopic'],
                ['entity' => 'app\modules\forum\models\FrmTopic']);

            $this->update('{{%ceo}}', ['entity' => 'app\models\forum\FrmCategory'],
                ['entity' => 'app\modules\forum\models\FrmCategory']);

            //com_counts
            $this->update('{{%com_counts}}', ['entity' => 'app\models\forum\Improvement'],
                ['entity' => 'app\modules\brokers\models\Improvement']);

            $this->update('{{%com_counts}}', ['entity' => 'app\models\forum\FrmTopic'],
                ['entity' => 'app\modules\forum\models\FrmTopic']);

            $this->update('{{%com_counts}}', ['entity' => 'app\models\forum\FrmCategory'],
                ['entity' => 'app\modules\forum\models\FrmCategory']);

            $this->update('{{%com_counts}}', ['entity' => 'app\models\forum\FrmComment'],
                ['entity' => 'app\modules\forum\models\FrmComment']);

            //com_likes
            $this->update('{{%com_likes}}', ['entity' => 'app\models\forum\Improvement'],
                ['entity' => 'app\modules\brokers\models\Improvement']);

            $this->update('{{%com_likes}}', ['entity' => 'app\models\forum\FrmTopic'],
                ['entity' => 'app\modules\forum\models\FrmTopic']);

            $this->update('{{%com_likes}}', ['entity' => 'app\models\forum\FrmComment'],
                ['entity' => 'app\modules\forum\models\FrmComment']);

            //lang_translate
            $this->update('{{%lang_translate}}', ['entity' => 'app\models\forum\Improvement'],
                ['entity' => 'app\modules\brokers\models\Improvement']);

            //slide
            $this->update('{{%slide}}', ['entity' => 'app\models\forum\Improvement'],
                ['entity' => 'app\modules\brokers\models\Improvement']);

            $transaction->commit();
            return true;
        }catch (\Exception $e){
            $transaction->rollBack();
            echo $e->getMessage();
            return false;
        }

    }
}
