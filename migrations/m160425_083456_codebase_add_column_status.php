<?php

use yii\db\Migration;

class m160425_083456_codebase_add_column_status extends Migration
{
    public function up()
    {
        $this->addColumn('{{%cdbs_description}}', 'status', "smallint(4)  DEFAULT 0");
    }

    public function down()
    {
        $this->dropColumn('{{%cdbs_description}}', 'status');
    }
}
