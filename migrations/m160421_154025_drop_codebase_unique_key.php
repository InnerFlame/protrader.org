<?php

use yii\db\Migration;

class m160421_154025_drop_codebase_unique_key extends Migration
{
    public function up()
    {
        $this->dropIndex('idx_unique_alias', 'cdbs_description');
    }

    public function down()
    {
        $this->createIndex('idx_unique_alias', 'cdbs_description', 'alias', $unique = true );
    }
}
