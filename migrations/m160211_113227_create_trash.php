<?php

use yii\db\Migration;
use yii\db\Schema;

class m160211_113227_create_trash extends Migration
{
    public $table_name = '{{%trash}}';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table_name, [
            'id'         => Schema::TYPE_INTEGER . '(11) NOT NULL AUTO_INCREMENT',
            'user_id'    => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'entity'     => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'entity_id'  => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'deleted_at' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'PRIMARY KEY (`id`)'
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable($this->table_name);
    }
}
