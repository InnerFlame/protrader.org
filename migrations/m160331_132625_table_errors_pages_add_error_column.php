<?php

use yii\db\Migration;

class m160331_132625_table_errors_pages_add_error_column extends Migration
{
    public function up()
    {
        $this->addColumn('{{%error_pages}}', 'deleted', "smallint(4)  DEFAULT 0");
    }

    public function down()
    {
        $this->dropColumn('{{%error_pages}}', 'deleted');
        return true;
    }
}
