<?php

use yii\db\Schema;
use yii\db\Migration;

class m151217_101517_user_pictures_articles_user_id extends Migration
{
    public function up()
    {
        $this->addColumn('{{%artcl_articles}}', 'user_id', "INT(11) NOT NULL AFTER `category_id`");
        $this->addColumn('{{%news_news}}', 'user_id', "INT(11) NOT NULL  AFTER `category_id`");
        $this->addColumn('{{%user_custom_value}}', 'value', "VARCHAR(256) NOT NULL  AFTER `field_id`");

        $this->dropColumn('{{%user}}', 'avatar');
        $this->addColumn('{{%user}}', 'pictures', "VARCHAR(256) NOT NULL  AFTER `motto`");
    }

    public function down()
    {
        $this->dropColumn('{{%artcl_articles}}', 'user_id');
        $this->dropColumn('{{%news_news}}', 'user_id');
        $this->dropColumn('{{%user_custom_value}}', 'value');

        $this->dropColumn('{{%user}}', 'pictures');
        $this->addColumn('{{%user}}', 'avatar', "INT(11) NOT NULL  AFTER `motto`");
    }

}
