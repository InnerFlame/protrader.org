<?php

use yii\db\Schema;
use yii\db\Migration;

class m160224_085441_status_topics extends Migration
{
    public function up()
    {
        $this->addColumn('{{%frm_topics}}', 'count_comments', "INT(11) NOT NULL AFTER `user_id`");
        $this->addColumn('{{%frm_topics}}', 'status', "INT(11) NOT NULL AFTER `count_comments`");

        $topics = \app\modules\forum\models\FrmTopic::find()->all();

        foreach ($topics as $topic) {
            $topic->count_comments = $topic->getCountComments();
            $topic->save(false);
        }


    }

    public function down()
    {
        $this->dropColumn('{{%frm_topics}}', 'count_comments');
        $this->dropColumn('{{%frm_topics}}', 'status');
        return true;
    }
}
