<?php

use yii\db\Schema;
use yii\db\Migration;
use app\modules\forum\models\FrmCategory;

class m151201_082745_add_parent_to_articles extends Migration
{

    public function up()
    {

        $this->addColumn('{{%artcl_category}}', 'parent', "INT(11) DEFAULT NULL AFTER `id`");

        $this->db->schema->refresh();


    }

    public function down()
    {
        $this->dropColumn('{{%artcl_category}}', 'parent');

    }

}
