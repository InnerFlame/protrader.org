<?php

use yii\db\Migration;

class m160511_091551_change_name_column_notify_tempolates extends Migration
{
    public function up()
    {
        $this->renameColumn('{{%notify_templates}}', 'pk', 'id');
    }

    public function down()
    {
        $this->renameColumn('{{%notify_templates}}', 'id', 'pk');
        return true;
    }

}
