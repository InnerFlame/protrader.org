<?php

use yii\db\Migration;

class m160217_122638_add_sender_id_column_to_notifications_table extends Migration
{
    public function up()
    {
        $this->addColumn('{{%notifications}}', 'sender_id', 'int(11) NOT NULL');

        $this->addForeignKey('fk_sender_id_to_user_id', '{{%notifications}}', 'sender_id', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk_user_id_to_user_id', '{{%notifications}}', 'user_id', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk_type_id_to_notifications_types_id', '{{%notifications}}', 'type_id', '{{%notifications_type}}', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_sender_id_to_user_id', '{{%notifications}}');
        $this->dropForeignKey('fk_user_id_to_user_id', '{{%notifications}}');
        $this->dropForeignKey('fk_type_id_to_notifications_types_id', '{{%notifications}}');
        $this->dropColumn('{{%notifications}}', 'sender_id');
    }
}
