<?php

use yii\db\Migration;

class m160307_125917_init_features_request extends Migration
{
    public function up()
    {

        $this->createTable('{{%features_request}}',[
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'title' => $this->string(60)->notNull(),
            'alias' => $this->string(255)->notNull(),
            'content' => $this->text()->notNull(),
            'status' => $this->smallInteger(1)->defaultValue(1),
            'deleted' => $this->smallInteger(1)->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('fk_features_request_to_users', '{{%features_request}}', 'user_id', '{{%user}}', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('fk_features_request_to_users', '{{%features_request}}');
        $this->dropTable('{{%features_request}}');
        return true;
    }
}
