<?php

use yii\db\Schema;
use yii\db\Migration;

class m150804_084601_init extends Migration
{
    public $table_name = '{{%user}}';

    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
            $tableOptionsMyISAM = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=MyISAM';
        }



        $this->createTable($this->table_name, [
            'id'                   => Schema::TYPE_INTEGER . '(11) NOT NULL AUTO_INCREMENT',
            'username'             => Schema::TYPE_STRING . '(64) DEFAULT NULL',
            'fullname'             => Schema::TYPE_STRING . '(64) DEFAULT NULL',
            'password_hash'        => Schema::TYPE_STRING . '(64) DEFAULT NULL',
            'password_reset_token' => Schema::TYPE_STRING . '(64) DEFAULT NULL',
            'auth_key'             => Schema::TYPE_STRING . '(32) DEFAULT NULL',
            'email'                => Schema::TYPE_STRING . '(32) DEFAULT NULL',
            'role'                 => Schema::TYPE_SMALLINT . '(4) DEFAULT 0',
            'status'               => Schema::TYPE_SMALLINT . '(4) DEFAULT 0',
            'motto'                => Schema::TYPE_STRING . '(128) DEFAULT NULL',
            'avatar'               => Schema::TYPE_STRING . '(128) DEFAULT NULL',
            'vkontakte_id'         => Schema::TYPE_INTEGER . '(11) DEFAULT NULL',
            'linkedin_id'          => Schema::TYPE_STRING . '(32) DEFAULT NULL',
            'google_id'            => Schema::TYPE_INTEGER . '(11) DEFAULT NULL',
            'facebook_id'          => Schema::TYPE_INTEGER . '(11) DEFAULT NULL',
            'twitter_id'           => Schema::TYPE_INTEGER . '(11) DEFAULT NULL',
            'birth_at'             => Schema::TYPE_INTEGER . '(11) DEFAULT NULL',
            'created_at'           => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'updated_at'           => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'deleted_at'           => Schema::TYPE_INTEGER . '(11) DEFAULT NULL',
            'PRIMARY KEY (`id`)'
        ], $tableOptions);

        $this->createTable('{{%user_settings}}', [
            'user_id' => 'INT(8) NOT NULL',
            'lang_id' => 'TINYINT(1) NOT NULL DEFAULT 1',
            'PRIMARY KEY (`user_id`),
             INDEX fk_user_settings_user_idx (`user_id` ASC),
             CONSTRAINT `fk_user_settings_user`
                FOREIGN KEY (`user_id`)
                REFERENCES {{%user}} (`id`)
                ON DELETE CASCADE
                ON UPDATE CASCADE'
        ], $tableOptions);


        $this->createTable('{{%user_profile}}', [
            'user_id' => 'INT(11) NOT NULL',
            'salutation' => 'TINYINT(1) NULL DEFAULT NULL',
            'first_name' => 'VARCHAR(60) NOT NULL',
            'second_name' => 'VARCHAR(60) NULL DEFAULT NULL',
            'last_name' => 'VARCHAR(60) NOT NULL',
            'address' => 'VARCHAR(255) NULL DEFAULT NULL',
            'phone' => 'VARCHAR(60) NULL DEFAULT NULL',
            'skype' => 'VARCHAR(60) NULL DEFAULT NULL',
            'birth_date' => 'INT(11) DEFAULT NULL',
            'PRIMARY KEY (`user_id`),
             INDEX fk_user_profile_user_idx (`user_id` ASC),
             CONSTRAINT `fk_user_profile_user`
                FOREIGN KEY (`user_id`)
                REFERENCES {{%user}} (`id`)
                ON DELETE CASCADE
                ON UPDATE CASCADE'
        ], $tableOptions);




        $this->db->schema->refresh();

        $initDataArr = require(__DIR__ . '/../config/init_migration.php');

        $this->insert('{{%user}}', $initDataArr['user']);
        $this->insert('{{%user_profile}}', $initDataArr['user_profile']);

    }

    public function down()
    {
        $this->dropTable('{{%user_profile}}');
        $this->dropTable('{{%user_settings}}');
        $this->dropTable('{{%user}}');
    }
}
