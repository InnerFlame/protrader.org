<?php

use yii\db\Migration;

class m160421_065832_add_return_votes_field_to_feature_request extends Migration
{
    public function up()
    {
        $this->addColumn('{{%features_request}}', 'return_votes', 'TINYINT(1) DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn('{{%features_request}}', 'return_votes');
    }
}
