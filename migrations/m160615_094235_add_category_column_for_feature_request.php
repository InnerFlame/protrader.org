<?php

use yii\db\Migration;

class m160615_094235_add_category_column_for_feature_request extends Migration
{
    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%features_request_categories}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'deleted' => $this->integer()->defaultValue(0),
        ], $tableOptions);

        $this->batchInsert('{{%features_request_categories}}', ['name', 'created_at', 'updated_at'], [
            [
                'name' => 'Feature',
                'created_at' => time(),
                'updated_at' => time()
            ],
            [
                'name' => 'Connection',
                'created_at' => time(),
                'updated_at' => time()
            ],
            [
                'name' => 'Interface',
                'created_at' => time(),
                'updated_at' => time()
            ]
        ]);

        $this->addColumn('{{%features_request}}', 'category_id', 'INT DEFAULT 1');
        $this->addColumn('{{%features_request}}', 'published', 'INT DEFAULT 1');
        $this->addForeignKey('fk_features_request_to_category', '{{%features_request}}', 'category_id', '{{%features_request_categories}}', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_features_request_to_category', '{{%features_request}}');
        $this->dropColumn('{{%features_request}}', 'category_id');
        $this->dropTable('{{%features_request_categories}}');
        return true;
    }
}
