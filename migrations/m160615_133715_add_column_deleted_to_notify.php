<?php

use yii\db\Migration;

class m160615_133715_add_column_deleted_to_notify extends Migration
{
    public function up()
    {
        $this->addColumn(app\modules\notify\models\Event::tableName(), 'deleted', 'TINYINT(1) DEFAULT 0');
        $this->addColumn(app\modules\notify\models\EventMessage::tableName(), 'deleted', 'TINYINT(1) DEFAULT 0');
        $this->addColumn(app\modules\notify\models\EventQueue::tableName(), 'deleted', 'TINYINT(1) DEFAULT 0');
        $this->addColumn(app\modules\notify\models\EventSubscribe::tableName(), 'deleted', 'TINYINT(1) DEFAULT 0');
        $this->addColumn(app\modules\notify\models\EventUserSettings::tableName(), 'deleted', 'TINYINT(1) DEFAULT 0');
        $this->addColumn(app\modules\cabinet\modules\notify\models\NotifyTemplate::tableName(), 'deleted', 'TINYINT(1) DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn(app\modules\notify\models\Event::tableName(), 'deleted');
        $this->dropColumn(app\modules\notify\models\EventMessage::tableName(), 'deleted');
        $this->dropColumn(app\modules\notify\models\EventQueue::tableName(), 'deleted');
        $this->dropColumn(app\modules\notify\models\EventSubscribe::tableName(), 'deleted');
        $this->dropColumn(app\modules\notify\models\EventUserSettings::tableName(), 'deleted');
        $this->dropColumn(app\modules\cabinet\modules\notify\models\NotifyTemplate::tableName(), 'deleted');
    }
}