<?php

use yii\db\Schema;
use yii\db\Migration;
use app\modules\forum\models\FrmCategory;

class m151127_133453_forum extends Migration
{


    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%frm_category}}', [
            'id'          => Schema::TYPE_INTEGER . '(11) NOT NULL AUTO_INCREMENT',
            'title'       => Schema::TYPE_STRING . '(64) NOT NULL',
            'alias'       => Schema::TYPE_STRING . '(64) NOT NULL',
            'weight'      => Schema::TYPE_SMALLINT . '(4) DEFAULT 0',
            'description' => Schema::TYPE_STRING . '(255) DEFAULT NULL',
            'root'        => Schema::TYPE_INTEGER . '(11) DEFAULT 1',
            'lft'         => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'rgt'         => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'level'       => Schema::TYPE_SMALLINT . '(4) NOT NULL',
            'published'   => Schema::TYPE_SMALLINT . '(4) DEFAULT NULL',
            'created_at'  => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'updated_at'  => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'deleted_at'  => Schema::TYPE_INTEGER . '(11) DEFAULT NULL',
            'PRIMARY KEY (`id`)'
        ], $tableOptions);


        $this->createTable('{{%frm_topics}}', [
            'id'          => 'INT(8) NOT NULL AUTO_INCREMENT',
            'category_id' => 'INT(8) NOT NULL',
            'user_id'     => 'INT(8) NOT NULL',
            'content'     => 'TEXT NULL DEFAULT NULL',
            'title'       => 'VARCHAR(60) NULL DEFAULT NULL',
            'published'   => 'INT(11) NULL DEFAULT NULL',
            'created_at'  => 'INT(11) NULL DEFAULT NULL',
            'updated_at'  => 'INT(11) NULL DEFAULT NULL',
            'PRIMARY KEY (`id`),
                    INDEX fk_topic_category (`id` ASC),
                     CONSTRAINT `fk_forums_category`
                        FOREIGN KEY (`category_id`)
                        REFERENCES {{%frm_category}} (`id`)
                          ON DELETE CASCADE
                          ON UPDATE CASCADE'

        ], $tableOptions);


        $this->createTable('{{%frm_comments}}', [
            'id'         => 'INT(8) NOT NULL AUTO_INCREMENT',
            'topic_id'   => 'INT(8) NOT NULL',
            'user_id'    => 'INT(8) NOT NULL',
            'content'    => 'TEXT NULL DEFAULT NULL',
            'published'  => 'INT(11) NULL DEFAULT NULL',
            'created_at' => 'INT(11) NULL DEFAULT NULL',
            'updated_at' => 'INT(11) NULL DEFAULT NULL',
            'PRIMARY KEY (`id`)'

        ], $tableOptions);

        $this->createTable('{{%frm_improvements}}', [
            'id'           => 'INT(8) NOT NULL AUTO_INCREMENT',
            'user_id'      => 'INT(8) NULL DEFAULT NULL',
            'title'        => 'VARCHAR(255) NULL DEFAULT NULL',
            'content'      => 'TEXT NULL DEFAULT NULL',
            'status'       => 'INT(8) DEFAULT NULL',
            'created_at'   => 'INT(11) NULL DEFAULT NULL',
            'updated_at'   => 'INT(11) NULL DEFAULT NULL',
            'published_at' => 'INT(11) NULL DEFAULT NULL',
            'PRIMARY KEY (`id`)'

        ], $tableOptions);

        $this->db->schema->refresh();



    }

    public function down()
    {
        $this->dropTable('{{%frm_topics}}');
        $this->dropTable('{{%frm_comments}}');
        $this->dropTable('{{%frm_improvements}}');
        $this->dropTable('{{%frm_category}}');
    }
}






