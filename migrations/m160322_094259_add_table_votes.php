<?php

use yii\db\Migration;

class m160322_094259_add_table_votes extends Migration
{
    public $tableName = '{{%com_votes}}';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName,[
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'entity' => $this->smallInteger(4)->notNull(),
            'entity_id' => $this->integer()->notNull(),
            'count' => $this->integer()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),

        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable($this->tableName);
    }

}
