<?php

use yii\db\Schema;
use yii\db\Migration;

class m160128_104530_replace_entity_column_for_forum extends Migration
{
    public function up()
    {
        $this->update('{{%com_comments}}', ['entity' => 'app\modules\brokers\models\Improvement'],
            ['entity' => 'app\models\forum\Improvement']);
    }

    public function down()
    {
        $this->update('{{%com_comments}}', ['entity' => 'app\models\forum\Improvement'],
            ['entity' => 'app\modules\brokers\models\Improvement']);
        return true;
    }
}
