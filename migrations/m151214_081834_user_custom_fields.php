<?php

use yii\db\Schema;
use yii\db\Migration;

class m151214_081834_user_custom_fields extends Migration
{
    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }


        $this->createTable('{{%user_custom_field}}', [
            'id' => 'INT(8) NOT NULL AUTO_INCREMENT',
            'name' => 'VARCHAR(64) NOT NULL',
            'alias' => 'VARCHAR(32) NOT NULL',
            'data' => 'TEXT DEFAULT NULL',
            'PRIMARY KEY (`id`),
                     INDEX fk_user_custom_field_user_idx (`id` ASC)'
        ], $tableOptions);



        $this->createTable('{{%user_custom_value}}', [
            'user_id' => 'INT(8) NOT NULL',
            'field_id' => 'INT(8) NOT NULL',
            'PRIMARY KEY (`user_id`, `field_id`),
                     INDEX fk_user_custom_value_user_idx (`user_id` ASC),
                     CONSTRAINT `fk_user_custom_value_user`
                        FOREIGN KEY (`user_id`)
                        REFERENCES {{%user}} (`id`)
                        ON DELETE CASCADE
                        ON UPDATE CASCADE,
                     INDEX fk_user_custom_value_user_field_idx (`field_id` ASC),
                     CONSTRAINT `fk_user_custom_value_user_field`
                        FOREIGN KEY (`field_id`)
                        REFERENCES {{%user_custom_field}} (`id`)
                        ON DELETE CASCADE
                        ON UPDATE CASCADE'
        ], $tableOptions);
    }


    public function down()
    {
        $this->dropTable('{{%user_custom_value}}');
        $this->dropTable('{{%user_custom_field}}');

    }
}
