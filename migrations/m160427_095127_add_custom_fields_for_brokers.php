<?php

use yii\db\Migration;

class m160427_095127_add_custom_fields_for_brokers extends Migration
{
    public function up()
    {
        $this->addColumn('{{broker}}', 'type', 'INT DEFAULT 0');

        $this->batchInsert('{{%broker_custom_field}}', ['name', 'alias'], [
            [
                'name' => 'available_instruments',
                'alias' => 'available_instruments'
            ],
            [
                'name' => 'real_time_data',
                'alias' => 'real_time_data'
            ],
            [
                'name' => 'historical_data',
                'alias' => 'historical_data'
            ],
            [
                'name' => 'pricing',
                'alias' => 'pricing'
            ],
            [
                'name' => 'pricing_url',
                'alias' => 'pricing_url'
            ],
        ]);
    }

    public function down()
    {
        $this->dropColumn('{{broker}}', 'type');
        $this->delete('{{%broker_custom_field}}', 'alias = :alias', [':alias' => 'available_instruments']);
        $this->delete('{{%broker_custom_field}}', 'alias = :alias', [':alias' => 'real_time_data']);
        $this->delete('{{%broker_custom_field}}', 'alias = :alias', [':alias' => 'historical_data']);
        $this->delete('{{%broker_custom_field}}', 'alias = :alias', [':alias' => 'pricing']);
        $this->delete('{{%broker_custom_field}}', 'alias = :alias', [':alias' => 'pricing_url']);
        return true;
    }
}
