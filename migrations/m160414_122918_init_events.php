<?php

use yii\db\Migration;

class m160414_122918_init_events extends Migration {
    public function up() {

        $this->batchInsert('{{%notify_events}}', ['name', 'label', 'enabled', 'created_at'], [
            [
                'name' => \app\modules\forum\ForumModule::EVENT_COMMENT_ADD,
                'label' => 'New posts in subscribed forum',
                'enabled' => 0,
                'created_at' => time()
            ],
            [
                'name' => \app\modules\forum\ForumModule::EVENT_TOPIC_ADD,
                'label' => 'New topics',
                'enabled' => 0,
                'created_at' => time()
            ],
            [
                'name' => \app\modules\forum\ForumModule::EVENT_COMMENT_EDIT,
                'label' => 'Edited posts',
                'enabled' => 0,
                'created_at' => time()
            ],
            [
                'name' => \app\modules\forum\ForumModule::EVENT_TOPIC_EDIT,
                'label' => 'Edited topics',
                'enabled' => 0,
                'created_at' => time()
            ],
            [
                'name' => \app\modules\forum\ForumModule::EVENT_TOPIC_MOVE,
                'label' => 'Topic has moved',
                'enabled' => 0,
                'created_at' => time()
            ]
        ]);
    }

    public function down() {
        $this->delete('{{%notify_events}}');
        return true;
    }
}