<?php

use yii\db\Migration;

class m160601_101953_add_columns_deleted_for_faqs_entity extends Migration
{
    public function up()
    {
        $this->addColumn(\app\models\main\Faq::tableName(), 'deleted', 'TINYINT(1) DEFAULT 0');
        $this->addColumn(\app\models\main\FaqCategory::tableName(), 'deleted', 'TINYINT(1) DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn(\app\models\main\Faq::tableName(), 'deleted');
        $this->dropColumn(\app\models\main\FaqCategory::tableName(), 'deleted');
        return true;
    }
}
