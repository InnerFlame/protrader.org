<?php

use yii\db\Schema;
use yii\db\Migration;

class m160128_105123_alter_use_settings extends Migration
{
    public function up()
    {
        $this->dropPrimaryKey('user_id', 'user_settings');

        $this->addColumn('{{%user_settings}}', 'id', "INT(8) NOT NULL AUTO_INCREMENT, ADD PRIMARY KEY (`id`)");

        $this->db->schema->refresh();
    }

    public function down()
    {

    }
}
