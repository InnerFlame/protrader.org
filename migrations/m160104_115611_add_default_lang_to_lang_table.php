<?php

use yii\db\Schema;
use yii\db\Migration;

class m160104_115611_add_default_lang_to_lang_table extends Migration
{
    public function up()
    {

        $initDataArr = require(__DIR__ . '/../config/init_migration.php');

        foreach($initDataArr['lang'] as $_lang)
            $this->insert('{{%lang_lang}}', $_lang);
    }

    public function down()
    {
       $english =  \app\models\lang\Languages::find()->where(['name' => 'english'])->one();
        if(is_object($english))$english->delete();

        $russian =  \app\models\lang\Languages::find()->where(['name' => 'russian'])->one();
        if(is_object($russian))$russian->delete();
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
