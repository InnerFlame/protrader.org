<?php

use yii\db\Schema;
use yii\db\Migration;

class m160311_143215_add_column_update_user_id_to_the_table_frmtopic extends Migration
{
    public function up()
    {
        $this->addColumn('{{%frm_topics}}', 'update_user_id', 'integer');
        $this->initOldSets('{{%frm_topics}}');
    }

    public function down()
    {
        $this->dropColumn('{{%frm_topics}}','update_user_id');

    }

    protected function initOldSets($tableName)
    {
        $connection = Yii::$app->getDb();
        $cmd = $connection->createCommand("UPDATE $tableName SET update_user_id = user_id");
        $cmd->query();
    }

}
