<?php

use yii\db\Migration;

class m160331_102135_table_errors_pages_add extends Migration
{
    public function up()
    {
        $this->createTable('{{%error_pages}}', [
            'id' => $this->primaryKey(),
            'expression' => $this->string(255)->notNull(),
            'title' => $this->string(128)->notNull(),
            'url' => $this->string(255)->notNull(),
            'view' => $this->integer()->defaultValue(0),
            'jumps' => $this->integer()->defaultValue(0)
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%error_pages}}');
        return true;
    }
}
