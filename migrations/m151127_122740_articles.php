<?php

use yii\db\Schema;
use yii\db\Migration;

class m151127_122740_articles extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
            $tableOptionsMyISAM = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=MyISAM';
        }


        $this->createTable('{{%artcl_category}}', [
            'id' => 'INT(8) NOT NULL AUTO_INCREMENT',
            'title' => 'VARCHAR(60) NULL DEFAULT NULL',
            'alias' => 'VARCHAR(256) NULL DEFAULT NULL',
            'data' => 'VARCHAR(256) NULL DEFAULT NULL',
            'weight' => 'INT(8) NULL DEFAULT NULL',
            'published' => 'INT(11) NULL DEFAULT NULL',
            'created_at' => 'INT(11) NULL DEFAULT NULL',
            'updated_at' => 'INT(11) NULL DEFAULT NULL',

            'PRIMARY KEY (`id`)'

        ], $tableOptions);


        $this->createTable('{{%artcl_articles}}', [
            'id' => 'INT(8) NOT NULL AUTO_INCREMENT',
            'category_id' => 'INT(8) NOT NULL',
            'title' => 'VARCHAR(60) NULL DEFAULT NULL',
            'alias' => 'VARCHAR(60) NULL DEFAULT NULL',
            'pictures' => 'VARCHAR(60) NULL DEFAULT NULL',
            'content' => 'TEXT NULL DEFAULT NULL',
            'published' => 'INT(11) NULL DEFAULT NULL',
            'created_at' => 'INT(11) NULL DEFAULT NULL',
            'updated_at' => 'INT(11) NULL DEFAULT NULL',
            'PRIMARY KEY (`id`),
            INDEX fk_user_profile_user_idx (`id` ASC),
             CONSTRAINT `fk_articles_category`
                FOREIGN KEY (`category_id`)
                REFERENCES {{%artcl_category}} (`id`)
                  ON DELETE CASCADE
                  ON UPDATE CASCADE'

        ], $tableOptions);



        $this->db->schema->refresh();

    }

    public function down()
    {

        $this->dropTable('{{%artcl_articles}}');
        $this->dropTable('{{%artcl_category}}');
    }
}
