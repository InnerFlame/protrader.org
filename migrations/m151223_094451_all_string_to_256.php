<?php

use yii\db\Schema;
use yii\db\Migration;

class m151223_094451_all_string_to_256 extends Migration
{
    public function up()
    {


        $this->alterColumn('{{%user_profile}}', 'first_name', "VARCHAR(255) DEFAULT NULL AFTER `salutation`");
        $this->alterColumn('{{%user_profile}}', 'second_name', "VARCHAR(255) DEFAULT NULL AFTER `first_name`");
        $this->alterColumn('{{%user_profile}}', 'last_name', "VARCHAR(255) DEFAULT NULL AFTER `second_name`");
        $this->alterColumn('{{%user_profile}}', 'phone', "VARCHAR(255) DEFAULT NULL AFTER `last_name`");
        $this->alterColumn('{{%user_profile}}', 'skype', "VARCHAR(255) DEFAULT NULL AFTER `phone`");



        $this->alterColumn('{{%vd_category}}', 'title', "VARCHAR(255) DEFAULT NULL AFTER `id`");

        $this->alterColumn('{{%vd_playlist}}', 'title', "VARCHAR(255) DEFAULT NULL AFTER `id`");


        $this->alterColumn('{{%vd_video}}', 'title', "VARCHAR(255) DEFAULT NULL AFTER `category_id`");
        $this->alterColumn('{{%vd_video}}', 'pictures', "VARCHAR(255) DEFAULT NULL AFTER `title`");


        $this->alterColumn('{{%artcl_articles}}', 'title', "VARCHAR(255) DEFAULT NULL AFTER `category_id`");
        $this->alterColumn('{{%artcl_articles}}', 'alias', "VARCHAR(255) DEFAULT NULL AFTER `title`");
        $this->alterColumn('{{%artcl_articles}}', 'pictures', "VARCHAR(255) DEFAULT NULL AFTER `alias`");


        $this->alterColumn('{{%frm_category}}', 'title', "VARCHAR(255) DEFAULT NULL AFTER `id`");
        $this->alterColumn('{{%frm_category}}', 'alias', "VARCHAR(255) DEFAULT NULL AFTER `title`");


        $this->alterColumn('{{%main_pages}}', 'title', "VARCHAR(255) DEFAULT NULL AFTER `id`");
        $this->alterColumn('{{%main_pages}}', 'alias', "VARCHAR(255) DEFAULT NULL AFTER `title`");


        $this->alterColumn('{{%main_faq_category}}', 'title', "VARCHAR(255) DEFAULT NULL AFTER `parent_id`");


        $this->alterColumn('{{%main_faq}}', 'title', "VARCHAR(255) DEFAULT NULL AFTER `answer`");


        $this->alterColumn('{{%role}}', 'name', "VARCHAR(255) DEFAULT NULL AFTER `id`");
        $this->alterColumn('{{%role}}', 'alias', "VARCHAR(255) DEFAULT NULL AFTER `name`");
        $this->alterColumn('{{%role}}', 'comment', "VARCHAR(255) DEFAULT NULL AFTER `alias`");


        $this->alterColumn('{{%rule}}', 'name', "VARCHAR(255) DEFAULT NULL AFTER `type_id`");
        $this->alterColumn('{{%rule}}', 'alias', "VARCHAR(255) DEFAULT NULL AFTER `name`");
        $this->alterColumn('{{%rule}}', 'comment', "VARCHAR(255) DEFAULT NULL AFTER `alias`");

        $this->alterColumn('{{%news_category}}', 'title', "VARCHAR(255) DEFAULT NULL AFTER `id`");

        $this->alterColumn('{{%news_news}}', 'title', "VARCHAR(255) DEFAULT NULL AFTER `category_id`");
        $this->alterColumn('{{%news_news}}', 'alias', "VARCHAR(255) DEFAULT NULL AFTER `title`");
        $this->alterColumn('{{%news_news}}', 'pictures', "VARCHAR(255) DEFAULT NULL AFTER `alias`");


        $this->alterColumn('{{%user_custom_field}}', 'name', "VARCHAR(255) DEFAULT NULL AFTER `id`");
        $this->alterColumn('{{%user_custom_field}}', 'alias', "VARCHAR(255) DEFAULT NULL AFTER `name`");

        $this->alterColumn('{{%settings_custom_field}}', 'name', "VARCHAR(255) DEFAULT NULL AFTER `id`");
        $this->alterColumn('{{%settings_custom_field}}', 'alias', "VARCHAR(255) DEFAULT NULL AFTER `name`");

        $this->alterColumn('{{%broker}}', 'name', "VARCHAR(255) DEFAULT NULL AFTER `id`");
        $this->alterColumn('{{%broker}}', 'email', "VARCHAR(255) DEFAULT NULL AFTER `name`");
        $this->alterColumn('{{%broker}}', 'logo', "VARCHAR(255) DEFAULT NULL AFTER `status`");
        $this->alterColumn('{{%broker}}', 'account', "VARCHAR(255) DEFAULT NULL AFTER `logo`");
        $this->alterColumn('{{%broker}}', 'regulation', "VARCHAR(255) DEFAULT NULL AFTER `account`");
        $this->alterColumn('{{%broker}}', 'traders', "VARCHAR(255) DEFAULT NULL AFTER `instruments`");


        $this->alterColumn('{{%broker_custom_field}}', 'name', "VARCHAR(255) DEFAULT NULL AFTER `id`");
        $this->alterColumn('{{%broker_custom_field}}', 'alias', "VARCHAR(255) DEFAULT NULL AFTER `name`");


        $this->alterColumn('{{%slide}}', 'title', "VARCHAR(255) DEFAULT NULL AFTER `id`");
        $this->alterColumn('{{%slide}}', 'pictures', "VARCHAR(255) DEFAULT NULL AFTER `title`");
        $this->alterColumn('{{%slide}}', 'link', "VARCHAR(255) DEFAULT NULL AFTER `pictures`");
        $this->alterColumn('{{%slide}}', 'description', "VARCHAR(255) DEFAULT NULL AFTER `link`");


        $this->alterColumn('{{%frm_improvements_category}}', 'title', "VARCHAR(255) DEFAULT NULL AFTER `id`");

    }

    public function down()
    {

        return true;
    }

}
