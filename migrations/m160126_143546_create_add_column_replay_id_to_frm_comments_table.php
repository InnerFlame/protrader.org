<?php

use yii\db\Schema;
use yii\db\Migration;

class m160126_143546_create_add_column_replay_id_to_frm_comments_table extends Migration
{
    public function up()
    {
        $this->addColumn('{{%frm_comments}}', 'reply_id', "INT(8)");
    }

    public function down()
    {
        $this->dropColumn('{{%frm_comments}}', 'reply_id');
        return true;
    }
}
