<?php

use yii\db\Schema;
use yii\db\Migration;

class m151221_102750_ceo extends Migration
{
    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%ceo}}', [
            'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
            'entity' => 'VARCHAR(128) NOT NULL',
            'entity_id' => 'INT(8) NOT NULL',
            'title' => 'VARCHAR(255) DEFAULT NULL',
            'description' => 'text DEFAULT NULL',
            'keywords' => 'text DEFAULT NULL',
            'h1' => 'VARCHAR(255) DEFAULT NULL',
            'canonical' => 'VARCHAR(255) DEFAULT NULL',
            'PRIMARY KEY (`id`)'
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%ceo}}');

    }
}
