<?php

use yii\db\Schema;
use yii\db\Migration;

class m151215_081715_com_counts_add_count_dislike extends Migration
{
    public function up()
    {
        $this->addColumn('{{%com_counts}}', 'count_dislike', "INT(8) NULL DEFAULT 0 AFTER `count_like`");

        $this->db->schema->refresh();
    }

    public function down()
    {
        $this->dropColumn('{{%com_counts}}', 'count_dislike');
    }
}
