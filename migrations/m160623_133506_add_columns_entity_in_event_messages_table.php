<?php

use yii\db\Migration;
use app\modules\notify\models\EventMessage;
use app\components\Entity;

class m160623_133506_add_columns_entity_in_event_messages_table extends Migration
{
    public function up()
    {
        $this->addColumn(EventMessage::tableName(), 'entity', 'INT(5)');
        $this->addColumn(EventMessage::tableName(), 'entity_id', 'INT');

        $this->createIndex('idx_event_messages_entity', EventMessage::tableName(), ['entity', 'entity_id']);

        $this->initEntityForAllMessages();

    }

    public function down()
    {
        $this->dropIndex('idx_event_messages_entity', EventMessage::tableName());
        $this->dropColumn(EventMessage::tableName(), 'entity');
        $this->dropColumn(EventMessage::tableName(), 'entity_id');
        return true;
    }

    /**
     * Вытягивает Entity из сериализованных данных сообщения
     * и устанавливает тип и идентификатор entity в соответствующие
     * поля.
     * Те сообщения, в сериализованных данных которых, находится битая
     * (несуществующая) ссылка на Entity буду удалены
     */
    private function initEntityForAllMessages()
    {
        foreach(EventMessage::find()->batch(30) as $key => $messages){
            printf('Started batch is %d ', $key);
            foreach($messages as $message){
                $entity = $message->getNotify()->getEntity();
                if($entity instanceof Entity){
                    $message->entity = $entity->getTypeEntity();
                    $message->entity_id = $entity->id;
                    if($message->save()){
                        printf("Added new entity: %d, %d to event message: %d \n", $message->id, $entity->getTypeEntity(), $entity->id);
                        continue;
                    }
                    printf("Error message: %d, not saved with parameters: entity: %d, entity_id: %d \n", $message->id, $entity->getTypeEntity(), $entity->id);
                    continue;
                }
                if($message->delete()){
                    printf("Deleted event messages reason: not found linked entity. \n");
                }
            }
        }
    }
}
