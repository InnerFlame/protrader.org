<?php

use yii\db\Schema;
use yii\db\Migration;

class m151216_105952_create_broker_tables extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%broker}}', [
            'id'                   => 'INT(11) NOT NULL AUTO_INCREMENT',
            'name'                 => 'VARCHAR(128) DEFAULT NULL',
            'email'                => 'VARCHAR(128) DEFAULT NULL',
            'site'                 => 'VARCHAR(128) DEFAULT NULL',
            'add_link'             => 'VARCHAR(255) DEFAULT NULL',
            'status'               => 'INT(4) DEFAULT NULL',
            'logo'                 => 'VARCHAR(128) DEFAULT NULL',
            'account'              => 'VARCHAR(128) DEFAULT NULL',
            'regulation'           => 'VARCHAR(128) DEFAULT NULL',
            'instruments'           => 'VARCHAR(255) DEFAULT NULL',
            'traders'              => 'VARCHAR(128) DEFAULT NULL',
            'votes'                => 'INT(4) DEFAULT NULL',
            'is_demo'              => 'TINYINT(1) DEFAULT NULL',
            'updated_at'           => 'INT(11) NOT NULL',
            'created_at'           => 'INT(11) NOT NULL',
            'deleted_at'           => 'INT(11) DEFAULT NULL',
            'PRIMARY KEY (`id`)'
        ], $tableOptions);

        $this->createTable('{{%broker_improvements}}', [
            'broker_id' => 'INT(8) NOT NULL',
            'improvement_id' => 'INT(8) NOT NULL',
            'PRIMARY KEY (`broker_id`, `improvement_id`),
                     INDEX fk_broker_improvements_broker_idx (`broker_id` ASC),
                     CONSTRAINT `fk_borker_improvements_borker`
                        FOREIGN KEY (`broker_id`)
                        REFERENCES {{%broker}} (`id`)
                        ON DELETE CASCADE
                        ON UPDATE CASCADE,
                     INDEX fk_broker_improvements_improvements_idx (`improvement_id` ASC),
                     CONSTRAINT `fk_broker_improvements_improvements_field`
                        FOREIGN KEY (`improvement_id`)
                        REFERENCES {{%frm_improvements}} (`id`)
                        ON DELETE CASCADE
                        ON UPDATE CASCADE'
        ], $tableOptions);

        $this->createTable('{{%broker_custom_field}}', [
            'id' => 'INT(8) NOT NULL AUTO_INCREMENT',
            'name' => 'VARCHAR(64) NOT NULL',
            'alias' => 'VARCHAR(32) NOT NULL',
            'data' => 'TEXT DEFAULT NULL',
            'PRIMARY KEY (`id`),
                     INDEX fk_broker_custom_field_broker_idx (`id` ASC)'
        ], $tableOptions);

        $this->createTable('{{%broker_custom_value}}', [
            'broker_id' => 'INT(8) NOT NULL',
            'field_id' => 'INT(8) NOT NULL',
            'value' => 'TEXT',
            'PRIMARY KEY (`broker_id`, `field_id`),
                     INDEX fk_user_custom_value_user_idx (`broker_id` ASC),
                     CONSTRAINT `fk_borker_custom_value_borker`
                        FOREIGN KEY (`broker_id`)
                        REFERENCES {{%broker}} (`id`)
                        ON DELETE CASCADE
                        ON UPDATE CASCADE,
                     INDEX fk_broker_custom_value_broker_field_idx (`field_id` ASC),
                     CONSTRAINT `fk_broker_custom_value_broker_field`
                        FOREIGN KEY (`field_id`)
                        REFERENCES {{%broker_custom_field}} (`id`)
                        ON DELETE CASCADE
                        ON UPDATE CASCADE'
        ], $tableOptions);

        $this->db->schema->refresh();

    }

    public function down()
    {
        $this->dropTable('{{%broker_custom_value}}');
        $this->dropTable('{{%broker_custom_field}}');
        $this->dropTable('{{%broker_improvements}}');
        $this->dropTable('{{%broker}}');

    }

}
