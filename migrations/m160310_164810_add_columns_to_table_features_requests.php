<?php

use yii\db\Migration;

class m160310_164810_add_columns_to_table_features_requests extends Migration
{
    public function up()
    {
        $this->addColumn('{{%features_request}}', 'platform', 'tinyint(1) DEFAULT 1');
    }

    public function down()
    {
        $this->dropColumn('{{%features_request}}', 'platform');
    }
}
