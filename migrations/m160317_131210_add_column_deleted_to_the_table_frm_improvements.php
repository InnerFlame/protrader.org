<?php

use yii\db\Schema;
use yii\db\Migration;

class m160317_131210_add_column_deleted_to_the_table_frm_improvements extends Migration
{
    public function up()
    {
        $this->addColumn('{{%frm_improvements_category}}', 'deleted', "smallint(4)  DEFAULT 0 AFTER `published`");
    }

    public function down()
    {
        $this->dropColumn('{{%frm_improvements_category}}', 'deleted');
    }


}
