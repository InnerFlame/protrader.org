<?php

use yii\db\Migration;
use app\modules\brokers\models\Broker;
use app\modules\features\models\FeatureRequest;
use app\models\community\Likes;
use app\components\widgets\vote\models\Vote;
use app\models\user\User;

/**
 * Миграция брокеров которые еще не подключены в фичи реквест, а так же перенос голосов
 * на движок фичи реквест
 * Class m160615_100902_init_brokers_as_features_request
 */
class m160615_100902_init_brokers_as_features_request extends Migration
{
    protected $statusMap = [
        Broker::STATUS_IN_THE_VOTING => FeatureRequest::STATUS_VOTING,
        Broker::STATUS_REFUSED => FeatureRequest::STATUS_REFUSED,
        Broker::STATUS_IN_PROGRESS => FeatureRequest::STATUS_DEV
    ];
    public function safeUp()
    {
        $brokers = Broker::find()->where('status <> :status', [':status' => Broker::STATUS_CONNECTED])->all();
        return $this->setBrokersAsRequest($brokers);
    }

    public function safeDown()
    {
        FeatureRequest::deleteAll(['category_id' => 2]);
        return true;
    }


    /**
     * Конвертирует брокера в FeatureRequest
     * @param $brokers Broker[]
     * @return bool
     */
    private function setBrokersAsRequest($brokers)
    {
        foreach($brokers as $broker){
            $request = new FeatureRequest();
            $request->title = sprintf('Add %s broker', $broker->name);
            $request->content = sprintf('Please add <a href="%s">%s</a> broker', $broker->real_url, $broker->name);
            $request->category_id = 2;
            $request->status = $this->statusMap[$broker->status];
            $request->published = 0;
            $request->user_id = 1;
            //задает голоса
            $like = Likes::find()->where('entity = :entity AND entity_id = :entity_id', [':entity' => $broker->getTypeEntity(), ':entity_id' => $broker->id])->one();
            if($like && $like->user instanceof User){
                $request->user_id = $like->user->id;
            }
            if(!$request->save()){
                print_r($request->getErrors());
                return false;
            }
            if($like && $like->user instanceof User){
                Vote::upFake($request->getTypeEntity(), $request->id, 1, $request->user_id);
            }
        }
    }

}
