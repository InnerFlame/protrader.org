<?php

use yii\db\Schema;
use yii\db\Migration;

class m160126_163721_add_column_is_ptmc_team extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'is_ptmc_team', "smallint(4)  DEFAULT 0 AFTER `status`");

        $this->db->schema->refresh();
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'is_ptmc_team');
    }
}
