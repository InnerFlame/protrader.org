<?php

use yii\db\Schema;
use yii\db\Migration;

class m151215_095036_add_column_pictures_to_video_table extends Migration
{
    public function up()
    {

        $this->addColumn('{{%vd_video}}', 'pictures', "VARCHAR(60) NULL DEFAULT NULL AFTER `title`");

        $this->db->schema->refresh();


    }

    public function down()
    {
        $this->dropColumn('{{%vd_video}}', 'pictures');

    }
}

