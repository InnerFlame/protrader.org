<?php

use yii\db\Schema;
use yii\db\Migration;

use app\modules\forum\models\FrmCategory;

class m151211_142745_frm_category_add_parent_id extends Migration
{

    public function before()
    {
        $root = new FrmCategory();
        $root->title = 'Root';
        $root->alias = 'root';
        $root->description = 'Root';
        $root->weight = 0;
        $root->makeRoot();

    }

    public function up()
    {

        $this->addColumn('{{%frm_category}}', 'parent_id', "INT(11) DEFAULT NULL AFTER `id`");

        $this->db->schema->refresh();

        $this->before();
    }

    public function down()
    {
        $this->dropColumn('{{%frm_category}}', 'parent_id');

    }
}


