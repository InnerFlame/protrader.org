<?php

use yii\db\Migration;

class m160520_134420_create_table_chart_user_settings extends Migration
{
    const TBL_NAME_USER_SETTINGS = '{{%chart_user_settings}}';

    const TBL_NAME_PANELS = '{{%chart_panels}}';

    const TBL_NAME_PANELS_SETTINGS = '{{%chart_panels_settings}}';




    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
            $tableOptionsMyISAM = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=MyISAM';
        }

        $this->createTable(self::TBL_NAME_PANELS, [
            'id' => $this->primaryKey(),
            'name' => $this->string(2)->notNull(),
            'label' => $this->string()->notNull(),
            'enabled' => $this->integer(1)->defaultValue(1)
        ],$tableOptions);

        $this->createTable(self::TBL_NAME_PANELS_SETTINGS, [
            'id' => $this->primaryKey(),
            'panel_id' => $this->integer(2)->notNull(),
            'value' => $this->string()->notNull(),
            'default' => $this->string()->notNull()
        ], $tableOptions);

        $this->createTable(self::TBL_NAME_USER_SETTINGS, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'panel_id' => $this->integer(2)->notNull(),
            'value' => $this->string()->notNull()
        ], $tableOptions);

        $this->addForeignKey('fk_chart_panels_settings_to_panels', self::TBL_NAME_PANELS_SETTINGS, 'panel_id', self::TBL_NAME_PANELS, 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_chart_user_settings_to_panels', self::TBL_NAME_USER_SETTINGS, 'panel_id', self::TBL_NAME_PANELS, 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_chart_user_settings_to_user', self::TBL_NAME_USER_SETTINGS, 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');

    }

    public function down()
    {
        $this->dropForeignKey('fk_chart_panels_settings_to_panels', self::TBL_NAME_PANELS_SETTINGS);
        $this->dropForeignKey('fk_chart_user_settings_to_panels', self::TBL_NAME_USER_SETTINGS);
        $this->dropForeignKey('fk_chart_user_settings_to_user', self::TBL_NAME_USER_SETTINGS);
        $this->dropTable(self::TBL_NAME_PANELS);
        $this->dropTable(self::TBL_NAME_PANELS_SETTINGS);
        $this->dropTable(self::TBL_NAME_USER_SETTINGS);

    }
}
