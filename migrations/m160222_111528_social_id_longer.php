<?php

use yii\db\Schema;
use yii\db\Migration;

class m160222_111528_social_id_longer extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%user}}', 'vkontakte_id', "VARCHAR(32) DEFAULT NULL AFTER `pictures`");
        $this->alterColumn('{{%user}}', 'google_id', "VARCHAR(32) DEFAULT NULL AFTER `vkontakte_id`");
        $this->alterColumn('{{%user}}', 'linkedin_id', "VARCHAR(32) DEFAULT NULL AFTER `google_id`");
        $this->alterColumn('{{%user}}', 'facebook_id', "VARCHAR(32) DEFAULT NULL AFTER `linkedin_id`");
        $this->alterColumn('{{%user}}', 'twitter_id', "VARCHAR(32) DEFAULT NULL AFTER `facebook_id`");
    }


    public function down()
    {
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
