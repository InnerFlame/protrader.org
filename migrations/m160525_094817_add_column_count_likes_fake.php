<?php

use yii\db\Migration;

class m160525_094817_add_column_count_likes_fake extends Migration
{
    public function up()
    {
        $this->addColumn('{{%com_counts}}', 'count_like_fake', 'INT(8) DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn('{{%com_counts}}', 'count_like_fake', 'INT(8) DEFAULT 0');
        return true;
    }
}
