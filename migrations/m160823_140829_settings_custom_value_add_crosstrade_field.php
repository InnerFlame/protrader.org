<?php

use yii\db\Migration;
use app\models\setting\SettingsCustomField;
use app\models\setting\SettingsCustomValue;

class m160823_140829_settings_custom_value_add_crosstrade_field extends Migration
{
    public function up()
    {
        $field = new SettingsCustomField();
        $field->name = 'is_subscription';
        $field->alias = 'is_subscription';
        $field->save();

        $value = new SettingsCustomValue();
        $value->field_id = $field->id;
        $value->value = 0;
        $value->save(false);
    }

    public function down()
    {
        return true;
    }
}
