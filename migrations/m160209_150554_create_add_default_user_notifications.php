<?php

use yii\db\Schema;
use yii\db\Migration;

class m160209_150554_create_add_default_user_notifications extends Migration
{
    public function up()
    {
        $builder = new \yii\db\Query();
        $users = $builder->select('id')
            ->from('user')
            ->all();

        $types = $builder->select('id')
            ->from('notifications_type')
            ->where('enabled = 1')
            ->all();

        $values = array();
        foreach($users as $user){
            foreach($types as $type){
                $values[] = "({$user['id']}, {$type['id']}, 1)";
            }
        }
        $values = implode(',', $values);
        $this->getDb()->createCommand("INSERT INTO user_notifications (user_id, type_id, enabled) VALUES $values")->execute();
    }

    public function down()
    {
        $this->getDb()->createCommand("TRUNCATE TABLE user_notifications")->execute();
        return true;
    }
}
