<?php

use yii\db\Migration;

class m160217_122011_changed_datetime_columns_notifications_table extends Migration
{
    public function up()
    {
        $this->dropColumn("{{%notifications}}",'create_at');
        $this->dropColumn("{{%notifications}}",'send_date');

        $this->addColumn("{{%notifications}}",'created_at', 'int');
        $this->addColumn("{{%notifications}}",'send_date', 'int');

    }

    public function down()
    {
        $this->dropColumn("{{%notifications}}",'created_at');
        $this->dropColumn("{{%notifications}}",'send_date');

        $this->addColumn("{{%notifications}}",'create_at', 'timestamp');
        $this->addColumn("{{%notifications}}",'send_date', 'timestamp');

        return true;
    }
}
